//Script created by gfern

import chest.createChest;
import chest.reward;
import difficulty.setLevel;

//-------------------------------------
//Variables and Constants
//-------------------------------------
chestMax = random(3, 6);
chestCurr = 0;
level = 10;
underDifficulty = 1;

CHEST_ROLL = 0..100;
CHEST_REQ = 0;
TYPE_ROLL = 100;
MIMIC_REQ = 80;
RING_LEVEL = 9;
RING_STEPS = 0..5;
MIN_LEVEL = 12;
MAX_LEVEL = 12;

//-------------------------------------
//Maps and Lists
//-------------------------------------
lootTypeList = ["gold", "orb", "ring"];
lootRarityMap = ["gold":100, "orb":10, "common":20, "uncommon":5, "ring":1];
lootItemMap = ["gold":2500..3000, "orb":4..8, "common":[], "uncommon":[], "ring":["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"]]
diffMultMap = [1:["gold":0.5, "orb":0.5], 2:["gold":1, "orb":1], 3:["gold":3, "orb":2]];

underChestList = [] as Set;
underSpawnerList = [] as Set;
difficultyMap = [1:0, 2:0.5, 3:1.0];

//-------------------------------------
//Chest Definitions
//-------------------------------------
c = new createChest();
r = new reward();

chest_5 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_5, "chest_5", "mimic_monster", "mimic_chest", 1, 1280, 460, "Under2_5", 1, 90, this);
underChestList << chest_5;

chest_7 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_7, "chest_7", "mimic_monster", "mimic_chest", 1, 690, 450, "Under2_7", 1, 90, this);
underChestList << chest_7;

chest_203 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_203, "chest_203", "mimic_monster", "mimic_chest", 1, 530, 730, "Under2_203", 1, 90, this);
underChestList << chest_203;

chest_205 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_205, "chest_205", "mimic_monster", "mimic_chest", 1, 850, 620, "Under2_205", 1, 90, this);
underChestList << chest_205;

chest_207 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_207, "chest_207", "mimic_monster", "mimic_chest", 1, 950, 370, "Under2_207", 1, 90, this);
underChestList << chest_207;

chest_401 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_401, "chest_401", "mimic_monster", "mimic_chest", 1, 1100, 440, "Under2_401", 1, 90, this);
underChestList << chest_401;

chest_403 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_403, "chest_403", "mimic_monster", "mimic_chest", 1, 440, 760, "Under2_403", 1, 90, this);
underChestList << chest_403;

chest_405 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_405, "chest_405", "mimic_monster", "mimic_chest", 1, 1270, 370, "Under2_405", 1, 90, this);
underChestList << chest_405;

chest_407 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_407, "chest_407", "mimic_monster", "mimic_chest", 1, 900, 340, "Under2_407", 1, 90, this);
underChestList << chest_407;

chest_601 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_601, "chest_601", "mimic_monster", "mimic_chest", 1, 1470, 340, "Under2_601", 1, 90, this);
underChestList << chest_601;

chest_603 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_603, "chest_603", "mimic_monster", "mimic_chest", 1, 1180, 120, "Under2_603", 1, 90, this);
underChestList << chest_603;

chest_605 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_605, "chest_605", "mimic_monster", "mimic_chest", 1, 320, 390, "Under2_605", 1, 90, this);
underChestList << chest_605;

chest_607 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_607, "chest_607", "mimic_monster", "mimic_chest", 1, 1350, 460, "Under2_607", 1, 90, this);
underChestList << chest_607;

chest_801 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_801, "chest_801", "mimic_monster", "mimic_chest", 1, 1030, 500, "Under2_801", 1, 90, this);
underChestList << chest_801;

chest_803 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_803, "chest_803", "mimic_monster", "mimic_chest", 1, 1150, 260, "Under2_803", 1, 90, this);
underChestList << chest_803;

chest_805 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_805, "chest_805", "mimic_monster", "mimic_chest", 1, 280, 650, "Under2_805", 1, 90, this);
underChestList << chest_805;

chest_807 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_807, "chest_807", "mimic_monster", "mimic_chest", 1, 1100, 650, "Under2_807", 1, 90, this);
underChestList << chest_807;

chest_1001 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_1001, "chest_1001", "mimic_monster", "mimic_chest", 1, 300, 600, "Under2_1001", 1, 90, this);
underChestList << chest_1001;

chest_1003 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_1003, "chest_1003", "mimic_monster", "mimic_chest", 1, 390, 260, "Under2_1003", 1, 90, this);
underChestList << chest_1003;

chest_1005 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_1005, "chest_1005", "mimic_monster", "mimic_chest", 1, 960, 810, "Under2_1005", 1, 90, this);
underChestList << chest_1005;

chest_1007 = c.chestOrMonster(CHEST_ROLL, CHEST_REQ, TYPE_ROLL, MIMIC_REQ, myRooms.Under2_1007, "chest_1007", "mimic_monster", "mimic_chest", 1, 1410, 670, "Under2_1007", 1, 90, this);
underChestList << chest_1007;

underChestList.remove(null)

//-------------------------------------
//Chest Logic
//-------------------------------------
l = new setLevel();

def checkLevel() {
	if(myArea.getAllPlayers().size() > 0) {
		player = random(myArea.getAllPlayers());
		underDifficulty = player.getTeam().getAreaVar("Z30_Undermountain", "Z30DeadmansDifficulty");
		if(!underDifficulty) { underDifficulty = 2 }
		
		player.getCrew().each() {
			if(it.getConLevel() + difficultyMap.get(underDifficulty) > level) {
				levelAdjust();
			}
		}
	}

	myManager.schedule(30) { checkLevel(); }
}

def levelAdjust() {
	level = l.setSpawnerLevel(MIN_LEVEL, MAX_LEVEL, underDifficulty, difficultyMap, player, underSpawnerList);
}

def spawnUnderChests() {
	if(chestCurr < chestMax) {
		chestCurr++;
		nextChestSpawner = random(underChestList);
		underChestList.remove(nextChestSpawner);
		underSpawnerList << nextChestSpawner;
		
		nextChest = nextChestSpawner.forceSpawnNow();
		r.rewardPlayersDMS(nextChest, 1, diffMultMap, lootTypeList, lootRarityMap, lootItemMap, RING_LEVEL, RING_STEPS, this);
		
		spawnUnderChests();
	}
}

def initArea() {
	myManager.schedule(1) { 
		if(myArea.getAllPlayers().size() > 0) {
			checkLevel();
			spawnUnderChests();
		} else {
			myManager.schedule(1) {
				initArea();
			}
		} 
	}
}

initArea();
