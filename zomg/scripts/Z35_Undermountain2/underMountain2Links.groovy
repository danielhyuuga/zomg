//Script created by gfern   **  AND RCOX!!! **  and lots of 5 hour energies
import switches.DungeonGen
import switches.switchActions;

//-----------------------------------------------------
//Constants & Variable Init
//-----------------------------------------------------
roomLinkCounter = 0;
areaInit = false;

//-----------------------------------------------------
//Switches
//-----------------------------------------------------
w = new switchActions(scriptObj:this);

cross405A2 = w.simpleWarp("cross405A", myRooms.Under2_405, 440, 270, "Under2_405", 730, 270);
cross405B2 = w.simpleWarp("cross405B", myRooms.Under2_405, 700, 190, "Under2_405", 410, 360);
cross407A2 = w.simpleWarp("cross407A", myRooms.Under2_407, 1530, 340, "Under2_407", 1500, 750);
cross407B2 = w.simpleWarp("cross407B", myRooms.Under2_407, 1530, 750, "Under2_407", 1500, 340);
cross601A2 = w.simpleWarp("cross601A", myRooms.Under2_601, 450, 250, "Under2_601", 730, 270);
cross601B2 = w.simpleWarp("cross601B", myRooms.Under2_601, 670, 170, "Under2_601", 420, 350);
cross603A2 = w.simpleWarp("cross603A", myRooms.Under2_603, 450, 250, "Under2_603", 730, 270);
cross603B2 = w.simpleWarp("cross603B", myRooms.Under2_603, 670, 170, "Under2_603", 420, 350);
cross605A2 = w.simpleWarp("cross605A", myRooms.Under2_605, 1110, 250, "Under2_605", 830, 270);
cross605B2 = w.simpleWarp("cross605B", myRooms.Under2_605, 860, 170, "Under2_605", 1130, 350);
cross607A2 = w.simpleWarp("cross607A", myRooms.Under2_607, 20, 730, "Under2_607", 70, 370);
cross607B2 = w.simpleWarp("cross607B", myRooms.Under2_607, 20, 370, "Under2_607", 70, 730);
cross801A2 = w.simpleWarp("cross801A", myRooms.Under2_801, 1110, 250, "Under2_801", 830, 270);
cross801B2 = w.simpleWarp("cross801B", myRooms.Under2_801, 860, 170, "Under2_801", 1130, 350);

def synchronized initRooms() {
	if(areaInit) {
		return;
		// [rc] Already inited!
	}
	areaInit = true;

	def dungeonGen = new DungeonGen(myRooms, w)

	def linkBack = getScriptObject("under1LinkBack")
	w.warpNoAggro( linkBack.under2Entrance, linkBack.under1Exit )
	dungeonGen.prune(dungeonGen.under2Map, linkBack.under2Entrance)

	def u2Exit = dungeonGen.linkNodes(dungeonGen.under2Map, dungeonGen.under2Map[linkBack.under2Entrance.room][0])
	println "Under 2 exit link is ${u2Exit.switchName}"

	w.warpNoAggro(u2Exit, [room:"Under3_1", targetX:130, targetY:310])

	setScriptObject("under2LinkBack", u2Exit)
}

def grueCheck() {
	if( ( gst() > 0 && gst() < 50 ) || ( gst() > 2350 && gst() < 2400 ) ) { //Check time
		grueRoll = random(1, 100); //Roll between 1 and 100
		if(grueRoll >= 98) { //Check if roll higher than 98
			myArea.getAllPlayers().clone().each {
				if(isOnline(it) && isInZone(it)) { //For each person on grueList, check that they are still in M1
					if(it.sortedEquippedRings.toString().contains("Solar Rays")) {
						it.centerPrint("You have entered a dark place. Rays of sunlight protect you.");
					} else {
						it.centerPrint("Oh, no! You have walked into the slavering fangs of a lurking grue!");
						it.smite();
					}
				}
			}
		} else {
			myManager.schedule(60) { grueCheck() }
		}
	} else {
		myManager.schedule(60) { grueCheck() }
	}
}

myManager.schedule(60) { grueCheck() }

onZoneIn() { event ->
	if(isPlayer(event.player) && event.player.isOnQuest(364, 2)) {
		event.player.updateQuest(364, "[NPC] Deva");
	}

	initRooms(); // [rc] Moved the check for "is inited" to the initRooms function so it can hide behind synchronize
}
