import com.gaiaonline.mmo.battle.communication.PlatformServerResponse;

swarf = spawnNPC( "swarf",  myRooms.TestIsland1_101, 100, 200 );
halzy = spawnNPC( "halzy",  myRooms.TestIsland1_101, 200, 200 );
bronstahd = spawnNPC( "bronstahd",  myRooms.TestIsland1_101, 300, 200 );
Trixie = spawnNPC("Trixie-VQS", myRooms.TestIsland1_101, 400, 200);
William = spawnNPC("BFG-William", myRooms.TestIsland1_101, 500, 200);
Frank = spawnNPC("Frank-VQS", myRooms.TestIsland1_101, 600, 200);
Terry = spawnNPC("Terry-VQS", myRooms.TestIsland1_101, 700, 200);

flajeu = spawnNPC( "flajeu",  myRooms.TestIsland1_101, 100, 300 );
mr_ubin = spawnNPC( "mr_ubin",  myRooms.TestIsland1_101, 200, 300 );
qixter = spawnNPC( "qixter",  myRooms.TestIsland1_101, 300, 300 );
Ian = spawnNPC("BartonIan", myRooms.TestIsland1_101, 400, 300);
rufus = spawnNPC("Rufus-VQS", myRooms.TestIsland1_101, 500, 300);
Labtech123 = spawnNPC("123-VQS", myRooms.TestIsland1_101, 600, 300);
Perry = spawnNPC("Perry-VQS", myRooms.TestIsland1_101, 700, 300);

Remo = spawnNPC("BFG-Remo", myRooms.TestIsland1_101, 300, 500);
Purvis = spawnNPC("Purvis-VQS", myRooms.TestIsland1_101, 400, 500);

Leon = spawnNPC("Leon-VQS", myRooms.TestIsland1_101, 300, 600);
Bill = spawnNPC("Rancher Bill-VQS", myRooms.TestIsland1_101, 400, 600);



def conv1 = Frank.createConversation("FrankOptions", true)

def dlg1 = [id:1]
dlg1.npctext = "What would you like to do?"
dlg1.options = []
dlg1.options << [text:"Toggle chattering", exec:{ event -> toggleChatter() }, result:DONE]
dlg1.options << [text:"Play all rings", exec:{ event -> playRings(event.actor) }, result:DONE]

conv1.addDialog(dlg1, Frank)

players = [
			swarf, 		flajeu, 	
			halzy,		mr_ubin,
			bronstahd, 	qixter, 
			Trixie, 	Ian,
			William, 	rufus,
			Frank,		Labtech123,
			Terry,		Perry,
			Remo,		Leon,
			Purvis,		Bill
		  ];
rageRanks = [0, 1, 2, 3];

numberOfAttacksPerRing = 5;
timePerAttack = 3;

def attack(ringID, rageRank, attacker, defender, watcher)
{
	def attackResponse = [
		bmt:"action", 
		displayType:"external",	
		id:attacker.getID(),
		length:0,
		rid:ringID,	
		rl:rageRank,
		speed:0,	
		tid:defender.getID()	
	];
	def bsrAttack = new PlatformServerResponse( 0l, "act" );
	bsrAttack.setProperty("result", attackResponse);
	myManager.postmaster.pushResult(bsrAttack, watcher.getGameUSer());
	
	def attackHits = [
		defender.getID()
	];
	
	def hitResponse = [
		actorID:attacker.getID(),
		bmt:"std", 
		displayType:"external",
		hits:attackHits,
		length:0,
		rid:ringID,	
		rl:rageRank,
		tid:defender.getID()	
	];
	def bsrHit = new PlatformServerResponse( 0l, "act" );
	bsrHit.setProperty("result", hitResponse);
	myManager.postmaster.pushResult(bsrHit, watcher.getGameUSer());
}

rings = [17710, 17711, 17712, 17713, 17714, 17715, 17716, 17717, 17718, 17719, 17720, 17721, 17722, 17723, 17724, 17725, 17726, 17727, 17728, 17729, 17730, 17731, 17732, 17733, 17734, 17735, 17736, 17737, 17738, 17739, 17740, 17741, 17742, 17743, 17744, 17745, 17747, 17748, 17749, 17866];

def runRing(ring, rageRank, watcher, attacksLeft)
{
	def localPlayers = players.clone();
	while(localPlayers.size() > 0)
	{
		def attacker = localPlayers.remove(0);
		def defender = localPlayers.remove(0);
		attack(ring, rageRank, attacker, defender, watcher);
	}
	
	myManager.schedule(timePerAttack) {
		if(attacksLeft > 0) {
			runRing(ring, rageRank, watcher, attacksLeft -1 );
		}
		else
		{
			playRings(watcher);
		}
	}
}

ringsPlayed = 0;

def playRings(watcher)
{
	def rageRank = ringsPlayed % 4;
	
	def ring = null;
	
	if(rageRank == 3) {
		ring = rings.remove(0);
	} else {
		ring = rings.get(0);
	}
	
	watcher.say("ATTACK: $ring $rageRank");
	runRing(ring, rageRank, watcher, numberOfAttacksPerRing);
	
	++ringsPlayed;
}


myManager.onEnter(myRooms.TestIsland1_101) { event ->
	if( isPlayer( event.actor ) ) {
		//myManager.schedule(10) {
			showRegPrompt(event.actor);
			//playRings(event.actor);
		//}
	}
}


chatter = false
def toggleChatter()
{
	if(chatter) {
		chatter = false
	}
	else
	{
		chatter=true
		makeChatter()
	}
}

def makeChatter()
{
	if(chatter)
	{
		def actor = random(players)

		actor.say("This is some random chatter to fill up the chat window.  Talk to the same NPC to turn off the chatter.")
		myManager.schedule(0.5) {
			makeChatter()
		}
	}
}
