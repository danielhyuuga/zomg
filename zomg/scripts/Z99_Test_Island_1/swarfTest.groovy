
ryan = spawnNPC( "swarf",  myRooms.TestIsland1_1, 500, 400 )
ryan.setDisplayName("[NPC] Swarf")

critter = null;

def spawnCritter() {
	critter = makeCritter("cow", myRooms.TestIsland1_1, 300, 300)

	critter.setUsable(true)
	critter.setDisplayName("Click me to pick up")
	critter.onUse { event ->
		if( event.critter.getLatchedTo() == event.player ) {
			event.critter.unLatch()
		}
		else
		{
			if(event.critter.isLatched()) {
				event.critter.say("I'm being stolen!")
			}
			event.critter.latchOnTo( event.player )

			event.critter.onDrop { dropEvent -> if(dropEvent.reason=="dazed") { dropEvent.actor.say("You fail") } }
		}
	}
}

def movieTest(player) {
	myManager.schedule(3) {
		if( isPlayer( player ) ) {
			playMovie( player, "splashscreen/news1.flv", 360,240) { 
				log("Testing the logging for scripts")
				player.centerPrint("Congratulations! You watched a movie!!")
				player.grantCoins(200)
			}
		}               
	}
}

def soulbindTest(player) {
	runOnSoulbind(player, 65103) { event ->
		if(event.result) {
			event.player.say("I have a soulbound ${event.itemID}");
		}
		else {
			event.player.say("I have no ${event.itemID}");
		}
	}
}

def conv1 = ryan.createConversation("RyansFirstTestConv", true)

def ryanMain1 = [id:1]
ryanMain1.npctext = "Chose"
ryanMain1.options = []
ryanMain1.options << [text:"Play a movie", exec: { event -> movieTest(event.player) }, result:DONE]
ryanMain1.options << [text:"Spawn a critter", exec:{ spawnCritter() }, result:1]
ryanMain1.options << [text:"Attach last critter to NPC Swarf", exec:{ event-> critter.latchOnTo(ryan) }, result:1]
ryanMain1.options << [text:"Detach last critter", exec:{ event-> critter.unLatch() }, result:1]
ryanMain1.options << [text:"Test monster aggro listener", exec:{ event -> testAggroEvent(event.player) }, result:1]
ryanMain1.options << [text:"Soulbind a boardwalk ticket!", exec:{ event -> soulbindTest(event.player) }, result:1]
conv1.addDialog(ryanMain1, ryan)

stealthSpawner1 = myRooms.TestIsland1_1.spawnStoppedSpawner( "testAggroSpawner", "omg", 1)
stealthSpawner1.setPos(100, 100)
stealthSpawner1.setHomeTetherForChildren(2000)
stealthSpawner1.setSpawnWhenPlayersAreInRoom(true)
stealthSpawner1.setMonsterLevelForChildren(10.0)

def testAggroEvent(player) {
	def monster = stealth1 = stealthSpawner1.forceSpawnNow()

	onAggroStart(monster) { event -> event.monster.say("I'm gonna getcha ${event.actor}!") }
	onAggroEnd(monster) { event -> event.monster.say("Ooh, a pretty flower.") }

	log("Testing aggro event with ${monster} for ${player}")
}



/// Testing quest removal  ///

Trixie = spawnNPC("Trixie-VQS", myRooms.TestIsland1_1, 700, 570)
Trixie.setRotation( 45 )
Trixie.setDisplayName( "TrixieTEST" )

def trixieConv = Trixie.createConversation("trixieConv", true)

def trixieTestDlg1 = [id:1]
trixieTestDlg1.npctext = "Click ok to get on this quest"
trixieTestDlg1.quest = 572
trixieTestDlg1.result = 2
trixieConv.addDialog( trixieTestDlg1, Trixie )

def trixieTestDlg2 = [id:2]
trixieTestDlg2.npctext = "Click ok to update this quest"
trixieTestDlg2.result = 3
trixieTestDlg2.quest = 572
trixieConv.addDialog( trixieTestDlg2, Trixie )

def trixieTestDlg3 = [id:3]
trixieTestDlg3.npctext = "Click ok to get removed from this quest"
trixieTestDlg3.result = DONE
trixieTestDlg3.exec = { event -> event.actor.removeQuest(572) }
trixieConv.addDialog( trixieTestDlg3, Trixie )
