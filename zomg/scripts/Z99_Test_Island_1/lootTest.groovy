//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

commonTestSpawner = myRooms.TestIsland1_2.spawnStoppedSpawner("testcommonspawner", "testcommon", 1)
commonTestSpawner.setPos(140, 610)
commonTestSpawner.setSpawnWhenPlayersAreInRoom(true)
commonTestSpawner.setMonsterLevelForChildren(1.0)
commonTestSpawner.setGuardPostForChildren("TestIsland1_2", 140, 610, 45)

commonTest = commonTestSpawner.forceSpawnNow()

commonDeathWatch()

def commonDeathWatch() {
	runOnDeath(commonTest) {
		myManager.schedule(3) {
			commonTest =commonTestSpawner.forceSpawnNow()
			commonDeathWatch()
		}
	}
}

uncommonTestSpawner = myRooms.TestIsland1_2.spawnStoppedSpawner("testuncommonspawner", "testuncommon", 1)
uncommonTestSpawner.setPos(240, 610)
uncommonTestSpawner.setSpawnWhenPlayersAreInRoom(true)
uncommonTestSpawner.setMonsterLevelForChildren(1.0)
uncommonTestSpawner.setGuardPostForChildren("TestIsland1_2", 240, 610, 45)

uncommonTest = uncommonTestSpawner.forceSpawnNow()

uncommonDeathWatch()

def uncommonDeathWatch() {
	runOnDeath(uncommonTest) {
		myManager.schedule(3) {
			uncommonTest =uncommonTestSpawner.forceSpawnNow()
			uncommonDeathWatch()
		}
	}
}

rareTestSpawner = myRooms.TestIsland1_2.spawnStoppedSpawner("testrarespawner", "testrare", 1)
rareTestSpawner.setPos(340, 610)
rareTestSpawner.setSpawnWhenPlayersAreInRoom(true)
rareTestSpawner.setMonsterLevelForChildren(1.0)
rareTestSpawner.setGuardPostForChildren("TestIsland1_2", 340, 610, 45)

rareTest = rareTestSpawner.forceSpawnNow()

rareDeathWatch()

def rareDeathWatch() {
	runOnDeath(rareTest) {
		myManager.schedule(3) {
			rareTest =rareTestSpawner.forceSpawnNow()
			rareDeathWatch()
		}
	}
}

recipeTestSpawner = myRooms.TestIsland1_2.spawnStoppedSpawner("testrecipespawner", "testrecipe", 1)
recipeTestSpawner.setPos(440, 610)
recipeTestSpawner.setSpawnWhenPlayersAreInRoom(true)
recipeTestSpawner.setMonsterLevelForChildren(1.0)
recipeTestSpawner.setGuardPostForChildren("TestIsland1_2", 440, 610, 45)

recipeTest = recipeTestSpawner.forceSpawnNow()

recipeDeathWatch()

def recipeDeathWatch() {
	runOnDeath(recipeTest) {
		myManager.schedule(3) {
			recipeTest =recipeTestSpawner.forceSpawnNow()
			recipeDeathWatch()
		}
	}
}

goldTestSpawner = myRooms.TestIsland1_2.spawnStoppedSpawner("testgoldspawner", "testgold", 1)
goldTestSpawner.setPos(540, 610)
goldTestSpawner.setSpawnWhenPlayersAreInRoom(true)
goldTestSpawner.setMonsterLevelForChildren(1.0)
goldTestSpawner.setGuardPostForChildren("TestIsland1_2", 540, 610, 45)

goldTest = goldTestSpawner.forceSpawnNow()

goldDeathWatch()

def goldDeathWatch() {
	runOnDeath(goldTest) {
		myManager.schedule(3) {
			goldTest =goldTestSpawner.forceSpawnNow()
			goldDeathWatch()
		}
	}
}