/*

import com.gaiaonline.mmo.battle.script.*;

//Coordinates for botKiosks

// Beach_402, 460, 350

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("kioskPosition", "markerOther", "Aqueduct_602", 580, 760, "HyperNet Access")

kiosk = makeSwitch( "botKiosk", myRooms.Aqueduct_602, 580, 760 )
kiosk.unlock()
kiosk.off()
kiosk.setRange( 200 )
kiosk.setMouseoverText("Access the HyperNet")

def clickKiosk = { event ->
	kiosk.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk.whenOn( clickKiosk )


//====================================================
// TUTORIAL TREE                                      
//====================================================

kioskWidth = 300

def giveReward( event ) {
	//gold reward
	goldGrant = random( 50, 150 )
	event.player.centerPrint( "You receive ${goldGrant} gold!" )
	event.player.grantCoins( goldGrant )

	//orb reward
	orbGrant = 2
	event.player.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
	event.player.grantQuantityItem( 100257, orbGrant )
}
	
def synchronized makeKioskMenu( player ) {
	titleString = "The HyperNet"
	descripString = "Choose Game Help to access loads of information about the game, or one of the other categories for easy downloads of useful tidbits."
	diffOptions = ["Game Help", "Area Info", "Exit HyperNet"]
	
	uiButtonMenu( player, "kioskMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Game Help" ) {
			showWidget( event.actor, "GameHelp", true, true )
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXGameHelpSelected" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXGameHelpSelected" )
				//gold reward
				goldGrant = random( 50, 150 )
				event.actor.centerPrint( "You receive ${goldGrant} gold!" )
				event.actor.grantCoins( goldGrant )

				//orb reward
				orbGrant = 2
				event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				event.actor.grantQuantityItem( 100257, orbGrant )
			}
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
		if( event.selection == "Tutorials" ) {
			makeTutorialMenu( player )
		}
		if( event.selection == "Area Info" ) {
			makeAreaMenu( player )
		}
		if( event.selection == "Exit HyperNet" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
	}
}

def makeAreaMenu( player ) {
	titleString = "Area Info"
	descripString = "A small archive of stories and tidbits of the area around you."
	diffOptions = ["Old Aqueduct", "Main Menu"]
	
	uiButtonMenu( player, "fictionMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Old Aqueduct" ) {
			tutorialNPC.pushDialog( event.actor, "oldAqueduct" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}
	


def makeTutorialMenu( player ) {
	titleString = "Tutorials"
	descripString = "These tutorials give you quick overviews on various features in the game."
	diffOptions = ["Main Menu"]
	
	uiButtonMenu( player, "tutorialMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// TUTORIAL MENU                                                                        
//======================================================================================
//======================================================================================



//======================================================================================
//======================================================================================
// AREA MENU                                                                            
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//BASS'KEN LAKE BACKGROUND                                                              
//--------------------------------------------------------------------------------------
def oldAqueduct = tutorialNPC.createConversation( "oldAqueduct", true )

def old1 = [id:1]
old1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Old Aqueduct</font></b></h1><br><font face='Arial' size ='12'>The Old Aqueduct in this area is now in ruins, but it once provided the main source of drinking water for Barton Town.<br><br>Now that the Durem Reclamation Facility is in place, the aqueduct has been allowed to fall into disrepair and the jungle to the NW has quickly begun to reclaim its stones.<br><br>]]></zOMG>"
old1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Aqueduct_602 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXOldAqueduct" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXOldAqueduct" )
			giveReward( event )
		}
	}
}
old1.result = DONE
oldAqueduct.addDialog( old1, tutorialNPC )

*/

