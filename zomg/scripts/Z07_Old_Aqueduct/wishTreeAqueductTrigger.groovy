import com.gaiaonline.mmo.battle.script.*;

//===============================
//WISH TREE VISION TRIGGER ZONE  
//===============================

onQuestStep(51, 2) { event -> event.player.addMiniMapQuestActorName( "Wish Tree-VQS" ) }

//=========THE AQUEDUCT==========
def wishTreeAqueductTrigger = "wishTreeAqueductTrigger"
myRooms.Aqueduct_104.createTriggerZone(wishTreeAqueductTrigger, 540, 435, 860, 650)

myManager.onTriggerIn(myRooms.Aqueduct_104, wishTreeAqueductTrigger) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(51, 2) && !event.actor.hasQuestFlag( GLOBAL, "Z07FoundWishTreeAqueductClue" ) ) {
		event.actor.say("I found the Wish Tree vision area...the Old Aqueduct ruins near the Jungle's edge.")
		event.actor.updateQuest(51, "Story-VQS" ) //push completion for this leg of the Vision Quest
		event.actor.setQuestFlag( GLOBAL, "Z07FoundWishTreeAqueductClue" )
	}
}
