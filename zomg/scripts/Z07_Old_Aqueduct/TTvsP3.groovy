import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// THE BATTLE ROYALE                        
//------------------------------------------

// Spawn the TTs and move them toward the encounter area
// Spawn the P3s and move them toward the encounter area
// If they encounter each other, they fight.
// Patrol in circles on the peanut brittle areas and then return back to burrow, being disposed of for a few seconds on their return before generating again and heading out. (Same with TTs)

//------------------------------------------
// TRIGGER ZONES (for disposals)            
//------------------------------------------

def ttDisposalZoneOne = "ttDisposalZoneOne"
myRooms.Aqueduct_103.createTriggerZone(ttDisposalZoneOne, 440, 270, 580, 380)

def ttDisposalZoneTwo = "ttDisposalZoneTwo"
myRooms.Aqueduct_103.createTriggerZone(ttDisposalZoneTwo, 870, 330, 1000, 440)

def ttDisposalZoneThree = "ttDisposalZoneThree"
myRooms.Aqueduct_104.createTriggerZone(ttDisposalZoneThree, 50, 390, 190, 510)

def p3BurrowThreeDisposalZone = "p3BurrowThreeDisposalZone"
myRooms.Aqueduct_403.createTriggerZone(p3BurrowThreeDisposalZone, 660, 200, 820, 310)

def p3BurrowFourDisposalZone = "p3BurrowFourDisposalZone"
myRooms.Aqueduct_305.createTriggerZone(p3BurrowFourDisposalZone, 50, 530, 185, 665)

//------------------------------------------
// PLAYER COUNT IN WARZONE                  
// Spawning strategy in the warzone is      
// dictated by how many players are in the  
// area so as to keep camping from being    
// too easy to accomplish.                  
//------------------------------------------

playersInWarzone = 0
warzoneList = []

//PLAYER COUNT IN ROOM 103
myManager.onEnter(myRooms.Aqueduct_103) { event ->
	if( isPlayer(event.actor) ) {
		warzoneList << event.actor
		playersInWarzone ++
	}
}

myManager.onExit(myRooms.Aqueduct_103) { event ->
	if( isPlayer(event.actor) ) {
		warzoneList.remove( event.actor )
		playersInWarzone --
	}
}

//PLAYER COUNT IN ROOM 104
myManager.onEnter(myRooms.Aqueduct_104) { event ->
	if( isPlayer(event.actor) ) {
		warzoneList << event.actor
		playersInWarzone ++
	}
}

myManager.onExit(myRooms.Aqueduct_104) { event ->
	if( isPlayer(event.actor) ) {
		warzoneList.remove( event.actor )
		playersInWarzone --
	}
}

//PLAYER COUNT IN ROOM 203
myManager.onEnter(myRooms.Aqueduct_203) { event ->
	if( isPlayer(event.actor) ) {
		warzoneList << event.actor
		playersInWarzone ++
	}
}

myManager.onExit(myRooms.Aqueduct_203) { event ->
	if( isPlayer(event.actor) ) {
		playersInWarzone --
		warzoneList.remove( event.actor )
	}
}

//PLAYER COUNT IN ROOM 204
myManager.onEnter(myRooms.Aqueduct_204) { event ->
	if( isPlayer(event.actor) ) {
		warzoneList << event.actor
		playersInWarzone ++
	}
}

myManager.onExit(myRooms.Aqueduct_204) { event ->
	if( isPlayer(event.actor) ) {
		playersInWarzone --
		warzoneList.remove( event.actor )
	}
}

//PLAYER COUNT IN ROOM 303
myManager.onEnter(myRooms.Aqueduct_303) { event ->
	if( isPlayer(event.actor) ) {
		warzoneList << event.actor
		playersInWarzone ++
	}
}

myManager.onExit(myRooms.Aqueduct_303) { event ->
	if( isPlayer(event.actor) ) {
		playersInWarzone --
		warzoneList.remove( event.actor )
	}
}

//------------------------------------------
// WARZONE SPAWNERS                         
//------------------------------------------

tinyOne = myRooms.Aqueduct_103.spawnSpawner( "tinyOne", "tiny_terror_war", 20 )
tinyOne.setPos( 320, 420 )
tinyOne.setHateRadiusForChildren( 1500 )
tinyOne.setMonsterLevelForChildren( 5.4 )
tinyOne.addPatrolPointForChildren( "Aqueduct_203", 630, 140, 0 )
tinyOne.addPatrolPointForChildren( "Aqueduct_203", 620, 600, 0 )
tinyOne.addPatrolPointForChildren( "Aqueduct_303", 460, 155, 0 )
tinyOne.addPatrolPointForChildren( "Aqueduct_303", 770, 250, 0 )
tinyOne.addPatrolPointForChildren( "Aqueduct_303", 920, 170, 3 )
tinyOne.addPatrolPointForChildren( "Aqueduct_203", 620, 600, 0 )
tinyOne.addPatrolPointForChildren( "Aqueduct_203", 630, 140, 0 )
tinyOne.addPatrolPointForChildren( "Aqueduct_103", 510, 330, 0 )

tinyTwo = myRooms.Aqueduct_103.spawnSpawner( "tinyTwo", "tiny_terror_war", 20 )
tinyTwo.setPos( 920, 490 )
tinyTwo.setHateRadiusForChildren( 1500 )
tinyTwo.setMonsterLevelForChildren( 5.4 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_203", 930, 190, 0 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_203", 660, 320, 0 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_303", 920, 170, 0 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_303", 770, 250, 5 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_303", 460, 155, 0 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_203", 620, 600, 0 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_203", 930, 190, 0 )
tinyTwo.addPatrolPointForChildren( "Aqueduct_103", 945, 380, 0 )

tinyThree = myRooms.Aqueduct_104.spawnSpawner( "tinyThree", "tiny_terror_war", 20 )
tinyThree.setPos( 130, 530 )
tinyThree.setHateRadiusForChildren( 1500 )
tinyThree.setMonsterLevelForChildren( 5.4 )
tinyThree.addPatrolPointForChildren( "Aqueduct_203", 930, 190, 0 )
tinyThree.addPatrolPointForChildren( "Aqueduct_203", 630, 440, 0 )
tinyThree.addPatrolPointForChildren( "Aqueduct_203", 620, 600, 0 )
tinyThree.addPatrolPointForChildren( "Aqueduct_303", 625, 90, 0 )
tinyThree.addPatrolPointForChildren( "Aqueduct_303", 920, 170, 0 )
tinyThree.addPatrolPointForChildren( "Aqueduct_303", 770, 250, 4 )
tinyThree.addPatrolPointForChildren( "Aqueduct_203", 810, 605, 0 )
tinyThree.addPatrolPointForChildren( "Aqueduct_203", 930, 190, 0 )
tinyThree.addPatrolPointForChildren( "Aqueduct_104", 120, 450, 0 )

pupThree = myRooms.Aqueduct_403.spawnSpawner( "pupThree", "p3_war", 20 )
pupThree.setPos( 230, 520 )
pupThree.setHateRadiusForChildren( 1500 )
pupThree.setMonsterLevelForChildren( 5.4 )
pupThree.addPatrolPointForChildren( "Aqueduct_303", 460, 550, 0 )
pupThree.addPatrolPointForChildren( "Aqueduct_303", 460, 155, 0 )
pupThree.addPatrolPointForChildren( "Aqueduct_303", 920, 170, 3 )
pupThree.addPatrolPointForChildren( "Aqueduct_303", 770, 250, 0 )
pupThree.addPatrolPointForChildren( "Aqueduct_303", 480, 280, 0 )
pupThree.addPatrolPointForChildren( "Aqueduct_403", 740, 240, 0 )

pupFour = myRooms.Aqueduct_305.spawnSpawner( "pupFour", "p3_war", 20 )
pupFour.setPos( 100, 625 )
pupFour.setHateRadiusForChildren( 1500 )
pupFour.setMonsterLevelForChildren( 5.4 )
pupFour.addPatrolPointForChildren( "Aqueduct_304", 600, 400, 0 )
pupFour.addPatrolPointForChildren( "Aqueduct_303", 920, 170, 0 )
pupFour.addPatrolPointForChildren( "Aqueduct_303", 460, 155, 5 )
pupFour.addPatrolPointForChildren( "Aqueduct_303", 770, 250, 0 )
pupFour.addPatrolPointForChildren( "Aqueduct_304", 600, 400, 0 )
pupFour.addPatrolPointForChildren( "Aqueduct_305", 110, 610, 0 )

//Make them all hate each other
tinyOne.feudWithSpawner( pupThree )
tinyOne.feudWithSpawner( pupFour )
tinyTwo.feudWithSpawner( pupThree )
tinyTwo.feudWithSpawner( pupFour )
tinyThree.feudWithSpawner( pupThree )
tinyThree.feudWithSpawner( pupFour )

//------------------------------------------
// Spawner Lists (for randomizing spawners) 
//------------------------------------------

listTTSpawners = [ tinyOne, tinyTwo, tinyThree ]
listP3Spawners = [ pupThree, pupFour ]

//------------------------------------------
// Combatant Counts within the WARZONE      
//------------------------------------------

def countTinyTerrors() {
	countTT = tinyOne.spawnsInUse() + tinyTwo.spawnsInUse() + tinyThree.spawnsInUse()
	checkTTNumbers()
}

def countPrediPups() {
	countP3 = pupThree.spawnsInUse() + pupFour.spawnsInUse()
	checkP3Numbers()
}

//------------------------------------------
// Tiny Terror Logic for Respawns           
// (Every 10 seconds, look into respawns)   
//------------------------------------------

numToSpawnTT = 0
timer = 2

def checkTTNumbers() {
	if( playersInWarzone > 0 && countTT < playersInWarzone * 2 && countTT < 10 ) { //keep a consistent pop the same as the player count, and then do the same with P3s
		if( playersInWarzone * 2 > 10 ) { //if there's more than five players in the area, then just spawn 10 minus current count to keep a full house
			numToSpawnTT = 10 - countTT
		} else { //if the result would be under 10, then spawn 
			numToSpawnTT = ( ( playersInWarzone * 2 ) - countTT )
		}
	}
	spawnTTs()
}

TTHateCollector = [] as Set

def spawnTTs() {
	if(numToSpawnTT > 0) {
		randomSpawner = random( listTTSpawners )
//		println "****** Spawning at ${randomSpawner} ******"
		tiny = randomSpawner.forceSpawnNow()
		runOnDeath( tiny, { event -> TTHateCollector.clear(); TTHateCollector.addAll( event.actor.getHated() ); creditTTs( event ) } )
		numToSpawnTT --
		myManager.schedule( timer ) { spawnTTs() }
	} else {
		myManager.schedule(timer) { countTinyTerrors() }
	}
}

def synchronized creditTTs( event ) {
	TTHateCollector.clone().each{ 
		if( isPlayer( it ) && it.isOnQuest( 36, 2 ) && it.getConLevel() < 6.2 ) {
			//if the monster died AND the player is in the warzone, then that means that the monster counts as a kill (because it was in the warzone also)
			if( warzoneList.contains( it ) ) {
				it.addPlayerVar( "Z07WarzoneTTKilled", 1 )
				numTTKilled = it.getPlayerVar( "Z07WarzoneTTKilled" ).intValue()
				if( numTTKilled <= 10 ) {
					it.centerPrint( "${numTTKilled} of 10 Tiny Terrors defeated." )
					if( numTTKilled >= 10 ) {
						if( it.getPlayerVar( "Z07WarzoneTTKilled" ) >= 10 && it.getPlayerVar( "Z07WarzoneP3Killed" ) >= 10 ) {
							it.updateQuest( 36, "Agent Caruthers-VQS" ) //complete step 2 of INTO THE FRAY
							myManager.schedule(3) { it.centerPrint( "You've created enough mayhem. Now return to Agent Caruthers." ) }
						}
					}
				} else {
					it.centerPrint( "You don't need to kill any more Tiny Terrors." )
				}
			} else {
				numTTKilled = it.getPlayerVar( "Z07WarzoneTTKilled" ).intValue()
				if( numTTKilled < 10 ) {
					it.centerPrint( "You defeated a Tiny Terror, but you must defeat it within the regular warzone for it to count." )
				} else {
					it.centerPrint( "You don't need to kill any more Tiny Terrors." )
				}
			}
		} else if( isPlayer( it ) && it.isOnQuest( 36, 2 ) && it.getConLevel() >= 6.2 ) {
			it.centerPrint( "You must change your level to 6.1 or lower to get credit for the 'Into the Fray' task." )
		}
	}
}
		
	
//------------------------------------------
// Predi-Pup Logic for Respawns             
// (Every 10 seconds, look into respawns)   
//------------------------------------------

numToSpawnP3 = 0

def checkP3Numbers() {
	if( playersInWarzone > 0 && countP3 < playersInWarzone * 2 && countP3 < 10 ) { //keep a consistent pop the same as the player count, and then do the same with P3s
		if( playersInWarzone * 2 > 10 ) { //if there's more than five players in the area, then just spawn 10 minus current count to keep a full house
			numToSpawnP3 = 10 - countP3
		} else { //if the result would be under 10, then spawn 
			numToSpawnP3 = ( ( playersInWarzone * 2 ) - countP3 )
		}
	}
	spawnP3s()
}

P3HateCollector = [] as Set

def spawnP3s() {
	if(numToSpawnP3 > 0) {
		randomSpawner = random( listP3Spawners )
//		println "****** Spawning at ${randomSpawner} ******"
		warPup = randomSpawner.forceSpawnNow()
		runOnDeath( warPup, { event -> P3HateCollector.clear(); P3HateCollector.addAll( event.actor.getHated() ); creditP3s() } )
		numToSpawnP3 --
		myManager.schedule( timer + 2 ) { spawnP3s() } //P3s spawn slightly slower than TTs because they have a shorter distance to travel to the warzone.
	} else {
		myManager.schedule(timer) { countPrediPups() }
	}
}

def synchronized creditP3s() {
	P3HateCollector.each{ 
		if( isPlayer( it ) && it.isOnQuest( 36, 2 ) && it.getConLevel() < 6.2 ) {
			//if the monster died AND the player is in the warzone, then that means that the monster counts as a kill (because it was in the warzone also)
			if( warzoneList.contains( it ) ) {
				it.addPlayerVar( "Z07WarzoneP3Killed", 1 )
				numP3Killed = it.getPlayerVar( "Z07WarzoneP3Killed" ).intValue()
				if( numP3Killed <= 10 ) {
					it.centerPrint( "${numP3Killed} of 10 Predator Prairie Pups defeated." )
					if( numP3Killed >= 10 ) {
						if( it.getPlayerVar( "Z07WarzoneTTKilled" ) >= 10 && it.getPlayerVar( "Z07WarzoneP3Killed" ) >= 10 ) {
							it.updateQuest( 36, "Agent Caruthers-VQS" ) //complete step 2 of INTO THE FRAY
							myManager.schedule(3) { it.centerPrint( "You've created enough mayhem. Now return to Agent Caruthers." ) }
						}
					}
				} else {
					it.centerPrint( "You don't need to kill any more Predator Prairie Pups." )
				}
			} else {
				numP3Killed = it.getPlayerVar( "Z07WarzoneP3Killed" ).intValue()
				if( numP3Killed < 10 ) {
					it.centerPrint( "You defeated a Preda-Pup, but you must defeat it within the regular warzone for it to count." )
				} else {
					it.centerPrint( "You don't need to kill any more Predator Prairie Pups." )
				}
			}
		} else if( isPlayer( it ) && it.isOnQuest( 36, 2 ) && it.getConLevel() >= 6.2 ) {
			it.centerPrint( "You must chagne your level to 6.1 or lower to get credit for the 'Into the Fray' task." )
		}
	}
}

//------------------------------------------
// RANDOM SAY BUBBLES during the war        
//------------------------------------------

sayingsTT = [ "yaoc!", "yaotlahueliloc!", "quinyaochihuaz!", "yaomiquico!", "yaotihua!"]
sayingsP3 = [ "Zrrk!", "Kee-tr^k plik!", "Plik!", "Q! Plik! Zrrk!!" ]

def randomSayings() {
	monsterList = []
	myRooms.Aqueduct_303.getActorList().each { if( isMonster( it ) ) { monsterList << it } }
	myRooms.Aqueduct_203.getActorList().each { if( isMonster( it ) ) { monsterList << it } }
	myRooms.Aqueduct_305.getActorList().each { if( isMonster( it ) ) { monsterList << it } }
	if( !monsterList.isEmpty() ) {
		talkingMonster = random( monsterList ) 
		if( talkingMonster.getMonsterType() == "tiny_terror_war" ) {
			talkingMonster.say( random( sayingsTT ) )
		} else if( talkingMonster.getMonsterType() == "p3_war" ) {
			talkingMonster.say( random( sayingsP3 ) )
		}
		myManager.schedule( random( 2, 5 ) ) { randomSayings() }
	} else {
		myManager.schedule( 15 ) { randomSayings() }
	}
}
	
randomSayings()

	
//------------------------------------------
// DISPOSAL SCRIPTS                         
// When a TT or Pup returns back to its     
// origin spot, it is disposed of so that   
// population decreases as the number of    
// players is reduced also.                 
//------------------------------------------

myManager.onTriggerIn(myRooms.Aqueduct_103, ttDisposalZoneOne) { event ->
	if( isMonster(event.actor) && event.actor.getMonsterType() == "tiny_terror_war" ) {
		event.actor.dispose()
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_103, ttDisposalZoneTwo) { event ->
	if( isMonster(event.actor) && event.actor.getMonsterType() == "tiny_terror_war" ) {
		event.actor.dispose()
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_104, ttDisposalZoneThree) { event ->
	if( isMonster(event.actor) && event.actor.getMonsterType() == "tiny_terror_war" ) {
		event.actor.dispose()
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_403, p3BurrowThreeDisposalZone) { event ->
	if( isMonster(event.actor) && event.actor.getMonsterType() == "p3_war" ) {
		event.actor.dispose()
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_305, p3BurrowFourDisposalZone) { event ->
	if( isMonster(event.actor) && event.actor.getMonsterType() == "p3_war" ) {
		event.actor.dispose()
	}
}


//==========================
//ALLIANCES                 
//==========================

tinyOne.allyWithSpawner( tinyTwo )
tinyOne.allyWithSpawner( tinyThree )
tinyTwo.allyWithSpawner( tinyThree )

pupThree.allyWithSpawner( pupFour )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

tinyOne.stopSpawning()
tinyTwo.stopSpawning()
tinyThree.stopSpawning()

pupThree.stopSpawning()
pupFour.stopSpawning()

countTinyTerrors()
countPrediPups()
