//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

peterSpawned = false
peterTalking = false
peterFollowing = false
peterDisposing = false
Peter = null
GuardGeorge = null
georgeSpawned = false
georgeTalking = false
georgeMoving = false
peterBelongsTo = null

spawningGeorge = new Object()
spawningPeter = new Object()
peterFollowSync = new Object()

peterList = []
georgeList = []
playerList602 = []

def despawnGeorgeAndPeter() {
	if( georgeSpawned == true && georgeMoving == false ) {
		GuardGeorge.pauseResume()
		
		GuardGeorge.setPatrol(georgeDespawnMove)
		GuardGeorge.startPatrol()
		
		Peter.setPatrol(peterGoAway)
		Peter.startPatrol()
		
		peterDisposing = true
	}
}

def peterStartFollow(event2) {
	if(peterSpawned == true && peterFollowing == false) {
		Peter.pauseResume()
		Peter.startFollow( event2.player ) 
		peterBelongsTo = event2.player //Store owner of Peter
		peterTalking = false
		peterFollowing = true
		playerList602.clone().each() {
			if(it.hasQuestFlag(GLOBAL, "Z7_Peter_QuestAllowed")) {
				it.unsetQuestFlag(GLOBAL, "Z7_Peter_QuestAllowed")
			}
		}
	}
}

//Various patrols - names should make their purposes apparent
peterGoAway = makeNewPatrol()
peterGoAway.addPatrolPoint(myRooms.Aqueduct_1, 785, 135, 0)
peterGoAway.addPatrolPoint(myRooms.Aqueduct_1, 610, 60, 10)

peterStand = makeNewPatrol()
peterStand.addPatrolPoint(myRooms.Aqueduct_602, 1340, 1050, 0)
peterStand.addPatrolPoint(myRooms.Aqueduct_602, 1260, 920, 10)

peterDispose = makeNewPatrol()
peterDispose.addPatrolPoint(myRooms.Aqueduct_602, 1340, 1050, 0)
peterDispose.addPatrolPoint(myRooms.Aqueduct_602, 1475, 1125, 10)

peterDespawn = makeNewPatrol()
peterDespawn.addPatrolPoint(myRooms.Aqueduct_102, 760, 580, 0)
peterDespawn.addPatrolPoint(myRooms.Aqueduct_102, 454, 448, 10)

georgeStand = makeNewPatrol()
georgeStand.addPatrolPoint(myRooms.Aqueduct_1, 890, 270, 0)
georgeStand.addPatrolPoint(myRooms.Aqueduct_1, 900, 200, 10)

georgeDespawnMove = makeNewPatrol()
georgeDespawnMove.addPatrolPoint(myRooms.Aqueduct_1, 975, 245, 0)
georgeDespawnMove.addPatrolPoint(myRooms.Aqueduct_2, 310, 450, 0)
georgeDespawnMove.addPatrolPoint(myRooms.Aqueduct_102, 525, 50, 0)
georgeDespawnMove.addPatrolPoint(myRooms.Aqueduct_102, 454, 448, 10)

//Trigger zone definitions
peterStandZone = "peterStandZone"
myRooms.Aqueduct_602.createTriggerZone(peterStandZone, 1210, 870, 1310, 970)

peterDisposeZone602 = "peterDisposeZone602"
myRooms.Aqueduct_602.createTriggerZone(peterDisposeZone602, 1400, 1050, 1550, 1200)

peterGoAwayZone = "peterGoAwayZone"
myRooms.Aqueduct_1.createTriggerZone(peterGoAwayZone, 560, 10, 660, 110)

georgeStandZone = "georgeStandZone"
myRooms.Aqueduct_1.createTriggerZone(georgeStandZone, 850, 150, 950, 250)

georgeDisposeZone102 = "georgeDisposeZone102"
myRooms.Aqueduct_102.createTriggerZone(georgeDisposeZone102, 404, 398, 504, 498)

//Trigger zone logic
myManager.onTriggerIn(myRooms.Aqueduct_602, peterStandZone) { event -> //When Peter gets in his stand location
	//println " #### ${event.actor} entered peterStandZone ####"
	if(event.actor == Peter && peterFollowing == false) {
		Peter?.pause() //Lock movement
		myManager.schedule(300) { //Wait 120 seconds and unlock movement
			if(Peter?.getRoomName( ) == "Aqueduct_602") {
				Peter?.pauseResume()
				Peter?.setPatrol(peterDispose)
				Peter?.startPatrol()
				peterDisposing = true
				peterFollowing = false
			}
		}
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_602, peterDisposeZone602) { event -> //When Peter enters dispose zone, dispose him and reset variables to false
	//println "#### ${event.actor} entered peterDisposeZone602 ####"
	if(event.actor == Peter && peterFollowing == false && peterDisposing == true) {
		Peter?.dispose()
		Peter = null
		peterSpawned = false
		peterTalking = false
		peterFollowing = false
		peterDisposing = false
		peterBelongsTo = null
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_1, georgeStandZone) { event -> //When George enters stand zone
	if(event.actor == GuardGeorge) {
		GuardGeorge?.pause() //Lock movement
		GuardGeorge?.setRotation(180)
		myManager.schedule(300) { despawnGeorgeAndPeter() }
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_1, peterGoAwayZone) { event -> //Peter enters go away zone and is set to disposing
	if(event.actor == Peter && peterDisposing == true) {
		Peter?.dispose()
		Peter = null
		peterSpawned = false
		peterTalking = false
		peterFollowing = false
		peterDisposing = false
		peterBelongsTo = null
	}
}

myManager.onTriggerIn(myRooms.Aqueduct_102, georgeDisposeZone102) { event -> //When George enters dispose zone, wait 5 seconds for Peter to catch up and dispose them both
	if(event.actor == GuardGeorge) {
		GuardGeorge?.pause()
		myManager.schedule(5) {
			GuardGeorge?.dispose()
			georgeSpawned = false
			georgeTalking = false
			georgeMoving = false
			
			Peter?.dispose()
			Peter = null
			peterSpawned = false
			peterTalking = false
			peterFollowing = false
			peterDisposing = false
			peterBelongsTo = null
		}
	}
	if(event.actor == Peter) { //When Peter enters, get rid o' his ass (and the rest o' him)
		Peter?.dispose()
		Peter = null
		peterSpawned = false
		peterTalking = false
		peterFollowing = false
		peterDisposing = false
		peterBelongsTo = null
	}
}

myManager.onEnter(myRooms.Aqueduct_202) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z7_Peter_OutClearing")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z7_Peter_OutClearing")
	}
	if(event.actor == Peter) {
		myManager.schedule(3) {
			Peter.say("This is as far as I go.")
		
			Peter.setPatrol(peterDespawn)
			Peter.startPatrol()
			
			GuardGeorge.setPatrol(georgeDespawnMove)
			GuardGeorge.startPatrol()
		}
	}
}

myManager.onExit(myRooms.Aqueduct_602) { event ->
	if(isPlayer(event.actor)) {
		playerList602.remove(event.actor)
	}
}

myManager.onEnter( myRooms.Aqueduct_602 ) { event ->
	synchronized(spawningPeter) {
		if( isPlayer( event.actor ) ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}

		if(isPlayer(event.actor)) { playerList602 << event.actor }
		if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z7_Peter_QuestAllowed")) {
			event.actor.setQuestFlag(GLOBAL, "Z7_Peter_QuestAllowed")
		}
		if(isPlayer(event.actor) && isPlayer(event.actor) && event.actor.isOnQuest(270, 2) && event.actor.hasQuestFlag(GLOBAL, "Z7_Peter_OutClearing")) {
			event.actor.unsetQuestFlag(GLOBAL, "Z7_Peter_OutClearing") //Remove out of clearing flag if player comes back into clearing
			if(peterBelongsTo == event.actor) {
				Peter?.warp("Aqueduct_602", 1475, 1240) //if the current Peter belongs to event.actor, warp Peter with actor
			}
		}
		if(isPlayer(event.actor) && event.actor.isOnQuest(270, 2) && event.actor.hasQuestFlag(GLOBAL, "Z7_Peter_Active")) {
			event.actor.unsetQuestFlag(GLOBAL, "Z7_Peter_Active") //"Just in case" cleanup
		}
		if(isPlayer(event.actor) && event.actor.isOnQuest(270, 2) && !event.actor.hasQuestFlag(GLOBAL, "Z7_Peter_Active") && peterSpawned == false ) { //Spawn Peter if no Peter already exists and player has quest
			Peter = spawnNPC("Whittlin Smith-VQS", myRooms.Aqueduct_602, 1475, 1240)
			Peter.setDisplayName( "Peter" )
		
			peterSpawned = true
		
			Peter.setPatrol(peterStand)
			Peter.startPatrol()
			
			//----------------------------------------------------------------------------
			//Peter's Busy                                                                
			//----------------------------------------------------------------------------
			def peterBusy = Peter.createConversation("peterBusy", true, "QuestStarted_270:2", "!Z7_Peter_Active", "!Z7_Peter_Break", "!Z7_Peter_OutClearing", "!Z7_Peter_QuestAllowed")
			
			def peterBusy1 = [id:1]
			peterBusy1.npctext = "Sorry, I'm a little busy talking to someone else right now. Why don't you try me again in a few minutes?"
			peterBusy1.playertext = "Oh, Peter, pull yourself together and I'll talk to you later."
			peterBusy1.result = DONE
			peterBusy.addDialog(peterBusy1, Peter)

			//----------------------------------------------------------------------------
			//Peter Make Follow                                                           
			//----------------------------------------------------------------------------
			def peterMakeFollow = Peter.createConversation("peterMakeFollow", true, "QuestStarted_270:2", "!Z7_Peter_Active", "!Z7_Peter_Break", "!Z7_Peter_OutClearing", "Z7_Peter_QuestAllowed")
		
			def peterMakeFollow1 = [id:1]
			peterMakeFollow1.npctext = "The Zurg will be our savior! They will come for us and take us into the heavens!"
			peterMakeFollow1.options = []
			peterMakeFollow1.options << [text:"Err... yep.", exec: { peterTalking = true }, result: 2]
			peterMakeFollow1.options << [text:"What? Do you have any idea how crazy you sound?", exec: { peterTalking = true },  result: 8]
			peterMakeFollow1.options << [text:"What are you doing out here, Peter? You took an oath to the Barton Regulars! Duty demands you return to Barton Town.", exec: { peterTalking = true}, result: 9]
			peterMakeFollow.addDialog(peterMakeFollow1, Peter)
		
			def peterMakeFollow2 = [id:2]
			peterMakeFollow2.npctext = "Have you come to join with us in awaiting the return of our extraterrestrial saviors?"
			peterMakeFollow2.options = []
			peterMakeFollow2.options << [text:"Not exactly.", result: 3]
			peterMakeFollow2.options << [text:"I don't know. I really don't have enough information at this point.", result: 4]
			peterMakeFollow.addDialog(peterMakeFollow2, Peter)
		
			def peterMakeFollow3 = [id:3]
			peterMakeFollow3.npctext = "What would bring you to this clearing, if not for the embrace the Zurg offer to True Believers?"
			peterMakeFollow3.options = []
			peterMakeFollow3.options << [text:"To be honest, I'm just lost. Do you think you could show me the way out of here?", result: 6]
			peterMakeFollow3.options << [text:"I just wanted to see the crazies for myself.", result: 8]
			peterMakeFollow3.options << [text:"You have abandoned your post with the Barton Regulars! Your brother has sent me to return you to your rightful play, in Barton Town.", result: 9]
			peterMakeFollow.addDialog(peterMakeFollow3, Peter)
		
			def peterMakeFollow4 = [id:4]
			peterMakeFollow4.npctext = "The Zurg will return, bringing SALVATION to the True Believers. What else is there to know?"
			peterMakeFollow4.options = []
			peterMakeFollow4.options << [text:"How about when they're returning?", result: 5]
			peterMakeFollow4.options << [text:"Salvation through insanity. An interesting concept.", result: 8]
			peterMakeFollow4.options << [text:"What about duty, Peter. Your oath to the Barton Regulars cannot be discarded, and beckons you to Barton Town.", result: 9]
			peterMakeFollow.addDialog(peterMakeFollow4, Peter)
		
			def peterMakeFollow5 = [id:5]
			peterMakeFollow5.npctext = "They will return when we are prepared to accept then and worthy of ascension."
			peterMakeFollow5.options = []
			peterMakeFollow5.options << [text:"Hmm, sounds like it might be a while. Think you could help me bring my things up this hill for the wait?", result: 7]
			peterMakeFollow5.options << [text:"So you're just going to sit around and wait for an indefinite period of time? You've gone mad!", result: 8]
			peterMakeFollow5.options << [text:"Peter, you can wait just as easily in Barton Town. You must return and live up to your oath.", result: 9]
			peterMakeFollow.addDialog(peterMakeFollow5, Peter)
		
			def peterMakeFollow6 = [id:6]
			peterMakeFollow6.npctext = "Strange. I can accompany you as far as the bottom of the hill, but no farther. I cannot stray too far from the other Believers."
			peterMakeFollow6.flag = "Z7_Peter_Active"
			peterMakeFollow6.exec = { event2 -> //Resume movement and follow player
				synchronized(peterFollowSync) { peterStartFollow(event2) }
			}
			peterMakeFollow6.result = DONE
			peterMakeFollow.addDialog(peterMakeFollow6, Peter)
		
			def peterMakeFollow7 = [id:7]
			peterMakeFollow7.npctext = "I'm not supposed to leave the clearing... though, I suppose nobody would begrudge my leaving for five minutes to help another follower. Very well, lead on."
			peterMakeFollow7.flag = "Z7_Peter_Active"
			peterMakeFollow7.exec = { event2 -> //Resume movement and follow player
				synchronized(peterFollowSync) { peterStartFollow(event2) }
			}
			peterMakeFollow7.result = DONE
			peterMakeFollow.addDialog(peterMakeFollow7, Peter)
		
			def peterMakeFollow8 = [id:8]
			peterMakeFollow8.npctext = "It is a sign of a weak and faithless mind to dismiss as insanity that which one cannot comprehend. I believe this conversation is over, friend."
			peterMakeFollow8.flag = "Z7_Peter_Break"
			peterMakeFollow8.exec = { peterTalking = false }
			peterMakeFollow8.result = DONE
			peterMakeFollow.addDialog(peterMakeFollow8, Peter)
		
			def peterMakeFollow9 = [id:9]
			peterMakeFollow9.npctext = "You tell my brother to mind his own business, and get out of here before I tell the rest of the Believer's we have an interloper."
			peterMakeFollow9.flag = "Z7_Peter_Break"
			peterMakeFollow9.exec = { peterTalking = false }
			peterMakeFollow9.result = DONE
			peterMakeFollow.addDialog(peterMakeFollow9, Peter)
		
			//----------------------------------------------------------------------------
			//Peter Break                                                                 
			//----------------------------------------------------------------------------
			def peterBreak = Peter.createConversation("peterBreak", true, "QuestStarted_270:2", "Z7_Peter_Break")
		
			def peterBreak1 = [id:1]
			peterBreak1.npctext = "I thought I told you to get outta here."
			peterBreak1.options = []
			peterBreak1.options << [text:"I know... problem is that I'm lost. Think you could show me the way?", result: 2]
			peterBreak1.options << [text:"I've been thinking, I was too harsh. This whole salvation thing is starting to make a lot of sense. Do you think you could help me carry my things up the hill so I could join you in waiting for the return of the Zurg?", result: 3]
			peterBreak1.options << [text:"Sorry, I'll be on my way.", result: DONE]
			peterBreak.addDialog(peterBreak1, Peter)
		
			def peterBreak2 = [id:2]
			peterBreak2.npctext = "Lost? Very well, I can escort you as far as the bottom of the hill but you're on your own beyond that."
			peterBreak2.flag = ["!Z7_Peter_Break", "Z7_Peter_Active"]
			peterBreak2.exec = { event2 -> //Resume movement and follow player
				synchronized(peterFollowSync) { peterStartFollow(event2) }
			}
			peterBreak2.result = DONE
			peterBreak.addDialog(peterBreak2, Peter)
		
			def peterBreak3 = [id:3]
			peterBreak3.npctext = "I'm not supposed to leave the clearing... though, I suppose nobody would begrudge my leaving for five minutes to help another follower. Very well, lead on."
			peterBreak3.flag = ["!Z7_Peter_Break", "Z7_Peter_Active"]
			peterBreak3.exec = { event2 -> //Resume movement and follow player
				synchronized(peterFollowSync) { peterStartFollow(event2) }
			}
			peterBreak3.result = DONE
			peterBreak.addDialog(peterBreak3, Peter)
		}
	}	
}

//Event to spawn Peter's escort to Barton Town
myManager.onEnter(myRooms.Aqueduct_1) { event -> //If George is spawned and player has control of Peter, set outclearing flag (for george convo) and warp peter
	synchronized(spawningGeorge){
		if(isPlayer(event.actor) && event.actor.isOnQuest(270, 2) && event.actor.hasQuestFlag(GLOBAL, "Z7_Peter_Active") && georgeSpawned == true && peterBelongsTo == event.actor) {
			event.actor.unsetQuestFlag(GLOBAL, "Z7_Peter_Active")
			if(Peter != null) { 
				event.actor.setQuestFlag(GLOBAL, "Z7_Peter_OutClearing") 
				Peter.warp("Aqueduct_1", 600, 50)
			}
		} //If George isn't spawned and player has control of Peter, spawn george, set outclearing flag (for george convo), and warp peter
		if(isPlayer(event.actor) && event.actor.isOnQuest(270, 2) && event.actor.hasQuestFlag(GLOBAL, "Z7_Peter_Active") && georgeSpawned == false && peterBelongsTo == event.actor) {
			event.actor.unsetQuestFlag(GLOBAL, "Z7_Peter_Active")
			if(Peter != null) { 
				event.actor.setQuestFlag(GLOBAL, "Z7_Peter_OutClearing") 
				Peter.warp("Aqueduct_1", 600, 50)
			}
		
			GuardGeorge = spawnNPC("BFG-George", myRooms.Aqueduct_1, 881, 281)
			GuardGeorge.setDisplayName("George")
		
			georgeSpawned = true
		
			GuardGeorge.setPatrol(georgeStand)
			GuardGeorge.startPatrol()
		
			//----------------------------------------------------------------------------
			//Peter Escort Home                                                           
			//----------------------------------------------------------------------------
			def peterEscortHome = GuardGeorge.createConversation("peterEscortHome", true, "QuestStarted_270:2", "Z7_Peter_OutClearing")
		
			def peterEscortHome1 = [id:1]
			peterEscortHome1.npctext = "Aha! You got Peter to come down!"
			peterEscortHome1.playertext = "Yes. I trust you can manage him back to Barton Town?"
			peterEscortHome1.exec = { georgeTalking = true }
			peterEscortHome1.result = 2
			peterEscortHome.addDialog(peterEscortHome1, GuardGeorge)
		
			def peterEscortHome2 = [id:2]
			peterEscortHome2.npctext = "What?! You tricked me!! Curse you, %p."
			peterEscortHome2.result = 3
			peterEscortHome.addDialog(peterEscortHome2, Peter)
		
			def peterEscortHome3 = [id:3]
			peterEscortHome3.npctext = "You leave %p alone, Peter. It's long passed time you returned to your duties."
			peterEscortHome3.result = 4
			peterEscortHome.addDialog(peterEscortHome3, GuardGeorge)
		
			def peterEscortHome4 = [id:4]
			peterEscortHome4.npctext = "But... the Zurg are coming!"
			peterEscortHome4.result = 5
			peterEscortHome.addDialog(peterEscortHome4, Peter)
		
			def peterEscortHome5 = [id:5]
			peterEscortHome5.npctext = "Don't start with that, I'm not interested in that fantastical nonsense. Just follow me back to Barton Town and don't try to fight or run, you'll just make it hard on yourself."
			peterEscortHome5.result = 6
			peterEscortHome.addDialog(peterEscortHome5, GuardGeorge)
		
			def peterEscortHome6 = [id:6]
			peterEscortHome6.npctext = "Don't worry, George, I still remember all the beatings you gave me in training. That's probably why my brother sent you."
			peterEscortHome6.result = 7
			peterEscortHome.addDialog(peterEscortHome6, Peter)
		
			def peterEscortHome7 = [id:7]
			peterEscortHome7.npctext = "Probably. %p, thanks for your help. I can handle getting Peter back to Barton Town, but could you let Jerry know of our success? I'd appreciate it."
			peterEscortHome7.playertext = "Sure thing, George. I'll head right over to him."
			peterEscortHome7.result = 8
			peterEscortHome.addDialog(peterEscortHome7, GuardGeorge)
		
			def peterEscortHome8 = [id:8]
			peterEscortHome8.npctext = "Great... well, take care. Let's get moving, Peter."
			peterEscortHome8.quest = 270
			peterEscortHome8.exec = { event2 ->
				if(georgeSpawned == true) {
					georgeTalking = false
					georgeMoving = true
					event2.player.addMiniMapQuestActorName("BFG-Jerry")
					Peter.startFollow(GuardGeorge)
			
					GuardGeorge.setPatrol(georgeDespawnMove)
					GuardGeorge.startPatrol()
				}
			}
			peterEscortHome8.result = DONE
			peterEscortHome.addDialog(peterEscortHome8, GuardGeorge)
		}
	}
}