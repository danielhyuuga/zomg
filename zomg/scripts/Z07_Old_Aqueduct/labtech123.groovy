import com.gaiaonline.mmo.battle.script.*;

myManager.onEnter( myRooms.Aqueduct_102 ) { event ->
	if( isPlayer(event.actor) ) {
		if( isPlayer( event.actor ) ) {
			if( event.actor.getConLevel() >= 6.2 ) {
				event.actor.setQuestFlag( GLOBAL, "Z07TooOldForAqueductQuests" )
			} else {
				event.actor.unsetQuestFlag( GLOBAL, "Z07TooOldForAqueductQuests" )
			}
		}
		event.actor.unsetQuestFlag( GLOBAL, "Z07RingRewardInProgress" )
	}
}

Labtech123 = spawnNPC("123-VQS", myRooms.Aqueduct_102, 815, 215)
Labtech123.setRotation( 135 )
Labtech123.setDisplayName( "G-Corp Labtech" )

onQuestStep( 118, 2 ) { event -> event.player.addMiniMapQuestActorName( "123-VQS" ) }

//---------------------------------------------------------
// LABTECH 123, the Observer                               
//---------------------------------------------------------

def firstGreet = Labtech123.createConversation( "firstGreet", true, "!Z07FirstGreetDone" )

first1 = [id:1]
first1.playertext = "Hello there!"
first1.result = 4
firstGreet.addDialog( first1, Labtech123 )

/*
first2 = [id:2]
first2.npctext = "Err...or, rather...hello! That's what I meant to say!"
first2.playertext = "Hmmm...you *do* look sort of familiar..."
first2.result = 3
firstGreet.addDialog( first2, Labtech123 )

first3 = [id:3]
first3.npctext = "Nope. Just a slip of the tongue. I'm pretty sure we've never met before."
first3.playertext = "Okaaay...but I could swear..."
first3.result = 4
firstGreet.addDialog( first3, Labtech123 )
*/

first4 = [id:4]
first4.npctext = "Listen, I'm pretty busy right now. We'll need to exchange pleasantries some other time."
first4.options = []
first4.options << [text: "Wait! What's a scientist-type like you doing out here?", result: 5]
first4.options << [text: "Okay, okay! Don't be so touchy! Sheesh!", result: DONE]
firstGreet.addDialog( first4, Labtech123 )

first5 = [id:5]
first5.npctext = "I'm not a 'scientist-type'! I'm a scientist! And if you must know, I'm here observing the aliens that came through the Star Portal."
first5.playertext = "Yeah, they're really something, aren't they?"
first5.result = 6
firstGreet.addDialog( first5, Labtech123 )

first6 = [id:6]
first6.npctext = "A star-faring race with an incredibly high technology level...in our own backyard...and the best way you can describe them is 'really something'?"
first6.playertext = "Well...they are!"
first6.result = 7
firstGreet.addDialog( first6, Labtech123 )

first7 = [id:7]
first7.npctext = "Look. They clearly have stealth technology; Their personal armament is extremely miniaturized and compact; They display an almost instinctive mastery of nano-technology; AND they are *obviously* highly intelligent!"
first7.result = 8
firstGreet.addDialog( first7, Labtech123 )

first8 = [id:8]
first8.npctext = "And that says *nothing* about the fact that since coming to Gaia, these aliens have clearly adapted their laser weaponry to affect the Animated...something that *our* scientists have not yet figured out!"
first8.playertext = "ummm..."
first8.result = 9
firstGreet.addDialog( first8, Labtech123 )

first9 = [id:9]
first9.npctext = "So yes! I'm QUITE interested in these 'really somethings' you describe. It may even be true that the understanding of their race and their technology may be a deciding element in the fate of our very *world*!"
first9.playertext = "Wow. You really lay it on thick, don't you?"
first9.result = 10
firstGreet.addDialog( first9, Labtech123 )

first10 = [id:10]
first10.npctext = "Perhaps, perhaps. You wouldn't be the first one to tell me that."
first10.exec = { event ->
	if( !event.player.hasQuestFlag( GLOBAL, "Z07TooOldForAqueductQuests" ) ) {
		event.player.setQuestFlag( GLOBAL, "Z07SalvageIntroOkay" )
		Labtech123.pushDialog( event.player, "salvageIntro" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z07GoodbyeOkay" )
		Labtech123.pushDialog( event.player, "goodbye" )
	}
}
firstGreet.addDialog( first10, Labtech123 )

//Goodbye conversation
goodbye = Labtech123.createConversation( "goodbye", true, "Z07GoodbyeOkay", "Z07TooOldForAqueductQuests" )

def bye1 = [id:1]
bye1.npctext = "Listen...I don't have any tasks tough enough for you. So please let me get back to my work."
bye1.playertext = "Oh. So sorry for taking up your time. I'm sure you're horribly busy standing here watching nothing happen. See ya."
bye1.flag = "!Z07GoodbyeOkay"
bye1.exec = { event ->
	event.player.centerPrint( "To access the Labtech's task, you need to change your level to 6.1 or lower." )
	myManager.schedule(3) { event.player.centerPrint( "Use the 'CHANGE LEVEL' option in the MENU to lower your level." ) }
}
bye1.result = DONE
goodbye.addDialog( bye1, Labtech123 )

//---------------------------------------------------------
// ALIEN SALVAGE (intro)                                   
//---------------------------------------------------------

salvageIntro = Labtech123.createConversation( "salvageIntro", true, "Z07SalvageIntroOkay", "!Z07SalvageIntroDone", "!Z07TooOldForAqueductQuests" )

def salv1 = [id:1]
salv1.playertext = "So, have you figured out what to do about the Preda-pups yet?"
salv1.result = 2
salvageIntro.addDialog( salv1, Labtech123 )

def salv2 = [id:2]
salv2.npctext = "No. The pesky things are easy enough to defeat, but they tend to self-detonate with a deplorable regularity...which has discouraged me rather effectively from pursuing any collection of their technology."
salv2.playertext = "Is that the only problem? With all the rings you guys have been passing out, I would have thought you could have some sort of crack assault team here to take out these aliens."
salv2.result = 3
salvageIntro.addDialog( salv2, Labtech123 )

def salv3 = [id:3]
salv3.npctext = "Well, it turns out the supply of a commodity does not automatically bestow the skills...or will...to master that commodity's use."
salv3.playertext = "Ummm...what?"
salv3.result = 4
salvageIntro.addDialog( salv3, Labtech123 )

def salv4 = [id:4]
salv4.npctext = "We had a sparsity of volunteers."
salv4.playertext = "Ah. So *that's* why you guys are passing these things out. You're too chicken to use them yourselves!"
salv4.result = 5
salvageIntro.addDialog( salv4, Labtech123 )

def salv5 = [id:5]
salv5.npctext = "Chicken! You accuse US of cowardice! You have no idea the powers that we're dealing with in our laboratories. We're on the cusp of something that will change the world forever! You will one day look back and realize that you spoke with GREATNESS here on this hill!"
salv5.options = []
salv5.options << [text: "Hmmm...perhaps we should talk about something else.", result: 7 ]
salv5.options << [text: "I think I'll just walk away now and avoid all that running. See ya.", result: 6]
salvageIntro.addDialog( salv5, Labtech123 )

def salv6 = [id:6]
salv6.npctext = "That's right! Walk away!"
salv6.result = DONE
salvageIntro.addDialog( salv6, Labtech123 )

def salv7 = [id:7]
salv7.npctext = "Well...perhaps. I suppose I was getting a bit carried away."
salv7.playertext = "So...about those Preda-Pups?"
salv7.flag = [ "Z07SalvageStartOkay", "Z07SalvageIntroDone" ]
salv7.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z07SalvageStartOkay" )
	Labtech123.pushDialog( event.player, "salvageStart" )
}
salv6.result = DONE
salvageIntro.addDialog( salv7, Labtech123 )

//---------------------------------------------------------
// ALIEN SALVAGE (start)                                   
//---------------------------------------------------------

salvageStart = Labtech123.createConversation( "salvageStart", true, "Z07SalvageStartOkay", "!QuestStarted_118", "!Z07TooOldForAqueductQuests" )

def start1 = [id:1]
start1.npctext = "They're a menace! There's so many of them, and their technology is simply incredible!"
start1.playertext = "Can't we open up some sort of diplomatic talks or something?"
start1.result = 2
salvageStart.addDialog( start1, Labtech123 )

def start2 = [id:2]
start2.npctext = "No. Or, at least, not yet anyway. Everytime we approach them, they send some electronic inquiry at us and we can't decipher it. Apparently, they're asking for some response...and when we don't give it, they attack!"
start2.playertext = "Bummer."
start2.result = 3
salvageStart.addDialog( start2, Labtech123 )

def start3 = [id:3]
start3.npctext = "Yes. Quite. I'm currently working under the theory that if we gather enough samples of their technology, we'll be able to catapult our own tech forward so we can decipher their query and then go into interrogative mode with them at once."
start3.playertext = "Ummm..."
start3.result = 4
salvageStart.addDialog( start3, Labtech123 )

def start4 = [id:4]
start4.npctext = "*sigh* Talk to them, I mean. If our tech is advanced enough, I think they'll recognize us as 'civilized' and speak with us. Mind you, that's just a theory."
start4.playertext = "And this is where we got into trouble last time, so let me cut to the chase. You need someone to gather that tech for you, right?"
start4.result = 5
salvageStart.addDialog( start4, Labtech123 )

def start5 = [id:5]
start5.npctext = "Yes. Quite. Would you be so kind?"
start5.options = []
start5.options << [text: "Sure. What sorts of tech do you need?", result: 7 ]
start5.options << [text: "Nah. I'm going to bow out on this one. See ya.", result: 6 ]
salvageStart.addDialog( start5, Labtech123 )

def start6 = [id:6]
start6.npctext = "Good day to you. But remember, this menace will not fade away. Nor will any of the others you haven't faced yet."
start6.result = DONE
salvageStart.addDialog( start6, Labtech123 )

def start7 = [id:7]
start7.npctext = "We need the base elements of their equipment...their biochips. Those chips seem to be used as building blocks to assemble the other parts of their equipment, with the order of chip placement determining the ultimate end-product. Almost a DNA-like setup, but for machinery."
start7.playertext = "Whoa."
start7.result = 8
salvageStart.addDialog( start7, Labtech123 )

def start8 = [id:8]
start8.npctext = "Indeed. So not only do we need the chips themselves, but we need to find out how large their instruction set is for commands...and depending on how complex that set is, I fear that we'll need MANY of the chips to make anything useful."
start8.playertext = "Well, I don't want to make a career out of this. Give me a short-term goal. How many do you need right now?"
start8.result = 9
salvageStart.addDialog( start8, Labtech123 )

def start9 = [id:9]
start9.npctext = "Well, in that case...five. Get me five biochips, damaged or not, from the P3s here in the Old Aqueduct. Bring them back to me and I'll make it worth your effort."
start9.options = []
start9.options << [ text: "You've got yourself a deal. I'm heading to the burrows now!", quest: 118, result: DONE ]
start9.options << [ text: "Not right now. Maybe I'll come back later. See ya.", result: 6 ]
salvageStart.addDialog( start9, Labtech123 )

//---------------------------------------------------------
// ALIEN SALVAGE (interim)                                 
//---------------------------------------------------------

salvageInterim = Labtech123.createConversation( "salvageInterim", true, "QuestStarted_118:2", "!Z07TooOldForAqueductQuests" )

def int1 = [id:1]
int1.npctext = "Have you returned with the biochips, %p?"
int1.playertext = "No, not yet."
int1.result = 2
salvageInterim.addDialog( int1, Labtech123 )

def int2 = [id:2]
int2.playertext = "Hey! Wait a minute! I never told you my name!"
int2.result = 3
salvageInterim.addDialog( int2, Labtech123 )

def int3 = [id:3]
int3.npctext = "Oh. Hmmm. Well, of course you must have! How else would I know? Come back when you have the biochips. I'm a busy man. Good day."
int3.playertext = "But..."
int3.result = 4
salvageInterim.addDialog( int3, Labtech123 )

def int4 = [id:4]
int4.npctext = "I said, 'Good Day'!"
int4.result = DONE
salvageInterim.addDialog( int4, Labtech123 )


//---------------------------------------------------------
// ALIEN SALVAGE (success)                                 
//---------------------------------------------------------

salvageSuccess = Labtech123.createConversation( "salvageSuccess", true, "QuestStarted_118:3", "!Z07TooOldForAqueductQuests" )
salvageSuccess.setUrgent( true )

def succ1 = [id:1]
succ1.npctext = "Have you achieved success at last?"
succ1.playertext = "Yup. All five biochips, only slightly damaged."
succ1.result = 2
salvageSuccess.addDialog( succ1, Labtech123 )

def succ2 = [id:2]
succ2.npctext = "Very good. Hand them over!"
succ2.exec = { event ->
	runOnDeduct( event.player, 100365, 5, chipsPresent, chipsNotPresent ) // Removes five damaged biochiops from player's inventory
}
succ2.result = DONE
salvageSuccess.addDialog( succ2, Labtech123 )


//The player DOES have the Damaged Biochips
def happyResponse = Labtech123.createConversation( "happyResponse", true, "QuestStarted_118:3", "Z05BiochipsPresent" )

def happy1 = [id:1]
happy1.npctext = "Excellent! These are exactly what we need...and we'll need even more of them if you have the time. My offer is open 24/7."
happy1.playertext = "We'll see. Maybe later. For now though...you mentioned a reward?"
happy1.result = 2
happyResponse.addDialog( happy1, Labtech123 ) 

def happy2 = [id:2]
happy2.npctext = "I certainly did. I've gathered all kinds of curious things. Let me see..."
happy2.exec = { event ->
	giveReward( event )
}
happy2.flag = "!Z05BiochipsPresent"
happy2.quest = 118 // Send the completion update for ALIEN SALVAGE quest
happy2.result = DONE
happyResponse.addDialog(happy2, Labtech123)

//The player DOES NOT have the Damaged Biochips
def sadResponse = Labtech123.createConversation( "sadResponse", true, "QuestStarted_118:3", "Z05BiochipsNotPresent" )

def sad1 = [id:1]
sad1.npctext = "Well? I'm waiting..."
sad1.playertext = "But...I had them here just a minute ago!"
sad1.result = 2
sadResponse.addDialog( sad1, Labtech123 )

def sad2 = [id:2]
sad2.npctext = "Yes, I'm certain. How very clever of you. Well, return to me when you have five biochips, as requested, and we shall speak further."
sad2.playertext = "All right. I'll be right back."
sad2.result = 3
sadResponse.addDialog( sad2, Labtech123 )

def sad3 = [id:3]
sad3.npctext = "Just get them!"
sad3.flag = "!Z05BiochipsNotPresent"
sad3.result = DONE
sadResponse.addDialog( sad3, Labtech123 )

//Closures to check to ensure that the player actually still has five pink flamingo feathers at the time of the transaction
chipsPresent = { event ->
	event.player.setQuestFlag( GLOBAL, "Z05BiochipsPresent" )
	event.player.removeMiniMapQuestActorName( "123-VQS" )
	Labtech123.pushDialog( event.player, "happyResponse" )
}

chipsNotPresent = { event ->
	event.player.setQuestFlag( GLOBAL, "Z05BiochipsNotPresent" )
	Labtech123.pushDialog( event.player, "sadResponse" )
}

//The reward routine

recipeList = [ "17785", "17793", "17788", "17787", "17794" ]
itemList = [ "17637", "17640", "17639", "17950", "17952" ]
powerupList = [ "18104", "18030", "18106" ]

def giveReward( event ) {
	event.player.centerPrint( "The Labtech reaches into his pockets and pulls out..." )
	myManager.schedule(3) { 
		roll = random( 100 )
		if( roll <= 25 ) {
			//give great gold
			goldGrant = random( 300, 600 )
			event.player.centerPrint( "...${ goldGrant } gold coins!" )
			event.player.grantCoins( goldGrant )
			
		} else if( roll > 25 && roll <= 60 ) {
			//give (less) gold and orbs
			goldGrant = random( 100, 200 )
			orbGrant = 4
			event.player.centerPrint( "...${ goldGrant } gold coins, plus ${ orbGrant } Charge Orbs!" )
			event.player.grantCoins( goldGrant )
			event.player.grantQuantityItem( 100257, orbGrant )
		} else if( roll > 60 && roll <= 95 ) {
			//give gold and orbs and a recipe
			goldGrant = random( 100, 200 )
			orbGrant = 4
			event.player.centerPrint( "...${ goldGrant } gold coins, plus ${ orbGrant } Charge Orbs, AND a recipe!" )
			event.player.grantCoins( goldGrant )
			event.player.grantQuantityItem( 100257, orbGrant )
			event.player.grantItem( random( recipeList ) )
		} else if( roll > 95 ) {
			//let the user select a ring
			event.player.centerPrint( "...nothing!" )
			myManager.schedule(3) { 
				event.player.centerPrint( "He's letting you choose a NEW RING instead!" )
				event.player.setQuestFlag( GLOBAL, "Z07RingGrantAllowed" )
				myManager.schedule(2) { Labtech123.pushDialog( event.player, "ringGrant" ) }
			}
		}
	}
}


//--------------------------------
// Ring Grant Dialog              
//--------------------------------
def ringGrant = Labtech123.createConversation( "ringGrant", true, "Z07RingGrantAllowed", "!Z07RingRewardInProgress" )

def grant1 = [id:1]
grant1.npctext = "I have every ring we've discovered to-date. Which would you like as your reward?"
grant1.exec = { event ->
	player = event.player
	event.player.setQuestFlag( GLOBAL, "Z07RingRewardInProgress" )
	makeMainMenu( player )
}
grant1.result = DONE
ringGrant.addDialog( grant1, Labtech123 )

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 3
CLdecimal = 0

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Debuffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Debuffs" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z07RingRewardInProgress" )
		}
	}
}

//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Dervish", "Hack", "Mantis", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Dervish" ) {
			makeDervishMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Mantis" ) {
			makeMantisMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Heavy Water Balloon", "Hornet Nest", "Hot Foot", "Hunter's Bow", "Shark Attack", "Shuriken", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Heavy Water Balloon" ) {
			makeHeavyWaterBalloonMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Hunter's Bow" ) {
			makeHuntersBowMenu( player )
		}
		if( event.selection == "Shark Attack" ) {
			makeSharkAttackMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Duct Tape", "Gumshoe", "Quicksand", "Scaredy Cat", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Duct Tape" ) {
			makeDuctTapeMenu( player )
		}
		if( event.selection == "Gumshoe" ) {
			makeGumshoeMenu( player )
		}
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Improbability Sphere", "Pot Lid", "Rock Armor", "Teflon Spray", "Turtle", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Improbability Sphere" ) {
			makeImprobabilitySphereMenu( player )
		}
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Rock Armor" ) {
			makeRockArmorMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Divinity", "Healing Halo", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Divinity" ) {
			makeDivinityMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Fleet Feet", "Ghost", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Fleet Feet" ) {
			makeFleetFeetMenu( player )
		}
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Adrenaline" ) {
			makeAdrenalineMenu( player )
		}
		if( event.selection == "Knife Sharpen" ) {
			makeKnifeSharpenMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.unsetQuestFlag( GLOBAL, "Z07RingGrantAllowed" )
}


	
