import com.gaiaonline.mmo.battle.script.*;

//---------------------------------------------------------
// TRIGGER ZONES FOR QUEST 98 (SCANNING THE BURROWS)       
//---------------------------------------------------------


def BurrowOne = "BurrowOne"
myRooms.Aqueduct_206.createTriggerZone( BurrowOne, 135, 365, 410, 535 )

myManager.onTriggerIn(myRooms.Aqueduct_206, BurrowOne) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow206" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow206" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}
		
def BurrowTwo = "BurrowTwo"
myRooms.Aqueduct_406.createTriggerZone( BurrowTwo, 345, 110, 610, 300 )

myManager.onTriggerIn(myRooms.Aqueduct_406, BurrowTwo) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow406" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow406" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}

def BurrowThree = "BurrowThree"
myRooms.Aqueduct_403.createTriggerZone( BurrowThree, 600, 225, 865, 410 )

myManager.onTriggerIn(myRooms.Aqueduct_403, BurrowThree) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow403" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow403" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}

def BurrowFour = "BurrowFour"
myRooms.Aqueduct_404.createTriggerZone( BurrowFour, 200, 1, 490, 125 )

myManager.onTriggerIn(myRooms.Aqueduct_404, BurrowFour) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow404A" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow404A" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}

def BurrowLTOne = "BurrowLTOne"
myRooms.Aqueduct_305.createTriggerZone( BurrowLTOne, 165, 460, 480, 650 )

myManager.onTriggerIn(myRooms.Aqueduct_305, BurrowLTOne) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow305" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow305" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}

def BurrowLTTwo = "BurrowLTTwo"
myRooms.Aqueduct_405.createTriggerZone( BurrowLTTwo, 575, 160, 850, 335 )

myManager.onTriggerIn(myRooms.Aqueduct_405, BurrowLTTwo) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow405" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow405" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}

def BurrowLTThree = "BurrowLTThree"
myRooms.Aqueduct_505.createTriggerZone( BurrowLTThree, 130, 1, 430, 110 )

myManager.onTriggerIn(myRooms.Aqueduct_505, BurrowLTThree) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow505" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow505" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}

def BurrowHQ = "BurrowHQ"
myRooms.Aqueduct_404.createTriggerZone( BurrowHQ, 510, 400, 820, 635 )

myManager.onTriggerIn(myRooms.Aqueduct_404, BurrowHQ) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 98, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z07ScannedBurrow404B" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z07ScannedBurrow404B" )
		event.actor.addToPlayerVar( "burrowCount", 1 )
		curBurrowCount = event.actor.getPlayerVar( "burrowCount" )
		event.actor.centerPrint( "You successfully scan the burrow. You have scanned ${curBurrowCount.intValue()} out of 5 burrows." )
		println "**** BURROW COUNT = ${ curBurrowCount } ****"
		if( event.actor.getPlayerVar( "burrowCount" ) == 5 ) {
			event.actor.updateQuest( 98, "Agent Caruthers-VQS" )
		}
	}
}