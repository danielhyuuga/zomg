
//--------------------------------------------
// WARP CRYSTAL TO THE NULL CHAMBER           
//--------------------------------------------

toNullChamber = makeSwitch( "toNullChamber", myRooms.Aqueduct_602, 1535, 790 )
toNullChamber.unlock()
toNullChamber.setRange( 200 )
toNullChamber.setMouseoverText("To the Null Chamber")

onClickCrystal = new Object()

def goToNullChamber = { event ->
	synchronized( onClickCrystal ) {
		toNullChamber.off()
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "nullChamber1_1", 510, 465, 6 )
		event.actor.setQuestFlag( GLOBAL, "Z21AqueductWarpOkay" )
		event.actor.setQuestFlag( GLOBAL, "Z21ComingFromOutside" )
	}
}

toNullChamber.whenOn( goToNullChamber )
