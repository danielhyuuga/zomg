import com.gaiaonline.mmo.battle.script.*;

//NOTE: trying to force them to new VillageTraining; obsolete all here below.

Dani = spawnNPC("BFG-Dani", myRooms.SewersTraining_101, 275, 425)
Dani.setRotation( 45 )
Dani.setDisplayName( "Dani" )

alreadyEntered = false
onEnterDaniArea = new Object()

//=============================================
// Teach the player about the Map PDA          
//=============================================

myManager.onEnter( myRooms.SewersTraining_1 ) { event ->
	if( isPlayer( event.actor ) ) { event.actor.warp( "VillageTraining_2", 500, 500 ) }
	/* disabling because we're rerouting them to new VillageTraining.
	synchronized( onEnterDaniArea ) {
		if( isPlayer( event.actor ) && event.actor.hasQuestFlag( GLOBAL, "Z25DaniHello" ) && alreadyEntered == false ) {
			alreadyEntered = true
			event.actor.setQuestFlag( GLOBAL, "Z25PlayerBelongsInSewersTraining" )
			event.actor.centerPrint( "The door closes behind you as Barry locks up the Train Station." )
			event.actor.unsetQuestFlag( GLOBAL, "Z25DefendRing" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25FinishOkay" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25AftermathOkay" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14OkayToLeaveTrainingSewers" )
			myManager.schedule(3) {
				tutorialNPC.pushDialog( event.actor, "map" )
				towardDani.on()
			}
		}
	}
	*/
}
//  because we're rerouting them to new VillageTraining.
myManager.onEnter( myRooms.SewersTraining_101 ) { event ->
	if( isPlayer( event.actor ) ) { event.actor.warp( "VillageTraining_2", 500, 500 ) }
}
//because we're rerouting them to new VillageTraining.
myManager.onEnter( myRooms.SewersTraining_103 ) { event ->
if( isPlayer( event.actor ) ) { event.actor.warp( "VillageTraining_2", 500, 500 ) }
}

map = tutorialNPC.createConversation( "map", true )

map1 = [id:1]
map1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/screenTransition.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
map1.result = DONE
map.addDialog( map1, tutorialNPC )


//=============================================
// When entering the room with Dani, teach the 
// player to click on NPCs to speak with them  
//=============================================

myManager.onEnter( myRooms.SewersTraining_101 ) { event ->
	if( isPlayer( event.actor ) ) {
		player = event.actor
		if( event.actor.hasQuestFlag( GLOBAL, "Z25DaniHello" ) ) {
			event.actor.centerPrint( "Now move to Dani and click on her." )
		
		//These next sections recover in case the user times out WHILE selecting from a ring selection menu
		} else if( event.actor.hasQuestFlag( GLOBAL, "Z25ChoosingAttackRing" ) ) {
			myManager.schedule(1) { makeAttackRingMenu() }
			
		} else if( event.actor.hasQuestFlag( GLOBAL, "Z25ChoosingNonAttackRing" ) ) {
			myManager.schedule(1) { makeNonAttackRingMenu() }
		}
	}
}

//------------------------------------------
// Dani's Hello                             
//------------------------------------------

//Dani grants attack ring
def hello = Dani.createConversation( "hello", true, "Z25DaniHello" )

def hi1 = [id:1]
hi1.npctext = "Hi there. Just come in from the Train?"
hi1.playertext = "I sure did."
hi1.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Dani" )
}
hi1.result = 2
hello.addDialog( hi1, Dani )

def hi2 = [id:2]
hi2.npctext = "Okay. I'll get you on your way quickly then, but first, do you have any rings?"
hi2.playertext = "Rings?"
hi2.result = 3
hello.addDialog( hi2, Dani )

def hi3 = [id:3]
hi3.npctext = "The Town Council insists that everyone be able to defend themselves, and the only things that affect the Animated are the rings."
hi3.result = 4
hello.addDialog( hi3, Dani )

def hi4 = [id:4]
hi4.npctext = "So, you need some rings. Take your time. You only need to make a quick couple of choices and we'll get you on your way."
hi4.flag = [ "!Z25DaniHello", "Z25ChoosingAttackRing" ]
hi4.exec = { event ->
	controlPlayer( event.player )
	if( event.player.hasQuestFlag( GLOBAL, "Z25ReceivedAttackRing" ) ) {
		event.player.centerPrint( "You've already received your Attack Ring!" )
		event.player.unsetQuestFlag( GLOBAL, "Z25ChoosingAttackRing" )
		event.player.setQuestFlag( GLOBAL, "Z25DefendRing" )
		Dani.pushDialog( event.player, "defend" )
	} else {
		makeAttackRingMenu()
	}
}
hi4.result = DONE
hello.addDialog( hi4, Dani )

//Dani grants defend ring
def defend = Dani.createConversation( "defend", true, "Z25DefendRing" )

def def1 = [id:1]
def1.npctext = "Good! Now choose a non-damaging ring and we're done!"
def1.flag = [ "!Z25DefendRing", "Z25ChoosingNonAttackRing" ]
def1.exec = { event ->
	if( event.player.hasQuestFlag( GLOBAL, "Z25ReceivedDefendRing" ) ) {
		event.player.centerPrint( "You've received your Non-Damage Ring already!" )
		event.player.unsetQuestFlag( GLOBAL, "Z25ChoosingNonAttackRing" )
		event.player.setQuestFlag( GLOBAL, "Z25FinishOkay" )
		Dani.pushDialog( event.player, "finish" )
	} else {
		makeNonAttackRingMenu()
	}
}
def1.result = DONE
defend.addDialog( def1, Dani )

//Now finish up
def finish = Dani.createConversation( "finish", true, "Z25FinishOkay" )

def fin1 = [id:1]
fin1.npctext = "Excellent. You're all set now."
fin1.playertext = "Thanks!"
fin1.result = 2
finish.addDialog( fin1, Dani )

def fin2 = [id:2]
fin2.npctext = "So you have one more choice to make. Will you head to the Waterworks and practice using your rings, or head to the Village Greens and help Commander Leon?"
fin2.playertext = "(Click on a conversation option below to make a choice.)"
fin2.options = []
fin2.options << [text: "I'll go to the Waterworks to practice.", result: 3]
fin2.options << [text: "I'm heading up to the Village Greens to see Leon.", result: 4]
finish.addDialog( fin2, Dani )

def fin3 = [id:3]
fin3.npctext = "Good choice. Knowledge is a powerful thing. Head through the door over there, then up the stairs and turn left to find the ladder up to Barton Town."
fin3.flag = [ "!Z25FinishOkay", "Z25ComingFromDani" ]
fin3.exec = { event ->
	freePlayer( event.player )
	sewerExit.on()
}
fin3.result = DONE
finish.addDialog( fin3, Dani )

def fin4 = [id:4]
fin4.npctext = "No problem. Just use the ladder over there and you'll come out near Leon, the Captain of the Guard. Ask him for help to get started."
fin4.flag = [ "!Z25FinishOkay", "Z25ComingFromDani" ]
fin4.exec = { event ->
	freePlayer( event.player )
	villageExit.on()
}
fin4.result = DONE
finish.addDialog( fin4, Dani )


//Dani aftermath conversation
def aftermath = Dani.createConversation( "aftermath", true, "Z25ComingFromDani" )

def after1 = [id:1]
after1.npctext = "It's up to you at this point. Either go up the ladder to exit into the Village Greens, or head into the Waterworks to do some more training."
after1.result = 2
aftermath.addDialog( after1, Dani )

def after2 = [id:2]
after2.npctext = "The ladder is north of me, and the entrance to the Waterworks is to the east. Good luck!"
after2.result = DONE
aftermath.addDialog( after2, Dani )


//=======================================
// GREEN ARROWS                          
//=======================================

//Toward Dani
towardDani = makeSwitch( "edgeArrow", myRooms.SewersTraining_1, 0, 0 )
towardDani.lock()
towardDani.off()
towardDani.setUsable( false )

//Into the Sewers Battlefield
sewerExit = makeSwitch( "greenArrow2", myRooms.SewersTraining_101, 0, 0 )
sewerExit.lock()
sewerExit.off()
sewerExit.setUsable( false )

//Into the Mumbler Room
villageExit = makeSwitch( "greenArrow1", myRooms.SewersTraining_101, 0, 0 )
villageExit.lock()
villageExit.off()
villageExit.setUsable( false )


//=====================================
// EXIT TO SEWERS                      
//=====================================
sewersTrainingExit = makeSwitch( "Sewers_In", myRooms.SewersTraining_101, 960, 370 )
sewersTrainingExit.unlock()
sewersTrainingExit.off()
sewersTrainingExit.setRange( 200 )
sewersTrainingExit.setMouseoverText("Enter the Waterworks")

def exitSewersTraining = { event ->
	sewersTrainingExit.off()
	sewersTrainingExit.lock()
	myManager.schedule(2) { sewersTrainingExit.unlock() }
	if( event.actor.hasQuestFlag( GLOBAL, "Z25ComingFromDani" ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z25PlayerBelongsInSewersTraining" )
		event.actor.centerPrint( "You pass through the doorway into the cool, moist air of the Waterworks tunnels." )
		event.actor.leftNoobZone()
		event.actor.warp( "Sewers_5", 350, 510 )
	} else {
		event.actor.centerPrint( "You need to talk to Dani before you may leave this area." )
	}
}

sewersTrainingExit.whenOn( exitSewersTraining )


//-------------------------------------------
// TO VILLAGE GREENS                         
//-------------------------------------------
ladder = makeSwitch( "ladder1", myRooms.SewersTraining_101, 290, 160 )
ladder.unlock()
ladder.off()
ladder.setRange( 200 )
ladder.setMouseoverText("Enter the Village Greens")

def goToBarton = { event ->
	ladder.off()
	ladder.lock()
	myManager.schedule(2) { ladder.unlock() }
	if( event.actor.hasQuestFlag( GLOBAL, "Z25ComingFromDani" ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z25PlayerBelongsInSewersTraining" )
		event.actor.centerPrint( "You climb up the long ladder and emerge into the sunshine." )
		event.actor.leftNoobZone()
		event.actor.warp( "VILLAGE_405", 620, 330 )
		event.actor.addMiniMapQuestActorName( "Leon-VQS" )
	} else {
		event.actor.centerPrint( "You need to talk to Dani before you may leave this area." )
	}
}

ladder.whenOn( goToBarton )




dialogBoxWidth = 400
CL = 1
CLdecimal = 0

//ATTACK RING MENUS
def makeAttackRingMenu() {
	titleString = "Attack Rings"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Slash" ]
	
	uiButtonMenu( player, "attackRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Slash" ) {
			makeSlashMenu()
		}
		if( event.selection == "Bump" ) {
			makeBumpMenu()
		}
	}
}

def makeSlashMenu() {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Back to Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			event.actor.setQuestFlag( GLOBAL, "Z25DefendRing" )
			event.actor.setQuestFlag( GLOBAL, "Z25ReceivedAttackRing" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ChoosingAttackRing" )
			Dani.pushDialog( player, "defend" )
		}
		if( event.selection == "Back to Menu" ) {
			makeAttackRingMenu()
		}
	}
}
	
def makeBumpMenu() {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Back to Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			event.actor.setQuestFlag( GLOBAL, "Z25DefendRing" )
			event.actor.setQuestFlag( GLOBAL, "Z25ReceivedAttackRing" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ChoosingAttackRing" )
			Dani.pushDialog( player, "defend" )
		}
		if( event.selection == "Back to Menu" ) {
			makeAttackRingMenu()
		}
	}
}
	
//NON-ATTACK RING MENUS	
def makeNonAttackRingMenu() {
	titleString = "Non-Attack Rings"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Teflon Spray", "Bandage", "Quicksand" ]
	
	uiButtonMenu( player, "nonAttackRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu()
		}
		if( event.selection == "Bandage" ) {
			makeBandageMenu()
		}
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu()
		}
	}
}

def makeTeflonSprayMenu() {
	titleString = "Teflon Spray"
	descripString = "Make some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and can even sometimes Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Back to Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			event.actor.setQuestFlag( GLOBAL, "Z25FinishOkay" )
			event.actor.setQuestFlag( GLOBAL, "Z25ReceivedDefendRing" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ChoosingNonAttackRing" )
			Dani.pushDialog( player, "finish" )
		}
		if( event.selection == "Back to Menu" ) {
			makeNonAttackRingMenu()
		}
	}
}
	
def makeBandageMenu() {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short period, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Back to Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			event.actor.setQuestFlag( GLOBAL, "Z25FinishOkay" )
			event.actor.setQuestFlag( GLOBAL, "Z25ReceivedDefendRing" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ChoosingNonAttackRing" )
			Dani.pushDialog( player, "finish" )
		}
		if( event.selection == "Back to Menu" ) {
			makeNonAttackRingMenu()
		}
	}
}
	
def makeQuicksandMenu() {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Back to Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			event.actor.setQuestFlag( GLOBAL, "Z25FinishOkay" )
			event.actor.setQuestFlag( GLOBAL, "Z25ReceivedDefendRing" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ChoosingNonAttackRing" )
			Dani.pushDialog( player, "finish" )
		}
		if( event.selection == "Back to Menu" ) {
			makeNonAttackRingMenu()
		}
	}
}
		
