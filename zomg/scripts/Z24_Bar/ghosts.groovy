import com.gaiaonline.mmo.battle.script.*;

// timer start - lanzer
def eventStart = false
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0


//------------------------------------------
//GHOST HUNT (Pier)               
//------------------------------------------
def spawning = 'none'


//SPAWNERS

ghost1 = myRooms.Bar_1.spawnSpawner( "ghost1", "ghost1", 100 )
ghost1.setPos( 850, 340 )
ghost1.setMonsterLevelForChildren( 1.0 )
ghost1.setHomeTetherForChildren( 6000 )
ghost1.setEdgeHinting( false )
ghost1.setTargetCycle( false )
ghost1.stopSpawning()

ghost2 = myRooms.Bar_1.spawnSpawner( "ghost2", "ghost2", 100 )
ghost2.setPos( 850, 340 )
ghost2.setMonsterLevelForChildren( 1.0 )
ghost2.setHomeTetherForChildren( 6000 )
ghost2.setEdgeHinting( false )
ghost2.setTargetCycle( false )
ghost2.stopSpawning()

ghost3 = myRooms.Bar_1.spawnSpawner( "ghost3", "ghost3", 100 )
ghost3.setPos( 850, 340 )
ghost3.setMonsterLevelForChildren( 1.0 )
ghost3.setHomeTetherForChildren( 6000 )
ghost3.setEdgeHinting( false )
ghost3.setTargetCycle( false )
ghost3.stopSpawning()

ghost4 = myRooms.Bar_1.spawnSpawner( "ghost4", "ghost4", 100 )
ghost4.setPos( 850, 340 )
ghost4.setMonsterLevelForChildren( 1.0 )
ghost4.setHomeTetherForChildren( 6000 )
ghost4.setEdgeHinting( false )
ghost4.setTargetCycle( false )
ghost4.stopSpawning()

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

ghostSpawnOkay = false

def countGhosts() {
	ghostCount = 0
	myRooms.values().each() {
		ghostCount = ghostCount + it.getSpawnTypeCount("ghost1") + it.getSpawnTypeCount("ghost2") + it.getSpawnTypeCount("ghost3") + it.getSpawnTypeCount("ghost4") 
	}
	
	//println "**** ghostCount = ${ghostCount} ****"
}

numGhostPositionsPerRoom = 4

ghostSpawnerList = [ ghost1, ghost2, ghost3, ghost4 ]

village3Map = [ 1:[650, 588, 0], 2:[320, 588, 0], 3:[510, 300, 0], 4:[494, 470, 0], 5:"Bar_1", 6:myRooms.Bar_1 ]
// 588 - 250:950, 494,470,   510,300.  372,144
//SAFE ROOM! village5Map = [ 1:[180, 340, 0], 2:[540, 335, 0], 3:[835, 465, 0], 4:[1000, 375, 0], 5:"Bar_101" ]
//SAFE ROOM! village6Map = [ 1:[35, 345, 0], 2:[645, 400, 0], 3:[750, 650, 0], 4:[960, 450, 0], 5:"VILLAGE_6" ]

roomList = [ village3Map ]
villageRoomList = ["Bar_1"]

//Server initialization script to figure out what day it is and to set the tree appropriately
def ghostTimer() {
    def end = startDay + 14
    def endMonth = 10
    if (end > 31) {
    	endMonth = 11
    	end = end - 31
    }
	if (debug == 1) {
		ghostSpawnOkay = true
		maxGhosts = 10
		spawnGhosts()
	} else {
		//bracket each date and set the same stuff that the "scheduleAt" stuff, below, would do normally
		if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && ghostSpawnOkay == false && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
			ghostSpawnOkay = true
			maxGhosts = 4
			spawnGhosts()
		}
		if( isLaterThan( "$endMonth/$end/$year 11:59 pm" ) ) {
			ghostSpawnOkay = false
		}
	}
	myManager.schedule(60) { ghostTimer() }
}

def setMaxGhosts() {
		maxGhosts = 4
}

//Decrease the ghostSpawnTimer if there are lots of players in the zone
def setGhostSpawnTimer() {
	if( areaPlayerCount() > 10 ) {
		ghostSpawnTimer = 10 - ( ( areaPlayerCount() - 10 ) / 5 )
	} else {
		ghostSpawnTimer = 10
	}
}	

//randomly spawn ghosts
def spawnGhosts() {
	//println "**** STARTING SPAWNEGGS ROUTINE ****"
	//if the current number of ghosts in the zone is less than "maxGhosts", then spawn an ghost
	if( ghostSpawnOkay == false ) return
	countGhosts()
	setMaxGhosts()
	setGhostSpawnTimer()
	if( ghostCount < maxGhosts ) {
		//find the room in the zone to warp to
		room = random( roomList )
		position = random( numGhostPositionsPerRoom )
		positionInfo = room.get( position )
		spawnHolder = positionInfo.get(2)
		//println "**** position = ${position} ****"
		//println "**** positionInfo = ${positionInfo} ****"
		//now look in position 2 of the "positionInfo" to see if the ghost is spawned yet or not. (0 = not spawned; 1 = spawned)
		//println "**** spawnHolder = ${ spawnHolder } ****"
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//println "**** modified positionInfo = ${positionInfo} ****"
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			//println "**** modified room map = ${room} ****"
			//determine which spawner to use

			ghostSpawner = random( ghostSpawnerList )

 			//println "**** using ghostSpawner = ${ghostSpawner} ****"
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//ghostSpawner.setHomeForChildren( roomName, X, Y )
			//now spawn the ghost
			ghostSpawner.warp( roomName.toString(), X, Y )
			ghostSpawner.setHomeForChildren( "${roomName}", X, Y )
			ghost = ghostSpawner.forceSpawnNow()

			runOnDeath( ghost, { event ->
				event.killer.addPlayerVar( "Z01GhostCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathRoomObject = event.actor.getRoom()
				//println "^^^^ deathRoom = ${event.actor.getRoom()} ^^^^"
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//println "**** DEATH POINT = roomName = ${ roomName }; deathX = ${deathX}; deathY = ${deathY} ****"
				//search through all the maps to find the map with the correct room name in it
				roomList.each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
						//println "**** deathRoom = ${deathRoom} ****"
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an ghost can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//println "****modified death position info list = ${deathPositionInfo} ****"
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
				//println "****modified deathRoom map = ${ deathRoom } ****"
				rewardPlayer( event )
			} )
		} else {
			//println "**** ghost position is already occupied ****"
		}
		//now spawn another ghost in 10 seconds
		myManager.schedule(ghostSpawnTimer) { spawnGhosts() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule(ghostSpawnTimer) { spawnGhosts() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//println "****checking position....deathPositionInfo being checked = {$deathPositionInfo} ****"
		//if the first element in this list is the same place the ghost "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
	//println "**** deathPosition = ${deathPosition} ****"
}

lootList = [ "100481", "100479", "100477", "100475", "100473", "100469" ] //100481 - Toothbrush; 100479 - Dental Floss; 100477 - Candy Corn; 100475 - Pumpkin Lid; 100473 - Vampire Blood; 100469 - Pumpkin Seed
recipeList = [ "46026", "46026", "46026", "61351", "61351", "45955" ] //46026 - Jackies; 45955 - Night Fright; 61351 - Pumpkin Fluff Plushie
goldGrantMin = 1000000
goldGrantMax = 1500000

def rewardPlayer( event ) {
	roll = random( 100 )
	if( roll <= 65 ) {
		//gold only
		event.killer.centerPrint( "The ghost had some gold!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
	} else if ( roll > 65 && roll <= 94 ) {
		//gold plus item
		event.killer.centerPrint( "The ghost had something to use in recipes!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
		event.killer.grantItem( random( lootList ) )
	} else if (roll > 99 ) {
		player.grantQuantityItem( 1001169, 1 )
		player.centerPrint( "Look! A Shadow Orb!" )
	} else if (roll > 95 ) {
		//recipe
		event.killer.centerPrint( "You get a special recipe!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
		event.killer.grantItem( random( recipeList ) )
	}
	//now check for badges
	if( event.killer.getPlayerVar( "Z01GhostCounter" ) >= 100 && !event.killer.isDoneQuest( 347 ) ) {
		event.killer.updateQuest( 347, "Story-VQS" )
	}
	if( event.killer.getPlayerVar( "Z01GhostCounter" ) >= 1000 && !event.killer.isDoneQuest( 348 ) ) {
		event.killer.updateQuest( 348, "Story-VQS" )
	}
}


//start it all up
ghostTimer()
