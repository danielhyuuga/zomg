import com.gaiaonline.mmo.battle.script.*;

// timer start - lanzer
def eventStart = false
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
lastSpawn = 0


//-----------------------------Pancakes------------------------------
Pancakes = spawnNPC("[NPC] Pancakes", myRooms.Bar_1, 770, 160)
Pancakes.setRotation(45)

def pancakesDefault = Pancakes.createConversation("pancakesDefault", true)

def pancakesDefault1 = [id:1]
pancakesDefault1.npctext = "(The Pancakes seem on the brink of movement, but are in fact utterly still)"
pancakesDefault1.result = 2
pancakesDefault.addDialog(pancakesDefault1, Pancakes)

def pancakesDefault2 = [id:2]
pancakesDefault2.npctext = 'Us "plain" pancake.  Used to be good.'
pancakesDefault2.result = 3
pancakesDefault.addDialog(pancakesDefault2, Pancakes)

def pancakesDefault3 = [id:3]
pancakesDefault3.npctext = "Patron have smile all a-round face when look at us.  Then patron see other patron PUMPKIN pancake... we fall-flat!  We fall-flat in garbage!  First patron order pumpkin too.  We expire in dumpster.  Pumpkin privilege!"
pancakesDefault3.result = 4
pancakesDefault.addDialog(pancakesDefault3, Pancakes)

def pancakesDefault4 = [id:4]
pancakesDefault4.npctext = "Christmas more a-round Halloween store each year.  Jack then say Halloween start a-round Easter over!  Make many pumpkin a-round long time.  Pumpkin a-round most of year, no special."
pancakesDefault4.result = 5
pancakesDefault.addDialog(pancakesDefault4, Pancakes)

def pancakesDefault5 = [id:5]
pancakesDefault5.npctext = "Jack turn strong Animated to pumpkin, bring a-round here protect Jack.  Jack know do wrong.  No pumpkin, Jack fall-flat!"
pancakesDefault5.result = 6
pancakesDefault.addDialog(pancakesDefault5, Pancakes)

def pancakesDefault6 = [id:6]
pancakesDefault6.npctext = "Us want Jack fall-flat.  Fall-flat all pumpkins a-round Jack.  Back normal."
pancakesDefault6.result = 7
pancakesDefault.addDialog(pancakesDefault6, Pancakes)

def pancakesDefault7 = [id:7]
pancakesDefault7.npctext = "Now hate name flapjacks.  No call a-round us, or we fall-flat."
pancakesDefault7.result = 8
pancakesDefault.addDialog(pancakesDefault7, Pancakes)

def pancakesDefault8 = [id:8]
pancakesDefault8.npctext = "Fall-flat Jack for us?"
pancakesDefault8.options = []
pancakesDefault8.options << [text:"I'll do my best.", result: 10]
pancakesDefault8.options << [text:"Could you ask Charon to transport me back for now?", result: 9]
pancakesDefault.addDialog(pancakesDefault8, Pancakes)

def pancakesDefault9 = [id:9]
pancakesDefault9.npctext = "See you a-round!"
pancakesDefault9.exec = { event ->
        event.actor.warp( "BARTON_202", 108, 310 )
}
pancakesDefault9.result = DONE
pancakesDefault.addDialog(pancakesDefault9, Pancakes)

def pancakesDefault10 = [id:10]
pancakesDefault10.npctext = "No waffling now!"
pancakesDefault10.result = DONE
pancakesDefault.addDialog(pancakesDefault10, Pancakes)


//------------------------------------------
//GHOST HUNT (Pier)               
//------------------------------------------
def spawning = 'none'
def jackSpec = [
	isAllowed : { attacker, target ->
 		def player = isPlayer( attacker ) ? attacker : target
 		def allowed = player.getConLevel() < 2.1
			if( player == attacker && !allowed ) { player.centerPrint( "You must be level 2.0 or below to attack here." ); }
			return allowed
		}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//SPAWNERS

pumpkin1 = myRooms.Bar_101.spawnSpawner("pumpkin_fluff2", "pumpkin_brookie_fluff", 100)
pumpkin1.setMiniEventSpec(jackSpec)
pumpkin1.setPos( 750, 340 )
pumpkin1.setMonsterLevelForChildren( 3.0 )
pumpkin1.setHomeTetherForChildren( 6000 )
pumpkin1.setEdgeHinting( false )
pumpkin1.setTargetCycle( false )
pumpkin1.stopSpawning()

pumpkin2 = myRooms.Bar_101.spawnSpawner("pumpkin_fluff3", "pumpkin_fritter_fluff", 100)
pumpkin2.setMiniEventSpec(jackSpec)
pumpkin2.setPos( 650, 340 )
pumpkin2.setMonsterLevelForChildren( 2.0 )
pumpkin2.setHomeTetherForChildren( 6000 )
pumpkin2.setEdgeHinting( false )
pumpkin2.setTargetCycle( false )
pumpkin2.stopSpawning()

pumpkin3 = myRooms.Bar_101.spawnSpawner("pumpkin_fluff4", "pumpkin_tart_fluff", 100)
pumpkin3.setMiniEventSpec(jackSpec)
pumpkin3.setPos( 550, 340 )
pumpkin3.setMonsterLevelForChildren( 2.0 )
pumpkin3.setHomeTetherForChildren( 6000 )
pumpkin3.setEdgeHinting( false )
pumpkin3.setTargetCycle( false )
pumpkin3.stopSpawning()

pumpkin4 = myRooms.Bar_101.spawnSpawner("pumpkin_fluff5", "pumpkin_truffle_fluff", 100)
pumpkin4.setMiniEventSpec(jackSpec)
pumpkin4.setPos( 450, 340 )
pumpkin4.setMonsterLevelForChildren( 1.0 )
pumpkin4.setHomeTetherForChildren( 6000 )
pumpkin4.setEdgeHinting( false )
pumpkin4.setTargetCycle( false )
pumpkin4.stopSpawning()

pumpkin5 = myRooms.Bar_101.spawnSpawner("pumpkin_fluff6", "pumpkin_mousse_fluff", 100)
pumpkin5.setMiniEventSpec(jackSpec)
pumpkin5.setPos( 350, 340 )
pumpkin5.setMonsterLevelForChildren( 1.0 )
pumpkin5.setHomeTetherForChildren( 6000 )
pumpkin5.setEdgeHinting( false )
pumpkin5.setTargetCycle( false )
pumpkin5.stopSpawning()

jack = myRooms.Bar_101.spawnSpawner("jack", "halloween_jack_boss", 100)
jack.setMiniEventSpec(jackSpec)
jack.setPos( 400, 340 )
jack.setMonsterLevelForChildren( 3.0 )
jack.setHomeTetherForChildren( 6000 )
jack.setEdgeHinting( false )
jack.setTargetCycle( false )
jack.stopSpawning()

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

ghostSpawnOkay = false

def countGhosts() {
	ghostCount = 0
	myRooms.values().each() {
		ghostCount = ghostCount + it.getSpawnTypeCount("pumpkin_fluff2") + it.getSpawnTypeCount("pumpkin_fluff3") + it.getSpawnTypeCount("pumpkin_fluff4") + it.getSpawnTypeCount("pumpkin_fluff5") + it.getSpawnTypeCount("pumpkin_fluff6")  + it.getSpawnTypeCount("jack") 
	}
	
	//println "**** ghostCount = ${ghostCount} ****"
}

numGhostPositionsPerRoom = 4

ghostSpawnerList = [ pumpkin5, pumpkin4, pumpkin3, pumpkin2,pumpkin1]

//village3Map = [ 1:[650, 588, 0], 2:[320, 588, 0], 3:[510, 300, 0], 4:[494, 470, 0], 5:"Bar_1", 6:myRooms.Bar_1 ]
// 588 - 250:950, 494,470,   510,300.  372,144
village4Map = [ 1:[150, 500, 0], 2:[550, 500, 0], 3:[800, 270, 0], 4:[240, 250, 0], 5:"Bar_101", 6:myRooms.Bar_101 ]
// 500y - 20 :640  , 270 - 0 : 900, 
//village5Map = [ 1:[150, 500, 0], 2:[450, 500, 0], 3:[600, 270, 0], 4:[240, 250, 0], 5:"Bar_201", 6:myRooms.Bar_201 ]

roomList = [ village4Map ]
villageRoomList = ["Bar_101"]
jackMessages = ["I will destroy you!", "Foolish Gaians, I cannot be killed.", "Noooooooo! You will be defeated!", "Darkness will cover the world.", "Do not try to stop me, my victory is assured!"]
jackDeathMessages = ["What?! How can this be?!", "The end is near... he's coming!"]

jackSpawnedToday = false
spawningJack = false
scale = 1

//Server initialization script to figure out what day it is and to set the tree appropriately
def ghostTimer() {
    def end = startDay + 14
    def endMonth = 10
    if (end > 31) {
    	endMonth = 11
    	end = end - 31
    }
	if (debug == 1) {
		ghostSpawnOkay = true
		maxGhosts = 10
		spawnGhosts()
	} else {
		//bracket each date and set the same stuff that the "scheduleAt" stuff, below, would do normally
		if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && ghostSpawnOkay == false && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
			ghostSpawnOkay = true
			maxGhosts = 10
			spawnGhosts()
		}
		if( isLaterThan( "$endMonth/$end/$year 11:59 pm" ) ) {
			ghostSpawnOkay = false
		}
	}
	myManager.schedule(60) { ghostTimer() }
}

def jackTimer() {

    Date date = new Date();   // given date
    Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
    calendar.setTime(date);   // assigns calendar to given date
    hournow = (calendar.get(Calendar.MONTH) * 100) + calendar.get(Calendar.HOUR_OF_DAY)

    if (lastSpawn == 0) {
    	lastSpawn = hournow - 1
    }

    if (jackSpawnedToday == true && jackPresent == false && lastSpawn <= hournow ) {
        lastSpawn = hournow + random(1,2)
        jackSpawnedToday = false
    }
	if (debug == 1) println "**** Check if Jack is present => ${jackPresent} spawnedToday => ${jackSpawnedToday}****"

    if (debug == 1 && jackPresent == false) {
    	jackSpawnedToday = false
    }

	myManager.schedule(60) { ghostTimer() }
}

def setMaxGhosts() {
		maxGhosts = 10
	
}

//Decrease the ghostSpawnTimer if there are lots of players in the zone
def setGhostSpawnTimer() {
	if( areaPlayerCount() > 10 ) {
		ghostSpawnTimer = 10 - ( ( areaPlayerCount() - 10 ) / 5 )
	} else {
		ghostSpawnTimer = 10
	}
}	

//randomly spawn ghosts
def spawnGhosts() {
	//println "**** STARTING SPAWNEGGS ROUTINE ****"
	//if the current number of ghosts in the zone is less than "maxGhosts", then spawn an ghost
	if( ghostSpawnOkay == false ) return
	countGhosts()
	setMaxGhosts()
	setGhostSpawnTimer()
	if( ghostCount < maxGhosts ) {
		//find the room in the zone to warp to
		room = random( roomList )
		position = random( numGhostPositionsPerRoom )
		positionInfo = room.get( position )
		spawnHolder = positionInfo.get(2)
		//println "**** position = ${position} ****"
		//println "**** positionInfo = ${positionInfo} ****"
		//now look in position 2 of the "positionInfo" to see if the ghost is spawned yet or not. (0 = not spawned; 1 = spawned)
		//println "**** spawnHolder = ${ spawnHolder } ****"
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//println "**** modified positionInfo = ${positionInfo} ****"
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			//println "**** modified room map = ${room} ****"
			//determine which spawner to use
			ghostRoll = random(1,9)
			if (debug == 1) {
				ghostRoll = 1
			}
			
			if(ghostRoll == 1 && jackSpawnedToday == false ) {
				jackSpawnedToday = true
				ghostSpawner = jack
				jackPresent = true
				spawningJack = true
				scale = 1
			} else {
				ghostSpawner = random( ghostSpawnerList )
				if (ghostSpawner == pumpkin1) {
					scale = 1.5
				} else if (ghostSpawner == pumpkin2) {
					scale = 1.3
				} else {
					scale = 1
				}
			}
			//println "**** using ghostSpawner = ${ghostSpawner} ****"
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//ghostSpawner.setHomeForChildren( roomName, X, Y )
			//now spawn the ghost
			ghostSpawner.warp( roomName.toString(), X, Y )
			ghostSpawner.setHomeForChildren( "${roomName}", X, Y )
			if(spawningJack == true) {
				println "**** warp ${ghostSpawner} to ${ roomName.toString() }, ${X}, ${Y} ****"
			}
			ghost = ghostSpawner.forceSpawnNow()
			if (scale != 1) {
				ghost.setClientDisplayScale(scale)
			}
			//when the ghost is destroyed, reset its record to 0 so the ghost can spawn again later
			if(spawningJack == true) {
				spawningJack = false
				ghost.shout("Muahaha! Jack isss back!")
				myManager.onHealth(ghost) { health ->
					if((health.didTransition(85) || health.didTransition(70) || health.didTransition(55) || health.didTransition(40) || health.didTransition(25) || health.didTransition(10)) && health.isDecrease()) {
						health.actor.shout(random(jackMessages))
						spawnX = health.actor.getX() + 7
						spawnY = health.actor.getY() + 7
						spawnRoom = health.actor.getRoomName()
						if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
							pumpkin.warp(spawnRoom, spawnX, spawnY)
							myManager.schedule(3) { 
								pumpkin1 = pumpkin.forceSpawnNow() 
								println "^^^^ Spawned ${pumpkin1} at ${spawnRoom}, ${spawnX}, ${spawnY} ^^^^"
							}
						}

						spawnX = health.actor.getX() - 7
						spawnY = health.actor.getY() - 7
						spawnRoom = health.actor.getRoomName()
						if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
							pumpkin.warp(spawnRoom, spawnX, spawnY)
							myManager.schedule(3) { 
								pumpkin2 = pumpkin.forceSpawnNow() 
								println "^^^^ Spawned ${pumpkin2} at ${spawnRoom}, ${spawnX}, ${spawnY} ^^^^"
							}
						}

						spawnX = health.actor.getX() + 7
						spawnY = health.actor.getY() - 7
						spawnRoom = health.actor.getRoomName()
						if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
							pumpkin.warp(spawnRoom, spawnX, spawnY)
							myManager.schedule(3) { 
								pumpkin3 = pumpkin.forceSpawnNow() 
								println "^^^^ Spawned ${pumpkin3} at ${spawnRoom}, ${spawnX}, ${spawnY} ^^^^"
							}
						}

						spawnX = health.actor.getX() - 7
						spawnY = health.actor.getY() + 7
						spawnRoom = health.actor.getRoomName()
						if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
							pumpkin.warp(spawnRoom, spawnX, spawnY)
							myManager.schedule(3) { 
								pumpkin4 = pumpkin.forceSpawnNow() 
								println "^^^^ Spawned ${pumpkin4} at ${spawnRoom}, ${spawnX}, ${spawnY} ^^^^"
							}
						}
					}
				}
				runOnDeath(ghost) { event ->
					event.actor.shout(random(jackDeathMessages))
					event.actor.getHated().each() {
						it.addPlayerVar("Z01GhostCounter", 1)
						if(!it.hasQuestFlag(GLOBAL, "Z01_Jack_Defeated")) {
							it.setQuestFlag(GLOBAL, "Z01_Jack_Defeated")
							it.updateQuest(349, "Story-VQS")
							goldGrant = random(7500, 9999) * 133
							it.grantCoins(goldGrant)
							it.grantItem("63211")
							//it.centerPrint("You received a Golden Ticket!")
						} else {
							goldGrant = random(7500, 9999) * 133
							it.grantCoins(goldGrant)
							//it.centerPrint("You've already received your Golden Ticket, here's some gold instead.")
						}
					}
					deathRoomName = event.actor.getRoomName()
					deathX = event.actor.getX().intValue()
					deathY = event.actor.getY().intValue()
					//println "**** DEATH POINT = roomName = ${ roomName }; deathX = ${deathX}; deathY = ${deathY} ****"
					//search through all the maps to find the map with the correct room name in it
					roomList.each{
						if( it.get(5) == deathRoomName ) {
							deathRoom = it
							//println "**** deathRoom = ${deathRoom} ****"
						}
					}
					//once the correct map is found, use the death X to make a match for which position to read
					seed = 1
					deathPosition = 0
					findCorrectPosition()
					deathPositionInfo = deathRoom.get( deathPosition )
					//now that we have the correct position, change out the info properly so an ghost can spawn there again
					deathPositionInfo.remove(2) //remove the placeholder at the end of the list
					deathPositionInfo.add(0)
					//println "****modified death position info list = ${deathPositionInfo} ****"
					//now update the deathRoom Map with the updated list
					deathRoom.remove( deathPosition)
					deathRoom.put( deathPosition, deathPositionInfo )
					jackPresent = false
					//println "****modified deathRoom map = ${ deathRoom } ****"
				}
			} else {
				runOnDeath( ghost, { event ->
					event.killer.addPlayerVar( "Z01GhostCounter", 1 )
					deathRoomName = event.actor.getRoomName()
					deathRoomObject = event.actor.getRoom()
					//println "^^^^ deathRoom = ${event.actor.getRoom()} ^^^^"
					deathX = event.actor.getX().intValue()
					deathY = event.actor.getY().intValue()
					//println "**** DEATH POINT = roomName = ${ roomName }; deathX = ${deathX}; deathY = ${deathY} ****"
					//search through all the maps to find the map with the correct room name in it
					roomList.each{
						if( it.get(5) == deathRoomName ) {
							deathRoom = it
							//println "**** deathRoom = ${deathRoom} ****"
						}
					}
					//once the correct map is found, use the death X to make a match for which position to read
					seed = 1
					deathPosition = 0
					findCorrectPosition()
					deathPositionInfo = deathRoom.get( deathPosition )
					//now that we have the correct position, change out the info properly so an ghost can spawn there again
					deathPositionInfo.remove(2) //remove the placeholder at the end of the list
					deathPositionInfo.add(0)
					//println "****modified death position info list = ${deathPositionInfo} ****"
					//now update the deathRoom Map with the updated list
					deathRoom.remove( deathPosition)
					deathRoom.put( deathPosition, deathPositionInfo )
					//println "****modified deathRoom map = ${ deathRoom } ****"
					rewardPlayer( event )
				} )
			}
			spawningJack = false
		} else {
			//println "**** ghost position is already occupied ****"
		}
		//now spawn another ghost in 10 seconds
		myManager.schedule(ghostSpawnTimer) { spawnGhosts() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule(ghostSpawnTimer) { spawnGhosts() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//println "****checking position....deathPositionInfo being checked = {$deathPositionInfo} ****"
		//if the first element in this list is the same place the ghost "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
	//println "**** deathPosition = ${deathPosition} ****"
}

lootList = [ "100481", "100479", "100477", "100475", "100473", "100469" ] //100481 - Toothbrush; 100479 - Dental Floss; 100477 - Candy Corn; 100475 - Pumpkin Lid; 100473 - Vampire Blood; 100469 - Pumpkin Seed
recipeList = [ "46026", "46026", "46026", "61351", "61351", "45955" ] //46026 - Jackies; 45955 - Night Fright; 61351 - Pumpkin Fluff Plushie
goldGrantMin = 2000000
goldGrantMax = 3500000

def rewardPlayer( event ) {
	roll = random( 100 )
	if( roll <= 65 ) {
		//gold only
		event.killer.centerPrint( "Pumpkin fluff had some gold!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
	} else if ( roll > 65 && roll <= 95 ) {
		//gold plus item
		event.killer.centerPrint( "Pumpkin fluff had something to use in recipes!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
		event.killer.grantItem( random( lootList ) )
	} else if (roll > 99 ) {
		player.grantQuantityItem( 1001169, 1 )
		player.centerPrint( "Look! A Shadow Orb!" )
	} else if (roll > 95 ) {
		//recipe
		event.killer.centerPrint( "You get a special recipe!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
		event.killer.grantItem( random( recipeList ) )
	}
}


//start it all up
ghostTimer()
jackTimer()