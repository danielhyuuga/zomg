import com.gaiaonline.mmo.battle.script.*;

//TODO: Fix the Main Door Patrol once Ryan fixes "setPatrol"
//TODO: Fix the Main Door switch once the art team gets the multi-state switch completed

MinML = 8.0
ML = 8.0
MaxML = 10.0
securityLevel = ML

tunnelSpawned = false
complexEntered = false
difficulty = null
ENTRY_LIMIT = 40
SWITCH_TIMER = 5

playerSet = [] as Set
playersInComplex = [] as Set
spawnerList = [] as Set
switch702GuardList = [] as Set
switch803GuardList = [] as Set
activeSpawners = new HashMap()

def synchronized adjustML() {
	ML = MinML

	playerSet.clear()
	myRooms.Sealab_1.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_2.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_102.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_103.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_106.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_203.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_204.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_304.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_305.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_401.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_402.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_403.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_405.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_406.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_506.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_507.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_603.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_604.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_605.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_606.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_701.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_702.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_703.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_704.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_705.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_706.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_801.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_802.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_803.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_804.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_805.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_806.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_901.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_902.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_903.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_904.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_905.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_906.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1001.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1002.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1003.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1004.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1005.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1006.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	
	//adjust the encounter ML to the group
	random( playerSet ).getCrew().clone().each{		
		if( it.getConLevel() > ML && it.getConLevel() <= MaxML ) {
			ML = it.getConLevel()
		} else if( it.getConLevel() > MaxML ) {
			ML = MaxML
		}
	}

	//kick players out if they are cheating
	playerSet.clone().each{
		if( it.getConLevel() > MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "Beach_2", 720, 280 )
		}
	}
	
	//DIFFICULTY
	if( difficulty == 1 ) { ML = ML - 0.6 }
	if( difficulty == 3 ) { ML = ML + 0.6 }
	
	getMonsterTotal()
	
	if(monsterCounter > ENTRY_LIMIT) {
		securityLevel = ML + 10
	} else {
		securityLevel = ML
	}

	//Now update all the spawners with the new ML so they start creating monsters at that new level
	
	//Robofish
	robo2a.setMonsterLevelForEveryone( ML )
	robo2b.setMonsterLevelForEveryone( ML )
	robo103a.setMonsterLevelForEveryone( ML )
	robo103b.setMonsterLevelForEveryone( ML )
	robo204a.setMonsterLevelForEveryone( ML )
	robo204b.setMonsterLevelForEveryone( ML )
	robo204c.setMonsterLevelForEveryone( ML )
	robo305a.setMonsterLevelForEveryone( ML )
	robo305b.setMonsterLevelForEveryone( ML )
	robo405.setMonsterLevelForEveryone( ML )
	robo406a.setMonsterLevelForEveryone( ML )
	robo406b.setMonsterLevelForEveryone( ML )
	robo507a.setMonsterLevelForEveryone( ML )
	robo507b.setMonsterLevelForEveryone( ML )
	grunny406a.setMonsterLevelForEveryone( ML )
	grunny406b.setMonsterLevelForEveryone( ML )
	//Tunnel Guards
	tunnel402a.setMonsterLevelForEveryone( ML )
	tunnel402b.setMonsterLevelForEveryone( ML )
	tunnel403a.setMonsterLevelForEveryone( ML )
	tunnel403b.setMonsterLevelForEveryone( ML )
	tunnel403c.setMonsterLevelForEveryone( ML )
	//X Rooms
	cycle1003A.setMonsterLevelForEveryone( ML )
	cycle1003B.setMonsterLevelForEveryone( ML )
	cycle1003C.setMonsterLevelForEveryone( ML )
	cycle1003D.setMonsterLevelForEveryone( ML )
	labtechGroup1003.setMonsterLevelForEveryone( ML )
	cycle1005A.setMonsterLevelForEveryone( ML )
	cycle1005B.setMonsterLevelForEveryone( ML )
	cycle1005C.setMonsterLevelForEveryone( ML )
	cycle1005D.setMonsterLevelForEveryone( ML )
	labtechGroup1005.setMonsterLevelForEveryone( ML )
	rightToLeft.setMonsterLevelForEveryone( ML )
	leftToRight.setMonsterLevelForEveryone( ML )
	labtechGroup1004.setMonsterLevelForEveryone( ML )
	labtechCycles1004.setMonsterLevelForEveryone( ML )
	//Right-side Gauntlet
	gauntletPatrolA.setMonsterLevelForEveryone( ML )
	gauntletPatrolB.setMonsterLevelForEveryone( ML )
	gauntletPatrolC.setMonsterLevelForEveryone( ML )
	cycle906A.setMonsterLevelForEveryone( ML )
	cycle906B.setMonsterLevelForEveryone( ML )
	cycle906C.setMonsterLevelForEveryone( ML )
	cycle806A.setMonsterLevelForEveryone( ML )
	cycle806B.setMonsterLevelForEveryone( ML )
	cycle806C.setMonsterLevelForEveryone( ML )
	cycle706HiddenA.setMonsterLevelForEveryone( ML )
	labtechGroup706.setMonsterLevelForEveryone( ML )
	cycle706A.setMonsterLevelForEveryone( ML )
	cycle706B.setMonsterLevelForEveryone( ML )
	cycle706C.setMonsterLevelForEveryone( ML )
	cycle705A.setMonsterLevelForEveryone( ML )
	cycle705B.setMonsterLevelForEveryone( ML )
	//Fallen Gambino Tower
	rampBottom3.setMonsterLevelForEveryone( ML )
	patrolCycle1.setMonsterLevelForEveryone( ML )
	patrolCycle2.setMonsterLevelForEveryone( ML )
	patrolCycle3.setMonsterLevelForEveryone( ML )
	rampBottom1.setMonsterLevelForEveryone( ML )
	rampBottom2.setMonsterLevelForEveryone( ML )
	labtechGroup901.setMonsterLevelForEveryone( ML )
	grunnyGuard901.setMonsterLevelForEveryone( ML )
	cycle801A.setMonsterLevelForEveryone( ML )
	cycle801B.setMonsterLevelForEveryone( ML )
	cycle801C.setMonsterLevelForEveryone( ML )
	labtechGroup801.setMonsterLevelForEveryone( ML )
	grunnyGuard801.setMonsterLevelForEveryone( ML )
	grunnyGuards701.setMonsterLevelForEveryone( ML )
	//Switch702
	switch702Leader1.setMonsterLevelForEveryone( ML )
	switch702Guard1.setMonsterLevelForEveryone( ML )
	switch702Guard2.setMonsterLevelForEveryone( ML )
	switch702Guard3.setMonsterLevelForEveryone( ML )
	switch702Leader2.setMonsterLevelForEveryone( ML )
	switch702Group.setMonsterLevelForEveryone( ML )
	//Switch803 & surrounding posted guards
	switch803Leader1.setMonsterLevelForEveryone( ML )
	switch803Guard1.setMonsterLevelForEveryone( ML )
	switch803Guard2.setMonsterLevelForEveryone( ML )
	switch803Guard3.setMonsterLevelForEveryone( ML )
	labtechGroup703A.setMonsterLevelForEveryone( ML )
	labtechGroup703B.setMonsterLevelForEveryone( ML )
	labtechGroup804A.setMonsterLevelForEveryone( ML )
	labtechGroup804B.setMonsterLevelForEveryone( ML )
	labtechGroup704A.setMonsterLevelForEveryone( ML )
	labtechGroup704B.setMonsterLevelForEveryone( ML )
	//Switch606 recurring guards
	switch606Leader1.setMonsterLevelForEveryone( ML )
	switch606Guard1.setMonsterLevelForEveryone( ML )
	switch606Guard2.setMonsterLevelForEveryone( ML )
	switch606Guard3.setMonsterLevelForEveryone( ML )
	//Miscellaneous Small Groups behind tower
	labtechGroup604A.setMonsterLevelForEveryone( ML )
	labtechGroup604B.setMonsterLevelForEveryone( ML )
	cycle604A.setMonsterLevelForEveryone( ML )
	cycle604B.setMonsterLevelForEveryone( ML )
	labtechGroup605A.setMonsterLevelForEveryone( ML )
	labtechGroup605B.setMonsterLevelForEveryone( ML )
	labtechGroup703C.setMonsterLevelForEveryone( ML )
	labtechGroup704C.setMonsterLevelForEveryone( ML )
	labtechGroup704D.setMonsterLevelForEveryone( ML )
	labtechGroup705A.setMonsterLevelForEveryone( ML )
	labtechGroup805A.setMonsterLevelForEveryone( ML )
	labtechGroup805B.setMonsterLevelForEveryone( ML )
	//Labtech 123 Ambush Group
	ambushCycle1.setMonsterLevelForEveryone( ML )
	ambushCycle2.setMonsterLevelForEveryone( ML )
	ambushCycle3.setMonsterLevelForEveryone( ML )
	ambushTech1.setMonsterLevelForEveryone( ML )
	ambushTech2.setMonsterLevelForEveryone( ML )
	ambushTech3.setMonsterLevelForEveryone( ML )
	//Security Spawners
	security606Spawner.setMonsterLevelForEveryone( securityLevel )
	security702Spawner.setMonsterLevelForEveryone( securityLevel )
	security803Spawner.setMonsterLevelForEveryone( securityLevel )
	//Sealab Door Guards
	securityMainDoorPatrolLeader.setMonsterLevelForEveryone( ML )
	securityMainDoorPatrolGuards.setMonsterLevelForEveryone( ML )
	securityMainDoor903Pylon.setMonsterLevelForEveryone( ML )
	securityMainDoor905Pylon.setMonsterLevelForEveryone( ML )
	mainDoorGuardLeft.setMonsterLevelForEveryone( ML )
	mainDoorGuardRight.setMonsterLevelForEveryone( ML )
	mainDoorGuard1.setMonsterLevelForEveryone( ML )
	mainDoorGuard2.setMonsterLevelForEveryone( ML )
	mainDoorGuard3.setMonsterLevelForEveryone( ML )
}

//============================================
// TUNNEL SPAWNERS                            
//============================================

robo2a = myRooms.Sealab_2.spawnStoppedSpawner( "robo2a", "robofish", 1)
robo2a.setPos( 520, 210 )
robo2a.setGuardPostForChildren( robo2a, 135 )
robo2a.setMonsterLevelForChildren( ML )

robo2b = myRooms.Sealab_2.spawnStoppedSpawner( "robo2b", "robofish", 1)
robo2b.setPos( 785, 350 )
robo2b.setGuardPostForChildren( robo2b, 135 )
robo2b.setMonsterLevelForChildren( ML )

robo103a = myRooms.Sealab_103.spawnStoppedSpawner( "robo103a", "robofish", 1)
robo103a.setPos( 580, 430 )
robo103a.setGuardPostForChildren( robo103a, 135 )
robo103a.setMonsterLevelForChildren( ML )

robo103b = myRooms.Sealab_103.spawnStoppedSpawner( "robo103b", "robofish", 1)
robo103b.setPos( 800, 250 )
robo103b.setGuardPostForChildren( robo103b, 135 )
robo103b.setMonsterLevelForChildren( ML )

robo204a = myRooms.Sealab_204.spawnStoppedSpawner( "robo204a", "robofish", 1)
robo204a.setPos( 810, 550 )
robo204a.setGuardPostForChildren( robo204a, 135 )
robo204a.setMonsterLevelForChildren( ML )

robo204b = myRooms.Sealab_204.spawnStoppedSpawner( "robo204b", "robofish", 1)
robo204b.setPos( 850, 180 )
robo204b.setGuardPostForChildren( robo204b, 135 )
robo204b.setMonsterLevelForChildren( ML )

robo204c = myRooms.Sealab_204.spawnStoppedSpawner( "robo204c", "robofish", 1)
robo204c.setPos( 490, 245 )
robo204c.setGuardPostForChildren( robo204c, 135 )
robo204c.setMonsterLevelForChildren( ML )

robo305a = myRooms.Sealab_305.spawnStoppedSpawner( "robo305a", "robofish", 1)
robo305a.setPos( 630, 275 )
robo305a.setGuardPostForChildren( robo305a, 135 )
robo305a.setMonsterLevelForChildren( ML )

robo305b = myRooms.Sealab_305.spawnStoppedSpawner( "robo305b", "robofish", 1)
robo305b.setPos( 970, 450 )
robo305b.setGuardPostForChildren( robo305b, 135 )
robo305b.setMonsterLevelForChildren( ML )

robo405 = myRooms.Sealab_405.spawnStoppedSpawner( "robo405", "robofish", 1)
robo405.setPos( 970, 220 )
robo405.setGuardPostForChildren( robo405, 135 )
robo405.setMonsterLevelForChildren( ML )

robo406a = myRooms.Sealab_406.spawnStoppedSpawner( "robo406a", "robofish", 1)
robo406a.setPos( 840, 210 )
robo406a.setGuardPostForChildren( robo406a, 135 )
robo406a.setMonsterLevelForChildren( ML )

robo406b = myRooms.Sealab_406.spawnStoppedSpawner( "robo406b", "robofish", 1)
robo406b.setPos( 600, 500 )
robo406b.setGuardPostForChildren( robo406b, 135 )
robo406b.setMonsterLevelForChildren( ML )

robo507a = myRooms.Sealab_507.spawnStoppedSpawner( "robo507a", "robofish", 1)
robo507a.setPos( 400, 90 )
robo507a.setGuardPostForChildren( robo507a, 135 )
robo507a.setMonsterLevelForChildren( ML )

robo507b = myRooms.Sealab_507.spawnStoppedSpawner( "robo507b", "robofish", 1)
robo507b.setPos( 830, 80 )
robo507b.setGuardPostForChildren( robo507b, 135 )
robo507b.setMonsterLevelForChildren( ML )

grunny406a = myRooms.Sealab_406.spawnStoppedSpawner( "grunny406a", "grunny_sub", 1)
grunny406a.setPos( 410, 210 )
grunny406a.setWanderBehaviorForChildren( 75, 150, 4, 9, 200)
grunny406a.setCFH( 1000 )
grunny406a.setMonsterLevelForChildren( ML )

grunny406b = myRooms.Sealab_406.spawnStoppedSpawner( "grunny406b", "grunny_sub", 1)
grunny406b.setPos( 685, 430 )
grunny406b.setWanderBehaviorForChildren( 75, 150, 4, 9, 200)
grunny406b.setCFH( 1000 )
grunny406b.setMonsterLevelForChildren( ML )

spawnerList << robo2a
spawnerList << robo2b
spawnerList << robo103a
spawnerList << robo103b
spawnerList << robo204a
spawnerList << robo204b
spawnerList << robo204c
spawnerList << robo305a
spawnerList << robo305b
spawnerList << robo405
spawnerList << robo406a
spawnerList << robo406b
spawnerList << grunny406a
spawnerList << grunny406b
spawnerList << robo507a
spawnerList << robo507b

//ALLIANCES
robo2a.allyWithSpawner( robo2b )
robo103a.allyWithSpawner( robo103b )
robo204a.allyWithSpawner( robo204b )
robo204a.allyWithSpawner( robo204c )
robo204b.allyWithSpawner( robo204c )
robo305a.allyWithSpawner( robo305b )
robo305a.allyWithSpawner( robo405 )
robo305b.allyWithSpawner( robo405 )
robo406a.allyWithSpawner( grunny406a )
robo406a.allyWithSpawner( grunny406b )
robo406a.allyWithSpawner( robo406b )
robo406b.allyWithSpawner( grunny406a )
robo406b.allyWithSpawner( grunny406b )
robo507a.allyWithSpawner( robo507b )
grunny406a.allyWithSpawner( grunny406b )

tunnel402a = myRooms.Sealab_402.spawnStoppedSpawner( "tunnel402a", "labtech_female", 1)
tunnel402a.setPos( 870, 500 )
tunnel402a.setGuardPostForChildren( tunnel402a, 135 )
tunnel402a.setMonsterLevelForChildren( ML )

tunnel402b = myRooms.Sealab_402.spawnStoppedSpawner( "tunnel402b", "labtech_male", 1)
tunnel402b.setPos( 670, 605 )
tunnel402b.setGuardPostForChildren( tunnel402b, 135 )
tunnel402b.setMonsterLevelForChildren( ML )

tunnel403a = myRooms.Sealab_403.spawnStoppedSpawner( "tunnel403a", "labtech_male", 1)
tunnel403a.setPos( 460, 430 )
tunnel403a.setGuardPostForChildren( tunnel403a, 135 )
tunnel403a.setMonsterLevelForChildren( ML )

tunnel403b = myRooms.Sealab_403.spawnStoppedSpawner( "tunnel403b", "grunny_sub", 1)
tunnel403b.setPos( 640, 510 )
tunnel403b.setGuardPostForChildren( tunnel403b, 135 )
tunnel403b.setMonsterLevelForChildren( ML )

tunnel403c = myRooms.Sealab_403.spawnStoppedSpawner( "tunnel403c", "labtech_female", 1)
tunnel403c.setPos( 460, 585 )
tunnel403c.setGuardPostForChildren( tunnel403c, 135 )
tunnel403c.setMonsterLevelForChildren( ML )

spawnerList << tunnel402a
spawnerList << tunnel402b
spawnerList << tunnel403a
spawnerList << tunnel403b
spawnerList << tunnel403c

//ALLIANCES
tunnel402a.allyWithSpawner( tunnel402b )
tunnel403a.allyWithSpawner( tunnel403b )
tunnel403a.allyWithSpawner( tunnel403c )
tunnel403b.allyWithSpawner( tunnel403c )

//============================================
// SEALAB COMPLEX SPAWNERS                    
//============================================

//============================================
// THE "X" ROOMS                              
//============================================

// Room 1003
cycle1003A = myRooms.Sealab_1003.spawnStoppedSpawner( "cycle1003A", "labtech_seacycle", 1 )
cycle1003A.setPos( 1100, 390 )
cycle1003A.setGuardPostForChildren( cycle1003A, 315 )
cycle1003A.setMonsterLevelForChildren( ML )

cycle1003B = myRooms.Sealab_1003.spawnStoppedSpawner( "cycle1003B", "labtech_seacycle", 1 )
cycle1003B.setPos( 1100, 550 )
cycle1003B.setGuardPostForChildren( cycle1003B, 45 )
cycle1003B.setMonsterLevelForChildren( ML )

cycle1003C = myRooms.Sealab_1003.spawnStoppedSpawner( "cycle1003C", "labtech_seacycle", 1 )
cycle1003C.setPos( 720, 550 )
cycle1003C.setGuardPostForChildren( cycle1003C, 135 )
cycle1003C.setMonsterLevelForChildren( ML )

cycle1003D = myRooms.Sealab_1003.spawnStoppedSpawner( "cycle1003D", "labtech_seacycle", 1 )
cycle1003D.setPos( 720, 390 )
cycle1003D.setGuardPostForChildren( cycle1003D, 225 )
cycle1003D.setMonsterLevelForChildren( ML )

labtechGroup1003 = myRooms.Sealab_1003.spawnStoppedSpawner( "labtechGroup1003", "labtech_male", 4)
labtechGroup1003.setPos( 910, 465 )
labtechGroup1003.setWanderBehaviorForChildren( 100, 350, 3, 8, 400)
labtechGroup1003.setMonsterLevelForChildren( ML )

// Room 1005
cycle1005A = myRooms.Sealab_1005.spawnStoppedSpawner( "cycle1005A", "labtech_seacycle", 1 )
cycle1005A.setPos( 840, 390 )
cycle1005A.setGuardPostForChildren( cycle1005A, 315 )
cycle1005A.setMonsterLevelForChildren( ML )

cycle1005B = myRooms.Sealab_1005.spawnStoppedSpawner( "cycle1005B", "labtech_seacycle", 1 )
cycle1005B.setPos( 840, 540 )
cycle1005B.setGuardPostForChildren( cycle1005B, 45 )
cycle1005B.setMonsterLevelForChildren( ML )

cycle1005C = myRooms.Sealab_1005.spawnStoppedSpawner( "cycle1005C", "labtech_seacycle", 1 )
cycle1005C.setPos( 460, 540 )
cycle1005C.setGuardPostForChildren( cycle1005C, 135 )
cycle1005C.setMonsterLevelForChildren( ML )

cycle1005D = myRooms.Sealab_1005.spawnStoppedSpawner( "cycle1005D", "labtech_seacycle", 1 )
cycle1005D.setPos( 460, 390 )
cycle1005D.setGuardPostForChildren( cycle1005D, 225 )
cycle1005D.setMonsterLevelForChildren( ML )

labtechGroup1005 = myRooms.Sealab_1005.spawnStoppedSpawner( "labtechGroup1005", "labtech_male", 4 )
labtechGroup1005.setPos( 650, 470 )
labtechGroup1005.setWanderBehaviorForChildren( 100, 350, 3, 8, 400)
labtechGroup1005.setMonsterLevelForChildren( ML )

//THE BIG "X" PATROLS
rightToLeft = myRooms.Sealab_1005.spawnStoppedSpawner( "rightToLeft", "grunny_sub", 2 )
rightToLeft.setPos( 260, 70 )
rightToLeft.setBaseSpeed( 200 )
rightToLeft.setChildrenToFollow( rightToLeft )
rightToLeft.setMonsterLevelForChildren( ML )
rightToLeft.addPatrolPointForSpawner( "Sealab_1005", 260, 70, 0 )
rightToLeft.addPatrolPointForSpawner( "Sealab_1004", 770, 470, 0 )
rightToLeft.addPatrolPointForSpawner( "Sealab_1003", 1300, 855, 0 )
rightToLeft.addPatrolPointForSpawner( "Sealab_1004", 770, 470, 0 )
rightToLeft.addPatrolPointForSpawner( "Sealab_1005", 260, 70, 0 )
rightToLeft.startPatrol()

leftToRight = myRooms.Sealab_1003.spawnStoppedSpawner( "leftToRight", "grunny_sub", 2 )
leftToRight.setPos( 1300, 70 )
leftToRight.setBaseSpeed( 200 )
leftToRight.setChildrenToFollow( leftToRight )
leftToRight.setMonsterLevelForChildren( ML )
leftToRight.addPatrolPointForSpawner( "Sealab_1003", 1300, 70, 0 )
leftToRight.addPatrolPointForSpawner( "Sealab_1004", 770, 470, 0 )
leftToRight.addPatrolPointForSpawner( "Sealab_1005", 260, 855, 0 )
leftToRight.addPatrolPointForSpawner( "Sealab_1004", 770, 470, 0 )
leftToRight.addPatrolPointForSpawner( "Sealab_1003", 1300, 70, 0 )
leftToRight.startPatrol()

//BIG "X" Reserve Groups
labtechGroup1004 = myRooms.Sealab_1004.spawnStoppedSpawner( "labtechGroup1004", "labtech_female", 4 )
labtechGroup1004.setPos( 775, 70 )
labtechGroup1004.setWanderBehaviorForChildren( 75, 150, 3, 8, 200)
labtechGroup1004.setMonsterLevelForChildren( ML )

labtechCycles1004 = myRooms.Sealab_1004.spawnStoppedSpawner( "labtechCycles1004", "labtech_seacycle", 3 )
labtechCycles1004.setPos( 775, 875 )
labtechCycles1004.setWanderBehaviorForChildren( 100, 200, 5, 15, 300)
labtechCycles1004.setMonsterLevelForChildren( ML )

spawnerList << cycle1003A
spawnerList << cycle1003B
spawnerList << cycle1003C
spawnerList << cycle1003D
spawnerList << labtechGroup1003
spawnerList << cycle1005A
spawnerList << cycle1005B
spawnerList << cycle1005C
spawnerList << cycle1005D
spawnerList << labtechGroup1005
spawnerList << rightToLeft
spawnerList << leftToRight
spawnerList << labtechGroup1004
spawnerList << labtechCycles1004

//ALLIANCES
cycle1003A.allyWithSpawner( cycle1003B )
cycle1003A.allyWithSpawner( cycle1003C )
cycle1003A.allyWithSpawner( cycle1003D )
cycle1003B.allyWithSpawner( cycle1003C )
cycle1003B.allyWithSpawner( cycle1003D )
cycle1003C.allyWithSpawner( cycle1003D )
labtechGroup1003.allyWithSpawner( cycle1003A )
labtechGroup1003.allyWithSpawner( cycle1003B )
labtechGroup1003.allyWithSpawner( cycle1003C )
labtechGroup1003.allyWithSpawner( cycle1003D )

cycle1005A.allyWithSpawner( cycle1005B )
cycle1005A.allyWithSpawner( cycle1005C )
cycle1005A.allyWithSpawner( cycle1005D )
cycle1005B.allyWithSpawner( cycle1005C )
cycle1005B.allyWithSpawner( cycle1005D )
cycle1005C.allyWithSpawner( cycle1005D )
labtechGroup1005.allyWithSpawner( cycle1005A )
labtechGroup1005.allyWithSpawner( cycle1005B )
labtechGroup1005.allyWithSpawner( cycle1005C )
labtechGroup1005.allyWithSpawner( cycle1005D )

rightToLeft.allyWithSpawner( leftToRight )
rightToLeft.allyWithSpawner( labtechGroup1004 )
rightToLeft.allyWithSpawner( labtechCycles1004 )
leftToRight.allyWithSpawner( labtechGroup1004 )
leftToRight.allyWithSpawner( labtechCycles1004 )
labtechGroup1004.allyWithSpawner( labtechCycles1004 )

//============================================
// RIGHT-SIDE GAUNTLET                        
//============================================

gauntletPatrolA = myRooms.Sealab_905.spawnStoppedSpawner( "gauntletPatrolA", "labtech_seacycle", 2 )
gauntletPatrolA.setPos( 1400, 610 )
gauntletPatrolA.setBaseSpeed( 200 )
gauntletPatrolA.setSpawnWhenPlayersAreInRoom( false, 10 )
gauntletPatrolA.setChildrenToFollow( gauntletPatrolA )
gauntletPatrolA.setMonsterLevelForChildren( ML )
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_905", 1400, 610, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_906", 385, 555, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_906", 725, 220, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_806", 495, 400, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_706", 540, 440, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_806", 495, 400, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_906", 725, 220, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_906", 385, 555, 0)
gauntletPatrolA.addPatrolPointForSpawner( "Sealab_905", 1400, 610, 0)
gauntletPatrolA.startPatrol()

gauntletPatrolB = myRooms.Sealab_806.spawnStoppedSpawner( "gauntletPatrolB", "labtech_seacycle", 2 )
gauntletPatrolB.setPos( 240, 680 )
gauntletPatrolB.setBaseSpeed( 200 )
gauntletPatrolB.setSpawnWhenPlayersAreInRoom( false, 10 )
gauntletPatrolB.setChildrenToFollow( gauntletPatrolB )
gauntletPatrolB.setMonsterLevelForChildren( ML )
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_905", 1400, 610, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_906", 385, 555, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_906", 725, 220, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_806", 495, 400, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_706", 540, 440, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_806", 495, 400, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_906", 725, 220, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_906", 385, 555, 0)
gauntletPatrolB.addPatrolPointForSpawner( "Sealab_905", 1400, 610, 0)
gauntletPatrolB.startPatrol()

gauntletPatrolC = myRooms.Sealab_706.spawnStoppedSpawner( "gauntletPatrolC", "labtech_seacycle", 2 )
gauntletPatrolC.setPos( 205, 800 )
gauntletPatrolC.setBaseSpeed( 200 )
gauntletPatrolC.setSpawnWhenPlayersAreInRoom( false, 10 )
gauntletPatrolC.setChildrenToFollow( gauntletPatrolC )
gauntletPatrolC.setMonsterLevelForChildren( ML )
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_905", 1400, 610, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_906", 385, 555, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_906", 725, 220, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_806", 495, 400, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_706", 540, 440, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_806", 495, 400, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_906", 725, 220, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_906", 385, 555, 0)
gauntletPatrolC.addPatrolPointForSpawner( "Sealab_905", 1400, 610, 0)
gauntletPatrolC.startPatrol()

spawnerList << gauntletPatrolA
spawnerList << gauntletPatrolB
spawnerList << gauntletPatrolC

//============================================
// AMBUSH SPAWNERS IN GAUNTLET                
//============================================
//hidden behind objects to leap out at players as they pass by

cycle906A = myRooms.Sealab_906.spawnStoppedSpawner( "cycle906A", "grunny_sub", 1 )
cycle906A.setPos( 690, 800 )
cycle906A.setGuardPostForChildren( cycle906A, 225 )
cycle906A.setMonsterLevelForChildren( ML )

cycle906B = myRooms.Sealab_906.spawnStoppedSpawner( "cycle906B", "grunny_sub", 1 )
cycle906B.setPos( 835, 700 )
cycle906B.setGuardPostForChildren( cycle906B, 225 )
cycle906B.setMonsterLevelForChildren( ML )

cycle906C = myRooms.Sealab_906.spawnStoppedSpawner( "cycle906C", "grunny_sub", 1 )
cycle906C.setPos( 1140, 195 )
cycle906C.setGuardPostForChildren( cycle906C, 180 )
cycle906C.setMonsterLevelForChildren( ML )

cycle806A = myRooms.Sealab_806.spawnStoppedSpawner( "cycle806A", "grunny_sub", 1 )
cycle806A.setPos( 240, 680 )
cycle806A.setGuardPostForChildren( cycle806A, 10 )
cycle806A.setMonsterLevelForChildren( ML )

cycle806B = myRooms.Sealab_806.spawnStoppedSpawner( "cycle806B", "grunny_sub", 1 )
cycle806B.setPos( 235, 170 )
cycle806B.setGuardPostForChildren( cycle806B, 10 )
cycle806B.setMonsterLevelForChildren( ML )

cycle806C = myRooms.Sealab_806.spawnStoppedSpawner( "cycle806C", "grunny_sub", 1 )
cycle806C.setPos( 680, 60 )
cycle806C.setGuardPostForChildren( cycle806C, 170 )
cycle806C.setMonsterLevelForChildren( ML )

cycle706HiddenA = myRooms.Sealab_706.spawnStoppedSpawner( "cycle706HiddenA", "grunny_sub", 1 )
cycle706HiddenA.setPos( 205, 800 )
cycle706HiddenA.setGuardPostForChildren( cycle706HiddenA, 315 )
cycle706HiddenA.setMonsterLevelForChildren( ML )

//Rooms 705 & 706
labtechGroup706 = myRooms.Sealab_706.spawnStoppedSpawner( "labtechGroup706", "labtech_male", 4 )
labtechGroup706.setPos( 935, 155 )
labtechGroup706.setWanderBehaviorForChildren( 100, 350, 3, 8, 400)
labtechGroup706.setMonsterLevelForChildren( ML )

cycle706A = myRooms.Sealab_706.spawnStoppedSpawner( "cycle706A", "labtech_seacycle", 1 )
cycle706A.setPos( 150, 160 )
cycle706A.setGuardPostForChildren( cycle706A, 85 )
cycle706A.setMonsterLevelForChildren( ML )

cycle706B = myRooms.Sealab_706.spawnStoppedSpawner( "cycle706B", "labtech_seacycle", 1 )
cycle706B.setPos( 350, 160 )
cycle706B.setGuardPostForChildren( cycle706B, 85 )
cycle706B.setMonsterLevelForChildren( ML )

cycle706C = myRooms.Sealab_706.spawnStoppedSpawner( "cycle706C", "labtech_seacycle", 1 )
cycle706C.setPos( 550, 160 )
cycle706C.setGuardPostForChildren( cycle706C, 85 )
cycle706C.setMonsterLevelForChildren( ML )

cycle705A = myRooms.Sealab_705.spawnStoppedSpawner( "cycle705A", "labtech_seacycle", 1 )
cycle705A.setPos( 1200, 190 )
cycle705A.setGuardPostForChildren( cycle705A, 135 )
cycle705A.setMonsterLevelForChildren( ML )

cycle705B = myRooms.Sealab_705.spawnStoppedSpawner( "cycle705B", "labtech_seacycle", 1 )
cycle705B.setPos( 1350, 280 )
cycle705B.setGuardPostForChildren( cycle705B, 135 )
cycle705B.setMonsterLevelForChildren( ML )

spawnerList << cycle906A
spawnerList << cycle906B
spawnerList << cycle906C
spawnerList << cycle806A
spawnerList << cycle806B
spawnerList << cycle806C
spawnerList << cycle706HiddenA
spawnerList << labtechGroup706
spawnerList << cycle706A
spawnerList << cycle706B
spawnerList << cycle706C
spawnerList << cycle705A
spawnerList << cycle705B

//ALLIANCES
gauntletPatrolA.allyWithSpawner( gauntletPatrolB )
gauntletPatrolA.allyWithSpawner( gauntletPatrolC )
gauntletPatrolB.allyWithSpawner( gauntletPatrolC )

labtechGroup706.allyWithSpawner( cycle706A )
labtechGroup706.allyWithSpawner( cycle706B )
labtechGroup706.allyWithSpawner( cycle706C )

cycle706A.allyWithSpawner( cycle706B )
cycle706A.allyWithSpawner( cycle706C )
cycle706B.allyWithSpawner( cycle706C )

cycle705A.allyWithSpawner( cycle705B )

//============================================
// FALLEN GAMBINO TOWER AREA                  
//============================================

// Room 1002
patrolCycle1 = myRooms.Sealab_1002.spawnStoppedSpawner( "patrolCycle1", "labtech_seacycle", 1 )
patrolCycle1.setPos( 920, 600 )
patrolCycle1.addPatrolPointForChildren( "Sealab_1002", 700, 690, 0)
patrolCycle1.addPatrolPointForChildren( "Sealab_1002", 1130, 440, 0)
patrolCycle1.setMonsterLevelForChildren( ML )

patrolCycle2 = myRooms.Sealab_1002.spawnStoppedSpawner( "patrolCycle2", "labtech_seacycle", 1 )
patrolCycle2.setPos( 1420, 260 )
patrolCycle2.addPatrolPointForChildren( "Sealab_1002", 1390, 530, 0)
patrolCycle2.addPatrolPointForChildren( "Sealab_902", 1190, 800, 0)
patrolCycle2.setMonsterLevelForChildren( ML )

patrolCycle3 = myRooms.Sealab_1002.spawnStoppedSpawner( "patrolCycle3", "labtech_seacycle", 1 )
patrolCycle3.setPos( 400, 750 )
patrolCycle3.addPatrolPointForChildren( "Sealab_1002", 670, 720, 0)
patrolCycle3.addPatrolPointForChildren( "Sealab_1001", 1300, 720, 0)
patrolCycle3.setMonsterLevelForChildren( ML )

spawnerList << patrolCycle1
spawnerList << patrolCycle2
spawnerList << patrolCycle3

patrolCycle1.allyWithSpawner( patrolCycle2 )
patrolCycle1.allyWithSpawner( patrolCycle3 )
patrolCycle2.allyWithSpawner( patrolCycle3 )

// Room 1001
rampBottom1 = myRooms.Sealab_1001.spawnStoppedSpawner( "rampBottom1", "labtech_seacycle", 1 )
rampBottom1.setPos( 540, 810 )
rampBottom1.setGuardPostForChildren( rampBottom1, 45 )
rampBottom1.setMonsterLevelForChildren( ML )

rampBottom2 = myRooms.Sealab_1001.spawnStoppedSpawner( "rampBottom2", "labtech_seacycle", 1)
rampBottom2.setPos( 760, 660 )
rampBottom2.setGuardPostForChildren( rampBottom2, 45 )
rampBottom2.setMonsterLevelForChildren( ML )

rampBottom3 = myRooms.Sealab_1001.spawnStoppedSpawner( "rampBottom3", "labtech_seacycle", 1)
rampBottom3.setPos( 550, 520 )
rampBottom3.setGuardPostForChildren( rampBottom3, 45 )
rampBottom3.setMonsterLevelForChildren( ML )

//Room 901
labtechGroup901 = myRooms.Sealab_901.spawnStoppedSpawner( "labtechGroup901", "labtech_male", 6 )
labtechGroup901.setPos( 185, 410 )
labtechGroup901.setWanderBehaviorForChildren( 100, 250, 3, 8, 350)
labtechGroup901.setMonsterLevelForChildren( ML )

grunnyGuard901 = myRooms.Sealab_901.spawnStoppedSpawner( "grunnyGuard901", "grunny_sub_LT", 1 )
grunnyGuard901.setPos( 1160, 790 )
grunnyGuard901.setGuardPostForChildren( grunnyGuard901, 135 )
grunnyGuard901.setHome( labtechGroup901 )
grunnyGuard901.setRFH( true )
grunnyGuard901.setDispositionForChildren( "coward" )
grunnyGuard901.setCowardLevelForChildren( 100 )
grunnyGuard901.setMonsterLevelForChildren( ML )

//Room 801
cycle801A = myRooms.Sealab_801.spawnStoppedSpawner( "cycle801A", "labtech_seacycle", 1 )
cycle801A.setPos( 310, 335 )
cycle801A.setGuardPostForChildren( cycle801A, 45 )
cycle801A.setMonsterLevelForChildren( ML )

cycle801B = myRooms.Sealab_801.spawnStoppedSpawner( "cycle801B", "labtech_seacycle", 1 )
cycle801B.setPos( 420, 365 )
cycle801B.setGuardPostForChildren( cycle801B, 45 )
cycle801B.setMonsterLevelForChildren( ML )

cycle801C = myRooms.Sealab_801.spawnStoppedSpawner( "cycle801C", "labtech_seacycle", 1 )
cycle801C.setPos( 560, 405 )
cycle801C.setGuardPostForChildren( cycle801C, 45 )
cycle801C.setMonsterLevelForChildren( ML )

labtechGroup801 = myRooms.Sealab_801.spawnStoppedSpawner( "labtechGroup801", "labtech_male", 5 )
labtechGroup801.setPos( 1300, 320 )
labtechGroup801.setWanderBehaviorForChildren( 100, 250, 3, 8, 350)
labtechGroup801.setMonsterLevelForChildren( ML )

grunnyGuard801 = myRooms.Sealab_801.spawnStoppedSpawner( "grunnyGuard801", "grunny_sub_LT", 1 )
grunnyGuard801.setPos( 740, 720 )
grunnyGuard801.setGuardPostForChildren( grunnyGuard801, 70 )
grunnyGuard801.setHome( labtechGroup801 )
grunnyGuard801.setRFH( true )
grunnyGuard801.setDispositionForChildren( "coward" )
grunnyGuard801.setCowardLevelForChildren( 100 )
grunnyGuard801.setMonsterLevelForChildren( ML )

//Room 701
grunnyGuards701 = myRooms.Sealab_701.spawnStoppedSpawner( "grunnyGuards701", "grunny_sub_LT", 3 )
grunnyGuards701.setPos( 1090, 730 )
grunnyGuards701.setWanderBehaviorForChildren( 75, 200, 2, 5, 300)
grunnyGuards701.setMonsterLevelForChildren( ML )

spawnerList << rampBottom1
spawnerList << rampBottom2
spawnerList << rampBottom3
spawnerList << labtechGroup901
spawnerList << grunnyGuard901
spawnerList << cycle801A
spawnerList << cycle801B
spawnerList << cycle801C
spawnerList << labtechGroup901
spawnerList << grunnyGuard801
spawnerList << grunnyGuards701

//ALLIANCES
rampBottom1.allyWithSpawner( rampBottom2 )
rampBottom1.allyWithSpawner( rampBottom3 )
rampBottom2.allyWithSpawner( rampBottom3 )
labtechGroup901.allyWithSpawner( grunnyGuard901 )
cycle801A.allyWithSpawner( cycle801B )
cycle801A.allyWithSpawner( cycle801C )
cycle801B.allyWithSpawner( cycle801C )
labtechGroup801.allyWithSpawner( cycle801A )
labtechGroup801.allyWithSpawner( cycle801B )
labtechGroup801.allyWithSpawner( cycle801C )
grunnyGuard801.allyWithSpawner( cycle801A )
grunnyGuard801.allyWithSpawner( cycle801B )
grunnyGuard801.allyWithSpawner( cycle801C )
labtechGroup801.allyWithSpawner( grunnyGuard801 )

//================================
// SWITCH 702 GUARDIANS           
//================================
//Leader and three guards spawn every 2-3 minutes to harass anyone sticking around to trigger the security switch
//They come in from off-screen to reposition at the switch
//Other guards and leaders just spawn once and don't respawn.
switch702Leader1 = myRooms.Sealab_703.spawnStoppedSpawner( "switch702Leader1", "labtech_female", 1 )
switch702Leader1.setPos( 200, 330 )
switch702Leader1.setGuardPostForChildren( "Sealab_702", 170, 440, 135 )
switch702Leader1.setMonsterLevelForChildren( ML )

switch702Guard1 = myRooms.Sealab_703.spawnStoppedSpawner( "switch702Guard1", "labtech_male", 1 )
switch702Guard1.setPos( 200, 330 )
switch702Guard1.setGuardPostForChildren( "Sealab_702", 240, 350, 135 )
switch702Guard1.setMonsterLevelForChildren( ML )

switch702Guard2 = myRooms.Sealab_703.spawnStoppedSpawner( "switch702Guard2", "labtech_male", 1 )
switch702Guard2.setPos( 200, 330 )
switch702Guard2.setGuardPostForChildren( "Sealab_702", 160, 240, 135 )
switch702Guard2.setMonsterLevelForChildren( ML )

switch702Guard3 = myRooms.Sealab_703.spawnStoppedSpawner( "switch702Guard3", "labtech_male", 1 )
switch702Guard3.setPos( 200, 330 )
switch702Guard3.setGuardPostForChildren( "Sealab_702", 390, 310, 135 )
switch702Guard3.setMonsterLevelForChildren( ML )

spawnerList << switch702Leader1
spawnerList << switch702Guard1
spawnerList << switch702Guard2
spawnerList << switch702Guard3

switch702GuardList << switch702Leader1
switch702GuardList << switch702Guard1
switch702GuardList << switch702Guard2
switch702GuardList << switch702Guard3

def spawnSwitch702Guards() {
	respawn702Started = false
	adjustML()
	
	leader702 = switch702Leader1.forceSpawnNow()
	leader702.setDisplayName( "Security Chief" )
	myManager.onAwarenessIn( leader702 ) { event ->
		if( isPlayer( event.actor ) ) {
			leader702.say( "Stand away from that Security Switch!" )
		}
	}
	guard702a = switch702Guard1.forceSpawnNow()
	guard702a.setDisplayName( "Switch Guard" )
	
	guard702b = switch702Guard2.forceSpawnNow()
	guard702b.setDisplayName( "Switch Guard" )
	
	guard702c = switch702Guard3.forceSpawnNow()
	guard702c.setDisplayName( "Switch Guard" )
	
	runOnDeath( leader702 ) { check702GuardRespawn() }
	runOnDeath( guard702a ) { check702GuardRespawn() }
	runOnDeath( guard702b ) { check702GuardRespawn() }
	runOnDeath( guard702c ) { check702GuardRespawn() }
}

def synchronized check702GuardRespawn() {
	if( leader702.isDead() && guard702a.isDead() && guard702b.isDead() && guard702c.isDead() && respawn702Started == false ) {
		respawn702Started = true
		if( sealabDoorUnlocked == false ) { myManager.schedule( random( 60, 90 ) ) { spawnSwitch702Guards() } }
	}
}

switch702Leader2 = myRooms.Sealab_702.spawnStoppedSpawner( "switch702Leader1", "labtech_female", 1 )
switch702Leader2.setPos( 520, 520 )
switch702Leader2.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
switch702Leader2.setMonsterLevelForChildren( ML )

switch702Group = myRooms.Sealab_702.spawnStoppedSpawner( "switch702Guard1", "labtech_male", 3 )
switch702Group.setPos( 1000, 420 )
switch702Group.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
switch702Group.setMonsterLevelForChildren( ML )

spawnerList << switch702Leader2
spawnerList << switch702Group

//ALLIANCES
switch702Leader1.allyWithSpawner( switch702Guard1 )
switch702Leader1.allyWithSpawner( switch702Guard2 )
switch702Leader1.allyWithSpawner( switch702Guard3 )
switch702Guard1.allyWithSpawner( switch702Guard2 )
switch702Guard1.allyWithSpawner( switch702Guard3 )
switch702Guard2.allyWithSpawner( switch702Guard3 )
switch702Leader1.allyWithSpawner( switch702Leader2 )	
switch702Leader1.allyWithSpawner( switch702Group )	
switch702Leader2.allyWithSpawner( switch702Group )
switch702Leader2.allyWithSpawner( switch702Guard1 )
switch702Leader2.allyWithSpawner( switch702Guard2 )
switch702Leader2.allyWithSpawner( switch702Guard3 )
switch702Group.allyWithSpawner( switch702Guard1 )
switch702Group.allyWithSpawner( switch702Guard2 )
switch702Group.allyWithSpawner( switch702Guard3 )


//================================
// SWITCH 803 GUARDIANS           
//================================
//Leader and three guards spawn every 2-3 minutes to harass anyone sticking around to trigger the security switch
//They come in from off-screen to reposition at the switch
//Other guards and leaders just spawn once and don't respawn.

switch803Leader1 = myRooms.Sealab_804.spawnStoppedSpawner( "switch803Leader1", "labtech_female", 1 )
switch803Leader1.setPos( 270, 660 )
switch803Leader1.setGuardPostForChildren( "Sealab_803", 1390, 460, 315 )
switch803Leader1.setMonsterLevelForChildren( ML )

switch803Guard1 = myRooms.Sealab_804.spawnStoppedSpawner( "switch803Guard1", "labtech_male", 1 )
switch803Guard1.setPos( 270, 660 )
switch803Guard1.setGuardPostForChildren( "Sealab_803", 1440, 600, 0 )
switch803Guard1.setMonsterLevelForChildren( ML )

switch803Guard2 = myRooms.Sealab_804.spawnStoppedSpawner( "switch803Guard2", "labtech_male", 1 )
switch803Guard2.setPos( 270, 660 )
switch803Guard2.setGuardPostForChildren( "Sealab_803", 1230, 500, 270 )
switch803Guard2.setMonsterLevelForChildren( ML )

switch803Guard3 = myRooms.Sealab_804.spawnStoppedSpawner( "switch803Guard3", "labtech_male", 1 )
switch803Guard3.setPos( 270, 660 )
switch803Guard3.setGuardPostForChildren( "Sealab_803", 1090, 580, 180 )
switch803Guard3.setMonsterLevelForChildren( ML )

spawnerList << switch803Leader1
spawnerList << switch803Guard1
spawnerList << switch803Guard2
spawnerList << switch803Guard3

switch803GuardList << switch803Leader1
switch803GuardList << switch803Guard1
switch803GuardList << switch803Guard2
switch803GuardList << switch803Guard3

def spawnSwitch803Guards() {
	respawn803Started = false
	adjustML()
	
	leader803 = switch803Leader1.forceSpawnNow()
	leader803.setDisplayName( "Security Chief" )
	myManager.onAwarenessIn( leader803 ) { event ->
		if( isPlayer( event.actor ) ) {
			leader803.say( "Back away! That switch is off-limits!" )
		}
	}
	guard803a = switch803Guard1.forceSpawnNow()
	guard803a.setDisplayName( "Switch Guard" )
	
	guard803b = switch803Guard2.forceSpawnNow()
	guard803b.setDisplayName( "Switch Guard" )
	
	guard803c = switch803Guard3.forceSpawnNow()
	guard803c.setDisplayName( "Switch Guard" )
	
	runOnDeath( leader803 ) { check803GuardRespawn() }
	runOnDeath( guard803a ) { check803GuardRespawn() }
	runOnDeath( guard803b ) { check803GuardRespawn() }
	runOnDeath( guard803c ) { check803GuardRespawn() }
}

def synchronized check803GuardRespawn() {
	if( leader803.isDead() && guard803a.isDead() && guard803b.isDead() && guard803c.isDead() && respawn803Started == false ) {
		respawn803Started = true
		if( sealabDoorUnlocked == false ) { myManager.schedule( random( 60, 90 ) ) { spawnSwitch803Guards() } }
	}
}


//ALLIANCES
switch702Leader1.allyWithSpawner( switch702Guard1 )
switch702Leader1.allyWithSpawner( switch702Guard2 )
switch702Leader1.allyWithSpawner( switch702Guard3 )
switch702Guard1.allyWithSpawner( switch702Guard2 )
switch702Guard1.allyWithSpawner( switch702Guard3 )
switch702Guard2.allyWithSpawner( switch702Guard3 )

//SURROUNDING ROOMS AROUND 803
labtechGroup703A = myRooms.Sealab_703.spawnStoppedSpawner( "labtechGroup703A", "labtech_female", 1 )
labtechGroup703A.setPos( 1270, 850 )
labtechGroup703A.setGuardPostForChildren( "Sealab_703", 1270, 850, 270 )
labtechGroup703A.setMonsterLevelForChildren( ML )

labtechGroup703B = myRooms.Sealab_703.spawnStoppedSpawner( "labtechGroup703B", "labtech_male", 1 )
labtechGroup703B.setPos( 1440, 850 )
labtechGroup703B.setGuardPostForChildren( "Sealab_703", 1440, 850, 315 )
labtechGroup703B.setMonsterLevelForChildren( ML )

spawnerList << labtechGroup703A
spawnerList << labtechGroup703B

labtechGroup703A.allyWithSpawner( labtechGroup703B )

labtechGroup704A = myRooms.Sealab_704.spawnStoppedSpawner( "labtechGroup704A", "labtech_female", 1 )
labtechGroup704A.setPos( 50, 760 )
labtechGroup704A.setGuardPostForChildren( "Sealab_704", 50, 760, 315 )
labtechGroup704A.setMonsterLevelForChildren( ML )

labtechGroup704B = myRooms.Sealab_704.spawnStoppedSpawner( "labtechGroup704B", "labtech_male", 1 )
labtechGroup704B.setPos( 250, 890 )
labtechGroup704B.setGuardPostForChildren( "Sealab_704", 250, 890, 315 )
labtechGroup704B.setMonsterLevelForChildren( ML )

spawnerList << labtechGroup704A
spawnerList << labtechGroup704B

labtechGroup704A.allyWithSpawner( labtechGroup704B )

labtechGroup804A = myRooms.Sealab_804.spawnStoppedSpawner( "labtechGroup804A", "labtech_male", 1 )
labtechGroup804A.setPos( 120, 170 )
labtechGroup804A.setGuardPostForChildren( "Sealab_804", 120, 170, 315 )
labtechGroup804A.setMonsterLevelForChildren( ML )

labtechGroup804B = myRooms.Sealab_804.spawnStoppedSpawner( "labtechGroup804B", "labtech_female", 1 )
labtechGroup804B.setPos( 120, 420 )
labtechGroup804B.setGuardPostForChildren( "Sealab_804", 120, 420, 0 )
labtechGroup804B.setMonsterLevelForChildren( ML )

spawnerList << labtechGroup804A
spawnerList << labtechGroup804B

labtechGroup804A.allyWithSpawner( labtechGroup804B )

//------------------------------------------
// Small Groups of Labtechs in Back Rooms   
//------------------------------------------

labtechGroup604A = myRooms.Sealab_604.spawnStoppedSpawner( "labtechGroup604A", "labtech_male", 4 )
labtechGroup604A.setPos( 400, 800 )
labtechGroup604A.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup604A.setMonsterLevelForChildren( ML )

labtechGroup604B = myRooms.Sealab_604.spawnStoppedSpawner( "labtechGroup604B", "labtech_male", 3 )
labtechGroup604B.setPos( 1130, 620 )
labtechGroup604B.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup604B.setMonsterLevelForChildren( ML )

cycle604A = myRooms.Sealab_604.spawnStoppedSpawner( "cycle604A", "labtech_seacycle", 1 )
cycle604A.setPos( 620, 600 )
cycle604A.setGuardPostForChildren( cycle604A, 45 )
cycle604A.setMonsterLevelForChildren( ML )

cycle604B = myRooms.Sealab_604.spawnStoppedSpawner( "cycle604B", "labtech_seacycle", 1 )
cycle604B.setPos( 770, 520 )
cycle604B.setGuardPostForChildren( cycle604B, 45 )
cycle604B.setMonsterLevelForChildren( ML )

spawnerList << labtechGroup604A
spawnerList << labtechGroup604B
spawnerList << cycle604A
spawnerList << cycle604B

//alliances for room 604
labtechGroup604A.allyWithSpawner( labtechGroup604B )
labtechGroup604A.allyWithSpawner( cycle604A )
labtechGroup604A.allyWithSpawner( cycle604B )
labtechGroup604B.allyWithSpawner( cycle604A )
labtechGroup604B.allyWithSpawner( cycle604B )
cycle604A.allyWithSpawner( cycle604B )

labtechGroup605A = myRooms.Sealab_605.spawnStoppedSpawner( "labtechGroup605A", "labtech_male", 3 )
labtechGroup605A.setPos( 1130, 620 )
labtechGroup605A.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup605A.setMonsterLevelForChildren( ML )

labtechGroup605B = myRooms.Sealab_605.spawnStoppedSpawner( "labtechGroup605B", "labtech_male", 3 )
labtechGroup605B.setPos( 340, 740 )
labtechGroup605B.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup605B.setMonsterLevelForChildren( ML )

labtechGroup703C = myRooms.Sealab_703.spawnStoppedSpawner( "labtechGroup703C", "labtech_male", 3 )
labtechGroup703C.setPos( 1350, 360 )
labtechGroup703C.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup703C.setMonsterLevelForChildren( ML )

labtechGroup704C = myRooms.Sealab_704.spawnStoppedSpawner( "labtechGroup704C", "labtech_male", 3 )
labtechGroup704C.setPos( 1340, 620 )
labtechGroup704C.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup704C.setMonsterLevelForChildren( ML )

labtechGroup704D = myRooms.Sealab_704.spawnStoppedSpawner( "labtechGroup704D", "labtech_male", 3 )
labtechGroup704D.setPos( 160, 260 )
labtechGroup704D.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup704D.setMonsterLevelForChildren( ML )

labtechGroup705A = myRooms.Sealab_705.spawnStoppedSpawner( "labtechGroup705A", "labtech_male", 3 )
labtechGroup705A.setPos( 680, 620 )
labtechGroup705A.setWanderBehaviorForChildren( 75, 175, 3, 8, 400)
labtechGroup705A.setMonsterLevelForChildren( ML )

labtechGroup805A = myRooms.Sealab_805.spawnStoppedSpawner( "labtechGroup805A", "labtech_female", 2 )
labtechGroup805A.setPos( 300, 410 )
labtechGroup805A.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup805A.setMonsterLevelForChildren( ML )

labtechGroup805B = myRooms.Sealab_805.spawnStoppedSpawner( "labtechGroup805B", "labtech_male", 3 )
labtechGroup805B.setPos( 875, 400 )
labtechGroup805B.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
labtechGroup805B.setMonsterLevelForChildren( ML )

spawnerList << labtechGroup605A
spawnerList << labtechGroup605B
spawnerList << labtechGroup703C
spawnerList << labtechGroup704C
spawnerList << labtechGroup704D
spawnerList << labtechGroup705A
spawnerList << labtechGroup805A
spawnerList << labtechGroup805B

//-------------------------------------------
// AMBUSH SPAWNERS                           
//-------------------------------------------
ambushCycle1 = myRooms.Sealab_605.spawnStoppedSpawner( "ambushCycle1", "labtech_seacycle", 1 )
ambushCycle1.setPos( 510, 540 )
ambushCycle1.setHomeTetherForChildren( 3000 )
ambushCycle1.setHateRadiusForChildren( 3000 )
ambushCycle1.setMonsterLevelForChildren( ML )

ambushCycle2 = myRooms.Sealab_1002.spawnStoppedSpawner( "ambushCycle2", "labtech_seacycle", 1 )
ambushCycle2.setPos( 125, 510 )
ambushCycle2.setHomeTetherForChildren( 3000 )
ambushCycle2.setHateRadiusForChildren( 3000 )
ambushCycle2.setMonsterLevelForChildren( ML )

ambushCycle3 = myRooms.Sealab_804.spawnStoppedSpawner( "ambushCycle3", "labtech_seacycle", 1 )
ambushCycle3.setPos( 1150, 470 )
ambushCycle3.setHomeTetherForChildren( 3000 )
ambushCycle3.setHateRadiusForChildren( 3000 )
ambushCycle3.setMonsterLevelForChildren( ML )

ambushTech1 = myRooms.Sealab_904.spawnStoppedSpawner( "ambushTech1", "labtech_female", 1 )
ambushTech1.setPos( 770, 880 )
ambushTech1.setHomeTetherForChildren( 3000 )
ambushTech1.setHateRadiusForChildren( 3000 )
ambushTech1.setMonsterLevelForChildren( ML )

ambushTech2 = myRooms.Sealab_903.spawnStoppedSpawner( "ambushTech2", "labtech_male", 1 )
ambushTech2.setPos( 540, 780 )
ambushTech2.setHomeTetherForChildren( 3000 )
ambushTech2.setHateRadiusForChildren( 3000 )
ambushTech2.setBaseSpeed( 120 );
ambushTech2.setMonsterLevelForChildren( ML )

ambushTech3 = myRooms.Sealab_905.spawnStoppedSpawner( "ambushTech3", "labtech_male", 1 )
ambushTech3.setPos( 1020, 780 )
ambushTech3.setHomeTetherForChildren( 3000 )
ambushTech3.setHateRadiusForChildren( 3000 )
ambushTech3.setBaseSpeed( 120 );
ambushTech3.setMonsterLevelForChildren( ML )

spawnerList << ambushCycle1
spawnerList << ambushCycle2
spawnerList << ambushCycle3
spawnerList << ambushTech1
spawnerList << ambushTech2
spawnerList << ambushTech3

//-------------------------------------------
// SECURITY SPAWNERS & MAIN DOOR DEFENSES    
//-------------------------------------------

security606Spawner = myRooms.Sealab_605.spawnStoppedSpawner( "security606Spawner", "labtech_seacycle", 12 )
security606Spawner.setPos( 510, 540 )
security606Spawner.setHomeTetherForChildren( 3000 )
security606Spawner.setBaseSpeed( 280 )
security606Spawner.setChildrenToFollow( security606Spawner );
security606Spawner.setMonsterLevelForChildren( ML )
security606Spawner.addPatrolPointForSpawner( "Sealab_605", 1200, 575, 0 )
security606Spawner.addPatrolPointForSpawner( "Sealab_606", 200, 700, 0 )
security606Spawner.addPatrolPointForSpawner( "Sealab_606", 440, 900, 0 )
security606Spawner.addPatrolPointForSpawner( "Sealab_706", 620, 230, 0 )
security606Spawner.addPatrolPointForSpawner( "Sealab_706", 150, 470, 0 )
security606Spawner.addPatrolPointForSpawner( "Sealab_705", 1225, 210, 0 )

security702Spawner = myRooms.Sealab_702.spawnStoppedSpawner( "security702Spawner", "labtech_seacycle", 12 )
security702Spawner.setPos( 1500, 350 )
security702Spawner.setHomeTetherForChildren( 3000 )
security702Spawner.setBaseSpeed( 280 )
security702Spawner.setChildrenToFollow( security702Spawner );
security702Spawner.setMonsterLevelForChildren( ML )
security702Spawner.addPatrolPointForSpawner( "Sealab_702", 280, 480, 0 )
security702Spawner.addPatrolPointForSpawner( "Sealab_702", 1120, 720, 0 )
security702Spawner.addPatrolPointForSpawner( "Sealab_702", 1500, 350, 0 )

security803Spawner = myRooms.Sealab_804.spawnStoppedSpawner( "security803Spawner", "labtech_seacycle", 12 )
security803Spawner.setPos( 285, 615 )
security803Spawner.setHomeTetherForChildren( 3000 )
security803Spawner.setBaseSpeed( 280 );
security803Spawner.setChildrenToFollow( security803Spawner );
security803Spawner.setMonsterLevelForChildren( ML )
security803Spawner.addPatrolPointForSpawner( "Sealab_803", 1270, 550, 0 )
security803Spawner.addPatrolPointForSpawner( "Sealab_703", 1370, 550, 0 )
security803Spawner.addPatrolPointForSpawner( "Sealab_704", 200, 770, 0 )
security803Spawner.addPatrolPointForSpawner( "Sealab_804", 125, 325, 0 )

spawnerList << security606Spawner
spawnerList << security702Spawner
spawnerList << security803Spawner

//MAIN SEALAB DOOR GROUPS
securityMainDoorPatrolLeader = myRooms.Sealab_904.spawnStoppedSpawner( "securityMainDoorPatrolLeader", "labtech_female", 1 )
securityMainDoorPatrolLeader.setPos( 800, 370 )
securityMainDoorPatrolLeader.setBaseSpeed( 180 )
securityMainDoorPatrolLeader.setMonsterLevelForChildren( ML )
securityMainDoorPatrolLeader.setChildrenToFollow( securityMainDoorPatrolLeader )

securityMainDoorPatrolGuards = myRooms.Sealab_904.spawnStoppedSpawner( "securityMainDoorPatrolGuards", "labtech_male", 3 )
securityMainDoorPatrolGuards.setPos( 800, 370 )
securityMainDoorPatrolGuards.setBaseSpeed( 180 );
securityMainDoorPatrolGuards.setMonsterLevelForChildren( ML )
securityMainDoorPatrolGuards.setChildrenToFollow( securityMainDoorPatrolLeader )

securityMainDoor903Pylon = myRooms.Sealab_903.spawnStoppedSpawner( "securityMainDoor903Pylon", "labtech_male", 4 )
securityMainDoor903Pylon.setPos( 880, 670 )
securityMainDoor903Pylon.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
securityMainDoor903Pylon.setMonsterLevelForChildren( ML )

securityMainDoor905Pylon = myRooms.Sealab_905.spawnStoppedSpawner( "securityMainDoor905Pylon", "labtech_male", 3 )
securityMainDoor905Pylon.setPos( 700, 660 )
securityMainDoor905Pylon.setWanderBehaviorForChildren( 75, 175, 3, 8, 300)
securityMainDoor905Pylon.setMonsterLevelForChildren( ML )

mainDoorGuardLeft = myRooms.Sealab_904.spawnStoppedSpawner( "mainDoorGuardLeft", "labtech_seacycle", 1 )
mainDoorGuardLeft.setPos( 800, 370 )
mainDoorGuardLeft.setGuardPostForChildren( "Sealab_904", 585, 490, 100 )
mainDoorGuardLeft.setMonsterLevelForChildren( ML )

mainDoorGuardRight = myRooms.Sealab_904.spawnStoppedSpawner( "mainDoorGuardRight", "labtech_seacycle", 1 )
mainDoorGuardRight.setPos( 800, 370 )
mainDoorGuardRight.setGuardPostForChildren( "Sealab_904", 995, 500, 80 )
mainDoorGuardRight.setMonsterLevelForChildren( ML )

mainDoorGuard1 = myRooms.Sealab_904.spawnStoppedSpawner( "mainDoorGuard1", "labtech_female", 1 )
mainDoorGuard1.setPos( 800, 370 )
mainDoorGuard1.setGuardPostForChildren( "Sealab_904", 780, 630, 90 )
mainDoorGuard1.setMonsterLevelForChildren( ML )

mainDoorGuard2 = myRooms.Sealab_904.spawnStoppedSpawner( "mainDoorGuard2", "labtech_male", 1 )
mainDoorGuard2.setPos( 800, 370 )
mainDoorGuard2.setGuardPostForChildren( "Sealab_904", 440, 650, 135 )
mainDoorGuard2.setMonsterLevelForChildren( ML )

mainDoorGuard3 = myRooms.Sealab_904.spawnStoppedSpawner( "mainDoorGuard3", "labtech_male", 1 )
mainDoorGuard3.setPos( 800, 370 )
mainDoorGuard3.setGuardPostForChildren( "Sealab_904", 1150, 670, 45 )
mainDoorGuard3.setMonsterLevelForChildren( ML )

spawnerList << securityMainDoorPatrolLeader
spawnerList << securityMainDoorPatrolGuards
spawnerList << securityMainDoor903Pylon
spawnerList << securityMainDoor905Pylon
spawnerList << mainDoorGuardLeft
spawnerList << mainDoorGuardRight
spawnerList << mainDoorGuard1
spawnerList << mainDoorGuard2
spawnerList << mainDoorGuard3

def spawnMainDoorGuards() {
	adjustML()
	//spawn Seacycles first
	seaLabDoor.on()
	myManager.schedule(3) { 
		mainDoorGuardLeft.forceSpawnNow()
		mainDoorGuardRight.forceSpawnNow()
		myManager.schedule(3) { seaLabDoor.off() }
	
		//then spawn foot soldier guards
		myManager.schedule(4) { 
			seaLabDoor.on()
			myManager.schedule(3) {
				leaderMainDoor = mainDoorGuard1.forceSpawnNow()
				leaderMainDoor.setDisplayName( "NeXuS" )
				guardMainDoorA = mainDoorGuard2.forceSpawnNow()
				guardMainDoorA.setDisplayName( "Tower Guard" )
				guardMainDoorB = mainDoorGuard3.forceSpawnNow()
				guardMainDoorB.setDisplayName( "Tower Guard" )
				myManager.schedule(3) { seaLabDoor.off() }

				//then spawn the patrol
				myManager.schedule(4) {
					seaLabDoor.on()
					myManager.schedule(3) {
						securityMainDoorPatrolLeader.warp( "Sealab_904", 800, 370 )
						securityMainDoorPatrolGuards.warp( "Sealab_904", 800, 370 )
						securityMainDoorPatrolLeader.setHome( "Sealab_904", 800, 370 )
						securityMainDoorPatrolGuards.setHome( "Sealab_904", 800, 370 )
						securityMainDoorPatrolLeader.spawnAllNow()
						securityMainDoorPatrolGuards.spawnAllNow()
						mainDoorPatrol = makeNewPatrol()
						mainDoorPatrol.addPatrolPoint( "Sealab_903", 810, 765, 0 )
						mainDoorPatrol.addPatrolPoint( "Sealab_904", 770, 880, 0 )
						mainDoorPatrol.addPatrolPoint( "Sealab_905", 650, 765, 0 )
						mainDoorPatrol.addPatrolPoint( "Sealab_904", 770, 880, 0 )
						mainDoorPatrol.addPatrolPoint( "Sealab_903", 810, 765, 0 )
						securityMainDoorPatrolLeader.setPatrol( mainDoorPatrol )
						securityMainDoorPatrolLeader.startPatrol()
						myManager.schedule(3) { seaLabDoor.off() }

						//now watch for their deaths
						mainDoorSpawnsInProgress = false
						watchForMainDoorRespawns()
					}
				}
			}
		}
	}
}

def watchForMainDoorRespawns() {
	if( mainDoorSpawnsInProgress == true ) return
	if( sealabDoorUnlocked == true ) return
	totalMainDoorSpawns = mainDoorGuardLeft.spawnsInUse() + mainDoorGuardRight.spawnsInUse() + mainDoorGuard1.spawnsInUse() + mainDoorGuard2.spawnsInUse() + mainDoorGuard3.spawnsInUse() + securityMainDoorPatrolLeader.spawnsInUse() + securityMainDoorPatrolGuards.spawnsInUse()
	
	if( totalMainDoorSpawns == 0 ) {
		mainDoorSpawnsInProgress = true
		myManager.schedule( random( 5, 10 ) ) { spawnMainDoorGuards() }
	} else {
		mainDoorSpawnsInProgress = false
		myManager.schedule(2) { watchForMainDoorRespawns() }
	}
}
			
//ALLIANCES
securityMainDoorPatrolLeader.allyWithSpawner( securityMainDoorPatrolGuards )
securityMainDoorPatrolLeader.allyWithSpawner( mainDoorGuardLeft )
securityMainDoorPatrolLeader.allyWithSpawner( mainDoorGuardRight )
securityMainDoorPatrolLeader.allyWithSpawner( mainDoorGuard1 )
securityMainDoorPatrolLeader.allyWithSpawner( mainDoorGuard2 )
securityMainDoorPatrolLeader.allyWithSpawner( mainDoorGuard3 )
securityMainDoorPatrolGuards.allyWithSpawner( mainDoorGuardLeft )
securityMainDoorPatrolGuards.allyWithSpawner( mainDoorGuardRight )
securityMainDoorPatrolGuards.allyWithSpawner( mainDoorGuard1 )
securityMainDoorPatrolGuards.allyWithSpawner( mainDoorGuard2 )
securityMainDoorPatrolGuards.allyWithSpawner( mainDoorGuard3 )
mainDoorGuardLeft.allyWithSpawner( mainDoorGuardRight )
mainDoorGuardLeft.allyWithSpawner( mainDoorGuard1 )
mainDoorGuardLeft.allyWithSpawner( mainDoorGuard2 )
mainDoorGuardLeft.allyWithSpawner( mainDoorGuard3 )
mainDoorGuardRight.allyWithSpawner( mainDoorGuard1 )
mainDoorGuardRight.allyWithSpawner( mainDoorGuard2 )
mainDoorGuardRight.allyWithSpawner( mainDoorGuard3 )
mainDoorGuard1.allyWithSpawner( mainDoorGuard2 )
mainDoorGuard1.allyWithSpawner( mainDoorGuard3 )
mainDoorGuard2.allyWithSpawner( mainDoorGuard3 )


//============================================
//============================================

//============================================
// TUNNEL ENTRANCE LOGIC                      
//============================================
onEnterTunnel = new Object()

myManager.onEnter( myRooms.Sealab_1) { event ->
	synchronized( onEnterTunnel ) {
		if( isPlayer( event.actor ) ) {
			//DIFFICULTY
			//The first player into the scenario sets the difficulty for everyone else
			if( difficulty == null ) {
				difficulty = event.actor.getTeam().getAreaVar( "Z11_Shallow_Sea", "Z11ShallowSeaDifficulty" )
				if( difficulty == 0 ) { difficulty = 1; println "**** difficulty changed to 1 ****" }
			}			
			//When the first actor enters the undersea caves, check all crew members and set the scenario's level to the highest player's CL
			if( tunnelSpawned == false ) {
				tunnelSpawned = true
				adjustML()
				tunnelSpawn()
			}	
		}
	}
}

def tunnelSpawn() {
	zoneBroadcast("Marshall-VQS", "They Are Watching", "Lab personnel will report anyone attempting to engage the main lock override. Thin their numbers before trying to prevent a security lockdown.")
	
	robo2a.forceSpawnNow()
	robo2b.forceSpawnNow()
	robo103a.forceSpawnNow()
	robo103b.forceSpawnNow()
	robo204a.forceSpawnNow()
	robo204b.forceSpawnNow()
	robo204c.forceSpawnNow()
	robo305a.forceSpawnNow()
	robo305b.forceSpawnNow()
	robo405.forceSpawnNow()
	robo406a.forceSpawnNow()
	robo406b.forceSpawnNow()
	robo507a.forceSpawnNow()
	robo507b.forceSpawnNow()
	
	grunny406a.forceSpawnNow()
	grunny406b.forceSpawnNow()
	
	tunnel402a.forceSpawnNow()
	tunnel402b.forceSpawnNow()
	tunnel403a.forceSpawnNow()
	tunnel403b.forceSpawnNow()
	tunnel403c.forceSpawnNow()
}	

//============================================
// ROBOFISH LOGIC                             
//============================================

firstSpawn2 = false
playerSet2 = [] as Set

myManager.onEnter( myRooms.Sealab_2 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet2 << event.actor
		if( firstSpawn2 == false ) {
			firstSpawn2 = true
			myManager.schedule(2) {
				if( playerSet2.isEmpty() == false ) {
					myRooms.Sealab_2.getActorList().each { if( isMonster( it ) ) { it.addHate( random( playerSet2 ), 100 ) } }
				}
			}
		} else { 
			myRooms.Sealab_2.getActorList().each{
				if( isMonster( it ) && it.getHated().isEmpty() ) {
					it.addHate( random( playerSet2 ), 100 ) 
				}
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_2 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet2.remove( event.actor )
	}
}

firstSpawn103 = false
playerSet103 = [] as Set

myManager.onEnter( myRooms.Sealab_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet103 << event.actor
		if( firstSpawn103 == false ) {
			firstSpawn103 = true
			myManager.schedule(2) {
				if( playerSet103.isEmpty() == false ) {
					myRooms.Sealab_103.getActorList().each { if( isMonster( it ) ) { it.addHate( random( playerSet103 ), 100 ) } }
				}
			}
		} else { 
			myRooms.Sealab_103.getActorList().each{
				if( isMonster( it ) && it.getHated().isEmpty() ) {
					it.addHate( random( playerSet103 ), 100 ) 
				}
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet103.remove( event.actor )
	}
}

firstSpawn204 = false
playerSet204 = [] as Set

myManager.onEnter( myRooms.Sealab_204 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet204 << event.actor
		if( firstSpawn204 == false ) {
			firstSpawn204 = true
			myManager.schedule(2) {
				if( playerSet204.isEmpty() == false ) {
					myRooms.Sealab_204.getActorList().each { if( isMonster( it ) ) { it.addHate( random( playerSet204 ), 100 ) } }
				}
			}
		} else { 
			myRooms.Sealab_204.getActorList().each{
				if( isMonster( it ) && it.getHated().isEmpty() ) {
					it.addHate( random( playerSet204 ), 100 ) 
				}
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_204 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet204.remove( event.actor )
	}
}

firstSpawn305 = false
playerSet305 = [] as Set

myManager.onEnter( myRooms.Sealab_305 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet305 << event.actor
		if( firstSpawn305 == false ) {
			firstSpawn305 = true
			myManager.schedule(2) {
				if( playerSet305.isEmpty() == false ) {
					myRooms.Sealab_305.getActorList().each { if( isMonster( it ) ) { it.addHate( random( playerSet305 ), 100 ) } }
				}
			}
		} else { 
			myRooms.Sealab_305.getActorList().each{
				if( isMonster( it ) && it.getHated().isEmpty() ) {
					it.addHate( random( playerSet305 ), 100 ) 
				}
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_305 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet305.remove( event.actor )
	}
}

firstSpawn405 = false
playerSet405 = [] as Set

myManager.onEnter( myRooms.Sealab_405 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet405 << event.actor
		if( firstSpawn405 == false ) {
			firstSpawn405 = true
			myManager.schedule(2) {
				if( playerSet405.isEmpty() == false ) {
					myRooms.Sealab_405.getActorList().each { if( isMonster( it ) ) { it.addHate( random( playerSet405 ), 100 ) } }
				}
			}
		} else { 
			myRooms.Sealab_405.getActorList().each{
				if( isMonster( it ) && it.getHated().isEmpty() ) {
					it.addHate( random( playerSet405 ), 100 ) 
				}
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_405 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet405.remove( event.actor )
	}
}

firstSpawn406 = false
playerSet406 = [] as Set

myManager.onEnter( myRooms.Sealab_406 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet406 << event.actor
		if( firstSpawn406 == false ) {
			firstSpawn406 = true
			myManager.schedule(2) {
				if( playerSet406.isEmpty() == false ) {
					myRooms.Sealab_406.getActorList().each { if( isMonster( it ) ) { it.addHate( random( playerSet406 ), 100 ) } }
				}
			}
		} else { 
			myRooms.Sealab_406.getActorList().each{
				if( isMonster( it ) && it.getHated().isEmpty() ) {
					it.addHate( random( playerSet406 ), 100 ) 
				}
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_406 ) { event ->
	playerSet406.remove( event.actor )
}

firstSpawn507 = false
playerSet507 = [] as Set

myManager.onEnter( myRooms.Sealab_507 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet507 << event.actor
		if( firstSpawn507 == false ) {
			firstSpawn507 = true
			myManager.schedule(2) {
				if( playerSet507.isEmpty() == false ) {
					myRooms.Sealab_507.getActorList().each { if( isMonster( it ) ) { it.addHate( random( playerSet507 ), 100 ) } }
				}
			}
		} else { 
			myRooms.Sealab_507.getActorList().each{
				if( isMonster( it ) && it.getHated().isEmpty() ) {
					it.addHate( random( playerSet507 ), 100 ) 
				}
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_507 ) { event ->
	playerSet507.remove( event.actor )
}

//------------------------------------------
//Marshall for broadcast                    
//------------------------------------------
Marshall = spawnNPC("Marshall-VQS", myRooms.Sealab_1, 450, 490)
Marshall.setDisplayName("Marshall")

marshallFarewell = Marshall.createConversation("marshallFarewell", true)

marshallFarewell1 = [id:1]
marshallFarewell1.npctext = "Well, this is as far as I can accompany you. The Lab is far too dangerous for my old bones."
marshallFarewell1.result = 2
marshallFarewell.addDialog(marshallFarewell1, Marshall)

marshallFarewell2 = [id:2]
marshallFarewell2.npctext = "I wish you luck, you will need it. Farewell."
marshallFarewell2.result = DONE
marshallFarewell.addDialog(marshallFarewell2, Marshall)

//------------------------------------------
// LABTECH 123's TAUNTING SPEECH (Tunnel)   
//------------------------------------------

Labtech123Tunnel = spawnNPC("under123-VQS", myRooms.Sealab_403, 830, 510)
Labtech123Tunnel.setRotation( 135 )
Labtech123Tunnel.setDisplayName( "Labtech 123" )

labtechTauntCompleted = false

myManager.onEnter( myRooms.Sealab_403 ) { event ->
	if( isPlayer( event.actor ) && labtechTauntCompleted == false ) {
		labtechTauntCompleted = true
		myManager.schedule(3) { Labtech123Tunnel.say( "So...the gibberish about intruders that the grunnies were gabbling about was true. You even made it past the robofish. Impressive!" ) }
		myManager.schedule(6) { Labtech123Tunnel.say( "All ultimately futile, of course. Even if you make it past these defenders, our Sealab forces grossly outnumber you." ) }
		myManager.schedule(10) { Labtech123Tunnel.say( "Ta, ta for now! Your doom awaits...so don't tarry too long!" ) }
		myManager.schedule(12) { Labtech123Tunnel.dispose() }
	}
}

//============================================
// COMPLEX ENTRANCE LOGIC                     
//============================================

onEnterComplex = new Object()

myManager.onEnter( myRooms.Sealab_1006) { event ->
	synchronized( onEnterComplex ) {
		if( isPlayer( event.actor ) ) {
			//safety net to get rid of old pda map flags
			if( !event.actor.hasQuestFlag( GLOBAL, "Z13OldFlagsRemoved" ) ) {
				event.actor.removeMiniMapQuestLocation( "switch606Flag" )
				event.actor.removeMiniMapQuestLocation( "switch702Flag" )
				event.actor.removeMiniMapQuestLocation( "switch803Flag" )
				event.actor.setQuestFlag( GLOBAL, "Z13OldFlagsRemoved" )
			}
			
			if( complexEntered == false ) {
				complexEntered = true
				adjustML()
				spawnComplex()
				addMiniMapMarker( "newSwitch606Flag", "markerChickenWell", "Sealab_606", 425, 540, "Security Switch" )
				addMiniMapMarker( "newSwitch702Flag", "markerChickenWell", "Sealab_702", 280, 295, "Security Switch" )
				addMiniMapMarker( "newSwitch803Flag", "markerChickenWell", "Sealab_803", 1290, 610, "Security Switch" )
			}

		}
	}
}

def spawnComplex() {
	//Big "X" Rooms
	cycle1003A.spawnAllNow()
	cycle1003B.spawnAllNow()
	cycle1003C.spawnAllNow()
	cycle1003D.spawnAllNow()
	labtechGroup1003.spawnAllNow()
	cycle1005A.spawnAllNow()
	cycle1005B.spawnAllNow()
	cycle1005C.spawnAllNow()
	cycle1005D.spawnAllNow()
	labtechGroup1005.spawnAllNow()
	rightToLeft.spawnAllNow()
	leftToRight.spawnAllNow()
	labtechGroup1004.spawnAllNow()
	labtechCycles1004.spawnAllNow()
	//Right-side Gauntlet
	gauntletPatrolA.spawnAllNow()
	gauntletPatrolB.spawnAllNow()
	gauntletPatrolC.spawnAllNow()
	cycle906A.spawnAllNow()
	cycle906B.spawnAllNow()
	cycle906C.spawnAllNow()
	cycle806A.spawnAllNow()
	cycle806B.spawnAllNow()
	cycle806C.spawnAllNow()
	cycle706HiddenA.spawnAllNow()
	labtechGroup706.spawnAllNow()
	cycle706A.spawnAllNow()
	cycle706B.spawnAllNow()
	cycle706C.spawnAllNow()
	cycle705A.spawnAllNow()
	cycle705B.spawnAllNow()
	//Fallen Gambino Tower
	rampBottom3.spawnAllNow()
	patrolCycle1.spawnAllNow()
	patrolCycle2.spawnAllNow()
	patrolCycle3.spawnAllNow()
	rampBottom1.spawnAllNow()
	rampBottom2.spawnAllNow()
	labtechGroup901.spawnAllNow()
	grunnyGuard901.spawnAllNow()
	cycle801A.spawnAllNow()
	cycle801B.spawnAllNow()
	cycle801C.spawnAllNow()
	labtechGroup801.spawnAllNow()
	grunnyGuard801.spawnAllNow()
	grunnyGuards701.spawnAllNow()
	//Switch702
	spawnSwitch702Guards()
	switch702Leader2.spawnAllNow()
	switch702Group.spawnAllNow()
	//Switch803
	spawnSwitch803Guards()
	labtechGroup703A.spawnAllNow()
	labtechGroup703B.spawnAllNow()
	labtechGroup804A.spawnAllNow()
	labtechGroup804B.spawnAllNow()
	labtechGroup704A.spawnAllNow()
	labtechGroup704B.spawnAllNow()
	//Small group spawners (behind Sealab Tower)
	labtechGroup604A.spawnAllNow()
	labtechGroup604B.spawnAllNow()
	cycle604A.spawnAllNow()
	cycle604B.spawnAllNow()
	labtechGroup605A.spawnAllNow()
	labtechGroup605B.spawnAllNow()
	labtechGroup703C.spawnAllNow()
	labtechGroup704C.spawnAllNow()
	labtechGroup704D.spawnAllNow()
	labtechGroup705A.spawnAllNow()
	labtechGroup805A.spawnAllNow()
	labtechGroup805B.spawnAllNow()
	//Sealab Main Door Guards
	spawnMainDoorGuards()
	securityMainDoor903Pylon.spawnAllNow()
	securityMainDoor905Pylon.spawnAllNow()
}

//-------------------------------------------
// SEALAB SECURITY SWITCH LOGIC              
//-------------------------------------------

// The main door switch can only be opened   
// after the other three switches are thrown.
// But those three switches may be thrown    
// in any order.                             

switch606 = makeSwitch( "switch606", myRooms.Sealab_606, 425, 540 )
switch702 = makeSwitch( "switch702", myRooms.Sealab_702, 280, 295 )
switch803 = makeSwitch( "switch803", myRooms.Sealab_803, 1290, 610 )
switch904 = makeSwitch( "switch904", myRooms.Sealab_904, 505, 775 )
seaLabDoor = makeSwitch( "seaLabDoor", myRooms.Sealab_904, 300, 300 )

switch606.setRange( 250 )
switch702.setRange( 250 )
switch803.setRange( 250 )
switch904.setRange( 250 )
seaLabDoor.setRange( 250 )

switch606.unlock()
switch702.unlock()
switch803.unlock()
switch606.off()
switch702.off()
switch803.off()

switch904.unlock()
seaLabDoor.lock()

seaLabDoor.off()

seaLabDoor.setUsable( false )

sealabDoorUnlocked = false
canEnterSealab = false
alreadyChecking = false
doorOpen = false

playerSet = [] as Set

def enterSealabX = "enterSealabX"
myRooms.Sealab_904.createTriggerZone(enterSealabX, 600, 280, 1000, 590)

myManager.onTriggerIn(myRooms.Sealab_904, enterSealabX) { event ->
	if( isPlayer(event.actor) && canEnterSealab == true) {
		event.actor.warp( "Sealab_106", 160, 825 )
	}
}

// Each reset routine resets the switch in six seconds, and checks the state of all three preliminary switches to determine whether the fourth should be unlocked or not
// Players need to throw all three switches within <SWITCH_TIMER> seconds of each other, or they will reset and the main door will not be unlocked.
// Players can't try again for 60 seconds (switch won't reset for that amount of time)

def check606 = { event ->
	switch606.lock()
	if( alreadyChecking == false ) {
		alreadyChecking = true
		myManager.schedule(SWITCH_TIMER) { checkSwitch904Status() }
	}
}

def check702 = { event ->
	switch702.lock()
	if( alreadyChecking == false ) {
		alreadyChecking = true
		myManager.schedule(SWITCH_TIMER) { checkSwitch904Status() }
	}
}

def check803 = { event ->
	switch803.lock()
	if( alreadyChecking == false ) {
		alreadyChecking = true
		myManager.schedule(SWITCH_TIMER) { checkSwitch904Status() }
	}
}
	
def findPlayersInComplex() {
	playersInComplex.clear()
	myRooms.Sealab_603.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_604.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_605.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_606.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_701.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_702.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_703.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_704.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_705.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_706.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_801.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_802.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_803.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_804.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_805.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_806.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_901.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_902.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_903.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_904.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_905.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_906.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_1001.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_1002.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_1003.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_1004.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_1005.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
	myRooms.Sealab_1006.getActorList().each { if( isPlayer( it ) ) { playersInComplex << it } }
}

def alarmClaxons() {
	sound("Alarm1").toZone()
	sound("Alarm2").toZone()
	sound("Alarm3").toZone()
	sound("Alarm4").toZone()
	sound("Alarm5").toZone()
	sound("Alarm6").toZone()
	sound("Alarm7").toZone()
}

def synchronized checkSwitch904Status() {
	//println "****SWITCH STATES : 606 = ${switch606.isOn()}; 702 = ${switch702.isOn()}; 803 = ${switch803.isOn()} ****"
	if( switch606.isOn() && switch702.isOn() && switch803.isOn() ) {
		if(monsterCounter > ENTRY_LIMIT) {
			findPlayersInComplex()
			playersInComplex.each{ it.centerPrint( "MAIN LOCK OVERRIDE SUCCESSFUL" ) }

			//start the alarms, lock the switches, and remove the minimap flags
			alarmClaxons()
			switch606.lock(); switch702.lock(); switch803.lock()
			removeMiniMapMarker("newSwitch606Flag")
			removeMiniMapMarker("newSwitch702Flag")
			removeMiniMapMarker("newSwitch803Flag")

			//summon security
			spawnSecurity()

			myManager.schedule(2) { playerSet.each{ it.centerPrint( "COMPLEX PERSONNEL REPORTING INTRUDERS" ) } }
			myManager.schedule(4) { playerSet.each{ it.centerPrint( "MANUAL LOCK OVERRIDE ENGAGED" ) } }
			myManager.schedule(6) { playerSet.each{ it.centerPrint( "60-SECOND SECURITY LOCKOUT ON ALL SWITCHES IN PROCESS" ) } }

			//continue the alarms once every 10 seconds until reset
			myManager.schedule(10) { alarmClaxons() }
			myManager.schedule(20) { alarmClaxons() }
			myManager.schedule(30) { alarmClaxons() }
			myManager.schedule(40) { alarmClaxons() }
			myManager.schedule(50) { alarmClaxons() }

			//reset the security systems
			myManager.schedule(60) {
				switch606.unlock(); switch606.off()
				switch702.unlock(); switch702.off()
				switch803.unlock(); switch803.off()
				playerSet.each{ it.centerPrint( "SECURITY ALERT CANCELLED...STAND DOWN" ) }			

				//dipose of the security forces
				sec606Set.each{ if( !it.isDead() && !it.isAggro() ) { it.instantPercentDamage( 100 ) } }
				sec702Set.each{ if( !it.isDead() && !it.isAggro() ) { it.instantPercentDamage( 100 ) } }
				sec803Set.each{ if( !it.isDead() && !it.isAggro() ) { it.instantPercentDamage( 100 ) } }

				sec606Set.clear()
				sec702Set.clear()
				sec803Set.clear()

				addMiniMapMarker( "newSwitch606Flag", "markerChickenWell", "Sealab_606", 425, 540, "Security Switch" )
				addMiniMapMarker( "newSwitch702Flag", "markerChickenWell", "Sealab_702", 280, 295, "Security Switch" )
				addMiniMapMarker( "newSwitch803Flag", "markerChickenWell", "Sealab_803", 1290, 610, "Security Switch" )

				alreadyChecking = false
			}
		} else {
			sealabDoorUnlocked = true
		
			//lock the three security switches so they can't be activated again
			switch606.lock(); switch702.lock(); switch803.lock() 
		
			findPlayersInComplex()
			playersInComplex.each{ it.centerPrint( "MAIN LOCK OVERRIDE SUCCESSFUL" ) }
			removeMiniMapMarker("newSwitch606Flag")
			removeMiniMapMarker("newSwitch702Flag")
			removeMiniMapMarker("newSwitch803Flag")

			addMiniMapMarker( "switch904Flag", "markerChickenWell", "Sealab_904", 505, 775, "Sealab Main Door" )
		}
	} else {
		findPlayersInComplex()
		playersInComplex.each{ it.centerPrint( "MAIN LOCK OVERRIDE UNSUCCESSFUL" ) }
	
		//start the alarms, lock the switches, and remove the minimap flags
		alarmClaxons()
		switch606.lock(); switch702.lock(); switch803.lock()
		removeMiniMapMarker("newSwitch606Flag")
		removeMiniMapMarker("newSwitch702Flag")
		removeMiniMapMarker("newSwitch803Flag")
	
		//summon security
		spawnSecurity()

		myManager.schedule(2) { playerSet.each{ it.centerPrint( "ALL THREE FAILSAFES MUST BE THROWN AT ONCE TO AVOID MAIN DOOR LOCKDOWN" ) } }
		myManager.schedule(4) { playerSet.each{ it.centerPrint( "SECURITY FORCES HAVE BEEN SUMMONED" ) } }
		myManager.schedule(6) { playerSet.each{ it.centerPrint( "60-SECOND SECURITY LOCKOUT ON ALL SWITCHES IN PROCESS" ) } }
	
		//continue the alarms once every 10 seconds until reset
		myManager.schedule(10) { alarmClaxons() }
		myManager.schedule(20) { alarmClaxons() }
		myManager.schedule(30) { alarmClaxons() }
		myManager.schedule(40) { alarmClaxons() }
		myManager.schedule(50) { alarmClaxons() }
	
		//reset the security systems
		myManager.schedule(60) {
			switch606.unlock(); switch606.off()
			switch702.unlock(); switch702.off()
			switch803.unlock(); switch803.off()
			playerSet.each{ it.centerPrint( "SECURITY ALERT CANCELLED...STAND DOWN" ) }			
		
			//dipose of the security forces
			sec606Set.each{ if( !it.isDead() && !it.isAggro() ) { it.instantPercentDamage( 100 ) } }
			sec702Set.each{ if( !it.isDead() && !it.isAggro() ) { it.instantPercentDamage( 100 ) } }
			sec803Set.each{ if( !it.isDead() && !it.isAggro() ) { it.instantPercentDamage( 100 ) } }
		
			sec606Set.clear()
			sec702Set.clear()
			sec803Set.clear()
		
			addMiniMapMarker( "newSwitch606Flag", "markerChickenWell", "Sealab_606", 425, 540, "Security Switch" )
			addMiniMapMarker( "newSwitch702Flag", "markerChickenWell", "Sealab_702", 280, 295, "Security Switch" )
			addMiniMapMarker( "newSwitch803Flag", "markerChickenWell", "Sealab_803", 1290, 610, "Security Switch" )
		
			alreadyChecking = false
		}
	} 
}


def spawnSecurity() {
	adjustML()
	if( (playersIn606.size() * 1.5).intValue() < 2 ) { numSpawns606 = 2 } else { numSpawns606 = (playersIn606.size() * 1.5).intValue() }
	if( (playersIn702.size() * 1.5).intValue() < 2 ) { numSpawns702 = 2 } else { numSpawns702 = (playersIn702.size() * 1.5).intValue() }
	if( (playersIn803.size() * 1.5).intValue() < 2 ) { numSpawns803 = 2 } else { numSpawns803 = (playersIn803.size() * 1.5).intValue() }
	spawnSecurity606()
	spawnSecurity702()
	spawnSecurity803()
}

// Swing the door open and enable the flag that allows players to enter via the trigger zone there.
def openSeaLabDoor = { event ->
	if( sealabDoorUnlocked == true ) {
		myManager.schedule(1) {
			seaLabDoor.on()
			canEnterSealab = true
			removeMiniMapMarker("switch904Flag")
		}
	} else {
		event.actor.centerPrint( "DEACTIVATE SECURITY SWITCHES FOR ADMITTANCE TO SEALAB." )
	}
}

switch606.whenOn( check606 )
switch702.whenOn( check702 )
switch803.whenOn( check803 )
switch904.whenOn( openSeaLabDoor ) //automatically open the "switch" that is the SeaLab Door


//-------------------------------------------
// SECURITY SPAWN WAVES                      
//-------------------------------------------

playersIn606 = [] as Set
playersIn702 = [] as Set
playersIn803 = [] as Set

//The "onEnter" and "onExit" for Sealab_606 are down in the Labtech 123 Ambush section

myManager.onEnter( myRooms.Sealab_702 ) { event ->
	if( isPlayer( event.actor ) ) {
		playersIn702 << event.actor
	}
}

myManager.onExit( myRooms.Sealab_702 ) { event ->
	if( isPlayer( event.actor ) ) {
		playersIn702.remove( event.actor )
	}
}

myManager.onEnter( myRooms.Sealab_803 ) { event ->
	if( isPlayer( event.actor ) ) {
		playersIn803 << event.actor
	}
}

myManager.onExit( myRooms.Sealab_803 ) { event ->
	if( isPlayer( event.actor ) ) {
		playersIn803.remove( event.actor )
	}
}

sec606Set = [] as Set
sec702Set = [] as Set
sec803Set = [] as Set

def spawnSecurity606() {
	if( numSpawns606 > 0 ) {
		sec606 = security606Spawner.forceSpawnNow()
		sec606Set << sec606
		numSpawns606 --
		spawnSecurity606()
		
	} else { //after everything is spawned, move them out toward the player
		security606Spawner.startPatrol()
	}
}
	
def spawnSecurity702() {
	if( numSpawns702 > 0 ) {
		sec702 = security702Spawner.forceSpawnNow()
		sec702Set << sec702
		numSpawns702 --
		spawnSecurity702()
	} else { //after everything is spawned, move them out toward the player
		security702Spawner.startPatrol()
	}
}

def spawnSecurity803() {
	if( numSpawns803 > 0 ) {
		sec803 = security803Spawner.forceSpawnNow()
		sec803Set << sec803
		numSpawns803 --
		spawnSecurity803()
	} else { //after everything is spawned, move them out toward the player
		security803Spawner.startPatrol()
	}
}



//------------------------------------------
// LABTECH 123 MONOLOGUE                    
//------------------------------------------

//LABTECH 123 (in Sealab_606, positioned for his monologue)
Labtech123 = spawnNPC("under123-VQS", myRooms.Sealab_606, 1070, 255)
Labtech123.setRotation( 135 )
Labtech123.setDisplayName( "Labtech 123" )

monologueStarted = false
monologueFinished = false
timer1Failed = false
timer2Failed = false
timer3Failed = false
timer4Failed = false
timer5Failed = false
timer6Failed = false
timer7Failed = false
timer8Failed = false
timer10Failed = false
timer11Failed = false
labtech123Present = true

playerSetAmbush606 = [] as Set

scoobyOneStarted = false
scoobyTwoStarted = false
scoobyThreeStarted = false
scoobyFourStarted = false
scoobyFiveStarted = false
scoobySixStarted = false
scoobySevenStarted = false
scoobyEightStarted = false
scoobyTenStarted = false
scoobyElevenStarted = false

numFinishedMwu1 = 0
numFinishedMwu2 = 0
numFinishedMwu3 = 0
numFinishedMwu4 = 0
numFinishedMwu5 = 0
numFinishedMwu6 = 0
numFinishedMwu7 = 0
numFinishedMwu8 = 0
numFinishedMwu10 = 0
numFinishedMwu11 = 0

onEnterBlock = new Object()

myManager.onEnter( myRooms.Sealab_606 ) { event->
	synchronized( onEnterBlock ) {
		if( isPlayer( event.actor ) ) {
			playersIn606 << event.actor
			playerSetAmbush606 << event.actor
			if( monologueStarted == false ) { //this prevents more than one person from starting the monologue
				monologueStarted = true
				switch606.lock(); switch702.lock(); switch803.lock() //Lock ALL THREE SWTICHES to prevent the alarm from wrecking the monologue by being triggered during it.
				myManager.schedule(2) {
					playerSetAmbush606.clone().each {
						it.setQuestFlag( GLOBAL, "Z13Labtech123MonologueOkayToStart" )
						it.setQuestFlag( GLOBAL, "Z13OneOkay" )
						Labtech123.pushDialog( it, "mwuhaha1" )
					}
					mwu1Timer = myManager.schedule(15) {
						timer1Failed = true
						scoobyOne( event )
					}
				}
			} else if( monologueStarted == true && monologueFinished == false ) {
				event.actor.setQuestFlag( GLOBAL, "Z13Labtech123MonologueOkayToStart" )
			}
		}
	}
}

myManager.onExit( myRooms.Sealab_606 ) { event ->
	if( isPlayer( event.actor ) ) {
		//if the player is the last one out of the room, and the monologue isn't finished, then reset all flags so the monologue can start again when the room is re-entered
		if( playerSetAmbush606.size() == 1 && monologueFinished == false ) { 
			playerSetAmbush606.clone().each {
				it.unsetQuestFlag( GLOBAL, "Z13OneOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13TwoOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13ThreeOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13FourOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13FiveOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13SixOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13SevenOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13EightOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13NineOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13TenOkay" )
				it.unsetQuestFlag( GLOBAL, "Z13Labtech123MonologueOkayToStart" )
			}
			monologueStarted = false
		}		
		
		playersIn606.remove( event.actor )
		playerSetAmbush606.remove( event.actor )

	}
}

//THE MONOLOGUE (Mwu-ha-ha!)
mwuhaha1 = Labtech123.createConversation( "mwuhaha1", true, "Z13Labtech123MonologueOkayToStart", "Z13OneOkay" )

def mwu1 = [id:1]
mwu1.npctext = "Wait! I give up! I surrender! You weren't supposed to make it this far! I'll tell you everything...just don't hit me!"
mwu1.flag = [ "!Z13OneOkay", "Z13FinishedMwu1" ]
mwu1.exec = { event ->
	numFinishedMwu1 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu1 >= playerSetAmbush606.size() && timer1Failed == false && scoobyOneStarted == false ) { 
		scoobyOneStarted = true
		mwu1Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyOne( event )
	}
}
mwu1.result = DONE
mwuhaha1.addDialog( mwu1, Labtech123 )

def scoobyOne( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "Everything? Like what? What is this place?" )
	myManager.schedule(2) { 
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13TwoOkay" )
			Labtech123.pushDialog( it, "mwuhaha2" )
		}
		mwu2Timer = myManager.schedule(15) {
			timer2Failed = true
			scoobyTwo( event )
		}
	}
}	

//part 2
mwuhaha2 = Labtech123.createConversation( "mwuhaha2", true, "Z13Labtech123MonologueOkayToStart", "Z13TwoOkay" )

def mwu2 = [id:2]
mwu2.npctext = "This is a deep-sea research laboratory, funded by the NeXuS Corp., investigating the unusual fauna in the area."
mwu2.flag = [ "!Z13TwoOkay", "Z13FinishedMwu2" ]
mwu2.exec = { event ->
	numFinishedMwu2 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu2 >= playerSetAmbush606.size() && timer2Failed == false && scoobyTwoStarted == false ) { 
		scoobyTwoStarted = true
		mwu2Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyTwo( event )
	}
}
mwu2.result = DONE
mwuhaha2.addDialog( mwu2, Labtech123 )

def scoobyTwo( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "'Unusual fauna'? You mean the Animated!" )
	myManager.schedule(1) { random( playerSetAmbush606 ).say( "What does NeXuS have to do with G-Corp anyway?" ) }
	myManager.schedule(3) {
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13ThreeOkay" )
			Labtech123.pushDialog( it, "mwuhaha3" )
		}
		mwu3Timer = myManager.schedule(15) {
			timer3Failed = true
			scoobyThree( event )
		}
	}
}	


//part 3
mwuhaha3 = Labtech123.createConversation( "mwuhaha3", true, "Z13Labtech123MonologueOkayToStart", "Z13ThreeOkay" )

def mwu3 = [id:3]
mwu3.npctext = "You think all THIS is just about ruining G-Corp? Well...sure...we all have our grudges here, but the demise of that empire is a mere byproduct of our true purpose. This sealab is about much more than Gambino's paltry vision of financial domination! This is about POWER!"
mwu3.flag = [ "!Z13ThreeOkay", "Z13FinishedMwu3" ]
mwu3.exec = { event ->
	numFinishedMwu3 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu3 >= playerSetAmbush606.size() && timer3Failed == false && scoobyThreeStarted == false ) { 
		scoobyThreeStarted = true
		mwu3Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyThree( event )
	}
}

mwu3.result = DONE
mwuhaha3.addDialog( mwu3, Labtech123 )

def scoobyThree( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "What sort of power?" )
	myManager.schedule(1) { random( playerSetAmbush606 ).say( "Why here, so far under the water?" ) }
	myManager.schedule(3) { 
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13FourOkay" )
			Labtech123.pushDialog( it, "mwuhaha4" )
		}
		mwu4Timer = myManager.schedule(15) {
			timer4Failed = true
			scoobyFour( event )
		}
	}
}	

//part 4
mwuhaha4 = Labtech123.createConversation( "mwuhaha4", true, "Z13Labtech123MonologueOkayToStart", "Z13FourOkay" )

def mwu4 = [id:4]
mwu4.npctext = "That fission event that attracted the Zurg detonated nearby...and when that occurred, it left a signature behind...a sort of *wound*...that's leaking energy into our world in way that has never before occurred."
mwu4.flag = [ "!Z13FourOkay", "Z13FinishedMwu4" ]
mwu4.exec = { event ->
	numFinishedMwu4 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu4 >= playerSetAmbush606.size() && timer4Failed == false && scoobyFourStarted == false ) { 
		scoobyFourStarted = true
		mwu4Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyFour( event )
	}
}
mwu4.result = DONE
mwuhaha4.addDialog( mwu4, Labtech123 )

def scoobyFour( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "You mean Ghi energy?" )
	myManager.schedule(1) { random( playerSetAmbush606 ).say( "Is the world wounded?" ) }
	myManager.schedule(3) {
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13FiveOkay" )
			Labtech123.pushDialog( it, "mwuhaha5" )
		}
		mwu5Timer = myManager.schedule(15) {
			timer5Failed = true
			scoobyFive( event )
		}
	}
}

//part 5
mwuhaha5 = Labtech123.createConversation( "mwuhaha5", true, "Z13Labtech123MonologueOkayToStart", "Z13FiveOkay" )

def mwu5 = [id:5]
mwu5.npctext = "The leak is massive, but even so, the world is gigantic in comparison. We didn't even know there was such a thing as a biosphere or Ghi energy before, but we know its size now and even at its increased dimensions this leak should bear no threat."
mwu5.flag = [ "!Z13FiveOkay", "Z13FinishedMwu5" ]
mwu5.exec = { event ->
	numFinishedMwu5 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu5 >= playerSetAmbush606.size() && timer5Failed == false && scoobyFiveStarted == false ) { 
		scoobyFiveStarted = true
		mwu5Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyFive( event )
	}
}
mwu5.result = DONE
mwuhaha5.addDialog( mwu5, Labtech123 )

def scoobyFive( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "What the heck's a biosphere?" )
	myManager.schedule(1) { random( playerSetAmbush606 ).say( "Increased size? You mean the rip is getting bigger?!?" ) }
	myManager.schedule(4) {
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13SixOkay" )
			Labtech123.pushDialog( it, "mwuhaha6" )
		}
		mwu6Timer = myManager.schedule(15) {
			timer6Failed = true
			scoobySix( event )
		}
	}
}

//part 6
mwuhaha6 = Labtech123.createConversation( "mwuhaha6", true, "Z13Labtech123MonologueOkayToStart", "Z13SixOkay" )

def mwu6 = [id:6]
mwu6.npctext = "Apparently, we're *all* part of the biosphere. The world seems to contain the biosphere's energy somehow, like a charged battery, and as near as we can tell, we all pull a charge constantly. We usually call it 'life', but that's messy nomenclature, so we call it Ghi energy instead. Same thing."
mwu6.flag = [ "!Z13SixOkay", "Z13FinishedMwu6" ] 
mwu6.exec = { event ->
	numFinishedMwu6 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu6 >= playerSetAmbush606.size() && timer6Failed == false && scoobySixStarted == false ) { 
		scoobySixStarted = true
		mwu6Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobySix( event )
	}
}
mwu6.result = DONE
mwuhaha6.addDialog( mwu6, Labtech123 )

def scoobySix( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "Get on with it!" )
	myManager.schedule(1) {
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13SevenOkay" )
			Labtech123.pushDialog( it, "mwuhaha7" )
		}
		mwu7Timer = myManager.schedule(15) {
			timer7Failed = true
			scoobySeven( event )
		}
	}
}

//part 7
mwuhaha7 = Labtech123.createConversation( "mwuhaha7", true, "Z13Labtech123MonologueOkayToStart", "Z13SevenOkay" )

def mwu7 = [id:7]
mwu7.npctext = "Yes...of course. Anyway, the rip wasn't leaking fast enough, so we built this complex to harvest the Animated that teemed across the sea floor close to the fissure. We extracted the rings and then distributed them as weapons to all of you so eager to protect your homes."
mwu7.flag = [ "!Z13SevenOkay", "Z13FinishedMwu7" ]
mwu7.exec = { event ->
	numFinishedMwu7 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu7 >= playerSetAmbush606.size() && timer7Failed == false && scoobySevenStarted == false ) { 
		scoobySevenStarted = true
		mwu7Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobySeven( event )
	}
}
mwu7.result = DONE
mwuhaha7.addDialog( mwu7, Labtech123 )

def scoobySeven( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "What? But that means...!" )
	myManager.schedule(2) {
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13EightOkay" )
			Labtech123.pushDialog( it, "mwuhaha8" )
		}
		mwu8Timer = myManager.schedule(30) {
			timer8Failed = true
			scoobyEight( event )
		}
	}
}

//part 8
mwuhaha8 = Labtech123.createConversation( "mwuhaha8", true, "Z13Labtech123MonologueOkayToStart", "Z13EightOkay" )

def mwu8 = [id:8]
mwu8.npctext = "Yes. You need the rings to protect your homes against the Animated, which are only affected by the Ghi energy focused through a ring, but in using the rings, you force the rift to widen further, loosing more Ghi energy upon the world which, in turn, creates more Animated!"
mwu8.result = 9
mwuhaha8.addDialog( mwu8, Labtech123 )

def mwu9 = [id:9]
mwu9.npctext = "Isn't it all just so symmetrical? And the boss man has captured and focused a lot of that extra Ghi you so unknowingly provided us."
mwu9.flag = [ "!Z13EightOkay", "Z13FinishedMwu8" ]
mwu9.exec = { event ->
	numFinishedMwu8 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu8 >= playerSetAmbush606.size() && timer8Failed == false && scoobyEightStarted == false ) { 
		scoobyEightStarted = true
		mwu8Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyEight( event )
	}
}
mwu9.result = DONE
mwuhaha8.addDialog( mwu9, Labtech123 )

def scoobyEight( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "Focused it into what?" )
	myManager.schedule(2) {
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13TenOkay" )
			Labtech123.pushDialog( it, "mwuhaha10" )
		}
		mwu10Timer = myManager.schedule(15) {
			timer10Failed = true
			scoobyTen( event )
		}
	}
}

//part 10
mwuhaha10 = Labtech123.createConversation( "mwuhaha10", true, "Z13Labtech123MonologueOkayToStart", "Z13TenOkay" )

def mwu10 = [id:10]
mwu10.npctext = "Ha! I'm not telling you! Even if you tripped all three security checkstations at once and got into the main lab, you'd still be too late. The boss has already started the breeding program!"
mwu10.flag = [ "!Z13TenOkay", "Z13FinishedMwu10" ]
mwu10.exec = { event ->
	numFinishedMwu10 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu10 >= playerSetAmbush606.size() && timer10Failed == false && scoobyTenStarted == false ) { 
		scoobyTenStarted = true
		mwu10Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyTen( event )
	}
}
mwu10.result = DONE
mwuhaha10.addDialog( mwu10, Labtech123 )

def scoobyTen( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	random( playerSetAmbush606 ).say( "Breeding program?" )
	myManager.schedule(2) {
		playerSetAmbush606.clone().each{
			it.setQuestFlag( GLOBAL, "Z13ElevenOkay" )
			Labtech123.pushDialog( it, "mwuhaha11" )
		}
		mwu11Timer = myManager.schedule(15) {
			timer11Failed = true
			scoobyEleven( event )
		}
	}
}

//part 11
mwuhaha11 = Labtech123.createConversation( "mwuhaha11", true, "Z13Labtech123MonologueOkayToStart", "Z13ElevenOkay" )

def mwu11 = [id:11]
mwu11.npctext = "You'll never know! And now there's been enough time for my reinforcements to ready their teleport mechanisms. Enjoy your battle! Ta ta for now!"
mwu11.flag = [ "!Z13Labtech123MonologueOkayToStart", "!Z13ElevenOkay", "Z13FinishedMwu11" ]
mwu11.exec = { event ->
	numFinishedMwu11 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMwu11 >= playerSetAmbush606.size() && timer11Failed == false && scoobyElevenStarted == false ) { 
		scoobyElevenStarted = true
		mwu11Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyEleven( event )
	}
}
mwu11.result = DONE
mwuhaha11.addDialog( mwu11, Labtech123 )

def scoobyEleven( event ) {
	if ( !playerSetAmbush606.size() ) {
		return
	}
	monologueFinished = true
	labtech123Present = false
	switch606.unlock(); switch702.unlock(); switch803.unlock() //Unlock all three switches again after the monologue
	startAmbush( event )
}

//------------------------------------------
// THE LABTECH 123 AMBUSH!                  
//------------------------------------------

ambushStarted = false

def startAmbush( event ) {
	if( ambushStarted == false ) {
		ambushStarted = true
		//event.player.centerPrint( "Ambush Started!" )
		numWaves = (playerSetAmbush606.size() / 2)
		spawnAmbushWaves()
		Labtech123.say( "Mwu-ha-ha!" )
		myManager.schedule(1) { Labtech123.dispose() }
	}
}

def spawnAmbushWaves() {
	if( numWaves > 0 ) {
		numWaves -- //decrement the counter
		
		// Spawn ambushers from remote locations and warp them in en masse
		cycle1 = ambushCycle1.forceSpawnNow()
		cycle2 = ambushCycle2.forceSpawnNow()
		cycle3 = ambushCycle3.forceSpawnNow()
		tech1 = ambushTech1.forceSpawnNow()
		tech2 = ambushTech2.forceSpawnNow()
		tech3 = ambushTech3.forceSpawnNow()
		myManager.schedule(2) {
			cycle1.warp( "Sealab_606", 250, 530, 6 )
			cycle2.warp( "Sealab_606", 135, 910, 6 )
			cycle3.warp( "Sealab_606", 1310, 925, 6 )
			tech1.warp( "Sealab_606", 500, 650, 6 )
			tech2.warp( "Sealab_606", 700, 790, 6 )
			tech3.warp( "Sealab_606", 1000, 910, 6 )
			
			if( !playerSetAmbush606.isEmpty() ) {
				cycle1.addHate( random( playerSetAmbush606 ), 100 )
				cycle2.addHate( random( playerSetAmbush606 ), 100 )
				cycle3.addHate( random( playerSetAmbush606 ), 100 )
				tech1.addHate( random( playerSetAmbush606 ), 100 )
				tech2.addHate( random( playerSetAmbush606 ), 100 )
				tech3.addHate( random( playerSetAmbush606 ), 100 )
			}
			checkForNextWave()
		}
	} else {
		myManager.schedule( random( 120, 180 ) ) { spawnSwitch606Guards() }
	}
}

def checkForNextWave() {
//Spawner.spawnsInUse()
	monsterCount = ambushCycle1.spawnsInUse() + ambushCycle2.spawnsInUse() + ambushCycle3.spawnsInUse() + ambushTech1.spawnsInUse() + ambushTech2.spawnsInUse() + ambushTech3.spawnsInUse()
	if( monsterCount < 3 ) {
		spawnAmbushWaves()
	} else {
		myManager.schedule(5) { checkForNextWave() }
	}
}

switch606Leader1 = myRooms.Sealab_705.spawnStoppedSpawner( "switch606Leader1", "labtech_female", 1 )
switch606Leader1.setPos( 850, 210 )
switch606Leader1.setGuardPostForChildren( "Sealab_606", 300, 660, 135 )
switch606Leader1.setMonsterLevelForChildren( ML )

switch606Guard1 = myRooms.Sealab_705.spawnStoppedSpawner( "switch606Guard1", "labtech_male", 1 )
switch606Guard1.setPos( 850, 210 )
switch606Guard1.setGuardPostForChildren( "Sealab_606", 300, 500, 135 )
switch606Guard1.setMonsterLevelForChildren( ML )

switch606Guard2 = myRooms.Sealab_705.spawnStoppedSpawner( "switch606Guard2", "labtech_male", 1 )
switch606Guard2.setPos( 850, 210 )
switch606Guard2.setGuardPostForChildren( "Sealab_606", 370, 600, 135 )
switch606Guard2.setMonsterLevelForChildren( ML )

switch606Guard3 = myRooms.Sealab_705.spawnStoppedSpawner( "switch606Guard3", "labtech_male", 1 )
switch606Guard3.setPos( 850, 210 )
switch606Guard3.setGuardPostForChildren( "Sealab_606", 500, 620, 135 )
switch606Guard3.setMonsterLevelForChildren( ML )

def spawnSwitch606Guards() {
	respawn606Started = false
	adjustML()
	
	leader606 = switch606Leader1.forceSpawnNow()
	leader606.setDisplayName( "Security Chief" )
	myManager.onAwarenessIn( leader606 ) { event ->
		if( isPlayer( event.actor ) ) {
			leader606.say( "You're trespassing on NeXuS property! Back away from that switch!" )
		}
	}
	guard606A = switch606Guard1.forceSpawnNow()
	guard606A.setDisplayName( "Switch Guard" )
	
	guard606B = switch606Guard2.forceSpawnNow()
	guard606B.setDisplayName( "Switch Guard" )
	
	guard606C = switch606Guard3.forceSpawnNow()
	guard606C.setDisplayName( "Switch Guard" )
	
	runOnDeath( leader606 ) { check606GuardRespawn() }
	runOnDeath( guard606A ) { check606GuardRespawn() }
	runOnDeath( guard606B ) { check606GuardRespawn() }
	runOnDeath( guard606C ) { check606GuardRespawn() }
}

def synchronized check606GuardRespawn() {
	if( leader606.isDead() && guard606A.isDead() && guard606B.isDead() && guard606C.isDead() && respawn606Started == false ) {
		respawn606Started = true
		if( sealabDoorUnlocked == false ) { myManager.schedule( random( 60, 90 ) ) { spawnSwitch606Guards() } }
	}
}

def getMonsterTotal() {
	monsterCounter = 0 //Reset the counter to get a fresh count
	spawnerList.clone().each { //Step through the spawner list
		if(it.spawnsInUse() > 0) {
			activeSpawners.put(it, it.spawnsInUse())
			monsterCounter = monsterCounter + it.spawnsInUse() //Add the number of spawns to the current total
		}
	}
	//println "^^^^ monsterCounter = ${monsterCounter} ^^^^"
	//println "^^^^ activeSpawners = ${activeSpawners} ^^^^"
	//myManager.schedule(30) { getMonsterTotal() }
}