import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// SEALAB X BOSS ROOM ENCOUNTER             
//------------------------------------------

LabtechX = spawnNPC("UncleBob-VQS", myRooms.Sealab_2, 500, 130 )
LabtechX.setDisplayName( "Labtech X" )

playerSet = [] as Set
goldDebug = new HashMap()

endGameNotStarted = true
newOneSet = false

timer1Failed = false
timer2Failed = false
timer4Failed = false
timer5Failed = false
timer6Failed = false
timer7Failed = false
timer8Failed = false
timer9Failed = false
timer10Failed = false
timer11Failed = false
timer12Failed = false
monologueStarted = false
monologueFinished = false

scoobyOneStarted = false
scoobyTwoStarted = false
scoobyFourStarted = false
scoobyFiveStarted = false
scoobySixStarted = false
scoobySevenStarted = false
scoobyEightStarted = false
scoobyNineStarted = false
scoobyTenStarted = false
scoobyElevenStarted = false
scoobyTwelveStarted = false

numFinishedMono1 = 0
numFinishedMono2 = 0
numFinishedMono3 = 0
numFinishedMono4 = 0
numFinishedMono5 = 0
numFinishedMono6 = 0
numFinishedMono7 = 0
numFinishedMono8 = 0
numFinishedMono9 = 0
numFinishedMono10 = 0
numFinishedMono11 = 0
numFinishedMono12 = 0

MinML = 8.0
ML = 8.0 
MaxML = 10.0

difficulty = null

onEnterBlock = new Object()

myManager.onEnter( myRooms.Sealab_106 ) { event ->
	synchronized( onEnterBlock ) { //this is synchronized to avoid more than one player somehow starting the monologue process
		if( isPlayer( event.actor ) ) {
			playerSet << event.actor
			
			//DIFFICULTY
			//The first player into the scenario sets the difficulty for everyone else
			if( difficulty == null ) {
				difficulty = event.actor.getTeam().getAreaVar( "Z11_Shallow_Sea", "Z11ShallowSeaDifficulty" )
				if( difficulty == 0 ) { difficulty = 1; println "**** difficulty changed to 1 ****" }
			}			

			///*  USE THESE TO COMMENT OUT THIS CHUNK TO SKIP MONOLOGUE WHILE TESTING==============================
			if( monologueStarted == false ) {
				monologueStarted = true
				myManager.schedule(5) {
					LabtechX.warp( "Sealab_106", 1790, 410 ) 
					myManager.schedule(1) { random( playerSet ).say( "Lookout! Another teleporter!" ) }
					myManager.schedule(2) { random( playerSet ).say( "Who is it?" ) }
					myManager.schedule(3) {
						//set flags and push the dialog for each player in the room
						playerSet.clone().each{
							it.setQuestFlag( GLOBAL, "Z13LabtechXMonologueAuthorized" )
							it.setQuestFlag( GLOBAL, "Z13Mono1Okay" )
							LabtechX.pushDialog( it, "monoX1" )
						}
						//now start the auto-advance timer
						mono1Timer = myManager.schedule(15) {
							timer1Failed = true
							scoobyOne( event )
						}
					}
				}
			} else if( monologueStarted == true && monologueFinished == false ) {
				event.actor.unsetQuestFlag( GLOBAL, "Z13LabtechXMonologueFinished" )
				event.actor.setQuestFlag( GLOBAL, "Z13LabtechXMonologueAuthorized" )
			
			//Also set up a way to begin the fight over again if the party got wiped last time
			} else if( monologueFinished == true && allDazed == true ) {
				allDazed = false
				myManager.schedule(3) { playerSet.clone().each{ it.centerPrint( "Ah! Gluttons for punishment, eh?" ) } }
				myManager.schedule(6) { playerSet.clone().each{ it.centerPrint( "Well, I am ready for you. My prototype Sealab has been once again restored to its former glory." ) } }
				myManager.schedule(9) { playerSet.clone().each{ it.centerPrint( "So now it is time for you to die!" ) } }
				myManager.schedule(12) { playerSet.clone().each{ it.centerPrint( "Again." ) }; adjustML(); spawnTail() }
			}				
			//*///  FOR SKIPPING MONOLOGUE (Everything above this)==============================
			//spawnTail() //DEBUG ONLY!!!!
			//mineBarrage() ///DEBUG ONLY!!!!
		}
	}
}

myManager.onExit( myRooms.Sealab_106 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet.remove( event.actor )
		event.actor.unsetQuestFlag( GLOBAL, "Z13LabtechXMonologueFinished" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13LabtechXMonologueAuthorized" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono1Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono2Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono4Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono5Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono6Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono7Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono8Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono9Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono10Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono11Okay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z13Mono12Okay" )
	}
}

//------------------------------------------
// GRUNNY SUB "SUMMONED" SPAWNERS           
//------------------------------------------

grunny1 = myRooms.Sealab_106.spawnStoppedSpawner( "grunny1", "grunny_sub", 20)
grunny1.setPos( 80, 1175 )
grunny1.setMonsterLevelForChildren( ML )

grunny2 = myRooms.Sealab_106.spawnStoppedSpawner( "grunny2", "grunny_sub", 20)
grunny2.setPos( 1860, 1175 )
grunny2.setMonsterLevelForChildren( ML )

//------------------------------------------
// MISSILE BARRAGE SPAWNERS                 
//------------------------------------------

missile1 = myRooms.Sealab_106.spawnStoppedSpawner( "missile1", "sealab_missile", 40)
missile1.setPos( 200, 10 )
missile1.setMonsterLevelForChildren( ML )

missile2 = myRooms.Sealab_106.spawnStoppedSpawner( "missile2", "sealab_missile", 40)
missile2.setPos( 550, 10 )
missile2.setMonsterLevelForChildren( ML )

missile3 = myRooms.Sealab_106.spawnStoppedSpawner( "missile3", "sealab_missile", 40)
missile3.setPos( 900, 10 )
missile3.setMonsterLevelForChildren( ML )

missile4 = myRooms.Sealab_106.spawnStoppedSpawner( "missile4", "sealab_missile", 40)
missile4.setPos( 1250, 10 )
missile4.setMonsterLevelForChildren( ML )

missile5 = myRooms.Sealab_106.spawnStoppedSpawner( "missile5", "sealab_missile", 40)
missile5.setPos( 1600, 10 )
missile5.setMonsterLevelForChildren( ML )


//------------------------------------------
// DEATH MINE SPAWNERS                      
//------------------------------------------

deathmine1 = myRooms.Sealab_106.spawnStoppedSpawner( "deathmine1", "sealab_deathmine", 20)
deathmine1.setPos( 200, 10 )
deathmine1.setMonsterLevelForChildren( ML )

deathmine2 = myRooms.Sealab_106.spawnStoppedSpawner( "deathmine2", "sealab_deathmine", 20)
deathmine2.setPos( 550, 10 )
deathmine2.setMonsterLevelForChildren( ML )

deathmine3 = myRooms.Sealab_106.spawnStoppedSpawner( "deathmine3", "sealab_deathmine", 20)
deathmine3.setPos( 900, 10 )
deathmine3.setMonsterLevelForChildren( ML )

deathmine4 = myRooms.Sealab_106.spawnStoppedSpawner( "deathmine4", "sealab_deathmine", 20)
deathmine4.setPos( 1250, 10 )
deathmine4.setMonsterLevelForChildren( ML )

deathmine5 = myRooms.Sealab_106.spawnStoppedSpawner( "deathmine5", "sealab_deathmine", 20)
deathmine5.setPos( 1600, 10 )
deathmine5.setMonsterLevelForChildren( ML )


//------------------------------------------
// SEALAB X PARTS                           
//------------------------------------------

tailSpawner = myRooms.Sealab_106.spawnStoppedSpawner( "tailSpawner", "sealab_tail", 1)
tailSpawner.enableForceSpawnFailureLogging()
tailSpawner.setPos( 1400, 810 )
tailSpawner.setMonsterLevelForChildren( ML )

bodySpawner = myRooms.Sealab_106.spawnStoppedSpawner( "bodySpawner", "sealab_body", 1)
bodySpawner.enableForceSpawnFailureLogging()
bodySpawner.setPos( 1400, 810 )
bodySpawner.setMonsterLevelForChildren( ML )

rightArm = myRooms.Sealab_106.spawnStoppedSpawner( "rightArm", "sealab_right_arm", 1)
rightArm.enableForceSpawnFailureLogging()
rightArm.setPos( 1400-490, 810-120 ) // see lArm.setMovementParent() below.
rightArm.setMonsterLevelForChildren( ML )

leftArm = myRooms.Sealab_106.spawnStoppedSpawner( "leftArm", "sealab_left_arm", 1)
leftArm.enableForceSpawnFailureLogging()
leftArm.setPos( 1400+160, 810+70 ) // see rArm.setMovementParent() below.
leftArm.setMonsterLevelForChildren( ML )

headUnit = myRooms.Sealab_106.spawnStoppedSpawner( "headUnit", "sealab_head", 1)
headUnit.enableForceSpawnFailureLogging()
headUnit.setPos( 1400, 810 )
headUnit.setMonsterLevelForChildren( ML )


//LISTS FOR LOGIC (below)
missileList = [ missile1, missile2, missile3, missile4, missile5 ]
deathmineList = [ deathmine1, deathmine2, deathmine3, deathmine4, deathmine5 ]

def adjustML() {
	ML = MinML

	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	playerSet.clear()
	myRooms.Sealab_1.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_2.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_102.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_103.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_106.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_203.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_204.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_304.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_305.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_401.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_402.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_403.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_405.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_406.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_506.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_507.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_603.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_604.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_605.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_606.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_701.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_702.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_703.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_704.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_705.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_706.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_801.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_802.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_803.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_804.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_805.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_806.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_901.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_902.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_903.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_904.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_905.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_906.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1001.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1002.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1003.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1004.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1005.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	myRooms.Sealab_1006.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
	
	//adjust the encounter ML to the group
	random( playerSet ).getCrew().clone().each{		
		if( it.getConLevel() > ML && it.getConLevel() <= MaxML ) {
			ML = it.getConLevel()
		} else if( it.getConLevel() > MaxML ) {
			ML = MaxML
		}
	}

	//kick players out if they are cheating
	playerSet.clone().each{
		if( it.getConLevel() > MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "Beach_2", 720, 280 )
		}
	}
	
	//DIFFICULTY
	if( difficulty == 1 ) { ML = ML - 0.6 }
	if( difficulty == 3 ) { ML = ML + 0.6 }

	//Now update all the spawners with the new ML so they start creating monsters at that new level
	grunny1.setMonsterLevelForChildren( ML )
	grunny2.setMonsterLevelForChildren( ML )
	missile1.setMonsterLevelForChildren( ML )
	missile2.setMonsterLevelForChildren( ML )
	missile3.setMonsterLevelForChildren( ML )
	missile4.setMonsterLevelForChildren( ML )
	missile5.setMonsterLevelForChildren( ML )
	deathmine1.setMonsterLevelForChildren( ML )
	deathmine2.setMonsterLevelForChildren( ML )
	deathmine3.setMonsterLevelForChildren( ML )
	deathmine4.setMonsterLevelForChildren( ML )
	deathmine5.setMonsterLevelForChildren( ML )
	tailSpawner.setMonsterLevelForChildren( ML )
	bodySpawner.setMonsterLevelForChildren( ML )
	rightArm.setMonsterLevelForChildren( ML )
	leftArm.setMonsterLevelForChildren( ML )
	headUnit.setMonsterLevelForChildren( ML )
}
	

//-------------------------------------
// LABTECH X MONOLOGUE                 
//-------------------------------------

//part 1
def monoX1 = LabtechX.createConversation( "monoX1", true, "Z13LabtechXMonologueAuthorized", "Z13Mono1Okay", "!Z13LabtechXMonologueFinished" )

def mono1 = [id:1]
mono1.npctext = "Who am I? I'm the shadow of the man to whom you all bend your knee. I am progeny ex machina. Simply put, I'm death insurance."
mono1.flag = [ "!Z13Mono1Okay", "Z13FinishedMono1" ]
mono1.exec = { event ->
	numFinishedMono1 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono1 >= playerSet.size() && timer1Failed == false && scoobyOneStarted == false ) { 
		scoobyOneStarted = true
		mono1Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyOne( event )
	}
}
mono1.result = DONE
monoX1.addDialog( mono1, LabtechX )

def scoobyOne( event ) {
	random( playerSet ).say( "Death insurance?" )
	myManager.schedule(1) { random( playerSet ).say( "What the heck does *that* mean?" ) }
	myManager.schedule(2) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono2Okay" )
			LabtechX.pushDialog( it, "monoX2" )
		}
		mono2Timer = myManager.schedule(15) {
			timer2Failed = true
			scoobyTwo( event )
		}
	}
}	

//part 2
def monoX2 = LabtechX.createConversation( "monoX2", true, "Z13LabtechXMonologueAuthorized", "Z13Mono2Okay", "!Z13LabtechXMonologueFinished" )

def mono2 = [id:2]
mono2.npctext = "It means that I have pretended for years, working steadily for my 'father', managing his companies as if I *was* him already."
mono2.result = 3
monoX2.addDialog( mono2, LabtechX )

def mono3 = [id:3]
mono3.npctext = "But if Johnny Gambino has the intellect of kings, why should it surprise anyone that his *copy* shares that same capacity for intellect?"
mono3.flag = [ "!Z13Mono2Okay", "Z13FinishedMono2" ]
mono3.exec = { event ->
	numFinishedMono2 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono2 >= playerSet.size() && timer2Failed == false && scoobyTwoStarted == false ) { 
		scoobyTwoStarted = true
		mono2Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyTwo( event )
	}
}
mono3.result = DONE
monoX2.addDialog( mono3, LabtechX )

def scoobyTwo( event ) {
	random( playerSet ).say( "Copy? Of Johnny Gambino?" )
	myManager.schedule(1) { random( playerSet ).say( "Is this guy talking to *us* or *himself*?" ) }
	myManager.schedule(3) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono4Okay" )
			LabtechX.pushDialog( it, "monoX4" )
		}
		mono4Timer = myManager.schedule(30) {
			timer4Failed = true
			scoobyFour( event )
		}
	}
}

//part 4
def monoX4 = LabtechX.createConversation( "monoX4", true, "Z13LabtechXMonologueAuthorized", "Z13Mono4Okay", "!Z13LabtechXMonologueFinished" )

def mono4 = [id:4]
mono4.npctext = "Even so, I was loyal. It was I that saved my father *and* his birth son, Gino. It was I that released the pent up Ghi energy with a timely injection of a critical catalyst."
mono4.flag = [ "!Z13Mono4Okay", "Z13FinishedMono4" ]
mono4.exec = { event ->
	numFinishedMono4 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono4 >= playerSet.size() && timer4Failed == false && scoobyFourStarted == false ) { 
		scoobyFourStarted = true
		mono4Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyFour( event )
	}
}
mono4.result = DONE
monoX4.addDialog( mono4, LabtechX )

def scoobyFour( event ) {
	random(playerSet ).say( "Wait a minute! This is the guy that triggered the Gambino event?!?" )
	myManager.schedule(2) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono5Okay" )
			LabtechX.pushDialog( it, "monoX5" )
		}
		mono5Timer = myManager.schedule(15) {
			timer5Failed = true
			scoobyFive( event )
		}
	}
}

//part 5
def monoX5 = LabtechX.createConversation( "monoX5", true, "Z13LabtechXMonologueAuthorized", "Z13Mono5Okay", "!Z13LabtechXMonologueFinished" )

def mono5 = [id:5]
mono5.npctext = "The energy released when the Gambinos separated was primordial, but surprisingly non-destructive in nature."
mono5.flag = [ "!Z13Mono5Okay", "Z13FinishedMono5" ]
mono5.exec = { event ->
	numFinishedMono5 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono5 >= playerSet.size() && timer5Failed == false && scoobyFiveStarted == false ) { 
		scoobyFiveStarted = true
		mono5Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyFive( event )
	}
}
mono5.result = DONE
monoX5.addDialog( mono5, LabtechX )

def scoobyFive( event ) {
	random( playerSet ).say( "Non-destructive? It sure made a heck of a hole!" )
	myManager.schedule(2) { random( playerSet ).say( "Yeah...but no one was killed, remember?" ) }
	myManager.schedule(4) { random( playerSet ).say( "Yeah...no one but Labtech X anyway. Or at least that's what we were told..." ) }
	myManager.schedule(6) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono6Okay" )
			LabtechX.pushDialog( it, "monoX6" )
		}
		mono6Timer = myManager.schedule(15) {
			timer6Failed = true
			scoobySix( event )
		}
	}
}

//part 6
def monoX6 = LabtechX.createConversation( "monoX6", true, "Z13LabtechXMonologueAuthorized", "Z13Mono6Okay", "!Z13LabtechXMonologueFinished" )

def mono6 = [id:6]
mono6.npctext = "I was plunged over a cliff into the sea by the Gambino fission, falling to what seemed certain doom...utterly ignored by my own father, whose only thought was for Gino...with no thought for the fate of a *REPLACEABLE* clone."
mono6.flag = [ "!Z13Mono6Okay", "Z13FinishedMono6" ]
mono6.exec = { event ->
	numFinishedMono6 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono6 >= playerSet.size() && timer6Failed == false && scoobySixStarted == false ) { 
		scoobySixStarted = true
		mono6Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobySix( event )
	}
}
mono6.result = DONE
monoX6.addDialog( mono6, LabtechX )

def scoobySix( event ) {
	random( playerSet ).say( "Oh, poor baby. Daddy doesn't want you..." )
	myManager.schedule(1) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono7Okay" )
			LabtechX.pushDialog( it, "monoX7" )
		}
		mono7Timer = myManager.schedule(15) {
			timer7Failed = true
			scoobySeven( event )
		}
	}
}

//part 7
def monoX7 = LabtechX.createConversation( "monoX7", true, "Z13LabtechXMonologueAuthorized", "Z13Mono7Okay", "!Z13LabtechXMonologueFinished" )

def mono7 = [id:7]
mono7.npctext = "Mock me not! For while the Gambinos were distracted by the Zurg invasion, and everyone else thought I had perished, I summoned to my side those Labtechs I had suborned over the years, solidifying my power base from assets I had hidden while I ran *his* businesses."
mono7.flag = [ "!Z13Mono7Okay", "Z13FinishedMono7" ]
mono7.exec = { event ->
	numFinishedMono7 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono7 >= playerSet.size() && timer7Failed == false && scoobySevenStarted == false ) { 
		scoobySevenStarted = true
		mono7Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobySeven( event )
	}
}
mono7.result = DONE
monoX7.addDialog( mono7, LabtechX )

def scoobySeven( event ) {
	random( playerSet ).say( "Gotcha. You stole from your Dad and snuck around while everyone was busy defending the world." )
	myManager.schedule(2) { random( playerSet ).say( "Nice one!" ) }
	myManager.schedule(4) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono8Okay" )
			LabtechX.pushDialog( it, "monoX8" )
		}
		mono8Timer = myManager.schedule(15) {
			timer8Failed = true
			scoobyEight( event )
		}
	}
}

//part 8
def monoX8 = LabtechX.createConversation( "monoX8", true, "Z13LabtechXMonologueAuthorized", "Z13Mono8Okay", "!Z13LabtechXMonologueFinished" )

def mono8 = [id:8]
mono8.npctext = "I did not *sneak*. I investigated the energy that continued to resonate from the nearby fission crater, and built this complex of undersea laboratories from which to study it."
mono8.flag = [ "!Z13Mono8Okay", "Z13FinishedMono8" ]
mono8.exec = { event ->
	numFinishedMono8 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono8 >= playerSet.size() && timer8Failed == false && scoobyEightStarted == false ) { 
		scoobyEightStarted = true
		mono8Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyEight( event )
	}
}
mono8.result = DONE
monoX8.addDialog( mono8, LabtechX )

def scoobyEight( event ) {
	random( playerSet ).say( "And what are you doing with all these labs?" )
	myManager.schedule(2) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono9Okay" )
			LabtechX.pushDialog( it, "monoX9" )
		}
		mono9Timer = myManager.schedule(15) {
			timer9Failed = true
			scoobyNine( event )
		}
	}
}

//part 9
def monoX9 = LabtechX.createConversation( "monoX9", true, "Z13LabtechXMonologueAuthorized", "Z13Mono9Okay", "!Z13LabtechXMonologueFinished" )

def mono9 = [id:9]
mono9.npctext = "My ultimate purpose is now nearly complete. This laboratory houses my gargantuan Sealab X prototype, and in mere moments, I will infuse it with Ghi energy and force it to fully Animate!"
mono9.flag = [ "!Z13Mono9Okay", "Z13FinishedMono9" ]
mono9.exec = { event ->
	numFinishedMono9 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono9 >= playerSet.size() && timer9Failed == false && scoobyNineStarted == false ) { 
		scoobyNineStarted = true
		mono9Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyNine( event )
	}
}
mono9.result = DONE
monoX9.addDialog( mono9, LabtechX )

def scoobyNine( event ) {
	random( playerSet ).say( "Prototype?" )
	myManager.schedule(1) { random( playerSet ).say( "Gargantuan?" ) }
	myManager.schedule(2) { random( playerSet ).say( "*You* are going to Animate it? You can *do* that?!?" ) }
	myManager.schedule(4) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono10Okay" )
			LabtechX.pushDialog( it, "monoX10" )
		}
		mono10Timer = myManager.schedule(15) {
			timer10Failed = true
			scoobyTen( event )
		}
	}
}

//part 10
def monoX10 = LabtechX.createConversation( "monoX10", true, "Z13LabtechXMonologueAuthorized", "Z13Mono10Okay", "!Z13LabtechXMonologueFinished" )

def mono10 = [id:10]
mono10.npctext = "That's right, I have learned much since that fateful day on the ocean cliffs. Soon, the unattached Ghi energy in the area will copy my prototype, as is the way of the Animated everywhere, and a whole host of them will come into existence!"
mono10.flag = [ "!Z13Mono10Okay", "Z13FinishedMono10" ]
mono10.exec = { event ->
	numFinishedMono10 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono10 >= playerSet.size() && timer10Failed == false && scoobyTenStarted == false ) { 
		scoobyTenStarted = true
		mono10Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyTen( event )
	}
}
mono10.result = DONE
monoX10.addDialog( mono10, LabtechX )

def scoobyTen( event ) {
	random( playerSet ).say( "But...that's insane! The Animated hate us 'naturals'!" )
	myManager.schedule(2) { random( playerSet ).say( "You'll kill us all!" ) }
	myManager.schedule(4) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono11Okay" )
			LabtechX.pushDialog( it, "monoX11" )
		}
		mono11Timer = myManager.schedule(15) {
			timer11Failed = true
			scoobyEleven( event )
		}
	}
}

//part 11
def monoX11 = LabtechX.createConversation( "monoX11", true, "Z13LabtechXMonologueAuthorized", "Z13Mono11Okay", "!Z13LabtechXMonologueFinished" )

def mono11 = [id:11]
mono11.npctext = "Not so! I built slaving mechanisms into my prototype. When the Animated copy themselves, they will also copy those controls, and I shall be able to command these new Animated as my own private army!"
mono11.flag = [ "!Z13Mono11Okay", "Z13FinishedMono11" ]
mono11.exec = { event ->
	numFinishedMono11 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono11 >= playerSet.size() && timer11Failed == false && scoobyElevenStarted == false ) { 
		scoobyElevenStarted = true
		mono11Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyEleven( event )
	}
}
mono11.result = DONE
monoX11.addDialog( mono11, LabtechX )

def scoobyEleven( event ) {
	random( playerSet ).say( "He said 'army'. We can't fight an army!" )
	myManager.schedule(2) { random( playerSet ).say( "We may not have a choice!" ) }
	myManager.schedule(4) {
		playerSet.clone().each() {
			it.setQuestFlag( GLOBAL, "Z13Mono12Okay" )
			LabtechX.pushDialog( it, "monoX12" )
		}
		mono12Timer = myManager.schedule(15) {
			timer12Failed = true
			scoobyTwelve( event )
		}
	}
}

//part 12
def monoX12 = LabtechX.createConversation( "monoX12", true, "Z13LabtechXMonologueAuthorized", "Z13Mono12Okay", "!Z13LabtechXMonologueFinished" )

def mono12 = [id:12]
mono12.npctext = "Your choices are over! The infusion is complete and I leave you now to control my Sealab. You shall be the first to feel its full might, but certainly not the last. I regret that you shall not survive the encounter."
mono12.flag = ["Z13LabtechXMonologueFinished", "!Z13Mono12Okay", "Z13FinishedMono12" ]
mono12.exec = { event ->
	numFinishedMono12 ++
	//if everyone in the Crew has clicked "OK", then progress to the say bubbles and the next conversation section
	if( numFinishedMono12 >= playerSet.size() && timer12Failed == false && scoobyTwelveStarted == false ) { 
		scoobyTwelveStarted = true
		mono12Timer.cancel() //cancel the timer if everyone has already clicked "OK"
		scoobyTwelve( event )
	}
}
mono12.result = DONE
monoX12.addDialog( mono12, LabtechX )

def scoobyTwelve( event ) {
	LabtechX.warp( "Sealab_2", 500, 130 )
	monologueFinished = true
	random( playerSet ).say( "He teleported away!" )
	myManager.schedule(1) { random( playerSet ).say( "But to where? And for what?!?" ) }
	myManager.schedule(2) { random( playerSet ).say( "What's that coming out of the darkness above us?!?" ) }
	myManager.schedule(3) { random( playerSet ).say( "Look out! It's huge!" ) }
	myManager.schedule(4) { random( playerSet ).say( "That's what she said." ) }
	myManager.schedule(5) { spawnTail() }
}

//=========================================
// SEALAB X BOSS ACTION LOGIC              
//=========================================

//Possible Changes:
// - Deathmines should shower down randomly, but continuously during the battle with the Head Unit
// - More megalomaniac conversations and centerPrints during the combat.

// Body and Arms were balanced too tough. Faced separately, they were formidable. Together, they were ridiculous. Boss is 2/3rd previous strength. Arms are 75% of what they were. This is still a very tough fight.
// Arm missiles do more damage than the body blast now. Blowing the arms off is a very valid strategy since the arms have less health than they did previously.
// Head unit health increased since it has but a single attack.
// Head laser damage increased.
// Missile barrages are more spread out, fewer missiles, and continue until the Body is destroyed.
// The Tail and Body elements can't go to the left side of the room again (like it used to be), BUT...missiles, grunnies and death mines tend to favor targets in this "safe zone" instead of targets in the areas that can be attacked more readily.
// Built-in rest breaks to recover a bit between boss stages (not that many...don't get complacent).

//STAGE 1 - Tail + waves of six grunnies (full monsters that attack often)
//STAGE 2 - Body + arms + waves of missiles (one attack per missile)
//STAGE 3 - Head unit + continuous death mines

//INITIALIZATIONS
grunnyList = [ grunny1, grunny2 ]
safeZoneSet = [] as Set
targetSet = [] as Set
checkSafeSet = [] as Set
nonDazedSet = [] as Set
allDazed = false
lastSaying = null
bodyDead = false
headDead = false

//NO-ATTACK TRIGGER ZONE AND LIST

def safeZoneTrigger = "safeZoneTrigger"
myRooms.Sealab_106.createTriggerZone( safeZoneTrigger, 0, 480, 550, 1260 )

myManager.onTriggerIn(myRooms.Sealab_106, safeZoneTrigger) { event ->
	if( isPlayer( event.actor ) ) {
		safeZoneSet << event.actor
	}
}

myManager.onTriggerOut( myRooms.Sealab_106, safeZoneTrigger ) { event ->
	if( isPlayer( event.actor ) ) {
		safeZoneSet.remove( event.actor )
	}
}

//check every 2 seconds to see if players are Dazed. If so, then warp them out of the room and get things reset for the next entry
def playerDeathWatch() {
	if( allDazed == false ) {
		numDazed = 0
		playerSet.clone().each{
			if( it.isDazed() ) {
				numDazed ++
			}
		}
		if( numDazed >= playerSet.size() ) {
			allDazed = true
			playerSet.clone().each{ it.centerPrint( "Ha! HA! Mwu-ha-HA! Not so self-righteous and indignant NOW are you?!?" ) }
			myManager.schedule(3) { 
				playerSet.clone().each{
					it.centerPrint( "Grunnies! Dump these bodies in the tunnels." )
					playerSet.clone().each{
						X = it.getX().intValue()
						Y = it.getY().intValue()
						spawner = random( grunnyList )
						spawner.setInitialMoveForChildren( "Sealab_106", X, Y )
						spawner.forceSpawnNow()				
					}
				}
				myManager.schedule(4) {
					if(headDead == false) {
						getAreaTeam().setAreaVar( "Z12_Undersea_Cliffs", "Z12RespawnRobofish", 1 )
						playerSet.clone().each{ it.warp( "Sealab_1", 470, 500 ) }
						myManager.schedule(1) {
							myRooms.Sealab_106.getActorList().each { if( isMonster( it ) ) { it.dispose() } }
							safeZoneSet.clear()
							targetSet.clear()
							checkSafeSet.clear()
							nonDazedSet.clear()
							bodyDead = false
							headDead = false
						}
					}					
				}
			}
		} else {
			myManager.schedule(2) { playerDeathWatch() }
		}
	}
}

			

//-------------------------------------------
//STAGE 1 - Boss Tail descends from ceiling  
//-------------------------------------------

//-------------------------------------------
//Sealab Tail Logic                          
//-------------------------------------------
def spawnTail() {
	playerDeathWatch()
	adjustML()
	tail = tailSpawner.forceSpawnNow()
	tail.setMostHatedRadius( 600 )
	//Set movement constraints for the tail
	tail.setMovementBoundary( 550, 0, 1900, 1035 )
	
	//Summon grunnies at each 20% of health loss.
	//Have the tail pause for a few seconds to allow the grunny summon animation to occur before it moves again.
	myManager.onHealth( tail ) { event ->
		if( event.didTransition( 75 ) && event.isDecrease() ) {
			tail.getAI().setNextAttackIndex( 1 ); numSpawn = playerSet.size(); targetSet = playerSet.clone(); summonGrunnies(); playerSet.each{ it.centerPrint( "Come to me, my Grunnies!" ) }
		} else if ( event.didTransition( 50 ) && event.isDecrease() ) {
			tail.getAI().setNextAttackIndex( 1 ); numSpawn = playerSet.size(); targetSet = playerSet.clone(); summonGrunnies(); playerSet.each{ it.centerPrint( "Grunnies! Protect me!" ) }
		} else if ( event.didTransition( 25 ) && event.isDecrease() ) {
			tail.getAI().setNextAttackIndex( 1 ); numSpawn = playerSet.size(); targetSet = playerSet.clone(); summonGrunnies(); playerSet.each{ it.centerPrint( "Don't just hide, you radioactive rabbits. Help me!" ) }
		}
	}
	runOnDeath( tail, {
		myManager.schedule(5) {
			playerSet.each{ it.centerPrint( "Ha! You think yourselves victorious, but the loss of the tail unit is nothing!" ) }
			myManager.schedule(3) { playerSet.each{ it.centerPrint( "This Animated is entirely modular. As you will soon find out!" ) } }
			myManager.schedule(6) { playerSet.each{ it.centerPrint( "But meanwhile, let the destruction rain down!" ) } }
		}
		myManager.schedule(13) {
			missileCount = playerSet.size() * 2
			targetSet = playerSet.clone()
			missileBarrage()
			myManager.schedule(30) {
				playerSet.each{
					it.centerPrint( "Now you face the awesome might of the main battle structure of this prototype Animated!" )
				}
				myManager.schedule(2) { spawnBody() }
			}
		}
	} )
}


//-------------------------------------------
//Grunny Summoner Routine                    
//-------------------------------------------

def summonGrunnies() { 
	adjustML()
	if( numSpawn > 0 ) {
		spawner = random( grunnyList )
		if( spawner.spawnsInUse() < 10 ) {
			g1 = spawner.forceSpawnNow()
			g1.setAwarenessRadius( 2000 )
			g1.setHateRadius( 2000 )
			targetSet.clone().each{ if( it.isDazed() ) { targetSet.remove( it ) } }
			if( !targetSet.isEmpty() ) {
				target = random( targetSet )
				targetSet.remove( target )
			} else {
				checkSafeSet = safeZoneSet.clone()
				checkSafeSet.clone().each{ if( it.isDazed() ) { checkSafeSet.remove( it ) } }
				if( !checkSafeSet.isEmpty() ) {
					target = random( checkSafeSet ) 
				} else {
					nonDazedSet = playerSet.clone()
					nonDazedSet.clone().each{ if( it.isDazed() ) { nonDazedSet.remove( it ) } }
					if( !nonDazedSet.isEmpty() ) {
						target = random( nonDazedSet )
					} else {
						target = random( playerSet )
					}
				}
			}
			g1.addHate( target, 200 )
		}
		numSpawn --
		myManager.schedule(2) { summonGrunnies() }
	}
}

//-------------------------------------------
//STAGE 2 - Body, Arms, and Missile Barrages 
//-------------------------------------------

//-------------------------------------------
//Spawn the Body and Arms                    
//-------------------------------------------

def spawnBody() {
	adjustML()
	body = bodySpawner.forceSpawnNow()

	rArm = rightArm.forceSpawnNow()
	rArm.setMostHatedRadius( 400 )
	rArm.setMovementParent( body, -490, -120 ) // see rightArm.setPos() above.

	lArm = leftArm.forceSpawnNow()
	lArm.setMostHatedRadius( 400 )
	lArm.setMovementParent( body, 160, 70 ) // see leftArm.setPos() above.
	
	//Link the arms to the body so that when the body dies, so do the arms
	body.addDeathLink( rArm )
	body.addDeathLink( lArm )
	//Set movement constraints for the body
	body.setMovementBoundary( 900, 0, 1500, 1035 )

	runOnDeath( body, {
		myManager.schedule(3) {
			bodyDead = true
			playerSet.each{ it.centerPrint( "NO! How is this possible?" ) }
			myManager.schedule(3) { playerSet.each{ it.centerPrint( "But this is just a setback. With the detached Head unit, I can still remake the Sealabs." ) } }
			myManager.schedule(6) { playerSet.each{ it.centerPrint( "My plan can still succeed! But first...you all must die." ) } }
			myManager.schedule(9) { playerSet.each{ it.centerPrint( "My Animated mines will keep you busy while I prepare the Head unit for your destruction!" ) } }
			myManager.schedule(10) { mineBarrage() }
			myManager.schedule(30) { spawnHead() }
		}
	} )
}


//-------------------------------------------
//Missile Barrage Routine                    
//-------------------------------------------

//spawn waves of missiles, 30 seconds apart, targeting each member of the Crew separately, until the Body is destroyed

def missileBarrage() {
	if( allDazed == true ) return
	adjustML()
	if( missileCount > 0 ) {
		launcher = random( missileList )
		m1 = launcher.forceSpawnNow()
		//Try to target each player in the room.
		//If less than six people are in the room, then assign one per Crew until each person has one missile, then randomly assign the others
		if( !targetSet.isEmpty() ) {
			target = random( targetSet )
			targetSet.remove( target )
		} else {
			checkSafeSet = safeZoneSet.clone()
			checkSafeSet.clone().each{ if( it.isDazed() ) { checkSafeSet.remove( it ) } }
			if( !checkSafeSet.isEmpty() ) {
				target = random( checkSafeSet ) 
			} else {
				nonDazedSet = playerSet.clone()
				nonDazedSet.clone().each{ if( it.isDazed() ) { nonDazedSet.remove( it ) } }
				if( !nonDazedSet.isEmpty() ) {
					target = random( nonDazedSet )
				} else {
					if( !playerSet.isEmpty() ) {
						target = random( playerSet )
					}
				}
			}
		}
		m1.addHate( target, 5000 ) //missiles can't be taunted
		missileCount --
		myManager.schedule(1) { missileBarrage() }
	} else {
		myManager.schedule( random( 20, 40 ) ) { prepNextBarrage() } //this sets the interval between missile barrage waves
	}
}

missileSayings = [ "I'll crush you ALL!", "Death from above!", "You cannot win!", "Prepare to be torpedoed!! haHAhaHAha!", "Bow before the might of SEALAB!", "Doomed! Doomed! DOOMED I SAY!", "It will *all* be mine! At last!" ]

def chooseASaying() {
	saying = random( missileSayings )
	if( lastSaying == saying ) { 
		chooseASaying()
	} else {
		lastSaying = saying
	}
}

def prepNextBarrage() {
	if( allDazed == true ) return
	println "**** body = ${ body } ****"
	if( bodyDead == false ) {
		playerSet.clear()
		myRooms.Sealab_106.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
		chooseASaying()
		playerSet.each{ it.centerPrint( saying ) }
		targetSet = playerSet.clone()
		missileCount = playerSet.size() * 2
		missileBarrage()
	}
}


//-------------------------------------------
//STAGE 3 - Head Unit and Deathmines         
//-------------------------------------------

//Fire off a mine every 6 seconds to attack a random player in the room
def mineBarrage() {
	if( allDazed == true ) return
	//adjustML()
	if( headDead == false ) {
		spawnPoint = random( deathmineList )
		d1 = spawnPoint.forceSpawnNow()
		playerSet.clear()
		myRooms.Sealab_106.getActorList().each { if( isPlayer( it ) ) { playerSet << it } }
		//check to see if anyone is dazed so as to attack only those awake targets
		nonDazedSet.clear()
		nonDazedSet = playerSet.clone()
		nonDazedSet.clone().each{ if( it.isDazed() ) { nonDazedSet.remove( it ) } }
		//attack a non-dazed target, if possible...otherwise just attack SOMEONE
		if( !nonDazedSet.isEmpty() ) {
			d1.addHate( random( nonDazedSet ), 1 )
		} else {
			if( !playerSet.isEmpty() ) {
				d1.addHate( random( playerSet ), 1 )
			}
		}
		myManager.schedule(6) { mineBarrage() }
	}
}

def spawnHead() {
	adjustML()
	head = headUnit.forceSpawnNow()
	myManager.schedule(3) { head.say( "Mwu-ha-ha! This head unit is protected by invulnerable force fields. It will make short work of you!" ) }
	myManager.onHealth( head ) { event ->
		if( event.didTransition( 75 ) && event.isDecrease() ) {
			playerSet.each{ it.centerPrint( "What? The force field compensators should be nullifying the damage!" ) }
		} else if ( event.didTransition( 50 ) && event.isDecrease() ) {
			playerSet.each{ it.centerPrint( "Must...defeat...you all! This Sealab must not be destroyed!" ) }
		} else if ( event.didTransition( 25 ) && event.isDecrease() ) {
			playerSet.each{ it.centerPrint( "There are labtechs out there that will pay for this failure." ) }
		} else if ( event.didTransition( 5 ) && event.isDecrease() ) {
			playerSet.each{ it.centerPrint( "Engaging escape pod failsafe routines." ) }
		}
	}
	runOnDeath( head, { headDead = true; myManager.schedule(3) { endGame() } } )
}


//-------------------------------------------
//END GAME - After Head Unit destroyed       
//-------------------------------------------

def endGame() {
	//destroy all existing deathmines
	myRooms.Sealab_106.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "sealab_deathmine" ) {
			it.instantPercentDamage(100)
		}
	}
	playerSet.clone().each{ it.centerPrint( "You did it! You stopped the nefarious plan of Labtech X!" ) }
	myManager.schedule(2) { playerSet.clone().each{ it.centerPrint( " Congratulations!!!" ) } }
	myManager.schedule(3) {
		playerSet.clone().each{ 
			//now give everyone a reward so the end of the fight is gratifying and encourages repeat play
			if( it.getConLevel() <= MaxML ) {

				//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
				if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
				else if( difficulty == 2 ) { goldMult = 3; orbMult = 1.5 }
				else if( difficulty == 3 ) { goldMult = 8; orbMult = 3 }

				//gold reward
				goldGrant = ( random( 2500, 5000 ) * goldMult ).intValue() 
				it.centerPrint( "You receive ${goldGrant} gold!" )
				it.grantCoins( goldGrant )
				
				goldDebug.put(it, goldGrant)

				//orb reward
				orbGrant = ( random( 10, 20 ) * orbMult ).intValue()
				it.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				it.grantQuantityItem( 100257, orbGrant )

			} else if( it.getConLevel() > MaxML ) {
				it.centerPrint( "Your level is too high for this scenario. No reward for you!" )
			}
			//println "^^^^ goldDebug = ${goldDebug} ^^^^"
		}
	}
	
	myManager.schedule(6) { playerSet.clone().each{ it.centerPrint( "Now to skip through all that mundane 'traveling home' stuff..." ) } }
	myManager.schedule(9) { playerSet.clone().each{ it.centerPrint( "...and go straight to your triumphant return home at Barton's Bar!" ) } }
	myManager.schedule(14) {
		playerSet.clone().each{
			it.setQuestFlag( GLOBAL, "Z13BossDead" )
			if( !it.hasQuestFlag( GLOBAL, "Z9_Jacques_MarshallSaved" ) ) {
				it.setQuestFlag( GLOBAL, "Z9_Jacques_MarshallSaved" )
			}
			it.warp( "Bar_1", 880, 820 )
		}
	}
}



