import com.gaiaonline.mmo.battle.script.*;
import com.gaiaonline.mmo.battle.*;

// EXAMPLE, do not enable.
// AreaInstance.setAreaRingLevelCap( myRooms.nullChamber1_1.getArea(), 2.4 )

toAqueduct = makeSwitch( "crystal7", myRooms.nullChamber1_1, 440, 380 )
toBeach = makeSwitch( "crystal6", myRooms.nullChamber1_1, 1150, 335 )
toLake = makeSwitch( "crystal4", myRooms.nullChamber1_1, 1140, 650 )
toGardens = makeSwitch( "crystal8", myRooms.nullChamber1_1, 415, 680 )
toVillage = makeSwitch( "crystal1", myRooms.nullChamber1_1, 680, 760 )
toRanch = makeSwitch( "crystal2", myRooms.nullChamber1_1, 930, 760 )

// Here is how you add mouseover text -Ryan
toAqueduct.setMouseoverText("Old Aqueduct")
toBeach.setMouseoverText("Gold Beach")
toLake.setMouseoverText("Bass'ken Lake")
toGardens.setMouseoverText("Zen Gardens")
toVillage.setMouseoverText("Village Greens")
toRanch.setMouseoverText("Bill's Ranch")

toAqueduct.unlock()
toBeach.unlock()
toLake.unlock()
toGardens.unlock()
toRanch.unlock()
toVillage.unlock()

toAqueduct.off()
toBeach.off()
toLake.off()
toGardens.off()
toRanch.off()
toVillage.off()

toAqueduct.setRange( 250 )
toBeach.setRange( 250 )
toLake.setRange( 250 )
toGardens.setRange( 250 )
toVillage.setRange( 250 )
toRanch.setRange( 250 )

//-------------------------------------------
// ATTUNEMENT MESSAGE                        
//-------------------------------------------

attunementMessage = tutorialNPC.createConversation( "attunementMessage", true, "Z21AttunementMessageOkay" )

def attune1 = [id:1]
attune1.npctext = "<h1><b><font face='Arial' size='14'>Attuning a Crystal</font></b></h1><font face='Arial' size ='12'>This crystal is not yet attuned to you.<p></p><p></p>Each of the crystals you find out in the world is linked to one of the crystals here in this Null Chamber.<p></p><p></p>Once you touch a crystal outside this chamber, you'll be transported to this Null Chamber. From then on, that crystal is 'attuned' to you and you can use that crystal to warp back to that outside location.<p></p><p></p>Eventually, after you explore the world fully, the Null Chamber becomes a transportation nexus letting you quickly warp from place to place.<br><br>"
attune1.flag = "!Z21AttunementMessageOkay"
attune1.result = DONE
attunementMessage.addDialog( attune1, tutorialNPC )



// This is what happens each time the switch is clicked
def goToAqueduct = { event ->
	if( event.actor.hasQuestFlag( GLOBAL, "Z21AqueductWarpOkay" ) ) {
		toAqueduct.lock()
		myManager.schedule(2) { toAqueduct.unlock() }
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "Aqueduct_602", 1450, 810, 6 )
	} else {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z21AttunementMessageOkay" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z21AttunementMessageOkay" )
			tutorialNPC.pushDialog( event.actor, "attunementMessage" )
		}
	}
	myManager.schedule(1) { toAqueduct.off() }
}

def goToBeach = { event ->
	if( event.actor.hasQuestFlag( GLOBAL, "Z21BeachWarpOkay" ) ) {
		toBeach.lock()
		myManager.schedule(2) { toBeach.unlock() }
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "Beach_402", 540, 635, 6 )
	} else {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z21AttunementMessageOkay" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z21AttunementMessageOkay" )
			tutorialNPC.pushDialog( event.actor, "attunementMessage" )
		}
	}
	myManager.schedule(1) { toBeach.off() }
}

def goToLake = { event ->
	if( event.actor.hasQuestFlag( GLOBAL, "Z21LakeWarpOkay" ) ) {
		toLake.lock()
		myManager.schedule(2) { toLake.unlock() }
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "BASSKEN_304", 390, 560, 6 )
	} else {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z21AttunementMessageOkay" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z21AttunementMessageOkay" )
			tutorialNPC.pushDialog( event.actor, "attunementMessage" )
		}
	}
	myManager.schedule(1) { toLake.off() }
}

def goToGardens = { event ->
	if( event.actor.hasQuestFlag( GLOBAL, "Z21GardensWarpOkay" ) ) {
		toGardens.lock()
		myManager.schedule(2) { toGardens.unlock() }
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "ZENGARDEN_502", 635, 565, 6 )
	} else {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z21AttunementMessageOkay" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z21AttunementMessageOkay" )
			tutorialNPC.pushDialog( event.actor, "attunementMessage" )
		}
	}
	myManager.schedule(1) { toGardens.off() }
}

def goToRanch = { event ->
	if( event.actor.hasQuestFlag( GLOBAL, "Z21RanchWarpOkay" ) ) {
		toRanch.lock()
		myManager.schedule(2) { toRanch.unlock() }
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "BF1_404", 350, 375, 6 )
	} else {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z21AttunementMessageOkay" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z21AttunementMessageOkay" )
			tutorialNPC.pushDialog( event.actor, "attunementMessage" )
		}
	}
	myManager.schedule(1) { toRanch.off() }
}

//NOTE: The Village crystal is ALWAYS available to players (otherwise they could get stuck in the null chamber with no way out if they haven't touched a crystal anywhere yet...)
def goToVillage = { event ->
	toVillage.lock()
	myManager.schedule(2) { toVillage.unlock() }
	event.actor.centerPrint( "A powerful force moves you through space..." )
	event.actor.warp( "VILLAGE_206", 390, 370, 6 )
	myManager.schedule(1) { toVillage.off() }
}

toAqueduct.whenOn( goToAqueduct )
toBeach.whenOn( goToBeach )
toLake.whenOn( goToLake )
toGardens.whenOn( goToGardens )
toVillage.whenOn( goToVillage )
toRanch.whenOn( goToRanch )

/* *****************************************************************************
//===============================================================
// BOT KIOSK                                                     
//===============================================================

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("kioskPosition", "markerOther", "nullChamber1_1", 990, 445, "HyperNet Access")

kiosk = makeSwitch( "botKiosk", myRooms.nullChamber1_1, 990, 445 )
kiosk.unlock()
kiosk.off()
kiosk.setRange( 400 )
kiosk.setMouseoverText("Access the HyperNet")

def clickKiosk = { event ->
	kiosk.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk.whenOn( clickKiosk )


//====================================================
// TUTORIAL TREE                                      
//====================================================

kioskWidth = 300

def giveReward( event ) {
	goldGrant = random( 50, 150 ) 
	event.player.centerPrint( "You receive ${goldGrant} gold!" )
	event.player.grantCoins( goldGrant )

	//orb reward
	orbGrant = 2
	event.player.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
	event.player.grantQuantityItem( 100257, orbGrant )
}
	
def synchronized makeKioskMenu( player ) {
	titleString = "The HyperNet"
	descripString = "Choose Game Help to access loads of information about the game, or one of the other categories for easy downloads of useful tidbits."
	diffOptions = ["Game Help", "Tutorials", "Area Info", "Exit HyperNet"]
	
	uiButtonMenu( player, "kioskMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Game Help" ) {
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXGameHelpSelected" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXGameHelpSelected" )
				//gold reward
				goldGrant = random( 50, 200 ) 
				event.actor.centerPrint( "You receive ${goldGrant} gold!" )
				event.actor.grantCoins( goldGrant )

				//orb reward
				orbGrant = 2
				event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				event.actor.grantQuantityItem( 100257, orbGrant )
			}
			showWidget( event.actor, "GameHelp", true, true )
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
		if( event.selection == "Tutorials" ) {
			makeTutorialMenu( player )
		}
		if( event.selection == "Area Info" ) {
			makeAreaMenu( player )
		}
		if( event.selection == "Exit HyperNet" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
	}
}

def makeAreaMenu( player ) {
	titleString = "Area Info"
	descripString = "A small archive of stories and tidbits of the area around you."
	diffOptions = ["The HyperNet", "The Null Chamber", "We're Immortal?", "Why Do We Use Rings?", "Drawbacks to Rings", "Main Menu"]
	
	uiButtonMenu( player, "fictionMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "The HyperNet" ) {
			tutorialNPC.pushDialog( event.actor, "hyperNet" )
		}
		if( event.selection == "The Null Chamber" ) {
			tutorialNPC.pushDialog( event.actor, "nullChamber" )
		}
		if( event.selection == "We're Immortal?" ) {
			tutorialNPC.pushDialog( event.actor, "immortality" )
		}
		if( event.selection == "Why Do We Use Rings?" ) {
			tutorialNPC.pushDialog( event.actor, "whyUseRings" )
		}
		if( event.selection == "Drawbacks to Rings" ) {
			tutorialNPC.pushDialog( event.actor, "ringDrawbacks" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}
	
//Attuning Null Crystals
//Salvaging Rings

def makeTutorialMenu( player ) {
	titleString = "Tutorials"
	descripString = "These tutorials give you quick overviews on various features in the game."
	diffOptions = ["Attuning Null Crystals", "Upgrading Rings", "Salvaging Rings", "Changing your Level", "Dazed Tips (on/off)", "Main Menu"]
	
	uiButtonMenu( player, "tutorialMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Attuning Null Crystals" ) {
			tutorialNPC.pushDialog( event.actor, "attuningCrystals" )
		}
		if( event.selection == "Upgrading Rings" ) {
			tutorialNPC.pushDialog( event.actor, "upgradingRings" )
		}
		if( event.selection == "Salvaging Rings" ) {
			tutorialNPC.pushDialog( event.actor, "salvagingRings" )
		}
		if( event.selection == "Changing your Level" ) {
			tutorialNPC.pushDialog( event.actor, "suppressCL" )
		}
		if( event.selection == "Dazed Tips (on/off)" ) {
			if( !event.actor.hasQuestFlag( GLOBAL, "Z21DazedTipsExplanationGiven" ) ) {
				tutorialNPC.pushDialog( event.actor, "dazedTips" )
			} else {
				player = event.actor
				makeDazedTipsMenu( player )
			}
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// TUTORIAL MENU                                                                        
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//ATTUNING NULL CRYSTALS                                                                
//--------------------------------------------------------------------------------------
attuningCrystals = tutorialNPC.createConversation( "attuningCrystals", true )

def tune1 = [id:1]
tune1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Attuning Null Crystals</font></b></h1><br><font face='Arial' size ='12'>Touching a Null Crystal is how you move from the world of Gaia to...wherever...this Null Chamber is physically located.<br><br>Each Null Crystal links to its twin here in the Null Chamber, but until you touch the Crystal out in the world, you can't use its twin here in the Chamber to exit the Null Chamber.<br><br>So at first, the only way you can leave the Chamber is out to Village Greens. But as you find more and more of the Null Crystals scattered throughout the world, you can touch them and 'attune' yourself to them, opening up more and more connections as you go.<br><br>]]></zOMG>"
tune1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXAttuningCrystals" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXAttuningCrystals" )
			giveReward( event )
		}
	}
}
tune1.result = DONE
attuningCrystals.addDialog( tune1, tutorialNPC )

//--------------------------------------------------------------------------------------
//UPGRADING RINGS TUTORIAL                                                              
//--------------------------------------------------------------------------------------
upgradingRings = tutorialNPC.createConversation( "upgradingRings", true )

def upgrade1 = [id:1]
upgrade1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Upgrading Rings</font></b></h1><br><font face='Arial' size ='12'>Upgrading rings is simple.<br><br>First, open your inventory pane.<br><br>Then select a ring and hit the 'UPGRADE' button on the right side of the inventory pane.<br><br>If you have enough Charge Orbs available, they'll then be pushed into the ring, increasing its Charge Level and making it more powerful.<br><br>]]></zOMG>"
upgrade1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXUpgradingRings" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXUpgradingRings" )
			giveReward( event )
		}
	}
}
upgrade1.result = DONE
upgradingRings.addDialog( upgrade1, tutorialNPC )

//--------------------------------------------------------------------------------------
//SALVAGING RINGS                                                                       
//--------------------------------------------------------------------------------------
salvagingRings = tutorialNPC.createConversation( "salvagingRings", true )

def salvage1 = [id:1]
salvage1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Salvaging Rings</font></b></h1><br><font face='Arial' size ='12'>Sometimes, while you adventure, you'll find yourself with a ring you don't want, either because it's a duplicate, or because you're just not interested in it.<br><br>The power within that ring can be Salvaged, releasing a portion of the charge orbs that were infused into it, and allowing you to move those orbs to a different ring.<br><br>To Salvage a ring, just open up your inventory pane, select the ring, and then click the 'SALVAGE' button on the right side of the pane.<br><br>]]></zOMG>"
salvage1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXSalvagingRings" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXSalvagingRings" )
			giveReward( event )
		}
	}
}
salvage1.result = DONE
salvagingRings.addDialog( salvage1, tutorialNPC )

//--------------------------------------------------------------------------------------
//SUPPRESS CL TUTORIAL                                                                  
//--------------------------------------------------------------------------------------
suppressCL = tutorialNPC.createConversation( "suppressCL", true )

def suppress1 = [id:1]
suppress1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Changing your Level</font></b></h1><br><font face='Arial' size ='12'>Once you've built your level up a bit, you'll find that certain events and hunting areas are off-limits to you because you're too powerful.<br><br>Never fear! You can still play all that content!<br><br>Click on the 'MENU' button on your hotbar. Then choose the 'CHANGE LEVEL' option.<br><br>Then you can easily drag your level down to the level you want to play at and join your friends for fun!<br><br><b>To reset your level back up to max, use the 'CHANGE LEVEL' option from the 'MENU' button again..</b><br><br>]]></zOMG>"
suppress1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXSuppressCL" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXSuppressCL" )
			giveReward( event )
		}
	}
}
suppress1.result = DONE
suppressCL.addDialog( suppress1, tutorialNPC )

//--------------------------------------------------------------------------------------
//DAZED TIPS (on/off) TUTORIAL                                                          
//--------------------------------------------------------------------------------------
def dazedTips = tutorialNPC.createConversation( "dazedTips", true )

def dazed1 = [id:1]
dazed1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Dazed Tips</font></b></h1><br><font face='Arial' size ='12'>Each time you return to the Null Chamber after being Dazed, Trixie will prompt you with game hints and tips to let you learn more about the game as you play.<br><br>These tips are very useful, but if you eventually decide to turn them off, you can choose to turn them OFF when prompted after this dialog.<br><br>]]></zOMG>"
dazed1.flag = "Z21DazedTipsExplanationGiven"
dazed1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeDazedTipsMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXDazedTips" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXDazedTips" )
			giveReward( event )
		}
	}
}
dazed1.result = DONE
dazedTips.addDialog(dazed1, tutorialNPC)

def makeDazedTipsMenu( player ) {
	titleString = "Dazed Tips (on/off)"
	descripString = "Turn on/off the hints and tips that Trixie gives you when you come to the Null Chamber after being Dazed."
	diffOptions = ["Dazed Tips ON", "Dazed Tips OFF"]
	
	uiButtonMenu( player, "dazedTipsOnOff", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Dazed Tips ON" ) {
			event.actor.setPlayerVar( "Z21TrixieHints", 0 ) //"0" sets it to ON.
			println "**** Z21TrixieHints = ${event.actor.getPlayerVar( "Z21TrixieHints" ) } ****"
			event.actor.centerPrint( "Dazed Tips are ON" )
		}
		if( event.selection == "Dazed Tips OFF" ) {
			event.actor.setPlayerVar( "Z21TrixieHints", 1 ) //"1" sets it to OFF. (Defaults to ON.)
			println "**** Z21TrixieHints = ${event.actor.getPlayerVar( "Z21TrixieHints" ) } ****"
			event.actor.centerPrint( "Dazed Tips are OFF" )
		}
		myManager.schedule(2) {
			player = event.actor
			makeTutorialMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// AREA MENU                                                                            
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//HYPERNET BACKGROUND                                                                   
//--------------------------------------------------------------------------------------
def hyperNet = tutorialNPC.createConversation( "hyperNet", true )

def hyper1 = [id:1]
hyper1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Origins of the HyperNet</font></b></h1><br><font face='Arial' size ='12'>The town of Aekea is well-known for its AI (artificial intelligence) personalities.<br><br>Those AIs move around in robotic bodies, but they aren't constrained to those mere metal forms.<br><br>The vast intellects of the AIs dwell within a cyberspace area that they call the 'HyperNet'.<br><br>The AIs decided long ago to let Gaians access that thoughtsphere and tap it for data when needed.<br><br>Try not to think too hard about the fact that you're rummaging around in someone's mind for this information.<br><br>]]></zOMG>"
hyper1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXHyperNet" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXHyperNet" )
			giveReward( event )
		}
	}
}
hyper1.result = DONE
hyperNet.addDialog( hyper1, tutorialNPC )

//--------------------------------------------------------------------------------------
//NULL CHAMBER BACKGROUND                                                               
//--------------------------------------------------------------------------------------
def nullChamber = tutorialNPC.createConversation( "nullChamber", true )

def null1 = [id:1]
null1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>The Null Chamber</font></b></h1><br><font face='Arial' size ='12'>Where is the Null Chamber located? No one knows.<br><br> Why does it exist? No one knows.<br><br>How do we get warped here when we touch a Null Crystal? No one knows.<br><br>However, the Null Chamber is very special.<br><br>Somehow, the Chamber blocks the energy the binds your rings to your fingers. You can freely swap rings around when you are here, and you can also upgrade your rings to invest Charge Orbs into them and make your rings more powerful.<br><br>]]></zOMG>"
null1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXNullChamber" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXNullChamber" )
			giveReward( event )
		}
	}
}
null1.result = DONE
nullChamber.addDialog( null1, tutorialNPC )

//--------------------------------------------------------------------------------------
//IMMORTALITY BACKGROUND                                                                
//--------------------------------------------------------------------------------------
def immortality = tutorialNPC.createConversation( "immortality", true )

def imm1 = [id:1]
imm1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>We're IMMORTAL?</font></b></h1><br><font face='Arial' size ='12'>When you enter the Null Chamber, it somehow stores your essence.<br><br>Whenever you're unconscious, you can release your essence and reform it in the Chamber.<br><br>Again...don't ask how this works. We don't know. But it *does* work.<br><br>So for all intents and purposes, you are now immortal and anytime you're Dazed, you can just release and reform in the Chamber.<br><br>]]></zOMG>"
imm1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXImmortality" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXImmortality" )
			giveReward( event )
		}
	}
}
imm1.result = DONE
immortality.addDialog( imm1, tutorialNPC )

//--------------------------------------------------------------------------------------
//WHY USE RINGS BACKGROUND                                                              
//--------------------------------------------------------------------------------------
def whyUseRings = tutorialNPC.createConversation( "whyUseRings", true )

def use1 = [id:1]
use1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Why Do We Use Rings?</font></b></h1><br><font face='Arial' size ='12'>The rings are created *within* the Animated themselves. (Sort of like pearls inside of oysters.)<br><br>Apparently, those rings are a conduit to the same power that drives the Animated. Because of this, the rings also end up being weapons that we can use to defeat the Animated.<br><br>And that's a good thing because normal weaponry doesn't do *anything* against the Animated.<br><br>]]></zOMG>"
use1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXWhyUseRings" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXWhyUseRings" )
			giveReward( event )
		}
	}
}
use1.result = DONE
whyUseRings.addDialog( use1, tutorialNPC )

//--------------------------------------------------------------------------------------
//RING DRAWBACKS BACKGROUND                                                             
//--------------------------------------------------------------------------------------
def ringDrawbacks = tutorialNPC.createConversation( "ringDrawbacks", true )

def draw1 = [id:1]
draw1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Drawbacks to Rings?</font></b></h1><br><font face='Arial' size ='12'>So you can't affect the Animated without rings, but rings come from within the Animated, and the Charge Orbs we use to power up the rings *also* come from the Animated.<br><br>Doesn't that seem peculiar?<br><br>Yes, it does. It's one of the mysteries we're investigating.<br><br>But, since we have to resist the Animated, we have no choices, so we're just going with it for now...and hope we're part of the solution, and not adding to the problem.<br><br>]]></zOMG>"
draw1.exec = { event ->
	if( event.actor.getRoom() == myRooms.nullChamber1_1 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXRingDrawbacks" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXRingDrawbacks" )
			giveReward( event )
		}
	}
}
draw1.result = DONE
ringDrawbacks.addDialog( draw1, tutorialNPC )

***************************************************************************** */
