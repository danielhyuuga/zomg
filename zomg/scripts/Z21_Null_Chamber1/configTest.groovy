
// [rc] Example listener
list = [ configKeyUpdated : { event -> 
	println "^^^^^^^^^^^^^^^ GOT CONFIG UPDATE:  $event ^^^^^^^^^^^^^^^^^^"
} ] as com.gaiaonline.mmo.ISiteConfig.ConfigKeyListener

// [rc] Uncomment the following line to start litsening for the config value "test_flag"
//def initialValue = getSiteConfig().addKeyListener("zomg", "test_flag", list)

/* Notes

 - Make sure to use the initial value returned by addKeyListener.  Or call getSiteConfig().get() after you add the listener.
 - All config keys and values are String
 - You can change a config with the admin panel here:  http://ztrunk.open.dev.gaiaonline.com/admin/    (Then click on "Config Editor")
 - Configs are cached so it can be a minute or two until the GSI call is returning the most recent version.
 - If you declare the listener with "def" then it might be garbage collected once the script is finished compiling.  However, if you declare it as a class variable, it appears to never be gc'd.  So, you may need to set up a way to remove the listener.

 */
