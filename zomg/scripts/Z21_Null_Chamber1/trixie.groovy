import com.gaiaonline.mmo.battle.script.*;

Trixie = spawnNPC("Trixie-VQS", myRooms.nullChamber1_1, 560, 570)
Trixie.setRotation( 45 )
Trixie.setDisplayName( "Trixie" )

hintList = []

hintList << "If you use Rage attacks, you'll get bigger effects for the same amount of Stamina."

hintList << "A quick Kneel gives you a breather between attacks...just don't get caught kneeling!"

hintList << "If you have a ring with Knockback, try knocking one target back, then switching targets to attack with another ring, then knock the original target back again when it returns to the fray."

hintList << "If you're faster than your target, then try running away a bit, hitting with a ranged attack, running away a bit, etc. Lather, Rinse, Repeat."

hintList << "Be sure you have Stamina remaining before you charge into battle. "

hintList << "If you attract the attention of too many monsters, you may find yourself running out of Stamina during the fight. Watch your Stamina meter!"

hintList << "In general, long-ranged attacks do less damage than close-ranged attacks. But not all monsters can hit you at range, so close-up attacks will get you damaged more often. Be aware of those choices!"

hintList << "Hit the SPACEBAR to select yourself quickly. This lets you use target yourself for buffs and heals very easily."

hintList << "You can sometimes 'pull' a monster toward you with a ranged attack so you can fight it at a safe distance from its friends. This has to be done carefully, but can be very effective."

hintList << "Running is honorable. If you're in over your head, run away and heal up. It's faster than coming back to the Null Chamber all the time."

hintList << "Don't be in a rush! Take your time, try to plan your attack."

hintList << "If you are in a new place, exploring, sit down and recover your stamina to full whenever you have a chance, cause you never know when the next big fight is coming."

hintList << "When using a healing ring, it's best not to wait till the last minute. Stamina conservation is great, but not getting Dazed is even cooler."

hintList << "Defensive buff rings (like Teflon Spray, Pot Lid, and others) can reduce the damage done to you by an animated. Use your buffs!"

hintList << "Crowd Control rings (like Scaredy Cat, Gumshoe, and others) can be used to keep Animated at bay. If they can't attack you, you won't suffer any damage until they're free."

hintList << "When entering a new area, find the Null Crystal first so you don't have to make that long trek back."

hintList << "Don't put all your Charge Orbs into only one ring. The most efficient use of Orbs is to bring all your rings up at similar levels."

hintList << "Watch the enemy indicators on the side of the screen so that you don't accidentally walk into ambushes."

hintList << "It *is* possible to sneak around some monsters. Many of them can only see you if you are in their front 180-degree arc and some of them are quite short-sighted. You don't have to kill *everything*."

hintList << "You get even more power out of your rings if you combine them into RING SETS (see the Advanced Combat section in GAME HELP for more details on Ring Sets)."

hintList << "The world isn't all combat. Take a moment to chill and hang with other players. Head back to Barton to chat with folks. Taking a break once in a while can make you happier." 

hintList << "Enemies are shown in different colors, and the important ones are like a traffic light. Red = STOP! They're dangerous! Yellow = Use caution. Green = Go, go, go! That monster is a good challenge for you."

hintList << "If you're getting lots of lag, try closing other applications so that zOMG! gets more of your bandwidth pipeline."

hintList << "Use defense buffs at full rage for better protection. (The same is true of healing or stat alterations.)"

hintList << "Keep your crew panel open (even if you're soloing) so you can monitor the status of your buffs and reapply them as soon as they run out."

hintList << "Avoid using attacks that hit multiple targets when you'd rather fight only one. (Bite off only what you can chew.)"

hintList << "Ghi getting low? Take a quick breather in Barton before you head back out. (Or join a Crew!) "

hintList << "Try to find good combinations of rings to use. For instance, Slash, THEN Hack, THEN Shark Attack so that the knockback effects of Hack or Shark Attack don't move your target out of range before you can use Slash."

hintList << "Be aware that later zones are TOUGH if you're just starting out. For instance, if you are lower than level 6.0, you should seriously reconsider whether Gold Beach is right for you or not. "

hintList << "Remember, joining a crew will increase the drop rate for a lot of items. This includes Gaia Gold, Charge Orbs, and even Recipes! "

hintList << "Power up your Ghi Meter by visiting Barton Town once in a while. Powering those Ghi Buffs up can help you survive a lot longer."

hintList << "If you have multiple rings you want to use, then pay attention to their recharge timers and 'twist' your way through the rings as they become available. This maximizes the amount of healing or damaging you can do in a short period of time."

hintList << "Obviously there is strength in numbers, so join a Crew. Perhaps someone will have Defib so you don't have read another 'Dazed Tip'."

hintList << "Don't waste AoE attacks on one enemy when there's a group coming up behind him. Hit him, wait till they are all together, and *then* use your AoE."

hintList << "Put the rings you need to use most after Awakening in your first few ring slots so they become unlocked first. (Lots of people do this with Defib so they can help Awaken crew members quickly.)"

hintList << "Buff up before entering a boss encounter...don't wait until the middle of the fight."

hintList << "Getting knocked back by monsters too much? Try using the MY DENSITY ring to increase your weight!"

hintList << "Need more Stamina? Try Divinity so you can use your stamina when it really counts. (Healing Halo does the same thing for Health.)"

hintList << "Some monsters are hard to hit. Try using Keen Aye to improve your Accuracy so you can hit them more easily, or Adrenaline to lower your target's Dodge rating."

hintList << "Rings that have 'Armor' reduce incoming damage and help you stay alive longer."

hintList << "Rings that have Deflection abilities have a percentage chance to prevent all the incoming damage from hitting you."

hintList << "Rings that have Reflection abilities have a percentage chance to bounce all the incoming damage straight back at the attacker."

hintList << "If you are in a Crew, try standing closer to others who are fighting. That makes it easier for them to heal you."

hintList << "A crew with a large variety of rings can provide better effectiveness in combat."

hintList << "Use your Crew panel to select Crew members for healing and buffing during combat. It's easier and faster than trying to click their avatar."

hintList << "Be nice to your crew. They won't help you if they don't like you. (Sounds silly, but it's oh, so true.)"

hintList << "When in a Crew, think about each person taking areas of responsibility. For example, one person heals, one person crowd controls, a couple of people do damage, etc."

hintList << "You get a lot more loot as part of a Crew than you do as a solo player."

hintList << "When in a Crew, keep your crew list open to keep an eye on your crewmates and ask them to watch your back as well!"


//Check to see if the player is on the NULL CHAMBER quest
myManager.onEnter( myRooms.nullChamber1_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		if(!event.actor.hasQuestFlag(GLOBAL, "Z21_Trixie_HintsOn") && !event.actor.hasQuestFlag(GLOBAL, "Z21_Trixie_HintsOff")) { event.actor.setQuestFlag(GLOBAL, "Z21_Trixie_HintsOn") }
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		event.actor.unsetQuestFlag( GLOBAL, "Z21AttunementMessageOkay" ) //unset this flag so that, no matter what, it gets unset so the player can still see Attunement messages.

		myManager.schedule(2) {
			if( event.actor.isOnQuest( 74, 3 ) ) {
				event.actor.setQuestFlag( GLOBAL, "Z21UpgradeSpeechActivated" )
				Trixie.pushDialog( event.actor, "ringUpgrade" )
			}
			//This next section is in case the player talks to Trixie and then loses connection before actually upgrading his ring.
			//This reinstates the UI event call so that they can finish the quest thereafter.
			if( event.actor.isOnQuest( 74, 4 ) ) {
				runOnUiEvent( event.actor, "ringUpdate", null ) { 
					event.actor.centerPrint("Congratulations! You upgraded your ring!")
					event.actor.updateQuest( 74, "Trixie-VQS" )  //push the completion to step 4 of the "Charging a Ring" quest
					if( !event.actor.hasQuestFlag( GLOBAL, "Z21ElizabethNullReturnFlagAlreadyAdded" ) ) {
						event.actor.setQuestFlag( GLOBAL, "Z21ElizabethNullReturnFlagAlreadyAdded" ) 
						event.actor.addMiniMapQuestActorName( "BFG-Elizabeth" )
					}
				}
			}
			//Display a death tip if the user doesn't have the "coming from outside" flag and isn't in a special quest situation, as listed above
			if( event.actor.getPlayerVar( "Z21TrixieHints" ) == 0 && !event.actor.hasQuestFlag( GLOBAL, "Z21ComingFromOutside" ) && !event.actor.isOnQuest( 74, 3) && !event.actor.isOnQuest( 74, 4 ) && event.actor.hasQuestFlag(GLOBAL, "Z21_Trixie_HintsOn") ) {
				Trixie.say( random( hintList ) )
			}				
		}
		if( event.actor.hasQuestFlag( GLOBAL, "Z02PlayerDazed" ) && ( !event.actor.isOnQuest( 74, 3 ) || event.actor.isOnQuest( 74, 4 ) ) ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02PlayerDazed" )
			tutorialNPC.pushDialog( event.actor, "soYoureDazed" )
		}
		
		//Logic to remove valve flags if player dies in WW
		event.actor.removeMiniMapQuestLocation( "Z14ValveOneFlag" )
		event.actor.removeMiniMapQuestLocation( "Z14ValveTwoFlag" )
		event.actor.removeMiniMapQuestLocation( "Z14ValveThreeFlag" )
		event.actor.removeMiniMapQuestLocation( "Z14ValveFourFlag" )
	}
}

myManager.onExit( myRooms.nullChamber1_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z21ComingFromOutside" )
	}
}

//Dazed Tutorial that appears after dying in the Waterworks
soYoureDazed = tutorialNPC.createConversation( "soYoureDazed", true )

def dazed1 = [id:1]
dazed1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/youWereDazed2.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
dazed1.exec = { event ->
	event.actor.centerPrint( "When you're ready, click the crystal that says 'Village Greens' to return there." )
}
dazed1.result = DONE
soYoureDazed.addDialog( dazed1, tutorialNPC )



//---------------------------------------------------------
// TEACH RING UPGRADING                                    
//---------------------------------------------------------

ringUpgrade = Trixie.createConversation("ringUpgrade", true, "Z21UpgradeSpeechActivated", "!Z21UpgradeSpeechCompleted", "!QuestStarted_74:4" )

def upgrade1 = [id:1]
upgrade1.npctext = "Welcome to the Null Chamber! I'm Trixie. I'm guessing you're here to upgrade your ring."
upgrade1.result = 2
ringUpgrade.addDialog( upgrade1, Trixie )

def upgrade2 = [id:2]
upgrade2.npctext = "I assume that Elizabeth gave you some Charge Orbs, so let's get started."
upgrade2.result = 3
ringUpgrade.addDialog( upgrade2, Trixie )

def upgrade3 = [id:3]
upgrade3.npctext = "First, you need to open your Inventory. It's the white bag near your ring."
upgrade3.result = 4
ringUpgrade.addDialog( upgrade3, Trixie )

def upgrade4 = [id:4]
upgrade4.npctext = "Once you have it open, and touch a ring to select it, you'll be able to upgrade your ring."
upgrade4.result = 6
ringUpgrade.addDialog( upgrade4, Trixie )

def upgrade5 = [id:5]
upgrade5.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/upgradeTutorial.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
upgrade5.result = 6
ringUpgrade.addDialog( upgrade5, tutorialNPC )

def upgrade6 = [id:6]
upgrade6.npctext = "Why don't you try upgrading your ring to Level 2, now? Return to Elizabeth when you're done."
upgrade6.flag = ["!Z21UpgradeSpeechActivated", "Z21UpgradeSpeechCompleted"]
upgrade6.exec = { event ->
	event.player.updateQuest( 74, "Trixie-VQS" ) //update the quest now that the player has heard Trixie explain the upgrade process (complete step 3)
	upgradeTimer = myManager.schedule(10) {
		event.player.centerPrint( "Press the 'R' key to open your inventory, then select a ring, and hit the UPGRADE button." )
	}			
	runOnUiEvent( event.player, "ringUpdate", null ) {
		upgradeTimer.cancel()
		event.player.centerPrint("Congratulations! You upgraded your ring! Return to Elizabeth.")
		event.player.updateQuest( 74, "Trixie-VQS" )  //push the completion to step 4 of the "Charging a Ring" quest
		event.player.addMiniMapQuestActorName( "BFG-Elizabeth" )
		event.player.setQuestFlag( GLOBAL, "Z21ElizabethNullReturnFlagAlreadyAdded" )
	}
}
upgrade6.result = DONE
ringUpgrade.addDialog( upgrade6, Trixie )

//---------------------------------------------------------
// NULL CHAMBER GUARDIAN CONVO                             
//---------------------------------------------------------
def hintsOnConvo = Trixie.createConversation("hintsOnConvo", true, "!QuestStarted_74", "Z21_Trixie_HintsOn" )

def hintsOn1 = [id:1]
hintsOn1.npctext = "Hi there! I'm Trixie! I'm just here to say 'Be calm! Everything is going to be okay!'"
hintsOn1.options = []
hintsOn1.options << [text:"Really? That's the only reason you're here?", result:2]
hintsOn1.options << [text:"Could you stop giving me all these hints?", result:4]
hintsOnConvo.addDialog( hintsOn1, Trixie )

def hintsOn2 = [id:2]
hintsOn2.npctext = "Pretty much. That and I think Clara's still annoyed about that whole flaming bag of poo thing I did to her the other day at HQ."
hintsOn2.playertext = "Ah. I see."
hintsOn2.result = 3
hintsOnConvo.addDialog( hintsOn2, Trixie )

def hintsOn3 = [id:3]
hintsOn3.npctext = "So...be calm! Everything's going to be okay!"
hintsOn3.playertext = "Thanks, Trixie."
hintsOn3.result = DONE
hintsOnConvo.addDialog( hintsOn3, Trixie )

def hintsOn4 = [id:4]
hintsOn4.npctext = "Sure, I can do that."
hintsOn4.flag = ["Z21_Trixie_HintsOff", "!Z21_Trixie_HintsOn"]
hintsOn4.result = DONE
hintsOnConvo.addDialog( hintsOn4, Trixie )

//---------------------------------------------------------
// NULL CHAMBER GUARDIAN CONVO                             
//---------------------------------------------------------
def hintsOffConvo = Trixie.createConversation("hintsOffConvo", true, "!QuestStarted_74", "Z21_Trixie_HintsOff" )

def hintsOff1 = [id:1]
hintsOff1.npctext = "Hi there! I'm Trixie! I'm just here to say 'Be calm! Everything is going to be okay!'"
hintsOff1.options = []
hintsOff1.options << [text:"Really? That's the only reason you're here?", result:2]
hintsOff1.options << [text:"Can you start giving me hints again?", result:4]
hintsOffConvo.addDialog( hintsOff1, Trixie )

def hintsOff2 = [id:2]
hintsOff2.npctext = "Pretty much. That and I think Clara's still annoyed about that whole flaming bag of poo thing I did to her the other day at HQ."
hintsOff2.playertext = "Ah. I see."
hintsOff2.result = 3
hintsOffConvo.addDialog( hintsOff2, Trixie )

def hintsOff3 = [id:3]
hintsOff3.npctext = "So...be calm! Everything's going to be okay!"
hintsOff3.playertext = "Thanks, Trixie."
hintsOff3.result = DONE
hintsOffConvo.addDialog( hintsOff3, Trixie )

def hintsOff4 = [id:4]
hintsOff4.npctext = "You betcha!"
hintsOff4.flag = ["Z21_Trixie_HintsOn", "!Z21_Trixie_HintsOff"]
hintsOff4.result = DONE
hintsOffConvo.addDialog( hintsOff4, Trixie )

/* onZoneIn example
onZoneIn() {
	event ->
	println "ON ZONE IN ${event.player}"
	Trixie.say("Welcome to the zone ${event.player}.  You're in ${event.room.getName()}")
}
*/
