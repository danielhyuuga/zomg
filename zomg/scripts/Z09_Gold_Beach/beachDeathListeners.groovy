//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

anchorBugList = [] as Set
sandCastleList = [] as Set

//-----------------------------------------------------------------------------------
//Death listener for AnchorBugs                                                       
//-----------------------------------------------------------------------------------
def beachAnchorBugDeathScheduler() {
	myManager.schedule(1) { beachAnchorBugDeathScheduler() }
	myRooms.values().each() {
		it.getActorList().each() {
			if(isMonster(it) && it.getMonsterType() == "anchor_bug" && !anchorBugList.contains(it)) {
				//println "#### Scheduling runOnDeath event for ${it} ####"
				anchorBugList << it
				runOnDeath(it) { event -> checkForAnchorBugUpdate(event); anchorBugList.remove(event.actor) }
			}
		}
	}
}

def checkForAnchorBugUpdate(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(91, 2)) {
			if(it.getConLevel() < 7.1) {
				if(it.getPlayerVar("Z09_AnchorBugKillTotal") == null) { it.setPlayerVar("Z09_AnchorBugKillTotal", 0) }
				it.addToPlayerVar("Z09_AnchorBugKillTotal", 1)
				it.centerPrint("Anchor Bug ${it.getPlayerVar("Z09_AnchorBugKillTotal").toInteger()}/20")
				if(it.getPlayerVar("Z09_AnchorBugKillTotal") == 20) { it.updateQuest(91, "Brodie-VQS"); it.deletePlayerVar("Z09_AnchorBugKillTotal") }
			} else {
				it.centerPrint( "You must be level 7.0 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

beachAnchorBugDeathScheduler()

//-----------------------------------------------------------------------------------
//Death listener for SandCastles                                                     
//-----------------------------------------------------------------------------------
def beachSandCastleDeathScheduler() {
	myManager.schedule(1) { beachSandCastleDeathScheduler() }
	myRooms.values().each() {
		it.getActorList().each() {
			if(isMonster(it) && it.getMonsterType() == "sand_golem" && !sandCastleList.contains(it)) {
				//println "#### Scheduling runOnDeath event for ${it} ####"
				sandCastleList << it
				runOnDeath(it) { event -> checkForSandCastleUpdate(event); sandCastleList.remove(event.actor) }
			}
		}
	}
}

def checkForSandCastleUpdate(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(275, 2)) {
			if(it.getConLevel() < 7.1) {
				if(it.getPlayerVar("Z09_SandCastleKillTotal") == null) { it.setPlayerVar("Z09_SandCastleKillTotal", 0) }
				it.addToPlayerVar("Z09_SandCastleKillTotal", 1)
				it.centerPrint("Sand Castle Golem ${it.getPlayerVar("Z09_SandCastleKillTotal").toInteger()}/20")
				if(it.getPlayerVar("Z09_SandCastleKillTotal") == 20) { it.updateQuest(275, "Ceejay-VQS"); it.deletePlayerVar("Z09_SandCastleKillTotal") }
			} else {
				it.centerPrint( "You must be level 7.0 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

beachSandCastleDeathScheduler()