//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner_905_1 = myRooms.Beach_905.spawnSpawner( "spawner_905_1", "water_spout", 1)
spawner_905_1.setPos( 360, 130 )
spawner_905_1.setHome( "Beach_905", 360, 130 )
spawner_905_1.setSpawnWhenPlayersAreInRoom( true )
spawner_905_1.setWaitTime( 30 , 40 )
spawner_905_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_905_1.childrenWander( true )
spawner_905_1.setMonsterLevelForChildren( 6.1 )

// Spawner two
spawner_905_2 = myRooms.Beach_905.spawnSpawner( "spawner_905_2", "water_spout", 1)
spawner_905_2.setPos( 745, 430 )
spawner_905_2.setHome( "Beach_905", 745, 430 )
spawner_905_2.setSpawnWhenPlayersAreInRoom( true )
spawner_905_2.setWaitTime( 30 , 40 )
spawner_905_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_905_2.childrenWander( true )
spawner_905_2.setMonsterLevelForChildren( 6.1 )

// Spawner three
spawner_905_3 = myRooms.Beach_905.spawnSpawner( "spawner_905_3", "water_spout", 1)
spawner_905_3.setPos( 840, 170 )
spawner_905_3.setHome( "Beach_905", 840, 170 )
spawner_905_3.setSpawnWhenPlayersAreInRoom( true )
spawner_905_3.setWaitTime( 30 , 40 )
spawner_905_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_905_3.childrenWander( true )
spawner_905_3.setMonsterLevelForChildren( 6.1 )

spawner_905_1.stopSpawning()
spawner_905_2.stopSpawning()
spawner_905_3.stopSpawning()

//Alliances
spawner_905_1.allyWithSpawner( spawner_905_2 )
spawner_905_1.allyWithSpawner( spawner_905_3 )
spawner_905_2.allyWithSpawner( spawner_905_3 )


//Variable definition
spawnTime = false
spawnedSpout1 = false
spawnedSpout2 = false
spawnedSpout3 = false
spoutRespawning = false
spout_905_1 = null
spout_905_2 = null
spout_905_3 = null

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() }
	//println "***** GST = ${ gst() } ******"
}

def spawnChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) { //party goes from 7pm to 2am.
//	if( gst() > 0 ) { //This line for each setting of time parameters for testing.
		spawnTime = true
	} else {
		spawnTime = false
	}
}	

def checkForDespawn() {
	//println "#### Ran checkForDespawn() spawnedSpout1 = ${spawnedSpout1} spawnedSpout2 = ${spawnedSpout2} spawnedSpout3 = ${spawnedSpout3} spawnTime = ${spawnTime} ####"
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == false ) {
		clockChecker()
		if( spawnedSpout1 == true ) {
			if(spout_905_1.getHated().size() == 0) {
				spout_905_1.dispose()
				spawnedSpout1 = false
			}
		}
		if( spawnedSpout2 == true ) {
			if(spout_905_2.getHated().size() == 0) {
				spout_905_2.dispose()
				spawnedSpout2 = false
			}
		}
		if( spawnedSpout3 == true ) {
			if(spout_905_3.getHated().size() == 0) {
				spout_905_3.dispose()
				spawnedSpout3 = false
			}
		}
	}
	myManager.schedule(30) { checkForDespawn() }
}

def spawnOrNoSpawn() {
	//println "#### Ran spawnOrNoSpawn() spawnedSpout1 = ${spawnedSpout1} spawnedSpout2 = ${spawnedSpout2} spawnedSpout3 = ${spawnedSpout3} spawnTime = ${spawnTime} ####"
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == true ) {
		//println "@@@@ Trying to spawn beach_905 @@@@"
		spoutRespawning = false
		if( spawnedSpout1 == false ) {
			spout_905_1 = spawner_905_1.forceSpawnNow()
			//println "%%%% spout_905_1 should have been spawned as ${spout_905_1} %%%%"
			spawnedSpout1 = true
			runOnDeath( spout_905_1 ) { 
				spawnedSpout1 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout2 == false ) {
			spout_905_2 = spawner_905_2.forceSpawnNow()
			//println "%%%% spout_905_2 should have been spawned as ${spout_905_2} %%%%"
			spawnedSpout2 = true
			runOnDeath( spout_905_2 ) { 
				spawnedSpout2 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() }
					spoutRespawning = true 
				}
			}
		}
		if( spawnedSpout3 == false ) {
			spout_905_3 = spawner_905_3.forceSpawnNow()
			//println "%%%% spout_905_3 should have been spawned as ${spout_905_3} %%%%"
			spawnedSpout3 = true
			runOnDeath( spout_905_3 ) { 
				spawnedSpout3 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
	}
}

//Initialization
clockChecker()
checkForDespawn()