import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner1 = myRooms.Beach_103.spawnStoppedSpawner( "spawner_103_1", "anchor_bug", 1)
spawner1.setPos( 490, 310 )
spawner1.setInitialMoveForChildren( "Beach_103", 110, 350 )
spawner1.setHome( "Beach_103", 110, 350 )
spawner1.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner1.setMonsterLevelForChildren( 7.1 )

// Spawner two
spawner2 = myRooms.Beach_103.spawnStoppedSpawner( "spawner_103_2", "anchor_bug", 1)
spawner2.setPos( 490, 305 )
spawner2.setInitialMoveForChildren( "Beach_103", 100, 600 )
spawner2.setHome( "Beach_103", 100, 600 )
spawner2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner2.setMonsterLevelForChildren( 7.1 )

// Spawner three
spawner3 = myRooms.Beach_103.spawnStoppedSpawner( "spawner_103_3", "anchor_bug", 1)
spawner3.setPos( 490, 300 )
spawner3.setInitialMoveForChildren( "Beach_103", 110, 325 )
spawner3.setHome( "Beach_103", 110, 325 )
spawner3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner3.setMonsterLevelForChildren( 7.1 )

// Spawner four
spawner4 = myRooms.Beach_103.spawnStoppedSpawner( "spawner_103_4", "anchor_bug", 1)
spawner4.setPos( 490, 295 )
spawner4.setInitialMoveForChildren( "Beach_103", 100, 580 )
spawner4.setHome( "Beach_103", 100, 580 )
spawner4.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner4.setMonsterLevelForChildren( 7.1 )

//===================================
//CONTAINER LOGIC                    
//===================================
chest103 = makeSwitch( "chest103", myRooms.Beach_103, 260, 610 )
chest103.lock()
chest103.off()
chest103.setUsable( false )

playerSet103 = [] as Set
hateCollector = [] as Set
zone = 9 //Gold Beach

def openChest103() {
	//look at every player in the room and grant them loot after the chest opens
	myRooms.Beach_103.getActorList().each { if( isPlayer( it ) ) { playerSet103 << it } }
	playerSet103.each{
		if( hateCollector.contains( it ) ) {
			chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe

			//scale the returned results down by relative con levels if the player is below the normal CL for the area
			if( it.getConLevel() > CLMap[zone] ) {
				lootMultiplier = 1.0
			} else {
				lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
			}

			//grant gold
			goldGrant = goldMap[zone] 
			goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
			it.grantCoins( ( goldGrant * lootMultiplier * chestType ).intValue() ) 

			//grant common items
			chance = (commonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				commonGrant = random( commonMap[zone] )
				it.grantItem( commonGrant )
			}

			//grant uncommon items
			chance = (uncommonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				uncommonGrant = random( uncommonMap[zone] )
				it.grantItem( uncommonGrant )
			}

			//grant recipes
			chance = (recipeChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				recipeGrant = random( recipeMap[zone] )
				it.grantItem( recipeGrant )
			}

			//grant orbs
			chance = (orbChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				orbGrant = orbMap[zone]
				orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType ).intValue()
				it.grantQuantityItem( 100257, orbGrant )
			}

			//grant rings
			chance = (ringChance * chestType).intValue()
			roll = random( 1, 100 )
			if( roll <= chance ) {
				ringGrant = random( ringMap[zone] )
				it.grantRing( ringGrant, true )
			}
		} else {
			if( it.getConLevel() > CLMap[zone]+1 ) {
				it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
			} else {
				it.centerPrint( "You did not help defeat the creatures protecting this container. No risk = no reward." )
			}
		}
	}
}

//Respawn Logic
def checkForContainerUnlock() {
	if( spawner1.spawnsInUse() + spawner2.spawnsInUse() + spawner3.spawnsInUse() + spawner4.spawnsInUse() == 0 ) {
		myManager.schedule(3) {
			chest103.on()
			openChest103()
			//respawn the camp after a suitable delay
			myManager.schedule( random( 300, 600) ) {
				//close and reset the chest
				chest103.off()
				hateCollector.clear()
				playerSet103.clear()
				spawn1 = spawner1.forceSpawnNow()
				runOnDeath( spawn1, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
				spawn2 = spawner2.forceSpawnNow()
				runOnDeath( spawn2, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
				spawn3 = spawner3.forceSpawnNow()
				runOnDeath( spawn3, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
				spawn4 = spawner4.forceSpawnNow()
				runOnDeath( spawn4, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )

				//start watching to unlock the container again
				checkForContainerUnlock()
			}
		}
	} else {
		myManager.schedule(2) { checkForContainerUnlock() }
	}
}

//Alliances
spawner1.allyWithSpawner( spawner2 )
spawner1.allyWithSpawner( spawner3 )
spawner1.allyWithSpawner( spawner4 )
spawner2.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner4 )
spawner3.allyWithSpawner( spawner4 )

//STARTUP LOGIC
spawn1 = spawner1.forceSpawnNow()
runOnDeath( spawn1, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
spawn2 = spawner2.forceSpawnNow()
runOnDeath( spawn2, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
spawn3 = spawner3.forceSpawnNow()
runOnDeath( spawn3, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
spawn4 = spawner4.forceSpawnNow()
runOnDeath( spawn4, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )

checkForContainerUnlock()

//====================================================================================
//====================================================================================
commonChance = 20 
uncommonChance = 10 
recipeChance = 1
orbChance = 5
ringChance = 1 

CLMap = [ 2:1, 3:2, 4:2.5, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

goldMap = [ 2:10, 3:20, 4:30, 5:40, 6:50, 7:60, 8:70, 9:80, 10:90, 11:100, 14:10, 16:90, 18:80 ]

commonMap = [ 2:["100272", "100289", "100385", "100297", "100397"], 3:["100291", "100262", "100263", "100298"], 4:["100388", "100278", "100275", "100283"], 5:["100281", "100367", "100411", "100394", "100284", "100405", "100267"], 6:["100265", "100285", "100398", "100397", "100376", "100296"], 7:["100373", "100260"], 8:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 9:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 11:["100371", "100294", "100391", "100277", "100290", "100368", "100386", "100403"], 14:[0], 16:["100287", "100409"], 18:["100393", "100293", "100259", "100292"] ]

uncommonMap = [ 2:["100280", "100279", "100270", "100380", "100384"], 3:["100378", "100261", "100271"], 4:["100276", "100258", "100268"], 5:["100381", "100382", "100282", "100383"], 6:["100390", "100299", "100286", "100369", "100400", "100387"], 7:["100365", "100410"], 8:["100389", "100264", "100406", "100399", "100402"], 9:["100389", "100264", "100406", "100399", "100402"], 10:["100395", "100413", "100407", "100266"], 11:["100396", "100269", "100401", "100372", "100375", "100366", "100379", "100274"], 14:[0], 16:["100404", "100288"], 18:["100395", "100413", "100407", "100266"] ]

recipeMap = [ 2:["17766", "17764", "17772", "17758", "17756"], 3:["17861", "17857", "17755"], 4:["17848", "17849", "17852", "17851", "17850", "17753"], 5:["17833", "17831", "17754", "17836", "17835"], 6:["17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779"], 7:["17785", "17793", "17788", "17787"], 8:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 9:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 10:["17796", "17846", "17803", "17844"], 11:["17819", "17816", "17811", "17808", "17806", "17812", "17813", "17814", "17810", "17809", "17815"], 14:[0], 16:["17786", "17791", "17790", "17794"], 18:["17796", "17846", "17803", "17844"] ]

orbMap = [ 2:1, 3:2, 4:3, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

ringMap = [ 2:["17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 3:["17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 4:["17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 5:["17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 6:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 7:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 8:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 9:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 10:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 11:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 14:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 16:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 18:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"] ]

//====================================================================================
//====================================================================================

