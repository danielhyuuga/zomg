//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

Brodie = spawnNPC( "Brodie-VQS", myRooms.Beach_906, 785, 600 )
Brodie.setDisplayName( "Brodie" )

onQuestStep( 95, 2 ) { event -> event.player.addMiniMapQuestActorName( "Brodie-VQS" ) }
onQuestStep( 91, 2 ) { event -> event.player.addMiniMapQuestActorName( "Brodie-VQS" ) }

myManager.onEnter(myRooms.Beach_906) { event ->
	if(isPlayer(event.actor) && event.actor.isDoneQuest(95) && !event.actor.hasQuestFlag(GLOBAL, "Z9_Brodie_Quest95_Done")) {
		event.actor.setQuestFlag(GLOBAL, "Z9_Brodie_Quest95_Done")
	}
}

//------------------------------------------------------------
//Brodie Default                                              
//------------------------------------------------------------
def brodieDefault = Brodie.createConversation("brodieDefault", true, "!QuestStarted_95", "!Z9_Brodie_Drag_Break", "!QuestCompleted_95")

def default1 = [id:1]
default1.npctext = "Sup! What's goin' down?"
default1.options = []
default1.options << [text:"Just working on my tan. You?", result: 2]
default1.options << [text:"Bah! I was gonna ask you that. I'm just looking for something to do.", result: 3]
default1.options << [text:"PARTY!!!!!!", result: 4]
brodieDefault.addDialog(default1, Brodie)

def default2 = [id:2]
default2.npctext = "Tan?! Isn't it faster to customize? Anyway, I'm pretty terrible atm. Nobody but me even cares about the dang anchor bugs walking around!"
default2.options = []
default2.options << [text:"Did you say anchors... walking... around? The Animated get stranger and stranger.", result: 5]
default2.options << [text:"WTF! I care! That just sounds bad.", result: 6]
default2.options << [text:"Why should anyone care? They're just anchors.", result: 7]
brodieDefault.addDialog(default2, Brodie)

def default3 = [id:3]
default3.npctext = "Hey, maybe you can help me with the anchor bugs!"
default3.options = []
default3.options << [text:"Anchor bugs? What is an anchor bug?!", result: 5]
default3.options << [text:"No idea what an anchor bug is but they sound nasty!", result: 6]
default3.options << [text:"Bugs? Why would I bet worried about bugs?", result: 7]
brodieDefault.addDialog(default3, Brodie)

def default4 = [id:4]
default4.npctext = "How can anyone party with the anchor bugs out there?"
default4.options = []
default4.options << [text:"Er, the what?", result: 5]
default4.options << [text:"Man, don't be a pill! It's PARTY TIME!", result: 8]
brodieDefault.addDialog(default4, Brodie)

def default5 = [id:5]
default5.npctext = "Yeah, giant anchors that crawl around and shoot themselves at people. Anchor bugs are serious business."
default5.options = []
default5.options << [text:"Sounds like it... what can we do?", result: 9]
default5.options << [text:"I'll be sure to avoid them. Thanks.", result: 10]
brodieDefault.addDialog(default5, Brodie)

def default6 = [id:6]
default6.npctext = "They're ridiculous! A huge anchor that walks around shooting itself at Gaians! We have to do something."
default6.options = []
default6.options << [text:"No kidding! What did you have in mind?", result: 9]
default6.options << [text:"I know what I'm gonna do. Avoid them like the plague.", result: 10]
brodieDefault.addDialog(default6, Brodie)

def default7 = [id:7]
default7.npctext = "Just anchors? These things are monstrous and they're scurrying all over the beach attacking Gaians!"
default7.options = []
default7.options << [text:"Oh, crap, we have to stop them! What can we do?", result: 9]
default7.options << [text:"So we just stay away from them. They can't be that hard to avoid.", result: 10]
brodieDefault.addDialog(default7, Brodie)

def default8 = [id:8]
default8.npctext = "Uhg. Moron, your bus is leaving."
default8.flag = "Z9_Brodie_Drag_Break"
default8.result = DONE
brodieDefault.addDialog(default8, Brodie)

def default9 = [id:9]
default9.npctext = "I dunno... maybe you could just kill them? You look pretty badass to me."
default9.options = []
default9.options << [text:"Hmm... maybe I could. I'll give it a shot.", result: 11]
default9.options << [text:"You got that right. I kick butt! I'm gonna wreck those bugs.", result: 11]
default9.options << [text:"No. I don't wanna.", result: 12]
brodieDefault.addDialog(default9, Brodie)

def default10 = [id:10]
default10.npctext = "Bah, that's not a viable plan."
default10.options = []
default10.options << [text:"Well, what did you have in mind then?", result: 9]
default10.options << [text:"Sure it is. Just watch!", result: 12]
brodieDefault.addDialog(default10, Brodie)

def default11 = [id:11]
default11.npctext = "Go getem, %p! You can find them to the north... and the further north the more you will find."
default11.quest = 95
default11.result = DONE
brodieDefault.addDialog(default11, Brodie)

def default12 = [id:12]
default12.npctext = "Will nobody help me with these bugs?!"
default12.flag = "Z9_Brodie_Drag_Break"
default12.result = DONE
brodieDefault.addDialog(default12, Brodie)

//------------------------------------------------------------
//Drag Break                                                  
//------------------------------------------------------------
def dragBreak = Brodie.createConversation("dragBreak", true, "Z9_Brodie_Drag_Break")

def dragBreak1 = [id:1]
dragBreak1.npctext = "Sup! Avoiding the bugs isn't as easy as you thought, huh?"
dragBreak1.options = []
dragBreak1.options << [text:"No, it's definitely not. What should I do?", result: 2]
dragBreak1.options << [text:"Nah man, I just came back to tell you it's no problem! Cya!", result: 3]
dragBreak.addDialog(dragBreak1, Brodie)

def dragBreak2 = [id:2]
dragBreak2.npctext = "I dunno, man, but you look pretty tough to me. Just beat 'em down!"
dragBreak2.options = []
dragBreak2.options << [text:"Yeah! I'll teach those dang bugs to mess with me!", result: 4]
dragBreak2.options << [text:"No way!", result: 3]
dragBreak.addDialog(dragBreak2, Brodie)

def dragBreak3 = [id:3]
dragBreak3.npctext = "Will no one confront these menacing bugs?!"
dragBreak3.result = DONE
dragBreak.addDialog(dragBreak3, Brodie)

def dragBreak4 = [id:4]
dragBreak4.npctext = "Yeah! Go getem! You can find them to the north... and the further north the more you will find."
dragBreak4.quest = 95
dragBreak4.flag = "!Z9_Brodie_Drag_Break"
dragBreak4.result = DONE
dragBreak.addDialog(dragBreak4, Brodie)

//------------------------------------------------------------
//Drag Active                                                 
//------------------------------------------------------------
def dragActive = Brodie.createConversation("dragActive", true, "QuestStarted_95:2")

def dragActive1 = [id:1]
dragActive1.npctext = "Did you do anything about the Anchor Bugs yet? No? Please hurry, you have to help!"
dragActive1.result = DONE
dragActive.addDialog(dragActive1, Brodie)

//------------------------------------------------------------
//Drag Complete                                               
//------------------------------------------------------------
def dragComplete = Brodie.createConversation("dragComplete", true, "QuestStarted_95:3")

def dragComplete1 = [id:1]
dragComplete1.npctext = "Well, that oughta keep the bugs under control for now. Good work, %p!"
dragComplete1.options = []
dragComplete1.options << [text:"Yeah, they learned their place! I need to beat a few more things up.", result: 2]
dragComplete1.options << [text:"Haha, that was fun! Anything else need killing?", result: 2]
dragComplete1.options << [text:"Thanks. It was really tiring though... I'm gonna take a break.", result: 3]
dragComplete.addDialog(dragComplete1, Brodie)

def dragComplete2 = [id:2]
dragComplete2.npctext = "Hmm, I may have something for you..."
dragComplete2.quest = 95
dragComplete2.flag = "Z9_Brodie_Quest95_Done"
dragComplete2.exec = { event -> event.player.removeMiniMapQuestActorName("Brodie-VQS"); Brodie.pushDialog(event.player, "dragRepeatGive") }
dragComplete2.result = DONE
dragComplete.addDialog(dragComplete2, Brodie)

def dragComplete3 = [id:3]
dragComplete3.npctext = "Alright, take care."
dragComplete3.quest = 95
dragComplete3.exec = { event -> event.player.removeMiniMapQuestActorName("Brodie-VQS") }
dragComplete3.result = DONE
dragComplete.addDialog(dragComplete3, Brodie)

//------------------------------------------------------------
//Drag Repeat Give                                            
//------------------------------------------------------------
def dragRepeatGive = Brodie.createConversation("dragRepeatGive", true, "Z9_Brodie_Quest95_Done", "!QuestStarted_91", "!Z9_Brodie_DragRepeat_Allowed", "!Z9_Brodie_DragRepeat_Denied", "!Z9_Brodie_DragRepeat_AllowedAgain")
dragRepeatGive.setUrgent(true)

def dragRepeatGive1 = [id:1]
dragRepeatGive1.npctext = "Dang bugs! They just won't leave! It's like they're dug in or something."
dragRepeatGive1.playertext = "I suppose that *is* what anchors do."
dragRepeatGive1.result = 2
dragRepeatGive.addDialog(dragRepeatGive1, Brodie)

def dragRepeatGive2 = [id:2]
dragRepeatGive2.npctext = "I hadn't thought of that, but you make a really good point. Meet stubborn with stubborn, I say. If they won't budge, we'll keep pushing until they do. Ready to go konk some more bugs?"
dragRepeatGive2.options = []
dragRepeatGive2.options << [text:"You betcha, I'm not giving up yet!", exec: {event ->
	if(event.player.getConLevel() < 7.1) {
		event.player.setQuestFlag(GLOBAL, "Z9_Brodie_DragRepeat_Allowed")
		Brodie.pushDialog(event.player, "dragRepeatAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z9_Brodie_DragRepeat_Denied")
		Brodie.pushDialog(event.player, "dragRepeatDenied")
	}
}, result: DONE]
dragRepeatGive2.options << [text:"I'm exhausted, Brodie. Let me take a break and maybe I'll try later.", result: 4]
dragRepeatGive.addDialog(dragRepeatGive2, Brodie)

def dragRepeatGive4 = [id:4]
dragRepeatGive4.npctext = "Alright, catch your breath and come back."
dragRepeatGive4.result = DONE
dragRepeatGive.addDialog(dragRepeatGive4, Brodie)

//------------------------------------------------------------
//Drag Repeat Active                                          
//------------------------------------------------------------
def dragRepeatActive = Brodie.createConversation("dragRepeatActive", true, "QuestStarted_91:2")

def dragRepeatActive1 = [id:1]
dragRepeatActive1.npctext = "I still see a lot of bugs out there... what's going on?"
dragRepeatActive1.playertext = "Still working on it, Brodie."
dragRepeatActive1.result = DONE
dragRepeatActive.addDialog(dragRepeatActive1, Brodie)

//------------------------------------------------------------
//Drag Repeat Complete                                        
//------------------------------------------------------------
def dragRepeatComplete = Brodie.createConversation("dragRepeatComplete", true, "QuestStarted_91:3")

def dragRepeatComplete1 = [id:1]
dragRepeatComplete1.npctext = "Jeez those bugs are stubborn! I think you're starting to get through, though. Great work, %p!"
dragRepeatComplete1.playertext = "Are you sure? It feels like I'm not making any headway."
dragRepeatComplete1.result = 2
dragRepeatComplete.addDialog(dragRepeatComplete1, Brodie)

def dragRepeatComplete2 = [id:2]
dragRepeatComplete2.npctext = "Pretty sure. There's just a ton of those things so it seems like you aren't. You just need to konk some more. Up for it?"
dragRepeatComplete2.options = []
dragRepeatComplete2.options << [text:"Yeah, I guess. I'm a little tired but I'm not gonna let some bugs beat me.", exec: { event ->
	event.player.removeMiniMapQuestActorName("Brodie-VQS")
	event.player.updateQuest(91, "Brodie-VQS")
	if(event.player.getConLevel() < 7.1) {
		event.player.setQuestFlag(GLOBAL, "Z9_Brodie_DragRepeat_AllowedAgain")
		Brodie.pushDialog(event.player, "dragRepeatAllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z9_Brodie_DragRepeat_Denied")
		Brodie.pushDialog(event.player, "dragRepeatDenied")
	}
}, result: DONE]
dragRepeatComplete2.options << [text:"Uhg, I need a break.", result: 4]
dragRepeatComplete.addDialog(dragRepeatComplete2, Brodie)

def dragRepeatComplete4 = [id:4]
dragRepeatComplete4.npctext = "Don't rest for too long!"
dragRepeatComplete4.quest = 91
dragRepeatComplete4.exec = { event -> event.player.removeMiniMapQuestActorName("Brodie-VQS") }
dragRepeatComplete4.result = DONE
dragRepeatComplete.addDialog(dragRepeatComplete4, Brodie)

//------------------------------------------------------------
//Drag Repeat Allowed Again                                   
//------------------------------------------------------------
def dragRepeatAllowedAgain = Brodie.createConversation("dragRepeatAllowedAgain", true, "Z9_Brodie_DragRepeat_AllowedAgain")

def dragRepeatAllowedAgain1 = [id:1]
dragRepeatAllowedAgain1.npctext = "I knew you'd be down. This time, let's only worry about the bugs on the Beach. Once we've gotten rid of them we can worry about other areas!"
dragRepeatAllowedAgain1.quest = 91
dragRepeatAllowedAgain1.flag = "!Z9_Brodie_DragRepeat_AllowedAgain"
dragRepeatAllowedAgain1.result = DONE
dragRepeatAllowedAgain.addDialog(dragRepeatAllowedAgain1, Brodie)

//------------------------------------------------------------
//Drag Repeat Allowed                                            
//------------------------------------------------------------
def dragRepeatAllowed = Brodie.createConversation("dragRepeatAllowed", true, "Z9_Brodie_DragRepeat_Allowed")

def dragRepeatAllowed1 = [id:1]
dragRepeatAllowed1.npctext = "Too cool, %p. Let's try focusing just on wiping out the bugs on the Beach this time. Konk 'em out and let me know when you've dropped twenty of them!"
dragRepeatAllowed1.quest = 91
dragRepeatAllowed1.flag = "!Z9_Brodie_DragRepeat_Allowed"
dragRepeatAllowed1.result = DONE
dragRepeatAllowed.addDialog(dragRepeatAllowed1, Brodie)

//------------------------------------------------------------
//Drag Repeat Denied                                          
//------------------------------------------------------------
def dragRepeatDenied = Brodie.createConversation("dragRepeatDenied", true, "Z9_Brodie_DragRepeat_Denied")

def dragRepeatDenied1 = [id:1]
dragRepeatDenied1.npctext = "Dude, I don't wanna waste your time. You're too tough for this stuff."
dragRepeatDenied1.flag = "!Z9_Brodie_DragRepeat_Denied"
dragRepeatDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 7.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
dragRepeatDenied1.result = DONE
dragRepeatDenied.addDialog(dragRepeatDenied1, Brodie)