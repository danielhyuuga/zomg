//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

FIBER_PERCENTAGE = 60

//--------------------------------------------------------------------------------------
// Beach_1001 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_1001_1 = myRooms.Beach_1001.spawnSpawner( "spawner_1001_1", "sand_fluff", 1)
spawner_1001_1.stopSpawning()
spawner_1001_1.setPos( 925, 480 )
spawner_1001_1.setHome( "Beach_1001", 925, 480 )
spawner_1001_1.setSpawnWhenPlayersAreInRoom( true )
spawner_1001_1.setWaitTime( 80 , 100 )
spawner_1001_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1001_1.setMonsterLevelForChildren( 6.0 )

spawner_1001_2 = myRooms.Beach_1001.spawnSpawner( "spawner_1001_2", "sand_fluff", 1)
spawner_1001_2.stopSpawning()
spawner_1001_2.setPos( 505, 115 )
spawner_1001_2.setHome( "Beach_1001", 505, 115 )
spawner_1001_2.setSpawnWhenPlayersAreInRoom( true )
spawner_1001_2.setWaitTime( 80 , 100 )
spawner_1001_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1001_2.setMonsterLevelForChildren( 6.0 )

spawner_1001_3 = myRooms.Beach_1001.spawnSpawner( "spawner_1001_3", "sand_fluff", 1)
spawner_1001_3.stopSpawning()
spawner_1001_3.setPos( 635, 325 )
spawner_1001_3.setHome( "Beach_1001", 635, 325 )
spawner_1001_3.setSpawnWhenPlayersAreInRoom( true )
spawner_1001_3.setWaitTime( 80 , 100 )
spawner_1001_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1001_3.setMonsterLevelForChildren( 6.0 )

spawner_1001_1.allyWithSpawner( spawner_1001_2 )
spawner_1001_1.allyWithSpawner( spawner_1001_3 )
spawner_1001_2.allyWithSpawner( spawner_1001_3 )

//--------------------------------------------------------------------------------------
// Beach_1002 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_1002_1 = myRooms.Beach_1002.spawnSpawner( "spawner_1002_1", "sand_fluff", 1)
spawner_1002_1.stopSpawning()
spawner_1002_1.setPos( 345, 520 )
spawner_1002_1.setInitialMoveForChildren( "Beach_1002", 545, 90 )
spawner_1002_1.setHome( "Beach_1002", 545, 90 )
spawner_1002_1.setSpawnWhenPlayersAreInRoom( true )
spawner_1002_1.setWaitTime( 80, 100 )
spawner_1002_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1002_1.setMonsterLevelForChildren( 6.1 )

spawner_1002_2 = myRooms.Beach_1002.spawnSpawner( "spawner_1002_2", "sand_fluff", 1)
spawner_1002_2.stopSpawning()
spawner_1002_2.setPos( 345, 515 )
spawner_1002_2.setInitialMoveForChildren( "Beach_1002", 850, 100 )
spawner_1002_2.setHome( "Beach_1002", 850, 100 )
spawner_1002_2.setSpawnWhenPlayersAreInRoom( true )
spawner_1002_2.setWaitTime( 80, 100 )
spawner_1002_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1002_2.setMonsterLevelForChildren( 6.1 )

spawner_1002_3 = myRooms.Beach_1002.spawnSpawner( "spawner_1002_3", "sand_fluff", 1)
spawner_1002_3.stopSpawning()
spawner_1002_3.setPos( 345, 510 )
spawner_1002_3.setInitialMoveForChildren( "Beach_1002", 110, 415 )
spawner_1002_3.setHome( "Beach_1002", 110, 415 )
spawner_1002_3.setSpawnWhenPlayersAreInRoom( true )
spawner_1002_3.setWaitTime( 80, 100 )
spawner_1002_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1002_3.setMonsterLevelForChildren( 6.1 )

spawner_1002_1.allyWithSpawner( spawner_1002_2 )
spawner_1002_1.allyWithSpawner( spawner_1002_3 )
spawner_1002_2.allyWithSpawner( spawner_1002_3 )

//--------------------------------------------------------------------------------------
// Beach_1003 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_1003_1 = myRooms.Beach_1003.spawnSpawner( "spawner_1003_1", "sand_fluff", 1)
spawner_1003_1.stopSpawning()
spawner_1003_1.setPos( 630, 400 )
spawner_1003_1.setInitialMoveForChildren( "Beach_1003", 860, 140 )
spawner_1003_1.setHome( "Beach_1003", 860, 140 )
spawner_1003_1.setSpawnWhenPlayersAreInRoom( true )
spawner_1003_1.setWaitTime( 80, 100 )
spawner_1003_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1003_1.setMonsterLevelForChildren( 6.2 )

spawner_1003_2 = myRooms.Beach_1003.spawnSpawner( "spawner_1003_2", "sand_fluff", 1)
spawner_1003_2.stopSpawning()
spawner_1003_2.setPos( 630, 395 )
spawner_1003_2.setInitialMoveForChildren( "Beach_1003", 500, 85 )
spawner_1003_2.setHome( "Beach_1003", 500, 85 )
spawner_1003_2.setSpawnWhenPlayersAreInRoom( true )
spawner_1003_2.setWaitTime( 80, 100 )
spawner_1003_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1003_2.setMonsterLevelForChildren( 6.2 )

spawner_1003_3 = myRooms.Beach_1003.spawnSpawner( "spawner_1003_3", "sand_fluff", 1)
spawner_1003_3.stopSpawning()
spawner_1003_3.setPos( 630, 390 )
spawner_1003_3.setInitialMoveForChildren( "Beach_1003", 200, 130 )
spawner_1003_3.setHome( "Beach_1003", 200, 130 )
spawner_1003_3.setSpawnWhenPlayersAreInRoom( true )
spawner_1003_3.setWaitTime( 80, 100 )
spawner_1003_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1003_3.setMonsterLevelForChildren( 6.2 )

spawner_1003_1.allyWithSpawner( spawner_1003_2 )
spawner_1003_1.allyWithSpawner( spawner_1003_3 )
spawner_1003_2.allyWithSpawner( spawner_1003_3 )

//--------------------------------------------------------------------------------------
// Beach_1004 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_1004_1 = myRooms.Beach_1004.spawnSpawner( "spawner_1004_1", "sand_fluff", 1)
spawner_1004_1.stopSpawning()
spawner_1004_1.setPos( 175, 470 )
spawner_1004_1.setInitialMoveForChildren( "Beach_1004", 105, 405 )
spawner_1004_1.setHome( "Beach_1004", 105, 405 )
spawner_1004_1.setSpawnWhenPlayersAreInRoom( true )
spawner_1004_1.setWaitTime( 80, 100 )
spawner_1004_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1004_1.setMonsterLevelForChildren( 6.1 )

spawner_1004_2 = myRooms.Beach_1004.spawnSpawner( "spawner_1004_2", "sand_fluff", 1)
spawner_1004_2.stopSpawning()
spawner_1004_2.setPos( 175, 465 )
spawner_1004_2.setInitialMoveForChildren( "Beach_1004", 415, 375 )
spawner_1004_2.setHome( "Beach_1004", 415, 375 )
spawner_1004_2.setSpawnWhenPlayersAreInRoom( true )
spawner_1004_2.setWaitTime( 80, 100 )
spawner_1004_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1004_2.setMonsterLevelForChildren( 6.1 )

spawner_1004_3 = myRooms.Beach_1004.spawnSpawner( "spawner_1004_3", "sand_fluff", 1)
spawner_1004_3.stopSpawning()
spawner_1004_3.setPos( 175, 460 )
spawner_1004_3.setInitialMoveForChildren( "Beach_1004", 170, 170 )+
spawner_1004_3.setHome( "Beach_1004", 170, 170 )
spawner_1004_3.setSpawnWhenPlayersAreInRoom( true )
spawner_1004_3.setWaitTime( 80, 100 )
spawner_1004_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1004_3.setMonsterLevelForChildren( 6.1 )

spawner_1004_4 = myRooms.Beach_1004.spawnSpawner( "spawner_1004_4", "sand_fluff", 1)
spawner_1004_4.stopSpawning()
spawner_1004_4.setPos( 175, 455 )
spawner_1004_4.setInitialMoveForChildren( "Beach_1004", 455, 115 )
spawner_1004_4.setHome( "Beach_1004", 455, 115 )
spawner_1004_4.setSpawnWhenPlayersAreInRoom( true )
spawner_1004_4.setWaitTime( 80, 100 )
spawner_1004_4.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_1004_4.setMonsterLevelForChildren( 6.1 )

spawner_1004_1.allyWithSpawner( spawner_1004_2 )
spawner_1004_1.allyWithSpawner( spawner_1004_3 )
spawner_1004_1.allyWithSpawner( spawner_1004_4 )
spawner_1004_2.allyWithSpawner( spawner_1004_3 )
spawner_1004_2.allyWithSpawner( spawner_1004_4 )
spawner_1004_3.allyWithSpawner( spawner_1004_4 )

//--------------------------------------------------------------------------------------
// Beach_802 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_802_1 = myRooms.Beach_802.spawnSpawner( "spawner_802_1", "sand_fluff", 1)
spawner_802_1.stopSpawning()
spawner_802_1.setPos( 250, 590 )
spawner_802_1.setInitialMoveForChildren( "Beach_802", 380, 505 )
spawner_802_1.setHome( "Beach_802", 380, 505 )
spawner_802_1.setSpawnWhenPlayersAreInRoom( true )
spawner_802_1.setWaitTime( 80, 100 )
spawner_802_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_802_1.setMonsterLevelForChildren( 6.3 )

spawner_802_2 = myRooms.Beach_802.spawnSpawner( "spawner_802_2", "sand_fluff", 1)
spawner_802_2.stopSpawning()
spawner_802_2.setPos( 250, 585 )
spawner_802_2.setInitialMoveForChildren( "Beach_802", 790, 525 )
spawner_802_2.setHome( "Beach_802", 790, 525 )
spawner_802_2.setSpawnWhenPlayersAreInRoom( true )
spawner_802_2.setWaitTime( 80, 100 )
spawner_802_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_802_2.setMonsterLevelForChildren( 6.3 )

spawner_802_3 = myRooms.Beach_802.spawnSpawner( "spawner_802_3", "sand_fluff", 1)
spawner_802_3.stopSpawning()
spawner_802_3.setPos( 250, 580 )
spawner_802_3.setInitialMoveForChildren( "Beach_802", 640, 315 )
spawner_802_3.setHome( "Beach_802", 640, 315 )
spawner_802_3.setSpawnWhenPlayersAreInRoom( true )
spawner_802_3.setWaitTime( 80, 100 )
spawner_802_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_802_3.setMonsterLevelForChildren( 6.3 )

spawner_802_4 = myRooms.Beach_802.spawnSpawner( "spawner_802_4", "sand_fluff", 1)
spawner_802_4.stopSpawning()
spawner_802_4.setPos( 250, 575 )
spawner_802_4.setInitialMoveForChildren( "Beach_802", 870, 215 )
spawner_802_4.setHome( "Beach_802", 870, 215 )
spawner_802_4.setSpawnWhenPlayersAreInRoom( true )
spawner_802_4.setWaitTime( 80, 100 )
spawner_802_4.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_802_4.setMonsterLevelForChildren( 6.3 )

spawner_802_1.allyWithSpawner( spawner_802_2 )
spawner_802_1.allyWithSpawner( spawner_802_3 )
spawner_802_1.allyWithSpawner( spawner_802_4 )
spawner_802_2.allyWithSpawner( spawner_802_3 )
spawner_802_2.allyWithSpawner( spawner_802_4 )
spawner_802_3.allyWithSpawner( spawner_802_4 )

//--------------------------------------------------------------------------------------
// Beach_803 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_803_1 = myRooms.Beach_803.spawnSpawner( "spawner_803_1", "sand_fluff", 1)
spawner_803_1.stopSpawning()
spawner_803_1.setPos( 485, 205 )
spawner_803_1.setInitialMoveForChildren( "Beach_803", 340, 240 )
spawner_803_1.setHome( "Beach_803", 340, 240 )
spawner_803_1.setSpawnWhenPlayersAreInRoom( true )
spawner_803_1.setWaitTime( 80, 100 )
spawner_803_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_803_1.setMonsterLevelForChildren( 6.4 )

spawner_803_2 = myRooms.Beach_803.spawnSpawner( "spawner_803_2", "sand_fluff", 1)
spawner_803_2.stopSpawning()
spawner_803_2.setPos( 485, 200 )
spawner_803_2.setInitialMoveForChildren( "Beach_803", 470, 200 )
spawner_803_2.setHome( "Beach_803", 470, 200 )
spawner_803_2.setSpawnWhenPlayersAreInRoom( true )
spawner_803_2.setWaitTime( 80, 100 )
spawner_803_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_803_2.setMonsterLevelForChildren( 6.4 )

spawner_803_3 = myRooms.Beach_803.spawnSpawner( "spawner_803_3", "sand_fluff", 1)
spawner_803_3.stopSpawning()
spawner_803_3.setPos( 485, 195 )
spawner_803_3.setInitialMoveForChildren( "Beach_803", 680, 480 )
spawner_803_3.setHome( "Beach_803", 680, 480 )
spawner_803_3.setSpawnWhenPlayersAreInRoom( true )
spawner_803_3.setWaitTime( 80, 100 )
spawner_803_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_803_3.setMonsterLevelForChildren( 6.4 )

spawner_803_4 = myRooms.Beach_803.spawnSpawner( "spawner_803_4", "sand_fluff", 1)
spawner_803_4.stopSpawning()
spawner_803_4.setPos( 485, 190 )
spawner_803_4.setInitialMoveForChildren( "Beach_803", 530, 545 )
spawner_803_4.setHome( "Beach_803", 530, 545 )
spawner_803_4.setSpawnWhenPlayersAreInRoom( true )
spawner_803_4.setWaitTime( 80, 100 )
spawner_803_4.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_803_4.setMonsterLevelForChildren( 6.4 )

spawner_803_5 = myRooms.Beach_803.spawnSpawner( "spawner_803_5", "sand_fluff", 1)
spawner_803_5.stopSpawning()
spawner_803_5.setPos( 485, 190 )
spawner_803_5.setInitialMoveForChildren( "Beach_803", 300, 500 )
spawner_803_5.setHome( "Beach_803", 300, 500 )
spawner_803_5.setSpawnWhenPlayersAreInRoom( true )
spawner_803_5.setWaitTime( 80, 100 )
spawner_803_5.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_803_5.setMonsterLevelForChildren( 6.4 )

spawner_803_1.allyWithSpawner( spawner_803_2 )
spawner_803_1.allyWithSpawner( spawner_803_3 )
spawner_803_1.allyWithSpawner( spawner_803_4 )
spawner_803_1.allyWithSpawner( spawner_803_5 )
spawner_803_2.allyWithSpawner( spawner_803_3 )
spawner_803_2.allyWithSpawner( spawner_803_4 )
spawner_803_2.allyWithSpawner( spawner_803_5 )
spawner_803_3.allyWithSpawner( spawner_803_4 )
spawner_803_3.allyWithSpawner( spawner_803_5 )
spawner_803_4.allyWithSpawner( spawner_803_5 )

//--------------------------------------------------------------------------------------
// Beach_804 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_804_4 = myRooms.Beach_804.spawnSpawner( "spawner_804_4", "sand_fluff", 1)
spawner_804_4.stopSpawning()
spawner_804_4.setPos( 300, 115 )
spawner_804_4.setHome( "Beach_804", 300, 115 )
spawner_804_4.setSpawnWhenPlayersAreInRoom( true )
spawner_804_4.setWaitTime( 80 , 100 )
spawner_804_4.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_804_4.childrenWander( true )
spawner_804_4.setMonsterLevelForChildren( 6.3 )

spawner_804_5 = myRooms.Beach_804.spawnSpawner( "spawner_804_5", "sand_fluff", 1)
spawner_804_5.stopSpawning()
spawner_804_5.setPos( 320, 270 )
spawner_804_5.setHome( "Beach_804", 300, 300 )
spawner_804_5.setSpawnWhenPlayersAreInRoom( true )
spawner_804_5.setWaitTime( 80 , 100 )
spawner_804_5.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_804_5.childrenWander( true )
spawner_804_5.setMonsterLevelForChildren( 6.3 )

spawner_804_6 = myRooms.Beach_804.spawnSpawner( "spawner_804_6", "sand_fluff", 1)
spawner_804_6.stopSpawning()
spawner_804_6.setPos( 110, 435 )
spawner_804_6.setHome( "Beach_804", 110, 435 )
spawner_804_6.setSpawnWhenPlayersAreInRoom( true )
spawner_804_6.setWaitTime( 80 , 100 )
spawner_804_6.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_804_6.childrenWander( true )
spawner_804_6.setMonsterLevelForChildren( 6.3 )

spawner_804_7 = myRooms.Beach_804.spawnSpawner( "spawner_804_7", "sand_fluff", 1)
spawner_804_7.stopSpawning()
spawner_804_7.setPos( 80, 130 )
spawner_804_7.setHome( "Beach_804", 80, 130 )
spawner_804_7.setSpawnWhenPlayersAreInRoom( true )
spawner_804_7.setWaitTime( 80 , 100 )
spawner_804_7.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_804_7.childrenWander( true )
spawner_804_7.setMonsterLevelForChildren( 6.3 )

spawner_804_4.allyWithSpawner( spawner_804_5 )
spawner_804_4.allyWithSpawner( spawner_804_6 )
spawner_804_4.allyWithSpawner( spawner_804_7 )
spawner_804_5.allyWithSpawner( spawner_804_6 )
spawner_804_5.allyWithSpawner( spawner_804_7 )
spawner_804_6.allyWithSpawner( spawner_804_7 )

//--------------------------------------------------------------------------------------
// Beach_901 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_901_1 = myRooms.Beach_901.spawnSpawner( "spawner_901_1", "sand_fluff", 1)
spawner_901_1.stopSpawning()
spawner_901_1.setPos( 695, 275 )
spawner_901_1.setInitialMoveForChildren( "Beach_901", 460, 480 )
spawner_901_1.setHome( "Beach_901", 460, 480 )
spawner_901_1.setSpawnWhenPlayersAreInRoom( true )
spawner_901_1.setWaitTime( 80, 100 )
spawner_901_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_901_1.setMonsterLevelForChildren( 6.1 )

spawner_901_2 = myRooms.Beach_901.spawnSpawner( "spawner_901_2", "sand_fluff", 1)
spawner_901_2.stopSpawning()
spawner_901_2.setPos( 695, 270 )
spawner_901_2.setInitialMoveForChildren( "Beach_901", 640, 260 )
spawner_901_2.setHome( "Beach_901", 640, 260 )
spawner_901_2.setSpawnWhenPlayersAreInRoom( true )
spawner_901_2.setWaitTime( 80, 100 )
spawner_901_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_901_2.setMonsterLevelForChildren( 6.1 )

spawner_901_3 = myRooms.Beach_901.spawnSpawner( "spawner_901_3", "sand_fluff", 1)
spawner_901_3.stopSpawning()
spawner_901_3.setPos( 695, 265 )
spawner_901_3.setInitialMoveForChildren( "Beach_901", 900, 90 )
spawner_901_3.setHome( "Beach_901", 900, 90 )
spawner_901_3.setSpawnWhenPlayersAreInRoom( true )
spawner_901_3.setWaitTime( 80, 100 )
spawner_901_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_901_3.setMonsterLevelForChildren( 6.1 )

spawner_901_1.allyWithSpawner( spawner_901_2 )
spawner_901_1.allyWithSpawner( spawner_901_3 )
spawner_901_2.allyWithSpawner( spawner_901_3 )

//--------------------------------------------------------------------------------------
// Beach_902 Spawners                                                                  
//--------------------------------------------------------------------------------------
spawner_902_1 = myRooms.Beach_902.spawnSpawner( "spawner_902_1", "sand_fluff", 1)
spawner_902_1.stopSpawning()
spawner_902_1.setPos( 110, 150 )
spawner_902_1.setInitialMoveForChildren( "Beach_902", 500, 150 )
spawner_902_1.setHome( "Beach_902", 500, 150 )
spawner_902_1.setSpawnWhenPlayersAreInRoom( true )
spawner_902_1.setWaitTime( 80, 100 )
spawner_902_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_902_1.setMonsterLevelForChildren( 6.2 )

spawner_902_2 = myRooms.Beach_902.spawnSpawner( "spawner_902_2", "sand_fluff", 1)
spawner_902_2.stopSpawning()
spawner_902_2.setPos( 110, 145 )
spawner_902_2.setInitialMoveForChildren( "Beach_902", 650, 355 )
spawner_902_2.setHome( "Beach_902", 650, 355 )
spawner_902_2.setSpawnWhenPlayersAreInRoom( true )
spawner_902_2.setWaitTime( 80, 100 )
spawner_902_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_902_2.setMonsterLevelForChildren( 6.2 )

spawner_902_3 = myRooms.Beach_902.spawnSpawner( "spawner_902_3", "sand_fluff", 1)
spawner_902_3.stopSpawning()
spawner_902_3.setPos( 110, 140 )
spawner_902_3.setInitialMoveForChildren( "Beach_902", 570, 515 )
spawner_902_3.setHome( "Beach_902", 570, 515 )
spawner_902_3.setSpawnWhenPlayersAreInRoom( true )
spawner_902_3.setWaitTime( 80, 100 )
spawner_902_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_902_3.setMonsterLevelForChildren( 6.2 )

spawner_902_1.allyWithSpawner( spawner_902_2 )
spawner_902_1.allyWithSpawner( spawner_902_3 )
spawner_902_2.allyWithSpawner( spawner_902_3 )

//Spawn logic
def spawn_1001_1() {
	garlic_1001_1 = spawner_1001_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1001_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1001_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1001_1 = event.actor.getHated()
		hateList_1001_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.6) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1001_1()

def spawn_1001_2() {
	garlic_1001_2 = spawner_1001_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1001_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1001_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1001_2 = event.actor.getHated()
		hateList_1001_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.6) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1001_2()

def spawn_1001_3() {
	garlic_1001_3 = spawner_1001_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1001_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1001_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1001_3 = event.actor.getHated()
		hateList_1001_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.6) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1001_3()

def spawn_1002_1() {
	garlic_1002_1 = spawner_1002_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1002_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1002_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1002_1 = event.actor.getHated()
		hateList_1002_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1002_1()

def spawn_1002_2() {
	garlic_1002_2 = spawner_1002_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1002_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1002_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1002_2 = event.actor.getHated()
		hateList_1002_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1002_2()

def spawn_1002_3() {
	garlic_1002_3 = spawner_1002_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1002_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1002_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1002_3 = event.actor.getHated()
		hateList_1002_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1002_3()

def spawn_1003_1() {
	garlic_1003_1 = spawner_1003_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1003_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1003_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1003_1 = event.actor.getHated()
		hateList_1003_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.8) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1003_1()

def spawn_1003_2() {
	garlic_1003_2 = spawner_1003_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1003_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1003_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1003_2 = event.actor.getHated()
		hateList_1003_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.8) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1003_2()

def spawn_1003_3() {
	garlic_1003_3 = spawner_1003_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1003_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1003_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1003_3 = event.actor.getHated()
		hateList_1003_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.8) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1003_3()

def spawn_1004_1() {
	garlic_1004_1 = spawner_1004_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1004_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1004_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1004_1 = event.actor.getHated()
		hateList_1004_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1004_1()

def spawn_1004_2() {
	garlic_1004_2 = spawner_1004_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1004_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1004_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1004_2 = event.actor.getHated()
		hateList_1004_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1004_2()

def spawn_1004_3() {
	garlic_1004_3 = spawner_1004_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1004_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1004_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1004_3 = event.actor.getHated()
		hateList_1004_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1004_3()

def spawn_1004_4() {
	garlic_1004_4 = spawner_1004_4.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_1004_4) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_1004_4()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_1004_4 = event.actor.getHated()
		hateList_1004_4.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_1004_4()

def spawn_802_1() {
	garlic_802_1 = spawner_802_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_802_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_802_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_802_1 = event.actor.getHated()
		hateList_802_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_802_1()

def spawn_802_2() {
	garlic_802_2 = spawner_802_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_802_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_802_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_802_2 = event.actor.getHated()
		hateList_802_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_802_2()

def spawn_802_3() {
	garlic_802_3 = spawner_802_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_802_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_802_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_802_3 = event.actor.getHated()
		hateList_802_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_802_3()

def spawn_802_4() {
	garlic_802_4 = spawner_802_4.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_802_4) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_802_4()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_802_4 = event.actor.getHated()
		hateList_802_4.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_802_4()

def spawn_803_1() {
	garlic_803_1 = spawner_803_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_803_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_803_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_803_1 = event.actor.getHated()
		hateList_803_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.0) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_803_1()

def spawn_803_2() {
	garlic_803_2 = spawner_803_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_803_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_803_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_803_2 = event.actor.getHated()
		hateList_803_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.0) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_803_2()

def spawn_803_3() {
	garlic_803_3 = spawner_803_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_803_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_803_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_803_3 = event.actor.getHated()
		hateList_803_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.0) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_803_3()

def spawn_803_4() {
	garlic_803_4 = spawner_803_4.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_803_4) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_803_4()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_803_4 = event.actor.getHated()
		hateList_803_4.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.0) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_803_4()

def spawn_803_5() {
	garlic_803_5 = spawner_803_5.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_803_5) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_803_5()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_803_5 = event.actor.getHated()
		hateList_803_5.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.0) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_803_5()

def spawn_804_4() {
	garlic_804_4 = spawner_804_4.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_804_4) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_804_4()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_804_4 = event.actor.getHated()
		hateList_804_4.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_804_4()

def spawn_804_5() {
	garlic_804_5 = spawner_804_5.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_804_5) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_804_5()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_804_5 = event.actor.getHated()
		hateList_804_5.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_804_5()

def spawn_804_6() {
	garlic_804_6 = spawner_804_6.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_804_6) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_804_6()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_804_6 = event.actor.getHated()
		hateList_804_6.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_804_6()

def spawn_804_7() {
	garlic_804_7 = spawner_804_7.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_804_7) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_804_7()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_804_7 = event.actor.getHated()
		hateList_804_7.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.9) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_804_7()

def spawn_901_1() {
	garlic_901_1 = spawner_901_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_901_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_901_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_901_1 = event.actor.getHated()
		hateList_901_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_901_1()

def spawn_901_2() {
	garlic_901_2 = spawner_901_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_901_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_901_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_901_2 = event.actor.getHated()
		hateList_901_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_901_2()

def spawn_901_3() {
	garlic_901_3 = spawner_901_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_901_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_901_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_901_3 = event.actor.getHated()
		hateList_901_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.7) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_901_3()

def spawn_902_1() {
	garlic_902_1 = spawner_902_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_902_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_902_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_902_1 = event.actor.getHated()
		hateList_902_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.8) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_902_1()

def spawn_902_2() {
	garlic_902_2 = spawner_902_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_902_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_902_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_902_2 = event.actor.getHated()
		hateList_902_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.8) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_902_2()

def spawn_902_3() {
	garlic_902_3 = spawner_902_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_902_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_902_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_902_3 = event.actor.getHated()
		hateList_902_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 7.8) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_902_3()