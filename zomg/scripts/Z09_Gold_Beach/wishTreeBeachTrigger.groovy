import com.gaiaonline.mmo.battle.script.*;

//===============================
//WISH TREE VISION TRIGGER ZONE  
//===============================

onQuestStep(51, 2) { event -> event.player.addMiniMapQuestActorName( "Wish Tree-VQS" ) }

//========THE LIGHTHOUSE=========
def wishTreeLighthouseTrigger = "wishTreeLighthouseTrigger"
myRooms.Beach_102.createTriggerZone(wishTreeLighthouseTrigger, 150, 475, 455, 620)

myManager.onTriggerIn(myRooms.Beach_102, wishTreeLighthouseTrigger) { event ->
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 51, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z09FoundWishTreeBeachClue" ) ) {
		event.actor.say("I found the Wish Tree vision area...the Lighthouse at Gold Beach.")
		event.actor.updateQuest(51, "Story3-VQS" ) //push completion for this leg of the Vision Quest
		event.actor.setQuestFlag( GLOBAL, "Z09FoundWishTreeBeachClue" )
	}
}