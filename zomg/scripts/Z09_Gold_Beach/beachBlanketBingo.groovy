import com.gaiaonline.mmo.battle.script.*;

//=============================================
// TO DO LIST:                                 
// - Bonfire shape switching at certain GSTs   
// - Trigger music to occur when bonfire starts
//=============================================

//=============================================
// TRIGGER ZONES                               
//=============================================

def dylanDanceTrigger = "dylanDanceTrigger"
myRooms.Beach_1006.createTriggerZone( dylanDanceTrigger, 260, 500, 390, 600 )

def surfGirlDanceTrigger = "surfGirlDanceTrigger"
myRooms.Beach_1006.createTriggerZone( surfGirlDanceTrigger, 490, 260, 585, 345 )

def beachDisposalTrigger = "beachDisposalTrigger"
myRooms.Beach_1005.createTriggerZone( beachDisposalTrigger, 205, 530, 500, 650 )

def ikeTrigger = "ikeTrigger"
myRooms.Beach_1006.createTriggerZone( ikeTrigger, 220, 280, 330, 360 )

def surfChickDanceTrigger = "surfChickDanceTrigger"
myRooms.Beach_1006.createTriggerZone( surfChickDanceTrigger, 805, 420, 910, 500 )

def waterDisposalTrigger = "waterDisposalTrigger"
myRooms.Beach_806.createTriggerZone( waterDisposalTrigger, 320, 550, 455, 645 )


//=============================================
// WALK ON / WALK OFF PATHS                    
//=============================================

dylanWalkOn = makeNewPatrol()
dylanWalkOn.addPatrolPoint( "Beach_1005", 430, 600, 0 )
dylanWalkOn.addPatrolPoint( "Beach_1005", 730, 310, 0 )
dylanWalkOn.addPatrolPoint( "Beach_1006", 325, 550, 0 )

dylanWalkOff = makeNewPatrol()
dylanWalkOff.addPatrolPoint( "Beach_1006", 325, 550, 0 )
dylanWalkOff.addPatrolPoint( "Beach_1005", 730, 310, 0 )
dylanWalkOff.addPatrolPoint( "Beach_1005", 430, 600, 0 )
dylanWalkOff.addPatrolPoint( "Beach_1005", 460, 650, 0 )

dylanDance = makeNewPatrol()
dylanDance.addPatrolPoint( "Beach_1006", 260, 540, 1 )
dylanDance.addPatrolPoint( "Beach_1006", 280, 540, 1 )

surfGirlWalkOn = makeNewPatrol()
surfGirlWalkOn.addPatrolPoint( "Beach_1005", 280, 600, 0 )
surfGirlWalkOn.addPatrolPoint( "Beach_1005", 325, 260, 0 )
surfGirlWalkOn.addPatrolPoint( "Beach_1005", 860, 200, 0 )
surfGirlWalkOn.addPatrolPoint( "Beach_1006", 530, 300, 0 )

surfGirlWalkOff = makeNewPatrol()
surfGirlWalkOff.addPatrolPoint( "Beach_1006", 530, 300, 0 )
surfGirlWalkOff.addPatrolPoint( "Beach_1005", 860, 200, 0 )
surfGirlWalkOff.addPatrolPoint( "Beach_1005", 325, 260, 0 )
surfGirlWalkOff.addPatrolPoint( "Beach_1005", 280, 640, 0 )

surfGirlDance = makeNewPatrol()
surfGirlDance.addPatrolPoint( "Beach_1006", 460, 300, 0 )
surfGirlDance.addPatrolPoint( "Beach_1006", 540, 300, 0 )
surfGirlDance.addPatrolPoint( "Beach_1006", 480, 260, 1 )

ikeWalkOn = makeNewPatrol()
ikeWalkOn.addPatrolPoint( "Beach_806", 380, 620, 0 )
ikeWalkOn.addPatrolPoint( "Beach_906", 330, 610, 0 )
ikeWalkOn.addPatrolPoint( "Beach_1006", 300, 350, 0 )

ikeWalkOff = makeNewPatrol()
ikeWalkOff.addPatrolPoint( "Beach_1006", 320, 110, 0 )
ikeWalkOff.addPatrolPoint( "Beach_906", 330, 610, 0 )
ikeWalkOff.addPatrolPoint( "Beach_806", 400, 575, 0 )

surfChickWalkOn = makeNewPatrol()
surfChickWalkOn.addPatrolPoint( "Beach_806", 720, 620, 0 )
surfChickWalkOn.addPatrolPoint( "Beach_906", 510, 530, 0 )
surfChickWalkOn.addPatrolPoint( "Beach_1006", 855, 455, 0 )

surfChickWalkOff = makeNewPatrol()
surfChickWalkOff.addPatrolPoint( "Beach_1006", 855, 455, 0 )
surfChickWalkOff.addPatrolPoint( "Beach_906", 510, 530, 0 )
surfChickWalkOff.addPatrolPoint( "Beach_806", 400, 575, 0 )

surfChickDance = makeNewPatrol()
surfChickDance.addPatrolPoint( "Beach_1006", 820, 465, 0 )
surfChickDance.addPatrolPoint( "Beach_1006", 820, 545, 0 )

boomBoxDance = makeNewPatrol()
boomBoxDance.addPatrolPoint( "Beach_1006", 750, 240, 0 )
boomBoxDance.addPatrolPoint( "Beach_1006", 805, 280, 0 )

//=============================================
// WALK ON TRIGGER LOGIC                       
//=============================================

dylanDancing = false
surfGirlDancing = false
boomBoxGalDancing = false
surfChickDancing = false

myManager.onTriggerIn(myRooms.Beach_1006, dylanDanceTrigger) { event ->
	if( event.actor == Dylan && partyTime == true && dylanDancing == false ) {
		Dylan.setPatrol( dylanDance )
		Dylan.startPatrol()
		dylanDancing = true
		activateDylanSpeech()
	}
}

myManager.onTriggerIn(myRooms.Beach_1006, surfGirlDanceTrigger) { event ->
	if( event.actor == surfGirl && partyTime == true && surfGirlDancing == false ) {
		surfGirl.setPatrol( surfGirlDance )
		surfGirl.startPatrol()
		surfGirlDancing = true
	}
	if( event.actor == boomBoxGal && partyTime == true && boomBoxGalDancing == false ) {
		boomBoxGal.setPatrol( boomBoxDance )
		boomBoxGal.startPatrol()
		boomBoxGalDancing = true
	}
}

myManager.onTriggerIn(myRooms.Beach_1006, surfChickDanceTrigger) { event ->
	if( event.actor == surfChick && partyTime == true && surfChickDancing == false ) {
		surfChick.setPatrol( surfChickDance )
		surfChick.startPatrol()
		surfChickDancing = true
	}
}

myManager.onTriggerIn(myRooms.Beach_1006, ikeTrigger) { event ->
	if( event.actor == Ike && partyTime == true ) {
		Ike.pause()
		Ike.setRotation( 135 )
		activateIkeSpeech()
	}
}

//=============================================
// WALK OFF LOGIC                              
//=============================================

myManager.onTriggerIn(myRooms.Beach_1005, beachDisposalTrigger) { event ->
	if( event.actor == Dylan && partyTime == false ) {
		Dylan.dispose()
		dylanDancing = false
		dylanDead = true
		Dylan = null
	}
	if( event.actor == boomBoxGal && partyTime == false ) {
		boomBoxGal.dispose()
		boomBoxGalDancing = false
		boomBoxGalDead = true
		boomBoxGal = null
	}
	if( event.actor == surfGirl && partyTime == false ) {
		surfGirl.dispose()
		surfGirlDancing = false
		surfGirlDead = true
		surfGirl = null
	}
}

myManager.onTriggerIn(myRooms.Beach_806, waterDisposalTrigger) { event ->
	if( event.actor == Ike && partyTime == false ) {
		Ike.dispose()
		ikeDead = true
		Ike = null
	}
	if( event.actor == surfChick && partyTime == false ) {
		surfChick.dispose()
		surfChickDancing = false
		surfChickDead = true
		surfChick = null
	}
}

//Initialize variables
Dylan = null
surfGirl = null
boomBoxGal = null
Ike = null
surfChick = null
partyTime = false
spawnedNPCs = false

dylanDead = true
surfGirlDead = true
boomBoxGalDead = true
ikeDead = true
surfChickDead = true

//THE BONFIRE SWITCH
bonfire = makeSwitch( "beachBonfire", myRooms.Beach_1006, 580, 475 )

bonfire.lock()
bonfire.setUsable( false )

//check the clock every 10 minutes to see if the spawn or despawn should occur.

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() }
}

def partyChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) { //party goes from 7pm to 2am.
//	if( gst() > 0 && gst() < 2359 ) { //This line for each setting of time parameters for testing.
		partyTime = true
		bonfire.on()
		sound("Bonfire").toZone()
	} else {
		partyTime = false
		bonfire.off()
		stopSound("Bonfire").toZone()		
	}
}	

def spawnOrNoSpawn() {
	partyChecker()
//	println "<<<<<< partyTime = ${partyTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"
	if( partyTime == false && spawnedNPCs == true ) {
		Dylan.setPatrol( dylanWalkOff )
		Dylan.startPatrol()
		
		surfGirl.setPatrol( surfGirlWalkOff )
		surfGirl.startPatrol()
		
		boomBoxGal.setPatrol( surfGirlWalkOff )
		boomBoxGal.startPatrol()
		
		Ike.setPatrol( ikeWalkOff )
		Ike.startPatrol()
		
		surfChick.setPatrol( surfChickWalkOff )
		surfChick.startPatrol()
		
		spawnedNPCs = false
		clockChecker()
	} else if( partyTime == true && spawnedNPCs == false ) { 
		if( dylanDead == true ) {
			Dylan = spawnNPC( "Dylan-VQS", myRooms.Beach_1005, 430, 620 )
			Dylan.setDisplayName( "Dylan" )
			dylanDead = false
			Dylan.setPatrol( dylanWalkOn )
			Dylan.startPatrol()
		}

		if( boomBoxGalDead == true ) {
			boomBoxGal = spawnNPC( "Boombox Gal-VQS", myRooms.Beach_1005, 280, 620 )
			boomBoxGal.setDisplayName( "Sky" )
			boomBoxGalDead = false
			boomBoxGal.setPatrol( surfGirlWalkOn )
			boomBoxGal.startPatrol()
		}
		if( surfGirlDead == true ) {
			myManager.schedule(2) {
				surfGirl = spawnNPC( "SurferGal-VQS", myRooms.Beach_1005, 280, 620 )
				surfGirl.setDisplayName( "Sandy" )
				surfGirlDead = false
				surfGirl.setPatrol( surfGirlWalkOn )
				surfGirl.startPatrol()
			}
		}
		if( ikeDead == true ) {
			Ike = spawnNPC( "Ike-VQS", myRooms.Beach_806, 380, 620 )
			Ike.setDisplayName( "Ike" )
			ikeDead = false
			Ike.setPatrol( ikeWalkOn )
			Ike.startPatrol()
		}
		if( surfChickDead == true ) {
			surfChick = spawnNPC( "SurferGirl-VQS", myRooms.Beach_806, 720, 620 )
			surfChick.setDisplayName( "Half-Pint" )
			surfChickDead = false
			surfChick.setPatrol( surfChickWalkOn )
			surfChick.startPatrol()
		}
		
		spawnedNPCs = true
		clockChecker()
	} else {
		clockChecker()
//		println "****** Nothing happened ******"
	}
}

clockChecker()

//=======================================================
// BONFIRE BURNING ROUTINE (step in me and take damage!) 
//=======================================================

burnMe = []
removalList = []
burningNotAlreadyStarted = true

def bonfireBurning = "bonfireBurning"
myRooms.Beach_1006.createTriggerZone( bonfireBurning, 480, 425, 690, 525 )

myManager.onTriggerIn(myRooms.Beach_1006, bonfireBurning ) { event ->
	if( isPlayer( event.actor ) && partyTime ) {
		burnMe << event.actor
//		println "**** burnMe = ${burnMe} and burningNotAlreadyStarted = ${burningNotAlreadyStarted}****"
		if( burningNotAlreadyStarted ) {
			burningNotAlreadyStarted = false
			burningMan()
		}
	}
	if( isPlayer( event.actor ) && partyTime == false ) {
		//println "****Bonfire not burning currently. No damage to player****"
	}
}

myManager.onTriggerOut(myRooms.Beach_1006, bonfireBurning ) { event ->
	if( isPlayer( event.actor ) ) {
		burnMe.remove( event.actor )
	}
}

//This routine continues to burn everyone on the "burnMe" list until all players have left the fire (and thus, left the list)
def burningMan() {
//	println "****GOT TO BURNING MAN****"
	if( burnMe.isEmpty() ) {
		burningNotAlreadyStarted = true
	}
//	println "**** burningNotAlreadyStarted (in Burning Man) = ${burningNotAlreadyStarted}****"
	if( burningNotAlreadyStarted == false ) {
		burnMe.clone().each{
			if( !it.isDazed() ) {
				it.centerPrint( "Fire BAD!" )
				it.instantPercentDamage(10)
			} else { //if they ARE dazed then remove the dazed person so he doesn't keep burning
				removalList << it
			}
		}
		burnMe.removeAll( removalList )
		removalList = []
		myManager.schedule(0.5) { burningMan() }
	}
}

//TODO: WOULD BE COOL to write a "line dancing" routine where the surfers do the Electric Slide once in a while during the party.

def activateIkeSpeech() {
	
	onQuestStep( 94, 2 ) { event -> event.player.addMiniMapQuestActorName( "Ike-VQS" ) }
	
	//------------------------------------------------------------
	// IKE DEFAULT DIALOG                                         
	//------------------------------------------------------------

	def ikeDefault = Ike.createConversation( "ikeDefault", true, "!QuestStarted_94", "!QuestCompleted_94", "!Z9_Ike_Waterspouts_Break")

	def default1 = [id:1]
	default1.npctext = "Hey bro, how's it goin'?"
	default1.options = []
	default1.options << [text:"Pretty good Ike. How about you?", result: 2]
	default1.options << [text:"Bro? Didn't you notice I'm a girl?!", result: 3]
	default1.options << [text:"Actually, I'm having kind of a crappy day.", result: 4]
	ikeDefault.addDialog( default1, Ike )
	
	def default2 = [id:2]
	default2.npctext = "Not bad. Just chillin' next to the fire until the Water Spouts leave."
	default2.options = []
	default2.options << [text:"Cool. Mind if I join you?", result: 5]
	default2.options << [text:"Um... Water Spouts? WTF is a Water Spout?", result: 6]
	ikeDefault.addDialog(default2, Ike)
	
	def default3 = [id:3]
	default3.npctext = "Whoa, take a chill. I call everyone bro, it's nothing personal."
	default3.playertext = "That's weird. So what's going on here?"
	default3.result = 7
	ikeDefault.addDialog(default3, Ike)
	
	def default4 = [id:4]
	default4.npctext = "Why don't you come hang out by the fire and relax? It always makes me feel better, bro."
	default4.options = []
	default4.options << [text:"Yeah... it's already starting to help a little.", result: 8]
	default4.options << [text:"Why don't you mind your own freaking business?", result: 9]
	ikeDefault.addDialog(default4, Ike)
	
	def default5 = [id:5]
	default5.npctext = "Not at all, bro. Pull up a seat."
	default5.options = []
	default5.options << [text:"Thanks. So tell me... what the heck is a Water Spout?", result: 6]
	default5.options << [text:"Thanks. Take care Ike.", result: 10]
	ikeDefault.addDialog(default5, Ike)
	
	def default6 = [id:6]
	default6.npctext = "They're a total bummer, bro. They're like little tornadoes in the water. It's impossible to surf with them out there. Luckily they seem to only come out at night and most of us know to get out of the water."
	default6.playertext = "Why doesn't somebody do something about them?"
	default6.result = 11
	ikeDefault.addDialog(default6, Ike)
	
	def default7 = [id:7]
	default7.npctext = "Oh not much. Just hanging out next to the fire until the Water Spouts leave."
	default7.options = []
	default7.options << [text:"Mind if I hang out too?", result: 5]
	default7.options << [text:"What in the world is a Water Spout?", result: 6]
	ikeDefault.addDialog(default7, Ike)
	
	def default8 = [id:8]
	default8.npctext = "See what I mean!"
	default8.options = []
	default8.options << [text:"I totally do. So what's going on here?", result: 7]
	default8.options << [text:"Yeah. I think I'm gonna kick back some, later.", result: 10]
	ikeDefault.addDialog(default8, Ike)
	
	def default9 = [id:9]
	default9.npctext = "Umm... alright. Take care, bro"
	default9.result = DONE
	ikeDefault.addDialog(default9, Ike)
	
	def default10 = [id:10]
	default10.npctext = "Take care, bro."
	default10.flag = "Z9_Ike_Waterspouts_Break"
	default10.result = DONE
	ikeDefault.addDialog(default10, Ike)
	
	def default11 = [id:11]
	default11.npctext = "Surfers are peaceful, bro. We just like to chill, not fight."
	default11.options = []
	default11.options << [text:"Well, I could probably do something about the Water Spouts... at least provide a little safety for any surfers that don't know better.", result: 12]
	default11.options << [text:"Yeah, good point. I guess it's more chill to just wait it out. Alright, I'm going to enjoy the fire.", result: 10]
	ikeDefault.addDialog(default11, Ike)
	
	def default12 = [id:12]
	default12.npctext = "Cool, they're all along the shore north of here... can't miss them."
	default12.quest = 94
	default12.result = DONE
	ikeDefault.addDialog(default12, Ike)

	//------------------------------------------------------------
	// Waterspout Break                                           
	//------------------------------------------------------------
	def waterspoutBreak = Ike.createConversation("waterspoutBreak", true, "Z9_Ike_Waterspouts_Break")
	
	def waterspoutBreak1 = [id:1]
	waterspoutBreak1.npctext = "Hey bro, wb. What can I do for ya?"
	waterspoutBreak1.options = []
	waterspoutBreak1.options << [text:"Hey, tell me about the Water Spouts.", result: 2]
	waterspoutBreak1.options << [text:"Uhm... I can't think of anything. Just saying 'hi.'", result: DONE]
	waterspoutBreak.addDialog(waterspoutBreak1, Ike)
	
	def waterspoutBreak2 = [id:2]
	waterspoutBreak2.npctext = "Sure. They're like little tornadoes in the water. It's impossible to surf with them out there. They're a total bummer, bro."
	waterspoutBreak2.options = []
	waterspoutBreak2.options << [text:"Man, that sucks. Want me to take care of a few of them?", result: 3]
	waterspoutBreak2.options << [text:"Sounds nasty. I'll be sure to avoid them.", result: DONE]
	waterspoutBreak.addDialog(waterspoutBreak2, Ike)
	
	def waterspoutBreak3 = [id:3]
	waterspoutBreak3.npctext = "That'd be totally cool, bro. They're all along the shore north of here. Can't miss them."
	waterspoutBreak3.quest = 94
	waterspoutBreak3.flag = "!Z9_Ike_Waterspouts_Break"
	waterspoutBreak3.result = DONE
	waterspoutBreak.addDialog(waterspoutBreak3, Ike)
	
	//------------------------------------------------------------
	// Waterspout Active                                          
	//------------------------------------------------------------
	def waterspoutActive = Ike.createConversation("waterspoutActive", true, "QuestStarted_94:2")
	
	def waterspoutActive1 = [id:1]
	waterspoutActive1.npctext = "How goes it with the Water Spouts, bro?"
	waterspoutActive1.playertext = "Still working on that one, Ike."
	waterspoutActive1.result = 2
	waterspoutActive.addDialog(waterspoutActive1, Ike)
	
	def waterspoutActive2 = [id:2]
	waterspoutActive2.npctext = "Alright, good luck."
	waterspoutActive2.result = DONE
	waterspoutActive.addDialog(waterspoutActive2, Ike)
	
	//------------------------------------------------------------
	// Waterspout Complete                                        
	//------------------------------------------------------------
	def waterspoutComplete = Ike.createConversation("waterspoutComplete", true, "QuestStarted_94:3")
	
	def waterspoutComplete1 = [id:1]
	waterspoutComplete1.npctext = "Whoa! You totally did it. Thanks bro, we won't have to worry about swirlie nightmares for a while."
	waterspoutComplete1.playertext = "Just glad I could help."
	waterspoutComplete1.exec = { event -> event.player.removeMiniMapQuestActorName("Ike-VQS") }
	waterspoutComplete1.quest = 94
	waterspoutComplete1.result = DONE
	waterspoutComplete.addDialog(waterspoutComplete1, Ike)
	
	//------------------------------------------------------------
	// Waterspout Finished                                        
	//------------------------------------------------------------
	def waterspoutFinished = Ike.createConversation("waterspoutFinished", true, "QuestCompleted_94")
	
	def waterspoutFinished1 = [id:1]
	waterspoutFinished1.npctext = "Hey bro, how's it goin'?"
	waterspoutFinished1.options = []
	waterspoutFinished1.options << [text:"Going good. Why aren't you surfing? I thought it was safe now...", result: 2]
	waterspoutFinished1.options << [text:"I thought I told you I was a girl. What's with calling me bro still?", result: 3]
	waterspoutFinished1.options << [text:"Actually, I'm looking for some information.", result: 4]
	waterspoutFinished.addDialog(waterspoutFinished1, Ike)
	
	def waterspoutFinished2 = [id:2]
	waterspoutFinished2.npctext = "Heh well, you see... I tried. I went out, but there were all these weird lights under the water. I got creeped out and came back to the party."
	waterspoutFinished2.playertext = "WTF? What kind of lights?"
	waterspoutFinished2.result = 5
	waterspoutFinished.addDialog(waterspoutFinished2, Ike)
	
	def waterspoutFinished3 = [id:3]
	waterspoutFinished3.npctext = "It's a surfer thing, bro."
	waterspoutFinished3.playertext = "Bah! So what are you doing here? I figured you'd be out surfing."
	waterspoutFinished3.result = 2
	waterspoutFinished.addDialog(waterspoutFinished3, Ike)
	
	def waterspoutFinished4 = [id:4]
	waterspoutFinished4.npctext = "Information? What kind of information?"
	waterspoutFinished4.playertext = "Information on the Animated! Any idea where they're coming from?"
	waterspoutFinished4.result = 6
	waterspoutFinished.addDialog(waterspoutFinished4, Ike)
	
	def waterspoutFinished5 = [id:5]
	waterspoutFinished5.npctext = "I dunno, bro... creepy lights. Some of them were moving around, some were blinking or fading in and out, and others were just there. It looked like they were spread across the bottom of the whole ocean!"
	waterspoutFinished5.options = []
	waterspoutFinished5.options << [text:"I've heard about some kind of fish that glows, do you think that's what it was?", result: 7]
	waterspoutFinished5.options << [text:"Do you think it has something to do with the explosion or maybe the Animated?", result: 8]
	waterspoutFinished5.options << [text:"Freaky... I wonder what's down there.", result: 9]
	waterspoutFinished.addDialog(waterspoutFinished5, Ike)
	
	def waterspoutFinished6 = [id:6]
	waterspoutFinished6.npctext = "Nah, not really bro. All I really know is that they weren't around before the explosion. Actually neither were the lights at the bottom of the sea."
	waterspoutFinished6.playertext = "Lights? What lights?"
	waterspoutFinished6.result = 5
	waterspoutFinished.addDialog(waterspoutFinished6, Ike)
	
	def waterspoutFinished7 = [id:7]
	waterspoutFinished7.npctext = "I'm not sure... I've never seen anything like it. Whatever it was, it was huge! Maybe you should find a way down there and check it out."
	waterspoutFinished7.playertext = "Hmm, well I can't exactly breathe underwater so that might be a bit tough."
	waterspoutFinished7.result = 10
	waterspoutFinished.addDialog(waterspoutFinished7, Ike)
	
	def waterspoutFinished8 = [id:8]
	waterspoutFinished8.npctext = "No idea, bro. I know it never happened before the explosion and that's about when the Animated started showing up but I couldn't tell you if that's a coincidence or a connection. If you wanna know the answer you should probably go check it out yourself."
	waterspoutFinished8.playertext = "Heh... I have a little trouble breathing underwater so I'm not sure how I'd manage that."
	waterspoutFinished8.result = 10
	waterspoutFinished.addDialog(waterspoutFinished8, Ike)
	
	def waterspoutFinished9 = [id:9]
	waterspoutFinished9.npctext = "Couldn't tell ya... all I can say is that if you want to find out you're gonna have to go down there yourself."
	waterspoutFinished9.playertext = "Right... and how am I supposed to do that? I can't exactly hold my breath long enough to explore the bottom of the ocean."
	waterspoutFinished9.result = 10
	waterspoutFinished.addDialog(waterspoutFinished9, Ike)
	
	def waterspoutFinished10 = [id:10]
	waterspoutFinished10.npctext = "There's gotta be some way. Maybe a diving suit or something... I dunno. You should check with Jacques up at the lighthouse. He's really old and used to be a sailor. He knows all kinds of stuff about the ocean... maybe he can help you get to the bottom."
	waterspoutFinished10.playertext = "I guess it's worth looking into. Thanks Ike."
	waterspoutFinished10.result = DONE
	waterspoutFinished.addDialog(waterspoutFinished10, Ike)
	
}

//------------------------------------------------------------
// DYLAN DEFAULT DIALOG                                       
//------------------------------------------------------------

def activateDylanSpeech() {

	def DefaultDylan = Dylan.createConversation( "DefaultDylan", true )

	def default1 = [id:1]
	default1.npctext = "Hey there. I'm Dylan. Enjoying the party?"
	default1.playertext = "Nice bonfire. You guys do this a lot?"
	default1.exec = { event ->
		Dylan.pause()
	}
	default1.result = 2
	DefaultDylan.addDialog( default1, Dylan )
	
	def default2 = [id:2]
	default2.npctext = "Pretty much every night. We used to do some night surfing when the moon was full, but ever since those water spout things started showing up...well...the bonfire's looking pretty good at night now."
	default2.playertext = "The water spouts don't like you out there?"
	default2.result = 3
	DefaultDylan.addDialog( default2, Dylan )
	
	def default3 = [id:3]
	default3.npctext = "Well, to be honest, we don't push them too hard. A wave might take you out while you're surfing, but not usually on purpose, y'know what I mean?"
	default3.playertext = "heh. Right. Absence is the better part of valor, eh?"
	default3.result = 4
	DefaultDylan.addDialog( default3, Dylan )
	
	def default4 = [id:4]
	default4.npctext = "Exactly. Meanwhile, the fire's warm and the company's cool, so we're aces anyway."
	default4.playertext = "Gotcha. So what's going on around here?"
	default4.result = 5
	DefaultDylan.addDialog( default4, Dylan )
	
	def default5 = [id:5]
	default5.npctext = "Mostly, we're just hanging out. This little section of beach seems to be pretty normal. That's why we do the bonfire here. Further west is pretty crazy though. It seems like the closer you get to the old aqueduct or the Otami ruins, the crazier things get."
	default5.playertext = "Oh yeah? What sorts of things?"
	default5.result = 6
	DefaultDylan.addDialog( default5, Dylan )
	
	def default6 = [id:6]
	default6.npctext = "Which area are you asking about?"
	default6.options = []
	default6.options << [text: "The Otami Ruins", result: 7]
	default6.options << [text: "The Old Aqueduct", result: 12]
	default6.options << [text: "Here at Gold Beach", result: 23]
	default6.options << [text: "Maybe I don't need to know after all. Thanks anyway.", result: 26]
	DefaultDylan.addDialog( default6, Dylan )
	
	def default7 = [id:7]
	default7.npctext = "The ruins? Man...all that statue stuff from the old ruins has come to life. It's nuts out there. And that crazy girl from school...you know the one with the machete?...is running all over the place there, asking questions about her missing Dad and looking for things to whack."
	default7.playertext = "Whoa. Have you been out there?"
	default7.result = 8
	DefaultDylan.addDialog( default7, Dylan )
	
	def default8 = [id:8]
	default8.npctext = "Me? No way. Even without the statues, I'm not much into jungles. Too many bugs and too little insect repellant. Besides, even before the Animated started showing up, that place was weird for all sorts of reasons."
	default8.playertext = "How was it weird?"
	default8.result = 9
	DefaultDylan.addDialog( default8, Dylan )
	
	def default9 = [id:9]
	default9.npctext = "Oh, you know. The stories about the old race that lived in the ruins still being around, or the explorers that go on about being 'touched' by old gods and stuff."
	default9.playertext = "Wow."
	default9.result = 10
	DefaultDylan.addDialog( default9, Dylan )
	
	def default10 = [id:10]
	default10.npctext = "I didn't used to believe any of it...but nowadays, who's to say what's possible and what's not, y'know?"
	default10.playertext = "No doubt. What else can you tell me?"
	default10.result = 11
	DefaultDylan.addDialog( default10, Dylan )
	
	def default11 = [id:11]
	default11.npctext = "About the ruins? Not much...like I said, I haven't been there myself. Those are just stories I've heard."
	default11.playertext = "Maybe you can tell me about one of those other areas then?"
	default11.result = 6
	DefaultDylan.addDialog( default11, Dylan )
	
	def default12 = [id:12]
	default12.npctext = "That's a warzone, man. The Star Portal is still active there from when the Zurg invaded a couple years ago. You remember that, right?"
	default12.options = []
	default12.options << [text: "Yeah...I remember that.", result: 20]
	default12.options << [text: "Zurg? Invasion? What?", result: 13]
	DefaultDylan.addDialog( default12, Dylan )
	
	default13 = [id:13]
	default13.npctext = "Oh man...where you been? Under a rock? This whole area got invaded by an alien race and nobody TOLD you?"
	default13.playertext = "Are you kidding me?"
	default13.result = 14
	DefaultDylan.addDialog( default13, Dylan )
	
	def default14 = [id:14]
	default14.npctext = "No, I'm not kidding you. It was after the Gambinos did that fission thing and blew apart. You remember *that*, right?"
	default14.options = []
	default14.options << [text: "Sure! Who wouldn't?", result: 27]
	default14.options << [text: "Ummm...no. What 'fission thing'?", result: 17]
	DefaultDylan.addDialog( default14, Dylan )
	
	def default15 = [id:15]
	default15.npctext = "Oh, man...you gotta get out more, friend. Okay, you're gonna have to ask other people for more details, but the guy that runs G-Corp, Johnny Gambino, somehow got merged with his son, Gino. They used some sort of super-juice or somethin' and it smashed them together into one body. Don't *even* ask me how. I don't know."
	default15.playertext = "They...merged into one body somehow. Johnny and Gino. Okaaay..."
	default15.result = 16
	DefaultDylan.addDialog( default15, Dylan )
	
	def default16 = [id:16]
	default16.npctext = "Yeah. Only no one knew they merged until one day they got blown apart by a dude that used to work for them."
	default16.playertext = "Blown apart? Who...?"
	default16.result = 17
	DefaultDylan.addDialog( default16, Dylan )
	
	def default17 = [id:17]
	default17.npctext = "Don't ask ME, man. I just know that when those two dudes split apart, a ton of energy got released. It supposedly *looked* like a nuke went off...but it didn't really do that much damage. But everyone felt the energy wash over them, even out beyond Aekea and Durem. It was HUGE!"
	default17.playertext = "All right. That's really weird. But what's it got to do with an alien invasion?"
	default17.result = 18
	DefaultDylan.addDialog( default17, Dylan )
	
	def default18 = [id:18]
	default18.npctext = "Well, apparently, the blast was some sort of energy that could be detected off-planet. I mean...nobody knew there were REALLY aliens out there, but we did after they saw that blast."
	default18.playertext = "They saw the blast from *outer space*???"
	default18.result = 19
	DefaultDylan.addDialog( default18, Dylan )
	
	def default19 = [id:19]
	default19.npctext = "Yeah. It was big. And it happened right near here. This was sort of ground zero for the whole thing. But anyway, the Zurg invaded after that. They wanted to find out more about that energy source."
	default19.playertext = "So what happened then?"
	default19.result = 20
	DefaultDylan.addDialog( default19, Dylan )
	
	def default20 = [id:20]
	default20.npctext = "Well, to tell the truth, I don't know much about the Zurg. But they put the Star Portal down over by the old aqueduct to the south of here, and the GIB have watched it carefully ever since."
	default20.playertext = "GIB? Who's that? And what's a Star Portal?"
	default20.result = 21
	DefaultDylan.addDialog( default20, Dylan )
	
	def default21 = [id:21]
	default21.npctext = "Man...you ask a lot of questions. The GIB are the government dudes that keep track of the aliens. Everyone always calls them the 'Gaians in Black' because of the suits they always wear. Anyway, there's always at least one stationed over there. If you wanna know more about that stuff, go ask the dude on duty."
	default21.playertext = "Okay, okay. Gotcha. Maybe I could ask you just a bit more before I go?"
	default21.result = 22
	DefaultDylan.addDialog( default21, Dylan )
	
	def default22 = [id:22]
	default22.npctext = "Yeah, yeah..."
	default22.result = 6
	DefaultDylan.addDialog( default22, Dylan )
	
	def default23 = [id:23]
	default23.npctext = "Man...it's a total bummer. The Beach used to be a great place to hang, but now that dang landshark patrols the sand trying to eat everything that moves and everything that *shouldn't* move is crawling around like it's really alive. I hate these Animated. They are RUINING my party vibe."
	default23.playertext = "Does anyone around here know more about what's going on?"
	default23.result = 24
	DefaultDylan.addDialog( default23, Dylan )
	
	def default24 = [id:24]
	default24.npctext = "Nah. The others here are younger than me and they just don't care much. Maybe that old crazy lighthouse keeper might know more, but you'd have to go ask him. His name is Jack, or Jacques, or something like that, I think."
	default24.playertext = "Okay, Dylan. Thanks. Maybe I'll check him out."
	default24.result = 25
	DefaultDylan.addDialog( default24, Dylan )
	
	def default25 = [id:25]
	default25.npctext = "If you do, don't blame me if he's too weird for you. He creeps most of us out. Even the roughest of us don't mess with the Keeper or his mutt."
	default25.playertext = "Right. Well thanks, Dylan. Maybe I could ask just a couple more questions?"
	default25.result = 6
	DefaultDylan.addDialog( default25, Dylan )
	
	def default26 = [id:26]
	default26.npctext = "Later."
	default26.playertext = "See ya."
	default26.exec = { event ->
		Dylan.pauseResume()
	}
	default26.result = DONE
	DefaultDylan.addDialog( default26, Dylan )
	
	def default27 = [id:27]
	default27.npctext = "Good. I was starting to wonder about you..."
	default27.result = 20
	DefaultDylan.addDialog( default27, Dylan )

}
