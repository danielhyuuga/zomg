import com.gaiaonline.mmo.battle.script.*;

//--------------------------------------
//NOTE:                                 
//    (DO NOT REMOVE THE printlns!!!)   
//    (They are too dang useful!!! :)   
//--------------------------------------

//------------------------------------------
// LANDSHARK SPAWNER                        
//------------------------------------------

def landSharkSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() < 7.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 7.0 or lower to attack LandShark." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

landSharkSpawner = myRooms.Beach_403.spawnSpawner( "landSharkSpawner", "landshark", 1 )
landSharkSpawner.setPos( 350, 290 )
landSharkSpawner.setWaitTime( 1, 2 )
landSharkSpawner.setChildrenToFollow( landSharkSpawner )
landSharkSpawner.setBaseSpeed( 130 )
landSharkSpawner.setHateRadiusForChildren( 2000 )
landSharkSpawner.setHomeTetherForChildren( 5000 )
landSharkSpawner.setMonsterLevelForChildren( 7.2 )
landSharkSpawner.setMiniEventSpec( landSharkSpec )
landSharkSpawner.addPatrolPointForSpawner( "Beach_402", 815, 105, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_302", 850, 270, 10 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_402", 815, 105, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_403", 200, 540, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_404", 180, 470, 10 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_503", 815, 200, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_503", 600, 600, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_603", 565, 435, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_703", 875, 300, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 845, 200, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 800, 430, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 585, 585, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 200, 550, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_903", 165, 135, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_903", 185, 370, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_903", 790, 555, 10 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_903", 185, 370, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_903", 165, 135, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 200, 550, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 585, 585, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 800, 430, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_803", 845, 200, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_703", 875, 300, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_603", 565, 435, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_503", 600, 600, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_503", 815, 200, 0 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_403", 900, 500, 10 )
landSharkSpawner.addPatrolPointForSpawner( "Beach_403", 200, 540, 0 )
landSharkSpawner.startPatrol()
landSharkSpawner.setForbiddenRoom( "Beach_102" )
landSharkSpawner.setForbiddenRoom( "Beach_103" )
landSharkSpawner.setForbiddenRoom( "Beach_104" )
landSharkSpawner.setForbiddenRoom( "Beach_502" )
landSharkSpawner.setForbiddenRoom( "Beach_602" )
landSharkSpawner.setForbiddenRoom( "Beach_702" )
landSharkSpawner.setForbiddenRoom( "Beach_802" )
landSharkSpawner.setForbiddenRoom( "Beach_902" )
landSharkSpawner.setForbiddenRoom( "Beach_1002" )
landSharkSpawner.setForbiddenRoom( "Beach_1003" )
landSharkSpawner.setForbiddenRoom( "Beach_1004" )
landSharkSpawner.setForbiddenRoom( "Beach_105" )
landSharkSpawner.setForbiddenRoom( "Beach_205" )
landSharkSpawner.setForbiddenRoom( "Beach_305" )
landSharkSpawner.setForbiddenRoom( "Beach_405" )
landSharkSpawner.setForbiddenRoom( "Beach_505" )
landSharkSpawner.setForbiddenRoom( "Beach_605" )
landSharkSpawner.setForbiddenRoom( "Beach_705" )
landSharkSpawner.setForbiddenRoom( "Beach_805" )
landSharkSpawner.setForbiddenRoom( "Beach_905" )
landSharkSpawner.setForbiddenRoom( "Beach_1005" )

hateCollector = [] as Set
bruceDisposed = false
recipeList = [ "17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800" ]
itemList = [ "17940", "17655", "17609", "17647", "17657", "17653", "17654", "18002", "17652" ]
ringList = ["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"]

//YE OLDE BASIC DEATHWATCH SCRIPT

def sharkDeathWatch() {
	myManager.schedule( 600 ) { checkForDisposal() } //If Bruce hasn't been killed in 10 minutes, then try to get rid of him.
	runOnDeath(Bruce, { event ->
		//if he was killed, then remember everyone that killed him and give them loot
		if( bruceDisposed == false ) {
			hateCollector << event.actor.getHated()
		//if he was disposed, then clear the HateCollector and no one gets loot
		} else {
			hateCollector.clear()
		}
		//he won't spawn again for 1-3 hours after getting killed, regardless of the state of the beach spawns
		myManager.schedule( random( 3600, 10800 ) ) { checkForBruceRespawn() }
	} ) 
}

def checkForDisposal() {
	if( !Bruce.isDead() ) {
		if( Bruce.getHated().isEmpty() ) {
			bruceDisposed = true
			Bruce.dispose()
		} else {
			myManager.schedule( 10 ) { checkForDisposal() }
		}
	}
}


def checkForBruceRespawn() {
	//count the beach critters
	//if below a certain threshold, then respawn Bruce
	//if not, then keep checking periodically until Bruce respawns
	
	//COUNT ALL THE BEACH SPAWNS
	counter901 = myRooms.Beach_901.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_901.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_901.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter901 = ${counter901}******"
	counter1001 = myRooms.Beach_1001.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_1001.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_1001.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter1001 = ${counter1001}******"
	
	counter602 = myRooms.Beach_602.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_602.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_602.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter602 = ${counter602}******"
	counter802 = myRooms.Beach_802.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_802.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_802.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter802 = ${counter802}******"
	counter902 = myRooms.Beach_902.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_902.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_902.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter902 = ${counter902}******"
	counter1002 = myRooms.Beach_1002.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_1002.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_1002.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter1002 = ${counter1002}******"
	
	counter103 = myRooms.Beach_103.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_103.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_103.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter103 = ${counter103}******"
	counter203 = myRooms.Beach_203.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_203.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_203.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter203 = ${counter203}******"
	counter303 = myRooms.Beach_303.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_303.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_303.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter303 = ${counter303}******"
	counter403 = myRooms.Beach_403.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_403.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_403.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter403 = ${counter403}******"
	counter503 = myRooms.Beach_503.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_503.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_503.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter503 = ${counter503}******"
	counter603 = myRooms.Beach_603.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_603.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_603.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter603 = ${counter603}******"
	counter703 = myRooms.Beach_703.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_703.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_703.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter703 = ${counter703}******"
	counter803 = myRooms.Beach_803.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_803.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_803.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter803 = ${counter803}******"
	counter903 = myRooms.Beach_903.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_903.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_903.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter903 = ${counter903}******"
	counter1003 = myRooms.Beach_1003.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_1003.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_1003.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter1003 = ${counter1003}******"

	counter204 = myRooms.Beach_204.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_204.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_204.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter204 = ${counter204}******"
	counter304 = myRooms.Beach_304.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_304.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_304.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter304 = ${counter304}******"
	counter404 = myRooms.Beach_404.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_404.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_404.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter404 = ${counter404}******"
	counter504 = myRooms.Beach_504.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_504.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_504.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter504 = ${counter504}******"
	counter604 = myRooms.Beach_604.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_604.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_604.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter604 = ${counter604}******"
	counter704 = myRooms.Beach_704.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_704.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_704.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter704 = ${counter704}******"
	counter804 = myRooms.Beach_804.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_804.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_804.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter804 = ${counter804}******"
	counter904 = myRooms.Beach_904.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_904.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_904.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter904 = ${counter904}******"
	counter1004 = myRooms.Beach_1004.getSpawnTypeCount( "anchor_bug" ) + myRooms.Beach_1004.getSpawnTypeCount( "sand_golem" ) + myRooms.Beach_1004.getSpawnTypeCount( "sand_fluff" )
	//println "****** counter1004 = ${counter1004}******"

	beachCounter = counter901 + counter1001 + counter602 + counter802 + counter902 + counter1002 + counter103 + counter203 + counter303 + counter403 + counter503 + counter603 + counter703 + counter803 + counter903 + counter1001 + counter204 + counter304 + counter404 + counter504 + counter604 + counter704 + counter804 + counter904 + counter1004
	//println "###### TOTAL BEACH COUNTER = ${beachCounter} ######"
	
	// NOW CHECK TO SEE IF BRUCE SHOULD RESPAWN
	if ( beachCounter < 50 ) { // This is the controlling variable. Tweak this downward to suppress Bruce spawns, based on the number of critters in the zone.
		hateCollector = []
		Bruce = landSharkSpawner.forceSpawnNow()
		bruceDisposed = false
		//println "******* LANDSHARK IS ALIVE AGAIN! *******"
		sharkDeathWatch()
		omnomnomnom()
	} else {
		//println "******* NO LANDSHARK RESPAWN...NOT ENOUGH BEACH DEATH *******"
		myManager.schedule(180) { checkForBruceRespawn() }
	}
}

def omnomnomnom() {
	if( !Bruce.isDead() ) {
		Bruce.say( "OMNOMNOMNOM!" )
		myManager.schedule( random( 10, 20 ) ) { omnomnomnom() }
	}
}


// INITIAL LOGIC

landSharkSpawner.stopSpawning()

myManager.schedule(900) { checkForBruceRespawn() }