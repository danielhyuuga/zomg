//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------------------------------------------------------------
//Blip definitions                                                                                
//------------------------------------------------------------------------------------------------

//Fast blip when metal is found
def fastBlip( event ) {
	//println "ran fastBlip"
	event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
	event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
	event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	sound("detect_fast").toPlayer( event.actor )
	myManager.schedule(0.5) { sound("detect_fast").toPlayer( event.actor ) }
	myManager.schedule(1) { sound("detect_fast").toPlayer( event.actor ) }
	myManager.schedule(1.5) { sound("detect_fast").toPlayer( event.actor ) }
	myManager.schedule(2) { sound("detect_fast").toPlayer( event.actor ) }
	myManager.schedule(2.5) { sound("detect_fast").toPlayer( event.actor ) }
	myManager.schedule(3.5) { 
		sound("detect_fast").toPlayer( event.actor ) 
		if( event.actor.getPlayerVar( "metalCount" ) >= 5 ) {
			event.actor.updateQuest(258, "BFG-Marcy")
		}
	}
}

//Medium speed blip when near metal
def midBlip( event ) {
	if(event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip")) { 
		//event.actor.centerPrint( "BLIP" )
		sound("detect_medium").toPlayer( event.actor )
		myManager.schedule(1.5) { midBlip( event ) } 
		//println "ran midBlip"
	}
}

//Slow speed blip when in same room as metal
def slowBlip( event ) {
	if(event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip")) { 
		//event.actor.centerPrint( "BLIP" )
		sound("detect_slow").toPlayer( event.actor )
		myManager.schedule(3) { slowBlip( event ) } 
		//println "ran slowBlip"
	}
}

//Very slow speed blip when in room next to metal
def verySlowBlip( event ) {
	if(event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip")) { 
		//event.actor.centerPrint( "BLIP" )
		sound("detect_slow").toPlayer( event.actor )
		myManager.schedule(5) { verySlowBlip( event ) } 
		//println "ran verySlowBlip"
	}
}

//------------------------------------------------------------------------------------------------
//103 enter/exits                                                                                 
//------------------------------------------------------------------------------------------------

def metalInner103 = "metalInner103"
myRooms.Beach_103.createTriggerZone(metalInner103, 190, 190, 290, 290)
myManager.onTriggerIn(myRooms.Beach_103, metalInner103) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector103")
		fastBlip( event )
		myManager.schedule(3) { 
			event.actor.centerPrint( "After a few moments of digging, you find something!" )
			event.actor.addToPlayerVar( "metalCount", 1 )
			metalCountNotifications( event )
			event.actor.grantCoins( 20 )
		}
	}
}

//Top trigger zone & enter/exit for 103
def metalTop103 = "metalTop103"
myRooms.Beach_103.createTriggerZone(metalTop103, 190, 90, 290, 190)

myManager.onTriggerIn(myRooms.Beach_103, metalTop103) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_103, metalTop103) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Bottom trigger zone & enter/exit for 103
def metalBottom103 = "metalBottom103"
myRooms.Beach_103.createTriggerZone(metalBottom103, 190, 290, 290, 390)

myManager.onTriggerIn(myRooms.Beach_103, metalBottom103) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_103, metalBottom103) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Left side trigger zone & enter/exit for 103
def metalLeft103 = "metalLeft103"
myRooms.Beach_103.createTriggerZone(metalLeft103, 90, 90, 190, 390)

myManager.onTriggerIn(myRooms.Beach_103, metalLeft103) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_103, metalLeft103) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Right side trigger zone & enter/exit for 103
def metalRight103 = "metalRight103"
myRooms.Beach_103.createTriggerZone(metalRight103, 290, 90, 390, 390)

myManager.onTriggerIn(myRooms.Beach_103, metalRight103) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_103, metalRight103) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//------------------------------------------------------------------------------------------------
//304 enter/exits                                                                                 
//------------------------------------------------------------------------------------------------

//Trigger zone and enter/exit for 304
def metalInner304 = "metalInner304"
myRooms.Beach_304.createTriggerZone(metalInner304, 300, 380, 400, 480)
myManager.onTriggerIn(myRooms.Beach_304, metalInner304) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector304")
		fastBlip( event )
		myManager.schedule(3) { 
			event.actor.centerPrint( "After a few moments of digging, you find something!" ) 
			event.actor.addToPlayerVar( "metalCount", 1 )
			metalCountNotifications( event )
			event.actor.grantCoins( 20 )
		}
	}
}

//Top trigger zone & enter/exit for 304
def metalTop304 = "metalTop304"
myRooms.Beach_304.createTriggerZone(metalTop304, 200, 280, 300, 380)

myManager.onTriggerIn(myRooms.Beach_304, metalTop304) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_304, metalTop304) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Bottom trigger zone & enter/exit for 304
def metalBottom304 = "metalBottom304"
myRooms.Beach_304.createTriggerZone(metalBottom304, 300, 480, 400, 580)

myManager.onTriggerIn(myRooms.Beach_304, metalBottom304) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_304, metalBottom304) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Left side trigger zone & enter/exit for 304
def metalLeft304 = "metalLeft304"
myRooms.Beach_304.createTriggerZone(metalLeft304, 200, 280, 300, 580)

myManager.onTriggerIn(myRooms.Beach_304, metalLeft304) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_304, metalLeft304) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Right side trigger zone & enter/exit for 304
def metalRight304 = "metalRight304"
myRooms.Beach_304.createTriggerZone(metalRight304, 400, 280, 500, 580)

myManager.onTriggerIn(myRooms.Beach_304, metalRight304) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_304, metalRight304) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//------------------------------------------------------------------------------------------------
//502 enter/exits                                                                                 
//------------------------------------------------------------------------------------------------

//Trigger zone and enter/exit for 502
def metalInner502 = "metalInner502"
myRooms.Beach_502.createTriggerZone(metalInner502, 840, 200, 940, 300)
myManager.onTriggerIn(myRooms.Beach_502, metalInner502) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) {
		//println "^^^^ ${event.actor} entered inner trigger ^^^^"
		event.actor.unsetGlobalFlag("Z9_MetalDetector502")
		fastBlip( event )
		myManager.schedule(3) { 
			event.actor.centerPrint( "After a few moments of digging, you find something!" ) 
			event.actor.addToPlayerVar( "metalCount", 1 )
			metalCountNotifications( event )
			event.actor.grantCoins( 20 )
		}
	}
}

//Top trigger zone & enter/exit for 502
def metalTop502 = "metalTop502"
myRooms.Beach_502.createTriggerZone(metalTop502, 840, 100, 940, 200)

myManager.onTriggerIn(myRooms.Beach_502, metalTop502) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) {
		//println "^^^^ ${event.actor} entered top trigger ^^^^"
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_502, metalTop502) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		//println "^^^^ ${event.actor} exited top trigger ^^^^"
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Bottom trigger zone & enter/exit for 502
def metalBottom502 = "metalBottom502"
myRooms.Beach_502.createTriggerZone(metalBottom502, 840, 300, 940, 400)

myManager.onTriggerIn(myRooms.Beach_502, metalBottom502) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) {
		//println "^^^^ ${event.actor} entered bottom trigger"
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_502, metalBottom502) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		//println "^^^^ ${event.actor} exited bottom trigger ^^^^"
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Left side trigger zone & enter/exit for 502
def metalLeft502 = "metalLeft502"
myRooms.Beach_502.createTriggerZone(metalLeft502, 740, 100, 840, 400)

myManager.onTriggerIn(myRooms.Beach_502, metalLeft502) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) {
		//println "^^^^ ${event.actor} entered left trigger ^^^^"
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_502, metalLeft502) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		//println "^^^^ ${event.actor} exited left trigger ^^^^"
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Right side trigger zone & enter/exit for 502
def metalRight502 = "metalRight502"
myRooms.Beach_502.createTriggerZone(metalRight502, 940, 100, 1030, 400)

myManager.onTriggerIn(myRooms.Beach_502, metalRight502) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) {
		//println "^^^^ ${event.actor} entered right trigger ^^^^"
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_502, metalRight502) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		//println "^^^^ ${event.actor} exited right trigger ^^^^"
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//------------------------------------------------------------------------------------------------
//703 enter/exits                                                                                 
//------------------------------------------------------------------------------------------------

//Trigger zone and enter/exit for 703
def metalInner703 = "metalInner703"
myRooms.Beach_703.createTriggerZone(metalInner703, 630, 200, 730, 300)
myManager.onTriggerIn(myRooms.Beach_703, metalInner703) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector703")
		fastBlip( event )
		myManager.schedule(3) { 
			event.actor.centerPrint( "After a few moments of digging, you find something!" ) 
			event.actor.addToPlayerVar( "metalCount", 1 )
			metalCountNotifications( event )
			event.actor.grantCoins( 20 )
		}
	}
}

//Top trigger zone & enter/exit for 703
def metalTop703 = "metalTop703"
myRooms.Beach_703.createTriggerZone(metalTop703, 630, 100, 730, 200)

myManager.onTriggerIn(myRooms.Beach_703, metalTop703) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_703, metalTop703) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Bottom trigger zone & enter/exit for 703
def metalBottom703 = "metalBottom703"
myRooms.Beach_703.createTriggerZone(metalBottom703, 630, 300, 730, 400)

myManager.onTriggerIn(myRooms.Beach_703, metalBottom703) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_703, metalBottom703) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Left side trigger zone & enter/exit for 703
def metalLeft703 = "metalLeft703"
myRooms.Beach_703.createTriggerZone(metalLeft703, 530, 100, 630, 400)

myManager.onTriggerIn(myRooms.Beach_703, metalLeft703) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_703, metalLeft703) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Right side trigger zone & enter/exit for 703
def metalRight703 = "metalRight703"
myRooms.Beach_703.createTriggerZone(metalRight703, 730, 100, 830, 400)

myManager.onTriggerIn(myRooms.Beach_703, metalRight703) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_703, metalRight703) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//------------------------------------------------------------------------------------------------
//1003 enter/exits                                                                                
//------------------------------------------------------------------------------------------------

//Trigger zone and enter/exit for 1003
def metalInner1003 = "metalInner1003"
myRooms.Beach_1003.createTriggerZone(metalInner1003, 580, 160, 680, 260)
myManager.onTriggerIn(myRooms.Beach_1003, metalInner1003) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector1003")
		fastBlip( event )
		myManager.schedule(3) { 
			event.actor.centerPrint( "After a few moments of digging, you find something!" ) 
			event.actor.addToPlayerVar( "metalCount", 1 )
			metalCountNotifications( event )
			event.actor.grantCoins( 20 )
		}
	}
}

//Top trigger zone & enter/exit for 1003
def metalTop1003 = "metalTop1003"
myRooms.Beach_1003.createTriggerZone(metalTop1003, 580, 60, 680, 160)

myManager.onTriggerIn(myRooms.Beach_1003, metalTop1003) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_1003, metalTop1003) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Bottom trigger zone & enter/exit for 1003
def metalBottom1003 = "metalBottom1003"
myRooms.Beach_1003.createTriggerZone(metalBottom1003, 580, 260, 680, 360)

myManager.onTriggerIn(myRooms.Beach_1003, metalBottom1003) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_1003, metalBottom1003) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Left side trigger zone & enter/exit for 1003
def metalLeft1003 = "metalLeft1003"
myRooms.Beach_1003.createTriggerZone(metalLeft1003, 480, 60, 580, 360)

myManager.onTriggerIn(myRooms.Beach_1003, metalLeft1003) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_1003, metalLeft1003) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//Right side trigger zone & enter/exit for 1003
def metalRight1003 = "metalRight1003"
myRooms.Beach_1003.createTriggerZone(metalRight1003, 680, 60, 780, 360)

myManager.onTriggerIn(myRooms.Beach_1003, metalRight1003) { event ->
	if( isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) {
		event.actor.setGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		midBlip( event )
	}
}

myManager.onTriggerOut(myRooms.Beach_1003, metalRight1003) { event ->
	if( isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_MidBlip") ) {
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

//************************************************************************************************
//------------------------------------------------------------------------------------------------
//Room Enter/Exits                                                                                
//------------------------------------------------------------------------------------------------
//************************************************************************************************

//------------------------------------------------------------------------------------------------
//Room enter/exit for 102                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_102 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) { //If player has metal in 103
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_102 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 103
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 103                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_103 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) { //If player has metal in 103
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_103 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") ) { //If player has metal in 103
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 104                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_104 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) { //If player has metal in 103
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_104 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 103
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 203                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_203 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector103") ) { //If player has metal in 103
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_203 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 103
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 204                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_204 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) { //If player has metal in 304
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_204 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 304
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 303                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_303 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) { //If player has metal in 304
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_303 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 304
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 304                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_304 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) { //If player has metal in 304
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_304 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") ) { //If player has metal in 304
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 305                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_305 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) { //If player has metal in 304
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_305 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 304
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 404                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_404 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector304") ) { //If player has metal in 304
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_404 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 304
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 402                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_402 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
	}

	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) { //If player has metal in 502
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_402 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 502
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 502                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_502 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) { //If player has metal in 502
		//println "^^^^ ${event.actor} entered Beach_502 ^^^^"
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_502 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") ) { //If player has metal in 502
		//println "^^^^ ${event.actor} exited Beach_502 ^^^^"
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 503                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_503 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) { //If player has metal in 502
		//println "^^^^ ${event.actor} entered Beach_503 ^^^^"
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_503 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 502
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		//println "^^^^ ${event.actor} exited Beach_503 ^^^^"
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 602                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_602 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector502") ) { //If player has metal in 502
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_602 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 502
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 603                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_603 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) { //If player has metal in 703
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_603 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 703
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 702                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_702 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) { //If player has metal in 703
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_702 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 703
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 703                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_703 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) { //If player has metal in 703
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_703 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") ) { //If player has metal in 703
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 704                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_704 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) { //If player has metal in 703
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_704 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 703
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 803                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_803 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector703") ) { //If player has metal in 703
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_803 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 703
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 903                                                                         
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_903 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) { //If player has metal in 1003
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_903 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 1003
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 1002                                                                        
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_1002 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) { //If player has metal in 1003
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_1002 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 1003
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 1003                                                                        
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_1003 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) { //If player has metal in 1003
		event.actor.setGlobalFlag("Z9_MetalDetector_SlowBlip")
		slowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_1003 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_SlowBlip") ) { //If player has metal in 1003
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//Room enter/exit for 1004                                                                        
//------------------------------------------------------------------------------------------------
myManager.onEnter( myRooms.Beach_1004 ) { event ->
	if(isPlayer(event.actor) && !event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector1003") ) { //If player has metal in 1003
		event.actor.setGlobalFlag("Z9_MetalDetector_VerySlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_SlowBlip")
		event.actor.unsetGlobalFlag("Z9_MetalDetector_MidBlip")
		verySlowBlip( event )
	}
}

myManager.onExit( myRooms.Beach_1004 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z9_MetalDetector_VerySlowBlip") ) { //If player has metal in 1003
		event.actor.unsetGlobalFlag("Z9_MetalDetector_VerySlowBlip")
	}
}

//------------------------------------------------------------------------------------------------
//NOTIFICATION SCRIPT (centerPrints that tell players how many objects they've found)             
//------------------------------------------------------------------------------------------------

def metalCountNotifications( event ) {
	myManager.schedule(2) {
		metalCountValue = event.actor.getPlayerVar( "metalCount" )
		event.actor.centerPrint( "You've now found ${ metalCountValue.intValue() } out of 5 pieces of metal." )
	}
}
		
