//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

Ceejay = spawnNPC( "Ceejay-VQS", myRooms.Beach_1005, 520, 480 )
Ceejay.setDisplayName( "Ceejay" )

onQuestStep( 101, 2 ) { event -> event.player.addMiniMapQuestActorName( "Ceejay-VQS" ) }
onQuestStep( 275, 2 ) { event -> event.player.addMiniMapQuestActorName( "Ceejay-VQS" ) }

//------------------------------------------------------------
//Ceejay Default                                              
//------------------------------------------------------------
def ceejayDefault = Ceejay.createConversation("ceejayDefault", true, "!QuestStarted_101", "!Z9_Ceejay_Golem_Break", "!QuestCompleted_101")

def default1 = [id:1]
default1.npctext = "OMG Hi! I'm Ceejay, I love the beach!!"
default1.options = []
default1.options << [text:"Hi Ceejay. I like the beach too.", result: 2]
default1.options << [text:"Wow, you're energetic.", result: 3]
default1.options << [text:"Meh... it's a little too sandy for my taste.", result: 4]
ceejayDefault.addDialog(default1, Ceejay)

def default2 = [id:2]
default2.npctext = "It's great, isn't it. But those sand thingies need to go."
default2.playertext = "Sand thingies?"
default2.result = 5
ceejayDefault.addDialog(default2, Ceejay)

def default3 = [id:3]
default3.npctext = "Hehe, thanks for noticing. It's hard work! It's getting harder and harder to stay upbeat with those sand thingies harshin' about, though."
default3.playertext = "Uhm... what sand thingies?"
default3.result = 5
ceejayDefault.addDialog(default3, Ceejay)

def default4 = [id:4]
default4.npctext = "Hmm... yeah I'd like it even more without those sand thingies roaming around too."
default4.playertext = "Sand thingies roaming around? Huh?"
default4.result = 6
ceejayDefault.addDialog(default4, Ceejay)

def default5 = [id:5]
default5.npctext = "There're like... giant living sand castles and they're mean! The other day I was out on the beach and one almost came up through the sand right underneath me! I wish someone would do something about them."
default5.options = []
default5.options << [text:"I could give that a try.", result: 7]
default5.options << [text:"Well, good luck with that. I'm just here to relax.", result: 8]
ceejayDefault.addDialog(default5, Ceejay)

def default6 = [id:6]
default6.npctext = "Oh! I thought that's what you were talking about. Anyway... they're kind of like giant living sand castles and they jump out of the sand right underneath you! I wish someone would do something about them."
default6.options = []
default6.options << [text:"Maybe I could do it?", result: 7]
default6.options << [text:"I hope someone else takes care of them too.", result: 8]
ceejayDefault.addDialog(default6, Ceejay)

def default7 = [id:7]
default7.npctext = "Could you? Please please please! They're scattered all over the beach, but mostly to the north."
default7.options = []
default7.options << [text:"I'll get started immediately.", result: 9]
default7.options << [text:"Let me think about it. I'll get back to you.", result: 8]
ceejayDefault.addDialog(default7, Ceejay)

def default8 = [id:8]
default8.npctext = "Oh, well let me know if you decide to help."
default8.flag = "Z9_Ceejay_Golem_Break"
default8.result = DONE
ceejayDefault.addDialog(default8, Ceejay)

def default9 = [id:9]
default9.npctext = "Great! Go getem!"
default9.quest = 101
default9.result = DONE
ceejayDefault.addDialog(default9, Ceejay)

//------------------------------------------------------------
//Golem Break                                                 
//------------------------------------------------------------
def golemBreak = Ceejay.createConversation("golemBreak", true, "Z9_Ceejay_Golem_Break", "!QuestCompleted_101", "!QuestStarted_101")

def golemBreak1 = [id:1]
golemBreak1.npctext = "Hi again. Did you decide to help out with the golems?"
golemBreak1.options = []
golemBreak1.options << [text:"Yeah, I think I want to give it a shot.", result: 2]
golemBreak1.options << [text:"Er, no... just saying hi.", result: 3]
golemBreak.addDialog(golemBreak1, Ceejay)

def golemBreak2 = [id:2]
golemBreak2.npctext = "Yay!! They're scattered all over the beach, but mostly to the north. Go kick some butt!"
golemBreak2.playertext = "I plan to."
golemBreak2.quest = 101
golemBreak2.flag = "!Z9_Ceejay_Golem_Break"
golemBreak2.result = DONE
golemBreak.addDialog(golemBreak2, Ceejay)

def golemBreak3 = [id:3]
golemBreak3.npctext = "Hi. Why won't you help?"
golemBreak3.options = []
golemBreak3.options << [text:"I'm just busy.", result: 4]
golemBreak3.options << [text:"I'm scared! Those sand castle things sound mean.", result: 5]
golemBreak3.options << [text:"That just sounds really boring. I'd rather do something fun.", result: 4]
golemBreak3.options << [text:"I'm just lazy. I'd rather hang out on the beach!", result: 4]
golemBreak.addDialog(golemBreak3, Ceejay)

def golemBreak4 = [id:4]
golemBreak4.npctext = "Oh..."
golemBreak4.result = DONE
golemBreak.addDialog(golemBreak4, Ceejay)

def golemBreak5 = [id:5]
golemBreak5.npctext = "A big strong Gaian like you? I'm sure you could handle them. Pretty pleeeeeeeeeease!"
golemBreak5.options = []
golemBreak5.options << [text:"Alright, alright, just stop begging!", result: 2]
golemBreak5.options << [text:"Bah, I don't wanna!", result: 4]
golemBreak.addDialog(golemBreak5, Ceejay)

//------------------------------------------------------------
//Golem Active                                                
//------------------------------------------------------------
def golemActive = Ceejay.createConversation("golemActive", true, "QuestStarted_101:2")

def golemActive1 = [id:1]
golemActive1.npctext = "Oooooh, go smash those golems!"
golemActive1.result = DONE
golemActive.addDialog(golemActive1, Ceejay)

//------------------------------------------------------------
//Golem Complete                                              
//------------------------------------------------------------
def golemComplete = Ceejay.createConversation("golemComplete", true, "QuestStarted_101:3")

def golemComplete1 = [id:1]
golemComplete1.npctext = "Yaaaaaay! My herooooo!"
golemComplete1.options = []
golemComplete1.options << [text:"Relax. It was nothing.", result: 2]
golemComplete1.options << [text:"Your effusiveness is apalling.", result: 3]
golemComplete1.options << [text:"I'm like totally amazing. OMG!", result: 4]
golemComplete.addDialog(golemComplete1, Ceejay)

def golemComplete2 = [id:2]
golemComplete2.npctext = "Not to me it wasn't. Thanks! *huggles*"
golemComplete2.exec = { event -> event.player.removeMiniMapQuestActorName("Ceejay-VQS") }
golemComplete2.quest = 101
golemComplete2.result = DONE
golemComplete.addDialog(golemComplete2, Ceejay)

def golemComplete3 = [id:3]
golemComplete3.npctext = "Hmm... I'm not sure what that means but it sounds like fun! Thanks!"
golemComplete3.exec = { event -> event.player.removeMiniMapQuestActorName("Ceejay-VQS") }
golemComplete3.quest = 101
golemComplete3.result = DONE
golemComplete.addDialog(golemComplete3, Ceejay)

def golemComplete4 = [id:4]
golemComplete4.npctext = "Hehehe! Yes you are!!"
golemComplete4.exec = { event -> event.player.removeMiniMapQuestActorName("Ceejay-VQS") }
golemComplete4.quest = 101
golemComplete4.result = DONE
golemComplete.addDialog(golemComplete4, Ceejay)

//------------------------------------------------------------
//Golem Repeat Give                                           
//------------------------------------------------------------
def golemRepeatGive = Ceejay.createConversation("golemRepeatGive", true, "QuestCompleted_101", "!QuestStarted_275", "!Z9_Ceejay_GolemRepeat_Allowed", "!Z9_Ceejay_GolemRepeat_AllowedAgain", "!Z9_Ceejay_GolemRepeatDenied")
golemRepeatGive.setUrgent(true)

def golemRepeatGive1 = [id:1]
golemRepeatGive1.npctext = "Oh thank goodness, %p is back! You won't believe it, those giant castle monsters are back."
golemRepeatGive1.playertext = "What?! It feels like I just finished clearing them out. How can they be back already?"
golemRepeatGive1.result = 2
golemRepeatGive.addDialog(golemRepeatGive1, Ceejay)

def golemRepeatGive2 = [id:2]
golemRepeatGive2.npctext = "Maybe there were just more of them than we thought? I'll bet if you took out a few more we'd be rid of them."
golemRepeatGive2.options = []
golemRepeatGive2.options << [text:"Oh, alright. I'll try taking out a few more.", exec: { event ->
	if(event.player.getConLevel() < 7.1) {
		event.player.setQuestFlag(GLOBAL, "Z9_Ceejay_GolemRepeat_Allowed")
		Ceejay.pushDialog(event.player, "golemRepeatAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z9_Ceejay_GolemRepeat_Denied")
		Ceejay.pushDialog(event.player, "golemRepeatDenied")
	}	
}, result: DONE]
golemRepeatGive2.options << [text:"Not right now, Ceejay. I'm still tired from the last bunch.", result: 4]
golemRepeatGive.addDialog(golemRepeatGive2, Ceejay)

def golemRepeatGive4 = [id:4]
golemRepeatGive4.npctext = "Dang! Maybe after you catch your breath?"
golemRepeatGive4.result = DONE
golemRepeatGive.addDialog(golemRepeatGive4, Ceejay)

//------------------------------------------------------------
//Golem Repeat Active                                         
//------------------------------------------------------------
def golemRepeatActive = Ceejay.createConversation("golemRepeatActive", true, "QuestStarted_275:2")

def golemRepeatActive1 = [id:1]
golemRepeatActive1.npctext = "Those castle-monster-thingies are in for it with %p on the job!"
golemRepeatActive1.result = DONE
golemRepeatActive.addDialog(golemRepeatActive1, Ceejay)

//------------------------------------------------------------
//Golem Repeat Complete                                       
//------------------------------------------------------------
def golemRepeatComplete = Ceejay.createConversation("golemRepeatComplete", true, "QuestStarted_275:3")

def golemRepeatComplete1 = [id:1]
golemRepeatComplete1.npctext = "You did it! You gotem!"
golemRepeatComplete1.playertext = "Yes, but not all of them."
golemRepeatComplete1.result = 2
golemRepeatComplete.addDialog(golemRepeatComplete1, Ceejay)

def golemRepeatComplete2 = [id:2]
golemRepeatComplete2.npctext = "You mean... you mean there's more of those brutish things? What can we do?! They just keep coming!"
golemRepeatComplete2.options = []
golemRepeatComplete2.options << [text:"Their numbers can't be infinite. I'll just keep beating them off until there are none left.", exec: { event ->
	event.player.removeMiniMapQuestActorName("Ceejay-VQS")
	event.player.updateQuest(275, "Ceejay-VQS")
	if(event.player.getConLevel() < 7.1) {
		event.player.setQuestFlag(GLOBAL, "Z9_Ceejay_GolemRepeat_AllowedAgain")
		Ceejay.pushDialog(event.player, "golemRepeatAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z9_Ceejay_GolemRepeat_Denied")
		Ceejay.pushDialog(event.player, "golemRepeatDenied")
	}
}, result: DONE]
golemRepeatComplete2.options << [text:"Right now, I need a break from these golems.", result: 4]
golemRepeatComplete.addDialog(golemRepeatComplete2, Ceejay)

def golemRepeatComplete4 = [id:4]
golemRepeatComplete4.npctext = "Okay... but pleeeeeeeease come back when you're rested!"
golemRepeatComplete4.exec = { event -> event.player.removeMiniMapQuestActorName("Ceejay-VQS") }
golemRepeatComplete4.quest = 275
golemRepeatComplete4.result = DONE
golemRepeatComplete.addDialog(golemRepeatComplete4, Ceejay)

//------------------------------------------------------------
//Golem Repeat Allowed Again                                  
//------------------------------------------------------------
def golemRepeatAllowedAgain = Ceejay.createConversation("golemRepeatAllowedAgain", true, "Z9_Ceejay_GolemRepeat_AllowedAgain")

def golemRepeatAllowedAgain1 = [id:1]
golemRepeatAllowedAgain1.npctext = "You're such a trooper!"
golemRepeatAllowedAgain1.quest = 275
golemRepeatAllowedAgain1.flag = "!Z9_Ceejay_GolemRepeat_AllowedAgain"
golemRepeatAllowedAgain1.result = DONE
golemRepeatAllowedAgain.addDialog(golemRepeatAllowedAgain1, Ceejay)

//------------------------------------------------------------
//Golem Repeat Allowed                                        
//------------------------------------------------------------
def golemRepeatAllowed = Ceejay.createConversation("golemRepeatAllowed", true, "Z9_Ceejay_GolemRepeat_Allowed")

def golemRepeatAllowed1 = [id:1]
golemRepeatAllowed1.npctext = "I knew you wouldn't let me down, %p!"
golemRepeatAllowed1.quest = 275
golemRepeatAllowed1.flag = "!Z9_Ceejay_GolemRepeat_Allowed"
golemRepeatAllowed1.result = DONE
golemRepeatAllowed.addDialog(golemRepeatAllowed1, Ceejay)

//------------------------------------------------------------
//Golem Repeat Denied                                           
//------------------------------------------------------------
def golemRepeatDenied = Ceejay.createConversation("golemRepeatDenied", true, "Z9_Ceejay_GolemRepeat_Denied")

def golemRepeatDenied1 = [id:1]
golemRepeatDenied1.npctext = "OMG, you're silly! This is way too easy for you!"
golemRepeatDenied1.flag = "!Z9_Ceejay_GolemRepeat_Denied"
golemRepeatDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 7.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
golemRepeatDenied1.result = DONE
golemRepeatDenied.addDialog(golemRepeatDenied1, Ceejay)