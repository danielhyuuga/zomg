def rockOnUpdater = "rockOnUpdater"
myRooms.Beach_103.createTriggerZone(rockOnUpdater, 685, 320, 865, 510)
myManager.onTriggerIn(myRooms.Beach_103, rockOnUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(228)) {
		event.actor.updateQuest(228, "Story-VQS")
	}
}