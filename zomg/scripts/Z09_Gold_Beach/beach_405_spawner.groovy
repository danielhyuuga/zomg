//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner_405_1 = myRooms.Beach_405.spawnSpawner( "spawner_405_1", "water_spout", 1)
spawner_405_1.setPos( 230, 130 )
spawner_405_1.setHome( "Beach_405", 230, 130 )
spawner_405_1.setSpawnWhenPlayersAreInRoom( true )
spawner_405_1.setWaitTime( 30 , 40 )
spawner_405_1.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_405_1.childrenWander( true )
spawner_405_1.setMonsterLevelForChildren( 6.6 )

// Spawner two
spawner_405_2 = myRooms.Beach_405.spawnSpawner( "spawner_405_2", "water_spout", 1)
spawner_405_2.setPos( 300, 530 )
spawner_405_2.setHome( "Beach_405", 300, 530 )
spawner_405_2.setSpawnWhenPlayersAreInRoom( true )
spawner_405_2.setWaitTime( 30 , 40 )
spawner_405_2.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_405_2.childrenWander( true )
spawner_405_2.setMonsterLevelForChildren( 6.6 )

spawner_405_1.stopSpawning()
spawner_405_2.stopSpawning()

//Alliances
spawner_405_1.allyWithSpawner( spawner_405_2 )

//Variable definition
spawnTime = false
spawnedSpout1 = false
spawnedSpout2 = false
spoutRespawning = false
spout_405_1 = null
spout_405_2 = null

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() } // ; //println "***** GST = ${ gst() } ******" }
}

def spawnChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) { //party goes from 7pm to 2am.
//	if( gst() > 0 ) { //This line for each setting of time parameters for testing.
		spawnTime = true
	} else {
		spawnTime = false
	}
}	

def checkForDespawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == false ) {
		clockChecker()
		if( spawnedSpout1 == true ) {
			if(spout_405_1.getHated().size() == 0) {
				spout_405_1.dispose()
				spawnedSpout1 = false
			}
		}
		if( spawnedSpout2 == true ) {
			if(spout_405_2.getHated().size() == 0) {
				spout_405_2.dispose()
				spawnedSpout2 = false
			}
		}
	}
	myManager.schedule(30) { checkForDespawn() }
}

def spawnOrNoSpawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == true ) { 
		spoutRespawning = false
		if( spawnedSpout1 == false ) {
			spout_405_1 = spawner_405_1.forceSpawnNow()
			spawnedSpout1 = true
			runOnDeath( spout_405_1 ) { 
				spawnedSpout1 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout2 == false ) {
			spout_405_2 = spawner_405_2.forceSpawnNow()
			spawnedSpout2 = true
			runOnDeath( spout_405_2 ) { 
				spawnedSpout2 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
	}
}

//Initialization
clockChecker()
checkForDespawn()