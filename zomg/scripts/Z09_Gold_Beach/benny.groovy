//Script creabenny by gfern

import com.gaiaonline.mmo.battle.script.*;

Benny = spawnNPC( "Benny-VQS", myRooms.Beach_1005, 825, 265 )
Benny.setDisplayName( "Benny" )

onQuestStep(96, 2) { event -> event.player.addMiniMapQuestActorName("Benny-VQS") }
onQuestStep(85, 2) { event -> event.player.addMiniMapQuestActorName("Benny-VQS") }

//------------------------------------------------------------
//Benny Default                                               
//------------------------------------------------------------
def bennyDefault = Benny.createConversation("bennyDefault", true, "!QuestStarted_96", "!Z9_Benny_SandFiber_Break", "!QuestCompleted_96")

bennyDefault.setUrgent(true)

def default1 = [id:1]
default1.npctext = "Duuuuuuude."
default1.options = []
default1.options << [text:"That's all you have to say? Dude?", result: 2]
default1.options << [text:"SWEEEEEEET!", result: 3]
default1.options << [text:"Were you just playing air guitar?", result: 4]
default1.options << [text:"What's going on?", result: 5]
bennyDefault.addDialog(default1, Benny)

def default2 = [id:2]
default2.npctext = "I'm just saying hi, what gives?"
default2.playertext = "Alright, fair enough. What's going on Benny?"
default2.result = 5
bennyDefault.addDialog(default2, Benny)

def default3 = [id:3]
default3.npctext = "Raaaaaaaad"
default3.options = []
default3.options << [text:"KILLER!", result: 6]
default3.options << [text:"Okay. That's enough of that. What's going on?", result: 5]
bennyDefault.addDialog(default3, Benny)

def default4 = [id:4]
default4.npctext = "Huh? No way, air guitar is bogus!"
default4.playertext = "Oh, my imagination I guess. So what's going on?"
default4.result = 5
bennyDefault.addDialog(default4, Benny)

def default5 = [id:5]
default5.npctext = "Aww dude, not much. I'm just trying to put a new board together... I almost have everything I need too."
default5.options = []
default5.options << [text:"Almost? What are you missing?", result: 7]
default5.options << [text:"Nice. Good luck with that.", result: 8]
bennyDefault.addDialog(default5, Benny)

def default6 = [id:6]
default6.npctext = "Okay, that's enough of that."
default6.playertext = "Lol, damn! Anyway, what's up?"
default6.result = 5
bennyDefault.addDialog(default6, Benny)

def default7 = [id:7]
default7.npctext = "Synthetic Fiber! I need about seven of them but ever since the fluffs started defending themselves they've been hard to come by."
default7.options = []
default7.options << [text:"Haha, you can't handle a few fluffs? Man, I own fluffs on the daily. I'll take care of that for you.", result: 9]
default7.options << [text:"Well, I could take a stab at the fibers if you'd like.", result: 9]
default7.options << [text:"I wish you the best of luck with that.", result: 8]
bennyDefault.addDialog(default7, Benny)

def default8 = [id:8]
default8.npctext = "Er... thanks."
default8.flag = "Z9_Benny_SandFiber_Break"
default8.result = DONE
bennyDefault.addDialog(default8, Benny)

def default9 = [id:9]
default9.npctext = "Dude that'd be awesome! I only need about seven of them."
default9.quest = 96
default9.result = DONE
bennyDefault.addDialog(default9, Benny)

//------------------------------------------------------------
//Synthetic Fiber Break                                       
//------------------------------------------------------------
def fiberBreak = Benny.createConversation("fiberBreak", true, "Z9_Benny_SandFiber_Break")

def fiberBreak1 = [id:1]
fiberBreak1.npctext = "Hey dude, come back to help me with my board?"
fiberBreak1.options = []
fiberBreak1.options << [text:"Yeah, what'd you need help with again?", result: 2]
fiberBreak1.options << [text:"Nah, just saying sup.", result: 3]
fiberBreak.addDialog(fiberBreak1, Benny)

def fiberBreak2 = [id:2]
fiberBreak2.npctext = "Oh, dude, I just need some Synthetic Fiber for my board. You can getem from Sand Fluffs west of here."
fiberBreak2.quest = 96
fiberBreak2.flag = "!Z9_Benny_SandFiber_Break"
fiberBreak2.result = DONE
fiberBreak.addDialog(fiberBreak2, Benny)

def fiberBreak3 = [id:3]
fiberBreak3.npctext = "Oh, sup?"
fiberBreak3.playertext = "Nuthin'."
fiberBreak3.result = DONE
fiberBreak.addDialog(fiberBreak3, Benny)

//------------------------------------------------------------
//Synthetic Fiber Active                                      
//------------------------------------------------------------
def fiberActive = Benny.createConversation("fiberActive", true, "QuestStarted_96:2")

def fiberActive1 = [id:1]
fiberActive1.npctext = "Any luck finding some Synthetic Fiber?"
fiberActive1.playertext = "Not yet."
fiberActive1.result = 2
fiberActive.addDialog(fiberActive1, Benny)

def fiberActive2 = [id:2]
fiberActive2.npctext = "Dang. Lemme know if you find some."
fiberActive2.result = DONE
fiberActive.addDialog(fiberActive2, Benny)

//------------------------------------------------------------
//Synthetic Fiber Complete                                    
//------------------------------------------------------------
def fiberComplete = Benny.createConversation("fiberComplete", true, "QuestStarted_96:3")

fiberComplete.setUrgent(true)

def fiberComplete1 = [id:1]
fiberComplete1.npctext = "Is that some Synthetic Fiber in your pocket or are you just happy to see me?"
fiberComplete1.options = []
fiberComplete1.options << [text:"Uh... it's Synthetic Fiber.", result: 2]
fiberComplete1.options << [text:"Can't it be both?", result: 3]
fiberComplete1.options << [text:"Wouldn't you like to know?", result: 4]
fiberComplete.addDialog(fiberComplete1, Benny)

def fiberComplete2 = [id:2]
fiberComplete2.npctext = "Sweet! Now I can finish my board!"
fiberComplete2.playertext = "Awesome!"
fiberComplete2.exec = { event -> 
	event.player.removeMiniMapQuestActorName("Benny-VQS")
	runOnDeduct( event.player, 100402, 7, newBoardSuccess, newBoardFail ) 
}
fiberComplete2.result = DONE
fiberComplete.addDialog(fiberComplete2, Benny)

def fiberComplete3 = [id:3]
fiberComplete3.npctext = "Errr... I hope that's a joke!"
fiberComplete3.playertext = "Oh, alright. It's just some Synthetic Fiber. Hehe."
fiberComplete3.result = 2
fiberComplete.addDialog(fiberComplete3, Benny)

def fiberComplete4 = [id:4]
fiberComplete4.npctext = "Ack! No! I only want Sythetic Fiber!"
fiberComplete4.playertext = "Bah, alright. That's really all I have for you anyway."
fiberComplete4.result = 2
fiberComplete.addDialog(fiberComplete4, Benny)

newBoardSuccess = { event ->
	event.actor.setQuestFlag(GLOBAL, "Z09_Benny_NewBoard_Success")
	Benny.pushDialog(event.actor, "fiberSuccess")
}

newBoardFail = { event ->
	event.actor.setQuestFlag(GLOBAL, "Z09_Benny_NewBoard_Fail")
	Benny.pushDialog(event.actor, "fiberFail")
}

//------------------------------------------------------------
//New Board Success                                           
//------------------------------------------------------------
def fiberSuccess = Benny.createConversation("fiberSuccess", true, "Z09_Benny_NewBoard_Success")

def fiberSuccess1 = [id:1]
fiberSuccess1.npctext = "Hey, you know now that I've got everything for my board I really don't need this pattern anymore. Why don't you take it?"
fiberSuccess1.playertext = "Cool! Thanks, Benny."
fiberSuccess1.flag = "!Z09_Benny_NewBoard_Success"
fiberSuccess1.quest = 96
fiberSuccess1.result = DONE
fiberSuccess.addDialog(fiberSuccess1, Benny)

//------------------------------------------------------------
//New Board Fail                                              
//------------------------------------------------------------
def fiberFail = Benny.createConversation("fiberFail", true, "Z09_Benny_NewBoard_Fail")

def fiberFail1 = [id:1]
fiberFail1.npctext = "Very funny, %p. You don't have all the fiber I asked for. Where'd it go?"
fiberFail1.playertxt = "I swear I had enough a minute ago! I don't know what happened..."
fiberFail1.result = 2
fiberFail.addDialog(fiberFail1, Benny)

def fiberFail2 = [id:2]
fiberFail2.npctext = "A likely story. You'd better find that fiber."
fiberFail2.playertext = "Right."
fiberFail2.flag = "!Z09_Benny_NewBoard_Fail"
fiberFail2.result = DONE
fiberFail.addDialog(fiberFail2, Benny)

//------------------------------------------------------------
//Synthetic Fiber Repeatable Give                             
//------------------------------------------------------------
def fiberRepeatGive = Benny.createConversation("fiberRepeatGive", true, "QuestCompleted_96", "!QuestStarted_85", "!Z9_Benny_BoardShop_Allowed", "!Z9_Benny_BoardShop_AllowedAgain", "!Z9_Benny_BoardShop_Denied")

fiberRepeatGive.setUrgent(true)

def fiberRepeatGive1 = [id:1]
fiberRepeatGive1.npctext = "Alright! It's my helper, %p!"
fiberRepeatGive1.playertext = "Hey Benny, how's it going?"
fiberRepeatGive1.result = 2
fiberRepeatGive.addDialog(fiberRepeatGive1, Benny)

def fiberRepeatGive2 = [id:2]
fiberRepeatGive2.npctext = "Great! I had so much fun making that board you helped me with that I decided to start up a board shop! I'm actually glad you stopped by because I'm gonna need lots more Synthetic Fiber."
fiberRepeatGive2.playertext = "Oh, so you want me to get you some more?"
fiberRepeatGive2.result = 3
fiberRepeatGive.addDialog(fiberRepeatGive2, Benny)

def fiberRepeatGive3 = [id:3]
fiberRepeatGive3.npctext = "That'd be great! I'll reward you every time you bring me fifteen more! Think you could get some now?"
fiberRepeatGive3.options = []
fiberRepeatGive3.options << [text:"Yup, I'll get right on it, Benny.", exec: { event ->
	if(event.player.getConLevel() < 7.1) {
		event.player.setQuestFlag(GLOBAL, "Z9_Benny_BoardShop_Allowed")
		Benny.pushDialog(event.player, "boardShopAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z9_Benny_BoardShop_Denied")
		Benny.pushDialog(event.player, "boardShopDenied")
	}
}, result: DONE]
fiberRepeatGive3.options << [text:"Not now, but maybe later... okay?", result: 5]
fiberRepeatGive.addDialog(fiberRepeatGive3, Benny)

def fiberRepeatGive5 = [id:5]
fiberRepeatGive5.npctext = "A'ight... later on then."
fiberRepeatGive5.result = DONE
fiberRepeatGive.addDialog(fiberRepeatGive5, Benny)

//------------------------------------------------------------
//Synthetic Fiber Repeatable Active                           
//------------------------------------------------------------
def fiberRepeatActive = Benny.createConversation("fiberRepeatActive", true, "QuestStarted_85:2")

def fiberRepeatActive1 = [id:1]
fiberRepeatActive1.npctext = "Do you have the fibers?"
fiberRepeatActive1.playertext = "Not yet. I'll come back when I do."
fiberRepeatActive1.result = DONE
fiberRepeatActive.addDialog(fiberRepeatActive1, Benny)

//------------------------------------------------------------
//Synthetic Fiber Repeatable Complete                         
//------------------------------------------------------------
def fiberRepeatComplete = Benny.createConversation("fiberRepeatComplete", true, "QuestStarted_85:3")

fiberRepeatComplete.setUrgent(true)

def fiberRepeatComplete1 = [id:1]
fiberRepeatComplete1.npctext = "Wait a second, %p. Let me make sure you brought enough fiber with you..."
fiberRepeatComplete1.exec = { event -> 
	event.player.removeMiniMapQuestActorName("Benny-VQS")
	runOnDeduct( event.player, 100402, 15, boardShopSuccess, boardShopFail ) 
}
fiberRepeatComplete1.result = DONE
fiberRepeatComplete.addDialog(fiberRepeatComplete1, Benny)

boardShopSuccess = { event ->
	event.player.setQuestFlag(GLOBAL, "Z09_Benny_BoardShop_Success")
	Benny.pushDialog(event.actor, "fiberRepeatSuccess")
}

boardShopFail = { event ->
	event.player.setQuestFlag(GLOBAL, "Z09_Benny_BoardShop_Fail")
	Benny.pushDialog(event.actor, "fiberRepeatFail")
}

//------------------------------------------------------------
//Synthetic Fiber Repeatable Success                          
//------------------------------------------------------------
def fiberRepeatSuccess = Benny.createConversation("fiberRepeatSuccess", true, "Z09_Benny_BoardShop_Success")

def fiberRepeatSuccess1 = [id:1]
fiberRepeatSuccess1.npctext = "Hey! You do have enough. Great work, %p"
fiberRepeatSuccess1.playertext = "Sure, glad I could help."
fiberRepeatSuccess1.quest = 85
fiberRepeatSuccess1.result = 2
fiberRepeatSuccess.addDialog(fiberRepeatSuccess1, Benny)

def fiberRepeatSuccess2 = [id:2]
fiberRepeatSuccess2.npctext = "Want to help again?"
fiberRepeatSuccess2.options = []
fiberRepeatSuccess2.options << [text:"Sure!", exec: { event ->
	event.player.updateQuest(85, "Benny-VQS")
	if(event.player.getConLevel() < 7.1) {
		event.player.setQuestFlag(GLOBAL, "Z9_Benny_BoardShop_AllowedAgain")
		event.player.unsetQuestFlag(GLOBAL, "Z09_Benny_BoardShop_Success")
		Benny.pushDialog(event.player, "boardShopAllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z9_Benny_BoardShop_Denied")
		event.player.unsetQuestFlag(GLOBAL, "Z09_Benny_BoardShop_Success")
		Benny.pushDialog(event.player, "boardShopDenied")
	}
}, result: DONE]
fiberRepeatSuccess2.options << [text:"Not right now.", result: 4]
fiberRepeatSuccess.addDialog(fiberRepeatSuccess2, Benny)

def fiberRepeatSuccess4 = [id:4]
fiberRepeatSuccess4.npctext = "Okay, maybe later?"
fiberRepeatSuccess4.flag = "!Z09_Benny_BoardShop_Success"
fiberRepeatSuccess4.result = DONE
fiberRepeatSuccess.addDialog(fiberRepeatSuccess4, Benny)

//------------------------------------------------------------
//Synthetic Fiber Repeatable Fail                             
//------------------------------------------------------------
def fiberRepeatFail = Benny.createConversation("fiberRepeatFail", true, "Z09_Benny_BoardShop_Fail")

def fiberRepeatFail1 = [id:1]
fiberRepeatFail1.npctext = "You sneaky little... you don't have enough fiber!"
fiberRepeatFail1.playertext = "Ack, sorry Benny. I thought I did. I don't know what could've happened to it..."
fiberRepeatFail1.result = 2
fiberRepeatFail.addDialog(fiberRepeatFail1, Benny)

def fiberRepeatFail2 = [id:2]
fiberRepeatFail2.npctext = "Well, figure out what you did with it, or find some more, and come back."
fiberRepeatFail2.flag = "!Z09_Benny_BoardShop_Fail"
fiberRepeatFail2.result = DONE
fiberRepeatFail.addDialog(fiberRepeatFail2, Benny)

//------------------------------------------------------------
//Synthetic Fiber Repeatable Allowed Again                    
//------------------------------------------------------------
def boardShopAllowedAgain = Benny.createConversation("boardShopAllowedAgain", true, "Z9_Benny_BoardShop_AllowedAgain")

def boardShopAllowedAgain1 = [id:1]
boardShopAllowedAgain1.npctext = "Great!"
boardShopAllowedAgain1.quest = 85
boardShopAllowedAgain1.flag = "!Z9_Benny_BoardShop_AllowedAgain"
boardShopAllowedAgain1.result = DONE
boardShopAllowedAgain.addDialog(boardShopAllowedAgain1, Benny)

//------------------------------------------------------------
//Synthetic Fiber Repeatable Allowed                          
//------------------------------------------------------------
def boardShopAllowed = Benny.createConversation("boardShopAllowed", true, "Z9_Benny_BoardShop_Allowed")

def boardShopAllowed1 = [id:1]
boardShopAllowed1.npctext = "Awesome! Just bring them by when you have fifteen."
boardShopAllowed1.quest = 85
boardShopAllowed1.flag = "!Z9_Benny_BoardShop_Allowed"
boardShopAllowed1.result = DONE
boardShopAllowed.addDialog(boardShopAllowed1, Benny)

//------------------------------------------------------------
//Synthetic Fiber Repeatable Denied                           
//------------------------------------------------------------
def boardShopDenied = Benny.createConversation("boardShopDenied", true, "Z9_Benny_BoardShop_Denied")

def boardShopDenied1 = [id:1]
boardShopDenied1.npctext = "Hey man, not cool. I know the beach is a cool place to hang out, but you're way too strong to be helping."
boardShopDenied1.flag = "!Z9_Benny_BoardShop_Denied"
boardShopDenied1.exec  = { event ->
	event.player.centerPrint( "You must be level 7.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
boardShopDenied1.result = DONE
boardShopDenied.addDialog(boardShopDenied1, Benny)