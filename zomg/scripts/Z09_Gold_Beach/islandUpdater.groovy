//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def islandUpdater1 = "islandUpdater1"
myRooms.Beach_204.createTriggerZone(islandUpdater1, 595, 200, 1000, 550)
myManager.onTriggerIn(myRooms.Beach_204, islandUpdater1) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(227)) {
		event.actor.updateQuest(227, "Story-VQS")
	}
}

def islandUpdater2 = "islandUpdater2"
myRooms.Beach_205.createTriggerZone(islandUpdater2, 30, 200, 340, 550)
myManager.onTriggerIn(myRooms.Beach_205, islandUpdater2) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(227)) {
		event.actor.updateQuest(227, "Story-VQS")
	}
}