//Script created by gfern

import com.gaiaonline.mmo.battle.script.*
	
//----------------------------------------
//Define arrays                           
//----------------------------------------
golemList302 = []
golemList402 = []
golemList303 = []
golemList403 = []
golemList603 = []
golemList604 = []
golemList703 = []
golemList704 = []
golemList803 = []

//----------------------------------------
//Define variables                        
//----------------------------------------
golemExplosion = 0

golemX302List = []
golemX302List << 730
golemX302List << 860

golemY302List = []
golemY302List << 575
golemY302List << 315

golemX302 = random( golemX302List )
golemY302 = random( golemY302List )

golemX402List = []
golemX402List << 910
golemX402List << 875

golemY402List = []
golemY402List << 160
golemY402List << 500

golemX402 = random( golemX402List )
golemY402 = random( golemY402List )

golemX403List = []
golemX403List << 125
golemX403List << 160
golemX403List << 520
golemX403List << 430

golemY403List = []
golemY403List << 290
golemY403List << 575
golemY403List << 570
golemY403List << 250

golemX403 = random( golemX403List )
golemY403 = random( golemY403List )

golemX603List = []
golemX603List << 365
golemX603List << 930
golemX603List << 380
golemX603List << 820

golemY603List = []
golemY603List << 600
golemY603List << 575
golemY603List << 140
golemY603List << 180

golemX603 = random( golemX603List )
golemY603 = random( golemY603List )

golemX604List = []
golemX604List << 160
golemX604List << 115

golemY604List = []
golemY604List << 510
golemY604List << 145

golemX604 = random( golemX604List )
golemY604 = random( golemY604List )

golemX703List = []
golemX703List << 865
golemX703List << 645

golemY703List = []
golemY703List << 575
golemY703List << 105

golemX703 = random( golemX703List )
golemY703 = random( golemY703List )

golemX704List = []
golemX704List << 110
golemX704List << 210

golemY704List = []
golemY704List << 145
golemY704List << 580

golemX704 = random( golemX704List )
golemY704 = random( golemY704List )

golemX803List = []
golemX803List << 495
golemX803List << 910

golemY803List = []
golemY803List << 70
golemY803List << 110

golemX803 = random( golemX803List )
golemY803 = random( golemY803List )

//----------------------------------------
//Golem spawners                          
//----------------------------------------

golemSpawner2 = myRooms.Beach_302.spawnSpawner( "golem_spawner2", "sand_golem", 1 )
golemSpawner2.setPos( 860, 315 )
golemSpawner2.setHome( "Beach_302", 860, 315 )
golemSpawner2.setSpawnWhenPlayersAreInRoom( true )
golemSpawner2.setWaitTime( 100, 180 )
golemSpawner2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner2.childrenWander( true )
golemSpawner2.setMonsterLevelForChildren( 6.8 )

golemSpawner3 = myRooms.Beach_402.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner3.setPos( 830, 200 )
golemSpawner3.setHome( "Beach_402", 830, 200 )
golemSpawner3.setSpawnWhenPlayersAreInRoom( true )
golemSpawner3.setWaitTime( 100, 180 )
golemSpawner3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner3.childrenWander( true )
golemSpawner3.setMonsterLevelForChildren( 6.7 )

golemSpawner5 = myRooms.Beach_403.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner5.setPos( 125, 290 )
golemSpawner5.setHome( "Beach_403", 225, 200 )
golemSpawner5.setSpawnWhenPlayersAreInRoom( true )
golemSpawner5.setWaitTime( 100, 180 )
golemSpawner5.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner5.childrenWander( true )
golemSpawner5.setMonsterLevelForChildren( 6.8 )

golemSpawner6 = myRooms.Beach_403.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner6.setPos( 160, 550 )
golemSpawner6.setHome( "Beach_403", 260, 550 )
golemSpawner6.setSpawnWhenPlayersAreInRoom( true )
golemSpawner6.setWaitTime( 100, 180 )
golemSpawner6.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner6.childrenWander( true )
golemSpawner6.setMonsterLevelForChildren( 6.8 )

golemSpawner13 = myRooms.Beach_603.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner13.setPos( 365, 600 )
golemSpawner13.setHome( "Beach_603", 365, 600 )
golemSpawner13.setSpawnWhenPlayersAreInRoom( true )
golemSpawner13.setWaitTime( 100, 180 )
golemSpawner13.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner13.childrenWander( true )
golemSpawner13.setMonsterLevelForChildren( 6.6 )

golemSpawner15 = myRooms.Beach_603.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner15.setPos( 380, 140 )
golemSpawner15.setHome( "Beach_603", 380, 140 )
golemSpawner15.setSpawnWhenPlayersAreInRoom( true )
golemSpawner15.setWaitTime( 100, 180 )
golemSpawner15.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner15.childrenWander( true )
golemSpawner15.setMonsterLevelForChildren( 6.6 )

golemSpawner17 = myRooms.Beach_604.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner17.setPos( 160, 590 )
golemSpawner17.setHome( "Beach_604", 160, 590 )
golemSpawner17.setSpawnWhenPlayersAreInRoom( true )
golemSpawner17.setWaitTime( 100, 180 )
golemSpawner17.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner17.childrenWander( true )
golemSpawner17.setMonsterLevelForChildren( 6.5 )

golemSpawner18 = myRooms.Beach_604.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner18.setPos( 115, 145 )
golemSpawner18.setHome( "Beach_604", 115, 145 )
golemSpawner18.setSpawnWhenPlayersAreInRoom( true )
golemSpawner18.setWaitTime( 100, 180 )
golemSpawner18.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner18.childrenWander( true )
golemSpawner18.setMonsterLevelForChildren( 6.5 )

golemSpawner20 = myRooms.Beach_703.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner20.setPos( 765, 275 )
golemSpawner20.setHome( "Beach_703", 765, 275 )
golemSpawner20.setSpawnWhenPlayersAreInRoom( true )
golemSpawner20.setWaitTime( 100, 180 )
golemSpawner20.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner20.childrenWander( true )
golemSpawner20.setMonsterLevelForChildren( 6.5 )

golemSpawner21 = myRooms.Beach_704.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner21.setPos( 110, 145 )
golemSpawner21.setHome( "Beach_704", 110, 145 )
golemSpawner21.setSpawnWhenPlayersAreInRoom( true )
golemSpawner21.setWaitTime( 100, 180 )
golemSpawner21.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner21.childrenWander( true )
golemSpawner21.setMonsterLevelForChildren( 6.4 )

golemSpawner22 = myRooms.Beach_704.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner22.setPos( 210, 580 )
golemSpawner22.setHome( "Beach_704", 210, 580 )
golemSpawner22.setSpawnWhenPlayersAreInRoom( true )
golemSpawner22.setWaitTime( 100, 180 )
golemSpawner22.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner22.childrenWander( true )
golemSpawner22.setMonsterLevelForChildren( 6.4 )

golemSpawner23 = myRooms.Beach_803.spawnSpawner( "golem_spawner3", "sand_golem", 1 )
golemSpawner23.setPos( 495, 70 )
golemSpawner23.setHome( "Beach_803", 490, 70 )
golemSpawner23.setSpawnWhenPlayersAreInRoom( true )
golemSpawner23.setWaitTime( 100, 180 )
golemSpawner23.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner23.childrenWander( true )
golemSpawner23.setMonsterLevelForChildren( 6.4 )

golemSpawner24 = myRooms.Beach_404.spawnSpawner( "golem_spawner_24", "sand_golem", 1 )
golemSpawner24.setPos( 140, 200 )
golemSpawner24.setHome( "Beach_803", 140, 200 )
golemSpawner24.setSpawnWhenPlayersAreInRoom( true )
golemSpawner24.setWaitTime( 100 , 180 )
golemSpawner24.childrenWander( false )
golemSpawner24.setMonsterLevelForChildren( 6.5 )
golemSpawner24.addPatrolPointForChildren( "Beach_404", 105, 600, 20 )
golemSpawner24.addPatrolPointForChildren( "Beach_504", 45, 350, 0 )
golemSpawner24.addPatrolPointForChildren( "Beach_604", 45, 350, 0 )
golemSpawner24.addPatrolPointForChildren( "Beach_704", 215, 350, 20 )
golemSpawner24.addPatrolPointForChildren( "Beach_604", 45, 350, 0 )
golemSpawner24.addPatrolPointForChildren( "Beach_504", 45, 350, 0 )

golemSpawner25 = myRooms.Beach_503.spawnSpawner( "golem_spawner_25", "sand_golem", 1 )
golemSpawner25.setPos( 400, 285 )
golemSpawner25.setHome( "Beach_503", 400, 285 )
golemSpawner25.setSpawnWhenPlayersAreInRoom( true )
golemSpawner25.setWaitTime( 100 , 180 )
golemSpawner25.childrenWander( false )
golemSpawner25.setMonsterLevelForChildren( 6.5 )
golemSpawner25.addPatrolPointForChildren( "Beach_503", 760, 460, 20 )
golemSpawner25.addPatrolPointForChildren( "Beach_603", 605, 370, 0 )
golemSpawner25.addPatrolPointForChildren( "Beach_703", 955, 360, 0 )
golemSpawner25.addPatrolPointForChildren( "Beach_803", 750, 290, 20 )
golemSpawner25.addPatrolPointForChildren( "Beach_703", 955, 350, 0 )
golemSpawner25.addPatrolPointForChildren( "Beach_603", 605, 370, 0 )

//---------------------------------------
//Room based spawners                    
//---------------------------------------
/*golemSpawner302a = myRooms.Beach_404.spawnSpawner( "golem_spawner_302a", "sand_golem", 50 )
golemSpawner302a.setPos( 140, 200 )
golemSpawner302a.setHome( "Beach_302", golemX302, golemY302 )
golemSpawner302a.setInitialMoveForChildren( "Beach_302", golemX302, golemY302 )
golemSpawner302a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner302a.setWaitTime( 2 , 5 )
golemSpawner302a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner302a.childrenWander( true )
golemSpawner302a.setMonsterLevelForChildren( 6.8 )

golemSpawner302b = myRooms.Beach_503.spawnSpawner( "golem_spawner_302b", "sand_golem", 50 )
golemSpawner302b.setPos( 400, 285 )
golemSpawner302b.setHome( "Beach_302", golemX302, golemY302 )
golemSpawner302b.setInitialMoveForChildren( "Beach_302", golemX302, golemY302 )
golemSpawner302b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner302b.setWaitTime( 2 , 5 )
golemSpawner302b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner302b.childrenWander( true )
golemSpawner302b.setMonsterLevelForChildren( 6.8 )

golemSpawner402a = myRooms.Beach_404.spawnSpawner( "golem_spawner_402a", "sand_golem", 50 )
golemSpawner402a.setPos( 140, 200 )
golemSpawner402a.setHome( "Beach_402", golemX402, golemY402 )
golemSpawner402a.setInitialMoveForChildren( "Beach_402", golemX402, golemY402 )
golemSpawner402a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner402a.setWaitTime( 2 , 5 )
golemSpawner402a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner402a.childrenWander( true )
golemSpawner402a.setMonsterLevelForChildren( 6.7 )

golemSpawner402b = myRooms.Beach_503.spawnSpawner( "golem_spawner_402b", "sand_golem", 50 )
golemSpawner402b.setPos( 400, 285 )
golemSpawner402b.setHome( "Beach_402", golemX402, golemY402 )
golemSpawner402b.setInitialMoveForChildren( "Beach_402", golemX402, golemY402 )
golemSpawner402b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner402b.setWaitTime( 2 , 5 )
golemSpawner402b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner402b.childrenWander( true )
golemSpawner402b.setMonsterLevelForChildren( 6.7 )

golemSpawner403a = myRooms.Beach_404.spawnSpawner( "golem_spawner_403a", "sand_golem", 50 )
golemSpawner403a.setPos( 140, 200 )
golemSpawner403a.setHome( "Beach_403", golemX403, golemY403 )
golemSpawner403a.setInitialMoveForChildren( "Beach_403", golemX403, golemY403 )
golemSpawner403a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner403a.setWaitTime( 2 , 5 )
golemSpawner403a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner403a.childrenWander( true )
golemSpawner403a.setMonsterLevelForChildren( 6.8 )

golemSpawner403b = myRooms.Beach_503.spawnSpawner( "golem_spawner_403b", "sand_golem", 50 )
golemSpawner403b.setPos( 400, 285 )
golemSpawner403b.setHome( "Beach_403", golemX403, golemY403 )
golemSpawner403b.setInitialMoveForChildren( "Beach_403", golemX403, golemY403 )
golemSpawner403b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner403b.setWaitTime( 2 , 5 )
golemSpawner403b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner403b.childrenWander( true )
golemSpawner403b.setMonsterLevelForChildren( 6.8 )

golemSpawner603a = myRooms.Beach_404.spawnSpawner( "golem_spawner_603a", "sand_golem", 50 )
golemSpawner603a.setPos( 140, 200 )
golemSpawner603a.setHome( "Beach_603", golemX603, golemY603 )
golemSpawner603a.setInitialMoveForChildren( "Beach_603", golemX603, golemY603 )
golemSpawner603a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner603a.setWaitTime( 2 , 5 )
golemSpawner603a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner603a.childrenWander( true )
golemSpawner603a.setMonsterLevelForChildren( 6.6 )

golemSpawner603b = myRooms.Beach_503.spawnSpawner( "golem_spawner_603b", "sand_golem", 50 )
golemSpawner603b.setPos( 400, 285 )
golemSpawner603b.setHome( "Beach_603", golemX603, golemY603 )
golemSpawner603b.setInitialMoveForChildren( "Beach_603", golemX603, golemY603 )
golemSpawner603b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner603b.setWaitTime( 2 , 5 )
golemSpawner603b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner603b.childrenWander( true )
golemSpawner603b.setMonsterLevelForChildren( 6.6 )

golemSpawner604a = myRooms.Beach_404.spawnSpawner( "golem_spawner_604a", "sand_golem", 50 )
golemSpawner604a.setPos( 140, 200 )
golemSpawner604a.setHome( "Beach_604", golemX604, golemY604 )
golemSpawner604a.setInitialMoveForChildren( "Beach_604", golemX604, golemY604 )
golemSpawner604a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner604a.setWaitTime( 2 , 5 )
golemSpawner604a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner604a.childrenWander( true )
golemSpawner604a.setMonsterLevelForChildren( 6.5 )

golemSpawner604b = myRooms.Beach_503.spawnSpawner( "golem_spawner_604b", "sand_golem", 50 )
golemSpawner604b.setPos( 400, 285 )
golemSpawner604b.setHome( "Beach_604", golemX604, golemY604 )
golemSpawner604b.setInitialMoveForChildren( "Beach_604", golemX604, golemY604 )
golemSpawner604b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner604b.setWaitTime( 2 , 5 )
golemSpawner604b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner604b.childrenWander( true )
golemSpawner604b.setMonsterLevelForChildren( 6.5 )

golemSpawner703a = myRooms.Beach_404.spawnSpawner( "golem_spawner_703a", "sand_golem", 50 )
golemSpawner703a.setPos( 140, 200 )
golemSpawner703a.setHome( "Beach_703", golemX703, golemY703 )
golemSpawner703a.setInitialMoveForChildren( "Beach_703", golemX703, golemY703 )
golemSpawner703a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner703a.setWaitTime( 2 , 5 )
golemSpawner703a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner703a.childrenWander( true )
golemSpawner703a.setMonsterLevelForChildren( 6.5 )

golemSpawner703b = myRooms.Beach_503.spawnSpawner( "golem_spawner_703b", "sand_golem", 50 )
golemSpawner703b.setPos( 400, 285 )
golemSpawner703b.setHome( "Beach_703", golemX703, golemY703 )
golemSpawner703b.setInitialMoveForChildren( "Beach_703", golemX703, golemY703 )
golemSpawner703b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner703b.setWaitTime( 2 , 5 )
golemSpawner703b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner703b.childrenWander( true )
golemSpawner703b.setMonsterLevelForChildren( 6.5 )

golemSpawner704a = myRooms.Beach_404.spawnSpawner( "golem_spawner_704a", "sand_golem", 50 )
golemSpawner704a.setPos( 140, 200 )
golemSpawner704a.setHome( "Beach_704", golemX704, golemY704 )
golemSpawner704a.setInitialMoveForChildren( "Beach_704", golemX704, golemY704 )
golemSpawner704a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner704a.setWaitTime( 2 , 5 )
golemSpawner704a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner704a.childrenWander( true )
golemSpawner704a.setMonsterLevelForChildren( 6.4 )

golemSpawner704b = myRooms.Beach_503.spawnSpawner( "golem_spawner_704b", "sand_golem", 50 )
golemSpawner704b.setPos( 400, 285 )
golemSpawner704b.setHome( "Beach_704", golemX704, golemY704 )
golemSpawner704b.setInitialMoveForChildren( "Beach_704", golemX704, golemY704 )
golemSpawner704b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner704b.setWaitTime( 2 , 5 )
golemSpawner704b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner704b.childrenWander( true )
golemSpawner704b.setMonsterLevelForChildren( 6.4 )

golemSpawner803a = myRooms.Beach_404.spawnSpawner( "golem_spawner_803a", "sand_golem", 50 )
golemSpawner803a.setPos( 140, 200 )
golemSpawner803a.setHome( "Beach_803", golemX803, golemY803 )
golemSpawner803a.setInitialMoveForChildren( "Beach_803", golemX803, golemY803 )
golemSpawner803a.setSpawnWhenPlayersAreInRoom( true )
golemSpawner803a.setWaitTime( 2 , 5 )
golemSpawner803a.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner803a.childrenWander( true )
golemSpawner803a.setMonsterLevelForChildren( 6.4 )

golemSpawner803b = myRooms.Beach_503.spawnSpawner( "golem_spawner_803b", "sand_golem", 50 )
golemSpawner803b.setPos( 400, 285 )
golemSpawner803b.setHome( "Beach_803", golemX803, golemY803 )
golemSpawner803b.setInitialMoveForChildren( "Beach_803", golemX803, golemY803 )
golemSpawner803b.setSpawnWhenPlayersAreInRoom( true )
golemSpawner803b.setWaitTime( 2 , 5 )
golemSpawner803b.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
golemSpawner803b.childrenWander( true )
golemSpawner803b.setMonsterLevelForChildren( 6.4 )

//---------------------------------------
//Counting scripts                       
//---------------------------------------
def checkCastleRoomsForPlayers() {
	playerCount404 = myRooms.Beach_404.getPlayerCount()
	//println "**** ${playerCount404} players in Beach_404 ****"

	playerCount503 = myRooms.Beach_503.getPlayerCount()
	//println "**** ${playerCount503} players in Beach_503 ****"
	
	playerCount903 = myRooms.Beach_903.getPlayerCount()
	//println "**** ${playerCount903} players in Beach_903 ****"
	
	//println "*** ${golemList} ****"
}

//---------------------------------------
//Check for golems in rooms and respawn  
//---------------------------------------
def golemRespawn302() {
	checkCastleRoomsForPlayers()
	golemX302 = random( golemX302List )
	golemY302 = random( golemY302List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner302a
		golemSpawnList << golemSpawner302b
		
		golemSpawner = random( golemSpawnList )
		golemList302 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner302b
		
		golemSpawner = random( golemSpawnList )
		golemList302 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner302a
		
		golemSpawner = random( golemSpawnList )
		golemList302 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

def golemRespawn402() {
	checkCastleRoomsForPlayers()
	golemX402 = random( golemX402List )
	golemY402 = random( golemY402List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner402a
		golemSpawnList << golemSpawner402b
		
		golemSpawner = random( golemSpawnList )
		golemList402 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner402b
		
		golemSpawner = random( golemSpawnList )
		golemList402 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner402a
		
		golemSpawner = random( golemSpawnList )
		golemList402 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

def golemRespawn403() {
	checkCastleRoomsForPlayers()
	golemX403 = random( golemX403List )
	golemY403 = random( golemY403List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner403a
		golemSpawnList << golemSpawner403b
		
		golemSpawner = random( golemSpawnList )
		golemList403 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner403b
		
		golemSpawner = random( golemSpawnList )
		golemList403 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner403a
		
		golemSpawner = random( golemSpawnList )
		golemList403 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

def golemRespawn603() {
	checkCastleRoomsForPlayers()
	golemX603 = random( golemX603List )
	golemY603 = random( golemY603List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner603a
		golemSpawnList << golemSpawner603b
		
		golemSpawner = random( golemSpawnList )
		golemList603 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner603b
		
		golemSpawner = random( golemSpawnList )
		golemList603 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner603a
		
		golemSpawner = random( golemSpawnList )
		golemList603 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

def golemRespawn604() {
	checkCastleRoomsForPlayers()
	golemX604 = random( golemX604List )
	golemY604 = random( golemY604List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner604a
		golemSpawnList << golemSpawner604b
		
		golemSpawner = random( golemSpawnList )
		golemList604 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner604b
		
		golemSpawner = random( golemSpawnList )
		golemList604 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner604a
		
		golemSpawner = random( golemSpawnList )
		golemList604 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

def golemRespawn703() {
	checkCastleRoomsForPlayers()
	golemX703 = random( golemX703List )
	golemY703 = random( golemY703List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner703a
		golemSpawnList << golemSpawner703b
		
		golemSpawner = random( golemSpawnList )
		golemList703 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner703b
		
		golemSpawner = random( golemSpawnList )
		golemList703 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner703a
		
		golemSpawner = random( golemSpawnList )
		golemList703 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

def golemRespawn704() {
	checkCastleRoomsForPlayers()
	golemX704 = random( golemX704List )
	golemY704 = random( golemY704List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner704a
		golemSpawnList << golemSpawner704b
		
		golemSpawner = random( golemSpawnList )
		golemList704 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner704b
		
		golemSpawner = random( golemSpawnList )
		golemList704 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner704a
		
		golemSpawner = random( golemSpawnList )
		golemList704 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

def golemRespawn803() {
	checkCastleRoomsForPlayers()
	golemX803 = random( golemX803List )
	golemY803 = random( golemY803List )
	
	if(playerCount404 == 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner803a
		golemSpawnList << golemSpawner803b
		
		golemSpawner = random( golemSpawnList )
		golemList803 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 == 0 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner803b
		
		golemSpawner = random( golemSpawnList )
		golemList803 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 == 0 && playerCount503 ) {
		golemSpawnList = []
		golemSpawnList << golemSpawner803a
		
		golemSpawner = random( golemSpawnList )
		golemList803 << golemSpawner.forceSpawnNow()
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion < 24) {
		golemExplosion++
		//println "***DEBUG*** golemExplosion = ${golemExplosion}"
	}
	if(playerCount404 > 0 && playerCount503 > 0 && golemExplosion >= 24) {
		golemList302 << golemSpawner302a.forceSpawnNow()
		golemList302 << golemSpawner302b.forceSpawnNow()
		
		golemList402 << golemSpawner402a.forceSpawnNow()
		golemList402 << golemSpawner402b.forceSpawnNow()
		
		golemList403 << golemSpawner403a.forceSpawnNow()
		golemList403 << golemSpawner403b.forceSpawnNow()
		
		golemList603 << golemSpawner603a.forceSpawnNow()
		golemList603 << golemSpawner603b.forceSpawnNow()
		
		golemList603 << golemSpawner604a.forceSpawnNow()
		golemList603 << golemSpawner604b.forceSpawnNow()
		
		golemList703 << golemSpawner703a.forceSpawnNow()
		golemList703 << golemSpawner703b.forceSpawnNow()
		
		golemList704 << golemSpawner704a.forceSpawnNow()
		golemList704 << golemSpawner704b.forceSpawnNow()
		
		golemList803 << golemSpawner803a.forceSpawnNow()
		golemList803 << golemSpawner803b.forceSpawnNow()
	}
}

//Initial logic

golemList302 << golemSpawner2.forceSpawnNow()
golemSpawner2.stopSpawning()

golemList402 << golemSpawner3.forceSpawnNow()
golemSpawner3.stopSpawning()

golemList403 << golemSpawner5.forceSpawnNow()
golemSpawner5.stopSpawning()

golemList403 << golemSpawner6.forceSpawnNow()
golemSpawner6.stopSpawning()

golemList603 << golemSpawner13.forceSpawnNow()
golemSpawner13.stopSpawning()

golemList603 << golemSpawner15.forceSpawnNow()
golemSpawner15.stopSpawning()

golemList604 << golemSpawner17.forceSpawnNow()
golemSpawner17.stopSpawning()

golemList604 << golemSpawner18.forceSpawnNow()
golemSpawner18.stopSpawning()

golemList703 << golemSpawner20.forceSpawnNow()
golemSpawner20.stopSpawning()

golemList704 << golemSpawner21.forceSpawnNow()
golemSpawner21.stopSpawning()

golemList704 << golemSpawner22.forceSpawnNow()
golemSpawner22.stopSpawning()

golemList803 << golemSpawner23.forceSpawnNow()
golemSpawner23.stopSpawning()

golemSpawner302a.stopSpawning()
golemSpawner302b.stopSpawning()

golemSpawner402a.stopSpawning()
golemSpawner402b.stopSpawning()

golemSpawner403a.stopSpawning()
golemSpawner403b.stopSpawning()

golemSpawner603a.stopSpawning()
golemSpawner603b.stopSpawning()

golemSpawner604a.stopSpawning()
golemSpawner604b.stopSpawning()

golemSpawner703a.stopSpawning()
golemSpawner703b.stopSpawning()

golemSpawner704a.stopSpawning()
golemSpawner704b.stopSpawning()

golemSpawner803a.stopSpawning()
golemSpawner803b.stopSpawning()

//Dead triggers
runOnDeath(golemList302 [0]) { 
	println "*** Removing ${golemList302 [0]} from golemList302 ***"
	golemList302.remove(golemList302 [0])
	myManager.schedule(30) { golemRespawn302() } 
}

runOnDeath(golemList402 [0]) { 
	println "*** Removing ${golemList402 [0]} from golemList402 ***"
	golemList402.remove(golemList402 [0])
	myManager.schedule(30) { golemRespawn402() }
}

runOnDeath(golemList403 [0]) { 
	println "*** Removing ${golemList403 [0]} from golemList403 ***"
	golemList403.remove(golemList403 [0])
	myManager.schedule(30) { golemRespawn403() }
}

runOnDeath(golemList403 [1]) { 
	println "*** Removing ${golemList403 [1]} from golemList403 ***"
	golemList403.remove(golemList403 [1])
	myManager.schedule(30) { golemRespawn403() }
}

runOnDeath(golemList603 [0]) { 
	println "*** Removing ${golemList603 [0]} from golemList603 ***"
	golemList603.remove(golemList603 [0])
	myManager.schedule(30) { golemRespawn403() }
}

runOnDeath(golemList603 [1]) { 
	println "*** Removing ${golemList603 [1]} from golemList603 ***"
	golemList603.remove(golemList603 [1])
	myManager.schedule(30) { golemRespawn403() }
}

runOnDeath(golemList604 [0]) { 
	println "*** Removing ${golemList604 [0]} from golemList604 ***"
	golemList604.remove(golemList604 [0])
	myManager.schedule(30) { golemRespawn604() }
}

runOnDeath(golemList604 [1]) { 
	println "*** Removing ${golemList604 [1]} from golemList604 ***"
	golemList604.remove(golemList604 [1])
	myManager.schedule(30) { golemRespawn604() }
}

runOnDeath(golemList703 [0]) {
	println "*** Removing ${golemList703 [0]} from golemList703 ***"
	golemList703.remove(golemList703 [0])
	myManager.schedule(30) { golemRespawn703() }
}

runOnDeath(golemList704 [0]) {
	println "*** Removing ${golemList704 [0]} from golemList704 ***"
	golemList704.remove(golemList704 [0])
	myManager.schedule(30) { golemRespawn704() }
}

runOnDeath(golemList704 [1]) {
	println "*** Removing ${golemList704 [1]} from golemList704 ***"
	golemList704.remove(golemList704 [1])
	myManager.schedule(30) { golemRespawn704() }
}

runOnDeath(golemList803 [0]) {
	println "*** Removing ${golemList803 [0]} from golemList803 ***"
	golemList803.remove(golemList803 [0])
	myManager.schedule(30) { golemRespawn803() }
}*/

golemSpawner2.spawnAllNow()


golemSpawner3.spawnAllNow()


golemSpawner5.spawnAllNow()


golemSpawner6.spawnAllNow()


golemSpawner13.spawnAllNow()


golemSpawner15.spawnAllNow()


golemSpawner17.spawnAllNow()


golemSpawner18.spawnAllNow()


golemSpawner20.spawnAllNow()


golemSpawner21.spawnAllNow()


golemSpawner22.spawnAllNow()


golemSpawner23.spawnAllNow()