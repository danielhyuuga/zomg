//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
def spawner1 = myRooms.Beach_303.spawnSpawner( "spawner_303_1", "anchor_bug", 1)
spawner1.setPos( 160, 210 )
spawner1.setInitialMoveForChildren( "Beach_303", 960, 540 )
spawner1.setHome( "Beach_303", 960, 540 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 80, 100 )
spawner1.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 6.9 )

// Spawner two
def spawner2 = myRooms.Beach_303.spawnSpawner( "spawner_303_2", "anchor_bug", 1)
spawner2.setPos( 160, 205 )
spawner2.setInitialMoveForChildren( "Beach_303", 945, 330 )
spawner2.setHome( "Beach_303", 945, 330 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 80, 100 )
spawner2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 6.9 )

// Spawner three
def spawner3 = myRooms.Beach_303.spawnSpawner( "spawner_303_3", "anchor_bug", 1)
spawner3.setPos( 160, 200 )
spawner3.setInitialMoveForChildren( "Beach_303", 890, 585 )
spawner3.setHome( "Beach_303", 890, 585 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 80, 100 )
spawner3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 6.9 )

// Spawner four
def spawner4 = myRooms.Beach_303.spawnSpawner( "spawner_303_4", "anchor_bug", 1)
spawner4.setPos( 160, 195 )
spawner4.setInitialMoveForChildren( "Beach_303", 120, 570 )
spawner4.setHome( "Beach_303", 120, 570 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 80, 100 )
spawner4.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 6.9 )

// Spawner five
def spawner5 = myRooms.Beach_303.spawnSpawner( "spawner_303_5", "anchor_bug", 1)
spawner5.setPos( 160, 190 )
spawner5.setInitialMoveForChildren( "Beach_303", 185, 600 )
spawner5.setHome( "Beach_303", 185, 600 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 80, 100 )
spawner5.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner5.childrenWander( true )
spawner5.setMonsterLevelForChildren( 6.9 )

// Spawner six
def spawner6 = myRooms.Beach_303.spawnSpawner( "spawner_303_6", "anchor_bug", 1)
spawner6.setPos( 160, 185 )
spawner6.setInitialMoveForChildren( "Beach_303", 785, 615 )
spawner6.setHome( "Beach_303", 785, 615 )
spawner6.setSpawnWhenPlayersAreInRoom( true )
spawner6.setWaitTime( 80, 100 )
spawner6.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner6.childrenWander( true )
spawner6.setMonsterLevelForChildren( 6.9 )

// Spawner seven
def spawner7 = myRooms.Beach_303.spawnSpawner( "spawner_303_7", "anchor_bug", 1)
spawner7.setPos( 160, 180 )
spawner7.setInitialMoveForChildren( "Beach_303", 950, 440 )
spawner7.setHome( "Beach_303", 950, 440 )
spawner7.setSpawnWhenPlayersAreInRoom( true )
spawner7.setWaitTime( 80, 100 )
spawner7.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner7.childrenWander( true )
spawner7.setMonsterLevelForChildren( 6.9 )

//Alliances
spawner1.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner7 )
spawner3.allyWithSpawner( spawner6 )
spawner4.allyWithSpawner( spawner5 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()
spawner4.forceSpawnNow()
spawner5.forceSpawnNow()
spawner6.forceSpawnNow()
spawner7.forceSpawnNow()