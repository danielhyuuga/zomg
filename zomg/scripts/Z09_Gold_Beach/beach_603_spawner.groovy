//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

FIBER_PERCENTAGE = 55

// Spawner one
spawner_603_1 = myRooms.Beach_603.spawnSpawner( "spawner_603_1", "sand_fluff", 1)
spawner_603_1.stopSpawning()
spawner_603_1.setPos( 200, 640 )
spawner_603_1.setInitialMoveForChildren( "Beach_603", 855, 185 )
spawner_603_1.setHome( "Beach_603", 855, 185 )
spawner_603_1.setSpawnWhenPlayersAreInRoom( true )
spawner_603_1.setWaitTime( 80, 100 )
spawner_603_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_603_1.setMonsterLevelForChildren( 6.6 )

// Spawner two
spawner_603_2 = myRooms.Beach_603.spawnSpawner( "spawner_603_2", "sand_fluff", 1)
spawner_603_2.stopSpawning()
spawner_603_2.setPos( 200, 645 )
spawner_603_2.setInitialMoveForChildren( "Beach_603", 650, 170 )
spawner_603_2.setHome( "Beach_603", 650, 170 )
spawner_603_2.setSpawnWhenPlayersAreInRoom( true )
spawner_603_2.setWaitTime( 80, 100 )
spawner_603_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_603_2.setMonsterLevelForChildren( 6.6 )

// Spawner three
spawner_603_3 = myRooms.Beach_603.spawnSpawner( "spawner_603_3", "sand_fluff", 1)
spawner_603_3.stopSpawning()
spawner_603_3.setPos( 200, 650 )
spawner_603_3.setInitialMoveForChildren( "Beach_603", 850, 515 )
spawner_603_3.setHome( "Beach_603", 850, 515 )
spawner_603_3.setSpawnWhenPlayersAreInRoom( true )
spawner_603_3.setWaitTime( 80, 100 )
spawner_603_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 300 )
spawner_603_3.setMonsterLevelForChildren( 6.6 )

//Alliances
spawner_603_1.allyWithSpawner( spawner_603_2 )
spawner_603_1.allyWithSpawner( spawner_603_3 )
spawner_603_2.allyWithSpawner( spawner_603_3 )

//Spawning
def spawn_603_1() {
	garlic_603_1 = spawner_603_1.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_603_1) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_603_1()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_603_1 = event.actor.getHated()
		hateList_603_1.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.2) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_603_1()

def spawn_603_2() {
	garlic_603_2 = spawner_603_2.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_603_2) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_603_2()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_603_2 = event.actor.getHated()
		hateList_603_2.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.2) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_603_2()

def spawn_603_3() {
	garlic_603_3 = spawner_603_3.forceSpawnNow() //Spawn the fluff
	runOnDeath(garlic_603_3) { event -> //When the fluff dies
		myManager.schedule(random(80, 100)) {spawn_603_3()}
		fiberRoll = random(100) //Just a roll, will be compared against FIBER_PERCENTAGE
		
		hateList_603_3 = event.actor.getHated()
		hateList_603_3.each() {
			if(isPlayer(it) && fiberRoll <= FIBER_PERCENTAGE && (it.isOnQuest(85, 2) || it.isOnQuest(96, 2)) && it.getConLevel() < 8.2) {
				it.grantQuantityItem(100402, 1)
			}
		}
	}
}

spawn_603_3()