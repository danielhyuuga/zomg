//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
def spawner1 = myRooms.Beach_204.spawnSpawner( "spawner_204_1", "anchor_bug", 1)
spawner1.setPos( 50, 310 )
spawner1.setInitialMoveForChildren( "Beach_204", 830, 340 )
spawner1.setHome( "Beach_204", 830, 340 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 80, 100 )
spawner1.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 6.9 )

// Spawner two
def spawner2 = myRooms.Beach_204.spawnSpawner( "spawner_204_2", "anchor_bug", 1)
spawner2.setPos( 50, 315 )
spawner2.setInitialMoveForChildren( "Beach_204", 885, 440 )
spawner2.setHome( "Beach_204", 885, 440 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 80, 100 )
spawner2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 6.9 )

// Spawner three
def spawner3 = myRooms.Beach_204.spawnSpawner( "spawner_204_3", "anchor_bug", 1)
spawner3.setPos( 50, 320 )
spawner3.setInitialMoveForChildren( "Beach_204", 115, 110 )
spawner3.setHome( "Beach_204", 115, 110 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 80, 100 )
spawner3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 6.9 )

// Spawner four
def spawner4 = myRooms.Beach_204.spawnSpawner( "spawner_204_4", "anchor_bug", 1)
spawner4.setPos( 50, 325 )
spawner4.setInitialMoveForChildren( "Beach_204", 95, 295 )
spawner4.setHome( "Beach_204", 95, 295 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 80, 100 )
spawner4.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 6.9 )

// Spawner five
def spawner5 = myRooms.Beach_204.spawnSpawner( "spawner_204_5", "anchor_bug", 1)
spawner5.setPos( 50, 330 )
spawner5.setInitialMoveForChildren( "Beach_204", 840, 470 )
spawner5.setHome( "Beach_204", 840, 470 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 80, 100 )
spawner5.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner5.childrenWander( true )
spawner5.setMonsterLevelForChildren( 6.9 )

// Spawner six
def spawner6 = myRooms.Beach_204.spawnSpawner( "spawner_204_6", "anchor_bug", 1)
spawner6.setPos( 50, 335 )
spawner6.setInitialMoveForChildren( "Beach_204", 940, 480 )
spawner6.setHome( "Beach_204", 940, 480 )
spawner6.setSpawnWhenPlayersAreInRoom( true )
spawner6.setWaitTime( 80, 100 )
spawner6.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner6.childrenWander( true )
spawner6.setMonsterLevelForChildren( 6.9 )

// Spawner seven
def spawner7 = myRooms.Beach_204.spawnSpawner( "spawner_204_7", "anchor_bug", 1)
spawner7.setPos( 50, 340 )
spawner7.setInitialMoveForChildren( "Beach_204", 90, 350 )
spawner7.setHome( "Beach_204", 90, 350 )
spawner7.setSpawnWhenPlayersAreInRoom( true )
spawner7.setWaitTime( 80, 100 )
spawner7.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner7.childrenWander( true )
spawner7.setMonsterLevelForChildren( 6.9 )

// Spawner eight
def spawner8 = myRooms.Beach_204.spawnSpawner( "spawner_204_8", "anchor_bug", 1)
spawner8.setPos( 50, 345 )
spawner8.setInitialMoveForChildren( "Beach_204", 95, 295 )
spawner8.setHome( "Beach_204", 95, 295 )
spawner8.setSpawnWhenPlayersAreInRoom( true )
spawner8.setWaitTime( 80, 100 )
spawner8.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner8.childrenWander( true )
spawner8.setMonsterLevelForChildren( 6.9 )

//Alliances
spawner3.allyWithSpawner( spawner4 )
spawner7.allyWithSpawner( spawner8 )
spawner1.allyWithSpawner( spawner2 )
spawner5.allyWithSpawner( spawner6 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()
spawner4.forceSpawnNow()
spawner5.forceSpawnNow()
spawner6.forceSpawnNow()
spawner7.forceSpawnNow()
spawner8.forceSpawnNow()