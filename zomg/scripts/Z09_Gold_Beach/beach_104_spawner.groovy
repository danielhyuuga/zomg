//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner_104_1 = myRooms.Beach_104.spawnSpawner( "spawner_104_1", "water_spout", 1)
spawner_104_1.setPos( 805, 175 )
spawner_104_1.setHome( "Beach_104", 805, 175 )
spawner_104_1.setSpawnWhenPlayersAreInRoom( true )
spawner_104_1.setWaitTime( 30 , 40 )
spawner_104_1.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_104_1.childrenWander( true )
spawner_104_1.setMonsterLevelForChildren( 7.0 )

// Spawner two
spawner_104_2 = myRooms.Beach_104.spawnSpawner( "spawner_104_2", "water_spout", 1)
spawner_104_2.setPos( 705, 460 )
spawner_104_2.setHome( "Beach_104", 705, 460 )
spawner_104_2.setSpawnWhenPlayersAreInRoom( true )
spawner_104_2.setWaitTime( 30 , 40 )
spawner_104_2.setWanderBehaviorForChildren( 25, 80, 4, 8, 300 )
spawner_104_2.childrenWander( true )
spawner_104_2.setMonsterLevelForChildren( 7.0 )

spawner_104_1.stopSpawning()
spawner_104_2.stopSpawning()

//Alliances
spawner_104_1.allyWithSpawner( spawner_104_2 )

//Variable definition
spawnTime = false
spawnedSpout1 = false
spawnedSpout2 = false
spoutRespawning = false
spout_104_1 = null
spout_104_2 = null

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() } // ; //println "***** GST = ${ gst() } ******" }
}

def spawnChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) {
//	if( gst() > 0 ) { //This line for each setting of time parameters for testing.
		spawnTime = true
	} else {
		spawnTime = false
	}
}	

def checkForDespawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == false ) {
		clockChecker()
		if( spawnedSpout1 == true ) {
			if(spout_104_1.getHated().size() == 0) {
				spout_104_1.dispose()
				spawnedSpout1 = false
			}
		}
		if( spawnedSpout2 == true ) {
			if(spout_104_2.getHated().size() == 0) {
				spout_104_2.dispose()
				spawnedSpout2 = false
			}
		}
	}
	myManager.schedule(30) { checkForDespawn() }
}

def spawnOrNoSpawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == true ) {
		if( spawnedSpout1 == false ) {
			spout_104_1 = spawner_104_1.forceSpawnNow()
			spawnedSpout1 = true
			runOnDeath( spout_104_1 ) { 
				spawnedSpout1 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout2 == false ) {
			spout_104_2 = spawner_104_2.forceSpawnNow()
			spawnedSpout2 = true
			runOnDeath( spout_104_2 ) { 
				spawnedSpout2 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
	} 
}

//Initialization
clockChecker()
checkForDespawn()
