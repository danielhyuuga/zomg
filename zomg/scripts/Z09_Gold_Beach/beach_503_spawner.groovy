//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
def spawner1 = myRooms.Beach_503.spawnSpawner( "spawner_503_1", "sand_golem", 1)
spawner1.setPos( 270, 420 )
spawner1.setHome( "Beach_503", 270, 420 )
spawner1.setSpawnWhenPlayersAreInRoom( false )
spawner1.setWaitTime( 30 , 40 )
spawner1.setGuardPostForChildren( spawner1, 30 )
spawner1.setMonsterLevelForChildren( 6.7 )

// Spawner two
def spawner2 = myRooms.Beach_503.spawnSpawner( "spawner_503_2", "sand_golem", 1)
spawner2.setPos( 400, 240 )
spawner2.setHome( "Beach_503", 400, 240 )
spawner2.setSpawnWhenPlayersAreInRoom( false )
spawner2.setWaitTime( 30 , 40 )
spawner2.setGuardPostForChildren( spawner2, 30 )
spawner2.setMonsterLevelForChildren( 6.7 )

//Alliances
spawner1.allyWithSpawner( spawner2 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()