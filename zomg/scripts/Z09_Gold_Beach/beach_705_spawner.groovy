//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner one
spawner_705_1 = myRooms.Beach_705.spawnSpawner( "spawner_705_1", "water_spout", 1)
spawner_705_1.setPos( 220, 75 )
spawner_705_1.setHome( "Beach_705", 220, 75 )
spawner_705_1.setSpawnWhenPlayersAreInRoom( true )
spawner_705_1.setWaitTime( 30 , 40 )
spawner_705_1.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_705_1.childrenWander( true )
spawner_705_1.setMonsterLevelForChildren( 6.3 ) 

// Spawner two
spawner_705_2 = myRooms.Beach_705.spawnSpawner( "spawner_705_2", "water_spout", 1)
spawner_705_2.setPos( 170, 310 )
spawner_705_2.setHome( "Beach_705", 170, 310 )
spawner_705_2.setSpawnWhenPlayersAreInRoom( true )
spawner_705_2.setWaitTime( 30 , 40 )
spawner_705_2.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_705_2.childrenWander( true )
spawner_705_2.setMonsterLevelForChildren( 6.3 )

// Spawner three
spawner_705_3 = myRooms.Beach_705.spawnSpawner( "spawner_705_3", "water_spout", 1)
spawner_705_3.setPos( 370, 565 )
spawner_705_3.setHome( "Beach_705", 370, 565 )
spawner_705_3.setSpawnWhenPlayersAreInRoom( true )
spawner_705_3.setWaitTime( 30 , 40 )
spawner_705_3.setWanderBehaviorForChildren( 10, 60, 4, 8, 150 )
spawner_705_3.childrenWander( true )
spawner_705_3.setMonsterLevelForChildren( 6.3 )

spawner_705_1.stopSpawning()
spawner_705_2.stopSpawning()
spawner_705_3.stopSpawning()

//Alliances
spawner_705_1.allyWithSpawner( spawner_705_2 )
spawner_705_1.allyWithSpawner( spawner_705_3 )
spawner_705_2.allyWithSpawner( spawner_705_3 )

//Variable definition
spawnTime = false
spawnedSpout1 = false
spawnedSpout2 = false
spawnedSpout3 = false
spoutRespawning = false
spout_705_1 = null
spout_705_2 = null
spout_705_3 = null

def clockChecker() { 
	myManager.schedule(30) { spawnOrNoSpawn() } // ; //println "***** GST = ${ gst() } ******" }
}

def spawnChecker() {
	if( ( gst() > 1900 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 200 ) ) { //party goes from 7pm to 2am.
//	if( gst() > 0 ) { //This line for each setting of time parameters for testing.
		spawnTime = true
	} else {
		spawnTime = false
	}
}	

def checkForDespawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == false ) {
		clockChecker()
		if( spawnedSpout1 == true ) {
			if(spout_705_1.getHated().size() == 0) {
				spout_705_1.dispose()
				spawnedSpout1 = false
			}
		}
		if( spawnedSpout2 == true ) {
			if(spout_705_2.getHated().size() == 0) {
				spout_705_2.dispose()
				spawnedSpout2 = false
			}
		}
		if( spawnedSpout3 == true ) {
			if(spout_705_3.getHated().size() == 0) {
				spout_705_3.dispose()
				spawnedSpout3 = false
			}
		}
	}
	myManager.schedule(30) { checkForDespawn() }
}

def spawnOrNoSpawn() {
	spawnChecker()
	time = gst()
	//println "<<<<<< time = ${time} and spawnTime = ${spawnTime} and spawnedNPCs = ${spawnedNPCs} >>>>>>"	
	if( spawnTime == true ) {
		spoutRespawning = false
		if( spawnedSpout1 == false ) {
			spout_705_1 = spawner_705_1.forceSpawnNow()
			spawnedSpout1 = true
			runOnDeath( spout_705_1 ) { 
				spawnedSpout1 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout2 == false ) {
			spout_705_2 = spawner_705_2.forceSpawnNow()
			spawnedSpout2 = true
			runOnDeath( spout_705_2 ) { 
				spawnedSpout2 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
		if( spawnedSpout3 == false ) {
			spout_705_3 = spawner_705_3.forceSpawnNow()
			spawnedSpout3 = true
			runOnDeath( spout_705_3 ) { 
				spawnedSpout3 = false
				if(spoutRespawning == false) {
					myManager.schedule(150) { spawnOrNoSpawn() } 
					spoutRespawning = true
				}
			}
		}
	}
}

//Initialization
clockChecker()
checkForDespawn()