//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

def Jacques = spawnNPC("Jacques-VQS", myRooms.Beach_102, 300, 545)
Jacques.setDisplayName( "Jacques" )

onQuestStep( 93, 2 ) { event -> event.player.addMiniMapQuestActorName( "Jacques-VQS" ) } 

//---------------------------------------------------------------------
//Default                                                              
//---------------------------------------------------------------------
def jacquesDefault = Jacques.createConversation("jacquesDefault", true, "!QuestStarted_93", "!QuestCompleted_93", "!Z9_Jacques_Duneslam_Break", "!Z10_Blaze_ReturnToJacques")

def default1 = [id:1]
default1.npctext = "Yar, who ye be?"
default1.options = []
default1.options << [text:"I be %p. Who be ye?", result: 2]
default1.options << [text:"Nice accent... any relation to Barnacle Jim?", result: 3]
default1.options << [text:"Oh crap! Another pirate?", result: 4]
jacquesDefault.addDialog(default1, Jacques)

def default2 = [id:2]
default2.npctext = "There are those who call me... Jacques?"
default2.options = []
default2.options << [text:"Is Jacques your name or do people just call you that?", result: 5]
default2.options << [text:"Okay, Jacques, is this your light'ouse?", result: 6]
default2.options << [text:"What's going on out here?", result: 7]
jacquesDefault.addDialog(default2, Jacques)

def default3 = [id:3]
default3.npctext = "That lice-infested dog! He's 'ardly a sailor at all. Why if it weren't for this 'ere light'ouse he woulda run agroun' years ago!"
default3.options = []
default3.options << [text:"Lol... I'm not sure if that means you like him or not!", result: 8]
default3.options << [text:"Okay... anything exciting going on here?", result: 7]
jacquesDefault.addDialog(default3, Jacques)

def default4 = [id:4]
default4.npctext = "I'm n'pirate! I'm a sailor. Well... I was a sailor. Now I'm the keeper o' this 'ere light'ouse."
default4.options = []
default4.options << [text:"That sounds like the most boring job ever.", result: 9]
default4.options << [text:"So I guess you see everything around here... seen anything exciting?", result: 7]
jacquesDefault.addDialog(default4, Jacques)

def default5 = [id:5]
default5.npctext = "Me name actually be Jacques Emmanuel Renault-Kincaid. Jacques be fer short."
default5.options = []
default5.options << [text:"I can see why. Your full name is a block and a half long!", result: 10]
default5.options << [text:"Guess you wouldn't want to go by your initials, would you?", result: 11]
jacquesDefault.addDialog(default5, Jacques)

def default6 = [id:6]
default6.npctext = "Aye, this be my light'ouse... or I be this 'ere light'ouse's keeper. I'm not so sure which be more accurate."
default6.options = []
default6.options << [text:"Fascinating. Looks like you can see for miles... ever see anything unusual?", result: 7]
default6.options << [text:"Zzzzzzzzzz. Oh, I'm sorry... were you saying something incredibly boring?", result: 9]
jacquesDefault.addDialog(default6, Jacques)

def default7 = [id:7]
default7.npctext = "The life o' the light'ouse keeper is more excitin' than ye'd think. All sort o' tings be 'appenin' 'ere an' dere. Lately I been watchin' somethin' that's got me a might concerned. Jus' o'er yonder I been seein' one of them sand castle things agrowin' and agrowin'. Methink someone should knock it down fore it gets too big."
default7.playertext = "That sounds like a request."
default7.result = 12
jacquesDefault.addDialog(default7, Jacques)

def default8 = [id:8]
default8.npctext = "It's not that I dislike 'im... I jus' go no use fer 'im who claims to be a pirate but spends most 'is time on land."
default8.options = []
default8.options << [text:"Don't you spend most of your time on land too, though?", result: 13]
default8.options << [text:"He is a bit of an ass too.", result: 14]
jacquesDefault.addDialog(default8, Jacques)

def default9 = [id:9]
default9.npctext = "Listen ya goober, keepin' this light'ouse runnin' offers a lot more excitement than ye'd imagine."
default9.playertext = "Excitement? What kind of excitement?"
default9.result = 7
jacquesDefault.addDialog(default9, Jacques)

def default10 = [id:10]
default10.npctext = "Har! Yes, I guess it is a wee bit long... but it has character! So what can ole Jacques do ye for?"
default10.options = []
default10.options << [text:"Have any work that needs doing?", result: 7]
default10.options << [text:"Nothing for now. Bye.", result: DONE]
jacquesDefault.addDialog(default10, Jacques)

def default11 = [id:11]
default11.npctext = "Woo! I ne'er 'eard that one before. Yer one original prankster. Be ye 'ere for a purpose other than pokin' fun at me names?"
default11.options = []
default11.options << [text:"Yes, I need work. Got any?.", result: 7]
default11.options << [text:"I guess nothing for now. Cya.", result: DONE]
jacquesDefault.addDialog(default11, Jacques)

def default12 = [id:12]
default12.npctext = "Aye, if ye think yer up to it. This golem's not likely to be a pushover."
default12.options = []
default12.options << [text:"I'm up to it! Where can I find this beast?", result: 15]
default12.options << [text:"Let me think about it.", result: 16]
jacquesDefault.addDialog(default12, Jacques)

def default13 = [id:13]
default13.npctext = "What? Aye, suppose that be true but when I was 'is age I ne'er spent more than shore leave on land. Anyway, an ol' sailors yarns are not what y'came fer no doubt. What can Jacques do ye fer?"
default13.options = []
default13.options << [text:"I dunno, what's going on around here?", result: 7]
default13.options << [text:"Nothing... I think I'm gonna take off.", result: DONE]
jacquesDefault.addDialog(default13, Jacques)

def default14 = [id:14]
default14.npctext = "Oh, I see ye've met 'im. So did ye 'ave a reason fer coming up 'ere?"
default14.options = []
default14.options << [text:"I'm just looking to help. Need any?.", result: 7]
default14.options << [text:"Er, nope. Guess it's time I left.", result: DONE]
jacquesDefault.addDialog(default14, Jacques)

def default15 = [id:15]
default15.npctext = "Ah, the beast hides itself upon an island off the easterly shore o' the beach. Surely ye can find a boat and make yer way to 'im. Jus' be sure ye find one what has n'oles!"
default15.quest = 93
default15.result = DONE
jacquesDefault.addDialog(default15, Jacques)

def default16 = [id:16]
default16.npctext = "Yar. Ye don't think too long."
default16.flag = "Z9_Jacques_Duneslam_Break"
default16.result = DONE
jacquesDefault.addDialog(default16, Jacques)

//---------------------------------------------------------------------
//Duneslam break                                                       
//---------------------------------------------------------------------
def duneslamBreak = Jacques.createConversation("duneslamBreak", true, "Z9_Jacques_Duneslam_Break")

def duneslamBreak1 = [id:1]
duneslamBreak1.npctext = "Yer back. Ready to take on Duneslam?"
duneslamBreak1.options = []
duneslamBreak1.options << [text:"Yeah, I think I'm ready now. Point me at him... er, it!", result: 2]
duneslamBreak1.options << [text:"No. Not now.", result: 3]
duneslamBreak.addDialog(duneslamBreak1, Jacques)

def duneslamBreak2 = [id:2]
duneslamBreak2.npctext = "Ah, the beast hides itself upon an island off the easterly shore o' the beach. Surely ye can find a boat and make yer way to 'im. Jus' be sure ye find one what has n'oles!"
duneslamBreak2.quest = 93
duneslamBreak2.flag = "!Z9_Jacques_Duneslam_Break"
duneslamBreak2.result = DONE
duneslamBreak.addDialog(duneslamBreak2, Jacques)

def duneslamBreak3 = [id:3]
duneslamBreak3.npctext = "Then why'd ye come back at all?"
duneslamBreak3.result = DONE
duneslamBreak.addDialog(duneslamBreak3, Jacques)

//---------------------------------------------------------------------
//Duneslam active                                                      
//---------------------------------------------------------------------
def duneslamActive = Jacques.createConversation("duneslamActive", true, "QuestStarted_93:2")

def duneslamAtive1 = [id:1]
duneslamAtive1.npctext = "Do ye think me daft? I can easily see ye havena killed Duneslam yet!"
duneslamAtive1.result = DONE
duneslamActive.addDialog(duneslamAtive1, Jacques)

//---------------------------------------------------------------------
//Duneslam complete                                                    
//---------------------------------------------------------------------
def duneslamComplete = Jacques.createConversation("duneslamComplete", true, "QuestStarted_93:3")

def duneslamComplete1 = [id:1]
duneslamComplete1.npctext = "I saw ye drop the beast from 'ere! Ye fight well... are ye sure ye aren't a sailor?"
duneslamComplete1.options = []
duneslamComplete1.options << [text:"Probably not... I think I'd remember something like that.", result: 2]
duneslamComplete1.options << [text:"Heh, you never know, maybe I was in a previous life.", result: 3]
duneslamComplete.addDialog(duneslamComplete1, Jacques)

def duneslamComplete2 = [id:2]
duneslamComplete2.npctext = "Ah well, ye still 'andle yerself well."
duneslamComplete2.quest = 93
duneslamComplete2.exec = { event -> event.player.removeMiniMapQuestActorName("Jacques-VQS") }
duneslamComplete2.result = DONE
duneslamComplete.addDialog(duneslamComplete2, Jacques)

def duneslamComplete3 = [id:3]
duneslamComplete3.npctext = "Aye, I wouldn't be surprised at that. Ye 'andle yerself well."
duneslamComplete3.quest = 93
duneslamComplete3.exec = { event -> event.player.removeMiniMapQuestActorName("Jacques-VQS") }
duneslamComplete3.result = DONE
duneslamComplete.addDialog(duneslamComplete3, Jacques)

//---------------------------------------------------------------------
//Duneslam Finished Blaze                                              
//---------------------------------------------------------------------
def duneslamFinished = Jacques.createConversation("duneslamFinished", true, "QuestCompleted_93", "Z9_Jacques_GoSeeBlaze", "!Z10_Blaze_ReturnToJacques")

def duneslamFinished1 = [id:1]
duneslamFinished1.npctext = "'ey again! Sorry to be rude but I'm a bit busy with this 'ere light'ouse at the moment."
duneslamFinished1.result = DONE
duneslamFinished.addDialog(duneslamFinished1, Jacques)

//---------------------------------------------------------------------
//Duneslam Finished No Blaze                                           
//---------------------------------------------------------------------
def duneslamFinishedNoBlaze = Jacques.createConversation("duneslamFinishedNoBlaze", true, "QuestCompleted_93", "!QuestCompleted_94", "!Z10_Blaze_ReturnToJacques")

def duneslamFinishedNoBlaze1 = [id:1]
duneslamFinishedNoBlaze1.npctext = "'ey again! Sorry to be rude but I'm a bit busy with this 'ere light'ouse at the moment."
duneslamFinishedNoBlaze1.result = DONE
duneslamFinishedNoBlaze.addDialog(duneslamFinishedNoBlaze1, Jacques)

//---------------------------------------------------------------------
//Introducing Blaze                                                    
//---------------------------------------------------------------------
def introducingBlaze = Jacques.createConversation("introducingBlaze", true, "QuestCompleted_93", "QuestCompleted_94", "!Z9_Jacques_GoSeeBlaze", "!Z10_Blaze_ReturnToJacques")

def blaze1 = [id:1]
blaze1.npctext = "Ye have the look of someone with something important t'say. Well, spit it out!"
blaze1.playertext = "What do you know about the Animated?"
blaze1.result = 2
introducingBlaze.addDialog(blaze1, Jacques)

def blaze2 = [id:2]
blaze2.npctext = "All I knows is that they be most unnatural. For years I have manned this lighthouse, never did I see s'many crawlies... not until after the explosion."
blaze2.playertext = "What do you think it means?"
blaze2.result = 3
introducingBlaze.addDialog(blaze2, Jacques)

def blaze3 = [id:3]
blaze3.npctext = "Well, a sailor such as me knows little about the land or explosions... but about the same time the Animated started poppin' up I took to noticin' the brightest o' lights on the floor o' the sea. We sailors know that to be a bad omen."
blaze3.playertext = "Well it's certainly creepy."
blaze3.result = 4
introducingBlaze.addDialog(blaze3, Jacques)

def blaze4 = [id:4]
blaze4.npctext = "Aye, that it is. Now tell me, friendo, with what cause do ye be askin' such questions?"
blaze4.playertext = "I've been trying to locate the source of the Animated, but I have to admit I'm running out of ideas."
blaze4.result = 5
introducingBlaze.addDialog(blaze4, Jacques)

def blaze5 = [id:5]
blaze5.npctext = "And? That doens't explain why I find ye on me doorstep."
blaze5.playertext = "And I thought you might be able to point me in the right direction. I mean, here you are in what must be the prime vantage point overlooking ground zero of the explosion with nothing to do all day but watch what happens. Surely you know something!"
blaze5.result = 6
introducingBlaze.addDialog(blaze5, Jacques)

def blaze6 = [id:6]
blaze6.npctext = "Me friend Marshall would know better than I, but I be not one fer betrayin' the secrets o' others. If it be answers ye seek, ye'll be needin' to earn the trust of another afore ye get more out of me."
blaze6.options = []
blaze6.options << [text:"Alright, who is this person?", result: 7]
blaze6.options << [text:"Can't you just tell me where this Marshall is and I'll go see him myself?", result: 8]
introducingBlaze.addDialog(blaze6, Jacques)

def blaze7 = [id:7]
blaze7.npctext = "Marshall's assistant, Blaze. She's off on her own right now... down at the ruins southwest from here."
blaze7.playertext = "And once she trusts me you'll let me know what you know?"
blaze7.result = 10
introducingBlaze.addDialog(blaze7, Jacques)

def blaze8 = [id:8]
blaze8.npctext = "Nay, he be... busy with his work. He told me of his work in confidence and I'll not be breakin' that."
blaze8.playertext = "Alright, who do you want me to talk to then?"
blaze8.result = 7
introducingBlaze.addDialog(blaze8, Jacques)

def blaze10 = [id:10]
blaze10.npctext = "Once Blaze gives the okay I will tell ye everything I know."
blaze10.flag = "Z9_Jacques_GoSeeBlaze"
blaze10.result = DONE
introducingBlaze.addDialog(blaze10, Jacques)

//---------------------------------------------------------------------
//Coming from Blaze                                                    
//---------------------------------------------------------------------
def blazeIncoming = Jacques.createConversation("blazeIncoming", true, "Z10_Blaze_ReturnToJacques", "!Z9_Jacques_GoSeeBlaze", "!Z9_Jacques_MarshallGiven", "!Z9_Jacques_MarshallSaved", "!QuestStarted_93:3")

def blazeIncoming1 = [id:1]
blazeIncoming1.npctext = "Yar, who ye be?"
blazeIncoming1.options = []
blazeIncoming1.options << [text:"I be %p. Who be ye?", result: 2]
blazeIncoming1.options << [text:"Nice accent... any relation to Barnacle Jim?", result: 3]
blazeIncoming1.options << [text:"Oh crap! Another pirate?", result: 4]
blazeIncoming.addDialog(blazeIncoming1, Jacques)

def blazeIncoming2 = [id:2]
blazeIncoming2.npctext = "There are those who call me... Jacques?"
blazeIncoming2.options = []
blazeIncoming2.options << [text:"Is Jacques your name or do people just call you that?", result: 5]
blazeIncoming2.options << [text:"Okay, Jacques, is this your light'ouse?", result: 6]
blazeIncoming2.options << [text:"Jacques, I'm looking for Marshall", result: 7]
blazeIncoming.addDialog(blazeIncoming2, Jacques)

def blazeIncoming3 = [id:3]
blazeIncoming3.npctext = "That lice-infested dog! He's 'ardly a sailor at all. Why if it weren't for this 'ere light'ouse he woulda run agroun' years ago!"
blazeIncoming3.options = []
blazeIncoming3.options << [text:"Lol... I'm not sure if that means you like him or not!", result: 8]
blazeIncoming3.options << [text:"Anyway... I need to know where to find Marshall.", result: 7]
blazeIncoming.addDialog(blazeIncoming3, Jacques)

def blazeIncoming4 = [id:4]
blazeIncoming4.npctext = "I'm n'pirate! I'm a sailor. Well... I was a sailor. Now I'm the keeper o' this 'ere light'ouse."
blazeIncoming4.options = []
blazeIncoming4.options << [text:"That sounds like the most boring job ever.", result: 9]
blazeIncoming4.options << [text:"So I guess you see everything around here... I bet you can tell me where Marshall is.", result: 7]
blazeIncoming.addDialog(blazeIncoming4, Jacques)

def blazeIncoming5 = [id:5]
blazeIncoming5.npctext = "Me name actually be Jacques Emmanuel Renault-Kincaid. Jacques be fer short."
blazeIncoming5.options = []
blazeIncoming5.options << [text:"I can see why. Your full name is a block and a half long!", result: 10]
blazeIncoming5.options << [text:"Guess you wouldn't want to go by your initials, would you?", result: 11]
blazeIncoming.addDialog(blazeIncoming5, Jacques)

def blazeIncoming6 = [id:6]
blazeIncoming6.npctext = "Aye, this be my light'ouse... or I be this 'ere light'ouse's keeper. I'm not so sure which be more accurate."
blazeIncoming6.options = []
blazeIncoming6.options << [text:"Fascinating. I was told you're close friends with Marshall. Any idea where I can find him?", result: 7]
blazeIncoming6.options << [text:"Zzzzzzzzzz. Oh, I'm sorry... were you saying something incredibly boring?", result: 9]
blazeIncoming.addDialog(blazeIncoming6, Jacques)

def blazeIncoming7 = [id:7]
blazeIncoming7.npctext = "Marshall? Where ye be gettin' that name from? I'm not sure I should tell ye where he be... I was told in confidence and I am not one fer repeatin' the secrets o' others."
blazeIncoming7.playertext = "What if I told you Blaze said it was okay?"
blazeIncoming7.result = 12
blazeIncoming.addDialog(blazeIncoming7, Jacques)

def blazeIncoming8 = [id:8]
blazeIncoming8.npctext = "It's not that I dislike 'im... I jus' go no use fer 'im who claims to be a pirate but spends most 'is time on land."
blazeIncoming8.options = []
blazeIncoming8.options << [text:"Don't you spend most of your time on land too, though?", result: 13]
blazeIncoming8.options << [text:"He is a bit of an ass too.", result: 14]
blazeIncoming.addDialog(blazeIncoming8, Jacques)

def blazeIncoming9 = [id:9]
blazeIncoming9.npctext = "Listen ya goober, keepin' this light'ouse runnin' offers a lot more excitement than ye'd imagine."
blazeIncoming9.playertext = "Excitement? What kind of excitement?"
blazeIncoming9.result = 7
blazeIncoming.addDialog(blazeIncoming9, Jacques)

def blazeIncoming10 = [id:10]
blazeIncoming10.npctext = "Har! Yes, I guess it is a wee bit long... but it has character! So what can ole Jacques do ye for?"
blazeIncoming10.options = []
blazeIncoming10.options << [text:"Blaze sent me. I'm looking for Marshall.", result: 7]
blazeIncoming10.options << [text:"Nothing for now. Bye.", result: DONE]
blazeIncoming.addDialog(blazeIncoming10, Jacques)

def blazeIncoming11 = [id:11]
blazeIncoming11.npctext = "Woo! I ne'er 'eard that one before. Yer one original prankster. Be ye 'ere for a purpose other than pokin' fun at me names?"
blazeIncoming11.options = []
blazeIncoming11.options << [text:"Blaze told you know where to find Marshall.", result: 7]
blazeIncoming11.options << [text:"I guess nothing for now. Cya.", result: DONE]
blazeIncoming.addDialog(blazeIncoming11, Jacques)

def blazeIncoming12 = [id:12]
blazeIncoming12.npctext = "Yar, I suppose if Blaze were okay with it. How do ye know the girl?"
blazeIncoming12.playertext = "It's a really long story and at least half of it sounds like crazy talk. Let's just say I helped her out and she trusts me now."
blazeIncoming12.result = 15
blazeIncoming.addDialog(blazeIncoming12, Jacques)

def blazeIncoming13 = [id:13]
blazeIncoming13.npctext = "What? Aye, suppose that be true but when I was 'is age I ne'er spent more than shore leave on land. Anyway, an ol' sailors yarns are not what y'came fer no doubt. What can Jacques do ye fer?"
blazeIncoming13.options = []
blazeIncoming13.options << [text:"I heard you can tell me where to find Marshall.", result: 7]
blazeIncoming13.options << [text:"Nothing... I think I'm gonna take off.", result: DONE]
blazeIncoming.addDialog(blazeIncoming13, Jacques)

def blazeIncoming14 = [id:14]
blazeIncoming14.npctext = "Oh, I see ye've met 'im. So did ye 'ave a reason fer coming up 'ere?"
blazeIncoming14.options = []
blazeIncoming14.options << [text:"Actually, I'm looking for Marshall. I was told you'd know how to find him.", result: 7]
blazeIncoming14.options << [text:"Er, nope. Guess it's time I left.", result: DONE]
blazeIncoming.addDialog(blazeIncoming14, Jacques)

def blazeIncoming15 = [id:15]
blazeIncoming15.npctext = "Yar... I suppose ye would not even know she was in the ruins were yer story a lie. Truth be told, I am glad ye come lookin' fer Marshall... I've been worried about him fer some time."
blazeIncoming15.playertext = "Worried? Why would you be worried about Marshall? Surely he's a very experienced diver."
blazeIncoming15.result = 16
blazeIncoming.addDialog(blazeIncoming15, Jacques)

def blazeIncoming16 = [id:16]
blazeIncoming16.npctext = "Experienced diver, yes, but he be not a fighter. I be having feeling about the undersea lights and I fear Marshall may have run headlong into a danger he can't defend himself from. It's good that ye are lookin' for him... I suspect he may need yer help."
blazeIncoming16.playertext = "I'm certainly a better fighter than a diver. I'll try to track him down and offer any help I can. Where should I start looking?"
blazeIncoming16.result = 17
blazeIncoming.addDialog(blazeIncoming16, Jacques)

def blazeIncoming17 = [id:17]
blazeIncoming17.npctext = "Just look fer the glowin' lights under the sea at night. They can't be missed! What be down there... or where exactly Marshall may be are not answers I have. Ye shall have to find out for yeself."
blazeIncoming17.options = []
blazeIncoming17.options << [text:"Danger, mystery, and adventure... just what I'm looking for!", result: 18]
blazeIncoming17.options << [text:"Once more into the breach, I guess. *sigh*.", result: 19]
blazeIncoming.addDialog(blazeIncoming17, Jacques)

def blazeIncoming18 = [id:18]
blazeIncoming18.npctext = "Yar, the audacity of youth."
blazeIncoming18.flag = "Z9_Jacques_MarshallGiven"
blazeIncoming18.result = DONE
blazeIncoming.addDialog(blazeIncoming18, Jacques)

def blazeIncoming19 = [id:19]
blazeIncoming19.npctext = "Yar, I'm sure a youngin' such as ye shall find it enjoyable."
blazeIncoming19.flag = "Z9_Jacques_MarshallGiven"
blazeIncoming19.result = DONE
blazeIncoming.addDialog(blazeIncoming19, Jacques)

def blazeIncoming20 = [id:20]
blazeIncoming20.npctext = "No idea. Marshall be the first to dive that area since the explosion. There be no tellin' what be down there."
blazeIncoming20.options = []
blazeIncoming20.options << [text:"Wait, back up. What explosion?!", result: 21]
blazeIncoming20.options << [text:"Marshall should be fine. The explosion probably killed anything that was in the area.", result: 22]
blazeIncoming.addDialog(blazeIncoming20, Jacques)

def blazeIncoming21 = [id:21]
blazeIncoming21.npctext = "You mean you don't know? The Gambino fission took place just off the coast. That's where Marshall is diving!"
blazeIncoming21.playertext = "What?! Is he crazy? Why is he diving there?"
blazeIncoming21.result = 23
blazeIncoming.addDialog(blazeIncoming21, Jacques)

def blazeIncoming22 = [id:22]
blazeIncoming22.npctext = "Er... nay, I'm afraid not. Poor wording on my part, matey. Twasn't an explosion, Twas more like a big flash o' light or some kind o' energy release. Looked like an explosion, only wasn't, and nobody died. Well, nobody aside from that brute Labtech X... and twas probably the fall what killed him."
blazeIncoming22.playertext = "Alright, look, I'm starting to get a little freaked out. If you want me to go after Marshall, I need to know what I'm getting into. What did Marshall say about the area he was diving in?"
blazeIncoming22.result = 23
blazeIncoming.addDialog(blazeIncoming22, Jacques)

def blazeIncoming23 = [id:23]
blazeIncoming23.npctext = "One thing I learned a long time ago was to ne'er ask Marshall 'bout 'is business. All I 'eard was 'im mutterin' to 'imself 'bout that big flash-o-light and Gambino fishin'. Seems a might weird to me... Gambino don't seem the fishin' type and I've no idear what them two things be 'avin' in common."
blazeIncoming23.playertext = "Yeah, that doesn't really seem to make any sense... Maybe we can figure it out, though. Why don't you tell me more about the big flash? Seems like you had a great vantage point for the whole thing."
blazeIncoming23.result = 24
blazeIncoming.addDialog(blazeIncoming23, Jacques)

def blazeIncoming24 = [id:24]
blazeIncoming24.npctext = "Aye, that I did. It all took place on the cliffs not far from here. At first, twas just a gatherin' of young folks. Gino, I recognized him from than unmistakable Gambino mane, and friends, I might guess."
blazeIncoming24.options = []
blazeIncoming24.options << [text:"Any idea what they were doing?", result: 25]
blazeIncoming24.options << [text:"Kids? Uhm... I think you can skip ahead to the relevant parts of the story.", result: 26]
blazeIncoming.addDialog(blazeIncoming24, Jacques)

def blazeIncoming25 = [id:25]
blazeIncoming25.npctext = "What kids always do, n'doubt. Havin' fun, hangin' out, chillin'... whatever kids be callin' it these days. Or at least it started out that way. From the looks of it Gino and some other kid were 'bout to 'ave it out."
blazeIncoming25.playertext = "You mean they were going to fight? What about?"
blazeIncoming25.result = 27
blazeIncoming.addDialog(blazeIncoming25, Jacques)

def blazeIncoming26 = [id:26]
blazeIncoming26.npctext = "Calm yerself, I'm comin' to that! So there they was, Gino and another kid, makin' ready to fight when some labcoat wearin' giant of a man steps between 'em, pulls out a needle, and jams it into Gino's chest!"
blazeIncoming26.playertext = "OMG! Did it hurt Gino... poison him?"
blazeIncoming26.result = 28
blazeIncoming.addDialog(blazeIncoming26, Jacques)

def blazeIncoming27 = [id:27]
blazeIncoming27.npctext = "B'danged if I know. Ain't important anyway, 'fore they got into it some giant in a labcoat jumped in front-o-Gino, pulled out a needle, and stabbed Gino in the chest with it!"
blazeIncoming27.playertext = "Holy crap! What happened to Gino?! Did it kill him?"
blazeIncoming27.result = 28
blazeIncoming.addDialog(blazeIncoming27, Jacques)

def blazeIncoming28 = [id:28]
blazeIncoming28.npctext = "Hardly! I thought 'e was done for, but then outta nowhere the kid's arm grew into this massive mess-o-muscle and he grabbed that surly labtech around the waist with one hand! I swear 'is arm was 10 yard long if it were a foot!"
blazeIncoming28.options = []
blazeIncoming28.options << [text:"BULL! Do you think I'm crazy? There's no way I'd believe that story. This is madness!", result: 29]
blazeIncoming28.options << [text:"Yeesh, that's some shot! Bet that labtech wasn't expecting that!", result: 30]
blazeIncoming.addDialog(blazeIncoming28, Jacques)

def blazeIncoming29 = [id:29]
blazeIncoming29.npctext = "Yar, crazy as it may seem I assure ye I saw it wit' me own eyes. The kid grabbed the labtech and pulled 'im right over the cliff and into a water grave."
blazeIncoming29.playertext = "Grave? I thought you said Gino didn't die!"
blazeIncoming29.result = 31
blazeIncoming.addDialog(blazeIncoming29, Jacques)

def blazeIncoming30 = [id:30]
blazeIncoming30.npctext = "Nay, nor do I think he was expectin' Gino to pull 'im over the edge or the watery grave that awaited 'im at the bottom."
blazeIncoming30.playertext = "Grave? I thought you said Gino didn't die!"
blazeIncoming30.result = 31
blazeIncoming.addDialog(blazeIncoming30, Jacques)

def blazeIncoming31 = [id:31]
blazeIncoming31.npctext = "Ye misunderstood me. The labtech rests alone at the bottom o' the sea."
blazeIncoming31.options = []
blazeIncoming31.options << [text:"Oh... Well, I guess he had it coming for attacking a kid out of the blue. Any idea who he was?", result: 32]
blazeIncoming31.options << [text:"Oh... What happened to Gino, then?", result: 33]
blazeIncoming.addDialog(blazeIncoming31, Jacques)

def blazeIncoming32 = [id:32]
blazeIncoming32.npctext = "Yar, I heard 'e goes by X, Labtech X. A stranger name I 'ave ne'er 'eard."
blazeIncoming32.playertext = "That's a weird one indeed. So what became of Gino in all this?"
blazeIncoming32.result = 34
blazeIncoming.addDialog(blazeIncoming32, Jacques)

def blazeIncoming33 = [id:33]
blazeIncoming33.npctext = "Strangest thing... Huge flash of light like an explosion, and 'e washes up ashore with his dad, Johnny, who e'ryone thought to be dead!"
blazeIncoming33.playertext = "Everyone thought Johnny was dead but he wasn't. Okay. I get it. But, where was he and how did he wind up on shore with Gino? That doesn't make any sense... maybe the labtech was Johnny in disguise!"
blazeIncoming33.result = 35
blazeIncoming.addDialog(blazeIncoming33, Jacques)

def blazeIncoming34 = [id:34]
blazeIncoming34.npctext = "Well, ya remember that explosion... er... the one what didn't kill anyone? Soon as Labtech X and Gino hit the water it went off. Dang thing nearly blinded me! Few minute later'n Gino washes up ashore with his dad, Johnny, who e'ryone thought to be dead!"
blazeIncoming34.playertext = "But where did Johnny come from? Everyone thought he was dead and he just happens to wash up on the beach with Gino after all that happens? It doesn't make any sense."
blazeIncoming34.result = 36
blazeIncoming.addDialog(blazeIncoming34, Jacques)

def blazeIncoming35 = [id:35]
blazeIncoming35.npctext = "Nay, I heard the big guy went by the name o' X, or Labtech X, and used to work for Gambino. People used to see 'em together all the time."
blazeIncoming35.playertext = "Then where did Johnny come from? Everyone thought he was dead and he just happens to wash up on the beach with Gino after all that happens? It doesn't make any sense."
blazeIncoming35.result = 36
blazeIncoming.addDialog(blazeIncoming35, Jacques)

def blazeIncoming36 = [id:36]
blazeIncoming36.npctext = "No idea, matey. Marshall seemed to think 'e was gonna find an answer to that and many other questions at the bottom o' the sea. Which reminds me, ye were lookin' fer 'im... weren't ye?"
blazeIncoming36.playertext = "Yeah, I was. How can I find his dive site?"
blazeIncoming36.result = 17
blazeIncoming.addDialog(blazeIncoming36, Jacques)

//---------------------------------------------------------------------
//Returning from Blaze                                                 
//---------------------------------------------------------------------
def blazeComplete = Jacques.createConversation("blazeComplete", true, "Z10_Blaze_ReturnToJacques", "Z9_Jacques_GoSeeBlaze", "!Z9_Jacques_MarshallGiven", "!Z9_Jacques_MarshallSaved", "!QuestStarted_93:3")

def blazeComplete1 = [id:1]
blazeComplete1.npctext = "Yar, I see ye return. Did ye speak with Blaze?"
blazeComplete1.playertext = "Yup, she says it's alright to tell me where Marshall is."
blazeComplete1.result = 2
blazeComplete.addDialog(blazeComplete1, Jacques)

def blazeComplete2 = [id:2]
blazeComplete2.npctext = "This be glad news indeed. Truth be told, I am glad ye come lookin' fer Marshall. I be having bad feelin's about the area he went divin' and I be more'n a might worried fer his safety."
blazeComplete2.playertext = "I thought Marshall was the most experienced diver in Gaia... why would you be worried about him?"
blazeComplete2.result = 3
blazeComplete.addDialog(blazeComplete2, Jacques)

def blazeComplete3 = [id:3]
blazeComplete3.npctext = "Tis true, thar be narry a diver more experienced 'n ol' Marshall, but even so... divin' in that area may 'ave risks he's not familiar with."
blazeComplete3.playertext = "Risks? Like what? Is the area dangerous?"
blazeComplete3.result = 7
blazeComplete.addDialog(blazeComplete3, Jacques)

def blazeComplete4 = [id:4]
blazeComplete4.npctext = "Just look fer the glowin' lights under the sea at night. They can't be missed! What be down there... or where exactly Marshall may be are not answers I have. Ye shall have to find out for yeself."
blazeComplete4.options = []
blazeComplete4.options << [text:"Danger, mystery, and adventure... just what I'm looking for!", result: 5]
blazeComplete4.options << [text:"Once more into the breach, I guess. *sigh*.", result: 6]
blazeComplete.addDialog(blazeComplete4, Jacques)

def blazeComplete5 = [id:5]
blazeComplete5.npctext = "Yar, the audacity of youth."
blazeComplete5.flag = "Z9_Jacques_MarshallGiven"
blazeComplete5.result = DONE
blazeComplete.addDialog(blazeComplete5, Jacques)

def blazeComplete6 = [id:6]
blazeComplete6.npctext = "Yar, I'm sure a youngin' such as ye shall find it enjoyable."
blazeComplete6.flag = "Z9_Jacques_MarshallGiven"
blazeComplete6.result = DONE
blazeComplete.addDialog(blazeComplete6, Jacques)

def blazeComplete7 = [id:7]
blazeComplete7.npctext = "No idea. Marshall be the first to dive that area since the explosion. There be no tellin' what be down there."
blazeComplete7.options = []
blazeComplete7.options << [text:"Wait, back up. What explosion?!", result: 8]
blazeComplete7.options << [text:"Marshall should be fine. The explosion probably killed anything that was in the area.", result: 9]
blazeComplete.addDialog(blazeComplete7, Jacques)

def blazeComplete8 = [id:8]
blazeComplete8.npctext = "You mean you don't know? The Gambino fission took place just off the coast. That's where Marshall is diving!"
blazeComplete8.playertext = "What?! Is he crazy? Why is he diving there?"
blazeComplete8.result = 10
blazeComplete.addDialog(blazeComplete8, Jacques)

def blazeComplete9 = [id:9]
blazeComplete9.npctext = "Er... nay, I'm afraid not. Poor wording on my part, matey. Twasn't an explosion, Twas more like a big flash o' light or some kind o' energy release. Looked like an explosion, only wasn't, and nobody died. Well, nobody aside from that brute Labtech X... and twas probably the fall what killed him."
blazeComplete9.playertext = "Alright, look, I'm starting to get a little freaked out. If you want me to go after Marshall, I need to know what I'm getting into. What did Marshall say about the area he was diving in?"
blazeComplete9.result = 10
blazeComplete.addDialog(blazeComplete9, Jacques)

def blazeComplete10 = [id:10]
blazeComplete10.npctext = "One thing I learned a long time ago was to ne'er ask Marshall 'bout 'is business. All I 'eard was 'im mutterin' to 'imself 'bout that big flash-o-light and Gambino fishin'. Seems a might weird to me... Gambino don't seem the fishin' type and I've no idear what them two things be 'avin' in common."
blazeComplete10.playertext = "Yeah, that doesn't really seem to make any sense... Maybe we can figure it out, though. Why don't you tell me more about the big flash? Seems like you had a great vantage point for the whole thing."
blazeComplete10.result = 11
blazeComplete.addDialog(blazeComplete10, Jacques)

def blazeComplete11 = [id:11]
blazeComplete11.npctext = "Aye, that I did. It all took place on the cliffs not far from here. At first, twas just a gatherin' of young folks. Gino, I recognized him from than unmistakable Gambino mane, and friends, I might guess."
blazeComplete11.options = []
blazeComplete11.options << [text:"Any idea what they were doing?", result: 12]
blazeComplete11.options << [text:"Kids? Uhm... I think you can skip ahead to the relevant parts of the story.", result: 13]
blazeComplete.addDialog(blazeComplete11, Jacques)

def blazeComplete12 = [id:12]
blazeComplete12.npctext = "What kids always do, n'doubt. Havin' fun, hangin' out, chillin'... whatever kids be callin' it these days. Or at least it started out that way. From the looks of it Gino and some other kid were 'bout to 'ave it out."
blazeComplete12.playertext = "You mean they were going to fight? What about?"
blazeComplete12.result = 14
blazeComplete.addDialog(blazeComplete12, Jacques)

def blazeComplete13 = [id:13]
blazeComplete13.npctext = "Calm yerself, I'm comin' to that! So there they was, Gino and another kid, makin' ready to fight when some labcoat wearin' giant of a man steps between 'em, pulls out a needle, and jams it into Gino's chest!"
blazeComplete13.playertext = "OMG! Did it hurt Gino... poison him?"
blazeComplete13.result = 15
blazeComplete.addDialog(blazeComplete13, Jacques)

def blazeComplete14 = [id:14]
blazeComplete14.npctext = "B'danged if I know. Ain't important anyway, 'fore they got into it some giant in a labcoat jumped in front-o-Gino, pulled out a needle, and stabbed Gino in the chest with it!"
blazeComplete14.playertext = "Holy crap! What happened to Gino?! Did it kill him?"
blazeComplete14.result = 15
blazeComplete.addDialog(blazeComplete14, Jacques)

def blazeComplete15 = [id:15]
blazeComplete15.npctext = "Hardly! I thought 'e was done for, but then outta nowhere the kid's arm grew into this massive mess-o-muscle and he grabbed that surly labtech around the waist with one hand! I swear 'is arm was 10 yard long if it were a foot!"
blazeComplete15.options = []
blazeComplete15.options << [text:"BULL! Do you think I'm crazy? There's no way I'd believe that story. This is madness!", result: 16]
blazeComplete15.options << [text:"Yeesh, that's some shot! Bet that labtech wasn't expecting that!", result: 17]
blazeComplete.addDialog(blazeComplete15, Jacques)

def blazeComplete16 = [id:16]
blazeComplete16.npctext = "Yar, crazy as it may seem I assure ye I saw it wit' me own eyes. The kid grabbed the labtech and pulled 'im right over the cliff and into a water grave."
blazeComplete16.playertext = "Grave? I thought you said Gino didn't die!"
blazeComplete16.result = 18
blazeComplete.addDialog(blazeComplete16, Jacques)

def blazeComplete17 = [id:17]
blazeComplete17.npctext = "Nay, nor do I think he was expectin' Gino to pull 'im over the edge or the watery grave that awaited 'im at the bottom."
blazeComplete17.playertext = "Grave? I thought you said Gino didn't die!"
blazeComplete17.result = 18
blazeComplete.addDialog(blazeComplete17, Jacques)

def blazeComplete18 = [id:18]
blazeComplete18.npctext = "Ye misunderstood me. The labtech rests alone at the bottom o' the sea."
blazeComplete18.options = []
blazeComplete18.options << [text:"Oh... Well, I guess he had it coming for attacking a kid out of the blue. Any idea who he was?", result: 19]
blazeComplete18.options << [text:"Oh... What happened to Gino, then?", result: 20]
blazeComplete.addDialog(blazeComplete18, Jacques)

def blazeComplete19 = [id:19]
blazeComplete19.npctext = "Yar, I heard 'e goes by X, Labtech X. A stranger name I 'ave ne'er 'eard."
blazeComplete19.playertext = "That's a weird one indeed. So what became of Gino in all this?"
blazeComplete19.result = 21
blazeComplete.addDialog(blazeComplete19, Jacques)

def blazeComplete20 = [id:20]
blazeComplete20.npctext = "Strangest thing... Huge flash of light like an explosion, and 'e washes up ashore with his dad, Johnny, who e'ryone thought to be dead!"
blazeComplete20.playertext = "Everyone thought Johnny was dead but he wasn't. Okay. I get it. But, where was he and how did he wind up on shore with Gino? That doesn't make any sense... maybe the labtech was Johnny in disguise!"
blazeComplete20.result = 22
blazeComplete.addDialog(blazeComplete20, Jacques)

def blazeComplete21 = [id:21]
blazeComplete21.npctext = "Well, ya remember that explosion... er... the one what didn't kill anyone? Soon as Labtech X and Gino hit the water it went off. Dang thing nearly blinded me! Few minute later'n Gino washes up ashore with his dad, Johnny, who e'ryone thought to be dead!"
blazeComplete21.playertext = "But where did Johnny come from? Everyone thought he was dead and he just happens to wash up on the beach with Gino after all that happens? It doesn't make any sense."
blazeComplete21.result = 23
blazeComplete.addDialog(blazeComplete21, Jacques)

def blazeComplete22 = [id:22]
blazeComplete22.npctext = "Nay, I heard the big guy went by the name o' X, or Labtech X, and used to work for Gambino. People used to see 'em together all the time."
blazeComplete22.playertext = "Then where did Johnny come from? Everyone thought he was dead and he just happens to wash up on the beach with Gino after all that happens? It doesn't make any sense."
blazeComplete22.result = 23
blazeComplete.addDialog(blazeComplete22, Jacques)

def blazeComplete23 = [id:23]
blazeComplete23.npctext = "No idea, matey. Marshall seemed to think 'e was gonna find an answer to that and many other questions at the bottom o' the sea. Which reminds me, ye were lookin' fer 'im... weren't ye?"
blazeComplete23.playertext = "Yeah, I was. How can I find his dive site?"
blazeComplete23.result = 4
blazeComplete.addDialog(blazeComplete23, Jacques)

//---------------------------------------------------------------------
//Player returned from Blaze and spoke about Marshall                  
//---------------------------------------------------------------------
def jacquesMarshallGiven = Jacques.createConversation("jacquesMarshallGiven", true, "Z9_Jacques_MarshallGiven", "!Z9_Jacques_MarshallSaved", "!QuestStarted_93:3")

def jacquesMarshallGiven1 = [id:1]
jacquesMarshallGiven1.npctext = "Marshall needs savin' yet ye 'ang about. What is it ye be waitin' for?"
jacquesMarshallGiven1.options = []
jacquesMarshallGiven1.options << [text:"I forgot where Marshall was.", result: 2]
jacquesMarshallGiven1.options << [text:"I really can't help Marshall right now. What else is going on?", result: 3]
jacquesMarshallGiven.addDialog(jacquesMarshallGiven1, Jacques)

def jacquesMarshallGiven2 = [id:2]
jacquesMarshallGiven2.npctext = "As I told ye afore, look for glowing lights under the sea. Marshall was headed to investigate them."
jacquesMarshallGiven2.playertext = "Ahh, right, the sea. Thanks, Jacques."
jacquesMarshallGiven2.result = DONE
jacquesMarshallGiven.addDialog(jacquesMarshallGiven2, Jacques)

def jacquesMarshallGiven3 = [id:3]
jacquesMarshallGiven3.npctext = "What?! Ye plan to leave me friend to fend for 'imself? Begone, and do not return until ye have rescued Marshall!"
jacquesMarshallGiven3.playertext = "Alright, Jacques. I hear you."
jacquesMarshallGiven3.result = DONE
jacquesMarshallGiven.addDialog(jacquesMarshallGiven3, Jacques)

//---------------------------------------------------------------------
//Player has rescued Marshall                                          
//---------------------------------------------------------------------
def jacquesMarshallSaved = Jacques.createConversation("jacquesMarshallSaved", true, "!QuestStarted_93", "!QuestCompleted_93", "Z9_Jacques_MarshallSaved")

def jacquesMarshallSaved1 = [id:1]
jacquesMarshallSaved1.npctext = "I hear ye saved Marshall. Nay, not just Marshall, all o' Gaia it be seemin'. Methinks ol' Jacques be a bit anti-climactic in askin', but do ye think ye could perform one more heroic deed?"
jacquesMarshallSaved1.options = []
jacquesMarshallSaved1.options << [text:"Possibly. What did you have in mind?", result: 2]
jacquesMarshallSaved1.options << [text:"Not right now Jacques.", result: 3]
jacquesMarshallSaved.addDialog(jacquesMarshallSaved1, Jacques)

def jacquesMarshallSaved2 = [id:2]
jacquesMarshallSaved2.npctext = "Lately I been watchin' somethin' that's got me a might concerned. Jus' o'er yonder I been seein' one of them sand castle things agrowin' and agrowin'. Methink someone should knock it down fore it gets too big."
jacquesMarshallSaved2.options =[]
jacquesMarshallSaved2.options << [text:"I can give it a shot. Can't be nearly as hard as Labtech X!", result: 4]
jacquesMarshallSaved2.options << [text:"Maybe later, Jacques.", result: 3]
jacquesMarshallSaved.addDialog(jacquesMarshallSaved2, Jacques)

def jacquesMarshallSaved3 = [id:3]
jacquesMarshallSaved3.npctext = "Come back if ye change your mind."
jacquesMarshallSaved3.result = DONE
jacquesMarshallSaved.addDialog(jacquesMarshallSaved3, Jacques)

def jacquesMarshallSaved4 = [id:4]
jacquesMarshallSaved4.npctext = "Exceptional. Ye can find the beast upon an island off the easterly shore o' the beach. Surely ye can find a boat and make yer way to 'im. Jus' be sure ye find one what has n'oles!."
jacquesMarshallSaved4.quest = 93
jacquesMarshallSaved4.result = DONE
jacquesMarshallSaved.addDialog(jacquesMarshallSaved4, Jacques)

//---------------------------------------------------------------------
//Jacques End                                                          
//---------------------------------------------------------------------
def jacquesEnd = Jacques.createConversation("jacquesEnd", true, "QuestCompleted_93", "Z9_Jacques_MarshallSaved")

def jacquesEnd1 = [id:1]
jacquesEnd1.npctext = "'ey again! Sorry to be rude but I'm a bit busy with this 'ere light'ouse at the moment."
jacquesEnd1.result = DONE
jacquesEnd.addDialog(jacquesEnd1, Jacques)