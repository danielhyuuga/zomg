import com.gaiaonline.mmo.battle.script.*;

birthdayFluffMap = new HashMap()

//PLAQUE 1 - QUOTES
plaque1 = makeSwitch( "plaque1", myRooms.Bar_1, 120, 405 )

plaque1.unlock()
plaque1.setRange( 200 )

def readPlaque1 = { event ->
	event.actor.setQuestFlag( GLOBAL, "Z24ReadPlaqueOne" )
	tutorialNPC.pushDialog( event.actor, "plaqueOne" )
}
	
plaque1.whenOn( readPlaque1 )

def plaqueOne = tutorialNPC.createConversation( "plaqueOne", true )

def pOne1 = [id:1]
pOne1.npctext = "<h1><b><font face='Arial' size='14'>The Quote List</font></b></h1><font face='Arial' size ='12'><p></p>Women are just more difficult. -- Marina, 5/14/07<p></p><p></p>This editor is like capitalism. Not a great system, but better than the others. -- Jon, 9/19/07<p></p><p></p>Let me grab Russ's stuff and merge with him. -- Ryan, 1/18/08<p></p><p></p>Hey. My switchbox are pretty. -- Fred 2/7/08<p></p><p></p>I'm a golden god and I'm full of delicious cow -- Garrett 5/22/08<p></p><p></p>We are too overly aggressively anti-de-duping. -- Ryan 8/1/08<p></p><p></p>I'm in trouble now. The Russians are correcting my spelling -- Dave 9/16/08<p></p><p></p>Aaagggh. rsync you stupid *%@#! -- Kaia 9/18/08<p></p><p></p>I just use the old avatar to get sex -- RC 9/29/08<p></p><p></p>Rings don't work on balls -- Dave, 10/9/08<br><br>"
pOne1.result = DONE
plaqueOne.addDialog( pOne1, tutorialNPC )

//PLAQUE 2 - QA
plaque2 = makeSwitch( "plaque2", myRooms.Bar_1, 290, 690 )

plaque2.unlock()
plaque2.setRange( 200 )

def readPlaque2 = { event ->
	event.actor.setQuestFlag( GLOBAL, "Z24ReadPlaqueTwo" )
	tutorialNPC.pushDialog( event.actor, "plaqueTwo" )
}
	
plaque2.whenOn( readPlaque2 )

def plaqueTwo = tutorialNPC.createConversation( "plaqueTwo", true )

def pTwo1 = [id:1]
pTwo1.npctext = "<h1><b><font face='Arial' size='14'>Quality Assurance</font></b></h1><font face='Arial' size ='12'><p></p><b>QA Manager</b><p></p>Marina Leitman (Naxash)<p></p><p></p><b>QA Team</b><p></p>Thomas Aftowicz (TAftowicz)<p></p>Kevin Basso (WastedAmmo)<p></p>John Bealkowski (testtoon)<p></p>Veronica Carrillo (Loie)<p></p>Madhavi Ghanta (coolangel08)<p></p>Richard Fleming (ShadowDragonz)<p></p>Mark Los (mlos)<p></p>Jonathan Ruiz (mouth kick ouch)<p></p>Stan Sultanov (stansult)<p></p>Evgeniy Vorobiev (Kuzuan)<br><br>"
pTwo1.result = DONE
plaqueTwo.addDialog( pTwo1, tutorialNPC )

//PLAQUE 3 - External Contractors
plaque3 = makeSwitch( "plaque3", myRooms.Bar_1, 610, 170 )

plaque3.unlock()
plaque3.setRange( 200 )

def readPlaque3 = { event ->
	event.actor.setQuestFlag( GLOBAL, "Z24ReadPlaqueThree" )
	tutorialNPC.pushDialog( event.actor, "plaqueThree" )
}
	
plaque3.whenOn( readPlaque3 )

def plaqueThree = tutorialNPC.createConversation( "plaqueThree", true )

def pThree1 = [id:1]
pThree1.npctext = "<h1><b><font face='Arial' size='14'>External Contributors</font></b></h1><font face='Arial' size ='12'><p></p><b>Ring Artwork</b><p></p>John Su (Bietol)<p></p><p></p><b>Music (Artistry Entertainment)</b><p></p>Jeremy Soule<p></p>Julian Soule<p></p><p></p><b>Sound Effects (dSonic)</b><p></p>Kemal Amarasingham (Dsonicka)<p></p>Simon Amarasingham (Imur Ismon)<p></p>Liz Bailey<p></p>Michael Carter<p></p>Luke Gilbert<p></p><p></p><p></p><b>External Programming (Symblaze)</b><p></p>Jirka Petvaldsky<p></p>Petr Pravda<p></p>Stanislav Zorjan - Stasha<p></p><p></p><b>External Art Assets</b><p></p>Vykarian Studios<br><br>"
pThree1.result = DONE
plaqueThree.addDialog( pThree1, tutorialNPC )

//PLAQUE 4 - Marketing & Execs
plaque4 = makeSwitch( "plaque4", myRooms.Bar_1, 1120, 160 )

plaque4.unlock()
plaque4.setRange( 200 )

def readPlaque4 = { event ->
	event.actor.setQuestFlag( GLOBAL, "Z24ReadPlaqueFour" )
	tutorialNPC.pushDialog( event.actor, "plaqueFour" )
}
	
plaque4.whenOn( readPlaque4 )

def plaqueFour = tutorialNPC.createConversation( "plaqueFour", true )

def pFour1 = [id:1]
pFour1.npctext = "<h1><b><font face='Arial' size='14'>Marketing and<p></p>Exec Team</font></b></h1><font face='Arial' size ='12'><p></p><b>Marketing</b><p></p>Rie Hirabaru<p></p>John Deyto<p></p>Chris Castagnetto<p></p>Melissa Rische<p></p><p></p><b>TriplePoint</b><p></p>Kate Pietrelli<p></p><p></p><b>Executive Team</b><p></p>Derek Liu (lanzer)<p></p>Scott Kinzie<p></p>Prakash Ramamurthy<p></p>Craig Sherman<p></p><br><br>"
pFour1.result = DONE
plaqueFour.addDialog( pFour1, tutorialNPC )

//PLAQUE 5 - Internal (non-dev team) Developers
plaque5 = makeSwitch( "plaque5", myRooms.Bar_1, 1330, 575 )

plaque5.unlock()
plaque5.setRange( 200 )

def readPlaque5 = { event ->
	event.actor.setQuestFlag( GLOBAL, "Z24ReadPlaqueFive" )
	tutorialNPC.pushDialog( event.actor, "plaqueFive" )
}
	
plaque5.whenOn( readPlaque5 )

def plaqueFive = tutorialNPC.createConversation( "plaqueFive", true )

def pFive1 = [id:1]
pFive1.npctext = "<h1><b><font face='Arial' size='14'>Other Gaia Developers</font></b></h1><font face='Arial' size ='12'><p></p><b>Additional Artists</b><p></p>Josh Gainsbrugh (l0cke)<p></p>Josh Barnett (kirbyUFO)<p></p>Marissa Saradpon (crazy spork i am)<p></p>Elda The (ethe)<p></p>Jeannie Lee (Juno)<p></p>Philana Chow<p></p><p></p><b>Additional Flash Engineering</b><p></p>Mark Rubin (Mr_Ubin)<p></p>(Thanks, Mark. Fred would have imploded without you!)<p></p><p></p><b>Other Gaia Engineers</b><p></p>Tim Lopez (pangrammic)<p></p>Vince Rubino (smokeytheb)<p></p>Ernie Chan (Eazzy)<p></p>Nick Esquerra (ramoneguru)<p></p>Kathryn Koehler (8elly8eans)<p></p>Ania Kurek (ania1899)<p></p>Tom Lang (excitom)<p></p>Lawrence Lee (vryhngry)<p></p>John Loehrer ([72])<p></p>Rich Martin (ariesboy571)<p></p>Filipe Medeiros (Fleep)<p></p>Edy Moreno (edy)<p></p>Jon New (Newtang)<p></p>Charles Packer (chuckp2)<p></p>Dan Quinlivan (DARKNRGY)<p></p>Mark Ruiz (FFFFEEF)<p></p>Martial Tipsey (Narumi Misuhara)<p></p>Jennifer Tsai (fieryange1)<p></p>Xiaoming (stats god)<br><br>"
pFive1.result = DONE
plaqueFive.addDialog( pFive1, tutorialNPC )


//------------------------------------------
// DEV TEAM SPAWNERS                        
//------------------------------------------

// TODO: Add Flanagan

//====DESIGN====
Dave = spawnNPC( "Qixter", myRooms.Bar_1, 695, 530 )
Dave.setRotation( 45 )
Dave.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/qixter_strip.png")

Garrett = spawnNPC( "bronstahd", myRooms.Bar_1, 560, 605 )
Garrett.setRotation( 45 )

//====CODE====

Ryan = spawnNPC( "swarf", myRooms.Bar_1, 1090, 535 )

Ben = spawnNPC( "halzy", myRooms.Bar_1, 1180, 730 )
Ben.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/halzy_strip.png")
MrFluffSpawner = myRooms.Bar_1.spawnStoppedSpawner( "mr_fluff_spawner", "mrfluff", 20 )
MrFluffSpawner.setPos( 1180, 730 )
MrFluffSpawner.setMonsterLevelForChildren( 20 )

Fred = spawnNPC( "flajeu", myRooms.Bar_1, 1215, 600 )

Jon = spawnNPC( "aiagreat", myRooms.Bar_1, 1070, 680 )
Jon.setRotation( 45 )
Jon.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/aiagreat_strip.png")

Kaia = spawnNPC( "kaia9", myRooms.Bar_1, 1065, 275 )
Kaia.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/kaia9_strip.png")

//====ART====
JohnK = spawnNPC( "[ JK ]", myRooms.Bar_1, 760, 880 )
JohnK.setRotation( 45 )
JohnK.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/jk_strip.png")

Bret = spawnNPC( "Mavdoc", myRooms.Bar_1, 670, 320 )
Bret.setRotation( 45 )

Tabitha = spawnNPC( "pepper-tea", myRooms.Bar_1, 470, 400 )
Tabitha.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/peppertea_strip.png")

Theresa = spawnNPC( "reapersun", myRooms.Bar_1, 340, 470 )
Theresa.setRotation( 45 )
Theresa.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/reapersun_strip.png")

Alex = spawnNPC( "o_8", myRooms.Bar_1, 940, 235 )
Alex.setRotation( 45 )
Alex.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/o_8_strip.png")

Jen = spawnNPC( "jenzee", myRooms.Bar_1, 550, 950 )
Jen.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/jenzee_strip.png")

Andre = spawnNPC( "Your-Fathers-Belt", myRooms.Bar_1, 415, 875 )

Justin = spawnNPC( "fulltimefailure", myRooms.Bar_1, 1170, 310 )
Justin.setURL("http://s2.cdn.gaiaonline.com/images/zomg/bar/fulltimefailure_strip.png")

//====EXTERNAL CONTRACTORS====
Simon = spawnNPC( "Imur Ismon", myRooms.Bar_1, 1420, 390 )
Simon.setRotation( 135 )

Kemal = spawnNPC( "Dsonicka", myRooms.Bar_1, 1310, 390 )
Kemal.setRotation( 45 )


//------------------------------------------
// HUZZAH SCRIPT (after the Sealab Battle)  
//------------------------------------------

myManager.onEnter( myRooms.Bar_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z24ChapterEndRingGrantInProgress" )
		
		if( event.actor.hasQuestFlag( GLOBAL, "Z13BossDead" ) ) { 
			event.actor.unsetQuestFlag( GLOBAL, "Z24ChapterEndRingGrantInProgress" )

			//event.actor.unsetQuestFlag( GLOBAL, "Z13BossDead" ) //DEBUG ONLY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

			Logan = spawnNPC("[NPC] Old Man Logan", myRooms.Bar_1, 950, 680 )
			Logan.setDisplayName( "Logan" )

			Agatha = spawnNPC("Agatha-VQS", myRooms.Bar_1, 800, 640 )
			Agatha.setRotation( 0 )
			Agatha.setDisplayName( "Agatha" )

			Kin = spawnNPC( "Kin No Mask-VQS", myRooms.Bar_1, 680, 710 )
			Kin.setRotation( 45 )
			Kin.setDisplayName( "Kin" )

			Leon = spawnNPC("Leon-VQS", myRooms.Bar_1, 570, 790 )
			Leon.setRotation( 45 )
			Leon.setDisplayName( "Leon" )

			devCheers()

			myManager.schedule(11) { leonPart(); myManager.schedule(1) { event.actor.setQuestFlag( GLOBAL, "Z24LeonTalkNow" ); Leon.pushDialog( event.actor, "leonTalk" ) } }

		} else {
			//event.actor.setQuestFlag( GLOBAL, "Z13BossDead" ) // DEBUG ONLY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			//myManager.schedule(4) { Dave.say( "Welcome to the Barton Bar! I'm Qixter, and these folks around the bar are the folks that built zOMG!" ) }
			//myManager.schedule(7) { Dave.say( "Stop on by and say howdy to them if you're interested in hearing a bit about them!") }

			activateTeamComments()
		}		
	}
}

barReset = true

def devCheers() {
	if( barReset == true ) {
		barReset = false
		myManager.schedule(2) {
			Dave.say( "Woohoo!" )
			Ben.say( "You won!" )
			JohnK.say( "All bow down!" )
			Kemal.say( "You saved us all!" )
		}

		myManager.schedule(3) {
			Theresa.say( "You did it!" )
			Justin.say( "Awesome!" )
		}

		myManager.schedule(4) {
			Ryan.say( "r0xx0rz!" )
			Jon.say( "Nice job!" )
		}

		myManager.schedule(5) {
			Kaia.say( "Fantastic!" )
			Andre.say( "You're the best" )
			Tabitha.say( "Congrats!" )
		}

		myManager.schedule(6) {
			Garrett.say( "Oh yeah!" )
			Fred.say( "Way to go!" )
			Bret.say( "Excellent!" )
		}

		myManager.schedule(7) {
			Jen.say( "Totally cool!" )
			Alex.say( "Stupendous!" )
			Simon.say( "Well, I'll be superamalgamated!" )
		}
		myManager.schedule(10) {
			barReset = true
		}
	}
}


def leonPart() {
	//Whichever NPC speaks first should update the "wrapper" quest so that their talk bubble is in the quest-alert state.
	//Do the Scooby Doo treatment with crew members questioning the NPCs a lot.

	leonTalk = Leon.createConversation( "leonTalk", true, "Z24LeonTalkNow" )

	leon1 = [id:1]
	leon1.npctext = "The whole town is proud of you! Well done! Everything seems to make so much more sense now that we know about the Gambino event being the source of our troubles!"
	leon1.playertext = "Yeah...but all I did was stop a megalomaniac from launching an invasion of remote-controlled Sealabs. We still have to deal with the Animated out there!"
	leon1.result = 2
	leonTalk.addDialog( leon1, Leon )
	
	leon2 = [id:2]
	leon2.npctext = "Even though stopping Labtech X from his plans doesn't actually get rid of the Animated, at least we know more about WHY they're appearing and that's a lot more than we knew previously."
	leon2.result = 3
	leonTalk.addDialog( leon2, Leon )
	
	leon3 = [id:3]
	leon3.npctext = "I mean, we have to keep using the rings or the Animated will overwhelm us, but we'll get the science-types focused on the crater and the concept of a global 'biosphere' right away!"
	leon3.playertext = "I suppose that's true."
	leon3.result = 4
	leonTalk.addDialog( leon3, Leon )
	
	leon4 = [id:4]
	leon4.npctext = "Heck, maybe now that we know a little bit, some way will be unveiled so we can know even more! This could be really exciting!"
	leon4.playertext = "Heh. That's a great attitude, Leon."
	leon4.exec = { event ->
		agathaPart()
		event.player.unsetQuestFlag( GLOBAL, "Z24LeonTalkNow" )
		myManager.schedule(1) { event.player.setQuestFlag( GLOBAL, "Z24AgathaTalkNow" ); Agatha.pushDialog( event.player, "agathaTalk" ) }
	}
	leon4.result = DONE
	leonTalk.addDialog( leon4, Leon )
	
}
	
def agathaPart() {
	agathaTalk = Agatha.createConversation( "agathaTalk", true, "Z24AgathaTalkNow" )

	agatha1 = [id:1]
	agatha1.npctext = "Hi, hun! It's so good to see you in one piece again!"
	agatha1.playertext = "You too, Agatha."
	agatha1.result = 2
	agathaTalk.addDialog( agatha1, Agatha )
	
	agatha2 = [id:2]
	agatha2.npctext = "Now that you've revealed who's behind the NeXuS corporation, we've sent messengers to the Gambinos about the situation. No one knows where Johnny is at the moment, but we wanted him to know right away about his clone's escapades."
	agatha2.playertext = "I still can't believe it. Labtech X is a clone of the most senior member of G-Corp?!?"
	agatha2.result = 3
	agathaTalk.addDialog( agatha2, Agatha )
	
	agatha3 = [id:3]
	agatha3.npctext = "Well...now we know. And although the clone may have stolen a lot more from Johnny than he revealed this time around, now that he's been destroyed, it's just a matter of time until G-Corp recovers all those hidden assets."
	agatha3.playertext = "The faster the better. There were a lot of Labtechs down there, and I'm not going to assume they were all captured or defeated. They might have a 'Plan B' or something that we don't want to find out about."
	agatha3.result = 4
	agathaTalk.addDialog( agatha3, Agatha )
	
	agatha4 = [id:4]
	agatha4.npctext = "Too true. Of course, we still have our hands full with the Animated, and we still have to restore reliable communications with Durem, Aekea, and Gambino Island itself. It might take us a while to tie up all these loose ends."
	agatha4.playertext = "That makes sense."
	agatha4.exec = { event ->
		kinPart()
		event.player.unsetQuestFlag( GLOBAL, "Z24AgathaTalkNow" )
		myManager.schedule(1) { event.player.setQuestFlag( GLOBAL, "Z24KinTalkNow" ); Kin.pushDialog( event.player, "kinTalk" ) }
	}
	agatha4.result = DONE
	agathaTalk.addDialog( agatha4, Agatha )
	
	//--------------------------------------------------------------
	//RING GRANT CONVERSATION                                       
	//--------------------------------------------------------------
	ringGrant = Agatha.createConversation( "ringGrant", true, "Z24ChapterEndRingGrantOkay", "!Z24ChapterEndRingGrantReceived", "!Z24ChapterEndRingGrantInProgress" )
	
	def grant1 = [id:1]
	grant1.npctext = "Just one moment before you leave, dearie. We have a special trove of highly-charged rings from which we'd like you to select your prize."
	grant1.playertext = "Fantastic! Thank you!"
	grant1.exec = { event ->
		player = event.player
		event.player.setQuestFlag( GLOBAL, "Z24ChapterEndRingGrantInProgress" )
		makeMainMenu( player )
	}
	grant1.result = DONE
	ringGrant.addDialog( grant1, Agatha )
	
}

def kinPart() {
	
	kinTalk = Kin.createConversation( "kinTalk", true, "Z24KinTalkNow" )
	//Sounds like LX doesn't know much about where energy comes from, but does know how to control it.

	kin1 = [id:1]
	kin1.npctext = "Greetings once more, young ninja-to-be."
	kin1.playertext = "Hi again, Kin!"
	kin1.result = 2
	kinTalk.addDialog( kin1, Kin )
	
	kin2 = [id:2]
	kin2.npctext = "I am quite interested in how Labtech X was able to control the Ghi energy itself. The concept of a life aura surrounding and permeating everything in our world will be embraced by my Clan's mystics."
	kin2.playertext = "Yeah...it sure seems like a lot of people are going to be trying to use that energy once word gets out that it's a controllable resource."
	kin2.result = 3
	kinTalk.addDialog( kin2, Kin )
	
	kin3 = [id:3]
	kin3.npctext = "That is true. But if this Ghi energy surrounds and embraces us, then perhaps there is more to it than even Labtech X may have suspected."
	kin3.playertext = "That's an interesting way to look at it, Kin."
	kin3.result = 4
	kinTalk.addDialog( kin3, Kin )
	
	kin4 = [id:4]
	kin4.npctext = "We shall look into it further as soon as I return to my Clan in the forest."	
	kin4.exec = { event ->
		loganPart()
		event.player.unsetQuestFlag( GLOBAL, "Z24KinTalkNow" )
		myManager.schedule(1) { event.player.setQuestFlag( GLOBAL, "Z24LoganTalkNow" ); Logan.pushDialog( event.player, "loganTalk" ) }
	}
	kin4.result = DONE
	kinTalk.addDialog( kin4, Kin )
}

def loganPart() {

	loganTalk = Logan.createConversation( "loganTalk", true, "Z24LoganTalkNow" )
	//Johnny is incommunicado. No help from him to clarify all this.
	//Reinforce the "ring trap" that LX foisted on everyone.

	logan1 = [id:1]
	logan1.npctext = "Hey there, junior. It's about time you made it back to Barton Town."
	logan1.playertext = "Yeah, yeah. Harass me some more, Logan."
	logan1.result = 2
	loganTalk.addDialog( logan1, Logan )
	
	logan2 = [id:2]
	logan2.npctext = "I'd be happy to, but for right now, I guess I'm gonna have to break down and congratulate ya instead."
	logan2.result = 3
	loganTalk.addDialog( logan2, Logan )
	
	logan3 = [id:3]
	logan3.npctext = "Nah. Nevermind. I can't do it. Let's just say you did 'passably well' and end all this complimenting once and for all."
	logan3.playertext = "Ha, ha. Thanks a load there."
	logan3.result = 4
	loganTalk.addDialog( logan3, Logan )
	
	logan4 = [id:4]
	logan4.npctext = "Never a problem. Listen, it stinks that everyone that's defending their homes is now officially 'part of the problem' as we keep pulling more Ghi energy out of this 'biosphere' you keep telling us about and making more Animated because of it."
	logan4.result = 5
	loganTalk.addDialog( logan4, Logan )
	
	logan5 = [id:5]
	logan5.npctext = "So we're going to need to find out a lot more about the nature of that energy, and that sort of research is almost certainly going to involve a lot of traveling beyond the Barton Town area."
	logan5.playertext = "Sounds interesting!"
	logan5.result = 6
	loganTalk.addDialog( logan5, Logan )
	
	logan6 = [id:6]
	logan6.npctext = "It had better! 'Cuz when we get our strategy figured out here, you can bet that we're going to come callin' to see if you'll help us with it."
	logan6.playertext = "Anytime, Logan! I'm happy to help any way I can."
	logan6.result = 7
	loganTalk.addDialog( logan6, Logan )
	
	logan7 = [id:7]
	logan7.npctext = "I know y'are, bub. I know y'are. But don't call us, we'll call you. In the meantime, just get out there and help folks any way you can."
	logan7.playertext = "Can do!"
	logan7.flag = ["!Z24LoganTalkNow"] 
	logan7.exec = { event ->
		//push the completion of the STORYLINE WRAPPER quest
		if( !event.player.isDoneQuest( 119 ) ) {
			event.player.updateQuest( 119, "Story-VQS" )
		}
		//check to see if player hasn't yet received all end-game recipes
		if( event.player.getPlayerVar( "Z24NumBossRecipesReceived" ) < 4 && !event.player.hasQuestFlag( GLOBAL, "Z24GotEndBossRecipeThisTrip" ) ) {
			event.player.setQuestFlag( GLOBAL, "Z24GotEndBossRecipeThisTrip" )
			pickAnItem( event )
		} else {
			event.player.centerPrint( "You have received all four of the Chapter-End rewards. Congratulations!" )
			event.player.centerPrint( "So here's a little shower of gold instead!" )
			event.player.grantCoins( random( 100, 200 ) ) 
			event.player.unsetQuestFlag( GLOBAL, "Z13BossDead" )
		}
	}
	logan7.result = DONE
	loganTalk.addDialog( logan7, Logan )

}

//Sinister Scarf (17860), Mechlab Bot (17818), Crystal Fluff (17828), Null Fluff (17834)

def synchronized pickAnItem( event ) { 
	if( event.player.getPlayerVar( "Z24Got17828" ) == 0 ) {
		event.player.grantItem( "17828" ) 
		event.player.setPlayerVar( "Z24Got17828", 1 )
		event.player.addToPlayerVar( "Z24NumBossRecipesReceived", 1 )
		if( !event.player.hasQuestFlag( GLOBAL, "Z24ChapterEndRingGrantReceived" ) ) {
			event.player.setQuestFlag( GLOBAL, "Z24ChapterEndRingGrantOkay" )
			Agatha.pushDialog( event.player, "ringGrant" )
		} else {
			event.player.unsetQuestFlag( GLOBAL, "Z24GotEndBossRecipeThisTrip" )
			event.player.unsetQuestFlag( GLOBAL, "Z13BossDead" )
		}
		
	} else if( event.player.getPlayerVar( "Z24Got17834" ) == 0 ) {
		event.player.grantItem( "17834" ) 
		event.player.setPlayerVar( "Z24Got17834", 1 )
		event.player.addToPlayerVar( "Z24NumBossRecipesReceived", 1 )
		event.player.unsetQuestFlag( GLOBAL, "Z24GotEndBossRecipeThisTrip" )
		event.player.unsetQuestFlag( GLOBAL, "Z13BossDead" )
		
	} else if( event.player.getPlayerVar( "Z24Got17818" ) == 0 ) {
		event.player.grantItem( "17818" ) 
		event.player.setPlayerVar( "Z24Got17818", 1 )
		event.player.addToPlayerVar( "Z24NumBossRecipesReceived", 1 )
		event.player.unsetQuestFlag( GLOBAL, "Z24GotEndBossRecipeThisTrip" )
		event.player.unsetQuestFlag( GLOBAL, "Z13BossDead" )
		
	} else if( event.player.getPlayerVar( "Z24Got17860" ) == 0 ) {
		event.player.grantItem( "17860" ) 
		event.player.setPlayerVar( "Z24Got17860", 1 )
		event.player.addToPlayerVar( "Z24NumBossRecipesReceived", 1 )
		event.player.unsetQuestFlag( GLOBAL, "Z24GotEndBossRecipeThisTrip" )
		event.player.unsetQuestFlag( GLOBAL, "Z13BossDead" )

	} else {
		event.actor.centerPrint( "You have received all four of the Chapter End recipe rewards. Congratulations!" )
		event.player.unsetQuestFlag( GLOBAL, "Z13BossDead" )
	}
}


//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 10
CLdecimal = 0

def synchronized makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Debuffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Debuffs" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z24ChapterEndRingGrantInProgress" )
		}
	}
}

//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Dervish", "Hack", "Mantis", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Dervish" ) {
			makeDervishMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Mantis" ) {
			makeMantisMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Heavy Water Balloon", "Hornet Nest", "Hot Foot", "Hunter's Bow", "Shark Attack", "Shuriken", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Heavy Water Balloon" ) {
			makeHeavyWaterBalloonMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Hunter's Bow" ) {
			makeHuntersBowMenu( player )
		}
		if( event.selection == "Shark Attack" ) {
			makeSharkAttackMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Duct Tape", "Gumshoe", "Quicksand", "Scaredy Cat", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Duct Tape" ) {
			makeDuctTapeMenu( player )
		}
		if( event.selection == "Gumshoe" ) {
			makeGumshoeMenu( player )
		}
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Improbability Sphere", "Pot Lid", "Rock Armor", "Teflon Spray", "Turtle", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Improbability Sphere" ) {
			makeImprobabilitySphereMenu( player )
		}
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Rock Armor" ) {
			makeRockArmorMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Divinity", "Healing Halo", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Divinity" ) {
			makeDivinityMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Fleet Feet", "Ghost", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Fleet Feet" ) {
			makeFleetFeetMenu( player )
		}
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Adrenaline" ) {
			makeAdrenalineMenu( player )
		}
		if( event.selection == "Knife Sharpen" ) {
			makeKnifeSharpenMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.setQuestFlag( GLOBAL, "Z24ChapterEndRingGrantReceived" )
	event.actor.unsetQuestFlag( GLOBAL, "Z24GotEndBossRecipeThisTrip" )
	event.actor.unsetQuestFlag( GLOBAL, "Z13BossDead" )
}

//------------------------------------------
// DEV TEAM COMMENTS                        
//------------------------------------------

def activateTeamComments() {

	//Dave
	def daveTalk = Dave.createConversation( "daveTalk", true )

	dave1 = [id:1]
	dave1.npctext = "Mwuhaha!"
	dave1.playertext = "Mwuhaha? Who do you think you are?"
	dave1.result = 2
	daveTalk.addDialog( dave1, Dave )
	
	dave2 = [id:2]
	dave2.npctext = "heh. My RL name is David Georgeson. I'm the Sr. Producer and Director for zOMG!. I'm also the self-proclaimed Evil Overlord of all that I survey."
	dave2.playertext = "Is that supposed to be impressive?"
	dave2.result = 3
	daveTalk.addDialog( dave2, Dave )
	
	dave3 = [id:3]
	dave3.npctext = "Nope. Not at all. But, you have to admit it looks good on a business card! If you don't mind, there's one quick thing I'd like to say to the camera..."
	dave3.options = []
	dave3.options << [text:"Sure. What did you want to say?", result: 4]
	dave3.options << [text:"Talk to the camera on your own time, sucker.", result: DONE]
	dave3.result = 4
	daveTalk.addDialog( dave3, Dave )
	
	dave4 = [id:4]
	dave4.npctext = "Thanks!"
	dave4.result = 5
	daveTalk.addDialog( dave4, Dave )
	
	dave5 = [id:5]
	dave5.npctext = "*looks at the camera* Hi Mom! Hi Dad! This one's for you!"
	dave5.result = 6
	daveTalk.addDialog( dave5, Dave )
	
	dave6 = [id:6]
	dave6.npctext = "I've always wanted to do that."
	dave6.playertext = "Goofball."
	dave6.result = DONE
	daveTalk.addDialog( dave6, Dave )

	//Garrett
	def garrettTalk = Garrett.createConversation( "garrettTalk", true )

	garrett1 = [id:1]
	garrett1.npctext = "I'm a golden god and I'm full of delicious cow."
	garrett1.playertext = "What the heck does THAT mean?"
	garrett1.result = 2
	garrettTalk.addDialog( garrett1, Garrett )
	
	garrett2 = [id:2]
	garrett2.npctext = "It means I've been working on these scripts too long and I've lost my mind a bit."
	garrett2.playertext = "What did you do for zOMG!?"
	garrett2.result = 3
	garrettTalk.addDialog( garrett2, Garrett )
	
	garrett3 = [id:3]
	garrett3.npctext = "I'm Garrett Fern. I'm a Mission Designer. What that means is that I spend a lot of time elbow-deep in Groovy scripts bringing NPC dialog and game logic to life."
	garrett3.playertext = "Very cool!"
	garrett3.result = 4
	garrettTalk.addDialog( garrett3, Garrett )
	
	garrett4 = [id:4]
	garrett4.npctext = "I have to agree with you. Bringing a world to life fulfills my (golden) god complex quite well."
	garrett4.playertext = "But what about the 'delicious cow' thing you mentioned?"
	garrett4.result = 5
	garrettTalk.addDialog( garrett4, Garrett )
	
	garrett5 = [id:5]
	garrett5.npctext = "Well...let's just say that there used to be a lot more cattle at Bill's Ranch."
	garrett5.playertext = "Oh. I see."
	garrett5.result = DONE
	garrettTalk.addDialog( garrett5, Garrett )
	

	//Ryan
	def ryanTalk = Ryan.createConversation( "ryanTalk", true )

	ryan1 = [id:1]
	ryan1.npctext = "Hi. What's up?"
	ryan1.playertext = "Not much. I'm just going from table to table, chatting."
	ryan1.result = 2
	ryanTalk.addDialog( ryan1, Ryan )
	
	ryan2 = [id:2]
	ryan2.npctext = "Everyone's gotta have a hobby, I guess."
	ryan2.playertext = "heh. I suppose so. What are you doing here?"
	ryan2.result = 3
	ryanTalk.addDialog( ryan2, Ryan )
	
	ryan3 = [id:3]
	ryan3.npctext = "I'm *resting*. I've been going non-stop on the game for months now, working more hours than I ever thought I would for any job."
	ryan3.playertext = "Oh? What do you do?"
	ryan3.result = 4
	ryanTalk.addDialog( ryan3, Ryan )
	
	ryan4 = [id:4]
	ryan4.npctext = "I'm Ryan Cox. I'm the Lead Programmer for the zOMG! team."
	ryan4.playertext = "Holy Cow!"
	ryan4.result = 5
	ryanTalk.addDialog( ryan4, Ryan )
	
	ryan5 = [id:5]
	ryan5.npctext = "No...that's 'Cox'. Not 'Cow'."
	ryan5.playertext = "...<confused stare>...Oh! I get it now. Funny."
	ryan5.result = 6
	ryanTalk.addDialog( ryan5, Ryan )
	
	ryan6 = [id:6]
	ryan6.npctext = "Indeed."
	ryan6.result = DONE
	ryanTalk.addDialog( ryan6, Ryan )

	//Ben
	myManager.onExit(myRooms.Bar_1) { event ->
		if(isPlayer(event.actor) && birthdayFluffMap.containsKey(event.actor)) {
			println "^^^^ Disposing ${birthdayFluffMap.get(event.actor)} ^^^^"
			birthdayFluffMap.get(event.actor)?.dispose()
		}
	}
	
	def benTalk = Ben.createConversation( "benTalk", true )

	ben1 = [id:1]
	ben1.npctext = "Nothing to see here. Move along."
	ben1.playertext = "Did the box just talk?"
	ben1.result = 2
	benTalk.addDialog( ben1, Ben )
	
	ben2 = [id:2]
	ben2.npctext = "..."
	ben2.playertext = "Wait. I *know* you just talked. Are you under the box, or *are* you the box?"
	ben2.result = 3
	benTalk.addDialog( ben2, Ben )
	
	ben3 = [id:3]
	ben3.npctext = "Go away. This is the only place I can find where I don't get glare on my screen and I need to get more work done."
	ben3.playertext = "Work? What sort of work?"
	ben3.result = 4
	benTalk.addDialog( ben3, Ben )
	
	ben4 = [id:4]
	ben4.npctext = "Java, baby. My code helps the world run, but if I don't stay on focus, it'll all come to an end."
	ben4.playertext = "Wow. Who are you?"
	ben4.result = 5
	benTalk.addDialog( ben4, Ben )
	
	ben5 = [id:5]
	ben5.npctext = "I'm Ben Halsted. Now leave me to my madness, please."
	ben5.playertext = "You bet. See ya, Ben Box."
	/*ben5.exec = { event ->
		if(!birthdayFluffMap.containsKey(event.player)) {
			def mr_fluff = MrFluffSpawner.forceSpawnNow();
			mr_fluff.startFollow(event.actor);
			mr_fluff.setDisplayName("Mr. Fluff");
		
			birthdayFluffMap.put(event.player, mr_fluff)
		}
	}*/
	ben5.result = DONE
	benTalk.addDialog( ben5, Ben )

	//Fred
	def fredTalk = Fred.createConversation( "fredTalk", true )

	fred1 = [id:1]
	fred1.npctext = "'Allo. I have nothing to say to you until you bring me a beer."
	fred1.playertext = "Okay...so where do I go to get you a beer?"
	fred1.result = 2
	fredTalk.addDialog( fred1, Fred )
	
	fred2 = [id:2]
	fred2.npctext = "I don't think you can. We haven't programmed that in yet."
	fred2.playertext = "So you're a programmer then?"
	fred2.result = 3
	fredTalk.addDialog( fred2, Fred )
	
	fred3 = [id:3]
	fred3.npctext = "Yes. I created the Flash client that draws everything you see as you play the game and move through the world."
	fred3.playertext = "Dang. That sounds like a big job."
	fred3.result = 4
	fredTalk.addDialog( fred3, Fred )
	
	fred4 = [id:4]
	fred4.npctext = "You have no idea. I've been working on it for two years now and there's still so much I want to add."
	fred4.playertext = "What sorts of things do you want to add?"
	fred4.result = 5
	fredTalk.addDialog( fred4, Fred )
	
	fred5 = [id:5]
	fred5.npctext = "Did you bring me that beer yet?"
	fred5.playertext = "But...you said it wasn't programmed into the game yet!"
	fred5.result = 6
	fredTalk.addDialog( fred5, Fred )
	
	fred6 = [id:6]
	fred6.npctext = "Well, then I guess we are done talking until I add that feature!"
	fred6.result = DONE
	fredTalk.addDialog( fred6, Fred )

	//Jon
	def jonTalk = Jon.createConversation( "jonTalk", true )

	jon1 = [id:1]
	jon1.npctext = "howfdy"
	jon1.playertext = "kinda hard to talk through that... thing?"
	jon1.result = 2
	jonTalk.addDialog( jon1, Jon )
	
	jon2 = [id:2]
	jon2.npctext = "whaft fing?"
	jon2.playertext = "ooookay! so, uh, what did you do on the game?"
	jon2.result = 3
	jonTalk.addDialog( jon2, Jon )
	
	jon3 = [id:3]
	jon3.npctext = "serfer stuff, japha, you knowf."
	jon3.playertext = "oh! know any good game secrets? heh heh!"
	jon3.result = 4
	jonTalk.addDialog( jon3, Jon )
	
	jon4 = [id:4]
	jon4.npctext = "surf! whscug xvqqvacus! (ohsg cz zr lbhe ershzr!) *wink*" // some rot13 for fun.
	jon4.playertext = "hm. yeah, uh... great. thanks. a bunch. really."
	jon4.result = 5
	jonTalk.addDialog( jon4, Jon )
	
	jon5 = [id:5]
	jon5.npctext = "nof froflem."
	jon5.result = DONE
	jonTalk.addDialog( jon5, Jon )
	
	//Kaia
	def kaiaTalk = Kaia.createConversation( "kaiaTalk", true )

	kaia1 = [id:1]
	kaia1.npctext = "What?"
	kaia1.playertext = "Huh? What do you mean?"
	kaia1.result = 2
	kaiaTalk.addDialog( kaia1, Kaia )
	
	kaia2 = [id:2]
	kaia2.npctext = "Can't you see that I'm busy?"
	kaia2.playertext = "I...ummm...well...no. You look like you're sitting in a bar!"
	kaia2.result = 3
	kaiaTalk.addDialog( kaia2, Kaia )
	
	kaia3 = [id:3]
	kaia3.npctext = "Oh. Yeah. Heh. You can't see what I'm really doing, can you?"
	kaia3.playertext = "What *are* you doing?"
	kaia3.result = 4
	kaiaTalk.addDialog( kaia3, Kaia )
	
	kaia4 = [id:4]
	kaia4.npctext = "Micromanaging every single aspect of your personal existence...and everyone else's also."
	kaia4.playertext = "You're doing WHAT?!?"
	kaia4.result = 5
	kaiaTalk.addDialog( kaia4, Kaia )
	
	kaia5 = [id:5]
	kaia5.npctext = "I'm Josh Sylvester. I'm the engineer in charge of handling ALL the data in the game, keeping track of every tiny detail down the nth null fragment."
	kaia5.playertext = "'Kaia, the All Seeing', then?"
	kaia5.result = 6
	kaiaTalk.addDialog( kaia5, Kaia )
	
	kaia6 = [id:6]
	kaia6.npctext = "Yup. That's about the size of it. Now scoot along. I have to go make sure some orbs get into a person's rings."
	kaia6.result = DONE
	kaiaTalk.addDialog( kaia6, Kaia )
	
	//John K
	def johnKTalk = JohnK.createConversation( "johnKTalk", true )

	johnK1 = [id:1]
	johnK1.npctext = "Heya, I'm John Kim and I appreciate the female form."
	johnK1.playertext = "Uhmm...okay..."
	johnK1.result = 2
	johnKTalk.addDialog( johnK1, JohnK )
	
	johnK2 = [id:2]
	johnK2.playertext = "You don't waste your words, do you?"
	johnK2.result = 3
	johnKTalk.addDialog( johnK2, JohnK )
	
	johnK3 = [id:3]
	johnK3.npctext = "... what do you expect? I'm the art director, not a writer."
	johnK3.playertext = "Oh...uhmm, alright..."
	johnK3.result = 4
	johnKTalk.addDialog( johnK3, JohnK )
	
	johnK4 = [id:4]
	johnK4.playertext = "You guys have nice graphics here."
	johnK4.result = 5
	johnKTalk.addDialog( johnK4, JohnK )
	
	johnK5 = [id:5]
	johnK5.npctext = "Thanks!"
	johnK5.result = 6
	johnKTalk.addDialog( johnK5, JohnK )
	
	johnK6 = [id:6]
	johnK6.npctext = "Man, wish I had a +16 drac bow..."
	johnK6.result = DONE
	johnKTalk.addDialog( johnK6, JohnK )
	
	//Bret
	def bretTalk = Bret.createConversation( "bretTalk", true )

	bret1 = [id:1]
	bret1.npctext = "Greetings traveler; the ancient prophecy foretold of your arrival."
	bret1.playertext = "Who might you be?"
	bret1.result = 2
	bretTalk.addDialog( bret1, Bret )
	
	bret2 = [id:2]
	bret2.npctext = "*builds CT*"
	bret2.playertext = "Um, you do realize this isn't a turn-based game, right?"
	bret2.result = 3
	bretTalk.addDialog( bret2, Bret )
	
	bret3 = [id:3]
	bret3.npctext = "Oh yeah, sorry... I forgot where I was."
	bret3.result = 4
	bretTalk.addDialog( bret3, Bret )
	
	bret4 = [id:4]
	bret4.npctext = "I'm Bret Denslow, and I worked on the environments."
	bret4.options = []
	bret4.options << [text: "Really, how was that?", result:5]
	bret4.options << [text: "Speed it along buddy, I'm trying to talk to everyone in this bar.", result: 9]
	bretTalk.addDialog( bret4, Bret )
	
	bret5 = [id:5]
	bret5.npctext = "Creating levels from scratch and getting to work in 2D was a lot of fun!"
	bret5.result = 6
	bretTalk.addDialog( bret5, Bret )
	
	bret6 = [id:6]
	bret6.npctext = "Although bringing this game to completion sometimes could feel like a HORRIFIC affair."
	bret6.result = 7
	bretTalk.addDialog( bret6, Bret )
	
	bret7 = [id:7]
	bret7.npctext = "*chugs Health Drink*"
	bret7.result = 8
	bretTalk.addDialog( bret7, Bret )
	
	bret8 = [id:8]
	bret8.npctext = "Using my amassed BP, now EX Mode should go smoother though."
	bret8.result = 10
	bretTalk.addDialog( bret8, Bret )
	
	bret9 = [id:9]
	bret9.npctext = "Anyway, I just really wanted to say..."
	bret9.options = []
	bret9.options << [text: "Next", result: 10]
	bret9.options << [text: "Skip Cinematic", result: DONE]
	bret9.result = DONE
	bretTalk.addDialog( bret9, Bret )
	
	bret10 = [id:10]
	bret10.npctext = "Thanks for playing, I hope you've had and continue to have fun!"
	bret10.result = DONE
	bretTalk.addDialog( bret10, Bret )	
	
	//Tabitha
	def tabTalk = Tabitha.createConversation( "tabTalk", true )

	tab1 = [id:1]
	tab1.npctext = "..."
	tab1.playertext = ".."
	tab1.result = 2
	tabTalk.addDialog( tab1, Tabitha )
	
	tab2 = [id:2]
	tab2.npctext = "..."
	tab2.playertext = "He--"
	tab2.result = 3
	tabTalk.addDialog( tab2, Tabitha )
	
	tab3 = [id:3]
	tab3.npctext = "Do you have the information?"
	tab3.playertext = "Information? Who...?"
	tab3.result = 4
	tabTalk.addDialog( tab3, Tabitha )
	
	tab4 = [id:4]
	tab4.npctext = "Never mind. Uh... I'm just a humble artist. I had a part in things like items and icons. Though, my main task was to work on the graphics for the 'Animated', creating the pieces for the animator to use."
	tab4.playertext = "You helped create the animated? Why...?"
	tab4.result = 5
	tabTalk.addDialog( tab4, Tabitha )
	
	tab5 = [id:5]
	tab5.npctext = "Wait! You must have...misheard me...No! I have nothing to do with those horrible creatures currently destroying all of Gaia. I am just an innocent person sitting in a bar drinking...tea."
	tab5.playertext = "Bu-"
	tab5.result = 6
	tabTalk.addDialog( tab5, Tabitha )
	
	tab6 = [id:6]
	tab6.npctext = "Forget it, I don't exist. Are you happy with yourself? Bothering imaginary people? You should go talk to some of these other interesting people instead."
	tab6.result = 7
	tabTalk.addDialog( tab6, Tabitha )
	
	tab7 = [id:7]
	tab7.npctext = "And for goodness' sake, stop talking to figments of your imagination!"
	tab7.result = DONE
	tabTalk.addDialog( tab7, Tabitha )

	//Theresa
	def theresaTalk = Theresa.createConversation( "theresaTalk", true )

	theresa1 = [id:1]
	theresa1.npctext = "Nyo ho ho! Dude, who do you think is more awesome: Prince Orin or Eros?"
	theresa1.playertext = "Uh..."
	theresa1.result = 2
	theresaTalk.addDialog( theresa1, Theresa )
	
	theresa2 = [id:2]
	theresa2.npctext = "Pfft oh that's right! It's REAPERSUN! >:D"
	theresa2.playertext = "..."
	theresa2.result = 3
	theresaTalk.addDialog( theresa2, Theresa )
	
	theresa3 = [id:3]
	theresa3.npctext = "Okay okay, seriously. I do lots of things here, but my main responsibility is monster animation. It's exciting to finally see a creature burst to life on my screen... and savage the living hell out of an avatar..."
	theresa3.playertext = "So you'd be the sadist creating those brutal attack animations."
	theresa3.result = 4
	theresaTalk.addDialog( theresa3, Theresa )
	
	theresa4 = [id:4]
	theresa4.npctext = "I guess you could say I'm responsible for 'animating' the 'Animated' *jabs with elbow*"
	theresa4.playertext = "Alright well, we're done here."
	theresa4.result = 5
	theresaTalk.addDialog( theresa4, Theresa )
	
	theresa5 = [id:5]
	theresa5.npctext = "Who do you think is hotter: Eros or Anurla? Kekeke!"
	theresa5.result = DONE
	theresaTalk.addDialog( theresa5, Theresa )
	
	//Alex
	def alexTalk = Alex.createConversation( "alexTalk", true )

	alex1 = [id:1]
	alex1.npctext = "Hey you! Go play Skullgirls when it comes out! How's that for subtle in-game advertisements? Yusss."
	alex1.playertext = "Amazingly...oh, SO subtle."
	alex1.result = 2
	alexTalk.addDialog( alex1, Alex )
	
	alex2 = [id:2]
	alex2.npctext = "I know. I'm smooth."
	alex2.playertext = "So who are you anyway?"
	alex2.result = 3
	alexTalk.addDialog( alex2, Alex )
	
	alex3 = [id:3]
	alex3.npctext = "I'm Alex Ahad. I did a lot of concept work on the monsters and I brainstormed the game intro with John Kim."
	alex3.playertext = "Wow!"
	alex3.result = 4
	alexTalk.addDialog( alex3, Alex )
	
	alex4 = [id:4]
	alex4.npctext = "Hmmm...glad you liked it. But...SKULLGIRLS!!! Do IT!"
	alex4.playertext = "Gotcha. Will do."
	alex4.result = DONE
	alexTalk.addDialog( alex4, Alex )

	//Jen
	def jenTalk = Jen.createConversation( "jenTalk", true )

	jen1 = [id:1]
	jen1.npctext = "HI!"
	jen1.playertext = "Wow! That was forceful!"
	jen1.result = 2
	jenTalk.addDialog( jen1, Jen )
	
	jen2 = [id:2]
	jen2.npctext = "Oh, sorry. You looked excited and I wanted to be excited too."
	jen2.playertext = "Okay then! We'll be excited together! I assume you worked on zOMG! too...what did you do?"
	jen2.result = 3
	jenTalk.addDialog( jen2, Jen )
	
	jen3 = [id:3]
	jen3.npctext = "TONS of concept artwork, most of the ring and buff icons, lots of character art, a bunch of work on the intro, and more!"
	jen3.playertext = "Cool. So you're a concept artist then?"
	jen3.result = 4
	jenTalk.addDialog( jen3, Jen )
	
	jen4 = [id:4]
	jen4.npctext = "That's not everything I do, but yes...I'm proud of that work."
	jen4.playertext = "I can see why! Nice job! What's your name?"
	jen4.result = 5
	jenTalk.addDialog( jen4, Jen )
	
	jen5 = [id:5]
	jen5.npctext = "I'm Jen Zee. And thank you!"
	jen5.playertext = "You're welcome!"
	jen5.result = DONE
	jenTalk.addDialog( jen5, Jen )

	//Andre
	def andreTalk = Andre.createConversation( "andreTalk", true )

	andre1 = [id:1]
	andre1.npctext = "Bow down before me."
	andre1.playertext = "Ummmm...no?"
	andre1.result = 2
	andreTalk.addDialog( andre1, Andre )
	
	andre2 = [id:2]
	andre2.npctext = "Oh. Okay then. That's fine too."
	andre2.playertext = "Should I bow down? I mean...I don't want to be too offensive or anything."
	andre2.result = 3
	andreTalk.addDialog( andre2, Andre )
	
	andre3 = [id:3]
	andre3.npctext = "You definitely *should* bow down...after all, I am *the* Andre Mina...but you don't really have to. I mean, I'm totally worth your worship, but no biggie."
	andre3.playertext = "Gotcha. I'll skip it for now. Thanks. Do you work on the zOMG! dev team?"
	andre3.result = 4
	andreTalk.addDialog( andre3, Andre )
	
	andre4 = [id:4]
	andre4.npctext = "You bet I do. I've worked on tons of the items, monster animations, and even some of the maps."
	andre4.playertext = "So you're an artist then?"
	andre4.result = 5
	andreTalk.addDialog( andre4, Andre )
	
	andre5 = [id:5]
	andre5.npctext = "That's about the size of it."
	andre5.playertext = "Cool. Well, maybe I *should* bow down after all."
	andre5.result = 6
	andreTalk.addDialog( andre5, Andre )
	
	andre6 = [id:6]
	andre6.npctext = "I can't argue with that. I'm definitely worth a kowtow or two."
	andre6.result = DONE
	andreTalk.addDialog( andre6, Andre )


	//Justin
	def justinTalk = Justin.createConversation( "justinTalk", true )

	justin1 = [id:1]
	justin1.npctext = "Hey there."
	justin1.playertext = "Hi. 'sup?"
	justin1.result = 2
	justinTalk.addDialog( justin1, Justin )
	
	justin2 = [id:2]
	justin2.npctext = "Nothing much. Just chillin' here, resting after working myself to death over the world maps and terrains for the game."
	justin2.playertext = "Wow. You created some of the world?"
	justin2.result = 3
	justinTalk.addDialog( justin2, Justin )
	
	justin3 = [id:3]
	justin3.npctext = "That's a cool way to describe it. Yeah, I did."
	justin3.playertext = "I love the world of Gaia! What's your name?"
	justin3.result = 4
	justinTalk.addDialog( justin3, Justin )
	
	justin4 = [id:4]
	justin4.npctext = "Justin Wong. I'm one of the artists on the team."
	justin4.playertext = "Well, great job! I've been crawling all over the stuff you made. Thanks!"
	justinTalk.addDialog( justin4, Justin )
	
	justin5 = [id:5]
	justin5.npctext = "No problem. That's my job. I'm glad you like it!"
	justin5.result = DONE
	justinTalk.addDialog( justin5, Justin )


	//Simon
	def simonTalk = Simon.createConversation( "simonTalk", true )

	simon1 = [id:1]
	simon1.npctext = "I was captured by a massive cash register that was about to kill me! I told it 'You have no ca-ching! I can provide a dSonic one for you, if you let me go.' And thus I bargained my way out of the till of death."
	simon1.result = DONE
	simonTalk.addDialog( simon1, Simon )

	//Kemal
	def kemalTalk = Kemal.createConversation( "kemalTalk", true )

	kemal1 = [id:1]
	kemal1.npctext = "Without sound, the world would be silent."
	kemal1.result = DONE
	kemalTalk.addDialog( kemal1, Kemal )

}

//=====================================================
// EXIT TRIGGER LOGIC                                  
//=====================================================
	
onClickExitBar = new Object()

barExit = makeSwitch( "Bar_Out", myRooms.Bar_1, 960, 890 )
barExit.unlock()
barExit.off()
barExit.setRange( 250 )

def exitBar = { event ->
	synchronized( onClickExitBar ) {
		barExit.off()
		if( isPlayer( event.actor ) ) {
			event.actor.warp( "BARTON_102", 1110, 560 )
		}
	}
}

barExit.whenOn( exitBar )