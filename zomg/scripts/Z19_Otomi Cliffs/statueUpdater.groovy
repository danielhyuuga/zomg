def statueUpdater = "statueUpdater"
myRooms.OtCliffs_503.createTriggerZone(statueUpdater, 50, 420, 320, 755)
myManager.onTriggerIn(myRooms.OtCliffs_503, statueUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(232)) {
		event.actor.updateQuest(232, "Story-VQS")
	}
}