//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

mikaSpawned = false
Mika = null
otCliffsRoomList = ["OtCliffs_1","OtCliffs_2","OtCliffs_3","OtCliffs_101","OtCliffs_102","OtCliffs_103","OtCliffs_104","OtCliffs_201","OtCliffs_202","OtCliffs_203","OtCliffs_204","OtCliffs_301","OtCliffs_303","OtCliffs_304","OtCliffs_401","OtCliffs_501","OtCliffs_503","OtCliffs_601"]

myManager.onEnter( myRooms.OtCliffs_503 ) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Active") && Mika != null) {
		playerCurrentX = event.actor.getX()
		playerCurrentY = event.actor.getY()
		Mika.warp("OtCliffs_503", playerCurrentX, playerCurrentY)
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(260, 2) && !event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Active") && mikaSpawned == false ) {
		Mika = spawnNPC("Mika-VQS", myRooms.OtCliffs_503, 100, 440)
		Mika.setDisplayName( "Mika" )
		
		mikaSpawned = true
		
		def mikaMakeFollow = Mika.createConversation("mikaMakeFollow", true, "QuestStarted_260:2", "!Z19_Mika_Active")
		
		def mikaMakeFollow1 = [id:1]
		mikaMakeFollow1.playertext = "Mika! What are you doing out here!"
		mikaMakeFollow1.result = 2
		mikaMakeFollow.addDialog(mikaMakeFollow1, Mika)
		
		def mikaMakeFollow2 = [id:2]
		mikaMakeFollow2.npctext = "Enjoying the view! The ruins are soooooo pretty."
		mikaMakeFollow2.options = []
		mikaMakeFollow2.options << [text:"They really are. How'd you find this spot?", result:3]
		mikaMakeFollow2.options << [text:"Mika, it isn't safe here. Come away from the cliff.", result:4]
		mikaMakeFollow2.options << [text:"It's time to go home, Mika. Your family is very worried.", result:5]
		mikaMakeFollow.addDialog(mikaMakeFollow2, Mika)
		
		def mikaMakeFollow3 = [id:3]
		mikaMakeFollow3.npctext = "Oh, it was easy. I climbed!"
		mikaMakeFollow3.playertext = "Climbed?! Up the cliff? But it has to be over a hundred feet, straight up, and you're wearing a dress! How is that possible?"
		mikaMakeFollow3.result = 6
		mikaMakeFollow.addDialog(mikaMakeFollow3, Mika)
		
		def mikaMakeFollow4 = [id:4]
		mikaMakeFollow4.npctext = "Not safe for you, maybe. Don't worry about me, though, I'm a great climber."
		mikaMakeFollow4.playertext = "Even so, that's over a hundred feet of sheer cliff-wall."
		mikaMakeFollow4.result = 7
		mikaMakeFollow.addDialog(mikaMakeFollow4, Mika)
		
		def mikaMakeFollow5 = [id:5]
		mikaMakeFollow5.npctext = "But I don't wanna! It's so much more fun out here than in stinky old Barton Town."
		mikaMakeFollow5.options = []
		mikaMakeFollow5.options << [text:"That may be, but you still have to come home.", result:8]
		mikaMakeFollow5.options << [text:"Mika, I know it seems like an adventure to you, but you're terrifying your family.", result:9]
		mikaMakeFollow5.options << [text:"Are you sure? You must be getting hungry.", result:10]
		mikaMakeFollow.addDialog(mikaMakeFollow5, Mika)
		
		def mikaMakeFollow6 = [id:6]
		mikaMakeFollow6.npctext = "I dunno, I just did it. I've always been good at climbing."
		mikaMakeFollow6.playertext = "Apparently. Look, Mika, it's time to go home. Your family misses you and they're afraid because you've been gone so long."
		mikaMakeFollow6.result = 5
		mikaMakeFollow.addDialog(mikaMakeFollow6, Mika)
		
		def mikaMakeFollow7 = [id:7]
		mikaMakeFollow7.npctext = "Is it? I didn't think about it... just put one hand over the other until I was at the top."
		mikaMakeFollow7.playertext = "Didn't think? What if you fell?!"
		mikaMakeFollow7.result = 11
		mikaMakeFollow.addDialog(mikaMakeFollow7, Mika)
		
		def mikaMakeFollow8 = [id:8]
		mikaMakeFollow8.npctext = "Nuh uh! You can't make me!!!"
		mikaMakeFollow8.playertext = "I wouldn't want to, but..."
		mikaMakeFollow8.options = []
		mikaMakeFollow8.options << [text:"you've been gone so long that people are starting to think you've been hurt, or worse. Let's just head down so they can see you're okay.",result:9]
		mikaMakeFollow8.options << [text:"you must be starving after being gone so long. Sure you don't want to go get something to eat?", result:10]
		mikaMakeFollow.addDialog(mikaMakeFollow8, Mika)
		
		def mikaMakeFollow9 = [id:9]
		mikaMakeFollow9.npctext = "Can't you see I'm fine? Just tell them I'm safe and will be home soon!"
		mikaMakeFollow9.playertext = "I'm sorry, but I was told to bring you back..."
		mikaMakeFollow9.options = []
		mikaMakeFollow9.options << [text:"...and I intend to do so. Let's go, Mika.", result:8]
		mikaMakeFollow9.options << [text:"...and I intend to get you home in time for dinner!", result:10]
		mikaMakeFollow.addDialog(mikaMakeFollow9, Mika)
		
		def mikaMakeFollow10 = [id:10]
		mikaMakeFollow10.npctext = "Come to think of it, I am hungry. I'll go with you, but only long enough for some food!"
		mikaMakeFollow10.playertext = "If you say so. Why don't you follow me, I don't think I can manage the cliff."
		mikaMakeFollow10.result = 11
		mikaMakeFollow.addDialog(mikaMakeFollow10, Mika)
		
		def mikaMakeFollow11 = [id:11]
		mikaMakeFollow11.npctext = "Okay, let's go."
		mikaMakeFollow11.flag = "Z19_Mika_Active"
		mikaMakeFollow11.exec = { event2 -> 
			Mika.startFollow( event2.player ) 
			event2.player.addMiniMapQuestActorName("BFG-Jerry")
		}
		mikaMakeFollow.addDialog(mikaMakeFollow11, Mika)
	}
}

myManager.onExit(myRooms.OtCliffs_503) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Ruins") && !otCliffsRoomList.contains(event.actor.getRoomName())) {
		//println "##### DISPOSING OF ${stoocie} #####"
		Mika?.dispose()
	}
}

//Trigger zone definition at exit of OtCliffs
def mikaCliffsExit = "mikaCliffsExit"
myRooms.OtCliffs_501.createTriggerZone(mikaCliffsExit, 75, 320, 425, 625)

//Event to advance quest when player brings Mika to OtCliffs exit
myManager.onTriggerIn(myRooms.OtCliffs_501, mikaCliffsExit) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(260, 2) && event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Active")) {
	
		event.actor.unsetQuestFlag(GLOBAL, "Z19_Mika_Active")
		if(Mika != null) { 
			event.actor.setQuestFlag(GLOBAL, "Z19_Mika_Ruins")
		}
	}
}

//Warp Mika to player when they enter 301
myManager.onEnter(myRooms.OtCliffs_301) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Active") && Mika != null) {
		playerCurrentX = event.actor.getX()
		playerCurrentY = event.actor.getY()
		Mika.warp("OtCliffs_301", playerCurrentX, playerCurrentY)
	}
}

//Warp Mika to player when they enter 401
myManager.onEnter(myRooms.OtCliffs_401) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Active") && Mika != null) {
		playerCurrentX = event.actor.getX()
		playerCurrentY = event.actor.getY()
		Mika.warp("OtCliffs_401", playerCurrentX, playerCurrentY)
	}
}