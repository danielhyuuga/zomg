onClickExitTwins = new Object()

twinsExit = makeSwitch( "Twins_Out", myRooms.OtCliffs_501, 270, 360 )
twinsExit.unlock()
twinsExit.off()
twinsExit.setRange( 250 )

def exitTwins = { event ->
	synchronized( onClickExitTwins ) {
		twinsExit.off()
		if( isPlayer( event.actor ) ) {
			event.actor.warp( "OtRuins_701", 340, 590 )
		}
	}
}

twinsExit.whenOn( exitTwins )