//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

clove1 = null
clove2 = null
clove3 = null
clove4 = null

// Spawner example
garlicLTSpawner = myRooms.BF1_5.spawnSpawner( "garlicLTSpawner", "garlic_lt", 1)
garlicLTSpawner.stopSpawning()
garlicLTSpawner.setPos( 190, 110 )
garlicLTSpawner.setHome( "BF1_5", 190, 110 )
garlicLTSpawner.setHomeTetherForChildren(5000)
garlicLTSpawner.setSpawnWhenPlayersAreInRoom( true )
garlicLTSpawner.setWaitTime( 50 , 70 )
garlicLTSpawner.childrenWander( false )
garlicLTSpawner.setBaseSpeed(150)
garlicLTSpawner.setCFH(500)
garlicLTSpawner.setMonsterLevelForChildren( 2.0 )
garlicLTSpawner.addPatrolPointForChildren("BF1_5", 100, 400, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_105", 200, 300, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_205", 200, 350, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_305", 400, 500, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_405", 600, 500, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_505", 800, 300, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_506", 500, 400, 10)
garlicLTSpawner.addPatrolPointForChildren("BF1_406", 700, 530, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_406", 300, 300, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_306", 360, 435, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_206", 200, 475, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_106", 550, 380, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_6", 175, 550, 0)
garlicLTSpawner.addPatrolPointForChildren("BF1_5", 800, 450, 10)

garlicCloveSpawner = myRooms.BF1_5.spawnSpawner("garlicCloveSpawner", "garlic", 4)
garlicCloveSpawner.stopSpawning()
garlicCloveSpawner.setPos(190, 110)
garlicCloveSpawner.setHome("BF1_5", 190, 110)
garlicCloveSpawner.setHomeTetherForChildren(5000)
garlicCloveSpawner.setSpawnWhenPlayersAreInRoom(true)
garlicCloveSpawner.childrenWander(true)
garlicCloveSpawner.setBaseSpeed(100)
garlicCloveSpawner.setMonsterLevelForChildren(2.0)

def spawnGilroy() {
	myManager.schedule(random(1800, 3600)) { 
		gilroy = garlicLTSpawner.forceSpawnNow()
		gilroy.setDisplayName("Gilroy")
		clove1?.dispose()
		clove2?.dispose()
		clove3?.dispose()
		clove4?.dispose()
		
		//println "##### Spawned ${gilroy} #####"

		runOnDeath(gilroy) { event ->
			spawnX = gilroy.getX() + 7
			spawnY = gilroy.getY() + 7
			spawnRoom = gilroy.getRoomName()
			if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
				garlicCloveSpawner.warp(spawnRoom, spawnX, spawnY)
				clove1 = garlicCloveSpawner.forceSpawnNow()
				clove1.addHate(event.killer, 1)
			}
			
			spawnX = gilroy.getX() - 7
			spawnY = gilroy.getY() - 7
			spawnRoom = gilroy.getRoomName()
			if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
				garlicCloveSpawner.warp(spawnRoom, spawnX, spawnY)
				clove2 = garlicCloveSpawner.forceSpawnNow()
				clove2.addHate(event.killer, 1)
			}
			
			spawnX = gilroy.getX() + 7
			spawnY = gilroy.getY() - 7
			spawnRoom = gilroy.getRoomName()
			if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
				garlicCloveSpawner.warp(spawnRoom, spawnX, spawnY)
				clove3 = garlicCloveSpawner.forceSpawnNow()
				clove3.addHate(event.killer, 1)
			}
			
			spawnX = gilroy.getX() - 7
			spawnY = gilroy.getY() + 7
			spawnRoom = gilroy.getRoomName()
			if((spawnX > 1 && spawnX < 1040) && (spawnY > 1 && spawnY < 672)) {
				garlicCloveSpawner.warp(spawnRoom, spawnX, spawnY)
				clove4 = garlicCloveSpawner.forceSpawnNow()
				clove4.addHate(event.killer, 1)
			}
			
			spawnGilroy()
		}
	}
}

spawnGilroy()
