import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------

def spawner1 = myRooms.BF1_806.spawnSpawner( "BF1_806_Spawner", "fluff", 1)
spawner1.setPos( 935, 360 )
spawner1.setInitialMoveForChildren( "BF1_806", 150, 100 )
spawner1.setHome( "BF1_806", 150, 100 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.0 )

def spawner2 = myRooms.BF1_806.spawnSpawner( "BF1_806_Spawner2", "mosquito", 1)
spawner2.setPos( 935, 350 )
spawner2.setInitialMoveForChildren( "BF1_806", 460, 270 )
spawner2.setHome( "BF1_806", 460, 270 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.0 )

def spawner3 = myRooms.BF1_806.spawnSpawner( "BF1_806_Spawner3", "mosquito", 1)
spawner3.setPos( 935, 340 )
spawner3.setInitialMoveForChildren( "BF1_806", 700, 165 )
spawner3.setHome( "BF1_806", 700, 165 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.0 )

//Alliances
spawner2.allyWithSpawner( spawner3 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()