/*

import com.gaiaonline.mmo.battle.script.*;

myManager.onEnter( myRooms.BF1_404 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
	}
}

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("kioskPosition", "markerOther", "BF1_404", 140, 390, "HyperNet Access")

kiosk = makeSwitch( "botKiosk", myRooms.BF1_404, 140, 390 )
kiosk.unlock()
kiosk.off()
kiosk.setRange( 200 )
kiosk.setMouseoverText("Access the HyperNet")

def clickKiosk = { event ->
	kiosk.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk.whenOn( clickKiosk )


//====================================================
// TUTORIAL TREE                                      
//====================================================

kioskWidth = 300

def giveReward( event ) {
	//gold reward
	goldGrant = random( 50, 150 )
	event.player.centerPrint( "You receive ${goldGrant} gold!" )
	event.player.grantCoins( goldGrant )

	//orb reward
	orbGrant = 2
	event.player.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
	event.player.grantQuantityItem( 100257, orbGrant )
}
	
def synchronized makeKioskMenu( player ) {
	titleString = "The HyperNet"
	descripString = "Choose Game Help to access loads of information about the game, or one of the other categories for easy downloads of useful tidbits."
	diffOptions = ["Game Help", "Tutorials", "Area Info", "Exit HyperNet"]
	
	uiButtonMenu( player, "kioskMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Game Help" ) {
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXGameHelpSelected" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXGameHelpSelected" )
				//gold reward
				goldGrant = 250 
				event.actor.centerPrint( "You receive ${goldGrant} gold!" )
				event.actor.grantCoins( goldGrant )

				//orb reward
				orbGrant = 2
				event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				event.actor.grantQuantityItem( 100257, orbGrant )
			}
			showWidget( event.actor, "GameHelp", true, true )
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
		if( event.selection == "Tutorials" ) {
			makeTutorialMenu( player )
		}
		if( event.selection == "Area Info" ) {
			makeAreaMenu( player )
		}
		if( event.selection == "Exit HyperNet" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
	}
}

def makeAreaMenu( player ) {
	titleString = "Area Info"
	descripString = "A small archive of stories and tidbits of the area around you."
	diffOptions = ["Bill's Ranch", "Main Menu"]
	
	uiButtonMenu( player, "fictionMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Bill's Ranch" ) {
			tutorialNPC.pushDialog( event.actor, "billRanch" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}
	


def makeTutorialMenu( player ) {
	titleString = "Tutorials"
	descripString = "These tutorials give you quick overviews on various features in the game."
	diffOptions = ["Advanced Combat", "Crew Advantages", "Suppress CL", "Ring Sets", "Main Menu"]
	
	uiButtonMenu( player, "tutorialMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Advanced Combat" ) {
			tutorialNPC.pushDialog( event.actor, "advCombat" )
		}
		if( event.selection == "Crew Advantages" ) {
			tutorialNPC.pushDialog( event.actor, "crewAdv" )
		}
		if( event.selection == "Suppress CL" ) {
			tutorialNPC.pushDialog( event.actor, "suppressCL" )
		}
		if( event.selection == "Ring Sets" ) {
			tutorialNPC.pushDialog( event.actor, "ringSets" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// TUTORIAL MENU                                                                        
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//ADVANCED COMBAT TUTORIAL                                                              
//--------------------------------------------------------------------------------------
advCombat = tutorialNPC.createConversation( "advCombat", true )

def adv1 = [id:1]
adv1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Advanced Combat</font></b></h1><br><font face='Arial' size ='12'>There are ways to utilize keyboard shortcuts and a floating Ring Bar in order to increase you reaction time during combat.<br><br>The first step is to open your Inventory. Then drag the 'Equipped Ring' tray away from the Inventory and close the Inventory.<br><br>You can activate any of your rings by clicking on them, but you can also use hotkey shortcuts. The left-most ring is '1' and the right-most is '8'.<br><br>So just click on a target and then use your number keys to quickly select your ring choices.<br><br>Additionally, you can cycle through targets by using the 'Q' key. This lets you cycle through enemies without having to use the mouse to click a target, letting you make all your combat choices with the keyboard, if you so desire.<br><br>]]></zOMG>"
adv1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BF1_404 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXAdvCombat" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXAdvCombat" )
			giveReward( event )
		}
	}
}
adv1.result = DONE
advCombat.addDialog( adv1, tutorialNPC )

//--------------------------------------------------------------------------------------
//CREW ADVANTAGES TUTORIAL                                                              
//--------------------------------------------------------------------------------------
crewAdv = tutorialNPC.createConversation( "crewAdv", true )

def crew1 = [id:1]
crew1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Advantages to Crewing</font></b></h1><br><font face='Arial' size ='12'>Crews are your friends.<br><br>First of all, you do *not* split loot with your Crewmates. So more loot is created for each monster defeated so that your personal gain is the same as if you were solo. However...<br><br>1) You can defeat more monsters while Crewed, so you'll end up with a lot more loot that way than if you solo, and;<br><br>2) Gold rewards *are* multiplied up, so you will gain more gold while Crewed than you will while soloing.<br><br>Additionally, when you solo, you can't use more than eight rings. But when Crewed, your Crewmates can use their rings on *you* and you can be a lot tougher than when you solo.<br><br>]]></zOMG>"
crew1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BF1_404 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXCrewAdv" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXCrewAdv" )
			giveReward( event )
		}
	}
}
crew1.result = DONE
crewAdv.addDialog( crew1, tutorialNPC )

//--------------------------------------------------------------------------------------
//SUPPRESS CL TUTORIAL                                                                  
//--------------------------------------------------------------------------------------
suppressCL = tutorialNPC.createConversation( "suppressCL", true )

def suppress1 = [id:1]
suppress1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Suppressing Your CL</font></b></h1><br><font face='Arial' size ='12'>Once you've built your overall Charge Level up a bit, you'll find that certain events and hunting areas are off-limits to you because you're too powerful.<br><br>Never fear! You can still play all that content!<br><br>Click on the 'Options' button on your Control Bar and then choose the 'SUPPRESS CL' option.<br><br>Then you can easily drag your overall CL down to the level you want to play at and join your friends for fun!<br><br>To reset your CL back up to max, just head to the Null Chamber and choose 'SUPPRESS CL' again. You can't raise your CL level while you're 'in the field', but you can reset it easily while in the Null Chamber.<br><br>]]></zOMG>"
suppress1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BF1_404 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXSuppressCL" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXSuppressCL" )
			giveReward( event )
		}
	}
}
suppress1.result = DONE
suppressCL.addDialog( suppress1, tutorialNPC )

//--------------------------------------------------------------------------------------
//RING SET TUTORIAL                                                                     
//--------------------------------------------------------------------------------------
ringSets = tutorialNPC.createConversation( "ringSets", true )

def set1 = [id:1]
set1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Ring Sets</font></b></h1><br><font face='Arial' size ='12'>So you've got a bunch of rings now and you think you've got the hang of it, right?<br><br>But wait! There's more!<br><br>If you wear your Rings in certain combinations, there are bonuses you can unlock for even more power!<br><br>Each Ring Set is composed of four unique rings. If all four of those rings are worn on a single hand (slots 1-4 or 5-8), then you get the bonus automatically.<br><br>The various Ring Sets are fully details in GAME HELP on the 'Ring Sets' page, so check it out!<br><br>Ring Sets are completely optional. Use them if they fit your play style!<br><br>]]></zOMG>"
set1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BF1_404 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXRingSets" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXRingSets" )
			giveReward( event )
		}
	}
}
set1.result = DONE
ringSets.addDialog( set1, tutorialNPC )


//======================================================================================
//======================================================================================
// AREA MENU                                                                            
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//BILL'S RANCH BACKGROUND                                                               
//--------------------------------------------------------------------------------------
def billRanch = tutorialNPC.createConversation( "billRanch", true )

def ranch1 = [id:1]
ranch1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Bill's Ranch</font></b></h1><br><font face='Arial' size ='12'>Rancher Bill has been homesteading the area at the base of a dark, foreboding mountain for years now.<br><br>Despite the occasional incursions by the nasty denizens of the mountain, he and his family have toughed it out, protecting their land and helping anyone that comes through the area.<br><br>Bill's family includes Purvis, his enormously strong, but dull-witted son, and a ranch hand named Rubella, who has been with Bill long enough that he thinks of her as family.<br><br>]]></zOMG>"
ranch1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BF1_404 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXBillRanch" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXBillRanch" )
			giveReward( event )
		}
	}
}
ranch1.result = DONE
billRanch.addDialog( ranch1, tutorialNPC )

*/
