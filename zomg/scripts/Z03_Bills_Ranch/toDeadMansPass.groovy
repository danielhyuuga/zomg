import com.gaiaonline.mmo.battle.script.*;


//---------------------------------------------------------
// SWITCH FOR DEADMAN'S PASS ENTRY                         
//---------------------------------------------------------
timerMap = new HashMap()
offlineResetSet = [] as Set

toDeadmans = makeSwitch("DeadmansPass_In", myRooms.BF1_705, 150, 330)
toDeadmans.setMouseoverText("Deadman's Gate")
toDeadmans.unlock()
toDeadmans.off()
toDeadmans.setRange(200)

def dmpWarp(player)
{
	player.centerPrint( "The rusty gate opens wide enough to allow entry." )
	sound( "deadmansGate" ).toPlayer( player )
	player.warp( "M1_407", 940, 350 )
}

def goToDeadmans =  { event ->
	if( isPlayer( event.actor ) ) {
		// if they have a crew in dms, let them run through dmp
		if(event.actor.getTeam().hasAreaVar( "Z30_Undermountain", "Z30DeadmansDifficulty") == true)
		{
			dmpWarp(event.actor);
		}
		else
		{
			event.actor.setCrewVar("DeadmansDifficultySelectionInProgress", 0)
			//check each crew member to see if any of them have the "key" that Bill gives in Quest 55
			
			event.actor.getCrew().each {
				if( it.isOnQuest( 55 ) || it.isDoneQuest( 55 ) || it.isOnQuest( 114 ) || it.isOnQuest( 264 ) || it.isOnQuest( 266 ) || it.isOnQuest( 268 ) || it.isDoneQuest( 114 ) || it.isDoneQuest( 264 ) || it.isDoneQuest( 266 ) || it.isDoneQuest( 268 ) ) {
					event.actor.setQuestFlag( GLOBAL, "Z03TempDeadMansPassEntryAllowed" )
				}
			}
			if( event.actor.hasQuestFlag( GLOBAL, "Z03TempDeadMansPassEntryAllowed" )) {
				//if no one on the team is in Deadman's Pass yet, then start checking difficulty
				if( event.actor.getTeam().hasAreaVar( "Z04_House_on_Hill", "Z04DeadmansDifficulty" ) == false ) {
					if( event.actor.getCrewVar( "DeadmansDifficultySelectionInProgress" ) == 0 ) {
						//Prevent multiple players from initiating the process of selecting difficulties
						event.actor.setCrewVar( "DeadmansDifficultySelectionInProgress", 1 )
	
						//If the leader is in the zone then...
						if( isInZone( event.actor.getTeam().getLeader() ) ) {
	
							event.actor.unsetQuestFlag(GLOBAL, "Z03TempDeadMansPassEntryAllowed")
							leader = event.actor.getTeam().getLeader()
							makeMenu(leader)
							event.actor.getCrew().each{ if( it != event.actor.getTeam().getLeader() ) { it.centerPrint( "Your Crew Leader is choosing the challenge level for this encounter." ) } }
	
							//Everyone else gets the following message while the leader chooses a setting. If no choice is made after 10 seconds, then everyone gets a message that says:
							decisionTimer = myManager.schedule(30) {
								event.actor.getCrew().each{
									it.centerPrint( "No choice was made within 30 seconds. Click Deadman's Gate to try again." )									
								}
								event.actor.setCrewVar( "DeadmansDifficultySelectionInProgress", 0 )
								//if no selection was made, then remove the leader and timer from the timerMap
								timerMap.remove( event.actor.getTeam().getLeader() )
							}
							//add the leader and timer to a map so the timer can be canceled later, if necessary
							timerMap.put( event.actor.getTeam().getLeader(), decisionTimer )
	
							//If the leader is not in the zone then tell the crew the leader must be present
						} else {
							event.actor.getCrew().each{
								if( it != event.actor.getTeam().getLeader() ) {
									it.centerPrint( "Your Crew Leader must be in Bill's Ranch before you can enter Deadman's Pass." )
								} else {
									it.centerPrint( "Your Crew is trying to enter Deadman's Pass. You must be in Bill's Ranch for that to occur." )
								}
							}
							event.actor.setCrewVar( "DeadmansDifficultySelectionInProgress", 0 )
						}
					} else if( event.actor.getCrewVar( "DeadmansDifficultySelectionInProgress" ) == 1 ) {
						if( event.actor != event.actor.getTeam().getLeader() ) {
							event.actor.centerPrint( "Your Crew Leader is already making a challenge level choice. One moment, please..." )
						}
					}
				} else {
					if(event.actor.getTeam().hasAreaVar("Z04_House_on_Hill", "Z04DeadmansDifficulty") == true) {					
						event.actor.unsetQuestFlag(GLOBAL, "Z03TempDeadMansPassEntryAllowed")
						dmpWarp(event.actor);
					}
				}	
			} else {
				event.actor.centerPrint( "The rusty lock on the gate stubbornly refuses to open." )
			}
		}	
	}
	myManager.schedule(1) { toDeadmans.off() }
}

toDeadmans.whenOn(goToDeadmans)

def synchronized makeMenu(leader) {
	descripString = "You must choose a Challenge Level to enter Deadman's Pass. You have 30 seconds to decide."
	diffOptions = ["Easy (small rewards)", "Normal (moderate rewards)", "Hard (large rewards)", "Cancel"]
	
	//println "**** trying to create the menu ****"
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy (small rewards)" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click Deadman's Gate to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click Deadman's Gate to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z04_House_on_Hill", "Z04DeadmansDifficulty", 1 )
			event.actor.setCrewVar( "DeadmansDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal (moderate rewards)" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click Deadman's Gate to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click Deadman's Gate to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z04_House_on_Hill", "Z04DeadmansDifficulty", 2 )
			event.actor.setCrewVar( "DeadmansDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard (large rewards)" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click Deadman's Gate to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click Deadman's Gate to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z04_House_on_Hill", "Z04DeadmansDifficulty", 3 )
			event.actor.setCrewVar( "DeadmansDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar("DeadmansDifficultySelectionInProgress", 0)
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}
