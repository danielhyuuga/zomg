//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BF1_503.spawnSpawner( "bf1_503_spawner1", "fluff", 1)
spawner1.setPos( 70, 170 )
spawner1.setHome( "BF1_503", 70, 170 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 1.8 )

def spawner2 = myRooms.BF1_503.spawnSpawner( "bf1_503_spawner2", "fluff", 1)
spawner2.setPos( 940, 565 )
spawner2.setHome( "BF1_503", 940, 565 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 1.8 )

def spawner3 = myRooms.BF1_503.spawnSpawner( "bf1_503_spawner3", "fluff", 1)
spawner3.setPos( 540, 440 )
spawner3.setHome( "BF1_503", 540, 440 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 1.8 )

def spawner4 = myRooms.BF1_503.spawnSpawner( "bf1_503_spawner4", "fluff", 1)
spawner4.setPos( 295, 305 )
spawner4.setHome( "BF1_503", 195, 305 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 1.8 )

//Alliances
spawner1.allyWithSpawner( spawner2 )
spawner1.allyWithSpawner( spawner3 )
spawner1.allyWithSpawner( spawner4 )
spawner2.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner4 )
spawner3.allyWithSpawner( spawner4 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()
spawner4.forceSpawnNow()