import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------

def spawner = myRooms.BF1_805.spawnSpawner( "BF1_805_Spawner", "mosquito", 1)
spawner.setPos( 830, 110 )
spawner.setInitialMoveForChildren( "BF1_805", 130, 280 )
spawner.setHome( "BF1_805", 130, 280 )
spawner.setSpawnWhenPlayersAreInRoom( true )
spawner.setWaitTime( 50 , 70 )
spawner.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner.childrenWander( true )
spawner.setMonsterLevelForChildren( 2.0 )

def spawner2 = myRooms.BF1_805.spawnSpawner( "BF1_805_Spawner2", "mosquito", 1)
spawner2.setPos( 830, 100 )
spawner2.setInitialMoveForChildren( "BF1_805", 345, 430 )
spawner2.setHome( "BF1_805", 345, 430 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.0 )

def spawner3 = myRooms.BF1_805.spawnSpawner( "BF1_805_Spawner3", "fluff", 1)
spawner3.setPos( 830, 100 )
spawner3.setInitialMoveForChildren( "BF1_805", 490, 135 )
spawner3.setHome( "BF1_805", 490, 135 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.0 )

def spawner4 = myRooms.BF1_805.spawnSpawner( "BF1_805_Spawner4", "fluff", 1)
spawner4.setPos( 830, 90 )
spawner4.setInitialMoveForChildren( "BF1_805", 900, 330 )
spawner4.setHome( "BF1_805", 900, 330 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 2.0 )

//Alliances
spawner.allyWithSpawner( spawner2 )
spawner3.allyWithSpawner( spawner4 )

//Spawning
spawner.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()
spawner4.forceSpawnNow()