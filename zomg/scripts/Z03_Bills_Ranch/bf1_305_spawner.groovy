//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner One - Cleared for take-off
def spawner1 = myRooms.BF1_305.spawnSpawner( "bf1_305_fluff1", "fluff", 1)
spawner1.setPos( 125, 590 )
spawner1.setHome( "BF1_305", 125, 590 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.0 )

// Spawner Two - Cleared for take-off
/*def spawner2 = myRooms.BF1_305.spawnSpawner( "bf1_305_spawner2", "garlic", 1)
spawner2.setPos( 190, 150 )
spawner2.setHome( "BF1_305", 190, 150 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50, 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.0 )

// Spawner Three - Cleared for take-off
def spawner3 = myRooms.BF1_305.spawnSpawner( "bf1_305_spawner3", "garlic", 1)
spawner3.setPos( 505, 330 )
spawner3.setHome( "BF1_305", 505, 330 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.0 )

// Spawner Three - Cleared for take-off
def spawner4 = myRooms.BF1_305.spawnSpawner( "bf1_305_spawner4", "garlic", 1)
spawner4.setPos( 865, 150 )
spawner4.setHome( "BF1_305", 865, 150 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 2.0 )*/

// Spawner Three - Cleared for take-off
def spawner5 = myRooms.BF1_305.spawnSpawner( "bf1_305_spawner5", "fluff", 1)
spawner5.setPos( 515, 615 )
spawner5.setHome( "BF1_305", 515, 615 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 50 , 70 )
spawner5.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner5.childrenWander( true )
spawner5.setMonsterLevelForChildren( 2.0 )

//Alliances
spawner1.allyWithSpawner( spawner5 )

//Spawning
spawner1.forceSpawnNow()
spawner5.forceSpawnNow()