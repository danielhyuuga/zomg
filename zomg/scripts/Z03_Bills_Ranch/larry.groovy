//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//Quest #s - Fluff Rush = 76, No Service = 28, Bill's Shipment = 75

Larry = spawnNPC("Larry-VQS", myRooms.BF1_706, 475, 340)
Larry.setDisplayName( "Larry" )

playerListBF1_706 = []

myManager.onEnter( myRooms.BF1_706 ) { event ->
	if(isPlayer(event.actor)) {
		playerListBF1_706 << event.actor
		if(event.actor.hasQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")) {
			event.actor.unsetQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")
		}
	}
}

myManager.onExit( myRooms.BF1_706 ) { event ->
	if(isPlayer(event.actor)) {
		playerListBF1_706.remove( event.actor )
	}
}

onQuestStep(279, 2) { event -> event.player.addMiniMapQuestActorName("Larry-VQS") }
onQuestStep(76, 2) { event -> event.player.addMiniMapQuestActorName("Larry-VQS") }

//Menu objects - for easy modification
MENU_TITLE = "Fluff Rush"

MENU_NON_QUEUE_DESCRIP = "Join the Fluff Rush queue?"
MENU_QUEUE_DESCRIP = "Check your position in the Fluff Rush queue?"

MENU_NON_QUEUE_OPTIONS = ["Join the Queue", "Check Queue Length", "Cancel"]
MENU_QUEUE_OPTIONS = ["Check My Position", "Cancel"]

fluffMenuSet = [] as Set

def fluffMenuCleanup() {
	myManager.schedule(10) { fluffMenuCleanup() }
	fluffMenuSet.each() {
		if(!isOnline(it)) {
			it.unsetQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")
			fluffMenuSet.remove(it)
		}
	}
}

fluffMenuCleanup()

//---------------------------------------------------------------------------------------------
//Default conversation when player speaks with Larry without any quest direction               
//---------------------------------------------------------------------------------------------
def larryDefault = Larry.createConversation("larryDefault", true, "!Z3_RancherBill_HelpingLarry_Active", "!QuestStarted_76", "!QuestCompleted_76", "!QuestStarted_92")

def default1 = [id:1]
default1.npctext = "Well, hello. How'd you wind up here?"
default1.options = []
default1.options << [text:"I don't know... I was just sort of walking around.", result:2]
default1.options << [text:"I want something to do! Something fun!", result:3]
larryDefault.addDialog(default1, Larry)

def default2 = [id:2]
default2.npctext = "Well, if you're tired of walking around and want to do something a bit more exciting, I could use your help!"
default2.result = 4
larryDefault.addDialog(default2, Larry)

def default3 = [id:3]
default3.npctext = "Something fun, eh? I've got something you could help me out with that should be lots of fun!"
default3.result = 4
larryDefault.addDialog(default3, Larry)

def default4 = [id:4]
default4.playertext = "What do you need help with?"
default4.result = 5
larryDefault.addDialog(default4, Larry)

def default5 = [id:5]
default5.npctext = "The Fluffs might look like cute, little balls of love but they're a menace! The miserable things keep finding their way onto the subway tracks, forcing us to shut the trains down!"
default5.result = 6
larryDefault.addDialog(default5, Larry)

def default6 = [id:6]
default6.playertext = "Why don't you stop them then?"
default6.result = 7
larryDefault.addDialog(default6, Larry)

def default7 = [id:7]
default7.npctext = "That's what I'm here for! I'm pretty sure they're getting underground through a hole hidden by that clump of mushrooms across the road... but without rings I can't do anything to stop them!"
default7.playertest = "What do you mean? Why can't you stop them without rings?"
default7.result = 8
larryDefault.addDialog(default7, Larry)

def default8 = [id:8]
default8.npctext = "They're Animated, you can't hurt them at all without rings! It's just one of the weird things about them..."
default8.options = []
default8.options << [text:"Ohh, I see what you're saying. Hey, I have a few rings, maybe I can help you.", result: 9]
default8.options << [text:"Maybe you should get some rings then.", result: 10]
larryDefault.addDialog(default8, Larry)

def default9 = [id:9]
default9.npctext = "That would be fantastic! They seem to come in waves. I'll keep an eye out for more and you off 'em as fast as you can, alright? I'll let you know when I don't see anymore coming."
default9.playertext = "Alright... I'll go stand by the mushrooms to start..."
default9.quest = 76
default9.exec = { event -> startFluffQueue( event ) }
default9.result = DONE
larryDefault.addDialog(default9, Larry)

def default10 = [id:10]
default10.npctext = "Yeah. Wait, I can see you have some rings. Would you help me?"
default10.options = []
default10.options << [text:"Yeah, I'm up for trying anyway.", result: 9]
default10.options << [text:"Nah, you're on your own.", result: DONE]
larryDefault.addDialog(default10, Larry)

//---------------------------------------------------------------------------------------------
//Conversation when player is coming from Bill                                                 
//---------------------------------------------------------------------------------------------
def larryBill = Larry.createConversation("larryBill", true, "Z3_RancherBill_HelpingLarry_Active", "!QuestStarted_76", "!QuestStarted_92", "!QuestCompleted_76")

def bill1 = [id:1]
bill1.npctext = "Well, hello. How'd you wind up here?"
bill1.result = 2
larryBill.addDialog(bill1, Larry)

def bill2 = [id:2]
bill2.playertext = "Bill sent me over... he said you needed some help with the some fluffs. I think he must be confused though... I can't imagine anything so cute and cuddly causing trouble."
bill2.result = 3
larryBill.addDialog(bill2, Larry)

def bill3 = [id:3]
bill3.npctext = "The Fluffs might look like cute, little balls of love but they're a menace! The miserable things keep finding their way onto the subway tracks, forcing us to shut the trains down!"
bill3.result = 4
larryBill.addDialog(bill3, Larry)

def bill4 = [id:4]
bill4.playertext = "Okay, why don't you stop them then?"
bill4.result = 5
larryBill.addDialog(bill4, Larry)

def bill5 = [id:5]
bill5.npctext = "That's what I'm here for! I'm pretty sure they're getting underground through a hole hidden by that clump of mushrooms across the road... but without rings I can't do anything to stop them!"
bill5.playertest = "What do you mean? Why can't you stop them without rings?"
bill5.result = 6
larryBill.addDialog(bill5, Larry)

def bill6 = [id:6]
bill6.npctext = "They're Animated, you can't hurt them at all without rings! It's just one of the weird things about them..."
bill6.options = []
bill6.options << [text:"Ohh, I see what you're saying. Hey, I have a few rings, maybe I can help you.", result: 7]
bill6.options << [text:"Maybe you should get some rings thing.", result: 8]
larryBill.addDialog(bill6, Larry)

def bill7 = [id:7]
bill7.npctext = "That would be fantastic! They seem to come in waves. I'll keep an eye out for more and you off 'em as fast as you can, alright? I'll let you know when I don't see anymore coming."
bill7.playertext = "Alright... I'll go stand by the mushrooms to start..."
bill7.quest = 76
bill7.flag = "!Z3_RancherBill_HelpingLarry_Active"
bill7.exec = { event -> startFluffQueue( event ) }
bill7.result = DONE
larryBill.addDialog(bill7, Larry)

def bill8 = [id:8]
bill8.npctext = "Yeah. Wait, I can see you have some rings. Would you help me?"
bill8.options = []
bill8.options << [text:"Yeah, I'm up for trying anyway.", result: 7]
bill8.options << [text:"Nah, you're on your own.", flag: "!Z3_RancherBill_HelpingLarry_Active", result: DONE]
larryBill.addDialog(bill8, Larry)

//---------------------------------------------------------------------------------------------
//Conversation when player is coming from Remo                                                 
//---------------------------------------------------------------------------------------------
def larryLeon = Larry.createConversation("larryLeon", true, "QuestStarted_92", "!QuestStarted_76", "!QuestCompleted_76", "!QuestCompleted_92")

def leon1 = [id:1]
leon1.npctext = "Well, hello. How'd you wind up here?"
leon1.result = 2
larryLeon.addDialog(leon1, Larry)

def leon2 = [id:2]
leon2.playertext = "Remo sent me because he heard you needed help keeping some fluffs out of the subway. I think he must be confused though... I can't imagine anything so cute and cuddly causing trouble."
leon2.result = 3
larryLeon.addDialog(leon2, Larry)

def leon3 = [id:3]
leon3.npctext = "The Fluffs might look like cute, little balls of love but they're a menace! The miserable things keep finding their way onto the subway tracks, forcing us to shut the trains down!"
leon3.result = 4
larryLeon.addDialog(leon3, Larry)

def leon4 = [id:4]
leon4.playertext = "Okay, why don't you stop them then?"
leon4.result = 5
larryLeon.addDialog(leon4, Larry)

def leon5 = [id:5]
leon5.npctext = "That's what I'm here for! I'm pretty sure they're getting underground through a hole hidden by that clump of mushrooms across the road... but without rings I can't do anything to stop them!"
leon5.playertext = "What do you mean? Why can't you stop them without rings?"
leon5.result = 6
larryLeon.addDialog(leon5, Larry)

def leon6 = [id:6]
leon6.npctext = "They're Animated, you can't hurt them at all without rings! It's just one of the weird things about them..."
leon6.options = []
leon6.options << [text:"Ohh, I see what you're saying. Hey, I have a few rings, maybe I can help you.", result: 7]
leon6.options << [text:"Maybe you should get some rings thing.", result: 8]
larryLeon.addDialog(leon6, Larry)

def leon7 = [id:7]
leon7.npctext = "That would be fantastic! They seem to come in waves. I'll keep an eye out for more and you off 'em as fast as you can, alright? I'll let you know when I don't see anymore coming."
leon7.playertext = "Alright... I'll go stand by the mushrooms to start..."
leon7.quest = 76
leon7.exec = { event -> 
	startFluffQueue( event )
	event.player.updateQuest(92, "Larry-VQS") 
	event.player.removeMiniMapQuestActorName("Larry-VQS")
	if(event.player.hasQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")) {
		event.player.unsetQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")
	}
}
leon7.result = DONE
larryLeon.addDialog(leon7, Larry)

def leon8 = [id:8]
leon8.npctext = "Yeah. Wait, I can see you have some rings. Would you help me?"
leon8.options = []
leon8.options << [text:"Yeah, I'm up for trying anyway.", result: 7]
leon8.options << [text:"Nah, you're on your own.", quest: 92, exec: { event -> event.player.removeMiniMapQuestActorName("Larry-VQS") }, result: DONE]
larryLeon.addDialog(leon8, Larry)

//---------------------------------------------------------------------------------------------
//Conversation when player is coming from Leon & completed Fluff Rush                          
//---------------------------------------------------------------------------------------------
def larryLeonTrain = Larry.createConversation("larryLeonTrain", true, "QuestStarted_92", "QuestCompleted_76", "!QuestCompleted_92")

def leonTrain1 = [id:1]
leonTrain1.npctext = "Well, hello. How'd you wind up here?"
leonTrain1.result = 2
larryLeonTrain.addDialog(leonTrain1, Larry)

def leonTrain2 = [id:2]
leonTrain2.playertext = "Remo sent me because he heard you needed help keeping some fluffs out of the subway. I think he must be confused though... I can't imagine anything so cute and cuddly causing trouble."
leonTrain2.result = 3
larryLeonTrain.addDialog(leonTrain2, Larry)

def leonTrain3 = [id:3]
leonTrain3.npctext = "But... you already helped me get the subway running. Don't you remember defeating the Fluff Rush?"
leonTrain3.playertext = "Oh... right. My bad."
leonTrain3.exec = { event ->
	event.player.removeMiniMapQuestActorName("Larry-VQS")
}
leonTrain3.quest = 92
leonTrain3.result = DONE
larryLeonTrain.addDialog(leonTrain3, Larry)

//---------------------------------------------------------------------------------------------
//Conversation when player is on Fluff Rush quest                                              
//---------------------------------------------------------------------------------------------
def larryFluffActive = Larry.createConversation("larryFluffActive", true, "QuestStarted_76:2")

def fluffActive1 = [id:1]
fluffActive1.npctext = "Not to be a nit, but I'm a little busy with these lousy fluffs!"
//fluffActive1.options = []
//fluffActive1.options << [text:"Yeah! I wanted to help with that.", result: 2]
//fluffActive1.options << [text:"I've been waiting to help... how long until you need me?", exec: { event -> yourQueueCheckPosition = fluffQueueList.size - 1; notifyYourFluffQueuePosition( event ) }, result: DONE]
fluffActive1.exec = { event -> 
	if(!event.player.hasQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")) {
		makeFluffMenu(event)
	} else {
		event.player.centerPrint("Fluff Rush menu already open.")
	}
}
fluffActive1.result = DONE
larryFluffActive.addDialog(fluffActive1, Larry)

/*def fluffActive2 = [id:2]
fluffActive2.npctext = "Oh, let me add you to the list."
fluffActive2.exec = { event -> startFluffQueue( event ) }
fluffActive2.result = DONE
larryFluffActive.addDialog(fluffActive2, Larry)*/

//---------------------------------------------------------------------------------------------
//Not in queue                                              
//---------------------------------------------------------------------------------------------
larryNotInQueue = Larry.createConversation("larryNotInQueue", true, "Z3_Larry_NotInQueue")

notInQueue1 = [id:1]
notInQueue1.npctext = "I don't see your name on my list. Would you like me to add you?"
/*notInQueue1.options = []
notInQueue1.options << [text:"Yes!", exec: { startFluffQueue( event ) }, flag: "!Z3_Larry_NotInQueue", result: DONE]
notInQueue1.options << [text:"No.", flag: "!Z3_Larry_NotInQueue", result: DONE]*/
notInQueue1.flag = "!Z3_Larry_NotInQueue"
notInQueue1.exec = { event -> makeFluffMenu(event) }
notInQueue1.result = DONE
larryNotInQueue.addDialog(notInQueue1, Larry)

//---------------------------------------------------------------------------------------------
//Conversation when player has completed Fluff Rush quest                                      
//---------------------------------------------------------------------------------------------
def larryFluffComplete = Larry.createConversation("larryFluffComplete", true, "QuestStarted_76:3", "!Z3_Larry_BillShipment_Break")
larryFluffComplete.setUrgent( true )

def fluffComplete1 = [id:1]
fluffComplete1.npctext = "Aha! You stopped those Fluffs nicely. I really appreciate it. Could you do me a favor and let Bill know that I'm headed back to Barton Town soon?"
fluffComplete1.options = []
fluffComplete1.options << [text:"Sure... where can I find Bill?", result:2]
fluffComplete1.options << [text:"I'm a little busy at the moment.", result:3]
larryFluffComplete.addDialog(fluffComplete1, Larry)

def fluffComplete2 = [id:2]
fluffComplete2.npctext = "Bill should be in front of his ranch directly to the west."
fluffComplete2.quest = 75
fluffComplete2.exec = { event -> 
	event.player.updateQuest(76, "Larry-VQS") 
	event.player.addMiniMapQuestActorName("Rancher Bill-VQS")
	event.player.removeMiniMapQuestActorName("Larry-VQS")
	if(event.player.hasQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")) {
		event.player.unsetQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")
	}
}
fluffComplete2.result =  DONE
larryFluffComplete.addDialog(fluffComplete2, Larry)

def fluffComplete3 = [id:3]
fluffComplete3.npctext = "Okay. Maybe you'll have time later? I know Bill won't ever trust you unless you prove yourself useful."
fluffComplete3.result = DONE
fluffComplete3.quest = 76
fluffComplete3.flag = "Z3_Larry_BillShipment_Break"
fluffComplete3.exec = { event ->
	event.player.removeMiniMapQuestActorName("Larry-VQS")
	if(event.player.hasQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")) {
		event.player.unsetQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")
	}
}
larryFluffComplete.addDialog(fluffComplete3, Larry)

//---------------------------------------------------------------------------------------------
//Conversation when player is on Bill's Shipment quest                                         
//---------------------------------------------------------------------------------------------
def larryShipmentBreak = Larry.createConversation("larryShipmentBreak", true, "Z3_Larry_BillShipment_Break")

def larryShipmentBreak1 = [id:1]
larryShipmentBreak1.npctext = "Hey, %p. Decided you want to talk to Bill?"
larryShipmentBreak1.options = []
larryShipmentBreak1.options << [text:"Yes! Where can I find him?", result: 2]
larryShipmentBreak1.options << [text:"No, not yet at least.", result: 3]
larryShipmentBreak.addDialog(larryShipmentBreak1, Larry)

def larryShipmentBreak2 = [id:2]
larryShipmentBreak2.npctext = "Bill should be in front of his ranch directly to the west."
larryShipmentBreak2.quest = 75
larryShipmentBreak2.exec = { event ->
	event.player.addMiniMapQuestActorName("Rancher Bill-VQS")
	if(event.player.hasQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")) {
		event.player.unsetQuestFlag(GLOBAL, "Z3_RancherBill_HelpingLarry_Active")
	}
}
larryShipmentBreak2.flag = "!Z3_Larry_BillShipment_Break"
larryShipmentBreak.addDialog(larryShipmentBreak2, Larry)

//---------------------------------------------------------------------------------------------
//Conversation when player is on Bill's Shipment quest                                         
//---------------------------------------------------------------------------------------------
def larryShipmentActive = Larry.createConversation("larryShipmentActive", true, "QuestStarted_75", "!QuestStarted_92")

def shipmentActive1 = [id:1]
shipmentActive1.npctext = "Please let Bill know that I'm headed back to Barton Town soon. He barely agreed to let me stay long enough to deal with the fluffs. You can find Bill by the gate to his ranch directly to the west."
shipmentActive1.result = DONE
larryShipmentActive.addDialog(shipmentActive1, Larry)

//---------------------------------------------------------------------------------------------
//GRANT - Fluff killing repeatable                                                             
//---------------------------------------------------------------------------------------------
def foreverFluffyGive = Larry.createConversation("foreverFluffyGive", true, "QuestCompleted_75", "!QuestStarted_279", "!Z3_Larry_ForeverFluffy_Allowed", "!Z3_Larry_ForeverFluffy_Denied")

def foreverFluffyGive1 = [id:1]
foreverFluffyGive1.npctext = "Hello again, %p."
foreverFluffyGive1.playertext = "Hey Larry, I thought you were going back to Barton Town. What gives?"
foreverFluffyGive1.result = 2
foreverFluffyGive.addDialog(foreverFluffyGive1, Larry)

def foreverFluffyGive2 = [id:2]
foreverFluffyGive2.npctext = "What gives is that these freaking fluffs keep coming!"
foreverFluffyGive2.playertext = "That sucks."
foreverFluffyGive2.result = 3
foreverFluffyGive.addDialog(foreverFluffyGive2, Larry)

def foreverFluffyGive3 = [id:3]
foreverFluffyGive3.npctext = "Yeah, but I have a new plan! I'm gonna wipe them out before they get a chance to rush past me. Hey! Maybe you can help?"
foreverFluffyGive3.options = []
foreverFluffyGive3.options << [text:"Sure, Larry, I'd be glad to help.", exec: { event ->
	if(event.player.getConLevel() < 3.1) {
		event.player.setQuestFlag(GLOBAL, "Z3_Larry_ForeverFluffy_Allowed")
		Larry.pushDialog(event.player, "foreverFluffyAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Larry_ForeverFluffy_Denied")
		Larry.pushDialog(event.player, "foreverFluffyDenied")
	}
}, result: DONE]
foreverFluffyGive3.options << [text:"Not now, Larry. Bigger things on my plate.", result: 4]
foreverFluffyGive.addDialog(foreverFluffyGive3, Larry)

def foreverFluffyGive4 = [id:4]
foreverFluffyGive4.npctext = "Okay, come back when you have time. Sure looks like the fluffs won't be wiped out any time soon."
foreverFluffyGive4.result = DONE
foreverFluffyGive.addDialog(foreverFluffyGive4, Larry)

//---------------------------------------------------------------------------------------------
//ALLOWED - Fluff killing repeatable                                                           
//---------------------------------------------------------------------------------------------
def foreverFluffyAllowed = Larry.createConversation("foreverFluffyAllowed", true, "Z3_Larry_ForeverFluffy_Allowed")

def foreverFluffyAllowed1 = [id:1]
foreverFluffyAllowed1.npctext = "I knew I could count on your help, %p. Just whack any fluffs you find in the woods nearby. Say about twenty of them."
foreverFluffyAllowed1.quest = 279
foreverFluffyAllowed1.flag = "!Z3_Larry_ForeverFluffy_Allowed"
foreverFluffyAllowed1.result = DONE
foreverFluffyAllowed.addDialog(foreverFluffyAllowed1, Larry)

//---------------------------------------------------------------------------------------------
//DENIED - Fluff killing repeatable                                                            
//---------------------------------------------------------------------------------------------
def foreverFluffyDenied = Larry.createConversation("foreverFluffyDenied", true, "Z3_Larry_ForeverFluffy_Denied")

def foreverFluffyDenied1 = [id:1]
foreverFluffyDenied1.npctext = "Seems to me you're wasting your time with my petty problems. I'm sure your talents would be better put to use elsewhere, %p."
foreverFluffyDenied1.flag = "!Z3_Larry_ForeverFluffy_Denied"
foreverFluffyDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 3.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
foreverFluffyDenied1.result = DONE
foreverFluffyDenied.addDialog(foreverFluffyDenied1, Larry)

//---------------------------------------------------------------------------------------------
//ACTIVE - Fluff killing repeatable                                                            
//---------------------------------------------------------------------------------------------
def foreverFluffyActive = Larry.createConversation("foreverFluffyActive", true, "QuestStarted_279:2")

def foreverFluffyActive1 = [id:1]
foreverFluffyActive1.npctext = "I don't think you've whacked enough fluffs yet."
foreverFluffyActive1.result = DONE
foreverFluffyActive.addDialog(foreverFluffyActive1, Larry)

//---------------------------------------------------------------------------------------------
//COMPLETE - Fluff killing repeatable                                                          
//---------------------------------------------------------------------------------------------
def foreverFluffyComplete = Larry.createConversation("foreverFluffyComplete", true, "QuestStarted_279:3")

def foreverFluffyComplete1 = [id:1]
foreverFluffyComplete1.npctext = "There you are, %p. Looks like you took out the fluffs I asked you to."
foreverFluffyComplete1.playertext = "Yeah... but it seems they're still coming."
foreverFluffyComplete1.result = 2
foreverFluffyComplete.addDialog(foreverFluffyComplete1, Larry)

def foreverFluffyComplete2 = [id:2]
foreverFluffyComplete2.npctext = "You're telling me! We'll get them yet, though. What do you say, up for another round of fluff whacking?"
foreverFluffyComplete2.options = []
foreverFluffyComplete2.options << [text:"You betcha!", exec: { event ->
	event.player.updateQuest(279, "Larry-VQS")
	event.player.removeMiniMapQuestActorName("Larry-VQS")
	if(event.player.getConLevel() < 3.1) {
		event.player.setQuestFlag(GLOBAL, "Z3_Larry_ForeverFluffy_AllowedAgain")
		Larry.pushDialog(event.player, "foreverFluffyAllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Larry_ForeverFluffy_Denied")
		Larry.pushDialog(event.player, "foreverFluffyDenied")
	}
}, result: DONE]
foreverFluffyComplete2.options << [text:"Maybe later, Larry.", result: 3]
foreverFluffyComplete.addDialog(foreverFluffyComplete2, Larry)

def foreverFluffyComplete3 = [id:3]
foreverFluffyComplete3.npctext = "Ah... later indeed."
foreverFluffyComplete3.quest = 279
foreverFluffyComplete3.exec = { event -> event.player.removeMiniMapQuestActorName("Larry-VQS") }
foreverFluffyComplete3.result = DONE
foreverFluffyComplete.addDialog(foreverFluffyComplete3, Larry)

//---------------------------------------------------------------------------------------------
//ALLOWED AGAIN - Fluff killing repeatable                                                     
//---------------------------------------------------------------------------------------------
def foreverFluffyAllowedAgain = Larry.createConversation("foreverFluffyAllowedAgain", true, "Z3_Larry_ForeverFluffy_AllowedAgain")

def foreverFluffyAllowedAgain1 = [id:1]
foreverFluffyAllowedAgain1.npctext = "Awesome, I guess you know what to do. Twenty again."
foreverFluffyAllowedAgain1.exec = { event ->
	event.player.updateQuest(279, "Larry-VQS")
}
foreverFluffyAllowedAgain1.flag = "!Z3_Larry_ForeverFluffy_AllowedAgain"
foreverFluffyAllowedAgain1.result = DONE
foreverFluffyAllowedAgain.addDialog(foreverFluffyAllowedAgain1, Larry)

//--------------------------------------------------------------------------------------------------------------------------------------
//This script creates a queue for the Fluff Rush quest                                                                                  
//--------------------------------------------------------------------------------------------------------------------------------------
playerListBF1_706 = []
fluffQueueList = []
playerAuthorized = false
playerIsInQueue = false
nextInQueue = null
fluffCanFail1 = false
fluffCanFail2 = false
fluffRushActive = false

//Adds the player to the queue if they are not a member of a Crew already in the queue
def startFluffQueue( event ) {
	playerAuthorized = true
	checkListPosition = fluffQueueList.size() - 1
	checkFluffQueueForDupes( event )
}

def checkFluffQueueForDupes( event ) {
	if( checkListPosition >= 0 ) {
		nextRegisteredPlayer = fluffQueueList.get( checkListPosition )
		if( nextRegisteredPlayer.getCrew().contains( event.player ) ) {
			event.player.centerPrint( "You can't register now because you're a member of a Crew that is already signed up.")
			playerAuthorized = false
		}
		checkListPosition --
		checkFluffQueueForDupes( event )
	} else {
		if( playerAuthorized == true ) {
			addToFluffQueue( event )
			//println "##### Adding ${event.player} to the Fluff Queue #####"
		}
	}
}

def addToFluffQueue( event ) {
	fluffQueueList << event.player //Now the player is in the queue!!
	event.player.getCrew().each { it.centerPrint( "You are now signed up for the Fluff Rush!") }
	myManager.schedule(2) { notifyFluffQueuePosition( event ) }
	myManager.schedule(4) { event.player.getCrew().each { it.centerPrint( "You will be notified when it is your turn." ) } }
	myManager.schedule(6) { event.player.getCrew().each { it.centerPrint( "When notified you will have 10 seconds to get in position to stop the fluffs." ) } }
	myManager.schedule(8) { event.player.getCrew().each { it.centerPrint( "Stay in sight or Larry will assume you left and ask another for help!" ) } }
}

def notifyFluffQueuePosition( event ) {
	if( event.player == fluffQueueList.get( 0 ) ) {
		nextInQueue = fluffQueueList.get( 0 )
		checkIfStillFluffy()
	} else {
		event.player.getCrew().each { it.centerPrint( "You are position ${fluffQueueList.indexOf(event.player) + 1} out of ${fluffQueueList.size()}." ) }
	}
}

def makeFluffMenu(event) {
	alreadyInQueue = false
	event.player.setQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")
	event.player.getCrew().each() {
		if(fluffQueueList.contains(it)) { alreadyInQueue = true}
	}
	if(alreadyInQueue == true) {
		uiButtonMenu(event.player, "fluffInQueueMenu", MENU_TITLE, MENU_QUEUE_DESCRIP, MENU_QUEUE_OPTIONS, 300) { menuEvent ->
			if(menuEvent.selection == MENU_QUEUE_OPTIONS.get(0)) {
				event.player.unsetQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")
				yourQueueCheckPosition = fluffQueueList.size - 1
				playerIsInQueue = false
				notifyYourFluffQueuePosition( event )
			}
			if(menuEvent.selection == MENU_QUEUE_OPTIONS.get(1)) {
				event.player.unsetQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")
			}
		}		
	} else {
		uiButtonMenu(event.player, "fluffNotInQueueMenu", MENU_TITLE, MENU_NON_QUEUE_DESCRIP, MENU_NON_QUEUE_OPTIONS, 300) { menuEvent ->
			if(menuEvent.selection == MENU_NON_QUEUE_OPTIONS.get(0)) {
				event.player.unsetQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")
				startFluffQueue(event)
			}
			if(menuEvent.selection == MENU_NON_QUEUE_OPTIONS.get(1)) {
				event.player.centerPrint("The Fluff Rush queue currently has ${fluffQueueList.size()} people waiting to play.")
				myManager.schedule(2) { makeFluffMenu(event) }
			}
			if(menuEvent.selection == MENU_NON_QUEUE_OPTIONS.get(2)) {
				event.player.unsetQuestFlag(GLOBAL, "Z3_Larry_MenuOpen")
			}
		}
	}
}

def notifyYourFluffQueuePosition( event ) {
	if( yourQueueCheckPosition >= 0 ) {
		nextRegisteredPlayerCheck = fluffQueueList.get( yourQueueCheckPosition )
		if(nextRegisteredPlayerCheck.getCrew().contains(event.player) ) {
			//Tells the player their queue position
			
			event.player.centerPrint( "You are position ${fluffQueueList.indexOf(event.player) + 1} out of ${fluffQueueList.size()}." )
			playerIsInQueue = true
		}
		yourQueueCheckPosition --
		notifyYourFluffQueuePosition( event )
	} else if( playerIsInQueue != true) {
		//Tells the player they are not in the queue
		event.player.setQuestFlag(GLOBAL, "Z3_Larry_NotInQueue")
		Larry.pushDialog( event.player, "larryNotInQueue")
	}
}



def notifyAllFluffQueuePosition() {
	//println "fluffQueuePosition = ${fluffQueuePosition}"
	if( fluffQueuePosition > 0 ) {
		tellThisCrew = fluffQueueList.get( fluffQueuePosition )
		if(isOnline(tellThisCrew) ) {
			tellThisCrew.getCrew().each { it.centerPrint( "You are in position ${fluffQueuePosition + 1} out of ${fluffQueueList.size()}." ) }
			fluffQueuePosition --
		} else {
			fluffQueueList.remove( tellThisCrew )
			fluffQueuePosition --
		}
		notifyAllFluffQueuePosition()
	} else {
		nextInQueue = fluffQueueList.get( 0 )
		checkIfStillFluffy()
	}
}

def moveToNextCrewInQueue() {
	fluffQueueList.remove( fluffQueueList.get( 0 ) )
	//println "fluffQueueList.size() = ${fluffQueueList.size()}"
	if( fluffQueueList.size() > 0 ) {
		if( isOnline( fluffQueueList.get( 0 ) ) ) {
			nextInQueue = fluffQueueList.get( 0 )
			fluffQueuePosition = fluffQueueList.size() -1
			notifyAllFluffQueuePosition()
		} else {
			moveToNextCrewInQueue()
		}
	}
}

def checkIfStillFluffy() {
	if( playerListBF1_706.contains( nextInQueue ) ) {
		myManager.schedule(10) {
			authorizedCrew = nextInQueue.getCrew()
			authorizedCrew.each { 
				it.centerPrint( "Larry needs your help with the next group of fluffs!" ) 
				it.centerPrint( "You have 10 seconds to get into position." ) 
			}
			fluffRushActive = true
			myManager.schedule(1) { startFluffRush() }
		}
	} else {
		nextInQueue.centerPrint( "You were removed from the fluff rush queue because Larry couldn't find you!" )
		moveToNextCrewInQueue()
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------
//This script controls the event for the Fluff Rush quest                                                                               
//--------------------------------------------------------------------------------------------------------------------------------------

FLUFF_RUSH_ROOM = myRooms.BF1_706
FLUFF_RUSH_SPAWN_ROOM = myRooms.BF1_705
FLUFF_RUSH_WAVES = 4
FLUFF_RUSH_WAVE_TIMER = 15

//TODO - Variable # of spawns based on group size

fluffTimerFail = null
nextScheduledWave = null
fluffRushLock = null
fluffRushWave = 0
fluffRushClears = 0

failTrigger1 = "failTrigger1"
failTrigger2 = "failTrigger2"
FLUFF_RUSH_ROOM.createTriggerZone(failTrigger1, 860, 580, 960, 680)
FLUFF_RUSH_ROOM.createTriggerZone(failTrigger2, 880, 515, 980, 615)

fluff1Map = new HashMap()
fluff2Map = new HashMap()
fluffArray = []
failArray = []

Fluff1XArray = [ 235, 270, 300 ]
Fluff1YArray = [ 475, 515, 570 ] 
Fluff1XList = random(Fluff1XArray)
Fluff1YList = random(Fluff1YArray)

Fluff2XArray = [ 470, 490, 515 ]
Fluff2YArray = [ 55, 75, 95 ]
Fluff2XList = random(Fluff2XArray)
Fluff2YList = random(Fluff2YArray)

Fluff1Spawner = FLUFF_RUSH_SPAWN_ROOM.spawnSpawner( "Fluff1Spawner", "fluffrushfluff", 10 )
Fluff1Spawner.setPos(860, 560)
Fluff1Spawner.setHome("BF1_706", 635, 585)
Fluff1Spawner.setWaitTime(1, 2)
Fluff1Spawner.setDispositionForChildren( "coward" )
Fluff1Spawner.setCowardLevelForChildren( 0 )
Fluff1Spawner.setRFH( true )
Fluff1Spawner.childrenWander(false)
Fluff1Spawner.setMonsterLevelForChildren( 1.8 ) 
Fluff1Spawner.addPatrolPointForChildren( myRooms.BF1_706, Fluff1XList, Fluff1YList, 1 )
Fluff1Spawner.addPatrolPointForChildren( myRooms.BF1_706, 910, 630, 20 )

Fluff1Spawner.stopSpawning()

Fluff2Spawner = FLUFF_RUSH_SPAWN_ROOM.spawnSpawner( "Fluff2Spawner", "fluffrushfluff", 10)
Fluff2Spawner.setPos(540, 85)
Fluff2Spawner.setHome("BF1_706", 900, 385)
Fluff2Spawner.setWaitTime(1, 2)
Fluff2Spawner.setDispositionForChildren( "coward" )
Fluff2Spawner.setCowardLevelForChildren( 0 )
Fluff2Spawner.setRFH( true )
Fluff2Spawner.childrenWander(false)
Fluff2Spawner.setMonsterLevelForChildren( 1.8 ) 
Fluff2Spawner.addPatrolPointForChildren( myRooms.BF1_706, Fluff2XList, Fluff2YList, 1 )
Fluff2Spawner.addPatrolPointForChildren( myRooms.BF1_706, 930, 565, 20 )

Fluff2Spawner.stopSpawning()

def failTask1 = myManager.onTriggerIn(FLUFF_RUSH_ROOM, failTrigger1) { event ->
	if(fluffArray.contains( event.actor ) && fluffCanFail2 == false) {
		fluffCanFail1 = true
		//println "### ${event.actor} entered the failTrigger1! ###"
		myManager.schedule(1) { // Fail them 1 sec after fluff enters trigger at stairs
			if(fluffCanFail1 == true) {
				fluffRushWave = 99
				//println "You fail, ${nextInQueue}"
				Larry.say("Oh no, ${nextInQueue}! The fluff rush got through!")
				nextInQueue.getCrew().each { 
					if(playerListBF1_706.contains(it) ) { it.centerPrint("You failed to stop the fluff rush!") }
				}
						
				fluffArray.each { it.dispose() }
				fluffArray.clear()
				nextScheduledWave?.cancel()
			
				myManager.schedule(5) { 
					resetFluffRush() 
					fluffTimerFail.cancel()
				}
					
				runOnDeath(Fluff1) { failTask1.cancel() }
			}
		}
	}
}
def failTask2 = myManager.onTriggerIn(FLUFF_RUSH_ROOM, failTrigger2) { event ->
	if(fluffArray.contains( event.actor ) && fluffCanFail1 == false) {
		fluffCanFail2 = true
		//println "### ${event.actor} entered the failTrigger2! ###"
		myManager.schedule(1) { // Fail them 1 sec after fluff enters trigger at stairs
			if(fluffCanFail2 == true) {
				fluffRushWave = 99
				//println "You fail, ${nextInQueue}"
				Larry.say("Oh no, ${nextInQueue}! The fluff rush got through!")
				nextInQueue.getCrew().each { 
					if(playerListBF1_706.contains(it) ) { it.centerPrint("You failed to stop the fluff rush!") }
				}
						
				fluffArray.each { it.dispose() }
				fluffArray.clear()
				nextScheduledWave?.cancel()
			
				myManager.schedule(5) { 
					resetFluffRush() 
					fluffTimerFail.cancel()
				}
					
				runOnDeath(Fluff2) { failTask2.cancel() }
			}
		}
	}
}

def startFluffRush() {
	Larry.say("Quick, ${nextInQueue}, get ready! Here comes the fluff rush!")

	waveAnnounce = [ "Don't stop yet ${nextInQueue}, that's not all the fluffs!", "Good work ${nextInQueue}, but you're not done yet.", "Great! You stopped that group... but there's another coming, ${nextInQueue}." ]
	
	//println "######fluffRushWave = ${fluffRushWave} at time of startFluffRush()######"
	nextFluffWave()
	fluffTimerFail = myManager.schedule(300) {
		if(fluffRushActive == true) {
			resetFluffRush()
			Larry.say("Geez, ${nextInQueue}, if I knew you were just gonna stand around I woulda gotten somebody else. In fact, I think I will.")
		}
	}
}

def nextFluffWave() {
	if( fluffRushActive == true ) {
		fluffRushWave++
		//println "######fluffRushWave = ${fluffRushWave} at time of nextFluffWave()######"
		if(fluffRushWave>FLUFF_RUSH_WAVES) return
	
		Fluff1 = Fluff1Spawner.forceSpawnNow()
		Fluff2 = Fluff2Spawner.forceSpawnNow()
		
		fluff1Map.put(Fluff1, Fluff2)
		fluff2Map.put(Fluff2, Fluff1)
	
		fluffArray << Fluff1
		fluffArray << Fluff2
	
		runOnDeath(Fluff1) { event ->
			if(fluff1Map.get(event.actor).isDead()) {
				fluffCanFail1 = false
				fluffRushClears++
				//println "##### fluffRushClears = ${fluffRushClears} #####"
				//fluffArray.remove(event.actor)
				if(fluffRushClears < FLUFF_RUSH_WAVES) { Larry.say( random(waveAnnounce) ) }
				//println "Finished wave ${fluffRushClears}"
	
				if(fluffRushClears >= FLUFF_RUSH_WAVES) {
					nextInQueue.getCrew().each { 
						it.centerPrint("You stopped the fluff rush!") 
						if( it.isOnQuest( 76, 2 ) ) { it.updateQuest(76, "Larry-VQS"); myManager.schedule(1) { Larry.pushDialog(it, "larryFluffComplete") } }
						fluffTimerFail.cancel()
					}
					//println "SUCCESS!"
					myManager.schedule(10) { 
						resetFluffRush() 
					}
				}
			}
		}
		runOnDeath(Fluff2) { event ->
			if(fluff2Map.get(event.actor).isDead()) {
				fluffCanFail2 = false
				fluffRushClears++
				//println "##### fluffRushClears = ${fluffRushClears} #####"
				//fluffArray.remove(event.actor)
				if(fluffRushClears < FLUFF_RUSH_WAVES) { Larry.say( random(waveAnnounce) ) }
				//println "Finished wave ${fluffRushClears}"
	
				if(fluffRushClears >= FLUFF_RUSH_WAVES) {
					nextInQueue.getCrew().each { 
						it.centerPrint("You stopped the fluff rush!") 
						if( it.isOnQuest( 76, 2 ) ) { it.updateQuest(76, "Larry-VQS"); myManager.schedule(1) { Larry.pushDialog(it, "larryFluffComplete") } }
						fluffTimerFail.cancel()
					}
					//println "SUCCESS!"
					myManager.schedule(10) { 
						resetFluffRush() 
					}
				}
			}
		}
		if(fluffRushWave < FLUFF_RUSH_WAVES) { nextScheduledWave = myManager.schedule(FLUFF_RUSH_WAVE_TIMER) { 
			nextFluffWave() 
			//println "##### Scheduled nextFluffWave when fluffRushWave = ${fluffRushWave} #####"
		} }
	}
}

def synchronized resetFluffRush() {
	if(fluffRushActive == true) {
		//println "[GBF] **Fluff Rush Reset Successful**"
		fluffRushWave = 0
		fluffRushClears = 0
		fluffRushActive = false
		fluffCanFail1 = false
		fluffCanFail2 = false
		nextInQueue = null
		moveToNextCrewInQueue()

		failArray.each { it.cancel() }
		failArray.clear()
		//println "fluffArray = ${fluffArray}"
		fluffArray.each { it.dispose() }
		fluffArray.clear()
		fluff1Map.clear()
		fluff2Map.clear()
	}
}
