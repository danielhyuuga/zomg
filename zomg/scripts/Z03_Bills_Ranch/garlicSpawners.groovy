//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

SPROUT_PERCENTAGE = 55 //The percentage at which sprouts will drop when a player is on the Plantin' Garlic quest

//----------------------------------------------------
//Garlic Spawners for BF1_405                         
//----------------------------------------------------
spawner_405_1 = myRooms.BF1_405.spawnSpawner( "bf1_405_spawner1", "garlic", 1)
spawner_405_1.stopSpawning()
spawner_405_1.setPos( 900, 590 )
spawner_405_1.setHome( "BF1_405", 900, 590 )
spawner_405_1.setSpawnWhenPlayersAreInRoom( true )
spawner_405_1.setWaitTime( 50 , 70 )
spawner_405_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_405_1.childrenWander( true )
spawner_405_1.setMonsterLevelForChildren( 2.0 )

spawner_405_2 = myRooms.BF1_405.spawnSpawner( "bf1_405_spawner2", "garlic", 1)
spawner_405_2.stopSpawning()
spawner_405_2.setPos( 595, 410 )
spawner_405_2.setHome( "BF1_405", 595, 410 )
spawner_405_2.setSpawnWhenPlayersAreInRoom( true )
spawner_405_2.setWaitTime( 50 , 70 )
spawner_405_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_405_2.childrenWander( true )
spawner_405_2.setMonsterLevelForChildren( 2.0 )

//----------------------------------------------------
//Garlic Spawners for BF1_406                         
//----------------------------------------------------
spawner_406_1 = myRooms.BF1_406.spawnSpawner( "BF1_406_Spawner1", "garlic", 1)
spawner_406_1.stopSpawning()
spawner_406_1.setPos( 125, 80 )
spawner_406_1.setInitialMoveForChildren( "BF1_406", 180, 140 )
spawner_406_1.setHome( "BF1_406", 180, 140 )
spawner_406_1.setSpawnWhenPlayersAreInRoom( true )
spawner_406_1.setWaitTime( 50 , 70 )
spawner_406_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_406_1.childrenWander( true )
spawner_406_1.setMonsterLevelForChildren( 2.3 )

spawner_406_2 = myRooms.BF1_406.spawnSpawner( "BF1_406_Spawner4", "garlic", 1)
spawner_406_2.stopSpawning()
spawner_406_2.setPos( 125, 110 )
spawner_406_2.setInitialMoveForChildren( "BF1_406", 770, 400 )
spawner_406_2.setHome( "BF1_406", 770, 400 )
spawner_406_2.setSpawnWhenPlayersAreInRoom( true )
spawner_406_2.setWaitTime( 50 , 70 )
spawner_406_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_406_2.childrenWander (true)
spawner_406_2.setMonsterLevelForChildren( 2.3 )

//----------------------------------------------------
//Garlic Spawners for BF1_505                          
//----------------------------------------------------
spawner_505_1 = myRooms.BF1_505.spawnSpawner( "BF1_505_Spawner", "garlic", 1)
spawner_505_1.stopSpawning()
spawner_505_1.setPos( 420, 400 )
spawner_505_1.setInitialMoveForChildren( "BF1_505", 890, 90 )
spawner_505_1.setHome( "BF1_505", 890, 90 )
spawner_505_1.setSpawnWhenPlayersAreInRoom( true )
spawner_505_1.setWaitTime( 50 , 70 )
spawner_505_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_505_1.childrenWander( true )
spawner_505_1.setMonsterLevelForChildren( 1.7 )

//----------------------------------------------------
//Garlic Spawners for BF1_605                         
//----------------------------------------------------
spawner_605_1 = myRooms.BF1_605.spawnSpawner( "BF1_605_Spawner1", "garlic", 1)
spawner_605_1.stopSpawning()
spawner_605_1.setPos( 260, 555 )
spawner_605_1.setInitialMoveForChildren( "BF1_605", 900, 80 )
spawner_605_1.setHome( "BF1_605", 900, 80 )
spawner_605_1.setSpawnWhenPlayersAreInRoom( true )
spawner_605_1.setWaitTime( 50 , 70 )
spawner_605_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_605_1.childrenWander( true )
spawner_605_1.setMonsterLevelForChildren( 1.8 )

//----------------------------------------------------
//Garlic Spawners for BF1_105                         
//----------------------------------------------------
spawner_105_1 = myRooms.BF1_105.spawnSpawner( "bf1_105_spawner1", "garlic", 1)
spawner_105_1.stopSpawning()
spawner_105_1.setPos( 565, 240 )
spawner_105_1.setInitialMoveForChildren( "BF1_105", 110, 560 )
spawner_105_1.setHome( "BF1_105", 110, 560 )
spawner_105_1.setSpawnWhenPlayersAreInRoom( true )
spawner_105_1.setWaitTime( 50 , 70 )
spawner_105_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_105_1.childrenWander( true )
spawner_105_1.setMonsterLevelForChildren( 2.4 )

spawner_105_2 = myRooms.BF1_105.spawnSpawner( "bf1_105_spawner2", "garlic", 1)
spawner_105_2.stopSpawning()
spawner_105_2.setPos( 575, 240 )
spawner_105_2.setInitialMoveForChildren( "BF1_105", 310, 350 )
spawner_105_2.setHome( "BF1_105", 310, 350 )
spawner_105_2.setSpawnWhenPlayersAreInRoom( true )
spawner_105_2.setWaitTime( 50 , 70 )
spawner_105_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_105_2.childrenWander( true )
spawner_105_2.setMonsterLevelForChildren( 2.4 )

spawner_105_3 = myRooms.BF1_105.spawnSpawner( "bf1_105_spawner4", "garlic", 1)
spawner_105_3.stopSpawning()
spawner_105_3.setPos( 595, 240 )
spawner_105_3.setInitialMoveForChildren( "BF1_105", 925, 100 )
spawner_105_3.setHome( "BF1_105", 925, 100 )
spawner_105_3.setSpawnWhenPlayersAreInRoom( true )
spawner_105_3.setWaitTime( 50 , 70 )
spawner_105_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_105_3.childrenWander( true )
spawner_105_3.setMonsterLevelForChildren( 2.4 )

//----------------------------------------------------
//Garlic Spawners for BF1_106                         
//----------------------------------------------------
spawner_106_1 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner1", "garlic", 1)
spawner_106_1.stopSpawning()
spawner_106_1.setPos( 50, 410 )
spawner_106_1.setInitialMoveForChildren( "BF1_106", 210, 560 )
spawner_106_1.setHome( "BF1_106", 210, 560 )
spawner_106_1.setSpawnWhenPlayersAreInRoom( true )
spawner_106_1.setWaitTime( 50 , 70 )
spawner_106_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_106_1.childrenWander( true )
spawner_106_1.setMonsterLevelForChildren( 2.2 )

spawner_106_2 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner2", "garlic", 1)
spawner_106_2.stopSpawning()
spawner_106_2.setPos( 50, 420 )
spawner_106_2.setInitialMoveForChildren( "BF1_106", 175, 225 )
spawner_106_2.setHome( "BF1_106", 175, 225 )
spawner_106_2.setSpawnWhenPlayersAreInRoom( true )
spawner_106_2.setWaitTime( 50, 70 )
spawner_106_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_106_2.childrenWander( true )
spawner_106_2.setMonsterLevelForChildren( 2.2 )

spawner_106_3 = myRooms.BF1_106.spawnSpawner( "bf1_106_spawner3", "garlic", 1)
spawner_106_3.stopSpawning()
spawner_106_3.setPos( 50, 430 )
spawner_106_3.setInitialMoveForChildren( "BF1_106", 760, 200 )
spawner_106_3.setHome( "BF1_106", 760, 200 )
spawner_106_3.setSpawnWhenPlayersAreInRoom( true )
spawner_106_3.setWaitTime( 50 , 70 )
spawner_106_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_106_3.childrenWander( true )
spawner_106_3.setMonsterLevelForChildren( 2.2 )

//----------------------------------------------------
//Garlic Spawners for BF1_204                         
//----------------------------------------------------
spawner_204_1 = myRooms.BF1_204.spawnSpawner( "bf1_204_spawner1", "garlic", 1)
spawner_204_1.stopSpawning()
spawner_204_1.setPos( 900, 365 )
spawner_204_1.setHome( "BF1_204", 900, 365 )
spawner_204_1.setSpawnWhenPlayersAreInRoom( true )
spawner_204_1.setWaitTime( 50 , 70 )
spawner_204_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_204_1.childrenWander( true )
spawner_204_1.setMonsterLevelForChildren( 2.2 )

spawner_204_2 = myRooms.BF1_204.spawnSpawner( "bf1_204_spawner2", "garlic", 1)
spawner_204_2.stopSpawning()
spawner_204_2.setPos( 880, 595 )
spawner_204_2.setHome( "BF1_204", 880, 595 )
spawner_204_2.setSpawnWhenPlayersAreInRoom( true )
spawner_204_2.setWaitTime( 50 , 70 )
spawner_204_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_204_2.childrenWander( true )
spawner_204_2.setMonsterLevelForChildren( 2.2 )

//----------------------------------------------------
//Garlic Spawners for BF1_205                         
//----------------------------------------------------
spawner_205_1 = myRooms.BF1_205.spawnSpawner( "spawner_205_1", "garlic", 1)
spawner_205_1.stopSpawning()
spawner_205_1.setPos( 420, 40 )
spawner_205_1.setInitialMoveForChildren( "BF1_205", 165, 585 )
spawner_205_1.setHome( "BF1_205", 165, 585 )
spawner_205_1.setSpawnWhenPlayersAreInRoom( true )
spawner_205_1.setWaitTime( 50 , 70 )
spawner_205_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_205_1.childrenWander( true )
spawner_205_1.setMonsterLevelForChildren( 2.1 )

spawner_205_2 = myRooms.BF1_205.spawnSpawner( "spawner_205_2", "garlic", 1)
spawner_205_2.stopSpawning()
spawner_205_2.setPos( 420, 50 )
spawner_205_2.setInitialMoveForChildren( "BF1_205", 375, 400 )
spawner_205_2.setHome( "BF1_205", 375, 400 )
spawner_205_2.setSpawnWhenPlayersAreInRoom( true )
spawner_205_2.setWaitTime( 50 , 70 )
spawner_205_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_205_2.childrenWander( true )
spawner_205_2.setMonsterLevelForChildren( 2.1 )

spawner_205_3 = myRooms.BF1_205.spawnSpawner( "spawner_205_3", "garlic", 1)
spawner_205_3.stopSpawning()
spawner_205_3.setPos( 420, 60 )
spawner_205_3.setInitialMoveForChildren( "BF1_205", 120, 145 )
spawner_205_3.setHome( "BF1_205", 120, 145 )
spawner_205_3.setSpawnWhenPlayersAreInRoom( true )
spawner_205_3.setWaitTime( 50 , 70 )
spawner_205_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_205_3.childrenWander( true )
spawner_205_3.setMonsterLevelForChildren( 2.1 )

spawner_205_4 = myRooms.BF1_205.spawnSpawner( "spawner_205_4", "garlic", 1)
spawner_205_4.stopSpawning()
spawner_205_4.setPos( 420, 70 )
spawner_205_4.setInitialMoveForChildren( "BF1_205", 920, 465 )
spawner_205_4.setHome( "BF1_205", 920, 465 )
spawner_205_4.setSpawnWhenPlayersAreInRoom( true )
spawner_205_4.setWaitTime( 50 , 70 )
spawner_205_4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_205_4.childrenWander( true )
spawner_205_4.setMonsterLevelForChildren( 2.1 )

//----------------------------------------------------
//Garlic Spawners for BF1_206                         
//----------------------------------------------------
spawner_206_1 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner1", "garlic", 1)
spawner_206_1.stopSpawning()
spawner_206_1.setPos( 100, 570 )
spawner_206_1.setHome( "BF1_206", 100, 570 )
spawner_206_1.setSpawnWhenPlayersAreInRoom( true )
spawner_206_1.setWaitTime( 50 , 70 )
spawner_206_1.setWanderBehaviorForChildren(50, 150, 2, 8, 250 )
spawner_206_1.childrenWander( true )
spawner_206_1.setMonsterLevelForChildren( 1.6 )

spawner_206_2 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner3", "garlic", 1)
spawner_206_2.stopSpawning()
spawner_206_2.setPos( 290, 280 )
spawner_206_2.setHome( "BF1_206", 190, 280 )
spawner_206_2.setSpawnWhenPlayersAreInRoom( true )
spawner_206_2.setWaitTime( 50 , 70 )
spawner_206_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 250 )
spawner_206_2.childrenWander( true )
spawner_206_2.setMonsterLevelForChildren( 1.6 )

spawner_206_3 = myRooms.BF1_206.spawnSpawner( "bf1_206_spawner4", "garlic", 1)
spawner_206_3.stopSpawning()
spawner_206_3.setPos( 170, 105 )
spawner_206_3.setHome( "BF1_206", 170, 105 )
spawner_206_3.setSpawnWhenPlayersAreInRoom( true )
spawner_206_3.setWaitTime( 50 , 70 )
spawner_206_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 250 )
spawner_206_3.childrenWander( true )
spawner_206_3.setMonsterLevelForChildren( 1.6 )

//----------------------------------------------------
//Garlic Spawners for BF1_305                         
//----------------------------------------------------
spawner_305_1 = myRooms.BF1_305.spawnSpawner( "bf1_305_spawner1", "garlic", 1)
spawner_305_1.stopSpawning()
spawner_305_1.setPos( 190, 150 )
spawner_305_1.setHome( "BF1_305", 190, 150 )
spawner_305_1.setSpawnWhenPlayersAreInRoom( true )
spawner_305_1.setWaitTime( 50, 70 )
spawner_305_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_305_1.childrenWander( true )
spawner_305_1.setMonsterLevelForChildren( 2.0 )

spawner_305_2 = myRooms.BF1_305.spawnSpawner( "bf1_305_spawner2", "garlic", 1)
spawner_305_2.stopSpawning()
spawner_305_2.setPos( 505, 330 )
spawner_305_2.setHome( "BF1_305", 505, 330 )
spawner_305_2.setSpawnWhenPlayersAreInRoom( true )
spawner_305_2.setWaitTime( 50 , 70 )
spawner_305_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_305_2.childrenWander( true )
spawner_305_2.setMonsterLevelForChildren( 2.0 )

spawner_305_3 = myRooms.BF1_305.spawnSpawner( "bf1_305_spawner3", "garlic", 1)
spawner_305_3.stopSpawning()
spawner_305_3.setPos( 865, 150 )
spawner_305_3.setHome( "BF1_305", 865, 150 )
spawner_305_3.setSpawnWhenPlayersAreInRoom( true )
spawner_305_3.setWaitTime( 50 , 70 )
spawner_305_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_305_3.childrenWander( true )
spawner_305_3.setMonsterLevelForChildren( 2.0 )

//----------------------------------------------------
//Garlic Spawners for BF1_502                         
//----------------------------------------------------
spawner_502_1 = myRooms.BF1_502.spawnSpawner( "bf1_502_spawner1", "garlic", 1)
spawner_502_1.stopSpawning()
spawner_502_1.setPos( 725, 95 )
spawner_502_1.setHome( "BF1_502", 725, 95 )
spawner_502_1.setSpawnWhenPlayersAreInRoom( true )
spawner_502_1.setWaitTime( 50 , 70 )
spawner_502_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_502_1.childrenWander( true )
spawner_502_1.setMonsterLevelForChildren( 2.0 )

spawner_502_2 = myRooms.BF1_502.spawnSpawner( "bf1_502_spawner2", "garlic", 1)
spawner_502_2.stopSpawning()
spawner_502_2.setPos( 900, 190 )
spawner_502_2.setHome( "BF1_502", 900, 190 )
spawner_502_2.setSpawnWhenPlayersAreInRoom( true )
spawner_502_2.setWaitTime( 50 , 70 )
spawner_502_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_502_2.childrenWander( true )
spawner_502_2.setMonsterLevelForChildren( 2.0 )

//----------------------------------------------------
//Garlic Spawners for BF1_506                         
//----------------------------------------------------
spawner_506_1 = myRooms.BF1_506.spawnSpawner( "BF1_506_Spawner1", "garlic", 1)
spawner_506_1.stopSpawning()
spawner_506_1.setPos( 200, 460 )
spawner_506_1.setInitialMoveForChildren( "BF1_506", 120, 575 )
spawner_506_1.setHome( "BF1_506", 120, 575 )
spawner_506_1.setSpawnWhenPlayersAreInRoom( true )
spawner_506_1.setWaitTime( 50 , 70 )
spawner_506_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_506_1.childrenWander (true)
spawner_506_1.setMonsterLevelForChildren( 2.1 )

spawner_506_2 = myRooms.BF1_506.spawnSpawner( "BF1_506_Spawner2", "garlic", 1)
spawner_506_2.stopSpawning()
spawner_506_2.setPos( 200, 450 )
spawner_506_2.setInitialMoveForChildren( "BF1_506", 770, 400 )
spawner_506_2.setHome( "BF1_506", 770, 400 )
spawner_506_2.setSpawnWhenPlayersAreInRoom( true )
spawner_506_2.setWaitTime( 50 , 70 )
spawner_506_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_506_2.childrenWander (true)
spawner_506_2.setMonsterLevelForChildren( 2.1 )

//----------------------------------------------------
//Garlic Spawners for BF1_506                         
//----------------------------------------------------
spawner_5_1 = myRooms.BF1_5.spawnSpawner( "bf1_5_spawner1", "garlic", 1)
spawner_5_1.stopSpawning()
spawner_5_1.setPos( 175, 85 )
spawner_5_1.setInitialMoveForChildren( "BF1_5", 170, 560 )
spawner_5_1.setHome( "BF1_5", 170, 560 )
spawner_5_1.setSpawnWhenPlayersAreInRoom( true )
spawner_5_1.setWaitTime( 50 , 70 )
spawner_5_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_5_1.childrenWander( true )
spawner_5_1.setMonsterLevelForChildren( 2.6 )

spawner_5_2 = myRooms.BF1_5.spawnSpawner( "bf1_5_spawner2", "garlic", 1)
spawner_5_2.stopSpawning()
spawner_5_2.setPos( 175, 80 )
spawner_5_2.setInitialMoveForChildren( "BF1_5", 870, 420 )
spawner_5_2.setHome( "BF1_5", 870, 420 )
spawner_5_2.setSpawnWhenPlayersAreInRoom( true )
spawner_5_2.setWaitTime( 50 , 70 )
spawner_5_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_5_2.childrenWander( true )
spawner_5_2.setMonsterLevelForChildren( 2.6 )

spawner_5_3 = myRooms.BF1_5.spawnSpawner( "bf1_5_spawner3", "garlic", 1)
spawner_5_3.stopSpawning()
spawner_5_3.setPos( 175, 90 )
spawner_5_3.setInitialMoveForChildren( "BF1_5", 100, 220 )
spawner_5_3.setHome( "BF1_5", 100, 220 )
spawner_5_3.setSpawnWhenPlayersAreInRoom( true )
spawner_5_3.setWaitTime( 50 , 70 )
spawner_5_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_5_3.childrenWander( true )
spawner_5_3.setMonsterLevelForChildren( 2.6 )

spawner_5_4 = myRooms.BF1_5.spawnSpawner( "bf1_5_spawner", "garlic", 1)
spawner_5_4.stopSpawning()
spawner_5_4.setPos( 175, 95 )
spawner_5_4.setInitialMoveForChildren( "BF1_5", 530, 490 )
spawner_5_4.setHome( "BF1_5", 530, 490 )
spawner_5_4.setSpawnWhenPlayersAreInRoom( true )
spawner_5_4.setWaitTime( 50 , 70 )
spawner_5_4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_5_4.childrenWander( true )
spawner_5_4.setMonsterLevelForChildren( 2.6 )

//----------------------------------------------------
//Garlic Spawners for BF1_5                           
//----------------------------------------------------
spawner_6_1 = myRooms.BF1_6.spawnSpawner( "bf1_6_spawner1", "garlic", 1)
spawner_6_1.stopSpawning()
spawner_6_1.setPos( 545, 338 )
spawner_6_1.setInitialMoveForChildren( "BF1_6", 115, 585 )
spawner_6_1.setHome( "BF1_6", 115, 585 )
spawner_6_1.setSpawnWhenPlayersAreInRoom( true )
spawner_6_1.setWaitTime( 50 , 70 )
spawner_6_1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_6_1.childrenWander( true )
spawner_6_1.setMonsterLevelForChildren( 2.4 )

spawner_6_2 = myRooms.BF1_6.spawnSpawner( "bf1_6_spawner2", "garlic", 1)
spawner_6_2.stopSpawning()
spawner_6_2.setPos( 335, 445 )
spawner_6_2.setInitialMoveForChildren( "BF1_6", 150, 390 )
spawner_6_2.setHome( "BF1_6", 150, 390 )
spawner_6_2.setSpawnWhenPlayersAreInRoom( true )
spawner_6_2.setWaitTime( 50 , 70 )
spawner_6_2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_6_2.childrenWander( true )
spawner_6_2.setMonsterLevelForChildren( 2.4 )

spawner_6_3 = myRooms.BF1_6.spawnSpawner( "bf1_6_spawner3", "garlic", 1)
spawner_6_3.stopSpawning()
spawner_6_3.setPos( 795, 436 )
spawner_6_3.setInitialMoveForChildren( "BF1_6", 620, 325 )
spawner_6_3.setHome( "BF1_6", 620, 325 )
spawner_6_3.setSpawnWhenPlayersAreInRoom( true )
spawner_6_3.setWaitTime( 50 , 70 )
spawner_6_3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_6_3.childrenWander( true )
spawner_6_3.setMonsterLevelForChildren( 2.4 )

spawner_6_4 = myRooms.BF1_6.spawnSpawner( "bf1_6_spawner4", "garlic", 1)
spawner_6_4.stopSpawning()
spawner_6_4.setPos( 820, 373 )
spawner_6_4.setInitialMoveForChildren( "BF1_6", 590, 575 )
spawner_6_4.setHome( "BF1_6", 590, 575 )
spawner_6_4.setSpawnWhenPlayersAreInRoom( true )
spawner_6_4.setWaitTime( 50 , 70 )
spawner_6_4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner_6_4.childrenWander( true )
spawner_6_4.setMonsterLevelForChildren( 2.4 )

//----------------------------------------------------
//Spawning and Loot Logic                             
//----------------------------------------------------
def spawn_405_1() {
	garlic_405_1 = spawner_405_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_405_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_405_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_405_1 = event.actor.getHated()
		hateList_405_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.6) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_405_1()

def spawn_405_2() {
	garlic_405_2 = spawner_405_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_405_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_405_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_405_2 = event.actor.getHated()
		hateList_405_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.6) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_405_2()

def spawn_406_1() {
	garlic_406_1 = spawner_406_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_406_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_406_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_406_1 = event.actor.getHated()
		hateList_406_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.9) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_406_1()

def spawn_406_2() {
	garlic_406_2 = spawner_406_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_406_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_406_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_406_2 = event.actor.getHated()
		hateList_406_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.9) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_406_2()

def spawn_505_1() {
	garlic_505_1 = spawner_505_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_505_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_505_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_505_1 = event.actor.getHated()
		hateList_505_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.3) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_505_1()

def spawn_605_1() {
	garlic_605_1 = spawner_605_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_605_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_605_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_605_1 = event.actor.getHated()
		hateList_605_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.4) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_605_1()

def spawn_105_1() {
	garlic_105_1 = spawner_105_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_105_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_105_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_105_1 = event.actor.getHated()
		hateList_105_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 4.0) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_105_1()

def spawn_105_2() {
	garlic_105_2 = spawner_105_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_105_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_105_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_105_2 = event.actor.getHated()
		hateList_105_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 4.0) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_105_2()

def spawn_105_3() {
	garlic_105_3 = spawner_105_3.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_105_3) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_105_3()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_105_3 = event.actor.getHated()
		hateList_105_3.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 4.0) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_105_3()

def spawn_106_1() {
	garlic_106_1 = spawner_106_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_106_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_106_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_106_1 = event.actor.getHated()
		hateList_106_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.8) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_106_1()

def spawn_106_2() {
	garlic_106_2 = spawner_106_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_106_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_106_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_106_2 = event.actor.getHated()
		hateList_106_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.8) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_106_2()

def spawn_204_1() {
	garlic_204_1 = spawner_204_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_204_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_204_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_204_1 = event.actor.getHated()
		hateList_204_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.6) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_204_1()

def spawn_204_2() {
	garlic_204_2 = spawner_204_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_204_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_204_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_204_2 = event.actor.getHated()
		hateList_204_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.6) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_204_2()

def spawn_205_1() {
	garlic_205_1 = spawner_205_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_205_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_205_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_205_1 = event.actor.getHated()
		hateList_205_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_205_1()

def spawn_205_2() {
	garlic_205_2 = spawner_205_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_205_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_205_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_205_2 = event.actor.getHated()
		hateList_205_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_205_2()

def spawn_205_3() {
	garlic_205_3 = spawner_205_3.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_205_3) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_205_3()}
		sproutRoll = random(100) //Just a roll, will be compared against players
	
		hateList_205_3 = event.actor.getHated()
		hateList_205_3.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_205_3()

def spawn_205_4() {
	garlic_205_4 = spawner_205_4.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_205_4) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_205_4()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_205_4 = event.actor.getHated()
		hateList_205_4.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_205_4()

def spawn_206_1() {
	garlic_206_1 = spawner_206_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_206_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_206_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_206_1 = event.actor.getHated()
		hateList_206_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_206_1()

def spawn_206_2() {
	garlic_206_2 = spawner_206_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_206_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_206_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_206_2 = event.actor.getHated()
		hateList_206_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_206_2()

def spawn_206_3() {
	garlic_206_3 = spawner_206_3.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_206_3) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_206_3()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_206_3 = event.actor.getHated()
		hateList_206_3.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_206_3()

def spawn_305_1() {
	garlic_305_1 = spawner_305_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_305_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_305_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_305_1 = event.actor.getHated()
		hateList_305_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_305_1()

def spawn_305_2() {
	garlic_305_2 = spawner_305_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_305_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_305_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_305_2 = event.actor.getHated()
		hateList_305_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_305_2()

def spawn_305_3() {
	garlic_305_3 = spawner_305_3.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_305_3) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_305_3()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_305_3 = event.actor.getHated()
		hateList_305_3.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_305_3()

def spawn_502_1() {
	garlic_502_1 = spawner_502_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_502_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_502_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_502_1 = event.actor.getHated()
		hateList_502_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_502_1()

def spawn_502_2() {
	garlic_502_2 = spawner_502_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_502_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_502_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_502_2 = event.actor.getHated()
		hateList_502_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_502_2()

def spawn_506_1() {
	garlic_506_1 = spawner_506_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_506_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_506_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_506_1 = event.actor.getHated()
		hateList_506_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_506_1()

def spawn_506_2() {
	garlic_506_2 = spawner_506_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_506_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_506_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_506_2 = event.actor.getHated()
		hateList_506_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_506_2()

def spawn_5_1() {
	garlic_5_1 = spawner_5_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_5_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_5_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_5_1 = event.actor.getHated()
		hateList_5_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_5_1()

def spawn_5_2() {
	garlic_5_2 = spawner_5_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_5_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_5_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_5_2 = event.actor.getHated()
		hateList_5_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_5_2()

def spawn_5_3() {
	garlic_5_3 = spawner_5_3.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_5_3) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_5_3()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_5_3 = event.actor.getHated()
		hateList_5_3.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_5_3()

def spawn_5_4() {
	garlic_5_4 = spawner_5_4.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_5_4) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_5_4()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_5_4 = event.actor.getHated()
		hateList_5_4.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_5_4()

def spawn_6_1() {
	garlic_6_1 = spawner_6_1.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_6_1) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_6_1()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_6_1 = event.actor.getHated()
		hateList_6_1.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_6_1()

def spawn_6_2() {
	garlic_6_2 = spawner_6_2.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_6_2) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_6_2()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_6_2 = event.actor.getHated()
		hateList_6_2.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_6_2()

def spawn_6_3() {
	garlic_6_3 = spawner_6_3.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_6_3) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_6_3()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_6_3 = event.actor.getHated()
		hateList_6_3.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_6_3()

def spawn_6_4() {
	garlic_6_4 = spawner_6_4.forceSpawnNow() //Spawn the garlic
	runOnDeath(garlic_6_4) { event -> //When the garlic dies
		myManager.schedule(random(50, 70)) {spawn_6_4()}
		sproutRoll = random(100) //Just a roll, will be compared against players
		
		hateList_6_4 = event.actor.getHated()
		hateList_6_4.each() {
			if(isPlayer(it) && sproutRoll <= SPROUT_PERCENTAGE && it.isOnQuest(64, 2) && it.getConLevel() < 3.7) {
				it.grantQuantityItem(100263, 1)
			}
		}
	}
}

spawn_6_4()

//----------------------------------------------------
//Garlic Spawner Alliances                            
//----------------------------------------------------
spawner_405_1.allyWithSpawner(spawner_405_2)
spawner_405_1.allyWithSpawner(spawner_406_1)
spawner_405_1.allyWithSpawner(spawner_406_2)
spawner_405_1.allyWithSpawner(spawner_505_1)
spawner_405_1.allyWithSpawner(spawner_605_1)
spawner_405_1.allyWithSpawner(spawner_105_1)
spawner_405_1.allyWithSpawner(spawner_105_2)
spawner_405_1.allyWithSpawner(spawner_105_3)
spawner_405_1.allyWithSpawner(spawner_106_1)
spawner_405_1.allyWithSpawner(spawner_106_2)
spawner_405_1.allyWithSpawner(spawner_106_3)
spawner_405_1.allyWithSpawner(spawner_204_1)
spawner_405_1.allyWithSpawner(spawner_204_2)
spawner_405_1.allyWithSpawner(spawner_205_1)
spawner_405_1.allyWithSpawner(spawner_205_2)
spawner_405_1.allyWithSpawner(spawner_205_3)
spawner_405_1.allyWithSpawner(spawner_205_4)
spawner_405_1.allyWithSpawner(spawner_206_1)
spawner_405_1.allyWithSpawner(spawner_206_2)
spawner_405_1.allyWithSpawner(spawner_206_3)
spawner_405_1.allyWithSpawner(spawner_305_1)
spawner_405_1.allyWithSpawner(spawner_305_2)
spawner_405_1.allyWithSpawner(spawner_305_3)
spawner_405_1.allyWithSpawner(spawner_502_1)
spawner_405_1.allyWithSpawner(spawner_502_2)
spawner_405_1.allyWithSpawner(spawner_506_1)
spawner_405_1.allyWithSpawner(spawner_506_2)
spawner_405_1.allyWithSpawner(spawner_5_1)
spawner_405_1.allyWithSpawner(spawner_5_2)
spawner_405_1.allyWithSpawner(spawner_5_3)
spawner_405_1.allyWithSpawner(spawner_5_4)
spawner_405_1.allyWithSpawner(spawner_6_1)
spawner_405_1.allyWithSpawner(spawner_6_2)
spawner_405_1.allyWithSpawner(spawner_6_3)
spawner_405_1.allyWithSpawner(spawner_6_4)

spawner_405_2.allyWithSpawner(spawner_406_1)
spawner_405_2.allyWithSpawner(spawner_406_2)
spawner_405_2.allyWithSpawner(spawner_505_1)
spawner_405_2.allyWithSpawner(spawner_605_1)
spawner_405_2.allyWithSpawner(spawner_105_1)
spawner_405_2.allyWithSpawner(spawner_105_2)
spawner_405_2.allyWithSpawner(spawner_105_3)
spawner_405_2.allyWithSpawner(spawner_106_1)
spawner_405_2.allyWithSpawner(spawner_106_2)
spawner_405_2.allyWithSpawner(spawner_106_3)
spawner_405_2.allyWithSpawner(spawner_204_1)
spawner_405_2.allyWithSpawner(spawner_204_2)
spawner_405_2.allyWithSpawner(spawner_205_1)
spawner_405_2.allyWithSpawner(spawner_205_2)
spawner_405_2.allyWithSpawner(spawner_205_3)
spawner_405_2.allyWithSpawner(spawner_205_4)
spawner_405_2.allyWithSpawner(spawner_206_1)
spawner_405_2.allyWithSpawner(spawner_206_2)
spawner_405_2.allyWithSpawner(spawner_206_3)
spawner_405_2.allyWithSpawner(spawner_305_1)
spawner_405_2.allyWithSpawner(spawner_305_2)
spawner_405_2.allyWithSpawner(spawner_305_3)
spawner_405_2.allyWithSpawner(spawner_502_1)
spawner_405_2.allyWithSpawner(spawner_502_2)
spawner_405_2.allyWithSpawner(spawner_506_1)
spawner_405_2.allyWithSpawner(spawner_506_2)
spawner_405_2.allyWithSpawner(spawner_5_1)
spawner_405_2.allyWithSpawner(spawner_5_2)
spawner_405_2.allyWithSpawner(spawner_5_3)
spawner_405_2.allyWithSpawner(spawner_5_4)
spawner_405_2.allyWithSpawner(spawner_6_1)
spawner_405_2.allyWithSpawner(spawner_6_2)
spawner_405_2.allyWithSpawner(spawner_6_3)
spawner_405_2.allyWithSpawner(spawner_6_4)

spawner_406_1.allyWithSpawner(spawner_406_2)
spawner_406_1.allyWithSpawner(spawner_505_1)
spawner_406_1.allyWithSpawner(spawner_605_1)
spawner_406_1.allyWithSpawner(spawner_105_1)
spawner_406_1.allyWithSpawner(spawner_105_2)
spawner_406_1.allyWithSpawner(spawner_105_3)
spawner_406_1.allyWithSpawner(spawner_106_1)
spawner_406_1.allyWithSpawner(spawner_106_2)
spawner_406_1.allyWithSpawner(spawner_106_3)
spawner_406_1.allyWithSpawner(spawner_204_1)
spawner_406_1.allyWithSpawner(spawner_204_2)
spawner_406_1.allyWithSpawner(spawner_205_1)
spawner_406_1.allyWithSpawner(spawner_205_2)
spawner_406_1.allyWithSpawner(spawner_205_3)
spawner_406_1.allyWithSpawner(spawner_205_4)
spawner_406_1.allyWithSpawner(spawner_206_1)
spawner_406_1.allyWithSpawner(spawner_206_2)
spawner_406_1.allyWithSpawner(spawner_206_3)
spawner_406_1.allyWithSpawner(spawner_305_1)
spawner_406_1.allyWithSpawner(spawner_305_2)
spawner_406_1.allyWithSpawner(spawner_305_3)
spawner_406_1.allyWithSpawner(spawner_502_1)
spawner_406_1.allyWithSpawner(spawner_502_2)
spawner_406_1.allyWithSpawner(spawner_506_1)
spawner_406_1.allyWithSpawner(spawner_506_2)
spawner_406_1.allyWithSpawner(spawner_5_1)
spawner_406_1.allyWithSpawner(spawner_5_2)
spawner_406_1.allyWithSpawner(spawner_5_3)
spawner_406_1.allyWithSpawner(spawner_5_4)
spawner_406_1.allyWithSpawner(spawner_6_1)
spawner_406_1.allyWithSpawner(spawner_6_2)
spawner_406_1.allyWithSpawner(spawner_6_3)
spawner_406_1.allyWithSpawner(spawner_6_4)

spawner_406_2.allyWithSpawner(spawner_505_1)
spawner_406_2.allyWithSpawner(spawner_605_1)
spawner_406_2.allyWithSpawner(spawner_105_1)
spawner_406_2.allyWithSpawner(spawner_105_2)
spawner_406_2.allyWithSpawner(spawner_105_3)
spawner_406_2.allyWithSpawner(spawner_106_1)
spawner_406_2.allyWithSpawner(spawner_106_2)
spawner_406_2.allyWithSpawner(spawner_106_3)
spawner_406_2.allyWithSpawner(spawner_204_1)
spawner_406_2.allyWithSpawner(spawner_204_2)
spawner_406_2.allyWithSpawner(spawner_205_1)
spawner_406_2.allyWithSpawner(spawner_205_2)
spawner_406_2.allyWithSpawner(spawner_205_3)
spawner_406_2.allyWithSpawner(spawner_205_4)
spawner_406_2.allyWithSpawner(spawner_206_1)
spawner_406_2.allyWithSpawner(spawner_206_2)
spawner_406_2.allyWithSpawner(spawner_206_3)
spawner_406_2.allyWithSpawner(spawner_305_1)
spawner_406_2.allyWithSpawner(spawner_305_2)
spawner_406_2.allyWithSpawner(spawner_305_3)
spawner_406_2.allyWithSpawner(spawner_502_1)
spawner_406_2.allyWithSpawner(spawner_502_2)
spawner_406_2.allyWithSpawner(spawner_506_1)
spawner_406_2.allyWithSpawner(spawner_506_2)
spawner_406_2.allyWithSpawner(spawner_5_1)
spawner_406_2.allyWithSpawner(spawner_5_2)
spawner_406_2.allyWithSpawner(spawner_5_3)
spawner_406_2.allyWithSpawner(spawner_5_4)
spawner_406_2.allyWithSpawner(spawner_6_1)
spawner_406_2.allyWithSpawner(spawner_6_2)
spawner_406_2.allyWithSpawner(spawner_6_3)
spawner_406_2.allyWithSpawner(spawner_6_4)

spawner_505_1.allyWithSpawner(spawner_605_1)
spawner_505_1.allyWithSpawner(spawner_105_1)
spawner_505_1.allyWithSpawner(spawner_105_2)
spawner_505_1.allyWithSpawner(spawner_105_3)
spawner_505_1.allyWithSpawner(spawner_106_1)
spawner_505_1.allyWithSpawner(spawner_106_2)
spawner_505_1.allyWithSpawner(spawner_106_3)
spawner_505_1.allyWithSpawner(spawner_204_1)
spawner_505_1.allyWithSpawner(spawner_204_2)
spawner_505_1.allyWithSpawner(spawner_205_1)
spawner_505_1.allyWithSpawner(spawner_205_2)
spawner_505_1.allyWithSpawner(spawner_205_3)
spawner_505_1.allyWithSpawner(spawner_205_4)
spawner_505_1.allyWithSpawner(spawner_206_1)
spawner_505_1.allyWithSpawner(spawner_206_2)
spawner_505_1.allyWithSpawner(spawner_206_3)
spawner_505_1.allyWithSpawner(spawner_305_1)
spawner_505_1.allyWithSpawner(spawner_305_2)
spawner_505_1.allyWithSpawner(spawner_305_3)
spawner_505_1.allyWithSpawner(spawner_502_1)
spawner_505_1.allyWithSpawner(spawner_502_2)
spawner_505_1.allyWithSpawner(spawner_506_1)
spawner_505_1.allyWithSpawner(spawner_506_2)
spawner_505_1.allyWithSpawner(spawner_5_1)
spawner_505_1.allyWithSpawner(spawner_5_2)
spawner_505_1.allyWithSpawner(spawner_5_3)
spawner_505_1.allyWithSpawner(spawner_5_4)
spawner_505_1.allyWithSpawner(spawner_6_1)
spawner_505_1.allyWithSpawner(spawner_6_2)
spawner_505_1.allyWithSpawner(spawner_6_3)
spawner_505_1.allyWithSpawner(spawner_6_4)

spawner_605_1.allyWithSpawner(spawner_105_1)
spawner_605_1.allyWithSpawner(spawner_105_2)
spawner_605_1.allyWithSpawner(spawner_105_3)
spawner_605_1.allyWithSpawner(spawner_106_1)
spawner_605_1.allyWithSpawner(spawner_106_2)
spawner_605_1.allyWithSpawner(spawner_106_3)
spawner_605_1.allyWithSpawner(spawner_204_1)
spawner_605_1.allyWithSpawner(spawner_204_2)
spawner_605_1.allyWithSpawner(spawner_205_1)
spawner_605_1.allyWithSpawner(spawner_205_2)
spawner_605_1.allyWithSpawner(spawner_205_3)
spawner_605_1.allyWithSpawner(spawner_205_4)
spawner_605_1.allyWithSpawner(spawner_206_1)
spawner_605_1.allyWithSpawner(spawner_206_2)
spawner_605_1.allyWithSpawner(spawner_206_3)
spawner_605_1.allyWithSpawner(spawner_305_1)
spawner_605_1.allyWithSpawner(spawner_305_2)
spawner_605_1.allyWithSpawner(spawner_305_3)
spawner_605_1.allyWithSpawner(spawner_502_1)
spawner_605_1.allyWithSpawner(spawner_502_2)
spawner_605_1.allyWithSpawner(spawner_506_1)
spawner_605_1.allyWithSpawner(spawner_506_2)
spawner_605_1.allyWithSpawner(spawner_5_1)
spawner_605_1.allyWithSpawner(spawner_5_2)
spawner_605_1.allyWithSpawner(spawner_5_3)
spawner_605_1.allyWithSpawner(spawner_5_4)
spawner_605_1.allyWithSpawner(spawner_6_1)
spawner_605_1.allyWithSpawner(spawner_6_2)
spawner_605_1.allyWithSpawner(spawner_6_3)
spawner_605_1.allyWithSpawner(spawner_6_4)

spawner_105_1.allyWithSpawner(spawner_105_2)
spawner_105_1.allyWithSpawner(spawner_105_3)
spawner_105_1.allyWithSpawner(spawner_106_1)
spawner_105_1.allyWithSpawner(spawner_106_2)
spawner_105_1.allyWithSpawner(spawner_106_3)
spawner_105_1.allyWithSpawner(spawner_204_1)
spawner_105_1.allyWithSpawner(spawner_204_2)
spawner_105_1.allyWithSpawner(spawner_205_1)
spawner_105_1.allyWithSpawner(spawner_205_2)
spawner_105_1.allyWithSpawner(spawner_205_3)
spawner_105_1.allyWithSpawner(spawner_205_4)
spawner_105_1.allyWithSpawner(spawner_206_1)
spawner_105_1.allyWithSpawner(spawner_206_2)
spawner_105_1.allyWithSpawner(spawner_206_3)
spawner_105_1.allyWithSpawner(spawner_305_1)
spawner_105_1.allyWithSpawner(spawner_305_2)
spawner_105_1.allyWithSpawner(spawner_305_3)
spawner_105_1.allyWithSpawner(spawner_502_1)
spawner_105_1.allyWithSpawner(spawner_502_2)
spawner_105_1.allyWithSpawner(spawner_506_1)
spawner_105_1.allyWithSpawner(spawner_506_2)
spawner_105_1.allyWithSpawner(spawner_5_1)
spawner_105_1.allyWithSpawner(spawner_5_2)
spawner_105_1.allyWithSpawner(spawner_5_3)
spawner_105_1.allyWithSpawner(spawner_5_4)
spawner_105_1.allyWithSpawner(spawner_6_1)
spawner_105_1.allyWithSpawner(spawner_6_2)
spawner_105_1.allyWithSpawner(spawner_6_3)
spawner_105_1.allyWithSpawner(spawner_6_4)

spawner_105_2.allyWithSpawner(spawner_105_3)
spawner_105_2.allyWithSpawner(spawner_106_1)
spawner_105_2.allyWithSpawner(spawner_106_2)
spawner_105_2.allyWithSpawner(spawner_106_3)
spawner_105_2.allyWithSpawner(spawner_204_1)
spawner_105_2.allyWithSpawner(spawner_204_2)
spawner_105_2.allyWithSpawner(spawner_205_1)
spawner_105_2.allyWithSpawner(spawner_205_2)
spawner_105_2.allyWithSpawner(spawner_205_3)
spawner_105_2.allyWithSpawner(spawner_205_4)
spawner_105_2.allyWithSpawner(spawner_206_1)
spawner_105_2.allyWithSpawner(spawner_206_2)
spawner_105_2.allyWithSpawner(spawner_206_3)
spawner_105_2.allyWithSpawner(spawner_305_1)
spawner_105_2.allyWithSpawner(spawner_305_2)
spawner_105_2.allyWithSpawner(spawner_305_3)
spawner_105_2.allyWithSpawner(spawner_502_1)
spawner_105_2.allyWithSpawner(spawner_502_2)
spawner_105_2.allyWithSpawner(spawner_506_1)
spawner_105_2.allyWithSpawner(spawner_506_2)
spawner_105_2.allyWithSpawner(spawner_5_1)
spawner_105_2.allyWithSpawner(spawner_5_2)
spawner_105_2.allyWithSpawner(spawner_5_3)
spawner_105_2.allyWithSpawner(spawner_5_4)
spawner_105_2.allyWithSpawner(spawner_6_1)
spawner_105_2.allyWithSpawner(spawner_6_2)
spawner_105_2.allyWithSpawner(spawner_6_3)
spawner_105_2.allyWithSpawner(spawner_6_4)

spawner_105_3.allyWithSpawner(spawner_106_1)
spawner_105_3.allyWithSpawner(spawner_106_2)
spawner_105_3.allyWithSpawner(spawner_106_3)
spawner_105_3.allyWithSpawner(spawner_204_1)
spawner_105_3.allyWithSpawner(spawner_204_2)
spawner_105_3.allyWithSpawner(spawner_205_1)
spawner_105_3.allyWithSpawner(spawner_205_2)
spawner_105_3.allyWithSpawner(spawner_205_3)
spawner_105_3.allyWithSpawner(spawner_205_4)
spawner_105_3.allyWithSpawner(spawner_206_1)
spawner_105_3.allyWithSpawner(spawner_206_2)
spawner_105_3.allyWithSpawner(spawner_206_3)
spawner_105_3.allyWithSpawner(spawner_305_1)
spawner_105_3.allyWithSpawner(spawner_305_2)
spawner_105_3.allyWithSpawner(spawner_305_3)
spawner_105_3.allyWithSpawner(spawner_502_1)
spawner_105_3.allyWithSpawner(spawner_502_2)
spawner_105_3.allyWithSpawner(spawner_506_1)
spawner_105_3.allyWithSpawner(spawner_506_2)
spawner_105_3.allyWithSpawner(spawner_5_1)
spawner_105_3.allyWithSpawner(spawner_5_2)
spawner_105_3.allyWithSpawner(spawner_5_3)
spawner_105_3.allyWithSpawner(spawner_5_4)
spawner_105_3.allyWithSpawner(spawner_6_1)
spawner_105_3.allyWithSpawner(spawner_6_2)
spawner_105_3.allyWithSpawner(spawner_6_3)
spawner_105_3.allyWithSpawner(spawner_6_4)

spawner_106_1.allyWithSpawner(spawner_106_2)
spawner_106_1.allyWithSpawner(spawner_106_3)
spawner_106_1.allyWithSpawner(spawner_204_1)
spawner_106_1.allyWithSpawner(spawner_204_2)
spawner_106_1.allyWithSpawner(spawner_205_1)
spawner_106_1.allyWithSpawner(spawner_205_2)
spawner_106_1.allyWithSpawner(spawner_205_3)
spawner_106_1.allyWithSpawner(spawner_205_4)
spawner_106_1.allyWithSpawner(spawner_206_1)
spawner_106_1.allyWithSpawner(spawner_206_2)
spawner_106_1.allyWithSpawner(spawner_206_3)
spawner_106_1.allyWithSpawner(spawner_305_1)
spawner_106_1.allyWithSpawner(spawner_305_2)
spawner_106_1.allyWithSpawner(spawner_305_3)
spawner_106_1.allyWithSpawner(spawner_502_1)
spawner_106_1.allyWithSpawner(spawner_502_2)
spawner_106_1.allyWithSpawner(spawner_506_1)
spawner_106_1.allyWithSpawner(spawner_506_2)
spawner_106_1.allyWithSpawner(spawner_5_1)
spawner_106_1.allyWithSpawner(spawner_5_2)
spawner_106_1.allyWithSpawner(spawner_5_3)
spawner_106_1.allyWithSpawner(spawner_5_4)
spawner_106_1.allyWithSpawner(spawner_6_1)
spawner_106_1.allyWithSpawner(spawner_6_2)
spawner_106_1.allyWithSpawner(spawner_6_3)
spawner_106_1.allyWithSpawner(spawner_6_4)

spawner_106_2.allyWithSpawner(spawner_106_3)
spawner_106_2.allyWithSpawner(spawner_204_1)
spawner_106_2.allyWithSpawner(spawner_204_2)
spawner_106_2.allyWithSpawner(spawner_205_1)
spawner_106_2.allyWithSpawner(spawner_205_2)
spawner_106_2.allyWithSpawner(spawner_205_3)
spawner_106_2.allyWithSpawner(spawner_205_4)
spawner_106_2.allyWithSpawner(spawner_206_1)
spawner_106_2.allyWithSpawner(spawner_206_2)
spawner_106_2.allyWithSpawner(spawner_206_3)
spawner_106_2.allyWithSpawner(spawner_305_1)
spawner_106_2.allyWithSpawner(spawner_305_2)
spawner_106_2.allyWithSpawner(spawner_305_3)
spawner_106_2.allyWithSpawner(spawner_502_1)
spawner_106_2.allyWithSpawner(spawner_502_2)
spawner_106_2.allyWithSpawner(spawner_506_1)
spawner_106_2.allyWithSpawner(spawner_506_2)
spawner_106_2.allyWithSpawner(spawner_5_1)
spawner_106_2.allyWithSpawner(spawner_5_2)
spawner_106_2.allyWithSpawner(spawner_5_3)
spawner_106_2.allyWithSpawner(spawner_5_4)
spawner_106_2.allyWithSpawner(spawner_6_1)
spawner_106_2.allyWithSpawner(spawner_6_2)
spawner_106_2.allyWithSpawner(spawner_6_3)
spawner_106_2.allyWithSpawner(spawner_6_4)

spawner_204_1.allyWithSpawner(spawner_204_2)
spawner_204_1.allyWithSpawner(spawner_205_1)
spawner_204_1.allyWithSpawner(spawner_205_2)
spawner_204_1.allyWithSpawner(spawner_205_3)
spawner_204_1.allyWithSpawner(spawner_205_4)
spawner_204_1.allyWithSpawner(spawner_206_1)
spawner_204_1.allyWithSpawner(spawner_206_2)
spawner_204_1.allyWithSpawner(spawner_206_3)
spawner_204_1.allyWithSpawner(spawner_305_1)
spawner_204_1.allyWithSpawner(spawner_305_2)
spawner_204_1.allyWithSpawner(spawner_305_3)
spawner_204_1.allyWithSpawner(spawner_502_1)
spawner_204_1.allyWithSpawner(spawner_502_2)
spawner_204_1.allyWithSpawner(spawner_506_1)
spawner_204_1.allyWithSpawner(spawner_506_2)
spawner_204_1.allyWithSpawner(spawner_5_1)
spawner_204_1.allyWithSpawner(spawner_5_2)
spawner_204_1.allyWithSpawner(spawner_5_3)
spawner_204_1.allyWithSpawner(spawner_5_4)
spawner_204_1.allyWithSpawner(spawner_6_1)
spawner_204_1.allyWithSpawner(spawner_6_2)
spawner_204_1.allyWithSpawner(spawner_6_3)
spawner_204_1.allyWithSpawner(spawner_6_4)

spawner_204_2.allyWithSpawner(spawner_205_1)
spawner_204_2.allyWithSpawner(spawner_205_2)
spawner_204_2.allyWithSpawner(spawner_205_3)
spawner_204_2.allyWithSpawner(spawner_205_4)
spawner_204_2.allyWithSpawner(spawner_206_1)
spawner_204_2.allyWithSpawner(spawner_206_2)
spawner_204_2.allyWithSpawner(spawner_206_3)
spawner_204_2.allyWithSpawner(spawner_305_1)
spawner_204_2.allyWithSpawner(spawner_305_2)
spawner_204_2.allyWithSpawner(spawner_305_3)
spawner_204_2.allyWithSpawner(spawner_502_1)
spawner_204_2.allyWithSpawner(spawner_502_2)
spawner_204_2.allyWithSpawner(spawner_506_1)
spawner_204_2.allyWithSpawner(spawner_506_2)
spawner_204_2.allyWithSpawner(spawner_5_1)
spawner_204_2.allyWithSpawner(spawner_5_2)
spawner_204_2.allyWithSpawner(spawner_5_3)
spawner_204_2.allyWithSpawner(spawner_5_4)
spawner_204_2.allyWithSpawner(spawner_6_1)
spawner_204_2.allyWithSpawner(spawner_6_2)
spawner_204_2.allyWithSpawner(spawner_6_3)
spawner_204_2.allyWithSpawner(spawner_6_4)

spawner_205_1.allyWithSpawner(spawner_205_2)
spawner_205_1.allyWithSpawner(spawner_205_3)
spawner_205_1.allyWithSpawner(spawner_205_4)
spawner_205_1.allyWithSpawner(spawner_206_1)
spawner_205_1.allyWithSpawner(spawner_206_2)
spawner_205_1.allyWithSpawner(spawner_206_3)
spawner_205_1.allyWithSpawner(spawner_305_1)
spawner_205_1.allyWithSpawner(spawner_305_2)
spawner_205_1.allyWithSpawner(spawner_305_3)
spawner_205_1.allyWithSpawner(spawner_502_1)
spawner_205_1.allyWithSpawner(spawner_502_2)
spawner_205_1.allyWithSpawner(spawner_506_1)
spawner_205_1.allyWithSpawner(spawner_506_2)
spawner_205_1.allyWithSpawner(spawner_5_1)
spawner_205_1.allyWithSpawner(spawner_5_2)
spawner_205_1.allyWithSpawner(spawner_5_3)
spawner_205_1.allyWithSpawner(spawner_5_4)
spawner_205_1.allyWithSpawner(spawner_6_1)
spawner_205_1.allyWithSpawner(spawner_6_2)
spawner_205_1.allyWithSpawner(spawner_6_3)
spawner_205_1.allyWithSpawner(spawner_6_4)

spawner_205_2.allyWithSpawner(spawner_205_3)
spawner_205_2.allyWithSpawner(spawner_205_4)
spawner_205_2.allyWithSpawner(spawner_206_1)
spawner_205_2.allyWithSpawner(spawner_206_2)
spawner_205_2.allyWithSpawner(spawner_206_3)
spawner_205_2.allyWithSpawner(spawner_305_1)
spawner_205_2.allyWithSpawner(spawner_305_2)
spawner_205_2.allyWithSpawner(spawner_305_3)
spawner_205_2.allyWithSpawner(spawner_502_1)
spawner_205_2.allyWithSpawner(spawner_502_2)
spawner_205_2.allyWithSpawner(spawner_506_1)
spawner_205_2.allyWithSpawner(spawner_506_2)
spawner_205_2.allyWithSpawner(spawner_5_1)
spawner_205_2.allyWithSpawner(spawner_5_2)
spawner_205_2.allyWithSpawner(spawner_5_3)
spawner_205_2.allyWithSpawner(spawner_5_4)
spawner_205_2.allyWithSpawner(spawner_6_1)
spawner_205_2.allyWithSpawner(spawner_6_2)
spawner_205_2.allyWithSpawner(spawner_6_3)
spawner_205_2.allyWithSpawner(spawner_6_4)

spawner_205_3.allyWithSpawner(spawner_205_4)
spawner_205_3.allyWithSpawner(spawner_206_1)
spawner_205_3.allyWithSpawner(spawner_206_2)
spawner_205_3.allyWithSpawner(spawner_206_3)
spawner_205_3.allyWithSpawner(spawner_305_1)
spawner_205_3.allyWithSpawner(spawner_305_2)
spawner_205_3.allyWithSpawner(spawner_305_3)
spawner_205_3.allyWithSpawner(spawner_502_1)
spawner_205_3.allyWithSpawner(spawner_502_2)
spawner_205_3.allyWithSpawner(spawner_506_1)
spawner_205_3.allyWithSpawner(spawner_506_2)
spawner_205_3.allyWithSpawner(spawner_5_1)
spawner_205_3.allyWithSpawner(spawner_5_2)
spawner_205_3.allyWithSpawner(spawner_5_3)
spawner_205_3.allyWithSpawner(spawner_5_4)
spawner_205_3.allyWithSpawner(spawner_6_1)
spawner_205_3.allyWithSpawner(spawner_6_2)
spawner_205_3.allyWithSpawner(spawner_6_3)
spawner_205_3.allyWithSpawner(spawner_6_4)

spawner_205_4.allyWithSpawner(spawner_206_1)
spawner_205_4.allyWithSpawner(spawner_206_2)
spawner_205_4.allyWithSpawner(spawner_206_3)
spawner_205_4.allyWithSpawner(spawner_305_1)
spawner_205_4.allyWithSpawner(spawner_305_2)
spawner_205_4.allyWithSpawner(spawner_305_3)
spawner_205_4.allyWithSpawner(spawner_502_1)
spawner_205_4.allyWithSpawner(spawner_502_2)
spawner_205_4.allyWithSpawner(spawner_506_1)
spawner_205_4.allyWithSpawner(spawner_506_2)
spawner_205_4.allyWithSpawner(spawner_5_1)
spawner_205_4.allyWithSpawner(spawner_5_2)
spawner_205_4.allyWithSpawner(spawner_5_3)
spawner_205_4.allyWithSpawner(spawner_5_4)
spawner_205_4.allyWithSpawner(spawner_6_1)
spawner_205_4.allyWithSpawner(spawner_6_2)
spawner_205_4.allyWithSpawner(spawner_6_3)
spawner_205_4.allyWithSpawner(spawner_6_4)

spawner_206_1.allyWithSpawner(spawner_206_2)
spawner_206_1.allyWithSpawner(spawner_206_3)
spawner_206_1.allyWithSpawner(spawner_305_1)
spawner_206_1.allyWithSpawner(spawner_305_2)
spawner_206_1.allyWithSpawner(spawner_305_3)
spawner_206_1.allyWithSpawner(spawner_502_1)
spawner_206_1.allyWithSpawner(spawner_502_2)
spawner_206_1.allyWithSpawner(spawner_506_1)
spawner_206_1.allyWithSpawner(spawner_506_2)
spawner_206_1.allyWithSpawner(spawner_5_1)
spawner_206_1.allyWithSpawner(spawner_5_2)
spawner_206_1.allyWithSpawner(spawner_5_3)
spawner_206_1.allyWithSpawner(spawner_5_4)
spawner_206_1.allyWithSpawner(spawner_6_1)
spawner_206_1.allyWithSpawner(spawner_6_2)
spawner_206_1.allyWithSpawner(spawner_6_3)
spawner_206_1.allyWithSpawner(spawner_6_4)

spawner_206_2.allyWithSpawner(spawner_206_3)
spawner_206_2.allyWithSpawner(spawner_305_1)
spawner_206_2.allyWithSpawner(spawner_305_2)
spawner_206_2.allyWithSpawner(spawner_305_3)
spawner_206_2.allyWithSpawner(spawner_502_1)
spawner_206_2.allyWithSpawner(spawner_502_2)
spawner_206_2.allyWithSpawner(spawner_506_1)
spawner_206_2.allyWithSpawner(spawner_506_2)
spawner_206_2.allyWithSpawner(spawner_5_1)
spawner_206_2.allyWithSpawner(spawner_5_2)
spawner_206_2.allyWithSpawner(spawner_5_3)
spawner_206_2.allyWithSpawner(spawner_5_4)
spawner_206_2.allyWithSpawner(spawner_6_1)
spawner_206_2.allyWithSpawner(spawner_6_2)
spawner_206_2.allyWithSpawner(spawner_6_3)
spawner_206_2.allyWithSpawner(spawner_6_4)

spawner_206_3.allyWithSpawner(spawner_305_1)
spawner_206_3.allyWithSpawner(spawner_305_2)
spawner_206_3.allyWithSpawner(spawner_305_3)
spawner_206_3.allyWithSpawner(spawner_502_1)
spawner_206_3.allyWithSpawner(spawner_502_2)
spawner_206_3.allyWithSpawner(spawner_506_1)
spawner_206_3.allyWithSpawner(spawner_506_2)
spawner_206_3.allyWithSpawner(spawner_5_1)
spawner_206_3.allyWithSpawner(spawner_5_2)
spawner_206_3.allyWithSpawner(spawner_5_3)
spawner_206_3.allyWithSpawner(spawner_5_4)
spawner_206_3.allyWithSpawner(spawner_6_1)
spawner_206_3.allyWithSpawner(spawner_6_2)
spawner_206_3.allyWithSpawner(spawner_6_3)
spawner_206_3.allyWithSpawner(spawner_6_4)

spawner_305_1.allyWithSpawner(spawner_305_2)
spawner_305_1.allyWithSpawner(spawner_305_3)
spawner_305_1.allyWithSpawner(spawner_502_1)
spawner_305_1.allyWithSpawner(spawner_502_2)
spawner_305_1.allyWithSpawner(spawner_506_1)
spawner_305_1.allyWithSpawner(spawner_506_2)
spawner_305_1.allyWithSpawner(spawner_5_1)
spawner_305_1.allyWithSpawner(spawner_5_3)
spawner_305_1.allyWithSpawner(spawner_5_3)
spawner_305_1.allyWithSpawner(spawner_5_4)
spawner_305_1.allyWithSpawner(spawner_6_1)
spawner_305_1.allyWithSpawner(spawner_6_3)
spawner_305_1.allyWithSpawner(spawner_6_3)
spawner_305_1.allyWithSpawner(spawner_6_4)

spawner_305_2.allyWithSpawner(spawner_305_3)
spawner_305_2.allyWithSpawner(spawner_502_1)
spawner_305_2.allyWithSpawner(spawner_502_2)
spawner_305_2.allyWithSpawner(spawner_506_1)
spawner_305_2.allyWithSpawner(spawner_506_2)
spawner_305_2.allyWithSpawner(spawner_5_1)
spawner_305_2.allyWithSpawner(spawner_5_2)
spawner_305_2.allyWithSpawner(spawner_5_3)
spawner_305_2.allyWithSpawner(spawner_5_4)
spawner_305_2.allyWithSpawner(spawner_6_1)
spawner_305_2.allyWithSpawner(spawner_6_2)
spawner_305_2.allyWithSpawner(spawner_6_3)
spawner_305_2.allyWithSpawner(spawner_6_4)

spawner_305_3.allyWithSpawner(spawner_502_1)
spawner_305_3.allyWithSpawner(spawner_502_2)
spawner_305_3.allyWithSpawner(spawner_506_1)
spawner_305_3.allyWithSpawner(spawner_506_2)
spawner_305_3.allyWithSpawner(spawner_5_1)
spawner_305_3.allyWithSpawner(spawner_5_2)
spawner_305_3.allyWithSpawner(spawner_5_3)
spawner_305_3.allyWithSpawner(spawner_5_4)
spawner_305_3.allyWithSpawner(spawner_6_1)
spawner_305_3.allyWithSpawner(spawner_6_2)
spawner_305_3.allyWithSpawner(spawner_6_3)
spawner_305_3.allyWithSpawner(spawner_6_4)

spawner_502_1.allyWithSpawner(spawner_502_2)
spawner_502_1.allyWithSpawner(spawner_506_1)
spawner_502_1.allyWithSpawner(spawner_506_2)
spawner_502_1.allyWithSpawner(spawner_5_1)
spawner_502_1.allyWithSpawner(spawner_5_2)
spawner_502_1.allyWithSpawner(spawner_5_3)
spawner_502_1.allyWithSpawner(spawner_5_4)
spawner_502_1.allyWithSpawner(spawner_6_1)
spawner_502_1.allyWithSpawner(spawner_6_2)
spawner_502_1.allyWithSpawner(spawner_6_3)
spawner_502_1.allyWithSpawner(spawner_6_4)

spawner_502_2.allyWithSpawner(spawner_506_1)
spawner_502_2.allyWithSpawner(spawner_506_2)
spawner_502_2.allyWithSpawner(spawner_5_1)
spawner_502_2.allyWithSpawner(spawner_5_2)
spawner_502_2.allyWithSpawner(spawner_5_3)
spawner_502_2.allyWithSpawner(spawner_5_4)
spawner_502_2.allyWithSpawner(spawner_6_1)
spawner_502_2.allyWithSpawner(spawner_6_2)
spawner_502_2.allyWithSpawner(spawner_6_3)
spawner_502_2.allyWithSpawner(spawner_6_4)

spawner_506_1.allyWithSpawner(spawner_506_2)
spawner_506_1.allyWithSpawner(spawner_5_1)
spawner_506_1.allyWithSpawner(spawner_5_2)
spawner_506_1.allyWithSpawner(spawner_5_3)
spawner_506_1.allyWithSpawner(spawner_5_4)
spawner_506_1.allyWithSpawner(spawner_6_1)
spawner_506_1.allyWithSpawner(spawner_6_2)
spawner_506_1.allyWithSpawner(spawner_6_3)
spawner_506_1.allyWithSpawner(spawner_6_4)

spawner_506_2.allyWithSpawner(spawner_5_1)
spawner_506_2.allyWithSpawner(spawner_5_2)
spawner_506_2.allyWithSpawner(spawner_5_3)
spawner_506_2.allyWithSpawner(spawner_5_4)
spawner_506_2.allyWithSpawner(spawner_6_1)
spawner_506_2.allyWithSpawner(spawner_6_2)
spawner_506_2.allyWithSpawner(spawner_6_3)
spawner_506_2.allyWithSpawner(spawner_6_4)

spawner_5_1.allyWithSpawner(spawner_5_2)
spawner_5_1.allyWithSpawner(spawner_5_3)
spawner_5_1.allyWithSpawner(spawner_5_4)
spawner_5_1.allyWithSpawner(spawner_6_1)
spawner_5_1.allyWithSpawner(spawner_6_2)
spawner_5_1.allyWithSpawner(spawner_6_3)
spawner_5_1.allyWithSpawner(spawner_6_4)

spawner_5_2.allyWithSpawner(spawner_5_3)
spawner_5_2.allyWithSpawner(spawner_5_4)
spawner_5_2.allyWithSpawner(spawner_6_1)
spawner_5_2.allyWithSpawner(spawner_6_2)
spawner_5_2.allyWithSpawner(spawner_6_3)
spawner_5_2.allyWithSpawner(spawner_6_4)

spawner_5_3.allyWithSpawner(spawner_5_4)
spawner_5_3.allyWithSpawner(spawner_6_1)
spawner_5_3.allyWithSpawner(spawner_6_2)
spawner_5_3.allyWithSpawner(spawner_6_3)
spawner_5_3.allyWithSpawner(spawner_6_4)

spawner_5_4.allyWithSpawner(spawner_6_1)
spawner_5_4.allyWithSpawner(spawner_6_2)
spawner_5_4.allyWithSpawner(spawner_6_3)
spawner_5_4.allyWithSpawner(spawner_6_4)

spawner_6_1.allyWithSpawner(spawner_6_2)
spawner_6_1.allyWithSpawner(spawner_6_3)
spawner_6_1.allyWithSpawner(spawner_6_4)

spawner_6_2.allyWithSpawner(spawner_6_3)
spawner_6_2.allyWithSpawner(spawner_6_4)

spawner_6_3.allyWithSpawner(spawner_6_4)