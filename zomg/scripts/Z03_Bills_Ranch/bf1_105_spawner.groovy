//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner One - Cleared for take-off
/*def spawner1 = myRooms.BF1_105.spawnSpawner( "bf1_5_spawner1", "garlic", 1)
spawner1.setPos( 565, 240 )
spawner1.setInitialMoveForChildren( "BF1_105", 110, 560 )
spawner1.setHome( "BF1_105", 110, 560 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.4 )

// Spawner One - Cleared for take-off
def spawner2 = myRooms.BF1_105.spawnSpawner( "bf1_5_spawner2", "garlic", 1)
spawner2.setPos( 575, 240 )
spawner2.setInitialMoveForChildren( "BF1_105", 310, 350 )
spawner2.setHome( "BF1_105", 310, 350 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.4 )*/

// Spawner One - Cleared for take-off
def spawner3 = myRooms.BF1_105.spawnSpawner( "bf1_5_spawner3", "fluff", 1)
spawner3.setPos( 585, 240 )
spawner3.setInitialMoveForChildren( "BF1_105", 200, 100 )
spawner3.setHome( "BF1_105", 200, 100 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.4 )

// Spawner One - Cleared for take-off
/*def spawner4 = myRooms.BF1_105.spawnSpawner( "bf1_5_spawner4", "garlic", 1)
spawner4.setPos( 595, 240 )
spawner4.setInitialMoveForChildren( "BF1_105", 925, 100 )
spawner4.setHome( "BF1_105", 925, 100 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 2.4 )*/

//Spawning
spawner3.forceSpawnNow()