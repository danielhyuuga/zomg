//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

// Spawner example
mosquitoLTSpawner = myRooms.BF1_804.spawnSpawner( "mosquitoLTSpawner", "mosquito_lt", 1)
mosquitoLTSpawner.stopSpawning()
mosquitoLTSpawner.setPos( 255, 300 )
mosquitoLTSpawner.setHome( "BF1_804", 330, 185 )
mosquitoLTSpawner.setGuardPostForChildren("BF1_804", 330, 185, 120)
mosquitoLTSpawner.setHomeTetherForChildren(5000)
mosquitoLTSpawner.setSpawnWhenPlayersAreInRoom( true )
mosquitoLTSpawner.setWaitTime( 50 , 70 )
mosquitoLTSpawner.childrenWander( false )
mosquitoLTSpawner.setBaseSpeed(150)
mosquitoLTSpawner.setCFH(500)
mosquitoLTSpawner.setMonsterLevelForChildren( 2.5 )

def spawnCuckoo() {
	myManager.schedule(random(1800, 3600)) { 
		cuckoo = mosquitoLTSpawner.forceSpawnNow()
		cuckoo.setDisplayName("Cuckoo")

		runOnDeath(cuckoo) {
			spawnCuckoo()
		}
	}
}


spawnCuckoo()