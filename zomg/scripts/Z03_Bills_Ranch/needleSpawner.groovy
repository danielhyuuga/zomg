//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

needleLTSpawned = false

def needleDispose() {
	if(needleLTSpawned == true && needleLT.getHated().size() == 0) {
		needleLT.dispose()
		needleLTSpawned = false
	}
}

//Defines spawner for Needle Nose LT

needleSpawner1 = myRooms.BF1_804.spawnSpawner("BF1_804_needleSpawner1", "mosquito", 1)
needleSpawner1.setPos( 665, 485 )
needleSpawner1.setMonsterLevelForChildren( 4.0 )

needleSpawner1.stopSpawning()

//Spawns LT, listens for death and gives update, and schedules disposal

myManager.onEnter( myRooms.BF1_804 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(72, 2) && needleLTSpawned == false) {
		needleLT = needleSpawner1.forceSpawnNow()
		needleLT.setDisplayName("Aggressive Skeeter")
		needleLTSpawned = true
		needleLT.addHate( event.actor, 1 )
		event.actor.centerPrint("That's the skeeter! That nose is huge!")
	
		runOnDeath(needleLT, { 
			event.actor.centerPrint("You have recovered a giant skeeter nose!")
			event.actor.updateQuest( 72, "[NPC] Old Man Logan" ) 
			needleLTSpawned = false
		} )
		
		myManager.schedule(60) { needleDispose() }
	}
}