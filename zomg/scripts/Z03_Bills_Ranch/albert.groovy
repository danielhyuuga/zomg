//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Albert = spawnNPC("BFG-Albert", myRooms.BF1_705, 380, 230)
Albert.setDisplayName( "Albert" )
Albert.setRotation( 45 )

onQuestStep(264, 2) { event -> event.player.addMiniMapQuestActorName("BFG-Albert") }

onQuestStep(266, 2) { event -> event.player.addMiniMapQuestActorName("BFG-Albert") }

onQuestStep(268, 2) { event -> event.player.addMiniMapQuestActorName("BFG-Albert") }

//-----------------------------------------------------------------------------------------------------------
//Make sure flag is set                                                                                      
//-----------------------------------------------------------------------------------------------------------
myManager.onEnter(myRooms.BF1_705) { event ->
	if(isPlayer(event.actor) && (event.actor.isOnQuest(264) || event.actor.isOnQuest(266) || event.actor.isOnQuest(268))) {
		if(!event.actor.hasQuestFlag(GLOBAL, "Z3_Albert_Active")) {
			event.actor.setQuestFlag(GLOBAL, "Z3_Albert_Active")
		}
		if(event.actor.isOnQuest(264,3) || event.actor.isOnQuest(266, 3) || event.actor.isOnQuest(268, 3)) {
			event.actor.setQuestFlag(GLOBAL, "Z3_Albert_QuestDoneConvo")
			if(event.actor.hasQuestFlag(GLOBAL, "Z3_Albert_NoQuestDoneConvo")) {
				event.actor.unsetQuestFlag(GLOBAL, "Z3_Albert_NoQuestDoneConvo")
			}
		} else {
			event.actor.setQuestFlag(GLOBAL, "Z3_Albert_NoQuestDoneConvo")
			if(event.actor.hasQuestFlag(GLOBAL, "Z3_Albert_QuestDoneConvo")) {
				event.actor.unsetQuestFlag(GLOBAL, "Z3_Albert_QuestDoneConvo")
			}
		}
	}
	if(isPlayer(event.actor) && event.actor.getPlayerVar("Z04DeadmansDifficulty") > 0) {
		event.actor.setPlayerVar("Z04DeadmansDifficulty", 0)
	}
}

//-----------------------------------------------------------------------------------------------------------
//Conversation when player is coming from Mark                                                               
//-----------------------------------------------------------------------------------------------------------
def albertGrant = Albert.createConversation("albertGrant", true, "!QuestStarted_264", "!QuestCompleted_264", "!QuestStarted_266", "!QuestCompleted_266", "!QuestStarted_268", "!QuestCompleted_268", "!Z3_Albert_Break")
albertGrant.setUrgent(true)

def albertGrant1 = [id:1]
albertGrant1.npctext = "Hey, hey, hey! I'm guard Albert."
albertGrant1.playertext = "Uhm... hey... Albert. Need any help?"
albertGrant1.result = 2
albertGrant.addDialog(albertGrant1, Albert)

def albertGrant2 = [id:2]
albertGrant2.npctext = "Sure do, %p. I thought nobody would ever ask."
albertGrant2.options = []
albertGrant2.options << [text:"Some people are pretty selfish.", result: 3]
albertGrant2.options << [text:"Yeesh, I'm sure they would have if they'd known you needed help.", result: 4]
albertGrant2.options << [text:"You act like people are here to serve your whims.", result: 4]
albertGrant.addDialog(albertGrant2, Albert)

def albertGrant3 = [id:3]
albertGrant3.npctext = "It's alright. What matters is that you're here now and ready to help, right?"
albertGrant3.options = []
albertGrant3.options << [text:"You betcha.", result: 5]
albertGrant3.options << [text:"Actually, I'm not ready just yet.", result: 6]
albertGrant.addDialog(albertGrant3, Albert)

def albertGrant4 = [id:4]
albertGrant4.npctext = "I didn't mean to sound ungrateful. What matters is that you're here now and ready to help, right?"
albertGrant4.options = []
albertGrant4.options << [text:"I suppose so.", result: 5]
albertGrant4.options << [text:"Not after that comment.", result: 6]
albertGrant.addDialog(albertGrant4, Albert)

def albertGrant5 = [id:5]
albertGrant5.npctext = "Right, I have some fairly simple tasks. Some of the bigger baddies in Deadman's Pass, the Laceback Galoshes, the Purse, and the OMFG, have been making trouble. I need them taken out."
albertGrant5.options = []
albertGrant5.options << [text:"You can count on me, Albert.", result: 7]
albertGrant5.options << [text:"I can't. Sorry.", result: 6]
albertGrant.addDialog(albertGrant5, Albert)

def albertGrant6 = [id:6]
albertGrant6.npctext = "I see... I'll be here if you change your mind."
albertGrant6.flag = "Z3_Albert_Break"
albertGrant6.exec = { event -> event.player.removeMiniMapQuestActorName( "BFG-Albert" ) }
albertGrant6.result = DONE
albertGrant.addDialog(albertGrant6, Albert)

def albertGrant7 = [id:7]
albertGrant7.npctext = "Appreciate it."
albertGrant7.flag = ["Z3_Albert_Active", "Z3_Albert_NoQuestDoneConvo"]
albertGrant7.exec = { event ->
	event.player.updateQuest(264, "BFG-Albert")
	event.player.updateQuest(266, "BFG-Albert")
	event.player.updateQuest(268, "BFG-Albert")
	event.player.removeMiniMapQuestActorName( "BFG-Albert" )
	if(!event.player.hasQuestFlag(GLOBAL, "markAlbertStarted1")) {
		event.player.setQuestFlag(GLOBAL, "markAlbertStarted1")
	}
}
albertGrant7.result = DONE
albertGrant.addDialog(albertGrant7, Albert)

//-----------------------------------------------------------------------------------------------------------
//Albert Break                                                                                               
//-----------------------------------------------------------------------------------------------------------
def albertBreak = Albert.createConversation("albertBreak", true, "Z3_Albert_Break")

def albertBreak1 = [id:1]
albertBreak1.npctext = "Hey, hey, hey! I had a feeling you'd be back, %p. Ready to help out?"
albertBreak1.options = []
albertBreak1.options << [text:"I am indeed.", result: 2]
albertBreak1.options << [text:"Nope.", result: DONE]
albertBreak.addDialog(albertBreak1, Albert)

def albertBreak2 = [id:2]
albertBreak2.npctext = "Right, I have some fairly simple tasks. Some of the bigger baddies in Deadman's Pass, the Laceback Galoshes, the Purse, and the OMFG, have been making trouble. I need them taken out."
albertBreak2.options = []
albertBreak2.options << [text:"You can count on me, Albert.", result: 3]
albertBreak2.options << [text:"No, I'm still not interested.", result: DONE]
albertBreak.addDialog(albertBreak2, Albert)

def albertBreak3 = [id:3]
albertBreak3.npctext = "Thank you, %p!"
albertBreak3.flag = [ "Z3_Albert_Active", "!Z3_Albert_Break", "Z3_Albert_NoQuestDoneConvo" ]
albertBreak3.exec = { event ->
	event.player.updateQuest(264, "BFG-Albert")
	event.player.updateQuest(266, "BFG-Albert")
	event.player.updateQuest(268, "BFG-Albert")
}
albertBreak3.result = DONE
albertBreak.addDialog(albertBreak3, Albert)

//-----------------------------------------------------------------------------------------------------------
//Albert NoQuest                                                                                              
//-----------------------------------------------------------------------------------------------------------
def albertNoQuest = Albert.createConversation("albertNoQuest", true, "Z3_Albert_Active", "!markAlbertCompleted", "!Z3_Albert_SnakeComplete", "!Z3_Albert_PurseComplete", "!Z3_Albert_OMFGComplete", "!Z3_Albert_NoOthers", "Z3_Albert_NoQuestDoneConvo")

def albertNoQuest1 = [id:1]
albertNoQuest1.npctext = "Hey, hey, hey! How goes it in Deadman's Pass? Have you dealt with the big baddies?"
albertNoQuest1.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_NoQuestDoneConvo")
	if(event.player.isOnQuest(264, 3) ) {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_SnakeComplete")
		Albert.pushDialog(event.player, "albertLacebackComplete")
	} else if(event.player.isOnQuest(266, 3) ) {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_PurseComplete")
		Albert.pushDialog(event.player, "albertPurseComplete")
	} else if(event.player.isOnQuest(268, 3) ) {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_OMFGComplete")
		Albert.pushDialog(event.player, "albertOMFGComplete")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_NoOthers")
		Albert.pushDialog(event.player, "albertNoOthers")
	}
}
albertNoQuest1.result = DONE
albertNoQuest.addDialog(albertNoQuest1, Albert)

//-----------------------------------------------------------------------------------------------------------
//Albert Active                                                                                              
//-----------------------------------------------------------------------------------------------------------
def albertActive = Albert.createConversation("albertActive", true, "Z3_Albert_Active", "!markAlbertCompleted", "!Z3_Albert_SnakeComplete", "!Z3_Albert_PurseComplete", "!Z3_Albert_OMFGComplete", "!Z3_Albert_NoOthers", "Z3_Albert_QuestDoneConvo")
albertActive.setUrgent(true)

def albertActive1 = [id:1]
albertActive1.npctext = "Hey, hey, hey! How goes it in Deadman's Pass? Have you dealt with the big baddies?"
albertActive1.exec = { event ->
	event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_QuestDoneConvo")
	if(event.player.isOnQuest(264, 3) ) {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_SnakeComplete")
		Albert.pushDialog(event.player, "albertLacebackComplete")
	} else if(event.player.isOnQuest(266, 3) ) {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_PurseComplete")
		Albert.pushDialog(event.player, "albertPurseComplete")
	} else if(event.player.isOnQuest(268, 3) ) {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_OMFGComplete")
		Albert.pushDialog(event.player, "albertOMFGComplete")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_NoOthers")
		Albert.pushDialog(event.player, "albertNoOthers")
	}
}
albertActive1.result = DONE
albertActive.addDialog(albertActive1, Albert)

//-----------------------------------------------------------------------------------------------------------
//Laceback Galoshes Complete                                                                                 
//-----------------------------------------------------------------------------------------------------------
def albertLacebackComplete = Albert.createConversation("albertLacebackComplete", true, "Z3_Albert_SnakeComplete", "!markAlbertCompleted")

def albertLacebackComplete1 = [id:1]
albertLacebackComplete1.playertext = "The Laceback Galoshes is no more... or is it are no more?"
albertLacebackComplete1.result = 2
albertLacebackComplete.addDialog(albertLacebackComplete1, Albert)

def albertLacebackComplete2 = [id:2]
albertLacebackComplete2.npctext = "Good work. I'll let Mark know how helpful you were. I'll let Mark know you took out the Laceback Galoshes."
albertLacebackComplete2.quest = 264
albertLacebackComplete2.flag = "!Z3_Albert_SnakeComplete"
albertLacebackComplete2.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Albert" )
	if(event.player.isOnQuest(266,3) || event.player.isOnQuest(268, 3)) {
		if( event.player.isOnQuest(266, 3) ) {
			event.player.setQuestFlag(GLOBAL, "Z3_Albert_PurseComplete")
			Albert.pushDialog(event.player, "albertPurseComplete")
		} else if( event.player.isOnQuest(268, 3) ) {
			event.player.setQuestFlag(GLOBAL, "Z3_Albert_OMFGComplete")
			Albert.pushDialog(event.player, "albertOMFGComplete")
		}
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_NoOthers")
		Albert.pushDialog(event.player, "albertNoOthers")
	}
	if( event.player.isDoneQuest(266) && event.player.isDoneQuest(268) ) {
		event.player.setQuestFlag(GLOBAL, "markAlbertCompleted")
		event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_Active")
		event.player.grantItem("18022")
	}
}
albertLacebackComplete2.result = DONE
albertLacebackComplete.addDialog(albertLacebackComplete2, Albert)

//-----------------------------------------------------------------------------------------------------------
//Purse Complete                                                                                             
//-----------------------------------------------------------------------------------------------------------
def albertPurseComplete = Albert.createConversation("albertPurseComplete", true, "Z3_Albert_PurseComplete", "!markAlbertCompleted")

def albertPurseComplete1 = [id:1]
albertPurseComplete1.playertext = "The Purse won't be causing anymore trouble."
albertPurseComplete1.result = 2
albertPurseComplete.addDialog(albertPurseComplete1, Albert)

def albertPurseComplete2 = [id:2]
albertPurseComplete2.npctext = "Great, I'll let Mark know you took care of the Purse. Did you happen to take care of anything else?"
albertPurseComplete2.quest = 266
albertPurseComplete2.flag = "!Z3_Albert_PurseComplete"
albertPurseComplete2.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Albert" )
	if( event.player.isOnQuest(268, 3) ) {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_OMFGComplete")
		Albert.pushDialog(event.player, "albertOMFGComplete")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z3_Albert_NoOthers")
		Albert.pushDialog(event.player, "albertNoOthers")
	}
	if( event.player.isDoneQuest(264) && event.player.isDoneQuest(268) ) {
		event.player.setQuestFlag(GLOBAL, "markAlbertCompleted")
		event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_Active")
		event.player.grantItem("18022")
	}
}
albertPurseComplete2.result = DONE
albertPurseComplete.addDialog(albertPurseComplete2, Albert)

//-----------------------------------------------------------------------------------------------------------
//OMFG Complete                                                                                              
//-----------------------------------------------------------------------------------------------------------
def albertOMFGComplete = Albert.createConversation("albertOMFGComplete", true, "Z3_Albert_OMFGComplete", "!markAlbertCompleted")

def albertOMFGComplete1 = [id:1]
albertOMFGComplete1.playertext = "I smashed that OMFG. It won't be acting up again."
albertOMFGComplete1.result = 2
albertOMFGComplete.addDialog(albertOMFGComplete1, Albert)

def albertOMFGComplete2 = [id:2]
albertOMFGComplete2.npctext = "Nice work, %p. Very nice work. I'll tell Mark about the OMFG."
albertOMFGComplete2.flag = "!Z3_Albert_OMFGComplete"
albertOMFGComplete2.quest = 268
albertOMFGComplete2.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Albert" )
	if( event.player.isDoneQuest(264) && event.player.isDoneQuest(266) ) {
		event.player.setQuestFlag(GLOBAL, "markAlbertCompleted")
		event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_Active")
		event.player.grantItem("18022")
	}
}
albertOMFGComplete2.result = DONE
albertOMFGComplete.addDialog(albertOMFGComplete2, Albert)

//-----------------------------------------------------------------------------------------------------------
//No Others Complete                                                                                         
//-----------------------------------------------------------------------------------------------------------
def albertNoOthers = Albert.createConversation("albertNoOthers", true, "Z3_Albert_NoOthers")

def albertNoOthers1 = [id:1]
albertNoOthers1.playertext = "Not yet, Albert. I'll get back at it."
albertNoOthers1.flag = ["!Z3_Albert_NoOthers", "Z3_Albert_NoQuestDoneConvo"]
albertNoOthers1.result = DONE
albertNoOthers.addDialog(albertNoOthers1, Albert)

//-----------------------------------------------------------------------------------------------------------
//All Done                                                                                                   
//-----------------------------------------------------------------------------------------------------------
def albertAllDone = Albert.createConversation("albertAllDone", true, "QuestCompleted_264", "QuestCompleted_266", "QuestCompleted_268")

def albertAllDone1 = [id:1]
albertAllDone1.npctext = "What are you hanging around here for? I've got nothing for you..."
albertAllDone1.result = DONE
albertAllDone.addDialog(albertAllDone1, Albert)