//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Bill = spawnNPC("Rancher Bill-VQS", myRooms.BF1_403, 110, 580)
Bill.setRotation( 45 )
Bill.setDisplayName( "Rancher Bill" )

onQuestStep( 75, 3 ) { event -> event.player.addMiniMapQuestActorName("Rancher Bill-VQS") }

onQuestStep( 64, 2 ) { event -> event.player.addMiniMapQuestLocation("Garlic Field", "BF1_203", 340, 400, "Garlic Field") }

onQuestStep( 64, 3 ) { event -> event.player.removeMiniMapQuestLocation( "Garlic Field" ); event.player.addMiniMapQuestActorName( "Rancher Bill-VQS" ) }


myManager.onEnter( myRooms.BF1_403 ) { event ->
	//**** Screwed-up Player Data resolution ****
	if(stoocieMap.values().contains(event.actor) && stooceIn403 == false ) {
		stooceIn403 = true
	}
	if( isPlayer(event.actor) && event.actor.isOnQuest( 65, 4 ) && event.actor.isDoneQuest( 64 ) ) {
		event.actor.updateQuest( 65, "Rancher Bill-VQS" )
		event.actor.centerPrint( "You complete the PURVIS LOVES RUBELLA task." )
	}
	if( isPlayer(event.actor) && event.actor.isOnQuest( 66, 3 ) && event.actor.isDoneQuest( 75 ) ) {
		event.actor.updateQuest( 66, "Purvis-VQS" )
		event.actor.centerPrint( "You complete the MILKIN' MOOERS task." )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(75, 3) && (event.actor.isOnQuest(65, 4) || event.actor.isDoneQuest(65))) {
		event.actor.updateQuest(75, "Purvis-VQS")
		if(event.actor.isOnQuest(75, 3)) { event.actor.updateQuest(75, "Rubella-VQS") }
		event.actor.centerPrint("You complete the BILL'S SHIPMENT task.")
	}
	if(isPlayer(event.actor)) { event.actor.unsetQuestFlag( GLOBAL, "Z03RingRewardInProgress" ) }
}

stooceIn403 = false
stoocie = null

bf1RoomList = ["BF1_2","BF1_3","BF1_4","BF1_5","BF1_6","BF1_101","BF1_102","BF1_103","BF1_104","BF1_105","BF1_106","BF1_201","BF1_202","BF1_203","BF1_204","BF1_205","BF1_206","BF1_301","BF1_302","BF1_303","BF1_304","BF1_305","BF1_306","BF1_401","BF1_402","BF1_403","BF1_404","BF1_405","BF1_406","BF1_502","BF1_503","BF1_504","BF1_505","BF1_506","BF1_603","BF1_604","BF1_605","BF1_606","BF1_706","BF1_705","BF1_803","BF1_804","BF1_805","BF1_806"]

stoocieMap = new HashMap()

def stoocieMapCleanup() {
	myManager.schedule(60) {
		//println "##### Ran stoocieMapCleanup() #####"
		stoocieMapCleanup()
		stoocieMap.clone().keySet().each {
			//println "###### it = ${it} #####"
			if( !isOnline(it) ) {
				stoocieMap.get(it).dispose()
				stoocieMap.remove(it)
			} else if( !bf1RoomList.contains(it?.getRoomName()) ) {
				//println "##### Disposing ${stoocieMap.get(it)} because ${it} is not in BF1 #####"
				stoocieMap.get(it).dispose()
				stoocieMap.remove(it)
			}
		}
	}
}

stoocieMapCleanup()

def stoocieFollow( event ) {
	if(event.actor.isOnQuest(55, 2) && event.actor.hasQuestFlag(GLOBAL, "Z4_Stooce_OutOfHouse") ) {
		stoocie.startFollow( event.actor )
		event.actor.unsetQuestFlag(GLOBAL, "Z4_Stooce_OutOfHouse")
		event.actor.setQuestFlag(GLOBAL, "Z4_Stooce_ToBill")
	}
}

//Spawn Stoocie when player on quest enters Bill's Ranch after escorting Stoocie
myManager.onEnter( myRooms.BF1_705 ) { event ->
	//println "**** LEFT HOUSE ON THE HILL ****"
	if(isPlayer(event.actor) && event.actor.isOnQuest(55, 2) && event.actor.hasQuestFlag(GLOBAL, "Z4_Stooce_OutOfHouse")) {
	
		stoocie = makeCritter( "cow", myRooms.BF1_705, 195, 465 )
		stoocie.setDisplayName("Stoocie")
		stoocie.setUsable( true )
		stoocieFollow( event )
		
		stoocieMap.put(event.actor, stoocie)
	}
}

myManager.onExit(myRooms.BF1_705) { event ->
	//println "##### ${event.actor} leaving BF1_705 #####"
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z4_Stooce_ToBill") && !bf1RoomList.contains(event.actor.getRoomName())) {
		//println "##### DISPOSING OF ${stoocieMap.get(event.actor)} #####"
		if(stoocieMap.containsKey(event.actor) ) { 
			event.actor.unsetQuestFlag(GLOBAL, "Z4_Stooce_ToBill")
			stoocieMap.get(event.actor)?.dispose()
			stoocieMap.remove(event.actor)
		}
	}
}

//TODO - Counter for Plantin' Garlic
//TODO - Complete for House on the Hill

//-----------------------------------------------------------------------------------------------------------
//Default conversation when player visits Bill before earning his trust                                      
//-----------------------------------------------------------------------------------------------------------
def billDefault = Bill.createConversation("billDefault", true, "!Z3_RancherBill_HelpingLarry_Active", "!Z3_RancherBill_HelpingLarry_Complete", "!QuestCompleted_75", "!QuestStarted_75", "!QuestStarted_92", "!QuestStarted_76", "!QuestStarted_55", "!QuestCompleted_55")

def default1 = [id:1]
default1.npctext = "Git offa my land!"
default1.options = []
default1.options << [text:"Your land? I didn't even know. I was just looking for something to do.", result:2]
default1.options << [text:"Sorry Bill, I was just hoping I could help you around the ranch.", result:3]
billDefault.addDialog(default1, Bill)

def default2 = [id:2]
default2.npctext = "If'n yer lookin' fer somethin' ta do, ya should have a chat wit' Larry. The durn fool's been 'ere on the ranch fer ages. I'm sure 'e's anxious to git 'ome and I'm anxious fer 'im to git gone. Go 'elp 'im git the subway runnin' an' mebbe we gots somethin' ta talk 'bout."
default2.flag = "Z3_RancherBill_HelpingLarry_Active"
default2.result = DONE
billDefault.addDialog(default2, Bill)

def default3 = [id:3]
default3.npctext = "Help on the ranch? I don' even know ya! Go 'elp Larry get the subway runnin' an' mebbe I'll have somethin' fer ya."
default3.flag = ["Z3_RancherBill_HelpingLarry_Active", "Z2TalkedToBill"] //"Z2TalkedToBill" is to change Remofs conversation flag for his post-quest default convo after the player has visited Bill.
default3.result = DONE
billDefault.addDialog(default3, Bill)

//-----------------------------------------------------------------------------------------------------------
//Default conversation when player visits Bill before earning his trust                                      
//-----------------------------------------------------------------------------------------------------------
def billHelpLarry = Bill.createConversation("billHelpLarry", true, "Z3_RancherBill_HelpingLarry_Active", "!QuestStarted_75", "!QuestCompleted_75")

def help1 = [id:1]
help1.npctext = "Git offa my land or go talk ta Larry!"
help1.flag = "Z2TalkedToBill" //"Z2TalkedToBill" is to change Remofs conversation flag for his post-quest default convo after the player has visited Bill.
help1.result = DONE
billHelpLarry.addDialog(help1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Default conversation when player visits Bill before earning his trust                                      
//-----------------------------------------------------------------------------------------------------------
def billShipment = Bill.createConversation("billShipment", true, "QuestStarted_75:2")

def shipment1 = [id:1]
shipment1.npctext = "Git offa my land!"
shipment1.result = 2
billShipment.addDialog(shipment1, Bill)

def shipment2 = [id:2]
shipment2.playertext = "But, Bill, Larry sent me to tell you that the subway is running again and he's headed home to Barton Town."
shipment2.result = 3
billShipment.addDialog(shipment2, Bill)

def shipment3 = [id:3]
shipment3.npctext = "Well'n... mebbe I done spoke too soon. If'n yer the one what got the subway a'runnin' mebbe I gots some work fer ya."
shipment3.result = 4
billShipment.addDialog(shipment3, Bill)

def shipment4 = [id:4]
shipment4.playertext = "I was the one. What kind of work do you have for me?"
shipment4.result = 5
billShipment.addDialog(shipment4, Bill)

def shipment5 = [id:5]
shipment5.npctext = "The shipment in't quite ready. Purvis were supposed ta git th' cows ready but I 'aven't 'eard from 'im. An' fair Rubella were supposed ta git th' chickens ready. I needs someone ta tell 'em git the shipment ready."
shipment5.options = []
shipment5.options << [text:"Sure, I can help out. (**NOTE: The reward for completing Rancher Bill's series of tasks includes the selection of a new ring!**)", result:6]
shipment5.options << [text:"I'm too busy right now.", result:10]
billShipment.addDialog(shipment5, Bill)

def shipment6 = [id:6]
shipment6.npctext = "Great! Any questions ya lemme know."
shipment6.options = []
shipment6.options << [text:"Where can I find Purvis?", result:7]
shipment6.options << [text:"Where can I find Rubella?", result:8]
shipment6.options << [text:"I think I'm all set... I'll go let Purvis and Rubella know you're ready for the shipment.", result:9]
billShipment.addDialog(shipment6, Bill)

def shipment7 = [id:7]
shipment7.npctext = "Purvis'll be wanderin' the field ta the north. 'e tends ta' roam the peri... perime... the outside parts lookin' fer knocked over cows."
shipment7.options = []
shipment7.options << [text:"Where can I find Rubella?", result:8]
shipment7.options << [text:"I think I'm all set... I'll go let Purvis and Rubella know you're ready for the shipment.", result:9]
billShipment.addDialog(shipment7, Bill)

def shipment8 = [id:8]
shipment8.npctext = "Rubella spends mosta 'er time 'angin' about over by the farm'ouse northwest of 'ere. Ya can't miss 'er... she's the one wit' curls fer hair."
shipment8.options = []
shipment8.options << [text:"Where can I find Purvis?", result:7]
shipment8.options << [text:"I think I'm all set... I'll go let Purvis and Rubella know you're ready for the shipment.", result:9]
billShipment.addDialog(shipment8, Bill)

def shipment9 = [id:9]
shipment9.npctext = "Tell 'em ta hurry. I wanna git them shipments together right fast. An' when yer done git back over here. I got more work fer ya."
shipment9.quest = 75
shipment9.flag = "Z2TalkedToBill" //"Z2TalkedToBill" is to change Remofs conversation flag for his post-quest default convo after the player has visited Bill.
shipment9.exec = { event ->
	event.player.removeMiniMapQuestActorName("Rancher Bill-VQS")
	event.player.addMiniMapQuestActorName("Purvis-VQS")
	event.player.addMiniMapQuestActorName("Rubella-VQS")
}
shipment9.result = DONE
billShipment.addDialog(shipment9, Bill)

def shipment10 = [id:10]
shipment10.npctext = "Well'n... ya better git offa my land unless yer'n gonna help!"
shipment10.flag = "Z2TalkedToBill" //"Z2TalkedToBill" is to change Remofs conversation flag for his post-quest default convo after the player has visited Bill.
shipment10.result = DONE
billShipment.addDialog(shipment10, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player is completing Chicken and Cow quests                                             
//-----------------------------------------------------------------------------------------------------------
def billRewardShipment = Bill.createConversation("billRewardShipment", true, "QuestStarted_75:4", "!QuestCompleted_75", "!QuestStarted_65:4")

def rewardShipment1 = [id:1]
rewardShipment1.npctext = "'owdy! I been lookin' fer ya. Purvis an' Rubella sent their shipments over, wanted to thank ya. 'Ere, take this!"
rewardShipment1.quest = 75
rewardShipment1.exec = { event -> 
	event.player.removeMiniMapQuestActorName("Rancher Bill-VQS")
}
rewardShipment1.result = DONE
billRewardShipment.addDialog(rewardShipment1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player is completing Chicken, Chicken, and Poem quests                                  
//-----------------------------------------------------------------------------------------------------------
def billRewardShipmentPoem = Bill.createConversation("billRewardShipmentPoem", true, "QuestStarted_65:4", "!Z3_GarlicConvo", "QuestStarted_75:4")
billRewardShipmentPoem.setUrgent(true)

def rewardShipmentPoem1 = [id:1]
rewardShipmentPoem1.npctext = "'owdy! I weren't 'spectin' ya back so soon."
rewardShipmentPoem1.playertext = "Bill... Purvis needs your advice with Rubella."
rewardShipmentPoem1.flag = "Z3_GarlicConvo"
rewardShipmentPoem1.exec = { event -> 
	event.player.updateQuest( 75, "Rancher Bill-VQS" )
	event.player.removeMiniMapQuestActorName("Rancher Bill-VQS")
	Bill.pushDialog( event.player, "billPlantinGarlic" )
} 
rewardShipmentPoem1.result = DONE
billRewardShipmentPoem.addDialog(rewardShipmentPoem1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player is completing Chicken, Chicken, and Poem quests                                  
//-----------------------------------------------------------------------------------------------------------
def billRewardShipmentComplete = Bill.createConversation("billRewardShipmentComplete", true, "QuestStarted_65:4", "!Z3_GarlicConvo", "QuestCompleted_75")
billRewardShipmentComplete.setUrgent(true)

def rewardShipmentComplete1 = [id:1]
rewardShipmentComplete1.npctext = "'owdy! I weren't 'spectin' ya back so soon."
rewardShipmentComplete1.playertext = "Bill... Purvis needs your advice with Rubella."
rewardShipmentComplete1.flag = "Z3_GarlicConvo"
rewardShipmentComplete1.exec = { event -> 
	event.player.removeMiniMapQuestActorName("Rancher Bill-VQS")
	Bill.pushDialog( event.player, "billPlantinGarlic" )
} 
rewardShipmentComplete1.result = DONE
billRewardShipmentComplete.addDialog(rewardShipmentComplete1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation after player has completed Love quest                                                         
//-----------------------------------------------------------------------------------------------------------
def billPlantinGarlic = Bill.createConversation("billPlantinGarlic", true, "QuestStarted_65:4", "Z3_GarlicConvo")

def plantinGarlic1 = [id:1]
plantinGarlic1.npctext = "That dang boy. I tol' 'im to stop chasin' that girl. My garlic 'opped outta the groun' and took off. I got a vampire overlookin' my ranch and no garlic to fight 'im! I got n'time fer girls and poems."
plantinGarlic1.result = 2
billPlantinGarlic.addDialog(plantinGarlic1, Bill)

def plantinGarlic2 = [id:2]
plantinGarlic2.playertext = "Did you say a vampire?"
plantinGarlic2.result = 3
billPlantinGarlic.addDialog(plantinGarlic2, Bill)

def plantinGarlic3 = [id:3]
plantinGarlic3.npctext = "Why d'ya think I grow garlic? Because I like the smell? I'm a rancher, not a farmer. The garlic is fer protection!"
plantinGarlic3.result = 4
billPlantinGarlic.addDialog(plantinGarlic3, Bill)

def plantinGarlic4 = [id:4]
plantinGarlic4.playertext = "So now you have no protection?"
plantinGarlic4.result = 5
billPlantinGarlic.addDialog(plantinGarlic4, Bill)

def plantinGarlic5 = [id:5]
plantinGarlic5.npctext = "Well, the gate's closed... but that won't keep 'em out forever. Dangit! I wish I could git ma garlic back in the groun'."
plantinGarlic5.options = []
plantinGarlic5.options << [text:"Maybe I could help?", result: 6]
plantinGarlic5.options << [text:"Good luck with that... I've got to go.", quest: 65, flag: "Z3_Bill_Garlic_Break", exec: { event - event.player.removeMiniMapQuestActorName("Rancher Bill-VQS") }, result: DONE]
billPlantinGarlic.addDialog(plantinGarlic5, Bill)

def plantinGarlic6 = [id:6]
plantinGarlic6.npctext = "Yee-aw! Ya could at that! Jus' grab a bunch, makem stop squirmin', and stick 'em back in the groun'. They's all aroun' the ranch, especially in the woods to the northeast."
plantinGarlic6.quest = 64
plantinGarlic6.flag = "!Z3_GarlicConvo"
plantinGarlic6.exec = { event ->
	event.player.updateQuest(65, "Rancher Bill-VQS")
	event.player.removeMiniMapQuestActorName("Rancher Bill-VQS")
}	
plantinGarlic6.result = DONE
billPlantinGarlic.addDialog(plantinGarlic6, Bill)

//-----------------------------------------------------------------------------------------------------------
//Plantin' Garlic Break                                                                                      
//-----------------------------------------------------------------------------------------------------------
def billGarlicBreak = Bill.createConversation("billGarlicBreak", true, "Z3_Bill_Garlic_Break")

def billGarlicBreak1 = [id:1]
billGarlicBreak1.npctext = "Well, lookee 'ere who's come 'round again. Good to see ya, %p. 'ope yer 'ere to see about muh garlic."
billGarlicBreak1.options = []
billGarlicBreak1.options << [text:"Oh fine, Bill, I'll help. What do you need me to do?", result: 2]
billGarlicBreak1.options << [text:"I told you no, and I meant it.", result: DONE]
billGarlicBreak.addDialog(billGarlicBreak1, Bill)

def billGarlicBreak2 = [id:2]
billGarlicBreak2.npctext = "Jus' 'ead over to the northeast, grab a bunch, and then come back to the field and stick 'em in the groun'."
billGarlicBreak2.quest = 64
billGarlicBreak2.flag = "!Z3_Bill_Garlic_Break"
billGarlicBreak.addDialog(billGarlicBreak2, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation while player is on Plantin' Garlic quest                                                      
//-----------------------------------------------------------------------------------------------------------
def billGarlicActive = Bill.createConversation("billGarlicActive", true, "QuestStarted_64", "!QuestStarted_64:4")

def garlicActive1 = [id:1]
garlicActive1.npctext = "What're ya doin' standin' aroun'? Git ma garlic back in the groun'!"
garlicActive1.result = DONE
billGarlicActive.addDialog(garlicActive1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation when player has planted garlic                                                                
//-----------------------------------------------------------------------------------------------------------
def billGarlicEnd = Bill.createConversation("billGarlicEnd", true, "QuestStarted_64:4")

def garlicEnd1 = [id:1]
garlicEnd1.npctext = "Did ya git ma garlic back in the groun'?"
garlicEnd1.result = 2
billGarlicEnd.addDialog(garlicEnd1, Bill)

def garlicEnd2 = [id:2]
garlicEnd2.playertext = "You bet I did, Bill. Planted and everything."
garlicEnd2.result = 3
billGarlicEnd.addDialog(garlicEnd2, Bill)

def garlicEnd3 = [id:3]
garlicEnd3.npctext = "Finally, some good news! Seems like it's nothin' but bad news lately. My prize cow, Stoocie, has gone missin'!"
garlicEnd3.result = 4
billGarlicEnd.addDialog(garlicEnd3, Bill)

def garlicEnd4 = [id:4]
garlicEnd4.playertext = "Missing? Where could a cow go?"
garlicEnd4.result = 5
billGarlicEnd.addDialog(garlicEnd4, Bill)

def garlicEnd5 = [id:5]
garlicEnd5.npctext = "I sent Purvis after 'er. 'e followed 'er 'oof prints all the way down to the gate to the 'ouse and got skeered."
garlicEnd5.options = []
garlicEnd5.options << [text:"You mean the vampire's house? Sounds like fun. Count me in!", result: 6]
garlicEnd5.options << [text:"So, you want me to go to the vampire's house? Do you think I'm crazy? No way!", result: 7]
billGarlicEnd.addDialog(garlicEnd5, Bill)

def garlicEnd6 = [id:6]
garlicEnd6.npctext = "Er... yeah! A 'ootin 'ollerin good time! Ya can find the gate to the south. There's no reason ta go in the 'ouse, though. This seems more like the work of misch... mischiev... rascally underlings."
garlicEnd6.quest = 55
garlicEnd6.exec = { event ->
	event.player.updateQuest( 64, "Rancher Bill-VQS")
	event.player.removeMiniMapQuestActorName("Rancher Bill-VQS")
}
garlicEnd6.result = DONE
billGarlicEnd.addDialog(garlicEnd6, Bill)

def garlicEnd7 = [id:7]
garlicEnd7.npctext = "Er... I've got chickens that don't squawk as loud as ya. There's no need to be skeered, ya probably won't even hafta go near the 'ouse."
garlicEnd7.options = []
garlicEnd7.options << [text:"Nobody... calls... me... chicken!", result: 8]
garlicEnd7.options << [text:"Nice try, but you aren't going to trick me into getting anywhere near a vampire.", result: 9]
billGarlicEnd.addDialog(garlicEnd7, Bill)

def garlicEnd8 = [id:8]
garlicEnd8.npctext = "Yee-aw! Go get muh Stoocie! Ya can find the gate to the south."
garlicEnd8.quest = 55
garlicEnd8.exec = { event ->
	event.player.updateQuest( 64, "Rancher Bill-VQS")
	event.player.removeQuestFlag("Rancher Bill-VQS")
}
garlicEnd8.result = DONE
billGarlicEnd.addDialog(garlicEnd8, Bill)

def garlicEnd9 = [id:9]
garlicEnd9.npctext = "Shucks. I'll pay ya."
garlicEnd9.options = []
garlicEnd9.options << [text:"Now you're talking my language. Where do I start?", result: 8]
garlicEnd9.options << [text:"I'll think about it.", exec: { event -> event.player.removeQuestFlag("Rancher Bill-VQS") }, flag: "Z4_Stooce_Break", quest: 64, result: DONE]
billGarlicEnd.addDialog(garlicEnd9, Bill)

//-----------------------------------------------------------------------------------------------------------
//Stoocie break conversation                                                                                 
//-----------------------------------------------------------------------------------------------------------
def stooceBreak = Bill.createConversation("stooceBreak", true, "Z4_Stooce_Break")

def stooceBreak1 = [id:1]
stooceBreak1.npctext = "Stooooooocie! Please tell me ya came back 'ere to go look for 'er."
stooceBreak1.options = []
stooceBreak1.options << [text:"Of course I'll look for her, Bill.", result: 2]
stooceBreak1.options << [text:"No. I'm not wasting my time looking for a silly cow!", result: 3]
stooceBreak.addDialog(stooceBreak1, Bill)

def stooceBreak2 = [id:2]
stooceBreak2.npctext = "Oh thank Gaia! I knew I could count on ya. Purvis done followed 'er 'oof prints down south to the gate afore 'e got too skeered and ran 'ome. I need ya to go through that gate and find muh Stoocie!"
stooceBreak2.quest = 55
stooceBreak2.flag = "Z4_Stooce_ToBill"
stooceBreak2.result = DONE
stooceBreak.addDialog(stooceBreak2, Bill)

def stooceBreak3 = [id:3]
stooceBreak3.npctext = "Noooooooo! Yer a cruel sort!"
stooceBreak3.result = DONE
stooceBreak.addDialog(stooceBreak3, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation while player is on Stoocie quest                                                              
//-----------------------------------------------------------------------------------------------------------
def billStooceActive = Bill.createConversation("billStooceActive", true, "QuestStarted_55:2", "!Z4_Stooce_ToBill")

def stooceActive1 = [id:1]
stooceActive1.npctext = "Go git muh Stoocie! Ya just need to go through the gate south of here."
stooceActive1.result = DONE
billStooceActive.addDialog(stooceActive1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Bounce in to see if Stooce is in the room                                                                  
//-----------------------------------------------------------------------------------------------------------
def billStooceBounce = Bill.createConversation("billStooceBounce", true, "QuestStarted_55:2", "Z4_Stooce_ToBill", "!Z4_Bill_StooceHere", "!Z4_Bill_StooceNotHere")

def stooceBounce1 = [id:1]
stooceBounce1.npctext = "'old it right there! This 'ere is private prop'rty..."
stooceBounce1.exec = { event ->
	if(stooceIn403 == true) {
		event.player.setGlobalFlag( "Z4_Bill_StooceHere" )
		event.player.unsetQuestFlag(GLOBAL, "Z4_Stooce_ToBill")
		Bill.pushDialog( event.player, "billStooceEnd" )
	}
	if(stooceIn403 == false) {
		event.player.setGlobalFlag( "Z4_Bill_StooceNotHere" )
		event.player.unsetQuestFlag(GLOBAL, "Z4_Stooce_ToBill")
		Bill.pushDialog( event.player, "billStooceNotHere" )
	}
}
stooceBounce1.result = DONE
billStooceBounce.addDialog( stooceBounce1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Converversation after bounce if Stooce not in room                                                         
//-----------------------------------------------------------------------------------------------------------
def billStooceNotHere = Bill.createConversation("billStooceNotHere", true, "QuestStarted_55:2", "Z4_Bill_StooceNotHere")

def stooceNotHere1 = [id:1]
stooceNotHere1.npctext = "...oh, it's you. I still don't see muh Stooce, though. Go git 'er!"
stooceNotHere1.flag = "!Z4_Bill_StooceNotHere"
stooceNotHere1.result = DONE
billStooceNotHere.addDialog(stooceNotHere1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Conversation after bounce if Stooce in room                                                                
//-----------------------------------------------------------------------------------------------------------
def billStooceEnd = Bill.createConversation("billStooceEnd", true, "QuestStarted_55:2", "Z4_Bill_StooceHere")

def stooceEnd1 = [id:1]
stooceEnd1.npctext = "...wait it's you! Stoocie!!! Ya saved 'er!"
stooceEnd1.options = []
stooceEnd1.options << [text:"Yeah! She was so scared... but she's a good little cow and followed me home.", result: 2]
stooceEnd1.options << [text:"Whoa, calm down. It's just a cow.", result: 6]
billStooceEnd.addDialog(stooceEnd1, Bill)

def stooceEnd2 = [id:2]
stooceEnd2.npctext = "Stoocie's a good cow. She's a moo-moo. A moo-moo-goo-goo. Yes she is."
stooceEnd2.options = []
stooceEnd2.options << [text:"A moo-moo-goo-goo-boo-boo?", result: 3]
stooceEnd2.options << [text:"I hope you were talking to the cow and not to me, because I have no idea what you just said.", result: 5]
billStooceEnd.addDialog(stooceEnd2, Bill)

def stooceEnd3 = [id:3]
stooceEnd3.npctext = "A moo-moo-goo-goo-boo-boo-doo-doo."
stooceEnd3.result = 4
billStooceEnd.addDialog(stooceEnd3, Bill)

def stooceEnd4 = [id:4]
stooceEnd4.playertext = "Do you need a private moment with your cow? I'll leave you two alone."
stooceEnd4.result = 7
billStooceEnd.addDialog(stooceEnd4, Bill)

def stooceEnd5 = [id:5]
stooceEnd5.npctext = "Course I was talkin' ta Stoocie! She's a special cow. She can understan' me. Isn't that right Stoocie? Who's my moo-moo-goo-goo-boo-goo-doo-doo?"
stooceEnd5.result = 4
billStooceEnd.addDialog(stooceEnd5, Bill)

def stooceEnd6 = [id:6]
stooceEnd6.npctext = "Jus' a cow? Stoocie is a special cow! She can understan' me. Isn't that right Stoocie? Who's my moo-moo-goo-goo-boo-goo-doo-doo?"
stooceEnd6.result = 4
billStooceEnd.addDialog(stooceEnd6, Bill)

def stooceEnd7 = [id:7]
stooceEnd7.npctext = "Oh! Before ya go, Rubella told me to tell ya that she's lookin' fer ya."
stooceEnd7.playertext = "Uhm... okay. Thanks Bill."
stooceEnd7.flag = ["Z3_Bill_Katsumi_Redirect", "!Z4_Bill_StooceHere"]
stooceEnd7.exec = { event -> 
	event.player.getCrew().each {
		if(it.isOnQuest(55, 2)){
			it.updateQuest(55, "Rancher Bill-VQS")
			if(!it == event.player) {
				it.centerPrint("You have completed The Stooce is Loose.")
			}
		}
	}
	event.player.removeMiniMapQuestActorName("Rancher Bill-VQS")
	event.player.getCrew().each {
		//println "#### it = ${it} ####"
		if(stoocieMap.containsKey(it)) {
			myStoocie = stoocieMap.get(it)
			myStoocie.startFollow( Bill )
			//println "#### ${stoocieMap.get(it)} following Bill ####"
			Bill.say("Stoocie, why don't ya 'ead out to pasture?")
			myManager.schedule(3) {
				//println "#### ${myStoocie} should be talking now ####}"
				myStoocie.say("Moo!")
				def stoocieDespawnMove = makeNewPatrol()
				stoocieDespawnMove.addPatrolPoint( myRooms.BF1_403, 215, 350, 0 )
				stoocieDespawnMove.addPatrolPoint( myRooms.BF1_402, 405, 525, 20 )
				myStoocie.setPatrol(stoocieDespawnMove)
				myStoocie.startPatrol()
				myManager.schedule(30) { 
					myStoocie.dispose()
					myStoocie = null
					stoocieIn403 = false
				}
			}
		}
	}
}
stooceEnd7.result = DONE
billStooceEnd.addDialog(stooceEnd7, Bill)

//-----------------------------------------------------------------------------------------------------------
//Machine Repair - Give                                                                                      
//-----------------------------------------------------------------------------------------------------------
def billMachineGive = Bill.createConversation("billMachineGive", true, "QuestCompleted_55", "!QuestStarted_69", "!QuestCompleted_69")

def machineGive1 = [id:1]
machineGive1.npctext = "%p, you're a lifesaver. We're starting to get on top of things 'ere on the ranch thanks to yer 'elp! If'n yer interested, I gots something else could use yer touch."
machineGive1.options = []
machineGive1.options << [text:"Sure, what do you need help with?", result: 2]
machineGive1.options << [text:"Maybe... tell me what you need help with and I'll let you know.", result: 3]
machineGive1.options << [text:"Sorry, Bill. I'm just way to busy right now.", result: 4]
billMachineGive.addDialog(machineGive1, Bill)

def machineGive2 = [id:2]
machineGive2.npctext = "I was thinkin' ya might be the one to git the machines runnin' again. Seems ta me that'll need someone with figurin' skills like yerself to remedy."
machineGive2.quest = 69
machineGive2.exec = { event ->
	event.player.addMiniMapQuestLocation( "First Windmill", "BF1_402", 500, 300, "First Windmill" )
}
machineGive2.result = DONE
billMachineGive.addDialog(machineGive2, Bill)

def machineGive3 = [id:3]
machineGive3.npctext = "Well, Rubella an' Purvis 'ave got the cows and chickens purty well in 'and, but I'm none to 'opeful when it comes to 'em fixin' the tractor and the windmills."
machineGive3.options = []
machineGive3.options << [text:"Fixing the tractor and windmills. What are you talking about?", result: 2]
machineGive3.options << [text:"Sorry, but I'm too busy right now Bill.", result: 4]
billMachineGive.addDialog(machineGive3, Bill)

def machineGive4 = [id:4]
machineGive4.npctext = "Shucks... I suppose I'll have to do it myself. Lemme know if ya change yer mind!"
machineGive4.result = DONE
billMachineGive.addDialog(machineGive4, Bill)

//-----------------------------------------------------------------------------------------------------------
//Machine Repair - Active                                                                                    
//-----------------------------------------------------------------------------------------------------------
def billMachineActive = Bill.createConversation("billMachineActive", true, "QuestStarted_69", "!QuestStarted_69:5", "!QuestStarted_69:1")

def machineActive1 =[id:1]
machineActive1.npctext = "How's them machines lookin'? They fixed yet?"
machineActive1.result = DONE
billMachineActive.addDialog(machineActive1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Machine Repair - End                                                                                       
//-----------------------------------------------------------------------------------------------------------
def billMachineEnd = Bill.createConversation("billMachineEnd", true, "QuestStarted_69:5", "!Z03MachineEndLastConvoCompleted", "!Z03RingRewardInProgress" )
billMachineEnd.setUrgent(true)

def machineEnd1 = [id:1]
machineEnd1.npctext  = "I can see them windmills a'turnin' and a plume of smoke comin' from the tractor. Good work, %p."
machineEnd1.options = []
machineEnd1.options << [text:"Would you expect any less from me?", result: 2]
machineEnd1.options << [text:"I just do what I can to help.", result: 3]
billMachineEnd.addDialog(machineEnd1, Bill)

def machineEnd2 = [id:2]
machineEnd2.npctext = "No, I've come to expect the utmost from ya. We ne'er woulda got the ranch back up and runnin' without ya. Listen, 'fore ya 'ead out I'd like to give ya a ring as muh thanks fer 'elpin'."
machineEnd2.result = 4
billMachineEnd.addDialog(machineEnd2, Bill)

def machineEnd3 = [id:3]
machineEnd3.npctext = "Ya do a 'eck of a job at it! Ne'er woulda got the ranch back up an runnin' without ya. 'ere. Listen, 'fore ya 'ead out I'd like to give ya a ring as muh thanks fer 'elpin'."
machineEnd3.result = 4
billMachineEnd.addDialog(machineEnd3, Bill)

def machineEnd4 = [id:4]
machineEnd4.playertext = "Awesome!"
machineEnd4.exec = { event ->
	player = event.player
	event.player.setQuestFlag( GLOBAL, "Z03RingRewardInProgress" )
	makeMainMenu( player )
}
machineEnd4.result = DONE
billMachineEnd.addDialog( machineEnd4, Bill )

//-----------------------------------------------------------------------------------------------------------
//Bill - All Complete                                                                                        
//-----------------------------------------------------------------------------------------------------------
def billAllComplete = Bill.createConversation("billAllComplete", true, "QuestCompleted_69", "!QuestStarted_72:2")

def allComplete1 = [id:1]
allComplete1.npctext = "%p! I gots n'more work fer ya now that things're back to normal. Again, muh thanks."
allComplete1.result = DONE
billAllComplete.addDialog(allComplete1, Bill)

//-----------------------------------------------------------------------------------------------------------
//Bill - Needle Nose helper                                                                                  
//-----------------------------------------------------------------------------------------------------------
def billLoganNeedle = Bill.createConversation("billLoganNeedle", true, "QuestStarted_72:2", "!QuestStarted_65:4", "!QuestStarted_75:4", "!QuestStarted_69:5", "!QuestStarted_55:3", "!QuestStarted_64:4", "!QuestStarted_75:2")

def loganNeedle1 = [id:1]
loganNeedle1.npctext = "Well 'owdy! What brings ya back to botherin' with ol' Bill?"
loganNeedle1.playertext = "Actually, Bill, I was sent down this way by Logan. I'm down here looking for something for him and he said you might be able to help."
loganNeedle1.result = 2
billLoganNeedle.addDialog(loganNeedle1, Bill)

def loganNeedle2 = [id:2]
loganNeedle2.npctext = "Izatafact? What kinda help did ya think I might be offerin'?"
loganNeedle2.playertext = "Logan and I are trying to build a... let's just say a device. He thinks we need a needle nose like the one you sent him, only bigger. Any idea where I might find something like that?"
loganNeedle2.result = 3
billLoganNeedle.addDialog(loganNeedle2, Bill)

def loganNeedle3 = [id:3]
loganNeedle3.npctext = "Well, of course I can tell ya where to find something like that! Ya can find an aggressive skeeter all the way south of the ranch. I'm sure it'd have a nose of the sort ya lookin' fer. Durn thing got a nose the size of my arm."
loganNeedle3.playertext = "Cool. You don't mind me whacking it?"
loganNeedle3.result = 4
billLoganNeedle.addDialog(loganNeedle3, Bill)

def loganNeedle4 = [id:4]
loganNeedle4.npctext = "Mind? Shucks, ya gonna be doin' me a favor. Only reason the dang thing is still down there is on accounta my inability to git rid of it meself."
loganNeedle4.playertext = "Great. And, thanks."
loganNeedle4.result = DONE
billLoganNeedle.addDialog(loganNeedle4, Bill)

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 1
CLdecimal = 5

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["New Rings", "Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "New Rings" ) {
			makeNewRingMenu( player )
		}
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z03RingRewardInProgress" )
		}
	}
}

//====================================================
// NEW RINGS                                          
//====================================================
def makeNewRingMenu( player ) {
	titleString = "New Rings Menu"
	descripString = "These rings are new to the list this time."
	diffOptions = [ "Defibrillate", "Guns, Guns, Guns", "Hack", "Iron Will", "My Density", "Main Menu"  ]
	
	uiButtonMenu( player, "newRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Hack", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Hornet Nest", "Hot Foot", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Quicksand", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Pot Lid", "Teflon Spray", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.updateQuest( 69, "Rancher Bill-VQS" ) //complete the MACHINE REPAIR task
	event.actor.updateQuest(250, "Rancher Bill-VQS")
	event.actor.setQuestFlag( GLOBAL, "Z03MachineEndLastConvoCompleted" )
	event.actor.setQuestFlag( GLOBAL, "Z03ThirdRingGrantReceived" )
}

