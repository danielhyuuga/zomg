import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------
def spawner2 = myRooms.BF1_406.spawnSpawner( "BF1_406_Spawner2", "fluff", 1)
spawner2.setPos( 125, 90 )
spawner2.setInitialMoveForChildren( "BF1_406", 775, 100 )
spawner2.setHome( "BF1_406", 775, 100 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander (true)
spawner2.setMonsterLevelForChildren( 2.3 )

def spawner3 = myRooms.BF1_406.spawnSpawner( "BF1_406_Spawner3", "fluff", 1)
spawner3.setPos( 125, 100 )
spawner3.setInitialMoveForChildren( "BF1_406", 120, 575 )
spawner3.setHome( "BF1_406", 120, 575 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander (true)
spawner3.setMonsterLevelForChildren( 2.3 )

def spawner5 = myRooms.BF1_406.spawnSpawner( "BF1_406_Spawner5", "fluff", 1)
spawner5.setPos( 125, 120 )
spawner5.setInitialMoveForChildren( "BF1_406", 505, 345 )
spawner5.setHome( "BF1_406", 505, 345 )
spawner5.setSpawnWhenPlayersAreInRoom( true )
spawner5.setWaitTime( 50 , 70 )
spawner5.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner5.childrenWander (true)
spawner5.setMonsterLevelForChildren( 2.3 )

//Alliances
spawner2.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner5 )
spawner3.allyWithSpawner( spawner5 )

//Spawning
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()
spawner5.forceSpawnNow()