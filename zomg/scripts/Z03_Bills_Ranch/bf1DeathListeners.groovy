//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

mosquitoList = [] as Set
fluffList = [] as Set
garlicList = [] as Set

//-----------------------------------------------------------------------------------
//Death listener for Mosquitos                                                       
//-----------------------------------------------------------------------------------
def bf1MosquitoDeathScheduler() {
	myManager.schedule(1) { bf1MosquitoDeathScheduler() }
	myRooms.values().each() {
		//println "#### Eaching through bf1RoomList ####"
		it.getActorList().each() {
			if(isMonster(it) && it.getMonsterType() == "mosquito" && !mosquitoList.contains(it)) {
				//println "#### Scheduling runOnDeath event for ${it} ####"
				mosquitoList << it
				runOnDeath(it) { event -> checkForMosquitoUpdate(event); mosquitoList.remove(event.actor) }
			}
		}
	}
}

def checkForMosquitoUpdate(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(278, 2)) {
			if(it.getConLevel() < 3.0) {
				if(it.getPlayerVar("Z03_MosquitoKillTotal") == null) { it.setPlayerVar("Z03_MosquitoKillTotal", 0) }
				it.addToPlayerVar("Z03_MosquitoKillTotal", 1)
				it.centerPrint("Alarmskeeter ${it.getPlayerVar("Z03_MosquitoKillTotal").toInteger()}/20")
				if(it.getPlayerVar("Z03_MosquitoKillTotal") == 20) { it.updateQuest(278, "Klaus Klokenmeyer"); it.deletePlayerVar("Z03_MosquitoKillTotal") }
			} else {
				it.centerPrint( "You must be level 2.9 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

bf1MosquitoDeathScheduler()

//-----------------------------------------------------------------------------------
//Death listener for Fluffs                                                       
//-----------------------------------------------------------------------------------
def bf1FluffDeathScheduler() {
	myManager.schedule(1) { bf1FluffDeathScheduler() }
	myRooms.values().each() {
		//println "#### Eaching through bf1RoomList ####"
		it.getActorList().each() {
			if(isMonster(it) && it.getMonsterType() == "fluff" && !fluffList.contains(it)) {
				//println "#### Scheduling runOnDeath event for ${it} ####"
				fluffList << it
				runOnDeath(it) { event -> checkForFluffUpdate(event); fluffList.remove(event.actor) }
			}
		}
	}
}

def checkForFluffUpdate(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(279, 2)) {
			if(it.getConLevel() < 3.0) {
				if(it.getPlayerVar("Z03_FluffKillTotal") == null) { it.setPlayerVar("Z03_FluffKillTotal", 0) }
				it.addToPlayerVar("Z03_FluffKillTotal", 1)
				it.centerPrint("Air Fluff ${it.getPlayerVar("Z03_FluffKillTotal").toInteger()}/20")
				if(it.getPlayerVar("Z03_FluffKillTotal") == 20) { it.updateQuest(279, "Larry-VQS"); it.deletePlayerVar("Z03_FluffKillTotal") }
			} else {
				it.centerPrint( "You must be level 2.9 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

bf1FluffDeathScheduler()

//-----------------------------------------------------------------------------------
//Death listener for Garlic                                                       
//-----------------------------------------------------------------------------------
def bf1GarlicDeathScheduler() {
	myManager.schedule(1) { bf1GarlicDeathScheduler() }
	myRooms.values().each() {
		//println "#### Eaching through bf1RoomList ####"
		it.getActorList().each() {
			if(isMonster(it) && it.getMonsterType() == "garlic" && !garlicList.contains(it)) {
				//println "#### Scheduling runOnDeath event for ${it} ####"
				garlicList << it
				runOnDeath(it) { event -> checkForGarlicUpdate(event); garlicList.remove(event.actor) }
			}
		}
	}
}

def checkForGarlicUpdate(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(280, 2)) {
			if(it.getConLevel() < 3.0) {
				if(it.getPlayerVar("Z03_GarlicKillTotal") == null) { it.setPlayerVar("Z03_GarlicKillTotal", 0) }
				it.addToPlayerVar("Z03_GarlicKillTotal", 1)
				it.centerPrint("Garlic ${it.getPlayerVar("Z03_GarlicKillTotal").toInteger()}/20")
				if(it.getPlayerVar("Z03_GarlicKillTotal") == 20) { it.updateQuest(280, "Purvis-VQS"); it.deletePlayerVar("Z03_GarlicKillTotal") }
			} else {
				it.centerPrint( "You must be level 2.9 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

bf1GarlicDeathScheduler()