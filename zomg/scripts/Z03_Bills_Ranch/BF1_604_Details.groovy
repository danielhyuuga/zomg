import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------

def spawner1 = myRooms.BF1_604.spawnSpawner( "BF1_604_Spawner1", "mosquito", 1)
spawner1.setPos( 400, 250 )
spawner1.setInitialMoveForChildren( "BF1_604", 165, 130 )
spawner1.setHome( "BF1_604", 165, 130 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.setMonsterLevelForChildren( 2.0 )

def spawner2 = myRooms.BF1_604.spawnSpawner( "BF1_604_Spawner2", "mosquito", 1)
spawner2.setPos( 400, 240 )
spawner2.setInitialMoveForChildren( "BF1_604", 790, 150 )
spawner2.setHome( "BF1_604", 790, 150 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.setMonsterLevelForChildren( 2.0 )

def spawner3 = myRooms.BF1_604.spawnSpawner( "BF1_604_Spawner3", "mosquito", 1)
spawner3.setPos( 400, 230 )
spawner3.setInitialMoveForChildren( "BF1_604", 550, 190 )
spawner3.setHome( "BF1_604", 550, 190 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.setMonsterLevelForChildren( 2.0 )

//Alliances
spawner1.allyWithSpawner( spawner2 )
spawner1.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner3 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()