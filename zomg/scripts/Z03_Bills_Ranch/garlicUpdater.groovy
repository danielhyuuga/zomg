//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def garlicUpdater = "garlicUpdater"
myRooms.BF1_203.createTriggerZone(garlicUpdater, 40, 210, 690, 610)
myManager.onTriggerIn(myRooms.BF1_203, garlicUpdater) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(64, 3)) {
		runOnDeduct( event.actor, 100263, 15, { event.actor.centerPrint("You have planted the garlic."); event.actor.updateQuest(64, "Rancher Bill-VQS") }, { event.actor.centerPrint("You do not have enough garlic for planting.") } )
	}
}