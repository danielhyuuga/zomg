
//--------------------------------------------
// WARP CRYSTAL TO THE NULL CHAMBER           
//--------------------------------------------

toNullChamber = makeSwitch( "toNullChamber", myRooms.BF1_404, 360, 345 )
toNullChamber.unlock()
toNullChamber.setRange( 200 )
toNullChamber.setMouseoverText("To the Null Chamber")

onClickCrystal = new Object()

def goToNullChamber = { event ->
	synchronized( onClickCrystal ) {
		toNullChamber.off()
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "nullChamber1_1", 960, 680, 6 )
		event.actor.setQuestFlag( GLOBAL, "Z21RanchWarpOkay" )
		event.actor.setQuestFlag( GLOBAL, "Z21ComingFromOutside" )
	}
}

toNullChamber.whenOn( goToNullChamber )
