//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def windmillUpdater1 = "windmillUpdater1"
myRooms.BF1_402.createTriggerZone(windmillUpdater1, 500, 300, 800, 500)
myManager.onTriggerIn(myRooms.BF1_402, windmillUpdater1) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(69, 2)) {
		event.actor.centerPrint("You have repaired the first windmill.")
		event.actor.removeMiniMapQuestLocation( "First Windmill" )
		event.actor.updateQuest(69, "Rancher Bill-VQS")
		event.actor.addMiniMapQuestLocation( "Tractor", "BF1_404", 40, 320, "Tractor" )
	}
}

def tractorUpdater1 = "tractorUpdater1"
myRooms.BF1_404.createTriggerZone(tractorUpdater1, 40, 320, 465, 650)
myManager.onTriggerIn(myRooms.BF1_404, tractorUpdater1) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(69, 3)) {
		event.actor.centerPrint("You have repaired the tractor")
		event.actor.removeMiniMapQuestLocation( "Tractor" )
		event.actor.updateQuest(69, "Rancher Bill-VQS")
		event.actor.addMiniMapQuestLocation( "Second Windmill", "BF1_204", 10, 275, "Second Windmill" )
	}
}

def windmillUpdater2 = "windmillUpdater2"
myRooms.BF1_204.createTriggerZone(windmillUpdater2, 10, 275, 185, 600)
myManager.onTriggerIn(myRooms.BF1_204, windmillUpdater2) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(69, 4)) {
		event.actor.centerPrint("You have repaired the second windmill.")
		event.actor.removeMiniMapQuestLocation( "Second Windmill" )
		event.actor.updateQuest(69, "Rancher Bill-VQS")
		event.actor.addMiniMapQuestActorName("Rancher Bill-VQS")
	}
}