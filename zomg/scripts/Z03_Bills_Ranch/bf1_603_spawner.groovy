//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BF1_603.spawnSpawner( "bf1_603_spawner1", "mosquito", 1)
spawner1.setPos( 985, 350 )
spawner1.setInitialMoveForChildren( "BF1_603", 820, 430 )
spawner1.setHome( "BF1_603", 820, 430 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.2 )

def spawner2 = myRooms.BF1_603.spawnSpawner( "bf1_603_spawner2", "mosquito", 1)
spawner2.setPos( 985, 340 )
spawner2.setInitialMoveForChildren( "BF1_603", 840, 140 )
spawner2.setHome( "BF1_603", 840, 140 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.2 )

//Alliances
spawner1.allyWithSpawner( spawner2 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()