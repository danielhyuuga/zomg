import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------

def spawner1 = myRooms.BF1_803.spawnSpawner( "BF1_803_Spawner", "mosquito", 1)
spawner1.setPos( 535, 470 )
spawner1.setHome( "BF1_803", 535, 470 )
spawner1.setSpawnWhenPlayersAreInRoom( true )
spawner1.setWaitTime( 50 , 70 )
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner1.childrenWander( true )
spawner1.setMonsterLevelForChildren( 2.6 )

def spawner2 = myRooms.BF1_803.spawnSpawner( "BF1_803_Spawner2", "mosquito", 1)
spawner2.setPos( 730, 270 )
spawner2.setHome( "BF1_803", 730, 270 )
spawner2.setSpawnWhenPlayersAreInRoom( true )
spawner2.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner2.childrenWander( true )
spawner2.setMonsterLevelForChildren( 2.6 )

def spawner3 = myRooms.BF1_803.spawnSpawner( "BF1_803_Spawner3", "mosquito", 1)
spawner3.setPos( 825, 400 )
spawner3.setHome( "BF1_803", 825, 400 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.6 )

//Alliances
spawner1.allyWithSpawner( spawner2 )
spawner1.allyWithSpawner( spawner3 )
spawner2.allyWithSpawner( spawner3 )

//Spawning
spawner1.forceSpawnNow()
spawner2.forceSpawnNow()
spawner3.forceSpawnNow()