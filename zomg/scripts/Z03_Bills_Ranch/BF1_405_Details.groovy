import com.gaiaonline.mmo.battle.script.*;

//----------------------------------------------------
// SPAWNERS AND PATROL PATHS                          
//----------------------------------------------------
def spawner3 = myRooms.BF1_405.spawnSpawner( "bf1_405_spawner3", "fluff", 1)
spawner3.setPos( 380, 70 )
spawner3.setHome( "BF1_405", 380, 70 )
spawner3.setSpawnWhenPlayersAreInRoom( true )
spawner3.setWaitTime( 50 , 70 )
spawner3.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner3.childrenWander( true )
spawner3.setMonsterLevelForChildren( 2.0 )

def spawner4 = myRooms.BF1_405.spawnSpawner( "bf1_405_spawner4", "fluff", 1)
spawner4.setPos( 490, 525 )
spawner4.setHome( "BF1_405", 490, 525 )
spawner4.setSpawnWhenPlayersAreInRoom( true )
spawner4.setWaitTime( 50 , 70 )
spawner4.setWanderBehaviorForChildren( 50, 150, 2, 8, 350 )
spawner4.childrenWander( true )
spawner4.setMonsterLevelForChildren( 2.0 )

//Alliances
spawner3.allyWithSpawner( spawner4 )

//Spawning
spawner3.forceSpawnNow()
spawner4.forceSpawnNow()