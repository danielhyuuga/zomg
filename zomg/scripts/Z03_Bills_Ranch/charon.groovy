import com.gaiaonline.mmo.battle.script.*;

// timer start - lanzer
def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
} else {
	return
}


def Charon = spawnNPC("[NPC] Charon", myRooms.BF1_403, 728, 422)
Charon.setRotation( 45 )
Charon.setDisplayName( "[NPC] Charon" )

def charonDefault = Charon.createConversation("charonDefault", true)

def charonDefault1 = [id:1]
charonDefault1.npctext = "Well, well, well.  Your name I remember reading from my book for today."
charonDefault1.options = []
charonDefault1.options << [text:"This is just a mule!", result: 2]
charonDefault1.options << [text:"Alright, I've made my peace.  I'm ready.", result: 3]
charonDefault1.options << [text:"I'm not falling for this again.", result: 15]
charonDefault1.options << [text:"** run for your life **", result: 99]
charonDefault.addDialog(charonDefault1, Charon)

def charonDefault2 = [id:2]
charonDefault2.npctext = "A tricky one here, aren't we?  Don't worry, all the doom and gloom talk is my boss's deal, along with relying on his book to dictate his day to day.  Gary Grimoire and his precious book.  He hates it when I use his real name."
charonDefault2.result = 4
charonDefault.addDialog(charonDefault2, Charon)

def charonDefault3 = [id:3]
charonDefault3.npctext = "BWA HA HA HA HA!  You mortals are so gullible.  Cheer up!  All the doom and gloom talk is my boss's deal, along with relying on his book to dictate his day to day.  Gary Grimoire and his precious book.  He hates it when I use his real name."
charonDefault3.result = 4
charonDefault.addDialog(charonDefault3, Charon)

def charonDefault4 = [id:4]
charonDefault4.npctext = "After he got promoted and donned the Reaper moniker and scythe, he shortened his last name to sound more menacing.  A similar title structure worked pretty well for the Vader's mystique too, didn't it? "
charonDefault4.result = 5
charonDefault.addDialog(charonDefault4, Charon)

def charonDefault5 = [id:5]
charonDefault5.npctext = "Gary's full last name was instead adopted as the definition of a spell book, since everyone thought someone with powers like him had to be using a magic tome.  If only they knew. "
charonDefault5.result = 6
charonDefault.addDialog(charonDefault5, Charon)

def charonDefault6 = [id:6]
charonDefault6.npctext = "Anyway, don't be fooled by the act though, he's just a bookworm and a loner.  A perfectionist bookworm that can't stand to have any errors in the pages."
charonDefault6.result = 7
charonDefault.addDialog(charonDefault6, Charon)

def charonDefault7 = [id:7]
charonDefault7.npctext = "That's where I come in.  Trying to fix any tiny discrepancies in the entries that he doesn't have time for.  So I get stuck with oddball cases like my current client."
charonDefault7.result = 8
charonDefault.addDialog(charonDefault7, Charon)

def charonDefault8 = [id:8]
charonDefault8.npctext = '"They" are dripping with animosity towards some character named Jack.  My already dead client is still rolling in is grave, completely beside himself over this creature named Jack.'
charonDefault8.result = 9
charonDefault.addDialog(charonDefault8, Charon)

def charonDefault9 = [id:9]
charonDefault9.npctext = "If he could just let it go, my client could easily be hanging out upstairs on fluffy clouds without a care in the afterlife.  But he can hold a grudge, stuck at this fork in the road, which just puts me in a jam."
charonDefault9.result = 10
charonDefault.addDialog(charonDefault9, Charon)

def charonDefault10 = [id:10]
charonDefault10.npctext = "I'm not allowed to get directly involved in such matters, only to manipulate... err, convince others to help out.  He'd really appreciate it I'm sure, as would I, so I could get back to the office."
charonDefault10.result = 11
charonDefault.addDialog(charonDefault10, Charon)

def charonDefault11 = [id:11]
charonDefault11.npctext = "Perks of this job, among other things, is the ability to transport anyone anywhere though.  Like you to the location of my client."
charonDefault11.options = []
charonDefault11.options << [text:"I'd rather not right now.", result: 99]
charonDefault11.options << [text:"Sure, I'll help out.", result: 12]
charonDefault.addDialog(charonDefault11, Charon)

def charonDefault12 = [id:12]
charonDefault12.npctext = "He's hanging out in a place called 'The Jackyard' where it's perpetually Halloween, so keep your wits about you.  From what I've heard, this Jack guy is one tough cookie and could be quite a handful if you pick a fight."
charonDefault12.result = 13
charonDefault.addDialog(charonDefault12, Charon)

def charonDefault13 = [id:13]
charonDefault13.npctext = "But what is it that your kind always likes to say...  YOLO?"
charonDefault13.playertext = 'Why do you refer to your client as both "he" and "they"?'
charonDefault13.result = 14
charonDefault.addDialog(charonDefault13, Charon)

def charonDefault14 = [id:14]
charonDefault14.npctext = "You'll see why after meeting my client.  So, will you venture to The Jackyard?"
charonDefault14.options = []
charonDefault14.options << [text:"Thanks, but I'll just stick with the potentially lethal threats around here.", result: 99]
charonDefault14.options << [text:"Alright, YOLO it is.", result: 15]
charonDefault.addDialog(charonDefault14, Charon)

def charonDefault15 = [id:15]
charonDefault15.npctext = "Okay, let me send your spirit to The Jackyard then.  When you're finished, look for my ... short stacked client for a ride back.  He'll dial me up, spiritual connection with the dead and all that stuff."
charonDefault15.options = []
charonDefault15.options << [text:"Wait! I've changed my mind.", result: 99]
charonDefault15.options << [text:"Here goes everything.", result: 16]
charonDefault.addDialog(charonDefault15, Charon)

def charonDefault16 = [id:16]
charonDefault16.npctext = "Have a good fight, I would tell you to stay alive but that's not my job."
charonDefault16.exec = { event ->
        event.actor.warp( "Bar_201", 400, 510 )
}
charonDefault16.result = DONE
charonDefault.addDialog(charonDefault16, Charon)

def charonDefault17 = [id:99]
charonDefault17.npctext = "I'll be expecting you, whether you like it or not."
charonDefault17.result = DONE
charonDefault.addDialog(charonDefault17, Charon)
