import com.gaiaonline.mmo.battle.script.*;

//NOTE: trying to force them to new VillageTraining; obsolete all here below.

tv = makeSwitch( "tv", myRooms.TrainStation_1, 100, 400 )
tv.lock()
tv.setUsable( false )
tv.off()

//=======================================
// TRAIN STATION LIGHT SWITCHES          
//=======================================
redLight = makeSwitch( "redLight", myRooms.TrainStation_1, 0, 0 )
whiteLight = makeSwitch( "whiteLight", myRooms.TrainStation_1, 0, 0 )

redLight.setUsable( false )
whiteLight.setUsable( false )

redLight.off()
whiteLight.on()

beacon = makeSwitch( "beacon_01", myRooms.TrainStation_1, 0, 0 )
beacon.off()
beacon.setUsable( false )

//=======================================
// GREEN ARROW FOR DOOR TO SEWERS        
//=======================================
toSewers = makeSwitch( "toSewers", myRooms.TrainStation_1, 215, 850 )
toSewers.lock()
toSewers.off()
toSewers.setUsable( false )

//=======================================
// ENTRY CODE AND AMBUSH LOGIC           
//=======================================
myManager.onEnter( myRooms.TrainStation_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.warp( "VillageTraining_2", 500, 500 )
		/* disabling because we're rerouting them to new VillageTraining.
		//event.actor.unsetQuestFlag( GLOBAL, "Z22HasLeftTrainStationOnceBefore" ) //FOR DEBUGGING ONLY !!!!!!
		//event.actor.unsetQuestFlag( GLOBAL, "Z22MoviePlayingNow" ) //FOR DEBUGGING ONLY !!!!!!
		if( event.actor.hasQuestFlag( GLOBAL, "Z22HasLeftTrainStationOnceBefore" ) || event.actor.hasQuestFlag( GLOBAL, "Z01ComingFromBartonTown" ) ) {
			if( event.actor.hasQuestFlag( GLOBAL, "Z25PlayerBelongsInSewersTraining" ) ) {
				toSewers.on()
				event.actor.centerPrint( "Head back to Dani by clicking on the door to the Waterworks Tunnels again." )
			} else {
				if( event.actor.hasQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" ) ) {
					tutorialNPC.pushDialog( event.actor, "soYoureDazed" )
				} else {
					event.actor.centerPrint( "When you're ready, click on the door to the Waterworks and try again." )
					toSewers.on()
				}
			}
			myManager.stopListenForHealth( event.actor )
			event.actor.unsetQuestFlag( GLOBAL, "Z14HasLeftWaterworks" )
		} else {

			Barry = spawnNPC("Barry-VQS", myRooms.TrainStation_1, 365, 870)
			Barry.setRotation( 45 )
			Barry.setDisplayName( "Barry" )
			event.actor.unsetQuestFlag( GLOBAL, "Z22MoveDialogAuthorized" )
			event.actor.unsetQuestFlag( GLOBAL, "Z22BarryHello" )
			event.actor.unsetQuestFlag( GLOBAL, "Z22AfterMovie" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25DaniHello" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14PlayerDazed" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14HasLeftWaterworks" )
			event.actor.unsetQuestFlag( GLOBAL, "Z22FirstTimeDazedDialog" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14RingRingGiven" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ChestOpeningTutorial" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14BossWarningGiven" )
			event.actor.unsetQuestFlag( GLOBAL, "ZXXOnHealthStarted" )
			event.actor.unsetQuestFlag( GLOBAL, "ZXXOnDeathStarted" )
			event.actor.unsetQuestFlag( GLOBAL, "Z02PlayerDazed" )
			event.actor.unsetQuestFlag( GLOBAL, "Z02LeonRunOnDeathSet" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ComingFromDani" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ReceivedAttackRing" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ReceivedDefendRing" )
			//event.actor.unsetQuestFlag( GLOBAL, "Z22MoviePlayingNow" )
			activateBarryConvos()
			//if the player was watching the movie and logged out before it finished, then take care of getting them started with conversations again.
			if( event.actor.hasQuestFlag( GLOBAL, "Z22MoviePlayingNow" ) && !event.actor.hasQuestFlag( GLOBAL, "Z22AfterMovie" ) ) {
				tv.on()
				myManager.schedule(1) {
					playMovie( event.actor, "splashscreen/news1.flv", 100, 180, 360, 240) {
						Barry.setRotation( 45 )
						myManager.schedule(1) {
							tv.off()
							event.actor.setQuestFlag( GLOBAL, "Z22AfterMovie" )
							Barry.pushDialog( event.actor, "afterMovie" )
							freePlayer( event.actor )
						}
					}
				}
			} else {
				event.actor.centerPrint( "You arrive on the train to Barton Station!" )
				myManager.schedule(3) { 
					event.actor.setQuestFlag( GLOBAL, "Z22MoveDialogAuthorized" )
					event.actor.setQuestFlag( GLOBAL, "Z22BarryHello" )
					Barry.pushDialog( event.actor, "movementDialog" )
				}
			}
		}
		*/	
	}
}

//Dazed Tutorial that appears after dying in the Waterworks
soYoureDazed = tutorialNPC.createConversation( "soYoureDazed", true )

def dazed1 = [id:1]
dazed1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/youWereDazed.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
dazed1.exec = { event ->
	event.actor.centerPrint( "When you're ready, click on the door to the Waterworks and try again." )
	toSewers.on()
}
dazed1.result = DONE
soYoureDazed.addDialog( dazed1, tutorialNPC )

def activateBarryConvos() {
	//Show the movement dialog
	movementDialog = Barry.createConversation( "movementDialog", true, "Z22MoveDialogAuthorized" )

	def move1 = [id:1]
	move1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/movement.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
	move1.flag = "!Z22MoveDialogAuthorized" 
	move1.exec = { event ->
		event.player.centerPrint( "Click near Barry to move close enough to talk to him." )
		beacon.on()

	}
	move1.result = DONE
	movementDialog.addDialog( move1, tutorialNPC )


	barryHello = Barry.createConversation( "barryHello", true, "Z22BarryHello" )

	//greeting
	def hi1 = [id:1]
	hi1.npctext = "Hi there! Sorry that everything's closed down right now."
	hi1.playertext = "What's going on?"
	hi1.exec = { event ->
		beacon.off()
		controlPlayer( event.player )
	}
	hi1.result = 2
	barryHello.addDialog( hi1, Barry )

	def hi2 = [id:2]
	hi2.npctext = "The trains are stopped until the current emergency is over."
	hi2.playertext = "Emergency?"
	hi2.result = 3
	barryHello.addDialog( hi2, Barry )

	def hi3 = [id:3]
	hi3.npctext = "You're kidding me, right? Here...I'll turn on the TV on so you can take a look."
	hi3.exec = { event ->
		event.player.setQuestFlag( GLOBAL, "Z22MoviePlayingNow" )
		Barry.setRotation( 135 )
		tv.on()
		myManager.schedule(1) {
			playMovie( event.player, "splashscreen/news1.flv", 100, 180, 360, 240) {
				Barry.setRotation( 45 )
				myManager.schedule(1) {
					tv.off()
					event.player.setQuestFlag( GLOBAL, "Z22AfterMovie" )
					event.player.unsetQuestFlag( GLOBAL, "Z22BarryHello" )
					Barry.pushDialog( event.player, "afterMovie" )
					freePlayer( event.player )
				}
			}
		}
	}
	hi3.result = DONE
	barryHello.addDialog( hi3, Barry )

	//Conversation plays after the movie is completed
	afterMovie = Barry.createConversation( "afterMovie", true, "Z22AfterMovie", "!Z25DaniHello" )

	def after1 = [id:1]
	after1.npctext = "See what I mean?!?"
	after1.playertext = "Wow. Okay. So I should just head up the escalator to go to town?"
	after1.result = 2
	afterMovie.addDialog( after1, Barry )

	//barry introduces himself and we indicate the escalators are blocked
	def after2 = [id:2]
	after2.npctext = "Nope. The escalators are barricaded at the top. You have to go through the tunnels toward the Waterworks instead."
	after2.playertext = "Really?"
	after2.result = 3
	afterMovie.addDialog (after2, Barry )

	def after3 = [id:3]
	after3.npctext = "Don't worry. Just head through the door behind me and find the guard named Dani. She'll guide you out."
	after3.flag = "Z25DaniHello"
	after3.exec = { event ->
		event.player.centerPrint( "Walk near the door and click it to enter the Waterworks Tunnels." )
		event.player.addMiniMapQuestActorName( "BFG-Dani" )
		toSewers.on()
	}
	after3.result = DONE
	afterMovie.addDialog( after3, Barry )
	
}


//=====================================
// EXIT TRAIN STATION SWITCH           
//=====================================
onClickStationExit = new Object()

stationExit = makeSwitch( "SewersTraining_In", myRooms.TrainStation_1, 180, 770 )
stationExit.unlock()
stationExit.off()
stationExit.setRange( 300 )
stationExit.setMouseoverText("Waterworks Tunnels")

def exitStation = { event ->
	stationExit.off()
	stationExit.lock()
	myManager.schedule(2) { stationExit.unlock() }
	if( isPlayer( event.actor ) ) {
		if( event.actor.hasQuestFlag( GLOBAL, "Z25DaniHello" ) || event.actor.hasQuestFlag( GLOBAL, "Z25PlayerBelongsInSewersTraining" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z22HasLeftTrainStationOnceBefore" )
			event.actor.warp( "SewersTraining_1", 820, 260 )
			event.actor.centerPrint( "You move through the door and into the Waterworks Tunnels." )
			
		} else if( event.actor.hasQuestFlag( GLOBAL, "Z22HasLeftTrainStationOnceBefore" ) || event.actor.hasQuestFlag( GLOBAL, "Z01ComingFromBartonTown" ) ) {
			event.actor.centerPrint( "You quickly travel back to the Waterworks." )
			event.actor.setQuestFlag( GLOBAL, "Z22ComingFromTrainStation" )
			event.actor.warp( "Sewers_5", 350, 510 )
			event.actor.unsetQuestFlag( GLOBAL, "Z14PlayerDazed" )
			
		} else {
			event.actor.centerPrint( "You need to speak to Barry before you can leave the Train Station." )
		}
	}
}

stationExit.whenOn( exitStation )
