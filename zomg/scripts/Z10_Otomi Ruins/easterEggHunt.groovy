import com.gaiaonline.mmo.battle.script.*;
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
startDay = 15

//------------------------------------------
//EASTER EGG HUNT (VILLAGE GREENS)          
//------------------------------------------

//SPAWNERS

egg1 = myRooms.OtRuins_805.spawnSpawner( "egg1", "egg1", 100 )
egg1.setPos( 850, 340 )
egg1.setMonsterLevelForChildren( 1.0 )
egg1.setHomeTetherForChildren( 6000 )
egg1.setEdgeHinting( false )
egg1.setTargetCycle( false )
egg1.stopSpawning()

egg2 = myRooms.OtRuins_805.spawnSpawner( "egg2", "egg2", 100 )
egg2.setPos( 850, 340 )
egg2.setMonsterLevelForChildren( 1.0 )
egg2.setHomeTetherForChildren( 6000 )
egg2.setEdgeHinting( false )
egg2.setTargetCycle( false )
egg2.stopSpawning()

egg3 = myRooms.OtRuins_805.spawnSpawner( "egg3", "egg3", 100 )
egg3.setPos( 850, 340 )
egg3.setMonsterLevelForChildren( 1.0 )
egg3.setHomeTetherForChildren( 6000 )
egg3.setEdgeHinting( false )
egg3.setTargetCycle( false )
egg3.stopSpawning()

egg4 = myRooms.OtRuins_805.spawnSpawner( "egg4", "egg4", 100 )
egg4.setPos( 850, 340 )
egg4.setMonsterLevelForChildren( 1.0 )
egg4.setHomeTetherForChildren( 6000 )
egg4.setEdgeHinting( false )
egg4.setTargetCycle( false )
egg4.stopSpawning()

egg5 = myRooms.OtRuins_805.spawnSpawner( "egg5", "egg5", 100 )
egg5.setPos( 850, 340 )
egg5.setMonsterLevelForChildren( 1.0 )
egg5.setHomeTetherForChildren( 6000 )
egg5.setEdgeHinting( false )
egg5.setTargetCycle( false )
egg5.stopSpawning()

egg6 = myRooms.OtRuins_805.spawnSpawner( "egg6", "egg6", 100 )
egg6.setPos( 850, 340 )
egg6.setMonsterLevelForChildren( 1.0 )
egg6.setHomeTetherForChildren( 6000 )
egg6.setEdgeHinting( false )
egg6.setTargetCycle( false )
egg6.stopSpawning()

goldenEgg = myRooms.OtRuins_805.spawnSpawner("goldeneggsolid", "goldeneggsolid", 100)
goldenEgg.setPos(850, 340)
goldenEgg.setHomeTetherForChildren(6000)
goldenEgg.setEdgeHinting(false)
goldenEgg.stopSpawning()

// The General!
hqGeneral = myRooms.OtRuins_805.spawnStoppedSpawner( "hqGeneral", "lawn_gnome_angry", 100 )
hqGeneral.setPos( 850, 340 )
hqGeneral.setHomeTetherForChildren( 6000 )
hqGeneral.setHateRadiusForChildren( 100 )
hqGeneral.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
hqGeneral.setMonsterLevelForChildren( 9 )
hqGeneral.stopSpawning()

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

eggSpawnOkay = false

def countEggs() {
	eggCount = 0
	myRooms.values().each() {
		eggCount = eggCount + it.getSpawnTypeCount("egg1") + it.getSpawnTypeCount("egg2") + it.getSpawnTypeCount("egg3") + it.getSpawnTypeCount("egg4") + it.getSpawnTypeCount("egg5") + it.getSpawnTypeCount("egg6") + it.getSpawnTypeCount("goldenegg")
	}
	
	//println "**** eggCount = ${eggCount} ****"
}

numEggPositionsPerRoom = 4

eggSpawnerList = [ egg1, egg2, egg3, egg4, egg5, egg6 ]

otami5Map = [ 1:[220, 170, 0], 2:[40, 600, 0], 3:[750, 240, 0], 4:[910, 360, 0], 5:"OtRuins_5", 6:myRooms.OtRuins_5 ]
otami6Map = [ 1:[130, 30, 0], 2:[900, 205, 0], 3:[780, 620, 0], 4:[490, 100, 0], 5:"OtRuins_6", 6:myRooms.OtRuins_6 ]
otami105Map = [ 1:[40, 60, 0], 2:[540, 390, 0], 3:[840, 420, 0], 4:[25, 570, 0], 5:"OtRuins_105", 6:myRooms.OtRuins_105 ]
otami106Map = [ 1:[150, 260, 0], 2:[370, 430, 0], 3:[420, 60, 0], 4:[960, 560, 0], 5:"OtRuins_106", 6:myRooms.OtRuins_106 ]
otami107Map = [ 1:[45, 315, 0], 2:[470, 380, 0], 3:[460, 640, 0], 4:[320, 60, 0], 5:"OtRuins_107", 6:myRooms.OtRuins_107 ]
otami205Map = [ 1:[50, 470, 0], 2:[230, 30, 0], 3:[540, 290, 0], 4:[950, 400, 0], 5:"OtRuins_205", 6:myRooms.OtRuins_205 ]
otami206Map = [ 1:[230, 50, 0], 2:[350, 220, 0], 3:[680, 270, 0], 4:[170, 530, 0], 5:"OtRuins_206", 6:myRooms.OtRuins_206 ]
otami207Map = [ 1:[30, 60, 0], 2:[590, 90, 0], 3:[560, 630, 0], 4:[760, 160, 0], 5:"OtRuins_207", 6:myRooms.OtRuins_207 ]
otami305Map = [ 1:[40, 475, 0], 2:[420, 650, 0], 3:[890, 650, 0], 4:[980, 200, 0], 5:"OtRuins_305", 6:myRooms.OtRuins_305 ]
otami306Map = [ 1:[110, 270, 0], 2:[300, 610, 0], 3:[960, 210, 0], 4:[660, 640, 0], 5:"OtRuins_306", 6:myRooms.OtRuins_306 ]
otami307Map = [ 1:[30, 320, 0], 2:[520, 650, 0], 3:[740, 420, 0], 4:[840, 70, 0], 5:"OtRuins_307", 6:myRooms.OtRuins_307 ]
otami402Map = [ 1:[520, 120, 0], 2:[250, 570, 0], 3:[700, 140, 0], 4:[960, 500, 0], 5:"OtRuins_402", 6:myRooms.OtRuins_402 ]
otami404Map = [ 1:[370, 360, 0], 2:[800, 380, 0], 3:[850, 40, 0], 4:[720, 590, 0], 5:"OtRuins_404", 6:myRooms.OtRuins_404 ]
otami405Map = [ 1:[430, 420, 0], 2:[120, 340, 0], 3:[970, 80, 0], 4:[1010, 470, 0], 5:"OtRuins_405", 6:myRooms.OtRuins_405 ]
otami406Map = [ 1:[550, 230, 0], 2:[270, 260, 0], 3:[960, 220, 0], 4:[220, 650, 0], 5:"OtRuins_406", 6:myRooms.OtRuins_406 ]
otami407Map = [ 1:[170, 160, 0], 2:[150, 620, 0], 3:[550, 650, 0], 4:[490, 40, 0], 5:"OtRuins_407", 6:myRooms.OtRuins_407 ]
otami502Map = [ 1:[60, 620, 0], 2:[420, 150, 0], 3:[670, 40, 0], 4:[990, 240, 0], 5:"OtRuins_502", 6:myRooms.OtRuins_502 ]
otami503Map = [ 1:[60, 130, 0], 2:[650, 140, 0], 3:[650, 410, 0], 4:[940, 230, 0], 5:"OtRuins_503", 6:myRooms.OtRuins_503 ]
otami504Map = [ 1:[140, 370, 0], 2:[450, 530, 0], 3:[570, 50, 0], 4:[1020, 260, 0], 5:"OtRuins_504", 6:myRooms.OtRuins_504 ]
otami505Map = [ 1:[150, 230, 0], 2:[720, 530, 0], 3:[970, 40, 0], 4:[310, 530, 0], 5:"OtRuins_505", 6:myRooms.OtRuins_505 ]
otami506Map = [ 1:[190, 90, 0], 2:[160, 490, 0], 3:[480, 560, 0], 4:[570, 160, 0], 5:"OtRuins_506", 6:myRooms.OtRuins_506 ]
otami602Map = [ 1:[440, 440, 0], 2:[840, 610, 0], 3:[990, 170, 0], 4:[560, 130, 0], 5:"OtRuins_602", 6:myRooms.OtRuins_602 ]
otami603Map = [ 1:[210, 430, 0], 2:[590, 390, 0], 3:[690, 240, 0], 4:[840, 510, 0], 5:"OtRuins_603", 6:myRooms.OtRuins_603 ]
otami604Map = [ 1:[150, 540, 0], 2:[490, 390, 0], 3:[860, 190, 0], 4:[940, 540, 0], 5:"OtRuins_604", 6:myRooms.OtRuins_604 ]
otami605Map = [ 1:[140, 520, 0], 2:[330, 250, 0], 3:[600, 100, 0], 4:[1020, 530, 0], 5:"OtRuins_605", 6:myRooms.OtRuins_605 ]
otami606Map = [ 1:[250, 340, 0], 2:[820, 170, 0], 3:[930, 450, 0], 4:[530, 550, 0], 5:"OtRuins_606", 6:myRooms.OtRuins_606 ]
otami702Map = [ 1:[360, 120, 0], 2:[520, 400, 0], 3:[860, 600, 0], 4:[860, 210, 0], 5:"OtRuins_702", 6:myRooms.OtRuins_702 ]
otami703Map = [ 1:[20, 190, 0], 2:[480, 610, 0], 3:[860, 140, 0], 4:[520, 170, 0], 5:"OtRuins_703", 6:myRooms.OtRuins_703 ]
otami704Map = [ 1:[240, 220, 0], 2:[210, 450, 0], 3:[830, 630, 0], 4:[650, 80, 0], 5:"OtRuins_704", 6:myRooms.OtRuins_704 ]
otami705Map = [ 1:[130, 420, 0], 2:[360, 530, 0], 3:[760, 50, 0], 4:[710, 320, 0], 5:"OtRuins_705", 6:myRooms.OtRuins_705 ]
otami706Map = [ 1:[190, 210, 0], 2:[710, 215, 0], 3:[480, 100, 0], 4:[910, 640, 0], 5:"OtRuins_706", 6:myRooms.OtRuins_706 ]
otami801Map = [ 1:[80, 230, 0], 2:[300, 530, 0], 3:[770, 50, 0], 4:[950, 500, 0], 5:"OtRuins_801", 6:myRooms.OtRuins_801 ]
otami802Map = [ 1:[40, 60, 0], 2:[450, 370, 0], 3:[650, 530, 0], 4:[960, 280, 0], 5:"OtRuins_802", 6:myRooms.OtRuins_802 ]
otami803Map = [ 1:[30, 270, 0], 2:[350, 440, 0], 3:[790, 580, 0], 4:[860, 30, 0], 5:"OtRuins_803", 6:myRooms.OtRuins_803 ]
otami804Map = [ 1:[80, 390, 0], 2:[400, 470, 0], 3:[650, 300, 0], 4:[960, 100, 0], 5:"OtRuins_804", 6:myRooms.OtRuins_804 ]
otami805Map = [ 1:[320, 530, 0], 2:[660, 580, 0], 3:[980, 230, 0], 4:[90, 40, 0], 5:"OtRuins_805", 6:myRooms.OtRuins_805 ]
otami806Map = [ 1:[110, 530, 0], 2:[530, 230, 0], 3:[910, 80, 0], 4:[370, 200, 0], 5:"OtRuins_806", 6:myRooms.OtRuins_806 ]
otami902Map = [ 1:[100, 500, 0], 2:[410, 20, 0], 3:[1020, 490, 0], 4:[920, 170, 0], 5:"OtRuins_902", 6:myRooms.OtRuins_902 ]
otami903Map = [ 1:[280, 10, 0], 2:[270, 580, 0], 3:[650, 270, 0], 4:[1010, 130, 0], 5:"OtRuins_903", 6:myRooms.OtRuins_903 ]
otami904Map = [ 1:[20, 150, 0], 2:[670, 270, 0], 3:[970, 380, 0], 4:[710, 510, 0], 5:"OtRuins_904", 6:myRooms.OtRuins_904 ]
otami905Map = [ 1:[430, 650, 0], 2:[370, 130, 0], 3:[890, 620, 0], 4:[980, 145, 0], 5:"OtRuins_905", 6:myRooms.OtRuins_905 ]

roomList = [ otami5Map, otami6Map, otami105Map, otami106Map, otami107Map, otami205Map, otami206Map, otami207Map, otami305Map, otami306Map, otami307Map, otami402Map, otami404Map, otami405Map, otami406Map, otami407Map, otami502Map, otami503Map, otami504Map, otami505Map, otami506Map, otami602Map, otami603Map, otami604Map, otami605Map, otami606Map, otami702Map, otami703Map, otami704Map, otami705Map, otami706Map, otami801Map, otami802Map, otami803Map, otami804Map, otami805Map, otami806Map, otami902Map, otami903Map, otami904Map, otami905Map ]
chickFollowList = [] as Set

goldenEggSpawnedToday = true
goldenEggPresent = false
spawningGoldenEgg = false
lastSpawn = 0

//Server initialization script to figure out what day it is and to set the tree appropriately

def eggTimer() {
	//bracket each date and set the same stuff that the "scheduleAt" stuff, below, would do normally
    def end = startDay + 11
	if( isLaterThan( "4/$startDay/$year 11:59 pm" ) && eggSpawnOkay == false && !isLaterThan("4/$end/$year 11:59 pm") ) {
		eggSpawnOkay = true
		maxEggs = 20
		spawnEggs()
		spawnPeeps()
	}
	if( isLaterThan( "4/$end/$year 11:59 pm" ) ) {
		eggSpawnOkay = false
		peepSpawnedList.clone().each() {
			it.dispose()
			peepSpawnedList.remove(it)
		}
	}
	myManager.schedule(60) { eggTimer() }
}

def goldenEggTimer() {
	Date date = new Date();   // given date
	Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
	calendar.setTime(date);   // assigns calendar to given date
	hournow = (calendar.get(Calendar.MONTH) * 100) + calendar.get(Calendar.HOUR_OF_DAY)

	if (lastSpawn == 0) lastSpawn = hournow + random(0,2)

	if (goldenEggSpawnedToday == true && goldenEggPresent == false && lastSpawn <= hournow ) {
		lastSpawn = hournow + random(2,5)
		goldenEggSpawnedToday = false
	}
	myManager.schedule(60) { goldenEggTimer() }
}

def setMaxEggs() {
	maxEggs = areaPlayerCount()
	if( areaPlayerCount() < 40 ) {
		maxEggs = 40
	}
}

//Decrease the eggSpawnTimer if there are lots of players in the zone
def setEggSpawnTimer() {
	if( areaPlayerCount() > 10 ) {
		eggSpawnTimer = 10 - ( ( areaPlayerCount() - 10 ) / 5 )
	} else {
		eggSpawnTimer = 10
	}
}	

//randomly spawn eggs
def spawnEggs() {
	//println "**** STARTING SPAWNEGGS ROUTINE ****"
	//if the current number of eggs in the zone is less than "maxEggs", then spawn an egg
	if( eggSpawnOkay == false ) return
	countEggs()
	setMaxEggs()
	setEggSpawnTimer()
	if( eggCount < maxEggs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numEggPositionsPerRoom )
		positionInfo = room.get( position )
		//println "**** position = ${position} ****"
		//println "**** positionInfo = ${positionInfo} ****"
		//now look in position 2 of the "positionInfo" to see if the egg is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		//println "**** spawnHolder = ${ spawnHolder } ****"
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//println "**** modified positionInfo = ${positionInfo} ****"
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			//println "**** modified room map = ${room} ****"
			//determine which spawner to use
			eggRoll = random(1000)
			
			if(eggRoll > 999 && goldenEggSpawnedToday == false) {
				goldenEggSpawnedToday = true
				eggSpawner = goldenEgg
				spawningGoldenEgg = true
			} else {
				eggSpawner = random( eggSpawnerList )
			}
			//println "**** using eggSpawner = ${eggSpawner} ****"
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//eggSpawner.setHomeForChildren( roomName, X, Y )
			//now spawn the egg
			eggSpawner.warp( roomName.toString(), X, Y )
			eggSpawner.setHomeForChildren( "${roomName}", X, Y )
			egg = eggSpawner.forceSpawnNow()
			if(spawningGoldenEgg == true) {
				hqGeneral.warp( roomName.toString(), X, Y )
				hqGeneral.setHomeForChildren( "${roomName}", X, Y )
				guardian = hqGeneral.forceSpawnNow()
				guardian.setClientDisplayScale(1.3)
		        guardian.setDisplayName( "Easter General" )
				println "**** warp ${eggSpawner} to ${ roomName.toString() }, ${X}, ${Y} ****"
				egg.setClientDisplayScale(2)
                zoneBroadcast("Blaze-VQS", "Golden_Egg", random( ["A Golden Egg was found!","Another Golden Egg? Where??","The Golden Egg is real!","Don't miss your golden opportunity!"] ) )
                myManager.schedule(300)
                {
                    removeZoneBroadcast("Golden_Egg")
                }
			}
			//when the egg is destroyed, reset its record to 0 so the egg can spawn again later
			if(spawningGoldenEgg == true) {
				runOnDeath(egg) { event ->
					event.actor.getHated().each() {
						it.addPlayerVar("Z01EggCounter_$year", 1)
						if(!it.hasQuestFlag(GLOBAL, "Z01_GoldenEgg_Defeated_2_$year")) {
							it.setQuestFlag(GLOBAL, "Z01_GoldenEgg_Defeated_2_$year")
							it.grantItem("55433")
							it.centerPrint("You received the Regal Adornment Bundle!")
						} else {
							it.grantCoins(random(7500, 9999))
							it.centerPrint("You've already received your bundle, here's some gold instead.")
						}
					}
					deathRoomName = event.actor.getRoomName()
					deathX = event.actor.getX().intValue()
					deathY = event.actor.getY().intValue()
					//println "**** DEATH POINT = roomName = ${ roomName }; deathX = ${deathX}; deathY = ${deathY} ****"
					//search through all the maps to find the map with the correct room name in it
					roomList.each{
						if( it.get(5) == deathRoomName ) {
							deathRoom = it
							//println "**** deathRoom = ${deathRoom} ****"
						}
					}
					//once the correct map is found, use the death X to make a match for which position to read
					seed = 1
					goldenEggPresent = false
					deathPosition = 0
					findCorrectPosition()
					deathPositionInfo = deathRoom.get( deathPosition )
					//now that we have the correct position, change out the info properly so an egg can spawn there again
					deathPositionInfo.remove(2) //remove the placeholder at the end of the list
					deathPositionInfo.add(0)
					//println "****modified death position info list = ${deathPositionInfo} ****"
					//now update the deathRoom Map with the updated list
					deathRoom.remove( deathPosition)
					deathRoom.put( deathPosition, deathPositionInfo )
					//println "****modified deathRoom map = ${ deathRoom } ****"
				}
			} else {
				runOnDeath( egg, { event ->
					event.killer.addPlayerVar( "Z01EggCounter_$year", 1 )
					deathRoomName = event.actor.getRoomName()
					deathRoomObject = event.actor.getRoom()
					//println "^^^^ deathRoom = ${event.actor.getRoom()} ^^^^"
					deathX = event.actor.getX().intValue()
					deathY = event.actor.getY().intValue()
					//println "**** DEATH POINT = roomName = ${ roomName }; deathX = ${deathX}; deathY = ${deathY} ****"
					//search through all the maps to find the map with the correct room name in it
					roomList.each{
						if( it.get(5) == deathRoomName ) {
							deathRoom = it
							//println "**** deathRoom = ${deathRoom} ****"
						}
					}
					//once the correct map is found, use the death X to make a match for which position to read
					seed = 1
					deathPosition = 0
					findCorrectPosition()
					deathPositionInfo = deathRoom.get( deathPosition )
					//now that we have the correct position, change out the info properly so an egg can spawn there again
					deathPositionInfo.remove(2) //remove the placeholder at the end of the list
					deathPositionInfo.add(0)
					//println "****modified death position info list = ${deathPositionInfo} ****"
					//now update the deathRoom Map with the updated list
					deathRoom.remove( deathPosition)
					deathRoom.put( deathPosition, deathPositionInfo )
					//println "****modified deathRoom map = ${ deathRoom } ****"
					rewardPlayer( event )
					
					chickRoll = random(100)
					
					if(chickRoll > 99) {
						spawnChick(event)
					}
				} )
			}
		spawningGoldenEgg = false
		} else {
			//println "**** egg position is already occupied ****"
		}
		//now spawn another egg in 10 seconds
		myManager.schedule(eggSpawnTimer) { spawnEggs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule(eggSpawnTimer) { spawnEggs() } 
	}
}

def synchronized spawnChick(event) {
	chickTypeRoll = random(100)
	
	if(chickTypeRoll < 45) {
		chick = makeCritter("peep1", deathRoomObject, deathX, deathY)
		chick.setDisplayName(peepNameMap.get(chick.getURL()))
		chick.setUsable(true)
		chick.setRange(2000)
		chick.onUse() { event2 ->
			event2.critter.setUsable(false)
			event2.critter.dispose()
			event2.player.grantItem("52045") // Churpy
			event2.player.centerPrint("You grabbed Churpy!")
			if(!event2.player.hasQuestFlag(GLOBAL, "Z1_Chicks_Chick1_$year")) {
				event2.player.setQuestFlag(GLOBAL, "Z1_Chicks_Chick1_$year")
				event2.player.addPlayerVar("Z1_Chicks_ChickCounter_$year", 1)
			}
			if(event2.player.getPlayerVar("Z1_Chicks_ChickCounter_$year") == 3) {
				event2.player.updateQuest(343, "Story-VQS")
			}	
		}
	}
	if(chickTypeRoll >= 45 && chickTypeRoll < 75) {
		chick = makeCritter("peep2", deathRoomObject, deathX, deathY)
		chick.setDisplayName(peepNameMap.get(chick.getURL()))
		chick.setUsable(true)
		chick.setRange(2000)
		chick.onUse() { event2 ->
			event2.critter.setUsable(false)
			event2.critter.dispose()
			event2.player.grantItem("52047") // Cheepy
			event2.player.centerPrint("You grabbed Cheepy!")
			if(!event2.player.hasQuestFlag(GLOBAL, "Z1_Chicks_Chick2_$year")) {
				event2.player.setQuestFlag(GLOBAL, "Z1_Chicks_Chick2_$year")
				event2.player.addPlayerVar("Z1_Chicks_ChickCounter_$year", 1)
			}
			if(event2.player.getPlayerVar("Z1_Chicks_ChickCounter_$year") == 3) {
				event2.player.updateQuest(343, "Story-VQS")
			}	
		}
	}
	if(chickTypeRoll >= 75 && chickTypeRoll < 95) {
		chick = makeCritter("peep4", deathRoomObject, deathX, deathY)
		chick.setDisplayName(peepNameMap.get(chick.getURL()))
		chick.setUsable(true)
		chick.setRange(2000)
		chick.onUse() { event2 ->
			event2.critter.setUsable(false)
			event2.critter.dispose()
			event2.player.grantItem("52051") // Chicky item
			event2.player.centerPrint("You grabbed Chicky!")
			if(!event2.player.hasQuestFlag(GLOBAL, "Z1_Chicks_Chick4_$year")) {
				event2.player.setQuestFlag(GLOBAL, "Z1_Chicks_Chick4_$year")
				event2.player.addPlayerVar("Z1_Chicks_ChickCounter_$year", 1)
			}
			if(event2.player.getPlayerVar("Z1_Chicks_ChickCounter_$year") == 3) {
				event2.player.updateQuest(343, "Story-VQS")
			}	
		}
	}
	if(chickTypeRoll >= 95) {
		chick = makeCritter("peep3", deathRoomObject, deathX, deathY)
		chick.setDisplayName(peepNameMap.get(chick.getURL()))
		chick.setUsable(true)
		chick.setRange(2000)
		chick.onUse() { event2 ->
			event2.critter.setUsable(false)
			event2.critter.dispose()
			event2.player.grantItem("52049") // Chirpy item
			event2.player.centerPrint("You grabbed Chirpy!")
			if(!event2.player.hasQuestFlag(GLOBAL, "Z1_Chicks_Chick3_$year")) {
				event2.player.setQuestFlag(GLOBAL, "Z1_Chicks_Chick3_$year")
				event2.player.addPlayerVar("Z1_Chicks_ChickCounter_$year", 1)
			}
			if(event2.player.getPlayerVar("Z1_Chicks_ChickCounter_$year") == 3) {
				event2.player.updateQuest(343, "Story-VQS")
			}	
		}
	}
}
def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//println "****checking position....deathPositionInfo being checked = {$deathPositionInfo} ****"
		//if the first element in this list is the same place the egg "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
	//println "**** deathPosition = ${deathPosition} ****"
}

lootList = [ "1000527", "100435", "100434" ] //1000527 = Easter Fluff Ball; 100435 = Plastic Grass; 100434 = Jelly Bean
recipeList = [ "10368665", "36105", "36105", "36105", "36105", "36105", "36107", "36107", "36107", "70857", "70859", "70861" ] // 36105 = zOMG Cheepz; 36017 = zOMG Easter Basket; 70857 = Mint Bunny Fluff Plushie; 70859 = Baby Blue Bunny Fluff Plushie; 70861 = Peach Bunny Fluff Plushie
goldGrantMin = 50
goldGrantMax = 150

def rewardPlayer( event ) {
	roll = random( 100 )
    if(!event.killer.hasQuestFlag(GLOBAL, "Z1_Recipe_Granted_$year")) {
        event.killer.centerPrint( "You get a special Easter Recipe Pack!  Open it in your inventory!" )
        event.killer.grantItem( "10368665" ) // 2017 receipe bundle 
        event.killer.setQuestFlag(GLOBAL, "Z1_Recipe_Granted_$year")
	} else if( roll <= 65 ) {
		//gold only
		event.killer.centerPrint( "The egg has a golden center!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
	} else if ( roll > 65 && roll <= 95 ) {
		//gold plus item
		event.killer.centerPrint( "The egg holds a tiny present to use in recipes!" )
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
		event.killer.grantItem( random( lootList ) )
	} else if (roll > 95 ) {
		//recipe
		event.killer.grantCoins( random( goldGrantMin, goldGrantMax ) )
		event.killer.centerPrint( "You get a classic Easter Recipe!" )
		event.killer.grantItem( random( recipeList ) )
	
	}
	//now check for badges
	if( event.killer.getPlayerVar( "Z01EggCounter_$year" ) >= 100 && !event.killer.isDoneQuest( 312 ) ) {
		event.killer.updateQuest( 312, "Story-VQS" )
	}
	if( event.killer.getPlayerVar( "Z01EggCounter_$year" ) >= 1000 && !event.killer.isDoneQuest( 44 ) ) {
		event.killer.updateQuest( 44, "Story-VQS" )
	}
}
	
peepSayings = [ "Happy Easter!", "Chock Full o'Marshmallowy Goodness!", "Thanks for not eggnoring me!", "Somebunny touched me! Thanks, somebunny!", "Bunnies make people hoppy.", "Eggs are good. No yolk!", "I'm one cool chick, eh?", "Peep. I should say that, right? Peep?", "Jeepers, creepers. CALL ME MR. PEEPERS!", "Why are you touching me?", "Dont poke too hard. I dent.", "Drying...out. Air...making...me...stale...*gasp*", "Please don't eat me. I taste horrible. Really!", "Am I mostly sugar, or mostly preservatives? You decide!", "As long as the peeps are playing, the eggs are a'laying." ]
peepList = ["peep1", "peep2", "peep3", "peep4"]
peepSpawnedList = [] as Set
peepNameMap = ["peep1":"Churpy", "peep2":"Cheepy", "peep3":"Chirpy", "peep4":"Chicky"]

numberOfPeeps = 0
peepCounter = 0

//spawn Peeps to wander the area and say things if you click on them
def spawnPeeps() {
	if(peepCounter < 5) {
		peepRoom = random( roomList )
		peepPosition = random( numEggPositionsPerRoom )
		peepPositionInfo = room.get( peepPosition )
	
		peepSpawnHolder = peepPositionInfo.get(2)
	
		if(peepSpawnHolder == 0) {
			peepPositionInfo.remove(2)
			peepPositionInfo.add(1)
		
			peepRoom.remove(peepPosition)
			peepRoom.put(peepPosition, peepPositionInfo)
		
			peepRoomName = peepRoom.get(6)
			peepX = peepPositionInfo.get(0)
			peepY = peepPositionInfo.get(1)
			
			//println "^^^^ Spawning peeps in ${peepRoomName} ^^^^"
		
			numberOfPeeps = random(2, 5)
			createPeeps()
		} else { myManager.schedule(1) { spawnPeeps() } }
	}
}

def createPeeps() {
	if(numberOfPeeps > 0) {
		peepType = random(peepList)
		peep = makeCritter(peepType, peepRoomName, peepX, peepY)
		peep.setDisplayName(peepNameMap.get(peep.getURL()))
		peep.setUsable(true)
		peep.setRange(200)
		//println "^^^^ Made a peep ${peep} ^^^^"
		
		peepSpawnedList << peep
		
		peep.onUse() { event ->
			//the following section prevents the player from being able to spam-click the sayings. They can only get one once every three seconds.
			if( event.player.getPlayerVar( "Z01PeepTimer" ) == null || event.player.getPlayerVar( "Z01PeepTimer" ) == 0 ) {
				event.critter.say( random( peepSayings ) )
				event.player.setPlayerVar( "Z01PeepTimer", 1 )
				myManager.schedule(3) { event.player.setPlayerVar( "Z01PeepTimer", 0 ) }
			}
		}
		
		numberOfPeeps--
		createPeeps()
	} else {
		myManager.schedule(30) { spawnPeeps() }
		peepCounter++
	}
}

//start it all up
eggTimer()
goldenEggTimer()
