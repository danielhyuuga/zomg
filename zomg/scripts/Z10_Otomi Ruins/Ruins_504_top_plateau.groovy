import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Masks                                    
//------------------------------------------

def MaskA = myRooms.OtRuins_504.spawnSpawner( "MaskA", "mask", 1 )
MaskA.setPos( 240, 300 )
MaskA.setWaitTime( 60, 90 )
MaskA.setGuardPostForChildren( MaskA, 135 )
MaskA.setCFH( 500 )
MaskA.setMonsterLevelForChildren( 7.5 )

def MaskB = myRooms.OtRuins_504.spawnSpawner( "MaskB", "mask", 1 )
MaskB.setPos( 485, 410 )
MaskB.setWaitTime( 60, 90 )
MaskB.setGuardPostForChildren( MaskB, 135 )
MaskB.setCFH( 500 )
MaskB.setMonsterLevelForChildren( 7.5 )

def MaskC = myRooms.OtRuins_504.spawnSpawner( "MaskC", "mask", 1 )
MaskC.setPos( 750, 535 )
MaskC.setWaitTime( 60, 90 )
MaskC.setGuardPostForChildren( MaskC, 135 )
MaskC.setCFH( 500 )
MaskC.setMonsterLevelForChildren( 7.5 )

//------------------------------------------
// Feathered Coatl Stair Wanderers          
//------------------------------------------

def FCWanderers = myRooms.OtRuins_504.spawnSpawner( "FCWanderers", "feathered_coatl", 3 )
FCWanderers.setPos( 910, 290 )
FCWanderers.setWaitTime( 30, 60 )
FCWanderers.setWanderBehaviorForChildren( 50, 150, 3, 7, 250)
FCWanderers.setMonsterLevelForChildren( 7.5 )


//==========================
// ALLIANCES                
//==========================
MaskA.allyWithSpawner( MaskB )
MaskA.allyWithSpawner( MaskC )
MaskB.allyWithSpawner( MaskC )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

MaskA.forceSpawnNow()
MaskB.forceSpawnNow()
MaskC.forceSpawnNow()
FCWanderers.spawnAllNow()