import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// SIDE-TO-SIDE GUARDS (Room 502)           
//------------------------------------------

def topOne = myRooms.OtRuins_502.spawnSpawner( "topOne", "bladed_statue", 1 )
topOne.setPos( 350, 290 )
topOne.setWaitTime( 50, 70 )
topOne.addPatrolPointForChildren( "OtRuins_502", 355, 290, 2 )
topOne.addPatrolPointForChildren( "OtRuins_502", 765, 55, 3 )
topOne.setMonsterLevelForChildren( 7.7 )

def middleOne = myRooms.OtRuins_502.spawnSpawner( "middleOne", "bladed_statue", 1 )
middleOne.setPos( 870, 250 )
middleOne.setWaitTime( 50, 70 )
middleOne.addPatrolPointForChildren( "OtRuins_502", 875, 250, 3 )
middleOne.addPatrolPointForChildren( "OtRuins_502", 400, 465, 2 )
middleOne.setMonsterLevelForChildren( 7.7 )

def bottomOne = myRooms.OtRuins_502.spawnSpawner( "bottomOne", "bladed_statue", 1 )
bottomOne.setPos( 440, 615 )
bottomOne.setWaitTime( 50, 70 )
bottomOne.addPatrolPointForChildren( "OtRuins_502", 445, 615, 2 )
bottomOne.addPatrolPointForChildren( "OtRuins_502", 960, 470, 3 )
bottomOne.setMonsterLevelForChildren( 7.7 )


topOne.forceSpawnNow()
middleOne.forceSpawnNow()
bottomOne.forceSpawnNow()
