import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Vases                                    
//------------------------------------------

def VaseA = myRooms.OtRuins_604.spawnSpawner( "VaseA", "bladed_statue", 1 )
VaseA.setPos( 640, 270 )
VaseA.setWaitTime( 50, 70 )
VaseA.addPatrolPointForChildren( "OtRuins_604", 510, 360, 2 )
VaseA.addPatrolPointForChildren( "OtRuins_604", 685, 240, 2 )
VaseA.setMonsterLevelForChildren( 7.4 )

def VaseB = myRooms.OtRuins_604.spawnSpawner( "VaseB", "bladed_statue", 1 )
VaseB.setPos( 800, 175 )
VaseB.setWaitTime( 50, 70 )
VaseB.addPatrolPointForChildren( "OtRuins_604", 725, 220, 2 )
VaseB.addPatrolPointForChildren( "OtRuins_604", 915, 120, 2 )
VaseB.setMonsterLevelForChildren( 7.4 )

//------------------------------------------
// Tiny Terror Wanderers                    
//------------------------------------------

def tinyWanderers604 = myRooms.OtRuins_604.spawnSpawner( "tinyWanderers604", "tiny_terror", 4 )
tinyWanderers604.setPos( 245, 165 )
tinyWanderers604.setWaitTime( 30, 60 )
tinyWanderers604.setWanderBehaviorForChildren( 50, 150, 3, 7, 200)
tinyWanderers604.setHomeTetherForChildren( 2000 )
tinyWanderers604.setHateRadiusForChildren( 2000 )
tinyWanderers604.setMonsterLevelForChildren( 7.4 )


//------------------------------------------
// Tiny Patroller                           
//------------------------------------------

def tinyPatroller604 = myRooms.OtRuins_604.spawnSpawner( "tinyPatroller604", "tiny_terror", 1 )
tinyPatroller604.setPos( 235, 150 )
tinyPatroller604.setWaitTime( 60, 90 )
tinyPatroller604.setHome( tinyWanderers604 )
tinyPatroller604.setRFH( true )
tinyPatroller604.setCFH( 300 )
tinyPatroller604.setHomeTetherForChildren( 2000 )
tinyPatroller604.setHateRadiusForChildren( 2000 )
tinyPatroller604.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
tinyPatroller604.setCowardLevelForChildren( 100 )
tinyPatroller604.setMonsterLevelForChildren( 7.4 )
tinyPatroller604.addPatrolPointForChildren( "OtRuins_604", 230, 150, 10 )
tinyPatroller604.addPatrolPointForChildren( "OtRuins_603", 780, 168, 1 )


//==========================
// ALLIANCES                
//==========================
tinyWanderers604.allyWithSpawner( VaseA )
tinyWanderers604.allyWithSpawner( VaseB )

tinyPatroller604.allyWithSpawner( tinyWanderers604 )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

tinyWanderers604.spawnAllNow()
tinyPatroller604.forceSpawnNow()
VaseA.forceSpawnNow()
VaseB.forceSpawnNow()