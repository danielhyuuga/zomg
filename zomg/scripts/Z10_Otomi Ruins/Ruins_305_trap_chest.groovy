import com.gaiaonline.mmo.battle.script.*;

FEATHER_PERCENTAGE = 55

//------------------------------------------
// Ambush Spawners                          
// (They spawn when the chest is opened.)   
//------------------------------------------
ambushOne305 = myRooms.OtRuins_305.spawnStoppedSpawner( "ambushOne305", "feathered_coatl", 1)
ambushOne305.setPos( 140, 450 )
ambushOne305.setCFH( 300 )
ambushOne305.setInitialMoveForChildren( "OtRuins_305", 280, 440 )
ambushOne305.setMonsterLevelForChildren( 7.5 )

ambushTwo305 = myRooms.OtRuins_305.spawnStoppedSpawner( "ambushTwo305", "feathered_coatl", 1)
ambushTwo305.setPos( 430, 460 )
ambushTwo305.setCFH( 300 )
ambushTwo305.setInitialMoveForChildren( "OtRuins_305", 375, 585 )
ambushTwo305.setMonsterLevelForChildren( 7.5 )

ambushThree305 = myRooms.OtRuins_305.spawnStoppedSpawner( "ambushThree305", "feathered_coatl", 1)
ambushThree305.setPos( 140, 375 )
ambushThree305.setCFH( 300 )
ambushThree305.setInitialMoveForChildren( "OtRuins_305", 240, 570 )
ambushThree305.setMonsterLevelForChildren( 7.5 )

//------------------------------------------
// Ambush Chest                             
//------------------------------------------

zone = 10 //Otami Ruins

//======Basket 305======
basket305 = makeSwitch( "halfVisibleChest", myRooms.OtRuins_305, 385, 610 )
basket305.unlock()
basket305.off()
basket305.setRange(150)

hateCollector = [] as Set
playerSet305 = [] as Set

def open305 = { event ->
	if( isPlayer( event.actor ) ) {
		basket305.off()
		basket305.lock()
		event.actor.centerPrint( "The basket lids snaps shut again and bird cries erupt from the trees!" )
		playerInAmbush = event.actor
		myManager.schedule(1) { 
			spawnAmbush()
			checkForChestUnlock()
		}
	}
}

basket305.whenOn( open305 )

def spawnAmbush() { 
	//spawn the ambush
	FC305a = ambushOne305.forceSpawnNow()
	FC305a.addHate( playerInAmbush, 140 )
	runOnDeath( FC305a ) { event -> gatherHateAndGiveCAMPRewards( event ) } 

	FC305b = ambushTwo305.forceSpawnNow()
	FC305b.addHate( playerInAmbush, 140 )
	runOnDeath( FC305b ) { event -> gatherHateAndGiveCAMPRewards( event ) } 

	FC305c = ambushThree305.forceSpawnNow()
	FC305c.addHate( playerInAmbush, 140 )
	runOnDeath( FC305c ) { event -> gatherHateAndGiveCAMPRewards( event ) } 
}

//this routine is called from the runOnDeath statements to gather the hate lists and reward users with Fiber if they are on that task
def synchronized gatherHateAndGiveCAMPRewards( event ) {
	event.actor.getHated().each{ if( isPlayer(it) ) { hateCollector << it } }
	featherRoll = random( 100 )
	event.actor.getHated().each{
		if( isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && ( it.isOnQuest(305, 2) || it.isOnQuest(102, 2) ) && it.getConLevel() <= CLMap[zone]+1 ) {
			it.grantItem( "100407" ) //Feather
		} else if( isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && ( it.isOnQuest(305, 2) || it.isOnQuest(102, 2) ) && it.getConLevel() > CLMap[zone]+1 ) {
			it.centerPrint( "Change your level to ${(CLMap[zone]+1).intValue() * 1.0} or less to gather Coatl Feathers." )
		}
	}
}

def checkForChestUnlock() {
	if( ambushOne305.spawnsInUse() + ambushTwo305.spawnsInUse() + ambushThree305.spawnsInUse() == 0 ) {
		myManager.schedule(2) { openBasket() }
	} else {
		myManager.schedule(2) { checkForChestUnlock() }
	}
}
	

def openBasket() {
	basket305.on()
	basket305.lock()
	if( isOnline( playerInAmbush ) ) {
		playerSet305.clear()
		myRooms.OtRuins_305.getActorList().each { if( isPlayer( it ) ) { playerSet305 << it } }
		
		//now remove any players from that Set that are not in OtRuins_305
		hateCollector.clone().each{ if( !playerSet305.contains( it ) ) { hateCollector.remove( it ) } }
		
		hateCollector.each{
			if( it.getConLevel() <= CLMap[zone]+1 ) {
				chestType = 1 //1 = basket; 2 = crate; 3 = chest; 4 = safe

				//award the badge if the player hasn't received it yet
				if(!it.isDoneQuest(230)) { it.updateQuest(230, "Story-VQS") }

				//scale the returned results down by relative con levels if the player is below the normal CL for the area
				if( it.getConLevel() > CLMap[zone] ) {
					lootMultiplier = 1.0
				} else {
					lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
				}

				//grant gold
				goldGrant = goldMap[zone] 
				goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
				it.grantCoins( ( goldGrant * lootMultiplier * chestType ).intValue() ) 

				//grant common items
				chance = (commonChance * chestType).intValue()
				roll = random( 100 )
				if( roll <= chance ) {
					commonGrant = random( commonMap[zone] )
					it.grantItem( commonGrant )
				}

				//grant uncommon items
				chance = (uncommonChance * chestType).intValue()
				roll = random( 100 )
				if( roll <= chance ) {
					uncommonGrant = random( uncommonMap[zone] )
					it.grantItem( uncommonGrant )
				}

				//grant recipes
				chance = (recipeChance * chestType).intValue()
				roll = random( 100 )
				if( roll <= chance ) {
					recipeGrant = random( recipeMap[zone] )
					it.grantItem( recipeGrant )
				}

				//grant orbs
				chance = (orbChance * chestType).intValue()
				roll = random( 100 )
				if( roll <= chance ) {
					orbGrant = orbMap[zone]
					orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType ).intValue()
					it.grantQuantityItem( 100257, orbGrant )
				}

				//grant rings
				chance = (ringChance * chestType).intValue()
				roll = random( 1, 100 )
				if( roll <= chance ) {
					ringGrant = random( ringMap[zone] )
					it.grantRing( ringGrant, true )
				}
			} else {
				it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
			}
		}
	}
	delay305() //delay before the chest resets
}

def delay305() {
	myManager.schedule( random( 300, 600) ) { hateCollector.clear(); basket305.off(); basket305.unlock() }
}

//==========================
// ALLIANCES                
//==========================
ambushOne305.allyWithSpawner( ambushTwo305 )
ambushOne305.allyWithSpawner( ambushThree305 )
ambushTwo305.allyWithSpawner( ambushThree305 )

//====================================================================================
//====================================================================================
commonChance = 20 
uncommonChance = 10 
recipeChance = 1
orbChance = 5
ringChance = 1 

CLMap = [ 2:1, 3:2, 4:2.5, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

goldMap = [ 2:10, 3:20, 4:30, 5:40, 6:50, 7:60, 8:70, 9:80, 10:90, 11:100, 14:10, 16:90, 18:80 ]

commonMap = [ 2:["100272", "100289", "100385", "100297", "100397"], 3:["100291", "100262", "100263", "100298"], 4:["100388", "100278", "100275", "100283"], 5:["100281", "100367", "100411", "100394", "100284", "100405", "100267"], 6:["100265", "100285", "100398", "100397", "100376", "100296"], 7:["100373", "100260"], 8:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 9:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 11:["100371", "100294", "100391", "100277", "100290", "100368", "100386", "100403"], 14:[0], 16:["100287", "100409"], 18:["100393", "100293", "100259", "100292"] ]

uncommonMap = [ 2:["100280", "100279", "100270", "100380", "100384"], 3:["100378", "100261", "100271"], 4:["100276", "100258", "100268"], 5:["100381", "100382", "100282", "100383"], 6:["100390", "100299", "100286", "100369", "100400", "100387"], 7:["100365", "100410"], 8:["100389", "100264", "100406", "100399", "100402"], 9:["100389", "100264", "100406", "100399", "100402"], 10:["100395", "100413", "100407", "100266"], 11:["100396", "100269", "100401", "100372", "100375", "100366", "100379", "100274"], 14:[0], 16:["100404", "100288"], 18:["100395", "100413", "100407", "100266"] ]

recipeMap = [ 2:["17766", "17764", "17772", "17758", "17756"], 3:["17861", "17857", "17755"], 4:["17848", "17849", "17852", "17851", "17850", "17753"], 5:["17833", "17831", "17754", "17836", "17835"], 6:["17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779"], 7:["17785", "17793", "17788", "17787"], 8:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 9:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 10:["17796", "17846", "17803", "17844"], 11:["17819", "17816", "17811", "17808", "17806", "17812", "17813", "17814", "17810", "17809", "17815"], 14:[0], 16:["17786", "17791", "17790", "17794"], 18:["17796", "17846", "17803", "17844"] ]

orbMap = [ 2:1, 3:2, 4:3, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

ringMap = [ 2:["17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 3:["17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 4:["17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 5:["17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 6:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 7:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 8:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 9:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 10:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 11:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 14:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 16:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 18:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"] ]

//====================================================================================
//====================================================================================

