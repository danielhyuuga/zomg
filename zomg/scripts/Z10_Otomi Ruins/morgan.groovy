//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Morgan = spawnNPC("Morgan-VQS", myRooms.OtRuins_307, 640, 640)
Morgan.setDisplayName( "Morgan" )

Morgan.setRotation( 135 )

onQuestStep( 308, 2 ) { event -> event.player.addMiniMapQuestActorName( "Morgan-VQS" ) } 
onQuestStep( 309, 2 ) { event -> event.player.addMiniMapQuestActorName( "Morgan-VQS" ) } 

//-----------------------------------------------------------------------------
//Grant Tiny Troubles - Morgan Default Convo                                   
//-----------------------------------------------------------------------------
def tinyGrant = Morgan.createConversation("tinyGrant", true, "!QuestStarted_308", "!Z10_Morgan_Tiny_Break", "!QuestStarted_309", "!QuestCompleted_308")

def tinyGrant1 = [id:1]
tinyGrant1.npctext = "IIIIIIEEEEEEE! Get away you nasty little things! Oh... wait. You're not one of them."
tinyGrant1.playertext = "One of what?"
tinyGrant1.result = 2
tinyGrant.addDialog(tinyGrant1, Morgan)

def tinyGrant2 = [id:2]
tinyGrant2.npctext = "You mean you haven't seen them crawling all over the place? Those tiny, little, spear-toting menaces!"
tinyGrant2.options = []
tinyGrant2.options << [text:"Can't say that I have.", result: 3]
tinyGrant2.options << [text:"Oh, them. Yeah, I've seen them. What's the big deal? They're not so tough.", result: 5]
tinyGrant2.options << [text:"Yeah! Little buggers keep attacking me. They're tough, too!", result: 6]
tinyGrant.addDialog(tinyGrant2, Morgan)

def tinyGrant3 = [id:3]
tinyGrant3.npctext = "You will! They have me trapped up here, and now you're probably trapped with me."
tinyGrant3.playertext = "What do you mean 'trapped?'"
tinyGrant3.result = 4
tinyGrant.addDialog(tinyGrant3, Morgan)

def tinyGrant4 = [id:4]
tinyGrant4.npctext = "IIIIIIEEEEEEE! Sorry, I thought I saw one. I'm a little on edge because every time I try to get out of here they chase me back in! I just wish I could get out of here."
tinyGrant4.options = []
tinyGrant4.options << [text:"Maybe I can clear the way.", result: 20]
tinyGrant4.options << [text:"Jeez, just walk out, like this.", result: 25]
tinyGrant.addDialog(tinyGrant4, Morgan)

def tinyGrant5 = [id:5]
tinyGrant5.npctext = "Not so tough? They've got me trapped back here!"
tinyGrant5.playertext = "Trapped? Why don't you just walk out?"
tinyGrant5.result = 4
tinyGrant.addDialog(tinyGrant5, Morgan)

def tinyGrant6 = [id:6]
tinyGrant6.npctext = "Yep, I'm trapped back here because I can't get past them!"
tinyGrant6.playertext = "Why can't you get past them?"
tinyGrant6.result = 4
tinyGrant.addDialog(tinyGrant6, Morgan)

def tinyGrant20 = [id:20]
tinyGrant20.npctext = "Really? That would be awesome! Maybe you can bring me their spear heads so I know it's safe?"
tinyGrant20.playertext = "Sure, I'll do that."
tinyGrant20.quest = 308
tinyGrant20.result = DONE
tinyGrant.addDialog(tinyGrant20, Morgan)

def tinyGrant25 = [id:25]
tinyGrant25.npctext = "Nooooo, don't leave me here!"
tinyGrant25.flag = "Z10_Morgan_Tiny_Break"
tinyGrant25.result = DONE
tinyGrant.addDialog(tinyGrant25, Morgan)

//-----------------------------------------------------------------------------
//Grant Tiny Troubles - Morgan Break Convo                                     
//-----------------------------------------------------------------------------
def tinyBreak = Morgan.createConversation("tinyBreak", true, "Z10_Morgan_Tiny_Break")

def tinyBreak1 = [id:1]
tinyBreak1.npctext = "IIIIIIEEEEEEE! Oh, you're back. Hi, %p. Going to help me get out of here now?"
tinyBreak1.options = []
tinyBreak1.options << [text:"Sure, I'll clear a path.", result: 2]
tinyBreak1.options << [text:"Nope, you're on your own to get out. Though, you can try following as I leave if you dare.", result: 3]
tinyBreak.addDialog(tinyBreak1, Morgan)

def tinyBreak2 = [id:2]
tinyBreak2.npctext = "Great, just bring me their spear heads so I know it's safe."
tinyBreak2.quest = 308
tinyBreak2.flag = "!Z10_Morgan_Tiny_Break"
tinyBreak2.result = DONE
tinyBreak.addDialog(tinyBreak2, Morgan)

def tinyBreak3 = [id:3]
tinyBreak3.npctext = "That doesn't sound safe at all."
tinyBreak3.result = DONE
tinyBreak.addDialog(tinyBreak3, Morgan)

//-----------------------------------------------------------------------------
//Grant Tiny Troubles - Morgan Active Convo                                    
//-----------------------------------------------------------------------------
def tinyActive = Morgan.createConversation("tinyActive", true, "QuestStarted_308:2")

def tinyActive1 = [id:1]
tinyActive1.npctext = "IIIIIIEEEEEEE! %p! Phew! Remember, spear heads so I know it's safe!"
tinyActive1.playertext = "Right, I'm on it."
tinyActive1.result = DONE
tinyActive.addDialog(tinyActive1, Morgan)

//-----------------------------------------------------------------------------
//Grant Tiny Troubles - Morgan Completion                                      
//-----------------------------------------------------------------------------
def tinyComplete = Morgan.createConversation("tinyComplete", true, "QuestStarted_308:3")
tinyComplete.setUrgent(true)

def tinyComplete1 = [id:1]
tinyComplete1.npctext = "Hello, %p. I have a bit of a confession to make..."
tinyComplete1.playertext = "A confession? What do you mean?"
tinyComplete1.result = 2
tinyComplete.addDialog(tinyComplete1, Morgan)

def tinyComplete2 = [id:2]
tinyComplete2.npctext = "I, uh... well I'm not really trapped here."
tinyComplete2.playertext = "Silly, I know that. I just got finished clearing the way out."
tinyComplete2.result = 3
tinyComplete.addDialog(tinyComplete2, Morgan)

def tinyComplete3 = [id:3]
tinyComplete3.npctext = "No, I mean I never was trapped. I really just needed the spear heads."
tinyComplete3.options = []
tinyComplete3.options << [text:"What?! I can't believe you tricked me!", result: 4]
tinyComplete3.options << [text:"I would have helped you if you had told me the truth. I don't know why you felt you had to lie.", result: 5]
tinyComplete3.options << [text:"Fine, since you tricked me I'm making you pay to get these spear heads!", result: 6]
tinyComplete.addDialog(tinyComplete3, Morgan)

def tinyComplete4 = [id:4]
tinyComplete4.npctext = "I'm sorry, I was desperate... and I'll reward you handsomely for the spear heads to make up for it."
tinyComplete4.playertext = "Okay, that sounds better."
tinyComplete4.result = 7
tinyComplete.addDialog(tinyComplete4, Morgan)

def tinyComplete5 = [id:5]
tinyComplete5.npctext = "Oh, I didn't know that. I feel really guilty now. Tell you what, I'll reward you for the spear heads to make up for my lie."
tinyComplete5.playertext = "That's more like it..."
tinyComplete5.result = 7
tinyComplete.addDialog(tinyComplete5, Morgan)

def tinyComplete6 = [id:6]
tinyComplete6.npctext = "I suppose that's only fair. I'll give you a nice reward for those spear heads to make up for lying to you."
tinyComplete6.playertext = "Yeah, you'd better!"
tinyComplete6.result = 7
tinyComplete.addDialog(tinyComplete6, Morgan)

def tinyComplete7 = [id:7]
tinyComplete7.npctext = "By the way, I still need spear heads. I'll reward you again if you bring more. Interested?"
tinyComplete7.options = []
tinyComplete7.options << [text:"You bet!", exec: { event ->
	event.player.removeMiniMapQuestActorName("Morgan-VQS")
	if(event.player.getConLevel() < 8.6) {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_Allowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")
	}
	runOnDeduct(event.player, 100395, 10, tinySuccess, tinyFail)
}, result: DONE]
tinyComplete7.options << [text:"I'm not really interested in working with someone I can't trust.", exec: { event -> 
	event.player.removeMiniMapQuestActorName("Morgan-VQS")
	runOnDeduct(event.player, 100395, 10, tinySuccess, tinyFail) 
}, result: DONE]
tinyComplete.addDialog(tinyComplete7, Morgan)

//-----------------------------------------------------------------------------
//Tiny Troubles runOnDeduct logic                                              
//-----------------------------------------------------------------------------
tinySuccess = { event -> //If player has enough spear heads, push appropriate dialog
	event.actor.updateQuest(308, "Morgan-VQS")
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Allowed")) { Morgan.pushDialog(event.actor, "spearAllowed") }
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")) { Morgan.pushDialog(event.actor, "spearDenied") }
	if(!event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Allowed") && !event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")) { 
		event.actor.setQuestFlag(GLOBAL, "Z10_Morgan_TinySuccess")
		Morgan.pushDialog(event.actor, "tinySuccess") 
	} 
}

tinyFail = { event -> //If player doesn't have enough, disable dialogs and push fail dialog
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Allowed")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z10_Morgan_Spear_Allowed")
	}
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")
	}
	event.actor.setQuestFlag(GLOBAL, "Z10_Morgan_tinyFail")
	Morgan.pushDialog(event.actor, "tinyFail")
}

//-----------------------------------------------------------------------------
//Success convo if player doesn't have 10 spears but doesn't go again          
//-----------------------------------------------------------------------------
def tinySuccess = Morgan.createConversation("tinySuccess", true, "Z10_Morgan_TinySuccess")

def tinySuccess1 = [id:1]
tinySuccess1.npctext = "Alright, up to you. I'll be here if you change your mind."
tinySuccess1.flag = "!Z10_Morgan_TinySuccess"
tinySuccess1.result = DONE
tinySuccess.addDialog(tinySuccess1, Morgan)

//-----------------------------------------------------------------------------
//Fail convo is player doesn't have 10 spears                                  
//-----------------------------------------------------------------------------
def tinyFail = Morgan.createConversation("tinyFail", true, "Z10_Morgan_TinyFail")

def tinyFail1 = [id:1]
tinyFail1.npctext = "You little trickster! You didn't clear out the Tiny Terrors... you barely have any Spear Heads at all! I'm hiding here until you can prove you dealt with the situation."
tinyFail1.playertext = "Bleh! I swear I had them. I'll be right back."
tinyFail1.flag = "!Z10_Morgan_TinyFail"
tinyFail1.result = DONE
tinyFail.addDialog(tinyFail1, Morgan)

//-----------------------------------------------------------------------------
//Grant Spear Me The Lies - Allowed                                            
//-----------------------------------------------------------------------------
def spearAllowed = Morgan.createConversation("spearAllowed", true, "Z10_Morgan_Spear_Allowed", "!QuestStarted_309")

def spearAllowed1 = [id:1]
spearAllowed1.npctext = "Great! Bring me twenty more Spear Heads and I'll gladly part with another reward."
spearAllowed1.quest = 309
spearAllowed1.flag = "!Z10_Morgan_Spear_Allowed"
spearAllowed1.result = DONE
spearAllowed.addDialog(spearAllowed1, Morgan)

//-----------------------------------------------------------------------------
//Grant Spear Me The Lies - Denied                                             
//-----------------------------------------------------------------------------
def spearDenied = Morgan.createConversation("spearDenied", true, "Z10_Morgan_Spear_Denied", "!QuestStarted_309")

def spearDenied1 = [id:1]
spearDenied1.npctext = "I think matters of such small importance are beneath you by now."
spearDenied1.flag = "!Z10_Morgan_Spear_Denied"
spearDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 8.5 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
spearDenied1.result = DONE
spearDenied.addDialog(spearDenied1, Morgan)

//-----------------------------------------------------------------------------
//Grant Spear Me The Lies                                                      
//-----------------------------------------------------------------------------
def spearGrant = Morgan.createConversation("spearGrant", true, "QuestCompleted_308", "!QuestStarted_309")
spearGrant.setUrgent(true)

def spearGrant1 = [id:1]
spearGrant1.npctext = "IIIIIIEEEEEEE knew you'd be back, %p. The lure of a reward is enticing."
spearGrant1.options = []
spearGrant1.options << [text:"You're right, I decided to help after all.", exec: { event ->
	event.player.removeMiniMapQuestActorName("Morgan-VQS")
	event.player.updateQuest(308, "Morgan-VQS")
	if(event.player.getConLevel() < 8.6) {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_Allowed")
		Morgan.pushDialog(event.player, "spearAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")
		Morgan.pushDialog(event.player, "spearDenied")
	}
},result: DONE]
spearGrant1.options << [text:"Huh? Nope, I'm just passing through.", result: DONE]
spearGrant.addDialog(spearGrant1, Morgan)

//-----------------------------------------------------------------------------
//Active Spear Me The Lies                                                     
//-----------------------------------------------------------------------------
def spearActive = Morgan.createConversation("spearActive", true, "QuestStarted_309:2")

def spearActive1 = [id:1]
spearActive1.npctext = "IIIIIIEEEEEEE!"
spearActive1.playertext = "Jeez, give it a rest. I'll bring your Spear Heads soon enough."
spearActive1.result = DONE
spearActive.addDialog(spearActive1, Morgan)

//-----------------------------------------------------------------------------
//Complete Spear Me The Lies                                                   
//-----------------------------------------------------------------------------
def spearComplete = Morgan.createConversation("spearComplete", true, "QuestStarted_309:3")
spearComplete.setUrgent(true)

def spearComplete1 = [id:1]
spearComplete1.npctext = "Oh, I guess it's time for me to pay up again."
spearComplete1.playertext = "You bet it is!"
spearComplete1.result = 2
spearComplete.addDialog(spearComplete1, Morgan)

def spearComplete2 = [id:2]
spearComplete2.npctext = "Fair enough, you've definitely earned it. Just so you know, the offer still stands if you want to gather more Spear Heads."
spearComplete2.options = []
spearComplete2.options << [text:"You've got a deal.", exec: { event ->
	event.player.removeMiniMapQuestActorName("Morgan-VQS")
	if(event.player.getConLevel() < 8.6) {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_AllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")
	}
	runOnDeduct(event.player, 100395, 20, spearSuccess, spearFail)
}, result: DONE]
spearComplete2.options << [text:"Why do you want so many Spear Heads anyway?", result: 3]
spearComplete2.options << [text:"Nah, I think I'm set for now.", exec: { event -> 
	event.player.removeMiniMapQuestActorName("Morgan-VQS")
	runOnDeduct(event.player, 100395, 20, spearSuccess, spearFail) 
}, result: DONE]
spearComplete.addDialog(spearComplete2, Morgan)

def spearComplete3 = [id:3]
spearComplete3.npctext = "I heard there's a theme park called 'Buccaneer Boardwalk' under construction. Theme parks sell this kind of thing as souvenir and I plan to corner the market."
spearComplete3.playertext = "That's not a bad idea... although I don't know what Spear Heads have to do with buccaneers or boardwalks."
spearComplete3.result = 5
spearComplete.addDialog(spearComplete3, Morgan)

def spearComplete5 = [id:5]
spearComplete5.npctext = "Yeah, well, you know tourists. They'll buy ANYTHING! Anyway, gonna get some more Spear Heads for me or just talk my ear off?"
spearComplete5.options = []
spearComplete5.options << [text:"Count me in.", exec: { event ->
	event.player.removeMiniMapQuestActorName("Morgan-VQS")
	if(event.player.getConLevel() < 8.6) {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_AllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")
	}
	runOnDeduct(event.player, 100395, 20, spearSuccess, spearFail)
}, result: DONE]
spearComplete5.options << [text:"Count me out.", exec: { event -> 
	event.player.removeMiniMapQuestActorName("Morgan-VQS")
	runOnDeduct(event.player, 100395, 20, spearSuccess, spearFail) 
}, result: DONE]
spearComplete.addDialog(spearComplete5, Morgan)

//-----------------------------------------------------------------------------
//Tiny Troubles runOnDeduct logic                                              
//-----------------------------------------------------------------------------
spearSuccess = { event -> //If player has enough spear heads, push appropriate dialog
	event.actor.updateQuest(309, "Morgan-VQS")
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Allowed")) { Morgan.pushDialog(event.actor, "spearAllowedAgain") }
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")) { Morgan.pushDialog(event.actor, "spearDenied") }
	if(!event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_AllowedAgain") && !event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")) { 
		event.actor.setQuestFlag(GLOBAL, "Z10_Morgan_SpearSuccess")
		Morgan.pushDialog(event.actor, "spearSuccess") 
	} 
}

spearFail = { event -> //If player doesn't have enough, disable dialogs and push fail dialog
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_AllowedAgain")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z10_Morgan_Spear_AllowedAgain")
	}
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z10_Morgan_Spear_Denied")
	}
	event.actor.setQuestFlag(GLOBAL, "Z10_Morgan_SpearFail")
	Morgan.pushDialog(event.actor, "spearFail")
}

//-----------------------------------------------------------------------------
//Success convo if player doesn't have 10 spears but doesn't go again          
//-----------------------------------------------------------------------------
def spearSuccess = Morgan.createConversation("spearSuccess", true, "Z10_Morgan_SpearSuccess")

def spearSuccess1 = [id:1]
spearSuccess1.npctext = "Alright. Take care, %p."
spearSuccess1.flag = "!Z10_Morgan_SpearSuccess"
spearSuccess1.result = DONE
spearSuccess.addDialog(spearSuccess1, Morgan)

//-----------------------------------------------------------------------------
//Fail convo is player doesn't have 20 spears                                  
//-----------------------------------------------------------------------------
def spearFail = Morgan.createConversation("spearFail", true, "Z10_Morgan_SpearFail")

def spearFail1 = [id:1]
spearFail1.npctext = "Well, I guess I can't blame you for trying to pull a fast one after I tricked you earlier, but I'm a little saavy to fall for it. Come back when you have twenty Spear Heads."
spearFail1.playertext = "Bleh! I swear I had them. I'll be right back."
spearFail1.flag = "!Z10_Morgan_SpearFail"
spearFail1.result = DONE
spearFail.addDialog(spearFail1, Morgan)

//-----------------------------------------------------------------------------
//Spear Me The Lies - Allowed Again                                            
//-----------------------------------------------------------------------------
def spearAllowedAgain = Morgan.createConversation("spearAllowedAgain", true, "Z10_Morgan_Spear_AllowedAgain")

def spearAllowedAgain1 = [id:1]
spearAllowedAgain1.npctext = "Great, best of luck. Just come back around once you have the Spear Heads."
spearAllowedAgain1.quest = 309
spearAllowedAgain1.flag = "!Z10_Morgan_Spear_AllowedAgain"
spearAllowedAgain1.result = DONE
spearAllowedAgain.addDialog(spearAllowedAgain1, Morgan)