import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Wanderer Groups                          
//------------------------------------------

def secondTierWanderers = myRooms.OtRuins_703.spawnSpawner( "secondTierWanderers", "tiny_terror", 4)
secondTierWanderers.setPos( 955, 240 )
secondTierWanderers.setWaitTime( 50, 70 )
secondTierWanderers.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
secondTierWanderers.setHomeTetherForChildren( 1200 )
secondTierWanderers.setHateRadiusForChildren( 1200 )
secondTierWanderers.setMonsterLevelForChildren( 7.4 )

def thirdTierWanderers = myRooms.OtRuins_703.spawnSpawner( "thirdTierWanderers", "tiny_terror", 4)
thirdTierWanderers.setPos( 120, 115 )
thirdTierWanderers.setWaitTime( 50, 70 )
thirdTierWanderers.setHomeTetherForChildren( 1200 )
thirdTierWanderers.setHateRadiusForChildren( 1200 )
thirdTierWanderers.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
thirdTierWanderers.setMonsterLevelForChildren( 7.4 )


//------------------------------------------
// Room Guards                              
//------------------------------------------

def secondTierGuard = myRooms.OtRuins_703.spawnSpawner( "secondTierGuard", "tiny_terror", 1)
secondTierGuard.setPos( 380, 490 )
secondTierGuard.setGuardPostForChildren( secondTierGuard, 135 )
secondTierGuard.setHome( secondTierWanderers )
secondTierGuard.setRFH( true )
secondTierGuard.setCFH( 400 )
secondTierGuard.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
secondTierGuard.setCowardLevelForChildren( 100 )
secondTierGuard.setWaitTime( 60, 90 )
secondTierGuard.setMonsterLevelForChildren( 7.4 )

def thirdTierGuard = myRooms.OtRuins_703.spawnSpawner( "thirdTierGuard", "tiny_terror", 1)
thirdTierGuard.setPos( 440, 240 )
thirdTierGuard.setGuardPostForChildren( thirdTierGuard, 45 )
thirdTierGuard.setHome( thirdTierWanderers )
thirdTierGuard.setRFH( true )
thirdTierGuard.setCFH( 200 )
thirdTierGuard.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
thirdTierGuard.setCowardLevelForChildren( 100 )
thirdTierGuard.setWaitTime( 60, 90 )
thirdTierGuard.setMonsterLevelForChildren( 7.4 )


//==========================
// ALLIANCES                
//==========================
secondTierWanderers.allyWithSpawner( secondTierGuard )
thirdTierWanderers.allyWithSpawner( thirdTierGuard )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

secondTierWanderers.spawnAllNow()
thirdTierWanderers.spawnAllNow()
secondTierGuard.forceSpawnNow()
thirdTierGuard.forceSpawnNow()
