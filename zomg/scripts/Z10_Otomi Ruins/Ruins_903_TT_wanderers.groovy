import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Tiny Terror Wanderers                    
//------------------------------------------

def tinyWanderers903 = myRooms.OtRuins_903.spawnSpawner( "tinyWanderers903", "tiny_terror", 3 )
tinyWanderers903.setPos( 445, 125 )
tinyWanderers903.setWaitTime( 50, 70 )
tinyWanderers903.setWanderBehaviorForChildren( 50, 150, 2, 5, 200)
tinyWanderers903.setMonsterLevelForChildren( 7.3 )

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

tinyWanderers903.spawnAllNow()