import com.gaiaonline.mmo.battle.script.*;

//Variable init
patrolLeader1 = null
patrolGroup1A = null
patrolGroup1B = null

patrolLeader2 = null
patrolGroup2A = null
patrolGroup2B = null

patrolLeader3 = null
patrolGroup3A = null
patrolGroup3B = null

comingFromAqueductPatrolOne = false
comingFromAqueductPatrolTwo = true
comingFromAqueductPatrolThree = true

SPEAR_PERCENTAGE = 55

//PATROL PATHS
bloodTreeLoop = makeNewPatrol()
bloodTreeLoop.addPatrolPoint( "OtRuins_206", 505, 505, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_206", 100, 240, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_205", 650, 150, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_105", 165, 500, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_105", 500, 100, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_5", 840, 540, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_6", 290, 270, 5 )
bloodTreeLoop.addPatrolPoint( "OtRuins_5", 840, 540, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_105", 500, 100, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_105", 165, 500, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_205", 650, 150, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_206", 100, 240, 0 )
bloodTreeLoop.addPatrolPoint( "OtRuins_206", 500, 500, 0 )

sideCampLoop = makeNewPatrol()
sideCampLoop.addPatrolPoint( "OtRuins_206", 505, 505, 0 )
sideCampLoop.addPatrolPoint( "OtRuins_206", 830, 340, 0 )
sideCampLoop.addPatrolPoint( "OtRuins_207", 250, 205, 0 )
sideCampLoop.addPatrolPoint( "OtRuins_107", 350, 250, 3 )
sideCampLoop.addPatrolPoint( "OtRuins_207", 250, 205, 0 )
sideCampLoop.addPatrolPoint( "OtRuins_206", 830, 340, 0 )
sideCampLoop.addPatrolPoint( "OtRuins_206", 500, 500, 0 )

aqueductLoop = makeNewPatrol()
aqueductLoop.addPatrolPoint( "OtRuins_206", 505, 505, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_206", 520, 560, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_306", 400, 200, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_306", 120, 475, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_406", 680, 400, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_407", 120, 280, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_407", 290, 490, 5 )
aqueductLoop.addPatrolPoint( "OtRuins_407", 120, 280, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_406", 680, 400, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_306", 120, 475, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_306", 400, 200, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_206", 520, 560, 0 )
aqueductLoop.addPatrolPoint( "OtRuins_206", 500, 500, 0 )


//TRIGGER ZONES
def junctionTrigger = "junctionTrigger"
myRooms.OtRuins_206.createTriggerZone(junctionTrigger, 180, 220, 645, 650)
//500, 500 is the central point in this trigger zone

def bloodTreeTrigger = "bloodTreeTrigger"
myRooms.OtRuins_5.createTriggerZone(bloodTreeTrigger, 480, 485, 1000, 650)

def sideCampTrigger = "sideCampTrigger"
myRooms.OtRuins_207.createTriggerZone(sideCampTrigger, 1, 1, 570, 400)

def aqueductTrigger = "aqueductTrigger"
myRooms.OtRuins_406.createTriggerZone(aqueductTrigger, 540, 150, 1000, 530)


//------------------------------------------
// Spawners                                 
//------------------------------------------

patrolLeaderOne = myRooms.OtRuins_206.spawnSpawner( "patrolLeaderOne", "tiny_terror_LT", 1 )
patrolLeaderOne.stopSpawning()
patrolLeaderOne.setPos( 100, 600 )
patrolLeaderOne.setWaitTime( 5, 10 )
patrolLeaderOne.setSpawnWhenPlayersAreInRoom( true )
patrolLeaderOne.setHomeTetherForChildren( 2000 )
patrolLeaderOne.setHateRadiusForChildren( 2000 )
patrolLeaderOne.setBaseSpeed( 160 )
patrolLeaderOne.setCFH( 400 )
patrolLeaderOne.setChildrenToFollow( patrolLeaderOne )
patrolLeaderOne.setMonsterLevelForChildren( 7.4 )

patrolGroupOne = myRooms.OtRuins_206.spawnSpawner( "patrolGroupOne", "tiny_terror", 2 )
patrolGroupOne.stopSpawning()
patrolGroupOne.setPos( 100, 600 )
patrolGroupOne.setWaitTime( 5, 10 )
patrolGroupOne.setSpawnWhenPlayersAreInRoom( true )
patrolGroupOne.setHomeTetherForChildren( 2000 )
patrolGroupOne.setHateRadiusForChildren( 2000 )
patrolGroupOne.setCFH( 400 )
patrolGroupOne.startFollow( patrolLeaderOne )
patrolGroupOne.setChildrenToFollow( patrolLeaderOne )
patrolGroupOne.setMonsterLevelForChildren( 7.4 )

patrolLeaderTwo = myRooms.OtRuins_206.spawnSpawner( "patrolLeaderTwo", "tiny_terror_LT", 1)
patrolLeaderTwo.stopSpawning()
patrolLeaderTwo.setPos( 100, 600 )
patrolLeaderTwo.setWaitTime( 5, 10 )
patrolLeaderTwo.setSpawnWhenPlayersAreInRoom( true )
patrolLeaderTwo.setHomeTetherForChildren( 2000 )
patrolLeaderTwo.setHateRadiusForChildren( 2000 )
patrolLeaderTwo.setBaseSpeed( 160 )
patrolLeaderTwo.setCFH( 400 )
patrolLeaderTwo.setChildrenToFollow( patrolLeaderTwo )
patrolLeaderTwo.setMonsterLevelForChildren( 7.4 )

patrolGroupTwo = myRooms.OtRuins_206.spawnSpawner( "patrolGroupTwo", "tiny_terror", 2 )
patrolGroupTwo.stopSpawning()
patrolGroupTwo.setPos( 100, 600 )
patrolGroupTwo.setWaitTime( 5, 10 )
patrolGroupTwo.setSpawnWhenPlayersAreInRoom( true )
patrolGroupTwo.setHomeTetherForChildren( 2000 )
patrolGroupTwo.setHateRadiusForChildren( 2000 )
patrolGroupTwo.setCFH( 400 )
patrolGroupTwo.startFollow( patrolLeaderTwo )
patrolGroupTwo.setChildrenToFollow( patrolLeaderTwo )
patrolGroupTwo.setMonsterLevelForChildren( 7.4 )

patrolLeaderThree = myRooms.OtRuins_206.spawnSpawner( "patrolLeaderThree", "tiny_terror_LT", 1)
patrolLeaderThree.stopSpawning()
patrolLeaderThree.setPos( 100, 600 )
patrolLeaderThree.setWaitTime( 5, 10 )
patrolLeaderThree.setSpawnWhenPlayersAreInRoom( true )
patrolLeaderThree.setHomeTetherForChildren( 2000 )
patrolLeaderThree.setHateRadiusForChildren( 2000 )
patrolLeaderThree.setBaseSpeed( 160 )
patrolLeaderThree.setCFH( 400 )
patrolLeaderThree.setChildrenToFollow( patrolLeaderThree )
patrolLeaderThree.setMonsterLevelForChildren( 7.4 )

patrolGroupThree = myRooms.OtRuins_206.spawnSpawner( "patrolGroupThree", "tiny_terror", 2 )
patrolGroupThree.stopSpawning()
patrolGroupThree.setPos( 100, 600 )
patrolGroupThree.setWaitTime( 5, 10 )
patrolGroupThree.setHomeTetherForChildren( 2000 )
patrolGroupThree.setHateRadiusForChildren( 2000 )
patrolGroupThree.setSpawnWhenPlayersAreInRoom( true )
patrolGroupThree.setCFH( 400 )
patrolGroupThree.startFollow( patrolLeaderThree )
patrolGroupThree.setChildrenToFollow( patrolLeaderThree )
patrolGroupThree.setMonsterLevelForChildren( 7.4 )

//RANDOM LOOP LIST (for determining which way the patrol goes at the junction)
whichWay = []
whichWay << bloodTreeLoop
whichWay << sideCampLoop

//------------------------------------------
// Trigger Zone Logic                       
//------------------------------------------

myManager.onTriggerIn(myRooms.OtRuins_206, junctionTrigger) { event ->
	if ( event.actor == patrolLeaderOne && comingFromAqueductPatrolOne == true ) {
		thisLoop = random( whichWay )
		patrolLeaderOne.setPatrol( thisLoop )
	}
	if ( event.actor == patrolLeaderOne && comingFromAqueductPatrolOne == false ) {
		patrolLeaderOne.setPatrol( aqueductLoop )
	}
	if ( event.actor == patrolLeaderTwo && comingFromAqueductPatrolTwo == true ) {
		thisLoop = random( whichWay )
		patrolLeaderTwo.setPatrol( thisLoop )
	}
	if ( event.actor == patrolLeaderTwo && comingFromAqueductPatrolTwo == false ) {
		patrolLeaderTwo.setPatrol( aqueductLoop )
	}
	if ( event.actor == patrolLeaderThree && comingFromAqueductPatrolThree == true ) {
		thisLoop = random( whichWay )
		patrolLeaderThree.setPatrol( thisLoop )
	}
	if ( event.actor == patrolLeaderThree && comingFromAqueductPatrolThree == false ) {
		patrolLeaderThree.setPatrol( aqueductLoop )
	}
}

myManager.onTriggerIn(myRooms.OtRuins_5, bloodTreeTrigger) { event ->
	if( event.actor == patrolLeaderOne ) {
		comingFromAqueductPatrolOne = false
	}
	if( event.actor == patrolLeaderTwo ) {
		comingFromAqueductPatrolTwo = false
	}
	if( event.actor == patrolLeaderThree ) {
		comingFromAqueductPatrolThree = false
	}

}

myManager.onTriggerIn(myRooms.OtRuins_207, sideCampTrigger) { event ->
	if( event.actor == patrolLeaderOne ) {
		comingFromAqueductPatrolOne = false
	}
	if( event.actor == patrolLeaderTwo ) {
		comingFromAqueductPatrolTwo = false
	}
	if( event.actor == patrolLeaderThree ) {
		comingFromAqueductPatrolThree = false
	}

}

myManager.onTriggerIn(myRooms.OtRuins_406, aqueductTrigger ) { event ->
	if( event.actor == patrolLeaderOne ) {
		comingFromAqueductPatrolOne = true
	}
	if( event.actor == patrolLeaderTwo ) {
		comingFromAqueductPatrolTwo = true
	}
	if( event.actor == patrolLeaderThree ) {
		comingFromAqueductPatrolThree = true
	}
}

//==========================
// ALLIANCES                
//==========================
patrolLeaderOne.allyWithSpawner( patrolGroupOne )
patrolLeaderTwo.allyWithSpawner( patrolGroupTwo )
patrolLeaderThree.allyWithSpawner( patrolGroupThree )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

def spawnPatrolOne() {
	if(patrolLeader1 == null || patrolLeader1.isDead()) {
		patrolLeader1 = patrolLeaderOne.forceSpawnNow()
		
		patrolLeaderOne.setPatrol( aqueductLoop )
		patrolLeaderOne.startPatrol()
		
		runOnDeath(patrolLeader1) { event -> 
			myManager.schedule(random(80, 100)) {spawnPatrolOne()}
			spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE
		
			patrolLeader1HateList = event.actor.getHated()
			patrolLeader1HateList.each() { //Run this for each person on hate list at time of death
				if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
					it.grantItem( "100395" ) //Spear Head
				} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
					it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
				}
			}
		}
		
		if(patrolGroup1A == null || patrolGroup1A.isDead()) {
			patrolGroup1A = patrolGroupOne.forceSpawnNow()
			
			runOnDeath(patrolGroup1A) { event -> 
				spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE

				patrolGroup1AHateList = event.actor.getHated()
				patrolGroup1AHateList.each() { //Run this for each person on hate list at time of death
					if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
						it.grantItem( "100395" ) //Spear Head
					} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
						it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
					}
				}
			}
		}
		
		if(patrolGroup1B == null || patrolGroup1B.isDead()) {
			patrolGroup1B = patrolGroupOne.forceSpawnNow()
			
			runOnDeath(patrolGroup1B) { event -> 
				spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE

				patrolGroup1BHateList = event.actor.getHated()
				patrolGroup1BHateList.each() { //Run this for each person on hate list at time of death
					if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
						it.grantItem( "100395" ) //Spear Head
					} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
						it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
					}
				}
			}
		}	
	}
}

spawnPatrolOne()

def spawnPatrolTwo() {
	if(patrolLeader2 == null || patrolLeader2.isDead()) {
		patrolLeader2 = patrolLeaderTwo.forceSpawnNow()
		
		patrolLeaderTwo.setPatrol( bloodTreeLoop )
		patrolLeaderTwo.startPatrol()
		
		runOnDeath(patrolLeader2) { event -> 
			myManager.schedule(random(80, 100)) {spawnPatrolTwo()}
			spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE
		
			patrolLeader2HateList = event.actor.getHated()
			patrolLeader2HateList.each() { //Run this for each person on hate list at time of death
				if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
					it.grantItem( "100395" ) //Spear Head
				} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
					it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
				}
			}
		}
		
		if(patrolGroup2A == null || patrolGroup2A.isDead()) {
			patrolGroup2A = patrolGroupTwo.forceSpawnNow()
			
			runOnDeath(patrolGroup2A) { event -> 
				spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE

				patrolGroup2AHateList = event.actor.getHated()
				patrolGroup2AHateList.each() { //Run this for each person on hate list at time of death
					if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
						it.grantItem( "100395" ) //Spear Head
					} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
						it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
					}
				}
			}
		}
		
		if(patrolGroup2B == null || patrolGroup2B.isDead()) {
			patrolGroup2B = patrolGroupTwo.forceSpawnNow()
			
			runOnDeath(patrolGroup2B) { event -> 
				spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE

				patrolGroup2BHateList = event.actor.getHated()
				patrolGroup2BHateList.each() { //Run this for each person on hate list at time of death
					if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
						it.grantItem( "100395" ) //Spear Head
					} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
						it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
					}
				}
			}
		}	
	}
}

spawnPatrolTwo()

def spawnPatrolThree() {
	if(patrolLeader3 == null || patrolLeader3.isDead()) {
		patrolLeader3 = patrolLeaderThree.forceSpawnNow()
		
		patrolLeaderThree.setPatrol( sideCampLoop )
		patrolLeaderThree.startPatrol()
		
		runOnDeath(patrolLeader3) { event -> 
			myManager.schedule(random(80, 100)) {spawnPatrolThree()}
			spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE
		
			patrolLeader3HateList = event.actor.getHated()
			patrolLeader3HateList.each() { //Run this for each person on hate list at time of death
				if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
					it.grantItem( "100395" ) //Spear Head
				} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
					it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
				}
			}
		}
		
		if(patrolGroup3A == null || patrolGroup3A.isDead()) {
			patrolGroup3A = patrolGroupThree.forceSpawnNow()
			
			runOnDeath(patrolGroup3A) { event -> 
				spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE

				patrolGroup3AHateList = event.actor.getHated()
				patrolGroup3AHateList.each() { //Run this for each person on hate list at time of death
					if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
						it.grantItem( "100395" ) //Spear Head
					} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
						it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
					}
				}
			}
		}
		
		if(patrolGroup3B == null || patrolGroup3B.isDead()) {
			patrolGroup3B = patrolGroupThree.forceSpawnNow()
			
			runOnDeath(patrolGroup3B) { event -> 
				spearRoll = random(100) //Just a roll, will be compared against SPEAR_PERCENTAGE

				patrolGroup3BHateList = event.actor.getHated()
				patrolGroup3BHateList.each() { //Run this for each person on hate list at time of death
					if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() <= 8.5 ) {
						it.grantItem( "100395" ) //Spear Head
					} else if( isPlayer(it) && spearRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3) ) && it.getConLevel() > 8.5 ) {
						it.centerPrint( "You must be CL 8.5 or lower to gather Spear Heads." )
					}
				}
			}
		}	
	}
}

spawnPatrolThree()