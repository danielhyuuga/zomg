import com.gaiaonline.mmo.battle.script.*;

//IDEAS
// - Do damage each pulse the players are in the ponds (if you can check against terrain type)
// - Create stacked trigger zones in each pool area to simulate "deeper water" and increase the ferociousness of the damage done by the "pirahnnas"
// - Each pulse that players take damage do a "say" bubble that pulls from a long list of random "ouch" phrases, like "Damn!", "Ow!", "Doh!", "Ouch!", "Quit it!", etc.

//Variable init
upperPad1 = null
upperPad2 = null
lowerPad1 = null
lowerPad2 = null
bigRightPad1 = null
bigRightPad2 = null
topSmallPad1 = null
bigMiddle1 = null
bigMiddle2 = null
wallGuard1 = null

FEATHER_PERCENTAGE = 55
SPEAR_PERCENTAGE = 55

//------------------------------------------
// Upper Room Coatls (ROOM 506)             
//------------------------------------------
upperPad = myRooms.OtRuins_506.spawnSpawner( "upperPad", "feathered_coatl", 2)
upperPad.stopSpawning()
upperPad.setPos( 400, 175 )
upperPad.setWanderBehaviorForChildren( 25, 100, 3, 7, 150)
upperPad.setWaitTime( 50, 70 )
upperPad.setMonsterLevelForChildren( 7.3 )

lowerPad = myRooms.OtRuins_506.spawnSpawner( "lowerPad", "feathered_coatl", 2)
lowerPad.stopSpawning()
lowerPad.setPos( 555, 610 )
lowerPad.setWanderBehaviorForChildren( 25, 100, 3, 7, 150)
lowerPad.setWaitTime( 50, 70 )
lowerPad.setMonsterLevelForChildren( 7.3 )

//------------------------------------------
// Middle Room Coatls (ROOM 606)            
//------------------------------------------
bigRightPad = myRooms.OtRuins_606.spawnSpawner( "bigRightPad", "feathered_coatl", 2)
bigRightPad.stopSpawning()
bigRightPad.setPos( 940, 490 )
bigRightPad.setWanderBehaviorForChildren( 25, 100, 3, 7, 150)
bigRightPad.setWaitTime( 50, 70 )
bigRightPad.setMonsterLevelForChildren( 7.2 )

topSmallPad = myRooms.OtRuins_606.spawnSpawner( "topSmallPad", "feathered_coatl", 1)
topSmallPad.stopSpawning()
topSmallPad.setPos( 840, 165 )
topSmallPad.setWanderBehaviorForChildren( 25, 100, 3, 7, 150)
topSmallPad.setWaitTime( 50, 70 )
topSmallPad.setMonsterLevelForChildren( 7.2 )

//------------------------------------------
// Lower Room Coatls (ROOM 706)             
//------------------------------------------
bigMiddle = myRooms.OtRuins_706.spawnSpawner( "bigMiddle", "feathered_coatl", 2)
bigMiddle.stopSpawning()
bigMiddle.setPos( 840, 165 )
bigMiddle.setWanderBehaviorForChildren( 25, 100, 3, 7, 150)
bigMiddle.setWaitTime( 50, 70 )
bigMiddle.setMonsterLevelForChildren( 7.2 )

//------------------------------------------
// Wall Guard                               
//------------------------------------------
wallGuard = myRooms.OtRuins_706.spawnSpawner( "wallGuard", "tiny_terror", 1)
wallGuard.stopSpawning()
wallGuard.setPos( 60, 200 )
wallGuard.setGuardPostForChildren( wallGuard, 145 )
wallGuard.setWaitTime( 50, 70 )
wallGuard.setMonsterLevelForChildren( 7.1 )

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================
def spawnUpperPad() {
	upperPad1 = upperPad.forceSpawnNow() //Spawn the coatl
	runOnDeath(upperPad1) { event -> //When the coatl dies
		if(upperPad2 == null || upperPad2.isDead()) {
			myManager.schedule(random(50, 70)) {spawnUpperPad()}
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListUpperPad1 = event.actor.getHated()
		hateListUpperPad1.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.9) {
				//println "#### Loot roll for ${it} successful ####"
				it.grantQuantityItem(100407, 1)
			}
		}
	}
	
	upperPad2 = upperPad.forceSpawnNow() //Spawn the coatl
	runOnDeath(upperPad2) { event -> //When the coatl dies
		if(upperPad1 == null || upperPad1.isDead()) {
			myManager.schedule(random(50, 70)) {spawnUpperPad()}
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListUpperPad2 = event.actor.getHated()
		hateListUpperPad2.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.9) {
				//println "#### Loot roll for ${it} successful ####"
				it.grantQuantityItem(100407, 1)
			}
		}
	}
}

spawnUpperPad()

isSpawningLowerPad = false
def synchronized spawnLowerPadLater() {
	if( isSpawningLowerPad == false )
	{
		isSpawningLowerPad = true;
		myManager.schedule(random(50, 70)) {
			spawnLowerPad()
			isSpawningLowerPad = false
		}
	}
}

def spawnLowerPad() {
	lowerPad1 = lowerPad.forceSpawnNow() //Spawn the coatl
	runOnDeath(lowerPad1) { event -> //When the coatl dies
		if(lowerPad2 == null || lowerPad2.isDead()) {
			spawnLowerPadLater()
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListLowerPad1 = event.actor.getHated()
		hateListLowerPad1.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.9) {
				//println "#### Loot roll for ${it} successful ####"
				it.grantQuantityItem(100407, 1)
			}
		}
	}
	
	lowerPad2 = lowerPad.forceSpawnNow() //Spawn the coatl
	runOnDeath(lowerPad2) { event -> //When the coatl dies
		if(lowerPad1 == null || lowerPad1.isDead()) {
			spawnLowerPadLater()
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListLowerPad2 = event.actor.getHated()
		hateListLowerPad2.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.9) {
				//println "#### Loot roll for ${it} successful ####"
				it.grantQuantityItem(100407, 1)
			}
		}
	}
}

spawnLowerPad()

def spawnBigRightPad() {
	if( bigRightPad.spawnsInUse() < 2 ) {
		if( bigRightPad.spawnsInUse() % 2 == 0 ) {
			bigRightPad.setHomeForChildren( "OtRuins_606", 840, 430 )
		} else {
			bigRightPad.setHomeForChildren( "OtRuins_606", 530, 560 )
		}
		rightPad = bigRightPad.forceSpawnNow()
		runOnDeath(rightPad, { event -> 
			//Just a roll, will be compared against players
			featherRoll = random(100) 

			hateListBigRightPad1 = event.actor.getHated()
			hateListBigRightPad1.each() {
				if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.8) {
					//println "#### Loot roll for ${it} successful ####"
					it.grantQuantityItem(100407, 1)
				}
			}
		} )
		myManager.schedule( random( 25, 35 ) ) { spawnBigRightPad() }
	}	
}

spawnBigRightPad()

def spawnTopSmallPad() {
	topSmallPad1 = topSmallPad.forceSpawnNow() //Spawn the coatl
	runOnDeath(topSmallPad1) { event -> //When the coatl dies
		myManager.schedule(random(50, 70)) {spawnTopSmallPad()}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListTopSmallPad1 = event.actor.getHated()
		hateListTopSmallPad1.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.8) {
				//println "#### Loot roll for ${it} successful ####"
				it.grantQuantityItem(100407, 1)
			}
		}
	}
}

spawnTopSmallPad()

def spawnBigMiddle() {
	bigMiddle1 = bigMiddle.forceSpawnNow() //Spawn the coatl
	runOnDeath(bigMiddle1) { event -> //When the coatl dies
		if(bigMiddle2 == null || bigMiddle2.isDead()) {
			myManager.schedule(random(50, 70)) {spawnBigMiddle()}
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListBigMiddle1 = event.actor.getHated()
		hateListBigMiddle1.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.8) {
				//println "#### Loot roll for ${it} successful ####"
				it.grantQuantityItem(100407, 1)
			}
		}
	}
	
	bigMiddle2 = bigMiddle.forceSpawnNow() //Spawn the coatl
	runOnDeath(bigMiddle2) { event -> //When the coatl dies
		if(bigMiddle1 == null || bigMiddle1.isDead()) {
			myManager.schedule(random(50, 70)) {spawnBigMiddle()}
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListBigMiddle2 = event.actor.getHated()
		hateListBigMiddle2.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.8) {
				//println "#### Loot roll for ${it} successful ####"
				it.grantQuantityItem(100407, 1)
			}
		}
	}
}

spawnBigMiddle()

def spawnWallGuard() {
	wallGuard1 = wallGuard.forceSpawnNow() //Spawn the coatl
	runOnDeath(wallGuard1) { event -> //When the coatl dies
		myManager.schedule(random(50, 70)) {spawnWallGuard()}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListWallGuard1 = event.actor.getHated()
		hateListWallGuard1.each() {
			//println "#### Rolling for loot for ${it} ####"
			if(isPlayer(it) && featherRoll <= SPEAR_PERCENTAGE && (it.isOnQuest(308, 2) || it.isOnQuest(309, 2) || it.isOnQuest(102, 3)) && it.getConLevel() < 9.0) {
				it.grantQuantityItem(100395, 1)
			}
		}
	}
}

spawnWallGuard()