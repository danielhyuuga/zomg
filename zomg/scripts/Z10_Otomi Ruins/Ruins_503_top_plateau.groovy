import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Vases                                    
//------------------------------------------

def VaseA = myRooms.OtRuins_503.spawnSpawner( "VaseA", "bladed_statue", 1 )
VaseA.setPos( 215, 300 )
VaseA.setWaitTime( 50, 70 )
VaseA.setGuardPostForChildren( VaseA, 90 )
VaseA.addPatrolPointForChildren( "OtRuins_503", 450, 50, 2 )
VaseA.addPatrolPointForChildren( "OtRuins_503", 500, 320, 2 )
VaseA.setMonsterLevelForChildren( 7.6 )

def VaseB = myRooms.OtRuins_503.spawnSpawner( "VaseB", "bladed_statue", 1 )
VaseB.setPos( 540, 405 )
VaseB.setWaitTime( 50, 70 )
VaseB.setGuardPostForChildren( VaseB, 135 )
VaseB.addPatrolPointForChildren( "OtRuins_503", 545, 420, 2 )
VaseB.addPatrolPointForChildren( "OtRuins_503", 790, 490, 2 )
VaseB.setMonsterLevelForChildren( 7.6 )

//------------------------------------------
// Tiny Terror Wanderers                    
//------------------------------------------

def tinyWanderers503 = myRooms.OtRuins_503.spawnSpawner( "tinyWanderers503", "tiny_terror", 4 )
tinyWanderers503.setPos( 720, 240 )
tinyWanderers503.setWaitTime( 50, 70 )
tinyWanderers503.setWanderBehaviorForChildren( 50, 150, 3, 7, 200)
tinyWanderers503.setHomeTetherForChildren( 2000 )
tinyWanderers503.setHateRadiusForChildren( 2000 )
tinyWanderers503.setMonsterLevelForChildren( 7.6 )


//------------------------------------------
// Tiny Patroller                           
//------------------------------------------

def tinyPatroller503 = myRooms.OtRuins_503.spawnSpawner( "tinyPatroller503", "tiny_terror", 1 )
tinyPatroller503.setPos( 725, 240 )
tinyPatroller503.setWaitTime( 60, 90 )
tinyPatroller503.setHome( tinyWanderers503 )
tinyPatroller503.setRFH( true )
tinyPatroller503.setCFH( 300 )
tinyPatroller503.setHomeTetherForChildren( 2000 )
tinyPatroller503.setHateRadiusForChildren( 2000 )
tinyPatroller503.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
tinyPatroller503.setCowardLevelForChildren( 100 )
tinyPatroller503.addPatrolPointForChildren( "OtRuins_503", 730, 240, 10 )
tinyPatroller503.addPatrolPointForChildren( "OtRuins_603", 460, 170, 1 )
tinyPatroller503.setMonsterLevelForChildren( 7.6 )


//==========================
// ALLIANCES                
//==========================
tinyWanderers503.allyWithSpawner( VaseA )
tinyWanderers503.allyWithSpawner( VaseB )

tinyPatroller503.allyWithSpawner( tinyWanderers503 )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

tinyWanderers503.spawnAllNow()
tinyPatroller503.forceSpawnNow()
VaseA.forceSpawnNow()
VaseB.forceSpawnNow()