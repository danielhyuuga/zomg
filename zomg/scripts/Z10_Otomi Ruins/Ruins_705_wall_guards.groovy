import com.gaiaonline.mmo.battle.script.*;


//------------------------------------------
// Tiny Terror Wall Guards (Room 705)       
//------------------------------------------

def wallFacingInside01 = myRooms.OtRuins_705.spawnSpawner( "wallFacingInside01", "tiny_terror", 1)
wallFacingInside01.setPos( 195, 350 )
wallFacingInside01.setGuardPostForChildren( wallFacingInside01, 225 )
wallFacingInside01.setWaitTime( 50, 70 )
wallFacingInside01.setMonsterLevelForChildren( 7.2 )

def wallFacingInside02 = myRooms.OtRuins_705.spawnSpawner( "wallFacingInside02", "tiny_terror", 1)
wallFacingInside02.setPos( 515, 140 )
wallFacingInside02.setGuardPostForChildren( wallFacingInside02, 225 )
wallFacingInside02.setWaitTime( 50, 70 )
wallFacingInside02.setMonsterLevelForChildren( 7.2 )

def wallFacingOutside01 = myRooms.OtRuins_705.spawnSpawner( "wallFacingOutside01", "tiny_terror", 1)
wallFacingOutside01.setPos( 270, 300 )
wallFacingOutside01.setGuardPostForChildren( wallFacingOutside01, 45 )
wallFacingOutside01.setWaitTime( 50, 70 )
wallFacingOutside01.setMonsterLevelForChildren( 7.2 )

def wallFacingOutside02 = myRooms.OtRuins_705.spawnSpawner( "wallFacingOutside02", "tiny_terror", 1)
wallFacingOutside02.setPos( 595, 60 )
wallFacingOutside02.setGuardPostForChildren( wallFacingOutside02, 45 )
wallFacingOutside02.setWaitTime( 50, 70 )
wallFacingOutside02.setMonsterLevelForChildren( 7.2 )

def wallFacingOutside03 = myRooms.OtRuins_705.spawnSpawner( "wallFacingOutside03", "tiny_terror", 1)
wallFacingOutside03.setPos( 910, 140 )
wallFacingOutside03.setGuardPostForChildren( wallFacingOutside03, 45 )
wallFacingOutside03.setWaitTime( 50, 70 )
wallFacingOutside03.setMonsterLevelForChildren( 7.2 )


//==========================
// ALLIANCES                
//==========================

wallFacingInside01.allyWithSpawner( wallFacingInside02 )
wallFacingInside01.allyWithSpawner( wallFacingOutside01 )
wallFacingInside01.allyWithSpawner( wallFacingOutside02 )
wallFacingInside01.allyWithSpawner( wallFacingOutside03 )
wallFacingInside02.allyWithSpawner( wallFacingOutside01 )
wallFacingInside02.allyWithSpawner( wallFacingOutside02 )
wallFacingInside02.allyWithSpawner( wallFacingOutside03 )
wallFacingOutside01.allyWithSpawner( wallFacingOutside02 )
wallFacingOutside01.allyWithSpawner( wallFacingOutside03 )
wallFacingOutside02.allyWithSpawner( wallFacingOutside03 )

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

wallFacingInside01.forceSpawnNow()
wallFacingInside02.forceSpawnNow()
wallFacingOutside01.forceSpawnNow()
wallFacingOutside02.forceSpawnNow()
wallFacingOutside03.forceSpawnNow()
