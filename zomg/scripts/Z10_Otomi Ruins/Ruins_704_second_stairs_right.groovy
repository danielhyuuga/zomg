import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Mask Spawner                             
//------------------------------------------

maskSpawner = myRooms.OtRuins_704.spawnSpawner( "maskSpawner", "mask", 2 )
maskSpawner.setPos( 300, 460)
maskSpawner.setWaitTime( 1, 2 )
maskSpawner.setWanderBehaviorForChildren( 50, 200, 2, 5, 400)
maskSpawner.setInitialMoveForChildren( "OtRuins_704", 730, 360 )
maskSpawner.setMonsterLevelForChildren( 7.3 )

//------------------------------------------
// Tiny Terror Guards                       
//------------------------------------------

tinyAmbushGuard = myRooms.OtRuins_704.spawnSpawner( "tinyAmbushGuard", "tiny_terror", 12 )
tinyAmbushGuard.setPos( 480, 30 )
tinyAmbushGuard.setWaitTime( 5, 10 )
tinyAmbushGuard.setHome( "OtRuins_704", 750, 390 )
tinyAmbushGuard.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
tinyAmbushGuard.setMonsterLevelForChildren( 7.3 )

tinyWallGuardA = myRooms.OtRuins_704.spawnSpawner( "tinyWallGuardA", "tiny_terror", 1 )
tinyWallGuardA.setPos( 750, 640 )
tinyWallGuardA.setWaitTime( 5, 10 )
tinyWallGuardA.setGuardPostForChildren( tinyWallGuardA, 225 )
tinyWallGuardA.setMonsterLevelForChildren( 7.3 )

tinyWallGuardB = myRooms.OtRuins_704.spawnSpawner( "tinyWallGuardB", "tiny_terror", 1 )
tinyWallGuardB.setPos( 940, 530 )
tinyWallGuardB.setWaitTime( 5, 10 )
tinyWallGuardB.setGuardPostForChildren( tinyWallGuardB, 225 )
tinyWallGuardB.setMonsterLevelForChildren( 7.3 )


playerList704 = []

//=========================
// SPAWNCOUNT MANAGERS     
//=========================

myManager.onEnter(myRooms.OtRuins_704) { event ->
	if( isPlayer(event.actor) ) {
		playerList704 << event.actor
	}
}

myManager.onExit(myRooms.OtRuins_704) { event ->
	if ( isPlayer(event.actor) ) {
		playerList704.remove( event.actor )
	}
}

//------------------------------------------
// Respawner Scripts (Ambush Strategies)    
//------------------------------------------

//Objectives:
//	Once a Mask is spawned, all objects in the room should pick targets to hate.
//	Room should have to be cleared and empty before respawns should occur.
//		Trigger zone at base of stairs starts the ambush
// 		Number in ambush dependent on number of players in the room at the time

roomIsReset = true
respawnConditionMet = false

TTList = []
MaskList = []

def ambushTrigger704 = "ambushTrigger704"
myRooms.OtRuins_704.createTriggerZone( ambushTrigger704, 500, 200, 760, 355 )

myManager.onTriggerIn(myRooms.OtRuins_704, ambushTrigger704) { event ->
	if( isPlayer( event.actor ) && roomIsReset == true && event.actor.getConLevel() <= 8.5 ) {
		roomIsReset = false
		playersIn704 = playerList704.size()
		
		if( playersIn704 > 6 ) {
			playersIn704 = 6
		}
		
		//want twice as many TTs as players
		numSpawnTT = ((playersIn704 * 1.5)-2).intValue() //subtract two because there are already two wall guards
		
		//one Mask for every three players, rounded up
		numSpawnMask = ( ( playersIn704 / 3) ).intValue()
		if( ( playersIn704 % 3 ) != 0 ) { numSpawnMask ++ }
		
		spawnTTAmbushers()
		spawnMaskAmbushers()
		
		//Make the wall guards hate someone if they don't already
		if( !wall1.isDead() ) {
			if( !playerList704.isEmpty() ) { wall1.addHate( random( playerList704 ), 1 ) }
		}
		if( !wall2.isDead() ) {
			if( !playerList704.isEmpty() ) { wall2.addHate( random( playerList704 ), 1 ) }
		}
	}
}

//generate one Tiny Terror per second until numSpawnTT is zero
def spawnTTAmbushers() {
	if( numSpawnTT > 0 ) {
		TTList << tinyAmbushGuard.forceSpawnNow() //This list starts at position 0, so subtract one from the list size to get the correct list position
		TTList.get( TTList.size() - 1 ).addHate( random ( playerList704 ), 1 ) //Get the ID of the TT that was just spawned and add hate to a random player in the room
		runOnDeath( TTList.get( TTList.size() - 1 ) ) { TTList.remove( TTList.get( TTList.size() - 1 ) ); checkForReset() } //runOnDeath for that just-spawned TT removes the dead Tiny Terror from the list AND THEN checks for the camp reset
		numSpawnTT -- 
		myManager.schedule(1) { spawnTTAmbushers() } 
	}
}

def spawnMaskAmbushers() {
	if( numSpawnMask > 0 ) {
		MaskList << maskSpawner.forceSpawnNow() 
		MaskList.get( MaskList.size() - 1 ).addHate( random ( playerList704 ), 1 ) 
		runOnDeath( MaskList.get( MaskList.size() - 1 ) ) { MaskList.remove( MaskList.get( MaskList.size() - 1 ) ); checkForReset() }
		numSpawnMask -- 
		myManager.schedule(1) { spawnMaskAmbushers() } 
	}
}
		
def synchronized checkForReset() {
	//Check the TTList. If it's not zero, then they're not all dead yet.
	//if all mobs are detroyed then reset the flags for the ambush to occur again
	if( respawnConditionMet == false && TTList.isEmpty() && MaskList.isEmpty() ) {
		respawnConditionMet = true
		myManager.schedule(60) {
			spawnTTWallGuards()
			TTList = []
			MaskList = []
			roomIsReset = true
			respawnConditionMet = false
		}
	}
}

def spawnTTWallGuards() {
	if( tinyWallGuardA.spawnsInUse() == 0 ) {
		wall1 = tinyWallGuardA.forceSpawnNow()
	}
	if( tinyWallGuardB.spawnsInUse() == 0 ) {
		wall2 = tinyWallGuardB.forceSpawnNow()
	}
}

//==========================
// ALLIANCES                
//==========================
tinyWallGuardA.allyWithSpawner( tinyWallGuardB )
tinyAmbushGuard.allyWithSpawner( tinyWallGuardA )
tinyAmbushGuard.allyWithSpawner( tinyWallGuardB )

maskSpawner.allyWithSpawner( tinyWallGuardA )
maskSpawner.allyWithSpawner( tinyWallGuardB )
maskSpawner.allyWithSpawner( tinyAmbushGuard )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

maskSpawner.stopSpawning()
tinyAmbushGuard.stopSpawning()

wall1 = tinyWallGuardA.forceSpawnNow()
wall2 = tinyWallGuardB.forceSpawnNow()

tinyWallGuardA.stopSpawning()
tinyWallGuardB.stopSpawning()