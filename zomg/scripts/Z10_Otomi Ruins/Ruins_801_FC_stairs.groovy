import com.gaiaonline.mmo.battle.script.*;

//Variable init

FEATHER_PERCENTAGE = 55

//------------------------------------------
// Feathered Coatl Stair Wanderers          
//------------------------------------------

lowerSpawner = myRooms.OtRuins_801.spawnSpawner( "lowerSpawner", "feathered_coatl", 3)
lowerSpawner.setPos( 355, 535 )
lowerSpawner.setWanderBehaviorForChildren( 25, 125, 2, 5, 200 )
lowerSpawner.setMonsterLevelForChildren( 7.3 )
lowerSpawner.stopSpawning()

upperSpawner = myRooms.OtRuins_801.spawnSpawner( "upperSpawner", "feathered_coatl", 2)
upperSpawner.setPos( 400, 125 )
upperSpawner.setWanderBehaviorForChildren( 25, 100, 2, 5, 150 )
upperSpawner.setMonsterLevelForChildren( 7.3 )
upperSpawner.stopSpawning()

//============================
// RESPAWN AND REWARD LOGIC   
//============================

def synchronized spawnLowerSpawner() {
	//Spawn the coatl
	lower = lowerSpawner.forceSpawnNow() 
	//When the coatl dies
	runOnDeath( lower, { event -> 
		//Award feathers for Blaze's quest
		featherRoll = random(100)
		event.actor.getHated().each() {
			if( isPlayer( it ) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.9 ) {
				it.grantQuantityItem( 100407, 1 )
			}
		}
		
		//now fill in the missing coatl 50-70 seconds later
		myManager.schedule( random( 50, 70 ) ) { spawnLowerSpawner() }
	} )
}
	


def synchronized spawnUpperSpawner() {
	//Spawn the coatl
	upper = upperSpawner.forceSpawnNow() 
	//When the coatl dies
	runOnDeath( upper, { event -> 
		//Award feathers for Blaze's quest
		featherRoll = random(100)
		event.actor.getHated().each() {
			if( isPlayer( it ) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.9 ) {
				it.grantQuantityItem( 100407, 1 )
			}
		}
		
		//now fill in the missing coatl 50-70 seconds later
		myManager.schedule( random( 50, 70 ) ) { spawnUpperSpawner() }
	} )
}

//============================
// INITIAL LOGIC              
//============================

spawnLowerSpawner()
myManager.schedule(1) { spawnLowerSpawner() }
myManager.schedule(2) { spawnLowerSpawner() }

spawnUpperSpawner()
myManager.schedule(1) { spawnUpperSpawner() }