import com.gaiaonline.mmo.battle.script.*;

def Blaze = spawnNPC("Blaze-VQS", myRooms.OtRuins_806, 550, 430)
Blaze.setDisplayName( "Blaze" )

Blaze.setRotation( 45 )

myManager.onEnter( myRooms.OtRuins_806 ) { event ->
	//**** Screwed-up Player Data resolution ****
	if( isPlayer( event.actor ) ) {
		if( event.actor.isDoneQuest(107) && !event.actor.hasQuestFlag( GLOBAL, "Z10BlazeRingGrantOkay" ) && !event.actor.hasQuestFlag( GLOBAL, "Z10BlazeRingGrantReceived" ) ) {
			event.actor.setQuestFlag(GLOBAL, "Z10BlazeRingGrantOkay")
			Blaze.pushDialog( event.actor, "ringGrant" )
		}
		if((event.actor.isOnQuest(107) || event.actor.isDoneQuest(107)) && event.actor.hasQuestFlag(GLOBAL, "Z10_Blaze_RinAndLin_Break")) {
			event.actor.unsetQuestFlag(GLOBAL, "Z10_Blaze_RinAndLin_Break")
		}
		event.actor.unsetQuestFlag( GLOBAL, "Z10RingRewardInProgress" )
	}
}

def bloodTree = "bloodTree"
myRooms.OtRuins_106.createTriggerZone(bloodTree, 250, 10, 640, 250)
myManager.onTriggerIn(myRooms.OtRuins_106, bloodTree) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(105, 2)) {
		event.actor.updateQuest( 105, "Blaze-VQS" )
		event.actor.centerPrint("You have successfully gone around the blood tree!")
		event.actor.removeMiniMapQuestLocation( "The Blood Tree" )
		event.actor.addMiniMapQuestActorName("Blaze-VQS")
	}
}

onQuestStep( 107, 2 ) { event -> event.player.addMiniMapQuestActorName( "Blaze-VQS" ) }
onQuestStep( 305, 2 ) { event -> event.player.addMiniMapQuestActorName("Blaze-VQS") }

//----------------------------------------------------------------------------------
//Blaze Default                                                                     
//----------------------------------------------------------------------------------

def blazeDefault = Blaze.createConversation("blazeDefault", true, "!QuestStarted_105", "!QuestCompleted_105")

def default1 = [id:1]
default1.npctext = "Hello, %p, I've been expecting you."
default1.playertext = "Expecting me? Did you hear I was looking for Marshall?"
default1.result = 2
blazeDefault.addDialog(default1, Blaze)

def default2 = [id:2]
default2.npctext = "What? Marshall? What a strange coincidence... Anyway, no, that isn't why I knew you were coming."
default2.playertext = "Then how did you know?"
default2.result = 3
blazeDefault.addDialog(default2, Blaze)

def default3 = [id:3]
default3.npctext = "Hang on a minute... I'll answer that in time, I'll even tell you how to find Marshall too, but first you have to do something."
default3.playertext = "Uhg, everyone is always bartering! Okay, what can I do for you?"
default3.result = 4
blazeDefault.addDialog(default3, Blaze)

def default4 = [id:4]
default4.npctext = "Honestly, I was expecting someone... a bit more able bodied. I need to confirm that you're actually the right person so I don't waste my friends' time."
default4.playertext = "Why? I'm sure I can handle anything I'd come across."
default4.result = 5
blazeDefault.addDialog(default4, Blaze)

def default5 = [id:5]
default5.npctext = "Perhaps. If you are the one I am looking for then I have no doubt you are quite capable, but I have no way of knowing for sure until you prove it."
default5.playertext = "Prove it how? You want me to just run around in the ruins or did you have something specific in mind?"
default5.result = 6
blazeDefault.addDialog(default5, Blaze)

def default6 = [id:6]
default6.npctext = "Head north across the lake and around the blood tree. If you make it back I will tell you how I knew you would arrive..."
default6.playertext = "Back in a flash, and I expect some answers!"
default6.quest = 105
default6.exec = { event ->
	event.player.addMiniMapQuestLocation( "The Blood Tree", "OtRuins_106", 445, 130, "The Blood Tree" )
}
default6.result = DONE
blazeDefault.addDialog(default6, Blaze)

//----------------------------------------------------------------------------------
//Blaze Blood Tree Active                                                           
//----------------------------------------------------------------------------------
def bloodTreeActive = Blaze.createConversation("bloodTreeActive", true, "QuestStarted_105:2")

def bloodTreeActive1 = [id:1]
bloodTreeActive1.npctext = "I see you're still hiding in the trees. Lost your nerve?"
bloodTreeActive1.playertext = "I'm getting ready to go!"
bloodTreeActive1.result = 2
bloodTreeActive.addDialog(bloodTreeActive1, Blaze)

def bloodTreeActive2 = [id:2]
bloodTreeActive2.npctext = "Uh huh."
bloodTreeActive2.result = DONE
bloodTreeActive.addDialog(bloodTreeActive2, Blaze)

//----------------------------------------------------------------------------------
//Blaze Blood Tree Complete                                                         
//----------------------------------------------------------------------------------
def bloodTreeComplete = Blaze.createConversation("bloodTreeComplete", true, "QuestStarted_105:3")

def bloodTreeComplete1 = [id:1]
bloodTreeComplete1.npctext = "You survived, I am impressed."
bloodTreeComplete1.options = []
bloodTreeComplete1.options << [text:"Don't act so surprised, I told you I could handle myself", result: 2]
bloodTreeComplete1.options << [text:"Yeah, barely though! You weren't kidding, it's rough out there.", result: 3]
bloodTreeComplete.addDialog(bloodTreeComplete1, Blaze)

def bloodTreeComplete2 = [id:2]
bloodTreeComplete2.npctext = "Apparently you can... I'm still not sure why the twins want to talk to you, though."
bloodTreeComplete2.playertext = "Twins? Huh?"
bloodTreeComplete2.result = 4
bloodTreeComplete.addDialog(bloodTreeComplete2, Blaze)

def bloodTreeComplete3 = [id:3]
bloodTreeComplete3.npctext = "It's not *that* rough... I mean I make the run for fun. It shouldn't have been hard for someone the twins seem to think is so important."
bloodTreeComplete3.playertext = "Twins? What twins?"
bloodTreeComplete3.result = 4
bloodTreeComplete.addDialog(bloodTreeComplete3, Blaze)

def bloodTreeComplete4 = [id:4]
bloodTreeComplete4.npctext = "The Otami twins, Rin and Lin, their ancestors used to live here. One day I was exploring the cliffs to the west and they just sort of appeared. I agreed to arrange a meeting between the three of you in exchange for their knowledge of the ruins."
bloodTreeComplete4.options = []
bloodTreeComplete4.options << [text:"Uhm... what do you mean they just appeared?", result: 5]
bloodTreeComplete4.options << [text:"No offense, but this is starting to sound a little crazy. How did they even know I would be here?", result: 6]
bloodTreeComplete.addDialog(bloodTreeComplete4, Blaze)

def bloodTreeComplete5 = [id:5]
bloodTreeComplete5.npctext = "Just what I said... at first I just heard their voices and then there they were, right in front of me. I tried to talk to them but they seemed pretty preoccupied... they've been expecting you for a while now."
bloodTreeComplete5.playertext = "How can you be sure it's me they want?"
bloodTreeComplete5.result = 7
bloodTreeComplete.addDialog(bloodTreeComplete5, Blaze)

def bloodTreeComplete6 = [id:6]
bloodTreeComplete6.npctext = "Well, maybe they can explain it better. All I know is they said a traveller would be arriving soon and a few days later you arrived. Apparently they think you're very important."
bloodTreeComplete6.playertext = "How can you be sure it's me they want?"
bloodTreeComplete6.result = 7
bloodTreeComplete.addDialog(bloodTreeComplete6, Blaze)

def bloodTreeComplete7 = [id:7]
bloodTreeComplete7.npctext = "Heh, let's just say the ruins don't exactly get a lot of visitors. I understand your confusion... I'm surprised too. I was expecting a god... or a giant... or at least someone remarkable. You look so... normal."
bloodTreeComplete7.options = []
bloodTreeComplete7.options << [text:"Hey, what's wrong with normal?", result: 8]
bloodTreeComplete7.options << [text:"I may look normal, but I assure you I am not.", result: 9]
bloodTreeComplete7.options << [text:"Yeah, I dunno about this. I guess it doesn't hurt to see what they want.", result: 10]
bloodTreeComplete.addDialog(bloodTreeComplete7, Blaze)

def bloodTreeComplete8 = [id:8]
bloodTreeComplete8.npctext = "I was just expecting, well, more. Why would Rin and Lin be so interested in you?"
bloodTreeComplete8.options = []
bloodTreeComplete8.options << [text:"You sound a little jealous.", result: 11]
bloodTreeComplete8.options << [text:"Don't worry, Blaze. I'm sure they'll talk to you once they're done with me.", result: 12]
bloodTreeComplete.addDialog(bloodTreeComplete8, Blaze)

def bloodTreeComplete9 = [id:9]
bloodTreeComplete9.npctext = "I guess so. I wish I knew what you had that I didn't."
bloodTreeComplete9.options = []
bloodTreeComplete9.options << [text:"You sound a little jealous.", result: 11]
bloodTreeComplete9.options << [text:"Don't worry, Blaze. I'm sure they'll talk to you once they're done with me.", result: 12]
bloodTreeComplete.addDialog(bloodTreeComplete9, Blaze)

def bloodTreeComplete10 = [id:10]
bloodTreeComplete10.npctext = "Yeah, I guess only they can tell you..."
bloodTreeComplete10.playertext = "Where can I find them?"
bloodTreeComplete10.result = 13
bloodTreeComplete.addDialog(bloodTreeComplete10, Blaze)

def bloodTreeComplete11 = [id:11]
bloodTreeComplete11.npctext = "Hrmph! It just isn't fair. I've been here for weeks trying to learn about the ruins and the only people who can answer my questions won't talk to me until after you go see them."
bloodTreeComplete11.playertext = "Alright, I'll go see them. Where are they?"
bloodTreeComplete11.result = 13
bloodTreeComplete.addDialog(bloodTreeComplete11, Blaze)

def bloodTreeComplete12 = [id:12]
bloodTreeComplete12.npctext = "You think? I hope so! They're so cool and I have so many questions about the ruins. You won't take too long, will you? I'm getting kind of bored just standing around here."
bloodTreeComplete12.playertext = "I'll try to hurry... where can I find Rin and Lin?"
bloodTreeComplete12.result = 13
bloodTreeComplete.addDialog(bloodTreeComplete12, Blaze)

def bloodTreeComplete13 = [id:13]
bloodTreeComplete13.npctext = "They're on the sacred cliffs to the west. You'll have to go through the cave to get up there."
bloodTreeComplete13.options = []
bloodTreeComplete13.options << [text:"Off I go! Wish me luck.", result: 14]
bloodTreeComplete13.options << [text:"Cave? No thanks! I'm afraid of the dark.", result: 15]
bloodTreeComplete13.options << [text:"Hmm... that sounds like a bit of a trek. I'll need a little time to get ready.", result: 16]
bloodTreeComplete.addDialog(bloodTreeComplete13, Blaze)

def bloodTreeComplete14 = [id:14]
bloodTreeComplete14.npctext = "Good luck, chosen one. *groan*"
bloodTreeComplete14.quest = 107
bloodTreeComplete14.exec = { event -> 
	event.player.updateQuest(105, "Blaze-VQS") 
	event.player.removeMiniMapQuestActorName("Blaze-VQS")
}
bloodTreeComplete14.result = DONE
bloodTreeComplete.addDialog(bloodTreeComplete14, Blaze)

def bloodTreeComplete15 = [id:15]
bloodTreeComplete15.npctext = "What?! The chosen one is afraid of the dark? Maybe Rin and Lin will let me fill in for you."
bloodTreeComplete15.flag = "Z10_Blaze_RinAndLin_Break"
bloodTreeComplete15.exec = { event ->
	event.player.removeMiniMapQuestActorName("Blaze-VQS")
}
bloodTreeComplete15.quest = 105
bloodTreeComplete15.result = DONE
bloodTreeComplete.addDialog(bloodTreeComplete15, Blaze)

def bloodTreeComplete16 = [id:16]
bloodTreeComplete16.npctext = "Don't take too long, I still need to talk to Rin and Lin when you are a done!!!"
bloodTreeComplete16.flag = "Z10_Blaze_RinAndLin_Break"
bloodTreeComplete16.exec = { event ->
	event.player.removeMiniMapQuestActorName("Blaze-VQS")
}
bloodTreeComplete16.quest = 105
bloodTreeComplete16.result = DONE
bloodTreeComplete.addDialog(bloodTreeComplete16, Blaze)

//----------------------------------------------------------------------------------
//Rin and Lin Break                                                                 
//----------------------------------------------------------------------------------
def rinLinBreak = Blaze.createConversation("rinLinBreak", true, "Z10_Blaze_RinAndLin_Break")

def rinLinBreak1 = [id:1]
rinLinBreak1.npctext = "I hope you changed your mind about talking to Rin and Lin... they still won't talk to me!"
rinLinBreak1.options = []
rinLinBreak1.options << [text:"Yeah, I think I'll go talk to them now. Where are they again?", result: 2]
rinLinBreak1.options << [text:"They're still waiting? Yeesh. They can't take a hint.", result: 3]
rinLinBreak.addDialog(rinLinBreak1, Blaze)

def rinLinBreak2 = [id:2]
rinLinBreak2.npctext = "They're on the sacred cliffs to the west. Use the cave at the southern base to get to the top."
rinLinBreak2.playertext = "Great, wish me luck!"
rinLinBreak2.result = 4
rinLinBreak.addDialog(rinLinBreak2, Blaze)

def rinLinBreak3 = [id:3]
rinLinBreak3.npctext = "Waaaaaaaah! If you don't talk to them they aren't gonna talk to me and then I can't get them to answer anything. Please go talk to them!"
rinLinBreak3.playertext = "I'll think about it."
rinLinBreak3.result = DONE
rinLinBreak.addDialog(rinLinBreak3, Blaze)

def rinLinBreak4 = [id:4]
rinLinBreak4.npctext = "Good luck, chosen one. *groan*"
rinLinBreak4.flag = "!Z10_Blaze_RinAndLin_Break"
rinLinBreak4.quest = 107
rinLinBreak4.result = DONE
rinLinBreak.addDialog(rinLinBreak4, Blaze)

//----------------------------------------------------------------------------------
//Rin and Lin Active                                                                
//----------------------------------------------------------------------------------
def rinLinActive = Blaze.createConversation("rinLinActive", true, "QuestStarted_107:2")

def rinLinActive1 = [id:1]
rinLinActive1.npctext = "I thought you were off to talk with Rin and Lin..."
rinLinActive1.playertext = "I'm getting to it!"
rinLinActive1.result = DONE
rinLinActive.addDialog(rinLinActive1, Blaze)

//----------------------------------------------------------------------------------
//Rin and Lin Complete                                                              
//----------------------------------------------------------------------------------
def rinLinComplete = Blaze.createConversation("rinLinComplete", true, "QuestStarted_107:3")
rinLinComplete.setUrgent(true)

def rinLinComplete1 = [id:1]
rinLinComplete1.npctext = "You helped out Rin and Lin, they said they'd answer any questions I have! Thanks! Is there anything I can do for you?"
rinLinComplete1.playertext = "You said you'd tell me where to find Marshall... I think it's time you made good on that promise."
rinLinComplete1.result = 2
rinLinComplete.addDialog(rinLinComplete1, Blaze)

def rinLinComplete2 = [id:2]
rinLinComplete2.npctext = "I guess that's the fair thing to do before I run off to interrogate Rin and Lin. Anyway, I'm not sure exactly where he is..."
rinLinComplete2.options = []
rinLinComplete2.options << [text:"You've got to be kidding. I should have known you were trying to trick me.", result: 3]
rinLinComplete2.options << [text:"How were you planning to help me find him if you don't know where he is?", result: 4]
rinLinComplete.addDialog(rinLinComplete2, Blaze)

def rinLinComplete3 = [id:3]
rinLinComplete3.npctext = "Calm down, I didn't trick you. I told you I could help you find Marshall and I can. I know Marshall is out on a dive. He always checks in with Jacques at the lighthouse before heading out to sea. Jacques will know exactly where he is diving."
rinLinComplete3.playetext = "And he'll just tell me where Marshall is?"
rinLinComplete3.result = 5
rinLinComplete.addDialog(rinLinComplete3, Blaze)

def rinLinComplete4 = [id:4]
rinLinComplete4.npctext = "Hey, just because I don't know exactly where he is doesn't mean I don't know how to find him. He just went out on a dive and would have checked in with Jacques at the lighthouse before putting out to sea. Jacques will know where to find Marshall."
rinLinComplete4.playertext = "And Jacques is willing to tell me where Marshall is?"
rinLinComplete4.result = 5
rinLinComplete.addDialog(rinLinComplete4, Blaze)

def rinLinComplete5 = [id:5]
rinLinComplete5.npctext = "I'm sure he will... if he gives you any grief just tell him I said it was okay. I'm Marshall's assistant so if Jacques knows I'm okay with it he shouldn't have a problem at all."
rinLinComplete5.playertext = "Cool... now how do I get to the lighthouse?"
rinLinComplete5.result = 6
rinLinComplete.addDialog(rinLinComplete5, Blaze)

def rinLinComplete6 = [id:6]
rinLinComplete6.npctext = "Just head over the the beach and go north immediately. The lighthouse is at the end of the grassy area."
rinLinComplete6.result = 7
rinLinComplete.addDialog(rinLinComplete6, Blaze)

def rinLinComplete7 = [id:7]
rinLinComplete7.npctext = "Oh! And one more thing! Take one of these rings I've been collecting. I've got oodles of them and you probably want 'just one more', am I right?"
rinLinComplete7.exec = { event ->
	event.player.removeMiniMapQuestActorName("Blaze-VQS")
	event.player.updateQuest( 107, "Blaze-VQS" )
	event.player.updateQuest( 254, "Blaze-VQS" )
	event.player.setQuestFlag( GLOBAL, "Z10_Blaze_ReturnToJacques" )
	event.player.setQuestFlag( GLOBAL, "Z10BlazeRingGrantOkay" )
	Blaze.pushDialog( event.player, "ringGrant" )
}
rinLinComplete7.result = DONE
rinLinComplete.addDialog(rinLinComplete7, Blaze)

//----------------------------------------------------------------------------------
//Coatl Feather's Grant                                                             
//----------------------------------------------------------------------------------
def coatlFeatherGrant = Blaze.createConversation("coatlFeatherGrant", true, "QuestCompleted_107", "!QuestStarted_305", "!Z10_Blaze_CoatlFeather_Allowed", "!Z10_Blaze_CoatlFeather_AllowedAgain", "!Z10_Blaze_CoatlFeather_Denied", "Z10BlazeRingGrantReceived")

def coatlFeatherGrant1 = [id:1]
coatlFeatherGrant1.npctext = "Hey, you're back! Just in time, too. I need your help again."
coatlFeatherGrant1.options = []
coatlFeatherGrant1.options << [text:"Okay, Blaze, I'll bite. What's up?", result: 2]
coatlFeatherGrant1.options << [text:"I'm a little busy right now. Later, Blaze.", result: 3]
coatlFeatherGrant.addDialog(coatlFeatherGrant1, Blaze)

def coatlFeatherGrant2 = [id:2]
coatlFeatherGrant2.npctext = "Well, thanks to you Rin and Lin will finally talk to me! They've given me a task... but I seem to be having some trouble."
coatlFeatherGrant2.playertext = "Trouble? What kind of trouble?"
coatlFeatherGrant2.result = 4
coatlFeatherGrant.addDialog(coatlFeatherGrant2, Blaze)

def coatlFeatherGrant3 = [id:3]
coatlFeatherGrant3.npctext = "Later, %p."
coatlFeatherGrant3.result = DONE
coatlFeatherGrant.addDialog(coatlFeatherGrant3, Blaze)

def coatlFeatherGrant4 = [id:4]
coatlFeatherGrant4.npctext = "Well, they wanted me to get them some Tropical Bird Feathers, but I... well, I can't really beat the Coatls. Maybe you could, with your fancy rings? I have a few orbs I can give you if you're willing to help me."
coatlFeatherGrant4.options = []
coatlFeatherGrant4.options << [text:"Alright, I could use a few orbs. How many Tropical Bird Feathers did you need?", exec: { event ->
	if(event.player.getConLevel() < 8.6) {
		event.player.setQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Allowed")
		Blaze.pushDialog(event.player, "coatlFeatherAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Denied")
		Blaze.pushDialog(event.player, "coatlFeatherDenied")
	}
}, result: DONE]
coatlFeatherGrant4.options << [text:"Nah, not interested.", result: 3]
coatlFeatherGrant.addDialog(coatlFeatherGrant4, Blaze)

//----------------------------------------------------------------------------------
//Coatl Feather's Allowed                                                             
//----------------------------------------------------------------------------------
def coatlFeatherAllowed = Blaze.createConversation("coatlFeatherAllowed", true, "Z10_Blaze_CoatlFeather_Allowed")

def coatlFeatherAllowed1 = [id:1]
coatlFeatherAllowed1.npctext = "They didn't say how many they wanted, but fifteen sounds good to me. Shouldn't take you any time at all."
coatlFeatherAllowed1.quest = 305
coatlFeatherAllowed1.exec = { event ->
	event.player.updateQuest(305, "Blaze-VQS")
}
coatlFeatherAllowed1.flag = "!Z10_Blaze_CoatlFeather_Allowed"
coatlFeatherAllowed1.result = DONE
coatlFeatherAllowed.addDialog(coatlFeatherAllowed1, Blaze)

//----------------------------------------------------------------------------------
//Coatl Feather's Denied                                                            
//----------------------------------------------------------------------------------
def coatlFeatherDenied = Blaze.createConversation("coatlFeatherDenied", true, "Z10_Blaze_CoatlFeather_Denied")

def coatlFeatherDenied1 = [id:1]
coatlFeatherDenied1.npctext = "I think you're a little too tough for this work."
coatlFeatherDenied1.flag = "!Z10_Blaze_CoatlFeather_Denied"
coatlFeatherDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 8.5 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
	if(event.player.isOnQuest(305, 3)) {
		event.player.updateQuest(305, "Blaze-VQS")
	}
}
coatlFeatherDenied1.result = DONE
coatlFeatherDenied.addDialog(coatlFeatherDenied1, Blaze)

//----------------------------------------------------------------------------------
//Coatl Feather's Active                                                            
//----------------------------------------------------------------------------------
def coatlFeatherActive = Blaze.createConversation("coatlFeatherBreak", true, "QuestStarted_305:2")

def coatlFeatherActive1 = [id:1]
coatlFeatherActive1.npctext = "Don't forget the Tropical Bird Feathers. You can find them on the Feathered Coatls."
coatlFeatherActive1.result = DONE
coatlFeatherActive.addDialog(coatlFeatherActive1, Blaze)

//----------------------------------------------------------------------------------
//Coatl Feather's Complete                                                          
//----------------------------------------------------------------------------------
def coatlFeatherComplete = Blaze.createConversation("coatlFeatherComplete", true, "QuestStarted_305:3", "Z10BlazeRingGrantReceived")
coatlFeatherComplete.setUrgent(true)

def coatlFeatherComplete1 = [id:1]
coatlFeatherComplete1.npctext = "%p, you did it! Rin and Lin will be so pleased. Thanks!"
coatlFeatherComplete1.options = []
coatlFeatherComplete1.options << [text:"Glad I could help. It's what I do.", result: 2]
coatlFeatherComplete1.options << [text:"Don't be so effusive, I was only doing it for the orbs.", result: 3]
coatlFeatherComplete.addDialog(coatlFeatherComplete1, Blaze)

def coatlFeatherComplete2 = [id:2]
coatlFeatherComplete2.npctext = "Great, I guess you won't be mad when I tell you that Rin and Lin want more than fifteen feathers."
coatlFeatherComplete2.playertext = "I'm not surprised... they seem to have a thing for feathers. How many do they want?"
coatlFeatherComplete2.result = 4
coatlFeatherComplete.addDialog(coatlFeatherComplete2, Blaze)

def coatlFeatherComplete3 = [id:3]
coatlFeatherComplete3.npctext = "At least you're honest. I can be honest too, Rin and Lin want more feathers. I have more orbs if you bring me more feathers."
coatlFeatherComplete3.playertext = "Do they want fifteen again?"
coatlFeatherComplete3.result = 5
coatlFeatherComplete.addDialog(coatlFeatherComplete3, Blaze)

def coatlFeatherComplete4 = [id:4]
coatlFeatherComplete4.npctext = "As many as you can find. I'll make you a deal though, just bring me fifteen at a time... that way you won't feel overwhelmed."
coatlFeatherComplete4.options = []
coatlFeatherComplete4.options << [text:"Fifteen, you said?", result: 5]
coatlFeatherComplete4.options << [text:"I think I'm going to pass for now.", exec: { event -> 
	event.player.removeMiniMapQuestActorName("Blaze-VQS")
	runOnDeduct(event.player, 100407, 15, featherSuccess, featherFail) 
}, result: DONE]
coatlFeatherComplete.addDialog(coatlFeatherComplete4, Blaze)

def coatlFeatherComplete5 = [id:5]
coatlFeatherComplete5.npctext = "That sounds about right."
coatlFeatherComplete5.options = []
coatlFeatherComplete5.options << [text:"I'll do it! Be back in a flash.", exec: { event ->
	event.player.removeMiniMapQuestActorName("Blaze-VQS")
	if(event.player.getConLevel() < 8.6) {
		event.player.setQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_AllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Denied")
	}
	runOnDeduct(event.player, 100407, 15, featherSuccess, featherFail)
}, result: DONE]
coatlFeatherComplete5.options << [text:"But nevermind, I'm not interested right now.", exec: { event -> 
	event.player.removeMiniMapQuestActorName("Blaze-VQS")
	runOnDeduct(event.player, 100407, 15, featherSuccess, featherFail) 
}, result: DONE] //push the completion of the quest if they decline the task
coatlFeatherComplete.addDialog(coatlFeatherComplete5, Blaze)

featherSuccess = { event ->
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_AllowedAgain")) {
		Blaze.pushDialog(event.actor, "coatlFeatherAllowedAgain")
	}
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Denied")) {
		Blaze.pushDialog(event.actor, "coatlFeatherDenied")
	}
	if(!event.actor.hasQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_AllowedAgain") && !event.actor.hasQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Denied")) { 
		event.actor.setQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Success")
		Blaze.pushDialog(event.actor, "coatlFeatherSuccess")
	}
}

featherFail = { event ->
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_AllowedAgain")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_AllowedAgain")
	}
	if(event.actor.hasQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Denied")) {
		event.actor.unsetQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Denied")
	}
	event.actor.setQuestFlag(GLOBAL, "Z10_Blaze_CoatlFeather_Fail")
	Blaze.pushDialog(event.actor, "coatlFeatherFail")
}

//----------------------------------------------------------------------------------
//Coatl Feather's Success (Declined Repeat - have feathers)                         
//----------------------------------------------------------------------------------
def coatlFeatherSuccess = Blaze.createConversation("coatlFeatherSuccess", true, "Z10_Blaze_CoatlFeather_Success")

def coatlFeatherSuccess1 = [id:1]
coatlFeatherSuccess1.npctext = "Aww, come on. You know you want to help."
coatlFeatherSuccess1.flag = "!Z10_Blaze_CoatlFeather_Success"
coatlFeatherSuccess1.quest = 305
coatlFeatherSuccess1.result = DONE
coatlFeatherSuccess.addDialog(coatlFeatherSuccess1, Blaze)

//----------------------------------------------------------------------------------
//Coatl Feather's Fail (no feathers)                                                
//----------------------------------------------------------------------------------
def coatlFeatherFail = Blaze.createConversation("coatlFeatherFail", true, "Z10_Blaze_CoatlFeather_Fail")

def coatlFeatherFail1 = [id:1]
coatlFeatherFail1.npctext = "I might not be the chosen one, but I can still count! Where are the rest of the feathers?"
coatlFeatherFail1.playertext = "I... erp... what did I do with them?"
coatlFeatherFail1.flag = "!Z10_Blaze_CoatlFeather_Fail"
coatlFeatherFail1.result = DONE
coatlFeatherFail.addDialog(coatlFeatherFail1, Blaze)

//----------------------------------------------------------------------------------
//Coatl Feather's Allowed Again                                                     
//----------------------------------------------------------------------------------
def coatlFeatherAllowedAgain = Blaze.createConversation("coatlFeatherAllowedAgain", true, "Z10_Blaze_CoatlFeather_AllowedAgain")

def coatlFeatherAllowedAgain1 = [id:1]
coatlFeatherAllowedAgain1.npctext = "Great! See you soon."
coatlFeatherAllowedAgain1.playertext = "As soon as I can find 'em!"
coatlFeatherAllowedAgain1.flag = "!Z10_Blaze_CoatlFeather_AllowedAgain"
coatlFeatherAllowedAgain1.quest = 305
coatlFeatherAllowedAgain1.exec  = { event ->
	event.player.updateQuest(305, "Blaze-VQS")
}
coatlFeatherAllowedAgain1.result = DONE
coatlFeatherAllowedAgain.addDialog(coatlFeatherAllowedAgain1, Blaze)

//--------------------------------------------------------------
//RING GRANT CONVERSATION                                       
//--------------------------------------------------------------
def ringGrant = Blaze.createConversation( "ringGrant", true, "Z10BlazeRingGrantOkay", "!Z10BlazeRingGrantReceived", "!Z10RingRewardInProgress" )

def grant1 = [id:1]
grant1.playertext = "You're not wrong!"
grant1.exec = { event ->
	player = event.player
	event.player.setQuestFlag( GLOBAL, "Z10RingRewardInProgress" )
	makeMainMenu( player )
}
grant1.result = DONE
ringGrant.addDialog( grant1, Blaze )

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 4
CLdecimal = 0

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Debuffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Debuffs" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z10RingRewardInProgress" )
		}
	}
}

//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Dervish", "Hack", "Mantis", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Dervish" ) {
			makeDervishMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Mantis" ) {
			makeMantisMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Heavy Water Balloon", "Hornet Nest", "Hot Foot", "Hunter's Bow", "Shark Attack", "Shuriken", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Heavy Water Balloon" ) {
			makeHeavyWaterBalloonMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Hunter's Bow" ) {
			makeHuntersBowMenu( player )
		}
		if( event.selection == "Shark Attack" ) {
			makeSharkAttackMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Duct Tape", "Gumshoe", "Quicksand", "Scaredy Cat", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Duct Tape" ) {
			makeDuctTapeMenu( player )
		}
		if( event.selection == "Gumshoe" ) {
			makeGumshoeMenu( player )
		}
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Improbability Sphere", "Pot Lid", "Rock Armor", "Teflon Spray", "Turtle", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Improbability Sphere" ) {
			makeImprobabilitySphereMenu( player )
		}
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Rock Armor" ) {
			makeRockArmorMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Divinity", "Healing Halo", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Divinity" ) {
			makeDivinityMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Fleet Feet", "Ghost", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Fleet Feet" ) {
			makeFleetFeetMenu( player )
		}
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Adrenaline" ) {
			makeAdrenalineMenu( player )
		}
		if( event.selection == "Knife Sharpen" ) {
			makeKnifeSharpenMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.setQuestFlag( GLOBAL, "Z10BlazeRingGrantReceived" )
}









//Players are directed to Agent Caruthers by Jesse. (Jesse knows that his dad is a friend of Caruthers, and indicates it might be a good place to ask questions.)
//Agent Caruthers tells the players that Marshall was looking for a "Blue Pearl" for some reason and that his daughter was here earlier asking about the same thing. Both of them headed toward the jungle.

//Players are directed to Surfers by Bart (in Boardwalk).
//Bart says to look up the Surfers because they've spoken about glowing lights deep within the ocean.
//Surfers tell the players about the glowing lights and that they wish they had scuba gear to check it out. 
//Player says he knows of a company that does that (Deep Blue Ventures). Surfers say "Some girl was here asking about some "blue pearl"...does that have anything to do with it?"
//Player says "no...but that sound interesting...where is she?" and is directed to the Ruins.

//Players are directed toward speaking to Blaze by the Surfers (who know her from school) and by Agent Caruthers (who knows her Dad).

// Blaze doesn't think her father is in the Otami Ruins, but her brother was driving her CRAZY, so she came out here to play tag with the Animated.
// 

//Have Rin and Lin give the quest for the Underwater Breathing status, but have Blaze mention that the "old ones" might not be all dead yet and point the players toward the top of the cliff area.

//GET TO THE BLOOD TREE (And Back)
// Introductory quest to get the trust of Blaze
// She considers it "fun" to make the run there and back again. "Want to do something fun?"
// Have to go BEHIND the blood tree, into a trigger zone there, and then back to Blaze to complete the quest

//FIND THE OLD ONES
// Once you're a friend of Blaze, she'll tell you that her Dad was looking for the "old ones", and that his research indicated they still lived, but high atop the jungle cliffs.
// Blaze can't find a way to the top of the cliffs, so she asks you to do it.
// Once the player finds a way to the top of the cliffs, they can find Rin and Lin.
// Rin and Lin can be convinced that the players are good people, if they are bribed with a blue pearl, as they were by the man that was here a couple days ago. Thus, they are willing to give the secret words that must be said at the sacred portal.
// Going to the sacred portal, the player can't remember the words really well and gets three choices. If the correct one is chosen, they portal to the top of the cliff. If not, then they get harmed. They can keep choosing until they get it right.
// 

//UNDERWATER BREATHING QUEST
// Have to go "run the cliff gauntlet" to get it. The underwater breathing ability is granted for having survived the "Headwaters" gauntlet, and the ability lets you breathe below the Shallow Sea ONLY.
// Find a piece of gear from Marshall during the process (clue for Blaze)
// Return to Blaze
// Blaze asks you to go talk to Jack to find out more, telling him what you know.
// Jack sends the players to the deep sea entry point, telling them where Marshall went.

//SHALLOW SEA
// Once the players get through the Shallow Sea, they'll encounter Marshall in the Undersea Cliffs area. There, he tells the players they must don a breathing helmet to survive the depths.

//GET TO THE BLOOD TREE (And Back)
// Introductory quest to get the trust of Blaze
// She considers it "fun" to make the run there and back again. "Want to do something fun?"
// Have to go BEHIND the blood tree, into a trigger zone there, and then back to Blaze to complete the quest
// Reward the player with the Otami Sandals recipe after this "running" quest
 
//FIND THE CLIFF TOP
// Once you're a friend of Blaze, she'll tell you that Marshall was looking for the "old ones", and that his research indicated they still lived, but high atop the jungle cliffs.
// Blaze can't find a way to the top of the cliffs, so she asks you to do it.
// Once the player finds a way to the top of the cliffs, they can find Rin and Lin. (Rin and Lin only appear if the player has the quest.)
