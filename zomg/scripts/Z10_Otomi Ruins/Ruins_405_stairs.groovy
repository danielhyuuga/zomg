import com.gaiaonline.mmo.battle.script.*;

//Variable init
wanderers4051 = null
wanderers4052 = null

FEATHER_PERCENTAGE = 55

//------------------------------------------
// Feathered Coatl Stair Wanderers          
//------------------------------------------

wanderers405 = myRooms.OtRuins_405.spawnSpawner( "wanderers405", "feathered_coatl", 2 )
wanderers405.stopSpawning()
wanderers405.setPos( 380, 560 )
wanderers405.setWaitTime( 50, 70 )
wanderers405.setWanderBehaviorForChildren( 50, 150, 3, 7, 250)
wanderers405.setMonsterLevelForChildren( 7.5 )


//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

def spawnWanderers405() {
	wanderers4051 = wanderers405.forceSpawnNow() //Spawn the coatl
	runOnDeath(wanderers4051) { event -> //When the coatl dies
		if((wanderers4052 == null || wanderers4052.isDead())) {
			myManager.schedule(random(50, 70)) {spawnWanderers405()}
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListWanderers4051 = event.actor.getHated()
		hateListWanderers4051.each() {
			if(isPlayer(event.actor) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.8) {
				it.grantQuantityItem(100407, 1)
			}
		}
	}
	
	wanderers4052 = wanderers405.forceSpawnNow() //Spawn the coatl
	runOnDeath(wanderers4052) { event -> //When the coatl dies
		if((wanderers4051 == null || wanderers4051.isDead())) {
			myManager.schedule(random(50, 70)) {spawnWanderers405()}
		}
		featherRoll = random(100) //Just a roll, will be compared against players
		
		hateListWanderers4052 = event.actor.getHated()
		hateListWanderers4052.each() {
			if(isPlayer(event.actor) && featherRoll <= FEATHER_PERCENTAGE && (it.isOnQuest(305, 2) || it.isOnQuest(102, 2)) && it.getConLevel() < 8.8) {
				it.grantQuantityItem(100407, 1)
			}
		}
	}
}

spawnWanderers405()