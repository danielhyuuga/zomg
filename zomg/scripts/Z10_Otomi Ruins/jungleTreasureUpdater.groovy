def jungleTreasureUpdater = "jungleTreasureUpdater"
myRooms.OtRuins_901.createTriggerZone(jungleTreasureUpdater, 700, 160, 900, 360)
myManager.onTriggerIn(myRooms.OtRuins_901, jungleTreasureUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(231)) {
		event.actor.updateQuest(231, "Story-VQS")
	}
}