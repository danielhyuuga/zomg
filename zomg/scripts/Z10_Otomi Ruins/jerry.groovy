//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Jerry = spawnNPC("BFG-Jerry", myRooms.OtRuins_906, 405, 315)
Jerry.setDisplayName( "Jerry" )
Jerry.setRotation(345)

onQuestStep(270, 2) { event -> event.player.addMiniMapQuestActorName( "BFG-Jerry" ) }

mikaIn906 = false
Mika = null

otRuinsRoomList =["OtRuins_4","OtRuins_5","OtRuins_6","OtRuins_7","OtRuins_104","OtRuins_105","OtRuins_106","OtRuins_107","OtRuins_205","OtRuins_206","OtRuins_207","OtRuins_304","OtRuins_305","OtRuins_306","OtRuins_307","OtRuins_402","OtRuins_403","OtRuins_404","OtRuins_405","OtRuins_406","OtRuins_407","OtRuins_502","OtRuins_503","OtRuins_504","OtRuins_505","OtRuins_506","OtRuins_507","OtRuins_602","OtRuins_603","OtRuins_604","OtRuins_605","OtRuins_606","OtRuins_607","OtRuins_701","OtRuins_702","OtRuins_703","OtRuins_704","OtRuins_705","OtRuins_706","OtRuins_707","OtRuins_801","OtRuins_802","OtRuins_803","OtRuins_804","OtRuins_805","OtRuins_806","OtRuins_807","OtRuins_901","OtRuins_902","OtRuins_903","OtRuins_904","OtRuins_905","OtRuins_906"]

mikaMap = new HashMap()

def mikaMapCleanup() {
	myManager.schedule(60) {
		mikaMapCleanup()
		mikaMap.clone().keySet().each {
			if( !isOnline(it) ) {
				mikaMap.get(it).dispose()
				mikaMap.remove(it)
			} else if( !otRuinsRoomList.contains(it?.getRoomName()) ) {
				mikaMap.get(it).dispose()
				mikaMap.remove(it)
			}
		}
	}
}

mikaMapCleanup()

myManager.onEnter(myRooms.OtRuins_906) { event ->
	if(mikaMap.values().contains(event.actor) && mikaIn906 == false) {
		mikaIn906 = true
	}
	if(isPlayer(event.actor)) {
		if(event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_ToJerry")) {
			if(event.actor.isDoneQuest(260) || !mikaMap.containsKey(event.actor)) {
				event.actor.unsetQuestFlag(GLOBAL, "Z19_Mika_ToJerry")
			}
		}
		if(event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Ruins")) {
			if(event.actor.isDoneQuest(260) || !mikaMap.containsKey(event.actor)) {
				event.actor.unsetQuestFlag(GLOBAL, "Z19_Mika_Ruins")
			}
		}
		if(event.actor.hasQuestFlag(GLOBAL, "Z19_Jerry_MikaHere")) {
			if(event.actor.isDoneQuest(260) || !mikaMap.containsKey(event.actor)) {
				event.actor.unsetQuestFlag(GLOBAL, "Z19_Jerry_MikaHere")
			}
		}
		if(event.actor.hasQuestFlag(GLOBAL, "Z19_Jerry_MikaNotHere")) {
			if(event.actor.isDoneQuest(260) || !mikaMap.contains(event.actor)) {
				event.actor.unsetQuestFlag(GLOBAL, "Z19_Jerry_MikaNotHere")
			}
		}
	}
}

myManager.onExit(myRooms.OtRuins_906) { event ->
	if(isPlayer(event.actor) && event.actor.isDoneQuest(270)) { event.actor.removeMiniMapQuestActorName("BFG-Jerry") }
}

myManager.onEnter( myRooms.OtRuins_701 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(260, 2) && event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_Ruins") ) {
		Mika = spawnNPC("Mika-VQS", myRooms.OtRuins_701, 330, 600)
		Mika.setDisplayName( "Mika" )
		Mika.startFollow(event.actor)
		
		//println "##### Mika = ${Mika} #####"
		
		mikaMap.put(event.actor, Mika)
		
		event.actor.unsetQuestFlag(GLOBAL, "Z19_Mika_Ruins")
		event.actor.setQuestFlag(GLOBAL, "Z19_Mika_ToJerry")
	}
}

myManager.onExit(myRooms.OtRuins_701) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z19_Mika_ToJerry") && !otRuinsRoomList.contains(event.actor.getRoomName())) {
		//println "##### DISPOSING OF ${stoocie} #####"
		if(mikaMap.containsKey(event.actor) ) { 
			event.actor.unsetQuestFlag(GLOBAL, "Z19_Mika_ToJerry")
			mikaMap.get(event.actor)?.dispose()
			mikaMap.remove(event.actor)
		}
	}
}

//Check which room Mika is in, if she is in OtRuins_906 advance quest, otherwise don't
def checkMikaRoom(event) {
	event.player.getCrew().each() {
		if(mikaMap.containsKey(it)) {
			if(mikaMap.get(it).getRoomName() == "OtRuins_906") {
				event.player.setQuestFlag( GLOBAL, "Z19_Jerry_MikaHere" )
			}
		}
	}
}

//---------------------------------------------------------------
//Player has talked to Mark                                      
//---------------------------------------------------------------
def jerryStart = Jerry.createConversation("jerryStart", true, "!QuestStarted_260", "!QuestCompleted_260")

def jerryStart1 = [id:1]
jerryStart1.npctext = "You there, %p! The Barton Regulars require your assistance."
jerryStart1.options = []
jerryStart1.options << [text:"Really? Because of my special and unique talents?!", result: 2]
jerryStart1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 234, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 234)
			Jerry.say("Very well, one freebie, coming right up!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Jerry today. Try again tomorrow!" )
		}
	})
}, result: DONE]
jerryStart.addDialog(jerryStart1, Jerry)

def jerryStart2 = [id:2]
jerryStart2.npctext = "No, I suppose not. Honestly it doesn't matter. Anyone capable will do."
jerryStart2.playertext = "Will do what?"
jerryStart2.result = 3
jerryStart.addDialog(jerryStart2, Jerry)

def jerryStart3 = [id:3]
jerryStart3.npctext = "If you spoke to Mark, you know that I'm in charge of rescues. Unfortunately, due to my position as a guard there are a couple of people in areas I can't safely approach. It is likely that you would have more success than I."
jerryStart3.options = []
jerryStart3.options << [text:"Why should I have more success than you?", result: 4]
jerryStart3.options << [text:"Really? Why can't you get to them?", result: 5]
jerryStart.addDialog(jerryStart3, Jerry)

def jerryStart4 = [id:4]
jerryStart4.npctext = "Because you do not wear the armor of a Barton Regular. You would be seen as a neutral party and permitted access to areas where I am denied it."
jerryStart4.options = []
jerryStart4.options << [text:"Strange, I would have thought the opposite would be true.", result: 5]
jerryStart4.options << [text:"I guess that makes sense. Who needed rescuing and how do I find them?", result: 6]
jerryStart.addDialog(jerryStart4, Jerry)

def jerryStart5 = [id:5]
jerryStart5.npctext = "Let's just say that we're far from Barton Town and there are certain... powers that do not appreciate the intrusion of the Barton Regulars."
jerryStart5.playertext = "I see, I'm not one of you so they wouldn't be suspicious of me. Who needs rescuing, then?"
jerryStart5.result = 6
jerryStart.addDialog(jerryStart5, Jerry)

def jerryStart6 = [id:6]
jerryStart6.npctext = "The daughter of an important citizen has gone missing. Apparently the girl likes to go on hikes and just wander aimlessly for hours. Looks like this time she wandered too far and couldn't find her way back."
jerryStart6.playertext = "That doesn't tell me where to find her..."
jerryStart6.result = 7
jerryStart.addDialog(jerryStart6, Jerry)

def jerryStart7 = [id:7]
jerryStart7.npctext = "I was coming to that. Anyway, I've been able to track her to the nearby cliffs but 'the spirits' won't allow me on 'such holy ground.' At least that's what that wild girl said. I'd like to see them stop me if I could find an accessible route..."
jerryStart7.playertext = "Wild girl?"
jerryStart7.result = 8
jerryStart.addDialog(jerryStart7, Jerry)

def jerryStart8 = [id:8]
jerryStart8.npctext = "Blaze, I think her name was. Crazy thing has been here so long the forest is practically her home. If anyone knows a way up to the cliffs, it's her."
jerryStart8.playertext = "And you think I can get her to tell me where it is? Why?"
jerryStart8.result = 9
jerryStart.addDialog(jerryStart8, Jerry)

def jerryStart9 = [id:9]
jerryStart9.npctext = "Because she knew your name and that you would be coming."
jerryStart9.playertext = "What?! That's not crazy, that's impossible!"
jerryStart9.result = 10
jerryStart.addDialog(jerryStart9, Jerry)

def jerryStart10 = [id:10]
jerryStart10.npctext = "Heh, yeah. I thought it was crazy but then Mark messaged me and you showed up here. I'm not sure what to make of it, but there's still a job to do. Are you in?"
jerryStart10.options = []
jerryStart10.options << [text:"Yes. How should I proceed?", result: 11]
jerryStart10.options << [text:"Sorry, but this is all too creepy for me. I'm out.", result: 12]
jerryStart.addDialog(jerryStart10, Jerry)

def jerryStart11 = [id:11]
jerryStart11.npctext = "It's up to you. If you want to hunt for a way up the cliff, do that. Or you could see if Blaze will just tell you. Just make sure you find that girl!"
jerryStart11.playertext = "Okay. Hey, you never told me her name! What is it?"
jerryStart11.result = 13
jerryStart.addDialog(jerryStart11, Jerry)

def jerryStart12 = [id:12]
jerryStart12.npctext = "I guess I'll have to find someone else..."
jerryStart12.exec = { event -> event.player.removeMiniMapQuestActorName( "BFG-Jerry" ) }
jerryStart12.result = DONE
jerryStart.addDialog(jerryStart12, Jerry)

def jerryStart13 = [id:13]
jerryStart13.npctext = "Oops, my bad. Her name is Mika. The cliffs are to the west and Blaze is to the north."
jerryStart13.quest = 260
jerryStart13.exec = { event -> 
	event.player.removeMiniMapQuestActorName( "BFG-Jerry" ) 
	if(!event.player.hasQuestFlag(GLOBAL, "markJerryStarted1")) {
		event.player.setQuestFlag(GLOBAL, "markJerryStarted1")
	}
}
jerryStart13.result = DONE
jerryStart.addDialog(jerryStart13, Jerry)

//---------------------------------------------------------------
//Missing Mika Active                                            
//---------------------------------------------------------------
def mikaActive = Jerry.createConversation("mikaActive", true, "QuestStarted_260:2", "!Z19_Mika_ToJerry", "!Z19_Jerry_MikaHere", "!Z19_Jerry_MikaNotHere")

def mikaActive1 = [id:1]
mikaActive1.npctext = "Any luck finding your way up to the cliffs?"
mikaActive1.options = []
mikaActive1.options << [text:"Not yet.", result: 2]
mikaActive1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 234, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 234)
			Jerry.say("Very well, one freebie, coming right up!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Jerry today. Try again tomorrow!" )
		}
	})
}, result: DONE]
mikaActive.addDialog(mikaActive1, Jerry)

def mikaActive2 = [id:2]
mikaActive2.npctext = "Well, hurry up. That poor girl must be terrified."
mikaActive2.result = DONE
mikaActive.addDialog(mikaActive2, Jerry)

//-----------------------------------------------------------------------------------------------------------
//Bounce in to see if Mika is in the room                                                                    
//-----------------------------------------------------------------------------------------------------------
def jerryMikaBounce = Jerry.createConversation("jerryMikaBounce", true, "QuestStarted_260:2", "Z19_Mika_ToJerry", "!Z19_Jerry_MikaHere", "!Z19_Jerry_MikaNotHere")

def mikaBounce1 = [id:1]
mikaBounce1.npctext = "Have you brought the girl?"
mikaBounce1.exec = { event ->
	checkMikaRoom(event)
	if(event.player.hasQuestFlag(GLOBAL, "Z19_Jerry_MikaHere")) {
		event.player.unsetQuestFlag(GLOBAL, "Z19_Mika_ToJerry")
		Jerry.pushDialog( event.player, "mikaComplete" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z19_Jerry_MikaNotHere" )
		Jerry.pushDialog( event.player, "jerryMikaNotHere" )
	}
}
mikaBounce1.result = DONE
jerryMikaBounce.addDialog( mikaBounce1, Jerry)

//-----------------------------------------------------------------------------------------------------------
//Converversation after bounce if Mika not in room                                                           
//-----------------------------------------------------------------------------------------------------------
def jerryMikaNotHere = Jerry.createConversation("jerryMikaNotHere", true, "QuestStarted_260:2", "Z19_Jerry_MikaNotHere")

def mikaNotHere1 = [id:1]
mikaNotHere1.npctext = "Uhm, I do not see Mika."
mikaNotHere1.flag = "!Z19_Jerry_MikaNotHere"
mikaNotHere1.result = DONE
jerryMikaNotHere.addDialog(mikaNotHere1, Jerry)

//---------------------------------------------------------------
//Missing Mika Complete                                          
//---------------------------------------------------------------
def mikaComplete = Jerry.createConversation("mikaComplete", true, "QuestStarted_260:2", "Z19_Jerry_MikaHere")

def mikaComplete1 = [id:1]
mikaComplete1.playertext = "I found Mika. She's safe now."
mikaComplete1.result = 4
mikaComplete.addDialog(mikaComplete1, Jerry)

/*def mikaComplete2 = [id:2]
mikaComplete2.npctext = "I knew you'd find a way up. Mika, I trust you can find the rest of the way home yourself."
mikaComplete2.result = 3
mikaComplete.addDialog(mikaComplete2, Jerry)

def mikaComplete3 = [id:3]
mikaComplete3.npctext = "Yes, Jerry. You can message my father and tell him I'm heading directly home. *sigh*"
mikaComplete3.result = 4
mikaComplete.addDialog(mikaComplete3, Mika)*/

def mikaComplete4 = [id:4]
mikaComplete4.npctext = "%p, you did well. I have another task in mind for you, one requiring the subtlety you seem to possess."
mikaComplete4.options = []
mikaComplete4.options << [text:"Subtlety? Me? Hmm...", result: 5]
mikaComplete4.options << [text:"Awesome, I love saving people!", result: 6]
mikaComplete4.options << [text:"Maybe later.", result: 7]
mikaComplete.addDialog(mikaComplete4, Jerry)

def mikaComplete5 = [id:5]
mikaComplete5.npctext = "Yes. I need someone who can get in and out without raising any alarm. The fact that you were able to get up to the cliffs without arousing suspicion from Blaze or her 'spirits' makes me think you can do this."
mikaComplete5.playertext = "If we're supposed to rescue someone why do we need to avoid raising an alarm? I thought rescuing people was a *good* thing!"
mikaComplete5.result = 8
mikaComplete.addDialog(mikaComplete5, Jerry)

def mikaComplete6 = [id:6]
mikaComplete6.npctext = "Excellent! I need someone who can get in and out without raising any alarm. The fact that you were able to get up to the cliffs without arousing suspicion from Blaze or her 'spirits' makes me think you can do this."
mikaComplete6.playertext = "Who would be alarmed by a rescue? I'd think they would be glad to see us."
mikaComplete6.result = 8
mikaComplete.addDialog(mikaComplete6, Jerry)

def mikaComplete7 = [id:7]
mikaComplete7.npctext = "Uh... okay. Come back soon?"
mikaComplete7.flag = ["Z10_Jerry_Regular_Break", "!Z19_Jerry_MikaHere"]
mikaComplete7.exec = { event -> 
	//event.player.addMiniMapQuestLocation("True Believers", "Aqueduct_602", 630, 55, "True Believers")
	event.player.removeMiniMapQuestActorName("BFG-Jerry")
	myManager.schedule(3) {
		if(mikaMap.containsKey(event.player)) {
			mikaMap.get(event.player).startFollow( Jerry ) 
			Jerry.say("Mika, I trust that you can find your own way back to Barton Town?")
			myManager.schedule(3) {
				mikaMap.get(event.player).say("Probably even faster than you could! Hehehe.")
				def mikaDespawnMove = makeNewPatrol()
				mikaDespawnMove.addPatrolPoint( myRooms.OtRuins_906, 680, 305, 0 )
				mikaDespawnMove.addPatrolPoint( myRooms.OtRuins_906, 1000, 330, 10 )
				mikaMap.get(event.player).setPatrol(mikaDespawnMove)
				mikaMap.get(event.player).startPatrol()
				myManager.schedule(5) { 
					mikaMap.get(event.player).dispose() 
					
					mikaIn906 = false
				}
			}
		}
	}
}
mikaComplete7.quest = 260
mikaComplete7.result = DONE
mikaComplete.addDialog(mikaComplete7, Jerry)

def mikaComplete8 = [id:8]
mikaComplete8.npctext = "Usually, but when you rescue someone from a cult the other members of the cult are pretty hostile."
mikaComplete8.playertext = "Err... cult?"
mikaComplete8.result = 9
mikaComplete.addDialog(mikaComplete8, Jerry)

def mikaComplete9 = [id:9]
mikaComplete9.npctext = "Did I say cult? I meant to say 'True Believers!' The ones awaiting the return of the Zurg at the top of the hill overlooking the Old Aqueduct."
mikaComplete9.playertext = "I don't get it. I'm supposed to rescue all of them? From what?"
mikaComplete9.result = 10
mikaComplete.addDialog(mikaComplete9, Jerry)

def mikaComplete10 = [id:10]
mikaComplete10.npctext = "No, not all of them. Just my idi... my misguided brother. He's joined them, the problem is he's already taken an oath of loyalty to the Barton Regulars and that's not the sort of thing you just opt out of. Now it's up to me to get him back."
mikaComplete10.options = []
mikaComplete10.options << [text:"If he's your brother can't you just go talk to him and get him to come back?", result: 11]
mikaComplete10.options << [text:"If he doesn't want to be a guard any more isn't that his business?", result: 12]
mikaComplete10.options << [text:"No way. If you want your brother back you'll have to kidnap him yourself.", result: 13]
mikaComplete.addDialog(mikaComplete10, Jerry)

def mikaComplete11 = [id:11]
mikaComplete11.npctext = "There's no way the Believers would let me near him. They know I am trying to rescue him."
mikaComplete11.playertext = "Hmm... so what am I supposed to do?"
mikaComplete11.result = 14
mikaComplete.addDialog(mikaComplete11, Jerry)

def mikaComplete12 = [id:12]
mikaComplete12.npctext = "He swore an oath and I'll not have our family dishonored because he thinks the Zurg are coming back. Besides, he can wait for the Zurg in a suit of armor just as well as he can wait in a white cloak."
mikaComplete12.playerext = "What am I supposed to do about that?"
mikaComplete12.result = 14
mikaComplete.addDialog(mikaComplete12, Jerry)

def mikaComplete13 = [id:13]
mikaComplete13.npctext = "I thought I made it clear that I couldn't. Very well, if you see fit to reconsider I'll be here looking for someone to help."
mikaComplete13.flag = ["Z10_Jerry_Regular_Break", "!Z19_Jerry_MikaHere"]
mikaComplete13.quest = 260
mikaComplete13.exec = { event -> 
	//event.player.addMiniMapQuestLocation("True Believers", "Aqueduct_602", 630, 55, "True Believers")
	event.player.removeMiniMapQuestActorName("BFG-Jerry")
	myManager.schedule(3) {
		if(mikaMap.containsKey(event.player)) {
			mikaMap.get(event.player).startFollow( Jerry ) 
			Jerry.say("Mika, I trust that you can find your own way back to Barton Town?")
			myManager.schedule(3) {
				mikaMap.get(event.player).say("Probably even faster than you could! Hehehe.")
				def mikaDespawnMove = makeNewPatrol()
				mikaDespawnMove.addPatrolPoint( myRooms.OtRuins_906, 680, 305, 0 )
				mikaDespawnMove.addPatrolPoint( myRooms.OtRuins_906, 1000, 330, 10 )
				mikaMap.get(event.player).setPatrol(mikaDespawnMove)
				mikaMap.get(event.player).startPatrol()
				myManager.schedule(5) { 
					mikaMap.get(event.player).dispose() 
					
					mikaIn906 = false
				}
			}
		}
	}
}
mikaComplete13.result = DONE
mikaComplete.addDialog(mikaComplete13, Jerry)

def mikaComplete14 = [id:14]
mikaComplete14.npctext = "Head up to the clearing where the True Believers await the return of the Zurg, talk with my brother, and convince him to leave the clearing. I will have someone ready to take him back to Barton Town."
mikaComplete14.options = []
mikaComplete14.options << [text:"Sounds easy enough. How will I recognize him?", result: 15]
mikaComplete14.options << [text:"Sorry, but that sounds like kidnapping. You're gonna have to do it yourself.", result: 13]
mikaComplete.addDialog(mikaComplete14, Jerry)

def mikaComplete15 = [id:15]
mikaComplete15.npctext = "Ask for him by name, he's Peter."
mikaComplete15.playertext = "Great, I'll get right on it."
mikaComplete15.flag = "!Z19_Jerry_MikaHere"
mikaComplete15.exec = { event -> 
	//event.player.addMiniMapQuestLocation("True Believers", "Aqueduct_602", 630, 55, "True Believers")
	event.player.updateQuest( 260, "BFG-Jerry" )
	event.player.updateQuest( 270, "BFG-Jerry" )
	event.player.removeMiniMapQuestActorName("BFG-Jerry")
	myManager.schedule(3) {
		if(mikaMap.containsKey(event.player)) {
			mikaMap.get(event.player).startFollow( Jerry ) 
			Jerry.say("Mika, I trust that you can find your own way back to Barton Town?")
			myManager.schedule(3) {
				mikaMap.get(event.player).say("Probably even faster than you could! Hehehe.")
				def mikaDespawnMove = makeNewPatrol()
				mikaDespawnMove.addPatrolPoint( myRooms.OtRuins_906, 680, 305, 0 )
				mikaDespawnMove.addPatrolPoint( myRooms.OtRuins_906, 1000, 330, 10 )
				mikaMap.get(event.player).setPatrol(mikaDespawnMove)
				mikaMap.get(event.player).startPatrol()
				myManager.schedule(5) { 
					mikaMap.get(event.player).dispose() 
					
					mikaIn906 = false
				}
			}
		}
	}
}
mikaComplete15.result = DONE
mikaComplete.addDialog(mikaComplete15, Jerry)

//---------------------------------------------------------------
//Returning Regular Break                                        
//---------------------------------------------------------------
def returningRegularBreak = Jerry.createConversation("returningRegularBreak", true, "Z10_Jerry_Regular_Break")

def returningRegularBreak1 = [id:1]
returningRegularBreak1.npctext = "Please! Help me return my family to honor!"
returningRegularBreak1.options = []
returningRegularBreak1.options << [text:"Fine, I'll help. How?", result: 2]
returningRegularBreak1.options << [text:"You're on your own. I'm no kidnapper.", result: 4]
returningRegularBreak1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 234, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 234)
			Jerry.say("Very well, one freebie, coming right up!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Jerry today. Try again tomorrow!" )
		}
	})
}, result: DONE]
returningRegularBreak.addDialog(returningRegularBreak1, Jerry)

def returningRegularBreak2 = [id:2]
returningRegularBreak2.npctext = "Just make your way up to the True Believers' clearing and talk with my brother. You'll have to convince him to leave the clearing, but I will have someone ready to take him back to Barton Town when you do."
returningRegularBreak2.options = []
returningRegularBreak2.options << [text:"Sounds easy. So I know who I'm looking for, what's his name?", result: 3]
returningRegularBreak2.options << [text:"Uhh... that's a bit shady. I'm no kidnapper.", result: 4]
returningRegularBreak.addDialog(returningRegularBreak2, Jerry)

def returningRegularBreak3 = [id:3]
returningRegularBreak3.npctext = "Peter. He should be easy to find."
returningRegularBreak3.playretext = "Consider it done."
returningRegularBreak3.quest = 270
returningRegularBreak3.flag = "!Z10_Jerry_Regular_Break"
returningRegularBreak3.result = DONE
returningRegularBreak.addDialog(returningRegularBreak3, Jerry)

def returningRegularBreak4 = [id:4]
returningRegularBreak4.npctext = "This is not kidnapping, it is honor!"
returningRegularBreak4.result = DONE
returningRegularBreak.addDialog(returningRegularBreak4, Jerry)

//---------------------------------------------------------------
//Returning Regular Active                                       
//---------------------------------------------------------------
def returningRegularActive = Jerry.createConversation("returningRegularActive", true, "QuestStarted_270:2")

def returningRegularActive1 = [id:1]
returningRegularActive1.npctext = "I have not heard from my men, were you able to retrieve my brother?"
returningRegularActive1.options = []
returningRegularActive1.options << [text:"Haven't found him yet. Soon, I think.", result: DONE]
returningRegularActive1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 234, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 234)
			Jerry.say("Very well, one freebie, coming right up!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Jerry today. Try again tomorrow!" )
		}
	})
}, result: DONE]
returningRegularActive.addDialog(returningRegularActive1, Jerry)

//---------------------------------------------------------------
//Returning Regular Complete                                     
//---------------------------------------------------------------
def returningRegularComplete = Jerry.createConversation("returningRegularComplete", true, "QuestStarted_270:3")
returningRegularComplete.setUrgent(true)

def returningRegularComplete1 = [id:1]
returningRegularComplete1.npctext = "I received word from my men that my brother has returned to Barton Town and is preparing for duty. My deepest thanks, %p."
returningRegularComplete1.playertext = "I hope he didn't put up much of a fight on the way back... I kind of tricked him into leaving the clearing."
returningRegularComplete1.result = 2
returningRegularComplete.addDialog(returningRegularComplete1, Jerry)

def returningRegularComplete2 = [id:2]
returningRegularComplete2.npctext = "Nah, once he saw the other Regulars he knew it was time to return to duty. My brother might be flighty, but he's not dumb."
returningRegularComplete2.playertext = "Cool. Anything else, or should I head back to Mark?"
returningRegularComplete2.result = 3
returningRegularComplete.addDialog(returningRegularComplete2, Jerry)

def returningRegularComplete3 = [id:3]
returningRegularComplete3.npctext = "I have no more tasks for you, perhaps you should return to Mark and see if he has any. I will inform him of how helpful you have been, though there is no doubt he has already noted the return of Peter."
returningRegularComplete3.exec = { event -> 
	event.player.removeMiniMapQuestActorName("BFG-Jerry") 
	event.player.updateQuest( 270, "BFG-Jerry" )
	event.player.unsetQuestFlag(GLOBAL, "Z3_Albert_Active")
	event.player.grantItem("17858")
}
returningRegularComplete3.result = DONE
returningRegularComplete.addDialog(returningRegularComplete3, Jerry)

//---------------------------------------------------------
//Daily Chance                                             
//---------------------------------------------------------
def dailyChance = Jerry.createConversation("dailyChance", true, "QuestCompleted_270")

def dailyChance1 = [id:1]
dailyChance1.npctext = "Hello again, %p. What can I do for you?"
dailyChance1.playertext = "Do you have any freebies available today?"
dailyChance1.exec = { event ->
	checkDailyChance(event.player, 234, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 234)
			Jerry.say("Very well, one freebie, coming right up!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Jerry today. Try again tomorrow!" )
		}
	})
}
dailyChance1.result = DONE
dailyChance.addDialog(dailyChance1, Jerry)
