import com.gaiaonline.mmo.battle.script.*;

// NOTE: trying to force them to new VillageTraining; obsolete all here below.

//TODO: Move the player starting point to Training_1, 900, 440 in code and remove the warp command in the onEnter.

//==============================================
// FRANK IN THE TRAIN                           
//==============================================

//Start in Training_1
// - Collision off the door to the rear cars and the door to the engine compartment.
// - Have Frank stand at front of the car and motion the player forward to teach movement
// 	-- Indicate area (in front of Frank) to click on the screen via a switch
//      -- Put trash cans one on each side of Frank (green arrows in front of them to light up when he says "target one of these")
//	-- Give player a ring (standard default, no choice, automatically goes to slot 1)
//	-- Tell player to kill a trash can (light up the green arrows, turn it off when can is dead, lighting up when alive again)
//	-- Tell about Stamina (show gauge) 
//	-- Upon success, teach Rage Rank (how to detect if Rage Rank is used? check health after one blow using onHealth...if not sufficient, destroy can and respawn new one. Try again.)
//	-- Upon success, stop the train and Frank makes excuses to go to back of train while player debarks.

Frank = spawnNPC("Frank-VQS", myRooms.Training_1, 615, 415)
Frank.setRotation( 45 )
Frank.setDisplayName( "Frank" )
Frank.setRange( 1000 )

//==============================================
// THE RE-KILLABLE TRASH CAN                    
//==============================================

garbage = myRooms.Training_1.spawnSpawner( "garbage", "fluff_training", 1 )
garbage.setPos( 650, 495 ) 
garbage.setMonsterLevelForChildren( 1.0 )
garbage.setGuardPostForChildren( "Training_1", 635, 485, 315 )

garbage.stopSpawning()

practiceTarget = garbage.forceSpawnNow()
numBlows = 0
watchHealth()

//THESE ROUTINES LAUNCH AS SOON AS TRAINSTATION_1 IS LOADED
targetDeathWatch()

//Once activated, these routines run indefinitely, allowing the player to practice against them as much as desired.
def targetDeathWatch() {
	runOnDeath(practiceTarget, { event ->
		myManager.schedule(3) { 
			practiceTarget = garbage.forceSpawnNow()
			numBlows = 0
			watchHealth()
			
			targetDeathWatch()
		}
		if( !curPlayer.hasQuestFlag( GLOBAL, "Z23GarbageCanDestroyed" ) && curPlayer.hasQuestFlag( GLOBAL, "Z23TrashCanTargetsOkay" ) ) {
			curPlayer.unsetQuestFlag( GLOBAL, "Z23TrashCanTargetsOkay" )
			 
			curPlayer.say( "Rubbin' out the rubbish!" )
			killTimer.cancel()

			myManager.schedule(2) { curPlayer.setQuestFlag( GLOBAL, "Z23GarbageCanDestroyed" ); Frank.pushDialog( curPlayer, "rageRanks" ) }
		}
	} )
}

def watchHealth() {
	myManager.onHealth( practiceTarget ) { event ->
		numBlows ++ 
		if( numBlows == 1 && !event.didTransition( 20 ) && event.isDecrease() && curPlayer.hasQuestFlag( GLOBAL, "Z23RageRankTalkCompleted" ) ) {
			rageTimer.cancel()
			curPlayer.centerPrint( "You didn't do enough damage in one blow. Click and HOLD until the Rage Meter charges more next time." )
			practiceTarget.instantPercentDamage(100)
			curPlayer.grantRage(100)
			rageTimer = myManager.schedule(10) {
				curPlayer.centerPrint( "Select the trash can, then click and HOLD to power your Rage Meter." )
			}
		} else if( numBlows == 1 && event.didTransition( 17 ) && event.isDecrease() && curPlayer.hasQuestFlag( GLOBAL, "Z23RageRankTalkCompleted" ) ) {
			rageTimer.cancel()
			curPlayer.unsetQuestFlag( GLOBAL, "Z23RageRankTalkCompleted" )
			myManager.schedule(2) { curPlayer.setQuestFlag( GLOBAL, "Z23ExitOkay" ); Frank.pushDialog( curPlayer, "success" ) }
		}				
	}
}


//=================================
//GREEN ARROW DIRECTIONAL SWITCHES 
//=================================
beacon_01 = makeSwitch( "beacon_01", myRooms.Training_1, 0, 0 )
exitArrow = makeSwitch( "exitArrow", myRooms.Training_1, 0, 0 )

beacon_01.off()
exitArrow.off()

beacon_01.setUsable( false )
exitArrow.setUsable( false )

//==============================================
// BEGIN TRAINING LOGIC                         
//==============================================
firstTimeDialog = false

myManager.precurtainHook() { actor ->
	// disabling because we're rerouting them to new VillageTraining.
	//unsetHUDWidgets( actor );
}

myManager.onEnter( myRooms.Training_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		curPlayer = event.actor
		curPlayer.warp( "VillageTraining_2", 500, 500 )
		/* disabling because we're rerouting them to new VillageTraining.
		//unset all quest flags so the complete series of training scenarios runs correctly -- do this anytime the player lands back on the Train
		freePlayer( event.actor )
		event.actor.unsetQuestFlag( GLOBAL, "Z23AlreadyLockedDownOnce" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23MovementTraining" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23RingGrantOkay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23AuxRingConvoOkay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23AuxRingConvoDone" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23TargetTrash" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23TrashCanTargetsOkay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23NowKill" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23GarbageCanDestroyed" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23RageRankTalkCompleted" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23ExitOkay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z23TrainHasStoppedOnceAlready" ) //if the player successfully left the train, then unset this flag so QA and devs can access the train again thereafter.
		//TrainStation flags
		event.actor.setQuestFlag( GLOBAL, "Z22TrainStationWidgetSettingsMade" ) 
		event.actor.unsetQuestFlag( GLOBAL, "Z22MoviePlayedSuccessfully" )
		event.actor.unsetQuestFlag( GLOBAL, "Z22BarryHello" )
		event.actor.unsetQuestFlag( GLOBAL, "Z22AfterMovie" )
		event.actor.unsetQuestFlag( GLOBAL, "Z22EndSpeechOkay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z22PlayerAuthorizedToLeaveTrainStation" )	
		event.actor.unsetQuestFlag( GLOBAL, "Z22PlayerHasLeftTheTrainStation" )
		//SewersTraining flags
		event.actor.unsetQuestFlag( GLOBAL, "Z14DaniHelloOkay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z14LastConvoOkay" )
		event.actor.unsetQuestFlag( GLOBAL, "Z14OkayToLeaveSewers" )
		event.actor.unsetQuestFlag( GLOBAL, "Z14HasLeftSewers" )
		//Elizabeth Null Chamber, Combat, and Explore Paths
		event.actor.unsetQuestFlag( GLOBAL, "Z01ElizabethSpeechDone" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01ElizabethAdvice" )
		event.actor.unsetQuestFlag( GLOBAL, "Z02HasNullChamberFlagSet" )
		event.actor.unsetQuestFlag( GLOBAL, "Z21UgradeSpeechCompleted" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01ExplorePathChosen" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01ExplorePathCompleted" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01ExplorePathRewardGiven" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01ExplorePathRingGiven" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01DefendPathChosen" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01DefendPathCompleted" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01DefendPathRewardGiven" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01DefendPathRingGiven" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01DefendPathStageTwoStarted" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01GramsterGooStarted" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01GotGoo" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01NotGotGoo" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01DefendPathStageThreeStarted" )
		//set the Barry dialog so that it's like the player is returning from the Sewers for his alternate conversation (not the "I died and reappeared here" convo).
		event.actor.setQuestFlag( GLOBAL, "Z25ComingFromSewersTraining" )
		
		//event.actor.warp( "Training_1", 900, 440 ) [rc] Taking this out because it's causing client issues
		event.actor.turnToFace(practiceTarget) // Face the trash can, which is lower left

		controlPlayer( event.actor ) //prevent player from moving until Frank says it's okay to move.
		myManager.schedule(4) { event.actor.setQuestFlag( GLOBAL, "Z23MovementTraining" ); Frank.pushDialog( event.actor, "movement" ) }
		//set a timer for 15 seconds, and then 20 seconds, after scenario start to keep pushing the dialog to the player
		firstReminderTimer = myManager.schedule(10) { event.actor.centerPrint( "Click on Frank to start the conversation or 'OK' to continue." ) }
		secondReminderTimer = myManager.schedule(20) { event.actor.centerPrint( "Click on Frank to start the conversation or 'OK' to continue." ) }
		if( event.actor.hasQuestFlag( GLOBAL, "Z23TrainHasStoppedOnceAlready" ) ) {
			freePlayer( event.actor )
			event.actor.warp( "TrainStation_1", 820, 800 )
		}
		*/
	}
}

def unsetHUDWidgets( actor ) { 
	//turn off all the HUD widgets that aren't necessary to see
	showWidget( actor, "RageMeter", false )
	showWidget( actor, "StaminaMeter", false )
	showWidget( actor, "HealthMeter", false )
	showWidget( actor, "GhiMeter", false )
	showWidget( actor, "RingBar", false )
	showWidget( actor, "BarRotator", false )
	showWidget( actor, "PowerUpsBar", false )
	showWidget( actor, "CrewListBtn", false )
	showWidget( actor, "PDABtn", false )
	showWidget( actor, "ChangePoseBtn", false )
	showWidget( actor, "HUD", false )
	showWidget( actor, "PDA", false )
	showWidget( actor, "QuestLog", false )
	showWidget( actor, "MiniMap", false )
	showWidget( actor, "InventoryPanel", false )
	showWidget( actor, "ChatPaneBtn", true )
	showWidget( actor, "ChatPane", true )
}
	

//DIALOG:
// Listen man, it's not safe out there anymore.
// There's animated everywhere. You need to take this ring and stay safe.
// Seriously! Listen to me! This is important!

//Frank's intro dialog
def movement = Frank.createConversation( "movement", true, "Z23MovementTraining" )

def move1 = [id:1]
move1.npctext = "Hey there!"
move1.exec = { event ->
	firstReminderTimer.cancel()
	secondReminderTimer.cancel()
}
move1.result = 2
movement.addDialog( move1, Frank )

def move2 = [id:2]
move2.npctext = "Come on over here. Let's talk for a sec."
move2.result = 3
movement.addDialog( move2, Frank )

def move3 = [id:3]
move3.npctext = "<zOMG dialogWidth='200'><![CDATA[<h1><b><font face='Arial' size='14'>Mouse Movement</font></b></h1><font face='Arial' size ='12'><br>Left-click a screen location to move to that spot.<br><p><img src='help-files/topic-content/default/mouse.png'></p><textformat leading='210'> </textformat><p><br> </p>]]></zOMG>"
move3.result = 4
movement.addDialog( move3, tutorialNPC )

def move4 = [id:4]
move4.npctext = "<h1><b><font face='Arial' size='14'>Keyboard Movement</font></b></h1><font face='Arial' size ='12'><br><p>Or use either the arrow keys or the 'WASD' keys to steer your character.</p><p><img src='help-files/topic-content/default/arrows.png'></p><p><img src='help-files/topic-content/default/wasd.png'></p><textformat leading='195'> </textformat><br><p>Close this tutorial box and try it!</p><br>"
move4.flag = "!Z23MovementTraining"
move4.exec = { event ->
	freePlayer( event.player )
	beacon_01.on()
	moveTimer = myManager.schedule(10) { event.player.centerPrint( "Click on the green glowing area to move to Frank." ) }
}
move4.result = DONE
movement.addDialog( move4, tutorialNPC )

//The player then moves to Frank by clicking or maneuvering to the indicated location
def comeToFrank = "comeToFrank"
myRooms.Training_1.createTriggerZone( comeToFrank, 650, 370, 730, 500 ) //surrounding the point at 680, 430

myManager.onTriggerIn(myRooms.Training_1, comeToFrank ) { event ->
	if( isPlayer( event.actor ) && !event.actor.hasQuestFlag( GLOBAL, "Z23AlreadyLockedDownOnce" ) ) {
		moveTimer.cancel()
		event.actor.setQuestFlag( GLOBAL, "Z23AlreadyLockedDownOnce" )
		
		//If the player has already received their Solar Rays ring, then don't grant another one
		if( event.actor.hasQuestFlag( GLOBAL, "Z23RingGrantReceived" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z23AuxRingConvoOkay" )
			Frank.pushDialog( event.actor, "ringAlreadyGranted" )
		//if they haven't received a ring yet then give them one
		} else { 
			event.actor.setQuestFlag( GLOBAL, "Z23RingGrantOkay" )
			Frank.pushDialog( event.actor, "ringGrant" )
		}
		beacon_01.off()
		controlPlayer( event.actor ) //turn off player control until after the player destroys the garbage cans
	}
}

//Give the player a ring and tell them to kill a trash can
def ringGrant = Frank.createConversation( "ringGrant", true, "Z23RingGrantOkay" )

def ring1 = [id:1]
ring1.npctext = "Thanks. My name is Frank. Can you believe what's been going on?"
ring1.playertext = "You mean the Animated?"
ring1.result = 2
ringGrant.addDialog( ring1, Frank )

def ring2 = [id:2]
ring2.npctext = "Yeah. I mean...who'd have thought? Inanimate objects coming to life and attacking us like this?"
ring2.playertext = "And we can't do *anything* to them! All our weapons just bounce off them!"
ring2.result = 3
ringGrant.addDialog( ring2, Frank )

def ring3 = [id:3]
ring3.npctext = "I know. They seem to hate us, attacking on sight."
ring3.playertext = "There's gotta be a way to resist them. Someone will figure it out."
ring3.result = 4
ringGrant.addDialog( ring3, Frank )

def ring4 = [id:4]
ring4.npctext = "Someone has!"
ring4.playertext = "Really?"
ring4.result = 5
ringGrant.addDialog( ring4, Frank )

def ring5 = [id:5]
ring5.npctext = "GCorp came to the rescue! Labtechs showed up with handfuls of these strange, powerful rings to use against the Animated. Just in the nick of time!"
ring5.playertext = "Rings? Like...on your fingers? How would that help?"
ring5.result = 6
ringGrant.addDialog( ring5, Frank )

def ring6 = [id:6]
ring6.npctext = "The rings draw their power from the same source that powers the Animated, and that lets us use their own energy source against them! It works when nothing else seems to and we can fight back this way!"
ring6.playertext = "Fantastic! Ummm...you don't happen to have a spare one of those rings around, do you?"
ring6.result = 7
ringGrant.addDialog( ring6, Frank )

def ring7 = [id:7]
ring7.npctext = "As a matter of fact, I do! Here...put this on!"
ring7.flag = "!Z23RingGrantOkay"
ring7.exec = { event ->
	showWidget( event.actor, "RingBar", true )
	if( !event.player.hasQuestFlag( GLOBAL, "Z23RingGrantReceived" ) ) {
		event.player.setQuestFlag( GLOBAL, "Z23RingGrantReceived" )
		event.player.grantRing( "17720", true ) 
	}
	myManager.schedule(3) { event.player.setQuestFlag( GLOBAL, "Z23TargetTrash" ); Frank.pushDialog( event.player, "targetTrash" ) }
}
ring7.result = DONE
ringGrant.addDialog( ring7, Frank )

//Auxiliary conversation if the player restarts Training, but already has the Solar Rays ring
def ringAlreadyGranted = Frank.createConversation( "ringAlreadyGranted", true, "Z23AuxRingConvoOkay", "!Z23AuxRingConvoDone" )

def already1 = [id:1]
already1.npctext = "Good to see you again."
already1.playertext = "Back at ya!"
already1.result = 2
ringAlreadyGranted.addDialog( already1, Frank )

def already2 = [id:2]
already2.npctext = "I see you still have your ring!"
already2.flag = "Z23AuxRingConvoDone"
already2.exec = { event ->
	showWidget( event.actor, "RingBar", true )
	event.player.setQuestFlag( GLOBAL, "Z23TargetTrash" )
	Frank.pushDialog( event.player, "targetTrash" )
}
already2.result = DONE
ringAlreadyGranted.addDialog( already2, Frank )


//Teach the player to target an object
def targetTrash = Frank.createConversation( "targetTrash", true, "Z23TargetTrash" )

def trash1 = [id:1]
trash1.npctext = "Looks great on you! That's a Solar Rays ring. You can point it at a target and focus solar energy at your target in a powerful flare of light!"
trash1.playertext = "Whoa, cool! I'm a superhero!"
trash1.result = 2
targetTrash.addDialog( trash1, Frank )

def trash2 = [id:2]
trash2.npctext = "Heh. Well, not yet. First you need to learn to focus your ring. Try thinking about *only* that trash can over there. Your ring does the rest!"
trash2.result = 3
targetTrash.addDialog( trash2, Frank )

//this uiResponse is declared from "trash2" below it
//this block occurs when the player targets a trash can for the first time
def uiResponse = { event ->
	event.actor.say("Cool. That was easy!")
	garbageTimer.cancel()
	myManager.schedule(1) { event.player.setQuestFlag( GLOBAL, "Z23NowKill" ); Frank.pushDialog( event.player, "killSomething" ) }
}

def trash3 = [id:3]
trash3.npctext = "<zOMG dialogWidth='260'><![CDATA[<h1><b><font face='Arial' size='14'>Target Selection</font></b></h1><p><font face='Arial' size ='12'><br>Click your mouse on the trash can. You'll know it's selected when it lights up.</font></p><p><img src='help-files/topic-content/default/selectedTrash.png'></p><textformat leading='273'> </textformat><br><p><font face='Arial' size ='12'>Close this tutorial box and try it!</font></p><br>]]></zOMG>"
trash3.flag = "!Z23TargetTrash"
trash3.exec = { event ->        
	runOnUiEvent( event.player, "targetActor", "Trash",  uiResponse ) //wait until after the dialog box is closed before you can "select a trash can"
	garbageTimer = myManager.schedule(5) { event.player.centerPrint( "Click on the trash can to select it." ) }
}
targetTrash.addDialog( trash3, tutorialNPC )

//Now that you can target something...kill it!
def killSomething = Frank.createConversation( "killSomething", true, "Z23NowKill" )

def kill1 = [id:1]
kill1.npctext = "You've got it going now! You can probably feel the energy ready to pour out of the ring. So, let it go, like opening your hand and releasing the wind."
kill1.playertext = "Okay...but that's not usually where I keep my 'wind'."
kill1.result = 2
killSomething.addDialog( kill1, Frank )

def kill2 = [id:2]
kill2.npctext = "<zOMG dialogWidth='350'><![CDATA[<h1><b><font face='Arial' size='14'>Takin' Out the Trash</font></b></h1><font face='Arial' size ='12'><br>Since you have the trash can selected, now you can attack it. There are two ways to do that.<br><br>You can click the ring icon on your ring tray.<br><br><p><img src='help-files/topic-content/default/ringTray.png'></p><textformat leading='48'> </textformat><br/><font face='Arial' size ='12'>Or you can use the '1' key to trigger the ring. (There are eight ring slots, and the 1-8 keys link to each of them, respectively.)<br><br><p><img src='help-files/topic-content/default/ringTrayNumbers.png'></p><textformat leading='60'> </textformat><br><p><font face='Arial' size ='12'>Close this tutorial box and try it!</p>]]></zOMG>"
kill2.flag = [ "Z23TrashCanTargetsOkay", "!Z23NowKill" ]
kill2.exec = { event ->
	killTimer = myManager.schedule(10) {
		event.player.centerPrint( "Select the trash can and click your ring to attack it." ) 
		myManager.schedule(2) { event.player.centerPrint( "Keep attacking until the can is destroyed." ) }
	}
}
kill2.result = DONE
killSomething.addDialog( kill2, tutorialNPC )

//After a garbage can is successfully destroyed, teach about Rage Ranks
def rageRanks = Frank.createConversation( "rageRanks", true, "Z23GarbageCanDestroyed", "!Z23RageRankTalkCompleted" )

def rage1 = [id:1]
rage1.npctext = "Fantastic! You just learned the *basic* way to use your ring. But as you fight, you build up a kind of Rage you can focus through the ring for even more power."
rage1.playertext = "How does that work?"
rage1.result = 2
rageRanks.addDialog( rage1, Frank )

def rage2 = [id:2]
rage2.npctext = "Well...instead of just releasing the energy, try holding it in and letting it build for a bit, kind of like back-pressure in a garden hose when you kink it. Then, let it go, and...BLAM!"
rage2.playertext = "Sounds fun. I'll try that!"
rage2.exec = { event ->
	showWidget( event.actor, "RageMeter", true )
}
rage2.result = 3
rageRanks.addDialog( rage2, Frank )

def rage3 = [id:3]
rage3.npctext = "<zOMG dialogWidth='225'><![CDATA[<h1><b><font face='Arial' size='14'>Rage Ranks</font></b></h1><font face='Arial' size ='12'><br>Click and hold your mouse button (or press and hold the ring hotkey) to push rage into your Rage Meter.<br><br><img src='help-files/topic-content/default/rageMeterFilling.png'><br><br><br><br>Every rank you build past puts more power into your ring!<br><br><p><img src='help-files/topic-content/default/rageMeterRanks.png'></p><textformat leading='65'> </textformat><br>(Click the can to select it first, then power up!)<br><br>]]></zOMG>"
rage3.exec = { event ->
	rageTimer = myManager.schedule(10) {
		event.player.centerPrint( "Select the trash can, then click and HOLD to power your Rage Meter." )
	}
	event.player.grantRage(100)
}
rage3.flag = [ "!Z23GarbageCanDestroyed", "Z23RageRankTalkCompleted" ]
rage3.result = DONE
rageRanks.addDialog( rage3, tutorialNPC )

//After a garbage can is damaged with rage rank 2 or higher, then stop the train 
def success = Frank.createConversation( "success", true, "Z23ExitOkay" )

def succ1 = [id:1]
succ1.npctext = "You're a natural!"
succ1.result = 2
success.addDialog( succ1, Frank )

def succ2 = [id:2]
succ2.npctext = "Well, it looks like the train's stopping at the Barton Town platform. I'm going to head back and grab a few things. Good luck to you, friend."
succ2.playertext = "Thanks, Frank!"
succ2.result = 3
success.addDialog( succ2, Frank )

def succ3 = [id:3]
succ3.npctext = "<h1><b><font face='Arial' size='14'>Leaving the Train</font></b></h1><font face='Arial' size ='12'><br>To leave the train, just close this tutorial box, and click on the doors after they unlock at the station.<br><p><img src='help-files/topic-content/default/exitGears.png'></p><textformat leading='214'> </textformat><br>"
succ3.flag = [ "!Z23ExitOkay", "Z23TrainHasStoppedOnceAlready" ]
succ3.exec = { event ->
	trainStop().toZone() //stop the train
	trainBrake.on()
	frankGoesAway() //make frank leave
	//delay a few seconds to let Frank clear the area and then free the player
	myManager.schedule(4) { freePlayer( event.player ); event.player.centerPrint( " The train is still slowing..." ) }
	//now free the player from Training
	myManager.schedule(7) { event.player.centerPrint( "The doors unlock as the train slides to a stop." ); exitArrow.on(); exitDoors.setUsable( true ); exitDoors.unlock() }
}
succ3.result = DONE
success.addDialog( succ3, tutorialNPC )


//------------------------------------------
// Making Frank Go Away                     
//------------------------------------------

//Frank should move right to the edge of the collision, the door opens, frank is disposed, and then the door closes
goAway = makeNewPatrol()
goAway.addPatrolPoint( "Training_1", 705, 430, 0 )
goAway.addPatrolPoint( "Training_1", 1010, 435, 0 )

def frankGoesAway() {
	//set Frank on a patrol path to the back of the car
	//when he hits the rear door trigger zone, dispose of him
	Frank.setPatrol( goAway )
	Frank.startPatrol()
}

//DISPOSING OF FRANK AT THE REAR DOOR
def frankDisposal = "frankDisposal"
myRooms.Training_1.createTriggerZone( frankDisposal, 990, 370, 1030, 450 )

myManager.onTriggerIn(myRooms.Training_1, frankDisposal ) { event ->
	if( event.actor == Frank ) {
		Frank.dispose()
		sound("Door1Open").toZone()
		myManager.schedule(1) { 
			sound("Door1Closed").toZone()
			//now free the player so he can move again
			myRooms.Training_1.getActorList().each{ 
				if( isPlayer( it ) ) {
					freePlayer( it )
					it.doneWithTraining()
				}
			}
		}
	}
}



//MAIN EXIT DOORS (Training_1)
exitDoors = makeSwitch( "exitDoors", myRooms.Training_1, 730, 350 )

exitDoors.off()
exitDoors.unlock()
exitDoors.setUsable( false )

def openMainDoors = { event ->
	if( isPlayer( event.actor ) ) {
		exitDoors.lock()
		sound("exteriorDoors1").toZone()
		event.actor.centerPrint( "The train's main doors open and you exit the train." )
		myManager.schedule(2) { event.actor.warp( "TrainStation_1", 820, 800) }
	}
}
	
exitDoors.whenOn( openMainDoors )

//THE TRAIN BRAKE (so I can make it move)
trainBrake = makeSwitch( "trainBrake", myRooms.Training_1, 195, 400 )

trainBrake.lock()
trainBrake.off()
trainBrake.setUsable( false )
