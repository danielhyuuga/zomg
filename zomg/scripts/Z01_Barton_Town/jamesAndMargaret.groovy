//Script created by gfern

import com.gaiaonline.mmo.battle.actor.Player;
import com.gaiaonline.mmo.battle.script.*;

def james = spawnNPC("BFG-James", myRooms.BARTON_102, 1225, 745)
james.setRotation( 45 )
james.setDisplayName( "James" )

def Margaret = spawnNPC("BFG-Margaret", myRooms.BARTON_102, 1320, 590)
Margaret.setRotation( 45 )
Margaret.setDisplayName( "Margaret" )

//--------------------------------------------------------------------------------------------------------
//Default conversation for James                                                                          
//--------------------------------------------------------------------------------------------------------
def jamesDefault = james.createConversation("jamesDefault", true, "!QuestStarted_50:2", "!QuestStarted_290")

def default1 = [id:1]
default1.npctext = "Would you look at that Margaret? I've had better conversations with a statue."
default1.options = []
default1.options << [text:"What's wrong with a little silence?", result:2]
default1.options << [text:"Afraid she's a better guard?", result:3]
default1.options << [text:"I bet I could get her to say something.", result:4]
jamesDefault.addDialog(default1, james)

def default2 = [id:2]
default2.npctext = "Believe me, I've got nothing against silence. But if she doesn't say something soon people are gonna start thinking she's a corpse."
default2.options = []
default2.options << [text:"I think you're just worried that she's a better guard than you.", result:3]
default2.options << [text:"I could get her talking.", result:4]
jamesDefault.addDialog(default2, james)

def default3 = [id:3]
default3.npctext = "A better guard than me? HAH! Maybe if we were here just to keep the crows away."
default3.options = []
default3.options << [text:"I kind of like the quiet.", result:2]
default3.options << [text:"Pfft. I'll get her to talk.", result:4]
jamesDefault.addDialog(default3, james)

def default4 = [id:4]
default4.npctext = "Right. You do that. Ask her about the weather. HAH!"
default4.result = 5
jamesDefault.addDialog(default4, james)

def default5 = [id:5]
default5.playertext = "I'll do just that."
default5.result = DONE
jamesDefault.addDialog(default5, james)

//--------------------------------------------------------------------------------------------------------
//James conversation when player is on quest from Cindy Donovinh                                          
//--------------------------------------------------------------------------------------------------------
def jamesNews = james.createConversation("jamesNews", true, "QuestStarted_50:2", "!QuestStarted_290")

def news1 = [id:1]
news1.npctext = "Hey, did you hear the one about the guard that thought she was a mime?"
news1.result = 2
jamesNews.addDialog(news1, james)

def news2 = [id:2]
news2.playertext = "I am here to interview you for Channel Nine News. Do you have time?"
news2.result = 3
jamesNews.addDialog(news2, james)

def news3 = [id:3]
news3.npctext = "Are you sure you wouldn't rather interview Margaret? Regular chatter-box that one!"
news3.result = 4
jamesNews.addDialog(news3, james)

def news4 = [id:4]
news4.playertext = "Very funny. So tell me, James, how is your new post at the Guild Hall treating you?"
news4.result = 5
jamesNews.addDialog(news4, james)

def news5 = [id:5]
news5.npctext = "Oh, it's great. Way better than my last post with Margaret. With the forums nearby there's always someone to talk to."
news5.result = 6
jamesNews.addDialog(news5, james)

def news6 = [id:6]
news6.playertext = "Would you recommend the forums to new Gaians?"
news6.result = 7
jamesNews.addDialog(news6, james)

def news7 = [id:7]
news7.npctext = "Definitely! They're the best place to meet people and sometimes they're just a riot. Don't tell Leon, but I usually spend most of my shift there!"
news7.result = 8
jamesNews.addDialog(news7, james)

def news8 = [id:8]
news8.playertext = "Your secret is safe with me. Well, that's all we have time for today. Back to Cindy Donovinh in the Channel Nine News studio."
news8.quest = 50
news8.exec = { event ->
	event.player.removeMiniMapQuestActorName("BFG-James")
	event.player.addMiniMapQuestActorName("Cindy-VQS")
}
news8.result = DONE
jamesNews.addDialog(news8, james)