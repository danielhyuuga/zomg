//Script created by gfern

import com.gaiaonline.mmo.battle.actor.Player;
import com.gaiaonline.mmo.battle.script.*;

def cindyDonovinh = spawnNPC("Cindy-VQS", myRooms.BARTON_304, 475, 635)
cindyDonovinh.setRotation( 135 )
cindyDonovinh.setDisplayName( "Cindy Donovinh" )

//--------------------------------------------------------------------------------------
// Convo from TrainStation                                                              
//--------------------------------------------------------------------------------------

def cindyFromTraining = cindyDonovinh.createConversation( "cindyFromTraining", true, "Z22GoTalkToCindy" )
cindyFromTraining.setUrgent( false )

def train1 = [id:1]
train1.npctext = "What can I help you with? I'm just warming up for my next newscast, so make it quick, please."
train1.playertext = "Okay. I was asked to speak with you by Barry, the conductor down at Old Station. He wanted me to tell you about a potential threat to the town."
train1.result = 2
cindyFromTraining.addDialog( train1, cindyDonovinh )

def train2 = [id:2]
train2.npctext = "It never rains, but it pours. What is it now?"
train2.playertext = "It's the gramsters. He says they're kind of like grunnies, but sort of a mutant hamster instead. They're all over the sewers below the town and he thinks that might be a bad thing for us, long-term."
train2.result = 3
cindyFromTraining.addDialog( train2, cindyDonovinh )

def train3 = [id:3]
train3.npctext = "Okay, I'll check it out soon. Thanks a lot, friend. It's people like you that keep the press alive and current with the happenings that matter to our viewers."
train3.playertext = "Ummm, okay. But what about the grunnies and gramsters?"
train3.result = 4
cindyFromTraining.addDialog( train3, cindyDonovinh )

def train4 = [id:4]
train4.npctext = "If you'd like to know more about grunnies, then go on into the newsroom and check out the historical archives. As for the new creatures...'gramsters' did you call them?...well, we've got more news like that than we know what to do with. Step aside, please...I'm about to prep for the camera."
train4.result = DONE
train4.flag = "!Z22GoTalkToCindy"
train4.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Cindy-VQS" )
}
cindyFromTraining.addDialog( train4, cindyDonovinh )

//--------------------------------------------------------------------------------------
//Default conversation for Cindy Donovinh                                               
//--------------------------------------------------------------------------------------
def cindyDefault = cindyDonovinh.createConversation("cindyDefault", true, "!Z22GoTalkToCindy", "!QuestStarted_77", "!QuestCompleted_77", "!QuestStarted_50", "!QuestCompleted_50", "!QuestStarted_52", "!QuestCompleted_52")

def default1 = [id:1]
default1.npctext = "Good evening and welcome to Channel Nine News, your source for the Zurg, the Animated, G-Corp, and everything else Gaia. This is Cindy Donovinh reporting."
default1.options = []
default1.options << [text:"Reporting? Uhm, reporting what?", result:2]
default1.options << [text:"What is 'Channel 9?'", result:10]
default1.options << [text:"Why are you the only reporter I see?", result:5]
cindyDefault.addDialog(default1, cindyDonovinh)

def default2 = [id:2]
default2.npctext = "I'm here, live, with... I'm sorry, what did you say your name was?"
default2.options = []
default2.options << [text:"My name is %p.", result:3]
default2.options << [text:"You're interviewing me? There isn't even a camera here!", result:5]
cindyDefault.addDialog(default2, cindyDonovinh)

def default3 = [id:3]
default3.npctext = "I'm here, live, with %p - I have it on good authority that %p is very involved with the Animated? Tell us, %p, what do you know?"
default3.result = 4
cindyDefault.addDialog(default3, cindyDonovinh)

def default4 = [id:4]
default4.playertext = "You must have me confused with someone else. I don't know anything!!"
default4.result = 5
cindyDefault.addDialog(default4, cindyDonovinh)

def default5 = [id:5]
default5.npctext = "CUT! Can we talk, off the record?"
default5.result = 6
cindyDefault.addDialog(default5, cindyDonovinh)

def default6 = [id:6]
default6.playertext = "Off the record? What are you...? Nevermind. Sure. Let's talk 'off the record.'"
default6.result = 7
cindyDefault.addDialog(default6, cindyDonovinh)

def default7 = [id:7]
default7.npctext = "Channel 9 employs a number of roving reporters to bring Gaia the most comprehensive news programming on television. We're always looking for new talent, but if you don't have anything to say, I can't use you. Get out there and get me a story!"
default7.options = []
default7.options << [text:"A story? Where am I supposed to find a story?", result:9]
default7.options << [text:"I'm not doing your job for you.", result:8]
cindyDefault.addDialog(default7, cindyDonovinh)

def default8 = [id:8]
default8.npctext = "Thank you for watching. For %p, this is Cindy Donovinh wishing you a good evening and a pleasant tomorrow."
default8.result = DONE
cindyDefault.addDialog(default8, cindyDonovinh)

def default9 = [id:9]
default9.npctext = "Coming after the break, %p has a live interview with one of Barton Town's newest Gaians, NewBea, at the fountain in the center of town."
default9.quest = 77
default9.exec = { event -> event.player.addMiniMapQuestActorName("NewBea") }
default9.result = DONE
cindyDefault.addDialog(default9, cindyDonovinh)

def default10 = [id:10]
default10.npctext = "This is Cindy Donovinh, reminding you that Channel Nine News is the leading source for breaking news in Barton Town and throughout Gaia."
default10.playertext = "Leading source?! You look like the only reporter here and I don't even see a camera!"
default10.result = 5
cindyDefault.addDialog(default10, cindyDonovinh)

//------------------------------------------------------------------------------------------
//Conversation when NewBea is in progress                                                   
//------------------------------------------------------------------------------------------
def cindyNewbeaActive = cindyDonovinh.createConversation("cindyNewbeaActive", true, "QuestStarted_77:2")

def newbeaActive1 = [id:1]
newbeaActive1.npctext = "Remember, after the break we will have a special interview with NewBea, Barton Town's newest resident, live from the fountain in the center of town."
newbeaActive1.result = DONE
cindyNewbeaActive.addDialog(newbeaActive1, cindyDonovinh)

//------------------------------------------------------------------------------------------
//Conversation when NewBea has been interviewed.                                            
//------------------------------------------------------------------------------------------
def cindyNewbeaComplete = cindyDonovinh.createConversation("cindyNewbeaComplete", true, "QuestStarted_77:3")

def newbeaComplete1 = [id:1]
newbeaComplete1.npctext = "And now, here is %p with a special report on a new citizen of Barton Town, NewBea."
newbeaComplete1.result = 2
cindyNewbeaComplete.addDialog(newbeaComplete1, cindyDonovinh)

def newbeaComplete2 = [id:2]
newbeaComplete2.playertext = "No, Cindy, I broadcast the interview 'live.' Didn't you watch it?"
newbeaComplete2.result = 3
cindyNewbeaComplete.addDialog(newbeaComplete2, cindyDonovinh)

def newbeaComplete3 = [id:3]
newbeaComplete3.npctext = "Of course I did! Another fine example of breaking news from Channel Nine. Are you ready for your second interview?"
newbeaComplete3.options = []
newbeaComplete3.options << [text:"Yes. Who am I interviewing next?", result:4]
newbeaComplete3.options << [text:"Not yet.", result:5]
cindyNewbeaComplete.addDialog(newbeaComplete3, cindyDonovinh)

def newbeaComplete4 = [id:4]
newbeaComplete4.npctext = "James has just been posted outside the Tavern, west of the Barton Town fountain. Channel Nine News will conduct an exclusive interview."
newbeaComplete4.quest = 50
newbeaComplete4.exec = { event -> 
	event.player.updateQuest(77, "Cindy-VQS")
	event.player.removeMiniMapQuestActorName("Cindy-VQS")
	event.player.addMiniMapQuestActorName("BFG-James")
}
newbeaComplete4.result = 6
cindyNewbeaComplete.addDialog(newbeaComplete4, cindyDonovinh)

def newbeaComplete5 = [id:5]
newbeaComplete5.npctext = "It's time for a break. We'll be back after these messages."
newbeaComplete5.quest = 77
newbeaComplete5.exec = { event -> event.player.removeMiniMapQuestActorName("Cindy-VQS") }
newbeaComplete5.result = DONE
cindyNewbeaComplete.addDialog(newbeaComplete5, cindyDonovinh)

//------------------------------------------------------------------------------------------------------------
//Interim conversation to give James quest if player is not ready upon completion of NewBea interview         
//------------------------------------------------------------------------------------------------------------
def cindyJamesBreak = cindyDonovinh.createConversation("cindyJamesBreak", true, "QuestCompleted_77", "!QuestStarted_50", "!QuestCompleted_50")

def jamesBreak1 = [id:1]
jamesBreak1.npctext = "Welcome back to Channel Nine News. I am Cindy Donovinh. Let's go to %p."
jamesBreak1.options = []
jamesBreak1.options << [text:"Thanks, Cindy. After the break I'll be interviewing James at the Tavern, west of the fountain.", result:2]
jamesBreak1.options << [text:"No... ind... aking u... ca... ar yo...", result:3]
cindyJamesBreak.addDialog(jamesBreak1, cindyDonovinh)

def jamesBreak2 = [id:2]
jamesBreak2.npctext = "Thanks, %p. We'll look forward to your interview."
jamesBreak2.quest = 50
jamesBreak2.exec = { event ->
	event.player.removeMiniMapQuestActorName("Cindy-VQS")
	event.player.addMiniMapQuestActorName("BFG-James")
}
jamesBreak2.result = DONE
cindyJamesBreak.addDialog(jamesBreak2, cindyDonovinh)

def jamesBreak3 = [id:3]
jamesBreak3.npctext = "We seem to be experiencing technical difficulties. Check back later for %p's story."
jamesBreak3.result = DONE
cindyJamesBreak.addDialog(jamesBreak3, cindyDonovinh)

//-------------------------------------------------------------------------------------------------------------
//Conversation for player returning to Cindy while on James Interview quest                                    
//-------------------------------------------------------------------------------------------------------------
def cindyJamesActive = cindyDonovinh.createConversation("cindyJamesActive", true, "QuestStarted_50:2")

def jamesActive1 = [id:1]
jamesActive1.npctext = "Coming up next, we have %p with a special interview of James, who has just been posted outside the Tavern."
jamesActive1.result = DONE
cindyJamesActive.addDialog(jamesActive1, cindyDonovinh)

//-------------------------------------------------------------------------------------------------------------
//Conversation for player returning to Cindy after completing interview with James                             
//-------------------------------------------------------------------------------------------------------------
def cindyJamesComplete = cindyDonovinh.createConversation("cindyJamesComplete", true, "QuestStarted_50:3")

def jamesComplete1 = [id:1]
jamesComplete1.npctext = "Once again, that was Channel Nine's very own roving reporter, %p, with an interview of James. Thanks %p."
jamesComplete1.result = 2
cindyJamesComplete.addDialog(jamesComplete1, cindyDonovinh)

def jamesComplete2 = [id:2]
jamesComplete2.playertext = "Thank you Cindy. That James is quite a character!"
jamesComplete2.result = 3
cindyJamesComplete.addDialog(jamesComplete2, cindyDonovinh)

def jamesComplete3 = [id:3]
jamesComplete3.npctext = "Channel Nine News reporter, %p, recently interviewed Julian about the boy who fell down the northwest well."
jamesComplete3.options = []
jamesComplete3.options << [text:"Thanks Cindy, I'll be bringing you that story after these messages.", result:4]
jamesComplete3.options << [text:"Back to me? No, Cindy, back to you.", result:5]
cindyJamesComplete.addDialog(jamesComplete3, cindyDonovinh)

def jamesComplete4 = [id:4]  
jamesComplete4.npctext = "We look forward to hearing from you, %p."
jamesComplete4.quest = 52
jamesComplete4.exec = { event -> 
	event.player.updateQuest(50, "Cindy-VQS")
	event.player.removeMiniMapQuestActorName("Cindy-VQS")
	event.player.addMiniMapQuestActorName("BFG-Julian")
	if( event.player.isOnQuest( 287, 5 ) ) {
		event.player.removeMiniMapQuestActorName( "Ellie-VQS" )
	}
}
jamesComplete4.result = DONE
cindyJamesComplete.addDialog(jamesComplete4, cindyDonovinh)

def jamesComplete5 = [id:5]
jamesComplete5.npctext = "I... uh... thank you, %p."
jamesComplete5.quest = 50
jamesComplete5.exec = { event -> event.player.removeMiniMapQuestActorName("Cindy-VQS") }
jamesComplete5.result = DONE
cindyJamesComplete.addDialog(jamesComplete5, cindyDonovinh)

//----------------------------------------------------------------------------------------------------
//Interim conversation to give Julian if player is not ready upon completion of James interview       
//----------------------------------------------------------------------------------------------------
def cindyJulianBreak = cindyDonovinh.createConversation("cindyJulianBreak", true, "QuestCompleted_50", "!QuestStarted_52", "!QuestCompleted_52")

def julianBreak1 = [id:1]
julianBreak1.npctext = "Now let's go to %p with an exlusive interview of Julian about the boy who fell down the northwest well."
julianBreak1.options = []
julianBreak1.options << [text:"I'll have this story for you after these messages.", result: 2]
julianBreak1.options << [text:"Just let me know when I'm on. Hello? Cindy? Anyone? I think we lost the feed.", result: DONE]
cindyJulianBreak.addDialog(julianBreak1, cindyDonovinh)

def julianBreak2 = [id:2]
julianBreak2.npctext = "We'll be looking forward to it."
julianBreak2.quest = 52
julianBreak2.exec = { event -> event.player.addMiniMapQuestActorName("BFG-Julian") }
julianBreak2.result = DONE
cindyJulianBreak.addDialog(julianBreak2, cindyDonovinh)

//----------------------------------------------------------------------------------------------------
//Conversation for player returning to Cindy while on Julian Interview quest                          
//----------------------------------------------------------------------------------------------------
def cindyJulianActive = cindyDonovinh.createConversation("cindyJulianActive", true, "QuestStarted_52:2")

def julianActive1 = [id:1]
julianActive1.npctext = "Up next: Did someone fall down the well? %p interviews Julian to get to the bottom of this story."
julianActive1.result = DONE
cindyJulianActive.addDialog(julianActive1, cindyDonovinh)

//----------------------------------------------------------------------------------------------------
//Conversation for player returning to Cindy after completing interview with Julian                   
//----------------------------------------------------------------------------------------------------
def cindyJulianComplete = cindyDonovinh.createConversation("cindyJulianComplete", true, "QuestStarted_52:3")

def julianComplete1 = [id:1]
julianComplete1.npctext = "Thank you, %p, for your interview of Julian. Very hard-hitting."
julianComplete1.result = 2
cindyJulianComplete.addDialog(julianComplete1, cindyDonovinh)

def julianComplete2 = [id:2]
julianComplete2.playertext = "Yes, Cindy. It seems Julian is a bit unhinged..."
julianComplete2.result = 3
cindyJulianComplete.addDialog(julianComplete2, cindyDonovinh)

def julianComplete3 = [id:3]
julianComplete3.npctext = "These times are tough on everyone, %p. And that's...the way it is. This is Cindy Donovinh...signing off! Good...day!"
julianComplete3.quest = 52
julianComplete3.exec = { event -> event.player.removeMiniMapQuestActorName("Cindy-VQS") }
julianComplete3.result = DONE
cindyJulianComplete.addDialog(julianComplete3, cindyDonovinh)

//----------------------------------------------------------------------------------------------------
//Conversation for Cindy when player has completed all quests                                         
//----------------------------------------------------------------------------------------------------
def cindyAllComplete = cindyDonovinh.createConversation("cindyAllComplete", true, "QuestCompleted_52")

def allComplete1 = [id:1]
allComplete1.npctext = "This is Cindy Donovinh reminding you that if you wish to submit footage to Channel Nine or view our archives simply enter the studio."
allComplete1.options = []
allComplete1.options << [text:"What do you have in your archives?", result: 2]
allComplete1.options << [text:"Maybe I'll check it out later.", result: 3]
cindyAllComplete.addDialog(allComplete1, cindyDonovinh)

def allComplete2 = [id:2]
allComplete2.npctext = "Channel Nine News is Gaia's leading news source. Our archive contains all our past stories. You can read about all of Gaia's important events in our archive."
allComplete2.options = []
allComplete2.options << [text:"Hmm, what are the most exciting stories? I want to start with them!", result: 4]
allComplete2.options << [text:"Cool, I'm going to read everything!", reuslt: 5]
allComplete2.options << [text:"Uh, whatever.", result: 3]
cindyAllComplete.addDialog(allComplete2, cindyDonovinh)

def allComplete3 = [id:3]
allComplete3.npctext = "Well, just remember that Channel Nine is the number one name in Gaian news! Stop by and check out our archive some other time."
allComplete3.result = DONE
cindyAllComplete.addDialog(allComplete3, cindyDonovinh)

def allComplete4 = [id:4]
allComplete4.npctext = "If it's excitement you want make sure you check out Gambino's tower, the Zurg, and the Grunnies!"
allComplete4.options = []
allComplete4.options << [text:"What's so exciting about a tower?", result: 6]
allComplete4.options << [text:"The Zurg? Is that some kind of monster?", result: 7]
allComplete4.options << [text:"Awww, Grunnies sound cute!", result: 8]
cindyAllComplete.addDialog(allComplete4, cindyDonovinh)

def allComplete5 = [id:5]
allComplete5.npctext = "You're my kind of person! Just click on the door behind me to view the archive."
allComplete5.result = DONE
cindyAllComplete.addDialog(allComplete5, cindyDonovinh)

def allComplete6 = [id:6]
allComplete6.npctext = "You mean aside from the fact that it was tall enough to stretch from Gambino Island to Durem laying on its side?"
allComplete6.options = []
allComplete6.options << [text:"What, how could you possibly know it was that tall?", result: 9]
allComplete6.options << [text:"From where to where?", result: 10]
cindyAllComplete.addDialog(allComplete6, cindyDonovinh)

def allComplete7 = [id:7]
allComplete7.npctext = "Monsters? I wouldn't say that at all. In fact, they were quite pleasant, if a bit other-worldly."
allComplete7.options = []
allComplete7.options << [text:"Other-worldly as in ghosts?", result:11]
allComplete7.options << [text:"What do you mean? aliens?", result: 12]
cindyAllComplete.addDialog(allComplete7, cindyDonovinh)

def allComplete8 = [id:8]
allComplete8.npctext = "Cute?! Hmmm, I suppose maybe they look cute but, believe me, there's nothing cute about something trying to gnaw through your skull and eat your brain!"
allComplete8.playertext = "Erp. Eat my brain? What kind of creature are these Grunnies!"
allComplete8.result = 13
cindyAllComplete.addDialog(allComplete8, cindyDonovinh)

def allComplete9 = [id:9]
allComplete9.npctext = "Because it fell, and the top landed in Durem."
allComplete9.playertext = "Whoa! How did that happen?!"
allComplete9.result = 14
cindyAllComplete.addDialog(allComplete9, cindyDonovinh)

def allComplete10 = [id:10]
allComplete10.npctext = "Gambino Island to Durem: they're two cities. I'm just trying to illustrate the un-believable height of the tower."
allComplete10.playertext = "That's an odd way to illustrate height..."
allComplete10.result = 15
cindyAllComplete.addDialog(allComplete10, cindyDonovinh)

def allComplete11 = [id:11]
allComplete11.npctext = "Good guess, but not exactly. Perhaps I should have said 'out of this world' instead. The Zurg are aliens that briefly visited Gaia."
allComplete11.options = []
allComplete11.options << [text:"Wow! I never even believed in aliens. Why did they come here?", result: 16]
allComplete11.options << [text:"I knew it! They're real! Did they have flying saucers and everything?!", result: 16]
allComplete11.options << [text:"Hrm, that doesn't seem that exciting to me. What else did you say I should look into?", result: 17]
cindyAllComplete.addDialog(allComplete11, cindyDonovinh)

def allComplete12 = [id:12]
allComplete12.npctext = "Nice guess! The Zurg were indeed an alien race that briefly visited Gaia."
allComplete12.options = []
allComplete12.options << [text:"Wow! I never even believed in aliens. Why did they come here?", result: 16]
allComplete12.options << [text:"I knew it! They're real! Did they  have flying saucers and everything?!", result: 16]
allComplete12.options << [text:"Hrm, that doesn't seem that exciting to me. What else did you say I should look into?", result: 17]
cindyAllComplete.addDialog(allComplete12, cindyDonovinh)

def allComplete13 = [id:13]
allComplete13.npctext = "Hmmm, I can't even describe them really. Zombie Bunnies, maybe? You should check out our archive, there are pictures of them in the arcticle entitled Halloween '05, Part 1."
allComplete13.options = []
allComplete13.options << [text:"I'll go do that!", result: DONE]
allComplete13.options << [text:"Hmmm, maybe later. What else did you say I should take a look at?", result: 18]
cindyAllComplete.addDialog(allComplete13, cindyDonovinh)

def allComplete14 = [id:14]
allComplete14.npctext = "You can find out, in our archives! Read the article from April 1, 2005."
allComplete14.options = []
allComplete14.options << [text:"I will visit your archives immediately!", result: DONE]
allComplete14.options << [text:"Hmmm, I think I can imagine it well enough. What other stories did you say were exciting?", result: 19]
cindyAllComplete.addDialog(allComplete14, cindyDonovinh)

def allComplete15 = [id:15]
allComplete15.npctext = "You wouldn't think that if you'd seen the top of that tower as it landed in Durem."
allComplete15.playertext = "Wait, it fell over and actually landed *in* Durem? How did that happen?!"
allComplete15.result = 14
cindyAllComplete.addDialog(allComplete15, cindyDonovinh)

def allComplete16 = [id:16]
allComplete16.npctext = "It's too long a story to recount verbally. Read the article entitled We Come In... Piece? It explains everything."
allComplete16.options = []
allComplete16.options << [text:"I'll go do that.", result: DONE]
allComplete16.options << [text:"Hmmm, that doesn't seem very interesting to me. What else exciting have you written articles on?", result: 17]
cindyAllComplete.addDialog(allComplete16, cindyDonovinh)

def allComplete17 = [id:17]
allComplete17.npctext = "I'd say you have to read about Gambino's tower and the Grunnies!"
allComplete17.options = []
allComplete17.options << [text:"What's so exciting about a tower?", result: 6]
allComplete17.options << [text:"Awww, Grunnies sound cute!", result: 8]
cindyAllComplete.addDialog(allComplete17, cindyDonovinh)

def allComplete18 = [id:18]
allComplete18.npctext = "You definitely will want to check out the articles on Gambino's tower and the Zurg"
allComplete18.options = []
allComplete18.options << [text:"What's so exciting about a tower?", result: 6]
allComplete18.options << [text:"The Zurg? Is that some kind of monster?", result: 7]
cindyAllComplete.addDialog(allComplete18, cindyDonovinh)

def allComplete19 = [id:19]
allComplete19.npctext = "Hmmm, the arcticles on the Grunnies and the Zurg are both really exciting."
allComplete19.options = []
allComplete19.options << [text:"The Zurg? Is that some kind of monster?", result: 7]
allComplete19.options << [text:"Awww, Grunnies sound cute!", result: 8]
cindyAllComplete.addDialog(allComplete19, cindyDonovinh)