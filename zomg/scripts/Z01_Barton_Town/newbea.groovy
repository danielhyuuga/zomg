//Script created by gfern
import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

Newbea = spawnNPC("NewBea", myRooms.BARTON_103, 1245, 785)
Newbea.setDisplayName( "NewBea" )
Newbea.setWanderBehavior( 50, 150, 3, 7, 250 )
Newbea.startWander(true)

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Newbea.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/newbea_strip.png")
}
//XMAS TODO: Make references in Newbea's dialog to the fountain change to "the tree" when Christmas is in effect.

//STOP MOVING WHEN TALKING
def stopMoving(event) {
	Newbea.pause()
}

//START MOVING AFTER DONE TALKING
def startMoving(event) {
	Newbea.pauseResume()
}

//-------------------------------------------------------------------------------
//NewBea's default conversation                                                  
//-------------------------------------------------------------------------------
def NewbeaDefault = Newbea.createConversation("NewbeaDefault", true, "!QuestStarted_77:2", "!Z1_Newbea_Five", "!Z1_Newbea_Ten", "!Z1_Newbea_TwentyFive", "!Z1_Newbea_Hundred", "!Z1_Newbea_TenTwo", "!Z1_Newbea_TwentyFiveTwo", "!Z1_Newbea_HundredTwo", "!Z1_Newbea_Thousand", "!Z1_Newbea_FiveThousand", "!Z1_Newbea_TenThousand")

def default1 = [id:1]
default1.npctext = "HELLO GAIA!"
default1.options = []
default1.options << [text:"OMG, are you new?!", result:2, exec: { event -> stopMoving() } ]
default1.options << [text:"Isn't the fountain beautiful?", result:4, exec: { event -> stopMoving() } ]
NewbeaDefault.addDialog(default1, Newbea)

def default2 = [id:2]
default2.npctext = "Hehe, I just got here!"
default2.options = []
default2.options << [text:"Welcome to Gaia.", result:3]
default2.options << [text:"Hey NewBea, enjoying the fountain?", result:4]
NewbeaDefault.addDialog(default2, Newbea)

def default3 = [id:3]
default3.npctext = "Thanx! Can I have some gold?"
default3.options = []
default3.options << [text:"Sure. Here you go.", result:5]
default3.options << [text:"You should go post on the forums if you want gold.", result:8]
default3.options << [text:"No way, noob.", result:6]
NewbeaDefault.addDialog(default3, Newbea)

def default4 = [id:4]
default4.npctext = "OMG yes! It's gorgeous! It's all splashy and wet but not very talky. I wish I could find the forums."
default4.result = 7
NewbeaDefault.addDialog(default4, Newbea)

def default5 = [id:5]
default5.playertext = "How much gold will you give NewBea?"
default5.options = []
default5.options << [text:"5 gold.", exec: { event -> runOnDeduct( event.actor, "g", 5, { event.player.setGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaFive" ); println "@@@@ ${event.player} gave Newbea 5 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
default5.options << [text:"10 gold.",  exec: { event -> runOnDeduct( event.actor, "g", 10, { event.player.setGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaTen" ); println "@@@@ ${event.player} gave Newbea 10 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
default5.options << [text:"25 gold.", exec: { event -> runOnDeduct( event.actor, "g", 25, { event.player.setGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaTwentyFive" ); println "@@@@ ${event.player} gave Newbea 25 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
default5.options << [text:"100 gold.", exec: { event -> runOnDeduct( event.actor, "g", 100, { event.player.setGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaHundred" ); println "@@@@ ${event.player} gave Newbea 100 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
NewbeaDefault.addDialog(default5, Newbea)

def default6 = [id:6]
default6.npctext = "WHY WON'T YOU HELP ME?!?!"
default6.exec = { event ->
	startMoving()
}
default6.result = DONE
NewbeaDefault.addDialog(default6, Newbea)

def default7 = [id:7]
default7.playertext = "Wow. You really are new."
default7.exec = { event ->
	startMoving()
}
default7.result = DONE
NewbeaDefault.addDialog(default7, Newbea)

def default8 = [id:8]
default8.npctext = "I was just looking for the forums but I got lost and saw this pretty fountain so here I am! I haven't been having much fun yet, though... I just want to chat."
default8.result = 7
NewbeaDefault.addDialog(default8, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 5 gold                                    
//--------------------------------------------------------------------------------
def newbeaFive = Newbea.createConversation("newbeaFive", true, "Z1_Newbea_Five")

def newbeaFive1 = [id:1]
newbeaFive1.npctext = "That's it?! Can I have some more?"
newbeaFive1.options = []
newbeaFive1.options << [text:"Sure.", result:2]
newbeaFive1.options << [text:"No.", result:3]
newbeaFive.addDialog(newbeaFive1, Newbea)

def newbeaFive2 = [id:2]
newbeaFive2.playertext = "How much more gold will you give NewBea?"
newbeaFive2.options = []
newbeaFive2.options << [text:"10 gold.", exec: { event -> runOnDeduct( event.actor, "g", 10, { event.player.setGlobalFlag( "Z1_Newbea_TenTwo" ); event.player.unsetGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaTenTwo" ); println "@@@@ ${event.player} gave Newbea 10 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaFive2.options << [text:"25 gold.", exec: { event -> runOnDeduct( event.actor, "g", 25, { event.player.setGlobalFlag( "Z1_Newbea_TwentyFiveTwo" ); event.player.unsetGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaTwentyFiveTwo" ); println "@@@@ ${event.player} gave Newbea 25 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaFive2.options << [text:"100 gold.", exec: { event -> runOnDeduct( event.actor, "g", 100, { event.player.setGlobalFlag( "Z1_Newbea_HundredTwo" ); event.player.unsetGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaHundredTwo" ); println "@@@@ ${event.player} gave Newbea 100 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaFive2.options << [text:"None.", exec: { event -> event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Five" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) }, result: DONE]
newbeaFive.addDialog(newbeaFive2, Newbea)

def newbeaFive3 = [id:3]
newbeaFive3.npctext = "WHY WON'T YOU HELP ME?!?!"
newbeaFive3.exec = { event -> startMoving() }
newbeaFive3.flag = "!Z1_Newbea_Five"
newbeaFive3.result = DONE
newbeaFive.addDialog(newbeaFive3, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 10 gold                                   
//--------------------------------------------------------------------------------
def newbeaTen = Newbea.createConversation("newbeaTen", true, "Z1_Newbea_Ten")

def newbeaTen1 = [id:1]
newbeaTen1.npctext = "I guess ten is better than nothing. Can I have some more?"
newbeaTen1.options = []
newbeaTen1.options << [text:"Sure.", result:2]
newbeaTen1.options << [text:"No.", result:3]
newbeaTen.addDialog(newbeaTen1, Newbea)

def newbeaTen2 = [id:2]
newbeaTen2.playertext = "How much more gold will you give NewBea?"
newbeaTen2.options = []
newbeaTen2.options << [text:"25 gold.", exec: { event -> runOnDeduct( event.actor, "g", 25, { event.player.setGlobalFlag( "Z1_Newbea_TwentyFiveTwo" ); event.player.unsetGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaTwentyFiveTwo" ); println "@@@@ ${event.player} gave Newbea 25 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaTen2.options << [text:"100 gold.", exec: { event -> runOnDeduct( event.actor, "g", 100, { event.player.setGlobalFlag( "Z1_Newbea_HundredTwo" ); event.player.unsetGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaHundredTwo" ); println "@@@@ ${event.player} gave Newbea 100 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaTen2.options << [text:"1000 gold.", exec: { event -> runOnDeduct( event.actor, "g", 1000, { event.player.setGlobalFlag( "Z1_Newbea_Thousand" ); event.player.unsetGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaThousand" ); println "@@@@ ${event.player} gave Newbea 1000 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaTen2.options << [text:"None.", exec: { event -> event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Ten" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) }, result: DONE]
newbeaTen.addDialog(newbeaTen2, Newbea)

def newbeaTen3 = [id:3]
newbeaTen3.npctext = "WHY WON'T YOU HELP ME?!?!"
newbeaTen3.exec = { event -> startMoving() }
newbeaTen3.flag = "!Z1_Newbea_Ten"
newbeaTen3.result = DONE
newbeaTen.addDialog(newbeaTen3, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 25 gold                                   
//--------------------------------------------------------------------------------
def newbeaTwentyFive = Newbea.createConversation("newbeaTwentyFive", true, "Z1_Newbea_TwentyFive")

def newbeaTwentyFive1 = [id:1]
newbeaTwentyFive1.npctext = "Maybe that's enough to buy something! Can I have some more?"
newbeaTwentyFive1.options = []
newbeaTwentyFive1.options << [text:"Sure.", result:2]
newbeaTwentyFive1.options << [text:"No.", result:3]
newbeaTwentyFive.addDialog(newbeaTwentyFive1, Newbea)

def newbeaTwentyFive2 = [id:2]
newbeaTwentyFive2.playertext = "How much more gold will you give NewBea?"
newbeaTwentyFive2.options = []
newbeaTwentyFive2.options << [text:"100 gold.", exec: { event -> runOnDeduct( event.actor, "g", 100, { event.player.setGlobalFlag( "Z1_Newbea_HundredTwo" ); event.player.unsetGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaHundredTwo" ); println "@@@@ ${event.player} gave Newbea 100 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaTwentyFive2.options << [text:"1000 gold.", exec: { event -> runOnDeduct( event.actor, "g", 1000, { event.player.setGlobalFlag( "Z1_Newbea_Thousand" ); event.player.unsetGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaThousand" ); println "@@@@ ${event.player} gave Newbea 1000 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaTwentyFive2.options << [text:"5000 gold.", exec: { event -> runOnDeduct( event.actor, "g", 5000, { event.player.setGlobalFlag( "Z1_Newbea_FiveThousand" ); event.player.unsetGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaFiveThousand" ); println "@@@@ ${event.player} gave Newbea 5000 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaTwentyFive2.options << [text:"None.", exec: { event -> event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_TwentyFive" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) }, result: DONE]
newbeaTwentyFive.addDialog(newbeaTwentyFive2, Newbea)

def newbeaTwentyFive3 = [id:3]
newbeaTwentyFive3.npctext = "WHY WON'T YOU HELP ME?!?!"
newbeaTwentyFive3.exec = { event -> startMoving() }
newbeaTwentyFive3.flag = "!Z1_Newbea_TwentyFive"
newbeaTwentyFive3.result = DONE
newbeaTwentyFive.addDialog(newbeaTwentyFive3, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 100 gold                                  
//--------------------------------------------------------------------------------
def newbeaHundred = Newbea.createConversation("newbeaHundred", true, "Z1_Newbea_Hundred")

def newbeaHundred1 = [id:1]
newbeaHundred1.npctext = "Yay gold! Can I have some more?"
newbeaHundred1.options = []
newbeaHundred1.options << [text:"Sure.", result:2]
newbeaHundred1.options << [text:"No.", result:3]
newbeaHundred.addDialog(newbeaHundred1, Newbea)

def newbeaHundred2 = [id:2]
newbeaHundred2.playertext = "How much more gold will you give NewBea?"
newbeaHundred2.options = []
newbeaHundred2.options << [text:"1000 gold.", exec: { event -> runOnDeduct( event.actor, "g", 1000, { event.player.setGlobalFlag( "Z1_Newbea_Thousand" ); event.player.unsetGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaThousand" ); println "@@@@ ${event.player} gave Newbea 1000 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaHundred2.options << [text:"5000 gold.", exec: { event -> runOnDeduct( event.actor, "g", 5000, { event.player.setGlobalFlag( "Z1_Newbea_FiveThousand" ); event.player.unsetGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaFiveThousand" ); println "@@@@ ${event.player} gave Newbea 5000 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaHundred2.options << [text:"10000 gold.", exec: { event -> runOnDeduct( event.actor, "g", 10000, { event.player.setGlobalFlag( "Z1_Newbea_TenThousand" ); event.player.unsetGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaTenThousand" ); println "@@@@ ${event.player} gave Newbea 10000 gold @@@@" }, { event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) } ) }, result: DONE]
newbeaHundred2.options << [text:"None.", exec: { event -> event.player.setGlobalFlag( "Z1_Newbea_NoGold" ); event.player.unsetGlobalFlag( "Z1_Newbea_Hundred" ); Newbea.pushDialog( event.player, "newbeaNoGold" ) }, result: DONE]
newbeaHundred.addDialog(newbeaHundred2, Newbea)

def newbeaHundred3 = [id:3]
newbeaHundred3.npctext = "WHY WON'T YOU HELP ME?!?!"
newbeaHundred3.exec = { event -> startMoving() }
newbeaHundred3.flag = "!Z1_Newbea_Hundred"
newbeaHundred3.result = DONE
newbeaHundred.addDialog(newbeaHundred3, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 10 gold again                             
//--------------------------------------------------------------------------------
def newbeaTenTwo = Newbea.createConversation("newbeaTenTwo", true, "Z1_Newbea_TenTwo")

def newbeaTenTwo1 = [id:1]
newbeaTenTwo1.npctext = "Thanx a little."
newbeaTenTwo1.flag = "!Z1_Newbea_TenTwo"
newbeaTenTwo1.result = DONE
newbeaTenTwo.addDialog(newbeaTenTwo1, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 25 gold again                             
//--------------------------------------------------------------------------------
def newbeaTwentyFiveTwo = Newbea.createConversation("newbeaTwentyFiveTwo", true, "Z1_Newbea_TwentyFiveTwo")

def newbeaTwentyFiveTwo1 = [id:1]
newbeaTwentyFiveTwo1.npctext = "Thanx a lot."
newbeaTwentyFiveTwo1.flag = "!Z1_Newbea_TwentyFiveTwo"
newbeaTwentyFiveTwo1.result = DONE
newbeaTwentyFiveTwo.addDialog(newbeaTwentyFiveTwo1, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 100 gold again                            
//--------------------------------------------------------------------------------
def newbeaHundredTwo = Newbea.createConversation("newbeaHundredTwo", true, "Z1_Newbea_HundredTwo")

def newbeaHundredTwo1 = [id:1]
newbeaHundredTwo1.npctext = "I has gold!"
newbeaHundredTwo1.flag = "!Z1_Newbea_HundredTwo"
newbeaHundredTwo1.result = DONE
newbeaHundredTwo.addDialog(newbeaHundredTwo1, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 1000 gold again                           
//--------------------------------------------------------------------------------
def newbeaThousand = Newbea.createConversation("newbeaThousand", true, "Z1_Newbea_Thousand")

def newbeaThousand1 = [id:1]
newbeaThousand1.npctext = "Wow! Thanx sooooo much!"
newbeaThousand1.flag = "!Z1_Newbea_Thousand"
newbeaThousand1.result = DONE
newbeaThousand.addDialog(newbeaThousand1, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 5000 gold again                           
//--------------------------------------------------------------------------------
def newbeaFiveThousand = Newbea.createConversation("newbeaFiveThousand", true, "Z1_Newbea_FiveThousand")

def newbeaFiveThousand1 = [id:1]
newbeaFiveThousand1.npctext = "I'm rich! HEHEHE!"
newbeaFiveThousand1.flag = "!Z1_Newbea_FiveThousand"
newbeaFiveThousand1.result = DONE
newbeaFiveThousand.addDialog(newbeaFiveThousand1, Newbea)

//--------------------------------------------------------------------------------
//Conversation when player gives NewBea 10000 gold again                          
//--------------------------------------------------------------------------------
def newbeaTenThousand = Newbea.createConversation("newbeaTenThousand", true, "Z1_Newbea_TenThousand")

def newbeaTenThousand1 = [id:1]
newbeaTenThousand1.npctext = "OMG u r the nicest person ever! EVER!!!! XOXOXO"
newbeaTenThousand1.flag = "!Z1_Newbea_TenThousand"
newbeaTenThousand1.result = DONE
newbeaTenThousand.addDialog(newbeaTenThousand1, Newbea)

//--------------------------------------------------------------------------------
//No gold for newbea                                                              
//--------------------------------------------------------------------------------
def newbeaNoGold = Newbea.createConversation("newbeaNoGold", true, "Z1_Newbea_NoGold")

def newbeaNoGold1 = [id:1]
newbeaNoGold1.npctext = "You said you were going to help me and now you won't! THAT'S MEAN! I HATE YOU!!!!"
newbeaNoGold1.flag = "!Z1_Newbea_NoGold"
newbeaNoGold1.result = DONE
newbeaNoGold.addDialog(newbeaNoGold1, Newbea)

//-------------------------------------------------------------------------------
//NewBea's conversation when player is on Channel Nine News quest                
//-------------------------------------------------------------------------------
def NewbeaNews = Newbea.createConversation("NewbeaNews", true, "QuestStarted_77:2")

def news1 = [id:1]
news1.npctext = "HELLO GAIA!"
news1.exec = {event ->
	stopMoving()
}
news1.result = 2
NewbeaNews.addDialog(news1, Newbea)

def news2 = [id:2]
news2.playertext = "Hello, NewBea. This is %p, with Channel Nine News. I'd like to ask you a few questions."
news2.result = 3
NewbeaNews.addDialog(news2, Newbea)

def news3 = [id:3]
news3.npctext = "OK! Sounds like fun. HEHE!"
news3.result = 4
NewbeaNews.addDialog(news3, Newbea)

def news4 = [id:4]
news4.playertext = "Okay, let's begin. How long have you been in Gaia?"
news4.result = 5
NewbeaNews.addDialog(news4, Newbea)

def news5 = [id:5]
news5.npctext = "Oh! I just got here! Can I have some gold?"
news5.result = 6
NewbeaNews.addDialog(news5, Newbea)

def news6 = [id:6]
news6.playertext = "Maybe later. What has your time in Gaia been like so far? Are people nice or mean?"
news6.result = 7
NewbeaNews.addDialog(news6, Newbea)

def news7 = [id:7]
news7.npctext = "Some people have been really nice! They gave me gold! Others have been mean, and just yell at me when I ask for gold. Very mean. Don't they know I'm new and don't have any?"
news7.result = 8
NewbeaNews.addDialog(news7, Newbea)

def news8 = [id:8]
news8.playertext = "What brings you to the fountain?"
news8.result = 9
NewbeaNews.addDialog(news8, Newbea)

def news9 = [id:9]
news9.npctext = "I got lost trying to find the forums. I'm just here to chat! Hehehe!"
news9.result = 10
NewbeaNews.addDialog(news9, Newbea)

def news10 = [id:10]
news10.playertext = "Thank you for your time, NewBea. For Channel Nine News this is %p. Now back to Cindy Donovinh in the studio."
news10.result = 11
NewbeaNews.addDialog(news10, Newbea)

def news11 = [id:11]
news11.npctext = "Can I have some gold? PLEEEEEZ?!?"
news11.playertext = "All right! Enough already!"
news11.flag = ["Z1_CindyDonovinh_Newbea_Complete", "!Z1_CindyDonovinh_Newbea_Active"]
news11.quest = 77
news11.exec = { event ->
	event.player.removeMiniMapQuestActorName("NewBea")
	event.player.addMiniMapQuestActorName("Cindy-VQS")
	startMoving()
}
news11.result = DONE
NewbeaNews.addDialog(news11, Newbea)