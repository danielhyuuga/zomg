import com.gaiaonline.mmo.battle.script.*;

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("tunnelGrate", "markerOther", "BARTON_104", 1180, 200, "Waterworks Entrance")

//-------------------------------------------
//BARTON_104 Manhole (WATERWORKS ENTRANCE)   
//-------------------------------------------

onClickTunnelsEntrance = new Object()

tunnelsEntrance = makeSwitch( "Grate1", myRooms.BARTON_104, 1180, 200 )
tunnelsEntrance.unlock()
tunnelsEntrance.off()
tunnelsEntrance.setRange( 200 )
tunnelsEntrance.setMouseoverText("Waterworks Entrance")

def enterTunnels = { event ->
	synchronized( onClickTunnelsEntrance ) {
		tunnelsEntrance.off()
		if( isPlayer( event.actor ) ) {
			if( event.actor.getConLevel() < 2.1 ) {
				event.actor.setQuestFlag( GLOBAL, "Z01ComingFromBartonTown" )
				event.actor.centerPrint( "You pry up the grate and descend down into the tunnels below..." )
				event.actor.unsetQuestFlag( GLOBAL, "Z14HasLeftWaterworks" )
				if(!event.actor.isOnQuest(332) && !event.actor.isDoneQuest(332)) { event.actor.updateQuest(332, "BFG-Elizabeth") }
				if(!event.actor.isOnQuest(333) && !event.actor.isDoneQuest(333)) { event.actor.updateQuest(333, "BFG-Elizabeth") }
				event.actor.warp( "Sewers_104", 250, 360 )
			} else {
				tutorialNPC.pushDialog( event.actor, "CLWarning" )
			}
		}		
	}
}

tunnelsEntrance.whenOn( enterTunnels )

CLWarning = tutorialNPC.createConversation( "CLWarning", true )

def warn1 = [id:1]
warn1.npctext = "<zOMG dialogWidth='240'><![CDATA[<h1><b><font face='Arial' size='14'>You're Too Tough!</font></b></h1><font face='Arial' size ='12'><br>You are trying to enter the Waterworks below Barton Town.<br><br>To keep this area friendly for new players, it is limited to players at level 2.0 or less.<br><br>Your current level is higher than that, so you can't go in.<br><br>But you *can* use the CHANGE LEVEL feature (in the MENU) to temporarily lower your level and then enter the Waterworks.<br><br>]]></zOMG>"
warn1.result = DONE
CLWarning.addDialog( warn1, tutorialNPC )



