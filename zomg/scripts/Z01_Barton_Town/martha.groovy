import com.gaiaonline.mmo.battle.script.*;

def Martha = spawnNPC("BFG-Martha", myRooms.BARTON_201, 292, 726)
Martha.setRotation( 45 )
Martha.setDisplayName( "Martha" )

//-----------------------------------
//DEFAULT CONVERSATION               
//-----------------------------------
def MarthaNeutral = Martha.createConversation("MarthaNeutral", true, "!QuestStarted_34:2")

def Neutral1 = [id:1]
Neutral1.npctext = "Hello there. Did you want something?"
Neutral1.result = 2
MarthaNeutral.addDialog(Neutral1, Martha)

def Neutral2 = [id:2]
Neutral2.playertext = "Let me think..."
Neutral2.options = []
Neutral2.options << [text: "Is this the West Gate?", result: 3]
Neutral2.options << [text: "Hi, What's your name?", result: 7]
Neutral2.options << [text: "No thanks!", result: 11]
MarthaNeutral.addDialog(Neutral2, Martha)

def Neutral3 = [id:3]
Neutral3.npctext = "It sure is. The Zen Gardens are just beyond this gate, and the road beyond runs all the way to Durem...but I doubt you're headed that way today."
Neutral3.playertext = "Oh? Why is that?"
Neutral3.result = 4
MarthaNeutral.addDialog(Neutral3, Martha)

def Neutral4 = [id:4]
Neutral4.npctext = "Durem is sealed off right now. There's some really bad stuff brewing between us and Durem, around the Water Reclamation Facility. No traffic is going between Barton Town and Durem until after we get that straightened out."
Neutral4.playertext = "How long will that be?"
Neutral4.result = 5
MarthaNeutral.addDialog(Neutral4, Martha)

def Neutral5 = [id:5]
Neutral5.npctext = "Who knows? Nobody is really certain that the changes have stopped escalating yet. Things might get a lot worse before they get better and each town is meeting the challenge in its own way. Very little effort is going into connecting the towns again until we know more."
Neutral5.playertext = "I guess that makes sense. Is there anything we citizens can do to help?"
Neutral5.result = 6
MarthaNeutral.addDialog(Neutral5, Martha)

def Neutral6 = [id:6]
Neutral6.npctext = "Absolutely. Head down past the South Gate and find Commander Leon. He's leading the defense of Barton Town and the villages against the Animated. He can use all the help he can get."
Neutral6.playertext = "Thanks!"
Neutral6.options = []
Neutral6.options << [text: "I'll head down there right away!", result: DONE, flag: "Z1FindLeonQuest"]
Neutral6.options << [text: "I'm busy right now, but maybe I'll head that way later. Thanks!", result: DONE]
MarthaNeutral.addDialog(Neutral6, Martha)

def Neutral7 = [id:7]
Neutral7.npctext = "I'm Martha. I come from a long line of Guards with my mother before me being a Captain, and her father being Lieutenant Colonel!"
Neutral7.playertext = "Really? The Guards don't seem big enough for those sorts of ranks."
Neutral7.result = 8
MarthaNeutral.addDialog(Neutral7, Martha)

def Neutral8 = [id:8]
Neutral8.npctext = "Well, not anymore. But back in the day, the Council kept the Guard at a bigger size and there were more opportunities. At the size we're at now, I'll be lucky to make Lieutenant one day, much less Lieutenant COLONEL."
Neutral8.playertext = "Still, it's a family tradition, eh?"
Neutral8.result = 9
MarthaNeutral.addDialog(Neutral8, Martha)

def Neutral9 = [id:9]
Neutral9.npctext = "That's right! And besides, I could never leave Barton Town now. I'm way too attached to some of the people here."
Neutral9.playertext = "I understand. Thanks for your help!"
Neutral9.result = 10
MarthaNeutral.addDialog(Neutral9, Martha)

def Neutral10 = [id:10]
Neutral10.npctext = "Anytime!"
Neutral10.result = DONE
MarthaNeutral.addDialog(Neutral10, Martha)

def Neutral11 = [id:11]
Neutral11.npctext = "Okay then, citizen. Have a nice day!"
Neutral11.result = DONE
MarthaNeutral.addDialog(Neutral11, Martha)

//------------------------------------------
// OLIVIA'S COOKIE quest (Martha's Leg)     
//------------------------------------------
def MarthaCookies = Martha.createConversation("MarthaCookies", true, "QuestStarted_34:2", "!martha133")

def Cookies1 = [id:1]
Cookies1.playertext = "Leon's mom sends her very best! Cookies for you!"
Cookies1.result = 2
MarthaCookies.addDialog(Cookies1, Martha)

def Cookies2 = [id:2]
Cookies2.npctext = "Oh. Ummm, I've been working so hard to maintain my figure recently. I don't suppose I really should."
Cookies2.options = []
Cookies2.options << [text:"Just try one! I'd hate to tell Olivia that you didn't even try *one*.", result: 3]
Cookies2.options << [text:"Are you sure? They're still waaaaarm...", result: 7]
Cookies2.options << [text:"Wise decision! These things are heavy enough to use as projectiles!", result: 9]
MarthaCookies.addDialog(Cookies2, Martha)

def Cookies3 = [id:3]
Cookies3.npctext = "You would do that? You WOULD do that! Oh no. All right. Give me one. I'll eat it later."
Cookies3.playertext = "hehehe. Clever girl. I'd 'eat it later' too."
Cookies3.result = 4
MarthaCookies.addDialog(Cookies3, Martha)

def Cookies4 = [id:4]
Cookies4.npctext = "No really! Of course I'll eat it!"
Cookies4.playertext = "Mmmm-hmmm..."
Cookies4.result = 5
MarthaCookies.addDialog(Cookies4, Martha)

def Cookies5 = [id:5]
Cookies5.npctext = "Oh all right. You win. I'm not going to eat that thing and you know it. Olivia is such a well-meaning woman. It's a shame that she's such a terror when cooking."
Cookies5.playertext = "Well, don't worry. I'll never tell. Your secret is safe with %p."
Cookies5.result = 6
MarthaCookies.addDialog(Cookies5, Martha)

def Cookies6 = [id:6]
Cookies6.npctext = "Thanks, %p. I'd really hate to hurt her feelings."
Cookies6.playertext = "No problem. Remember to bury that cookie deeply where it won't leak out and poison anything."
Cookies6.flag = "Z1MarthaGoodbye"
Cookies6.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Martha" )
	event.player.updateQuest( 34, "BFG-Martha" ) //push completion of Martha's step of Olivia's Cookies quest) 
	Martha.pushDialog(event.player, "MarthaGoodbye")
}
MarthaCookies.addDialog(Cookies6, Martha)

def Cookies7 = [id:7]
Cookies7.npctext = "Well, okay. Maybe this time she's actually made something nice to eat. Oh...they're heavy!"
Cookies7.playertext = "Yup. And getting heavier all the time. I think the chocolate chips are black holes or something."
Cookies7.result = 8
MarthaCookies.addDialog(Cookies7, Martha)

def Cookies8 = [id:8]
Cookies8.npctext = "Perhaps, I should...reconsider."
Cookies8.playertext = "Sounds smart. Don't worry, I'll never tell."
Cookies8.result = 6
MarthaCookies.addDialog(Cookies8, Martha)

def Cookies9 = [id:9]
Cookies9.npctext = "Hey! Now *there's* an idea. Give me a dozen of those things. We'll see how the Animated like them when I chuck a few in their direction."
Cookies9.playertext = "Yeah. If you miss with your throw, the Animated might eat them instead, and then you'll still win!"
Cookies9.result = 10
MarthaCookies.addDialog(Cookies9, Martha)

def Cookies10 = [id:10]
Cookies10.npctext = "Lol. Okay, maybe a couple more then. Thanks!"
Cookies10.playertext = "Have fun!"
Cookies10.flag = "Z1MarthaGoodbye"
Cookies10.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Martha" )
	event.player.updateQuest( 34, "BFG-Martha" ) //push completion of Martha's step of Olivia's Cookies quest) 
	Martha.pushDialog(event.player, "MarthaGoodbye")
}
MarthaCookies.addDialog(Cookies10, Martha)

//--------------------------------------
//MARTHA'S GOODBYE                      
//Remind the player to go back to Olivia
//--------------------------------------
def MarthaGoodbye = Martha.createConversation("MarthaGoodbye", true, "!QuestCompleted_34", "Z1JohnGoodbye", "Z1MarthaGoodbye", "Z1WilliamGoodbye")

def Goodbye1 = [id:1]
Goodbye1.npctext = "Say, if you're all done passing out those culinary travesties, Olivia usually wants her plate back."
Goodbye1.playertext = "Okay. I'll head that way next and give it back."
Goodbye1.result = 2
MarthaGoodbye.addDialog(Goodbye1, Martha)

def Goodbye2 = [id:2]
Goodbye2.npctext = "And listen, if she tries to reward you with food, then for goodness sake, tell her you'll eat it somewhere else and get...rid...of...it!"
Goodbye2.playertext = "Gotcha."
Goodbye2.result = DONE
Goodbye2.exec = { event ->
	event.player.addMiniMapQuestActorName( "Olivia-VQS" )
}
MarthaGoodbye.addDialog(Goodbye2, Martha)