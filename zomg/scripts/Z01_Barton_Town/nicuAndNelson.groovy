import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def Nicu = spawnNPC("Nicolae-VQS", myRooms.BARTON_201, 960, 460)
Nicu.setDisplayName( "Nicolae" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Nicu.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/nicolae_strip.png")
}
randomFlag = []
randomFlag << "Z0NelsonR1"
randomFlag << "Z0NelsonR2"
randomFlag << "Z0NelsonR3"
randomFlag << "Z0NelsonR4"

playerList = []
fluffList = []

myManager.onEnter( myRooms.BARTON_201 ) { event ->
	if(isPlayer(event.actor)) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		//Nelson's Random Conversation flag
		if( isPlayer( event.actor ) ) {
			myManager.schedule(2) { event.actor.centerPrint( "You've arrived at the West Gate." ) }
			convoFlag = random( randomFlag )
			event.actor.setQuestFlag( GLOBAL, convoFlag )
			playerList << event.actor 
			event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" )
		}
	}
}

myManager.onExit( myRooms.BARTON_201 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
		playerList.remove( event.actor )
	}
}

/*def convoStub = Nicu.createConversation( "convoStub", true, "!Z01NicuChat" )

def stub1 = [id:1]
stub1.npctext = "Did you come by for a freebie, or do you want to chat?"
stub1.options = []
stub1.options << [text: "I'd like a freebie, please.", exec: { event ->
	checkDailyChance(event.player, 220, { result -> 
		if( result == false ) {
			Nicu.say( "Enjoy! Compliments of Back Alley Bargains! Tell all your friends!" )
			runDailyChance(event.player, 220)
		} else {
			event.player.centerPrint( "Nicu scowls at you as if to say 'You've had your freebie today. Don't abuse my limited generosity!'." )
			myManager.schedule(3) { event.player.centerPrint( "Which is a lot for a simple scowl to say, really." ) }
		}
	})
}, result: DONE]
stub1.options << [text: "I'd like to chat!", exec: { event ->
	event.player.setQuestFlag( GLOBAL, "Z01NicuChat" )
	if( !event.player.hasQuestFlag( GLOBAL, "Z1NicuFirstSpeech" ) ) {
		Nicu.pushDialog( event.player, "nicuDefault" )
	} else {
		Nicu.pushDialog( event.player, "queryNicu" )
	}
}, result: DONE]
convoStub.addDialog( stub1, Nicu )*/

//======================================
// NICU'S INITIAL INTRODUCTION SPEECH   
//======================================
def nicuDefault = Nicu.createConversation("nicuDefault", true, "!Z1NicuFirstSpeech" )

def Def1 = [id:1]
Def1.npctext = "Hello! Welcome to my humble shop! How may I take your money on this fine day?"
Def1.playertext = "What?!?"
Def1.result = 2
nicuDefault.addDialog( Def1, Nicu )

def Def2 = [id:2]
Def2.npctext = "I said, how may I help you on this fine day?"
Def2.playertext = "Oh. I thought you said something else. Is this a shop?"
Def2.result = 3
nicuDefault.addDialog( Def2, Nicu )

def Def3 = [id:3]
Def3.npctext = "This is *not* a 'shop'. A shop is a prosaic, stationary thing which offers the same merchandise to the same customers, day after day."
Def3.result = 4
nicuDefault.addDialog( Def3, Nicu )

def Def4 = [id:4]
Def4.npctext = "THIS...this is my vardo, my home that travels with me. A cornucopia of curiosities from all over this wild, wonderful world, brought to this location for the convenience of...you."
Def4.playertext = "So, it's a shop?"
Def4.result = 5
nicuDefault.addDialog( Def4, Nicu)

def Def5 = [id:5]
Def5.npctext = "Soulless philistine. Yes, it's a shop. What can I do for you?"
Def5.playertext = "What do you sell?"
Def5.result = 6
nicuDefault.addDialog( Def5, Nicu )

def Def6 = [id:6]
Def6.npctext = "Hmmm, the answer to that is usually 'What would you like to buy?', followed quickly by an inquiry into the flexibility of your scruples. However, I can see that you're a discerning customer looking for quality products only. So allow me to answer succinctly."
Def6.result = 7
nicuDefault.addDialog( Def6, Nicu )

def Def7 = [id:7]
Def7.npctext = "Since the rise of the Animated, folks aren't sleeping much, trying to find ways to defend against them. Even though nothing better than the rings has been found so far, there have been a number of other useful items created anyway. I have access to many of those new trinkets, some of which you may be surprised to desire."
Def7.result = 8
nicuDefault.addDialog( Def7, Nicu )

def Def8 = [id:8]
Def8.npctext = "I also have something of a knack for finding new uses for useless things you find, combining them together and turning them into useful items. For a small fee, of course."
Def8.playertext = "Wow. That sounds great. What can you show me?"
Def8.result = 9
nicuDefault.addDialog( Def8, Nicu )

def Def9 = [id:9]
Def9.npctext = "Lots of wonderful things. Here, let me show you."
Def9.playertext = "Thanks, Nicolae!"
Def9.result = 10
nicuDefault.addDialog( Def9, Nicu )

def Def10 = [id:10]
Def10.npctext = "Call me Nicu. All of my best customers do."
Def10.result = DONE
Def10.flag = ["Z1NicuFirstSpeech"]
nicuDefault.addDialog( Def10, Nicu )

//======================================
// CONTINUING NICU'S DISCUSSION         
//======================================

def queryNicu = Nicu.createConversation("queryNicu", true, "Z1NicuFirstSpeech" )

def query1 = [id:1]
query1.npctext = "You look inquisitive. Is there anything else I can help you with?"
query1.result = 2
queryNicu.addDialog( query1, Nicu )

def query2 = [id:2]
query2.npctext = "Well...?"
query2.options = []
query2.options << [text: "What's the world like outside of town?", result: 3]
query2.options << [text: "What can you tell me about the rings that work against the Animated?", result: 5]
query2.options << [text: "Is there anything you need done?", result: 6]
query2.options << [text: "Uh, nevermind. Talk to you later.", result: 7]
queryNicu.addDialog( query2, Nicu )

def query3 = [id:3]
query3.npctext = "Turbulent. Nasty. Dangerous. And most of all, non-profitable. People are never at their purchasing best when they fear for their lives. It's annoying!"
query3.playertext = "I can see how that might annoy you. Of course, those people *are* fearing for their lives..."
query3.result = 4
queryNicu.addDialog( query3, Nicu )

def query4 = [id:4]
query4.npctext = "Exactly! That's what I'm saying! Do you have any idea how difficult it is to convince someone they need a beautiful, only slight-used necklace when they're running from some lawn mower come to life?"
query4.playertext = "How...inconvenient for you. Do you have time for another question?"
query4.result = 2
queryNicu.addDialog( query4, Nicu )

def query5 = [id:5]
query5.npctext = "Not much! I thought about selling them, but since they don't seem to come off peoples' fingers very often, and thus, there are supply issues with that business idea. I've settled for some of the harder-to-find stuff that's been appearing here and there. Take a look in the shop, if you're curious!"
query5.playertext = "Thanks, I will. I have another question, if you've got a moment still."
query5.result = 2
queryNicu.addDialog( query5, Nicu )

def query6 = [id:6]
query6.npctext = "Oh yes! Absolutely. But no, not by you. What I have in mind requires a nice candle lit dinner and a pretty young thing that doesn't mind the smell of horse liniment."
query6.playertext = "Yikes! I'm going to forget I asked. Thanks anyway. But, ummm..."
query6.result = 2
queryNicu.addDialog( query6, Nicu )

def query7 = [id:7]
query7.npctext = "Okay then. Come on by anytime when you feel the need to have lighter pockets and a freer attitude toward life."
query7.playertext = "Freer? Are you offering a discount then?"
query7.result = 8
queryNicu.addDialog( query7, Nicu )

def query8 = [id:8]
query8.npctext = "...go away kid, ya bother me."
query8.flag = "!Z01NicuChat"
query8.result = DONE
queryNicu.addDialog( query8, Nicu )

if (eventStart != 'HALLOWEEN') return

//======================================
// HALLOWEEN TRICK OR TREATING SCRIPT   
//======================================

//Pumpkin Fluff Spawner
pumpFluff201 = myRooms.BARTON_201.spawnSpawner( "pumpFluff201", "pumpkin_fluff", 6 )
pumpFluff201.setPos( 745, 455 )
pumpFluff201.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
pumpFluff201.setMonsterLevelForChildren( 1.0 )

pumpFluff201.stopSpawning()

//=========================================
// JACK-O-LANTERN SWITCH                   
//=========================================

//TODO : Set things up so that trick or treating only occurs at night (when the pumpkins light up)
// GST test : Turns ON pumpkins at night, turns them OFF during day.
// Pumpkins are locked during the day.
// Pumpkins turn off when activated at night, but immediately turn back ON for the next player. (Light flickers when used.)

jack201 = makeSwitch( "jack201", myRooms.BARTON_201, 750, 380 )
jack201.setRange( 200 )

def startTrickOrTreating = { event ->
	jack201.on()
	if( !event.actor.hasQuestFlag( GLOBAL, "TrickOrTreatActive" ) ) {
		//set a preventative flag that expires in 5 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "TrickOrTreatActive" ) 
		myManager.schedule(5) { event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" ) }
		//now push the trick or treat dialog
		event.actor.setQuestFlag( GLOBAL, "Z01TrickOrTreat201A" )
		Nicu.pushDialog( event.actor, "trickOrTreat" )
	}
}
	
jack201.whenOff( startTrickOrTreating )

//Variable Initialization & Master Timer
fluffsAlreadySpawned = false
hourCounter = System.currentTimeMillis()
trickTreatTimer()
jackOLanternTimer()

//Every hour, update the hourCounter variable
def trickTreatTimer() {
	myManager.schedule(4200){ hourCounter = System.currentTimeMillis(); trickTreatTimer() }
	
}

//Turn on and unlock Jack O'Lanterns at night. Turn them off during day and lock them.
def jackOLanternTimer() {
//	println "**** GST = ${gst()} ****"
	if( ( gst() > 1800 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 600 ) ) { //trick or treating allowed between 6pm and 6am.
		jack201.unlock()
		jack201.on()
	} else {
		jack201.lock()
		jack201.off()
	}
	myManager.schedule(120) { jackOLanternTimer() } //check time of day again in five minutes
}
	
//=========================================
// DIALOG AND SCRIPT LOGIC                 
//=========================================

//determine how to respond to the player
def trickOrTreat = Nicu.createConversation( "trickOrTreat", true, "Z01TrickOrTreat201A" )

TT1 = [id:1]
TT1.npctext = "Happy Halloween!"
TT1.flag = "!Z01TrickOrTreat201A"
TT1.exec = { event ->
	if( event.player.getPlayerVar( "Z01VisitedNicu" ).longValue() == null || event.player.getPlayerVar( "Z01VisitedNicu" ).longValue() < hourCounter ) {
		def awardList = playerList.clone().intersect( event.player.getCrew() )
		awardList.each{
			//don't allow crewmembers that already got treats from this pumpkin to get them again just because they're in a different crew.
			if( it.getPlayerVar( "Z01VisitedNicu" ).longValue() >= hourCounter ) {
				it.setQuestFlag( GLOBAL, "Z01NotYet" )
				Nicu.pushDialog( it, "notYet" )
			}
		} 
		event.player.setQuestFlag( GLOBAL, "Z01TrickOrTreat201B" )
		Nicu.pushDialog( event.player, "trickOrTreat2" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z01NotYet" )
		Nicu.pushDialog( event.player, "notYet" )
	}
}
trickOrTreat.addDialog( TT1, Nicu )

//If the player has received a treat too recently then don't give him a treat
def notYet = Nicu.createConversation( "notYet", true, "Z01NotYet" )

nYet1 = [id:1]
nYet1.npctext = "Wait a sec! I talked to you already tonight! Try again tomorrow night."
nYet1.flag = ["!Z01NotYet", "Z01HalloweenBridgeNicu"]
nYet1.exec = { event ->
	Nicu.pushDialog( event.player, "halloweenNormalNicu" )
}
nYet1.result = DONE
notYet.addDialog( nYet1, Nicu )

//if the player qualifies for a treat or trick, then get this conversation instead
def trickOrTreat2 = Nicu.createConversation( "trickOrTreat2", true, "Z01TrickOrTreat201B" )

TT2 = [id:1]
TT2.playertext = "Trick or Treat?"
TT2.flag = "!Z01TrickOrTreat201B"
TT2.result = DONE
TT2.exec = { event ->
	def awardList = playerList.clone().intersect( event.player.getCrew() )
	awardList.clone().each{ if( it.getPlayerVar( "Z01VisitedNicu" ).longValue() >= hourCounter ) { awardList.remove( it ) } }
	awardList.each{
		//reset the playerVar hourCounter in case people try to exploit by joining the Crew after the first conversation dialog.
		it.setPlayerVar( "Z01VisitedNicu", hourCounter )

		//increment the number of pumpkins clicked counter
		it.addToPlayerVar( "Z01JackOLanternsClicked", 1 )

		//check for Badge Updates
		if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 50 && it.getPlayerVar( "Z01JackOLanternsClicked" ) < 200 && !it.isDoneQuest(276) ) {
			it.updateQuest( 276, "Story-VQS" ) //Update the "Jack O'Smasher" badge
		} else if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 200 && !it.isDoneQuest(277) ) {
			it.updateQuest( 277, "Story-VQS" ) //Update the "Pumpkin King" badge
		}

		//determine if it's a trick or treat.
		roll = random( 100 )
		def player = it
		if( roll <= 40 ) { //This value is the chance of a trick. Remainder is the treat percentage.
			player.centerPrint( "You get TRICKED!" )
			trick( player, awardList )
		} else {
			player.centerPrint( "You get a TREAT!" )
			treat( player, awardList )
		}
	}
}
trickOrTreat2.addDialog( TT2, Nicu )

//=========================================
// TRICKS!!!                               
//=========================================
def synchronized trick( player, awardList ) {
	roll =  random( 100 )
	if( fluffsAlreadySpawned == true && roll <= 45 ) {
		roll = random( 46, 100 )
	}
	if( roll <= 45 ) {
		//pumpkin fluff spawn
		player.centerPrint( "From out of the pumpkin come pumpkin-costumed fluffs to attack you!" )
		fluffsAlreadySpawned = true
		fluffSpawnNum = awardList.size()
		spawnPumpkinFluffs()
	} else if( roll > 45 && roll <= 90 ) {
		//give the player a dentist gift
		dentistGift = random( 100 )
		if( dentistGift <= 50 ) {
			dentistGift = "100479"
			player.centerPrint( "Oh man, are you kidding? Dental Floss?!? Who gives dental floss instead of candy?" )
		} else { 
			dentistGift = "100481"
			player.centerPrint( "No way! A toothbrush? WTF?!? It's Halloween, people!" )
		}
		player.grantItem( dentistGift )
	} else if( roll > 90 ) {
		//warp the player
		player.centerPrint( "You feel a strange energy pulling you away to...somewhere else!" )
		roll = random( 100 )
		if( roll <= 50 ) {
			//somewhere in Barton (one of three places)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "BARTON_301", 580, 200 ) //behind statue
			} else if( roll == 2 ) {
				player.warp( "BARTON_4", 1400, 700 ) //near Dusty in corner
			} else if( roll == 3 ) {
				player.warp( "BARTON_1", 1360, 290 ) //in trees above Maestro
			} 
		} else if( roll > 50 && roll <= 80 ) {
			//somewhere in Village Greens (three places, one is nasty)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "VILLAGE_3", 310, 380 ) //in quiet corner at top of map
			} else if( roll == 2 ) {
				player.warp( "VILLAGE_201", 155, 375 ) //Way over by the goof marker at top of west side of map
			} else if( roll == 3 ) {
				player.warp( "VILLAGE_1004", 900, 590 ) //Down by the flamingo spiral at the bottom, right of the map. DANGER!!!
			} 
		} else if( roll > 80 && roll <= 95 ) {
			//danger spots in other zones near Crystals (pull a random one from a map with coords for warping)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "ZENGARDEN_502", 500, 625 ) //in Katsumi's shrine
			} else if( roll == 2 ) {
				player.warp( "BASSKEN_403", 350, 330 ) //By Logan
			} else if( roll == 3 ) {
				player.warp( "Aqueduct_602", 1330, 530 ) //Up by the True Believers
			} 
		} else if( roll > 95 ) {
			player.warp( "Hive_1", 735, 530 ) //YIKES! THE HIVE!!
		}
	}
}

def spawnPumpkinFluffs() {
	if( fluffSpawnNum > 0 ) {
		fluffList << pumpFluff201.forceSpawnNow() //This list starts at position 0, so subtract one from the list size to get the correct list position
		fluffList.get( fluffList.size() - 1 ).addHate( random ( playerList ), 1 ) 
		runOnDeath( fluffList.get( fluffList.size() - 1 ) ) { event -> fluffList.remove( fluffList.get( fluffList.size() - 1 ) ); checkForLoot( event ) }
		fluffSpawnNum -- 
		myManager.schedule(1) { spawnPumpkinFluffs() } 
	} else {
		myManager.schedule(30) { disposalTimer() }
	}
}

def synchronized checkForLoot( event ) {
	def awardList = playerList.clone().intersect( event.killer.getCrew() )
	awardList.each {
		roll = random( 100 )
		if( !it.hasQuestFlag( GLOBAL, "Z01GotOhMyGumball10" ) ) {
			if( roll <= 25 ) { //trick is 50% and pumpkin fluffs are 50% of that and this is 20% of that, so 2.5% chance of getting the item for each fluff killed.
				it.centerPrint( "Happy Halloween! You found the 'Oh My Gumball'!" )
				it.grantItem( "28768" ) //TODO: Fill in with the correct item ID
				it.setQuestFlag( GLOBAL, "Z01GotOhMyGumball10" )
			}
		} else { //if player has already received the 'Oh My Gumball' item
			if( roll <= 25 ) { //only tell them about the 'Oh My Gumball' if they would have normally received it
				it.centerPrint( "You've already found the 'Oh My Gumball', so all you find this time is pumpkin guts." )
			}
		}			
	}
}

def disposalTimer() {
	if( !fluffList.isEmpty() ) {
		fluffList.each{ it.dispose() }
	}
	fluffsAlreadySpawned = false
}

//=========================================
// TREATS!!!                               
//=========================================

commonList = [ 100272, 100289, 100385, 100297, 100289, 100397, 100291, 100262, 100263, 100298, 100388, 100278, 100275, 100283, 100281, 100367, 100411, 100394, 100284, 100405, 100267, 100265, 100285, 100398, 100397, 100376, 100296 ]

uncommonList = [ 100280, 100279, 100270, 100380, 100279, 100384, 100378, 100261, 100271, 100276, 100258, 100268, 100381, 100382, 100282, 100383, 100390, 100299, 100286, 100369, 100400, 100387 ]

recipeList = [ "17766", "17764", "17772", "17758", "17756", "17861", "17857", "17755", "17848", "17849", "17852", "17851", "17850", "17753", "17833", "17831", "17754", "17836", "17835", "17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779" ]

ringList = [ "17716", "17722", "17723", "17737", "17738", "17741", "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "20253" ]

def synchronized treat( player, awardList ) {
	//scale the returned results by relative con levels for each Crew member
	roll = random( 100 )

	lootMultiplier = player.getConLevel() / 10 //the lower the overall CL of a player in relation to player max CL (currently 10), the lower the reward so the trick or treating can be meaningful to all CLs
	//grant gold
	if( roll <= 50 ) {
		goldGrant = ( random( 100000, 300000 ) * lootMultiplier ).intValue()
		player.grantCoins( goldGrant ) 
		player.centerPrint( "It's raining gold!" )
	//grant common item
	} else if( roll > 50 && roll <= 65 ) {
		player.grantQuantityItem( random( commonList ), random(2,5) )
		player.centerPrint( "All right! A loot item!" )
	//grant uncommon item
	} else if( roll > 65 && roll <= 75 ) {
		player.grantQuantityItem( random( uncommonList ), random(1,3) )
		player.centerPrint( "An uncommon loot item!" )
	//grant recipes
	} else if( roll > 75 && roll <= 85 ) {
		player.grantItem( random( recipeList ) )
		player.centerPrint( "Awesome! A recipe!" )
	//grant orbs
	} else if( roll > 85 && roll <= 95 ) {
		orbGrant = ( 10 * lootMultiplier ).intValue()
		player.grantQuantityItem( 100257, orbGrant )
		player.centerPrint( "Look! Charge Orbs!!" )
	//rings
	} else if( roll > 95 ) {
		player.grantRing( random( ringList ), true )
		player.centerPrint( "Woohoo! A Ring!" )
	}
}


//===========================================================
// NELSON'S RANDOM DIALOG CONVO                              
//===========================================================

def NelsonVQS = spawnNPC("Nelson-VQS", myRooms.BARTON_201, 580, 240)
NelsonVQS.setRotation( 45 )
NelsonVQS.setDisplayName( "Nelson" )

//======================================
// RANDOM CONVERSATIONS                 
//======================================

def Random1 = NelsonVQS.createConversation("Random1", true, "Z0NelsonR1", "!Z01ElizabethSendsToQuizQuest" )

def rand1 = [id:1]
rand1.npctext = "Everyone is on their own now. Aekea, Durem, Gambino...all cut off from each other right now. We have to make due with our local guards for protection...for now."
rand1.flag = "!Z0NelsonR1"
rand1.exec = { event ->
	selectRandomFlag( event )
}
rand1.result = DONE
Random1.addDialog(rand1, NelsonVQS)
//--------------------
def Random2 = NelsonVQS.createConversation("Random2", true, "Z0NelsonR2", "!Z01ElizabethSendsToQuizQuest" )

def rand2 = [id:1]
rand2.npctext = "How? How do clothing and machine items suddenly come alive? How does that happen?!?"
rand2.flag = "!Z0NelsonR2"
rand2.exec = { event ->
	selectRandomFlag( event )
}
rand2.result = DONE
Random2.addDialog(rand2, NelsonVQS)
//--------------------
def Random3 = NelsonVQS.createConversation("Random3", true, "Z0NelsonR3", "!Z01ElizabethSendsToQuizQuest" )

def rand3 = [id:1]
rand3.npctext = "I've heard about the Rings that Leon and his Guards are using. We're all going to need those before things get much crazier."
rand3.flag = "!Z0NelsonR3"
rand3.exec = { event ->
	selectRandomFlag( event )
}
rand3.result = DONE
Random3.addDialog(rand3, NelsonVQS)
//--------------------
def Random4 = NelsonVQS.createConversation("Random4", true, "Z0NelsonR4", "!Z01ElizabethSendsToQuizQuest" )

def rand4 = [id:1]
rand4.npctext = "Are you new to Gaia? In that case, head down south to the Village Greens and look for Leon! He's coordinating the defense of the town and if you want to help out, he's the Man."
rand4.flag = "!Z0NelsonR4"
rand4.exec = { event ->
	selectRandomFlag( event )
}
rand4.result = DONE
Random4.addDialog(rand4, NelsonVQS)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================


def selectRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( randomFlag )
	if( lastFlag == convoFlag ) {
		selectRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}

