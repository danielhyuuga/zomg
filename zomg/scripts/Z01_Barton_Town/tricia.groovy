import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def Tricia = spawnNPC("Tricia-VQS", myRooms.BARTON_302, 1040, 540)
Tricia.setRotation( 135 )
Tricia.setDisplayName( "Tricia" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Tricia.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/tricia_strip.png")
}

//======================================
// RANDOM CONVERSATIONS                 
//======================================

def Random1 = Tricia.createConversation("Random1", true, "Z0TriciaR1" )

def rand1 = [id:1]
rand1.npctext = "Go away! I'm hiding here!"
rand1.flag = "!Z0TriciaR1"
rand1.exec = { event ->
	selectRandomFlag( event )
}
rand1.result = DONE
Random1.addDialog(rand1, Tricia)
//--------------------
def Random2 = Tricia.createConversation("Random2", true, "Z0TriciaR2" )

def rand2 = [id:1]
rand2.npctext = "Shoo! Seriously! If no one knows I'm here, then I can't be attacked!"
rand2.flag = "!Z0TriciaR2"
rand2.exec = { event ->
	selectRandomFlag( event )
}
rand2.result = DONE
Random2.addDialog(rand2, Tricia)
//--------------------
def Random3 = Tricia.createConversation("Random3", true, "Z0TriciaR3" )

def rand3 = [id:1]
rand3.npctext = "*sob* What will happen to the world now? We're cut off from everyone else!"
rand3.flag = "!Z0TriciaR3"
rand3.exec = { event ->
	selectRandomFlag( event )
}
rand3.result = DONE
Random3.addDialog(rand3, Tricia)
//--------------------
def Random4 = Tricia.createConversation("Random4", true, "Z0TriciaR4" )

def rand4 = [id:1]
rand4.npctext = "What is to become of us all?"
rand4.flag = "!Z0TriciaR4"
rand4.exec = { event ->
	selectRandomFlag( event )
}
rand4.result = DONE
Random4.addDialog(rand4, Tricia)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

myManager.onEnter( myRooms.BARTON_302 ) { event ->
	if( isPlayer( event.actor) ) {
		convoFlag = random( randomFlag )
		event.actor.setQuestFlag( GLOBAL, convoFlag )
	}
}

myManager.onExit( myRooms.BARTON_302 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
	}
}

randomFlag = []
randomFlag << "Z0TriciaR1"
randomFlag << "Z0TriciaR2"
randomFlag << "Z0TriciaR3"
randomFlag << "Z0TriciaR4"


def selectRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( randomFlag )
	if( lastFlag == convoFlag ) {
		selectRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}
