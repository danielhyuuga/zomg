import com.gaiaonline.mmo.battle.script.*;

// timer start - lanzer
def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def FrancisR = spawnNPC("Francis R", myRooms.BARTON_202, 1175, 480)
FrancisR.setRotation( 135 )
FrancisR.setDisplayName( "Francis" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	FrancisR.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/francis_strip.png")
}

def padma = spawnNPC("Burgess H", myRooms.BARTON_202, 526, 250)
padma.setRotation( 45 )
padma.setDisplayName( "Padma" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	padma.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/padma_strip.png")
}


//======================================
// LOGIC FLOW FOR FRANCIS RANDOM LIST   
//======================================

francisFlags = []
francisFlags << "Z0FrancisR1"
francisFlags << "Z0FrancisR2"
francisFlags << "Z0FrancisR3"
francisFlags << "Z0FrancisR4"


def selectRandomFrancisFlag( event ) {
	lastFrancisFlag = francisConvoFlag
	francisConvoFlag = random( francisFlags )
	if( lastFrancisFlag == francisConvoFlag ) {
		selectRandomFrancisFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, francisConvoFlag )
}

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

padmaFlags = []
padmaFlags << "Z0BurgessR1"
padmaFlags << "Z0BurgessR2"
padmaFlags << "Z0BurgessR3"
padmaFlags << "Z0BurgessR4"


def selectRandomPadmaFlag( event ) {
	lastPadmaFlag = padmaConvoFlag
	padmaConvoFlag = random( padmaFlags )
	if( lastPadmaFlag == padmaConvoFlag ) {
		selectRandomPadmaFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, padmaConvoFlag )
}
	
//=======================================
//BARTON_202 onEnter / onExit LOGIC      
//=======================================
playerList = []
fluffList = []

myManager.onEnter( myRooms.BARTON_202 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList << event.actor 
		event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" )
		francisConvoFlag = random( francisFlags )
		event.actor.setQuestFlag( GLOBAL, francisConvoFlag )
		padmaConvoFlag = random( padmaFlags )
		event.actor.setQuestFlag( GLOBAL, padmaConvoFlag )
	}
}

myManager.onExit( myRooms.BARTON_202 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList.remove( event.actor )
		event.actor.unsetQuestFlag( GLOBAL, francisConvoFlag )
		event.actor.unsetQuestFlag( GLOBAL, padmaConvoFlag )
	}
}

//======================================
// RANDOM FRANCIS CONVERSATIONS         
//======================================

def Random1 = FrancisR.createConversation("Random1", true, "Z0FrancisR1" )

def rand1 = [id:1]
rand1.npctext = "I know a little bit about everything! Don't you wish you were me?"
rand1.flag = "!Z0FrancisR1"
rand1.exec = { event ->
	selectRandomFrancisFlag( event )
}
rand1.result = DONE
Random1.addDialog(rand1, FrancisR)
//--------------------
def Random2 = FrancisR.createConversation("Random2", true, "Z0FrancisR2" )

def rand2 = [id:1]
rand2.npctext = "Be sure to check out the Art Arena, if you do art, or even just like to look at it. People from all over the world are contributing stuff all the time!"
rand2.flag = "!Z0FrancisR2"
rand2.exec = { event ->
	selectRandomFrancisFlag( event )
}
rand2.result = DONE
Random2.addDialog(rand2, FrancisR)
//--------------------
def Random3 = FrancisR.createConversation("Random3", true, "Z0FrancisR3" )

def rand3 = [id:1]
rand3.npctext = "Don't have any friends? Go make some! The forums are to the west of the town's fountain!"
rand3.flag = "!Z0FrancisR3"
rand3.exec = { event ->
	selectRandomFrancisFlag( event )
}
rand3.result = DONE
Random3.addDialog(rand3, FrancisR)
//--------------------
def Random4 = FrancisR.createConversation("Random4", true, "Z0FrancisR4" )

def rand4 = [id:1]
rand4.npctext = "Check out Channel 9 sometime! It's the building with the helicopter on the roof. Cindy Donovinh is the person to talk to if you want to hear about current events!"
rand4.flag = "!Z0FrancisR4"
rand4.exec = { event ->
	selectRandomFrancisFlag( event )
}
rand4.result = DONE
Random4.addDialog(rand4, FrancisR)

//======================================
// RANDOM PADMA CONVERSATIONS           
//======================================

def padmaRandom1 = padma.createConversation("padmaRandom1", true, "Z0BurgessR1" )
Random1.setUrgent( false )

def prand1 = [id:1]
prand1.npctext = "I'll bet Gambino is behind all this craziness somehow. I just know it!"
prand1.flag = "!Z0BurgessR1"
prand1.exec = { event ->
	selectRandomPadmaFlag( event )
}
prand1.result = DONE
padmaRandom1.addDialog(prand1, padma)

//--------------------
def padmaRandom2 = padma.createConversation("padmaRandom2", true, "Z0BurgessR2" )
Random2.setUrgent( false )

def prand2 = [id:1]
prand2.npctext = "If you're going to leave town, you might think about finding some friends and travelling in a group. It's safer!"
prand2.flag = "!Z0BurgessR2"
prand2.exec = { event ->
	selectRandomPadmaFlag( event )
}
prand2.result = DONE
padmaRandom2.addDialog(prand2, padma)

//--------------------
def padmaRandom3 = padma.createConversation("padmaRandom3", true, "Z0BurgessR3" )
Random3.setUrgent( false )

def prand3 = [id:1]
prand3.npctext = "If you find musical notes, take them to the Maestro! He knows what to do with them."
prand3.flag = "!Z0BurgessR3"
prand3.exec = { event ->
	selectRandomPadmaFlag( event )
}
prand3.result = DONE
padmaRandom3.addDialog(prand3, padma)

//--------------------
def padmaRandom4 = padma.createConversation("padmaRandom4", true, "Z0BurgessR4" )
Random4.setUrgent( false )

def prand4 = [id:1]
prand4.npctext = "Hi! Do you have a big group of friends? Have you made a Clan with them yet? If not, head down to the SW corner of the town and register your Clan at the Clan House there!"
prand4.flag = "!Z0BurgessR4"
prand4.exec = { event ->
	selectRandomPadmaFlag( event )
}
prand4.result = DONE
padmaRandom4.addDialog(prand4, padma)

//--------------------

if (eventStart != 'HALLOWEEN') return

//======================================
// HALLOWEEN TRICK OR TREATING SCRIPT   
//======================================

//Pumpkin Fluff Spawner
pumpFluff202 = myRooms.BARTON_202.spawnSpawner( "pumpFluff202", "pumpkin_fluff", 6 )
pumpFluff202.setPos( 1150, 600 )
pumpFluff202.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
pumpFluff202.setMonsterLevelForChildren( 1.0 )

pumpFluff202.stopSpawning()

//=========================================
// JACK-O-LANTERN SWITCH                   
//=========================================

//TODO : Set things up so that trick or treating only occurs at night (when the pumpkins light up)
// GST test : Turns ON pumpkins at night, turns them OFF during day.
// Pumpkins are locked during the day.
// Pumpkins turn off when activated at night, but immediately turn back ON for the next player. (Light flickers when used.)

jack202 = makeSwitch( "jack202", myRooms.BARTON_202, 1190, 485 )
jack202.setRange( 200 )

def startTrickOrTreating = { event ->
	jack202.on()
	if( !event.actor.hasQuestFlag( GLOBAL, "TrickOrTreatActive" ) ) {
		//set a preventative flag that expires in 5 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "TrickOrTreatActive" ) 
		myManager.schedule(5) { event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" ) }
		//now push the trick or treat dialog
		event.actor.setQuestFlag( GLOBAL, "Z01TrickOrTreat202A" )
		FrancisR.pushDialog( event.actor, "trickOrTreat" )
	}
}
	
jack202.whenOff( startTrickOrTreating )

//Variable Initialization & Master Timer
fluffsAlreadySpawned = false
hourCounter = System.currentTimeMillis()
trickTreatTimer()
jackOLanternTimer()

//Every hour, update the hourCounter variable
def trickTreatTimer() {
	myManager.schedule(4200){ hourCounter = System.currentTimeMillis(); trickTreatTimer() }
	
}

//Turn on and unlock Jack O'Lanterns at night. Turn them off during day and lock them.
def jackOLanternTimer() {
//	println "**** GST = ${gst()} ****"
	if( ( gst() > 1800 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 600 ) ) { //trick or treating allowed between 6pm and 6am.
		jack202.unlock()
		jack202.on()
	} else {
		jack202.lock()
		jack202.off()
	}
	myManager.schedule(120) { jackOLanternTimer() } //check time of day again in five minutes
}
	
//=========================================
// DIALOG AND SCRIPT LOGIC                 
//=========================================

//determine how to respond to the player
def trickOrTreat = FrancisR.createConversation( "trickOrTreat", true, "Z01TrickOrTreat202A" )

TT1 = [id:1]
TT1.npctext = "Happy Halloween!"
TT1.flag = "!Z01TrickOrTreat202A"
TT1.exec = { event ->
	if( event.player.getPlayerVar( "Z01VisitedFrancisR" ).longValue() == null || event.player.getPlayerVar( "Z01VisitedFrancisR" ).longValue() < hourCounter ) {
		def awardList = playerList.clone().intersect( event.player.getCrew() )
		awardList.each{
			//don't allow crewmembers that already got treats from this pumpkin to get them again just because they're in a different crew.
			if( it.getPlayerVar( "Z01VisitedFrancisR" ).longValue() >= hourCounter ) {
				it.setQuestFlag( GLOBAL, "Z01NotYet" )
				FrancisR.pushDialog( it, "notYet" )
			}
		} 
		event.player.setQuestFlag( GLOBAL, "Z01TrickOrTreat202B" )
		FrancisR.pushDialog( event.player, "trickOrTreat2" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z01NotYet" )
		FrancisR.pushDialog( event.player, "notYet" )
	}
}
trickOrTreat.addDialog( TT1, FrancisR )

//If the player has received a treat too recently then don't give him a treat
def notYet = FrancisR.createConversation( "notYet", true, "Z01NotYet" )

nYet1 = [id:1]
nYet1.npctext = "Wait a sec! I talked to you already tonight! Try again tomorrow night."
nYet1.flag = "!Z01NotYet"
nYet1.result = DONE
notYet.addDialog( nYet1, FrancisR )

//if the player qualifies for a treat or trick, then get this conversation instead
def trickOrTreat2 = FrancisR.createConversation( "trickOrTreat2", true, "Z01TrickOrTreat202B" )

TT2 = [id:1]
TT2.playertext = "Trick or Treat?"
TT2.flag = "!Z01TrickOrTreat202B"
TT2.result = DONE
TT2.exec = { event ->
	def awardList = playerList.clone().intersect( event.player.getCrew() )
	awardList.clone().each{ if( it.getPlayerVar( "Z01VisitedFrancisR" ).longValue() >= hourCounter ) { awardList.remove( it ) } }
	awardList.each{
		//reset the playerVar hourCounter in case people try to exploit by joining the Crew after the first conversation dialog.
		it.setPlayerVar( "Z01VisitedFrancisR", hourCounter )

		//increment the number of pumpkins clicked counter
		it.addToPlayerVar( "Z01JackOLanternsClicked", 1 )

		//check for Badge Updates
		if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 50 && it.getPlayerVar( "Z01JackOLanternsClicked" ) < 200 && !it.isDoneQuest(276) ) {
			it.updateQuest( 276, "Story-VQS" ) //Update the "Jack O'Smasher" badge
		} else if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 200 && !it.isDoneQuest(277) ) {
			it.updateQuest( 277, "Story-VQS" ) //Update the "Pumpkin King" badge
		}

		//determine if it's a trick or treat.
		roll = random( 100 )
		def player = it
		if( roll <= 40 ) { //This value is the chance of a trick. Remainder is the treat percentage.
			player.centerPrint( "You get TRICKED!" )
			trick( player, awardList )
		} else {
			player.centerPrint( "You get a TREAT!" )
			treat( player, awardList )
		}
	}
}
trickOrTreat2.addDialog( TT2, FrancisR )

//=========================================
// TRICKS!!!                               
//=========================================
def synchronized trick( player, awardList ) {
	roll =  random( 100 )
	if( fluffsAlreadySpawned == true && roll <= 45 ) {
		roll = random( 46, 100 )
	}
	if( roll <= 45 ) {
		//pumpkin fluff spawn
		player.centerPrint( "From out of the pumpkin come pumpkin-costumed fluffs to attack you!" )
		fluffsAlreadySpawned = true
		fluffSpawnNum = awardList.size()
		spawnPumpkinFluffs()
	} else if( roll > 45 && roll <= 90 ) {
		//give the player a dentist gift
		dentistGift = random( 100 )
		if( dentistGift <= 50 ) {
			dentistGift = "100479"
			player.centerPrint( "Oh man, are you kidding? Dental Floss?!? Who gives dental floss instead of candy?" )
		} else { 
			dentistGift = "100481"
			player.centerPrint( "No way! A toothbrush? WTF?!? It's Halloween, people!" )
		}
		player.grantItem( dentistGift )
	} else if( roll > 90 ) {
		//warp the player
		player.centerPrint( "You feel a strange energy pulling you away to...somewhere else!" )
		roll = random( 100 )
		if( roll <= 50 ) {
			//somewhere in Barton (one of three places)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "BARTON_301", 580, 200 ) //behind statue
			} else if( roll == 2 ) {
				player.warp( "BARTON_4", 1400, 700 ) //near Dusty in corner
			} else if( roll == 3 ) {
				player.warp( "BARTON_1", 1360, 290 ) //in trees above Maestro
			} 
		} else if( roll > 50 && roll <= 80 ) {
			//somewhere in Village Greens (three places, one is nasty)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "VILLAGE_3", 310, 380 ) //in quiet corner at top of map
			} else if( roll == 2 ) {
				player.warp( "VILLAGE_201", 155, 375 ) //Way over by the goof marker at top of west side of map
			} else if( roll == 3 ) {
				player.warp( "VILLAGE_1004", 900, 590 ) //Down by the flamingo spiral at the bottom, right of the map. DANGER!!!
			} 
		} else if( roll > 80 && roll <= 95 ) {
			//danger spots in other zones near Crystals (pull a random one from a map with coords for warping)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "ZENGARDEN_502", 500, 625 ) //in Katsumi's shrine
			} else if( roll == 2 ) {
				player.warp( "BASSKEN_403", 350, 330 ) //By Logan
			} else if( roll == 3 ) {
				player.warp( "Aqueduct_602", 1330, 530 ) //Up by the True Believers
			} 
		} else if( roll > 95 ) {
			player.warp( "Hive_1", 735, 530 ) //YIKES! THE HIVE!!
		}
	}
}

def spawnPumpkinFluffs() {
	if( fluffSpawnNum > 0 ) {
		fluffList << pumpFluff202.forceSpawnNow() //This list starts at position 0, so subtract one from the list size to get the correct list position
		fluffList.get( fluffList.size() - 1 ).addHate( random ( playerList ), 1 ) 
		runOnDeath( fluffList.get( fluffList.size() - 1 ) ) { event -> fluffList.remove( fluffList.get( fluffList.size() - 1 ) ); checkForLoot( event ) }
		fluffSpawnNum -- 
		myManager.schedule(1) { spawnPumpkinFluffs() } 
	} else {
		myManager.schedule(30) { disposalTimer() }
	}
}

def synchronized checkForLoot( event ) {
	def awardList = playerList.clone().intersect( event.killer.getCrew() )
	awardList.each {
		roll = random( 100 )
		if( !it.hasQuestFlag( GLOBAL, "Z01GotOhMyGumball10" ) ) {
			if( roll <= 25 ) { //trick is 50% and pumpkin fluffs are 50% of that and this is 20% of that, so 2.5% chance of getting the item for each fluff killed.
				it.centerPrint( "Happy Halloween! You found the 'Oh My Gumball'!" )
				it.grantItem( "28768" ) //TODO: Fill in with the correct item ID
				it.setQuestFlag( GLOBAL, "Z01GotOhMyGumball10" )
			}
		} else { //if player has already received the 'Oh My Gumball' item
			if( roll <= 25 ) { //only tell them about the 'Oh My Gumball' if they would have normally received it
				it.centerPrint( "You've already found the 'Oh My Gumball', so all you find this time is pumpkin guts." )
			}
		}			
	}
}

def disposalTimer() {
	if( !fluffList.isEmpty() ) {
		fluffList.each{ it.dispose() }
	}
	fluffsAlreadySpawned = false
}

//=========================================
// TREATS!!!                               
//=========================================

commonList = [ 100272, 100289, 100385, 100297, 100289, 100397, 100291, 100262, 100263, 100298, 100388, 100278, 100275, 100283, 100281, 100367, 100411, 100394, 100284, 100405, 100267, 100265, 100285, 100398, 100397, 100376, 100296 ]

uncommonList = [ 100280, 100279, 100270, 100380, 100279, 100384, 100378, 100261, 100271, 100276, 100258, 100268, 100381, 100382, 100282, 100383, 100390, 100299, 100286, 100369, 100400, 100387 ]

recipeList = [ "17766", "17764", "17772", "17758", "17756", "17861", "17857", "17755", "17848", "17849", "17852", "17851", "17850", "17753", "17833", "17831", "17754", "17836", "17835", "17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779" ]

ringList = [ "17867", "17716", "17722", "17723", "17737", "17738", "17741", "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "20253" ]

def synchronized treat( player, awardList ) {
	//scale the returned results by relative con levels for each Crew member
	roll = random( 100 )

	lootMultiplier = player.getConLevel() / 10 //the lower the overall CL of a player in relation to player max CL (currently 10), the lower the reward so the trick or treating can be meaningful to all CLs
	//grant gold
	if( roll <= 50 ) {
		goldGrant = ( random( 100000, 300000 ) * lootMultiplier ).intValue()
		player.grantCoins( goldGrant ) 
		player.centerPrint( "It's raining gold!" )
	//grant common item
	} else if( roll > 50 && roll <= 65 ) {
		player.grantQuantityItem( random( commonList ), random(2,5) )
		player.centerPrint( "All right! A loot item!" )
	//grant uncommon item
	} else if( roll > 65 && roll <= 75 ) {
		player.grantQuantityItem( random( uncommonList ), random(1,3) )
		player.centerPrint( "An uncommon loot item!" )
	//grant recipes
	} else if( roll > 75 && roll <= 85 ) {
		player.grantItem( random( recipeList ) )
		player.centerPrint( "Awesome! A recipe!" )
	//grant orbs
	} else if( roll > 85 && roll <= 95 ) {
		orbGrant = ( 10 * lootMultiplier ).intValue()
		player.grantQuantityItem( 100257, orbGrant )
		player.centerPrint( "Look! Charge Orbs!!" )
	//rings
	} else if( roll > 95 ) {
		player.grantRing( random( ringList ), true )
		player.centerPrint( "Woohoo! A Ring!" )
	}
}

def Charon = spawnNPC("[NPC] Charon", myRooms.BARTON_202, 294, 610)
Charon.setRotation( 45 )
Charon.setDisplayName( "[NPC] Charon" )

def charonDefault = Charon.createConversation("charonDefault", true)

def charonDefault1 = [id:1]
charonDefault1.npctext = "Well, well, well.  Your name I remember reading from my book for today."
charonDefault1.options = []
charonDefault1.options << [text:"This is just a mule!", result: 2]
charonDefault1.options << [text:"Alright, I've made my peace.  I'm ready.", result: 3]
charonDefault1.options << [text:"I'm not falling for this again.", result: 15]
charonDefault1.options << [text:"** run for your life **", result: 99]
charonDefault.addDialog(charonDefault1, Charon)

def charonDefault2 = [id:2]
charonDefault2.npctext = "A tricky one here, aren't we?  Don't worry, all the doom and gloom talk is my boss's deal, along with relying on his book to dictate his day to day.  Gary Grimoire and his precious book.  He hates it when I use his real name."
charonDefault2.result = 4
charonDefault.addDialog(charonDefault2, Charon)

def charonDefault3 = [id:3]
charonDefault3.npctext = "BWA HA HA HA HA!  You mortals are so gullible.  Cheer up!  All the doom and gloom talk is my boss's deal, along with relying on his book to dictate his day to day.  Gary Grimoire and his precious book.  He hates it when I use his real name."
charonDefault3.result = 4
charonDefault.addDialog(charonDefault3, Charon)

def charonDefault4 = [id:4]
charonDefault4.npctext = "After he got promoted and donned the Reaper moniker and scythe, he shortened his last name to sound more menacing.  A similar title structure worked pretty well for the Vader's mystique too, didn't it? "
charonDefault4.result = 5
charonDefault.addDialog(charonDefault4, Charon)

def charonDefault5 = [id:5]
charonDefault5.npctext = "Gary's full last name was instead adopted as the definition of a spell book, since everyone thought someone with powers like him had to be using a magic tome.  If only they knew. "
charonDefault5.result = 6
charonDefault.addDialog(charonDefault5, Charon)

def charonDefault6 = [id:6]
charonDefault6.npctext = "Anyway, don't be fooled by the act though, he's just a bookworm and a loner.  A perfectionist bookworm that can't stand to have any errors in the pages."
charonDefault6.result = 7
charonDefault.addDialog(charonDefault6, Charon)

def charonDefault7 = [id:7]
charonDefault7.npctext = "That's where I come in.  Trying to fix any tiny discrepancies in the entries that he doesn't have time for.  So I get stuck with oddball cases like my current client."
charonDefault7.result = 8
charonDefault.addDialog(charonDefault7, Charon)

def charonDefault8 = [id:8]
charonDefault8.npctext = '"They" are dripping with animosity towards some character named Jack.  My already dead client is still rolling in is grave, completely beside himself over this creature named Jack.'
charonDefault8.result = 9
charonDefault.addDialog(charonDefault8, Charon)

def charonDefault9 = [id:9]
charonDefault9.npctext = "If he could just let it go, my client could easily be hanging out upstairs on fluffy clouds without a care in the afterlife.  But he can hold a grudge, stuck at this fork in the road, which just puts me in a jam."
charonDefault9.result = 10
charonDefault.addDialog(charonDefault9, Charon)

def charonDefault10 = [id:10]
charonDefault10.npctext = "I'm not allowed to get directly involved in such matters, only to manipulate... err, convince others to help out.  He'd really appreciate it I'm sure, as would I, so I could get back to the office."
charonDefault10.result = 11
charonDefault.addDialog(charonDefault10, Charon)

def charonDefault11 = [id:11]
charonDefault11.npctext = "Perks of this job, among other things, is the ability to transport anyone anywhere though.  Like you to the location of my client."
charonDefault11.options = []
charonDefault11.options << [text:"I'd rather not right now.", result: 99]
charonDefault11.options << [text:"Sure, I'll help out.", result: 12]
charonDefault.addDialog(charonDefault11, Charon)

def charonDefault12 = [id:12]
charonDefault12.npctext = "He's hanging out in a place called 'The Jackyard' where it's perpetually Halloween, so keep your wits about you.  From what I've heard, this Jack guy is one tough cookie and could be quite a handful if you pick a fight."
charonDefault12.result = 13
charonDefault.addDialog(charonDefault12, Charon)

def charonDefault13 = [id:13]
charonDefault13.npctext = "But what is it that your kind always likes to say...  YOLO?"
charonDefault13.playertext = 'Why do you refer to your client as both "he" and "they"?'
charonDefault13.result = 14
charonDefault.addDialog(charonDefault13, Charon)

def charonDefault14 = [id:14]
charonDefault14.npctext = "You'll see why after meeting my client.  So, will you venture to The Jackyard?"
charonDefault14.options = []
charonDefault14.options << [text:"Thanks, but I'll just stick with the potentially lethal threats around here.", result: 99]
charonDefault14.options << [text:"Alright, YOLO it is.", result: 15]
charonDefault.addDialog(charonDefault14, Charon)

def charonDefault15 = [id:15]
charonDefault15.npctext = "Okay, let me send your spirit to The Jackyard then.  When you're finished, look for my ... short stacked client for a ride back.  He'll dial me up, spiritual connection with the dead and all that stuff."
charonDefault15.options = []
charonDefault15.options << [text:"Wait! I've changed my mind.", result: 99]
charonDefault15.options << [text:"Here goes everything.", result: 16]
charonDefault.addDialog(charonDefault15, Charon)

def charonDefault16 = [id:16]
charonDefault16.npctext = "Have a good fight, I would tell you to stay alive but that's not my job."
charonDefault16.exec = { event ->
        event.actor.warp( "Bar_201", 400, 510 )
}
charonDefault16.result = DONE
charonDefault.addDialog(charonDefault16, Charon)

def charonDefault17 = [id:99]
charonDefault17.npctext = "I'll be expecting you, whether you like it or not."
charonDefault17.result = DONE
charonDefault.addDialog(charonDefault17, Charon)


