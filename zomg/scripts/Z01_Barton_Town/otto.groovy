import com.gaiaonline.mmo.battle.script.*;

def Otto = spawnNPC("Otto-VQS", myRooms.BARTON_104, 1085, 590)
Otto.setDisplayName( "Otto" )

def aroundTown = makeNewPatrol()
aroundTown.addPatrolPoint( "BARTON_104", 1080, 590, 0)
aroundTown.addPatrolPoint( "BARTON_104", 845, 750, 0)
aroundTown.addPatrolPoint( "BARTON_204", 585, 335, 0)
aroundTown.addPatrolPoint( "BARTON_204", 170, 765, 0)
aroundTown.addPatrolPoint( "BARTON_203", 1325, 845, 0)
aroundTown.addPatrolPoint( "BARTON_303", 825, 95, 0)
aroundTown.addPatrolPoint( "BARTON_303", 455, 390, 0)
aroundTown.addPatrolPoint( "BARTON_303", 220, 135, 0)
aroundTown.addPatrolPoint( "BARTON_302", 1360, 120, 0)
aroundTown.addPatrolPoint( "BARTON_302", 725, 170, 0)
aroundTown.addPatrolPoint( "BARTON_201", 1470, 480, 3)
aroundTown.addPatrolPoint( "BARTON_102", 190, 495, 0)
aroundTown.addPatrolPoint( "BARTON_101", 1470, 225, 0)
aroundTown.addPatrolPoint( "BARTON_1", 845, 920, 0)
aroundTown.addPatrolPoint( "BARTON_1", 1080, 690, 0)
aroundTown.addPatrolPoint( "BARTON_2", 220, 690, 0)
aroundTown.addPatrolPoint( "BARTON_2", 515, 820, 0)
aroundTown.addPatrolPoint( "BARTON_2", 1450, 680, 0)
aroundTown.addPatrolPoint( "BARTON_3", 250, 760, 0)
aroundTown.addPatrolPoint( "BARTON_103", 365, 230, 0)
aroundTown.addPatrolPoint( "BARTON_103", 1215, 205, 0)
aroundTown.addPatrolPoint( "BARTON_104", 1080, 590, 0)


Otto.setPatrol( aroundTown )
Otto.startPatrol()


