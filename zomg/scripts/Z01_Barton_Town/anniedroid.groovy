import com.gaiaonline.mmo.battle.script.*;

/* AnnieDroid */

Annie = spawnNPC("Anniedroid", myRooms.BARTON_301, 970, 450)

Annie.setWanderBehavior( 35, 80, 3, 7, 150 )
Annie.setRotation( 45 )
Annie.startWander(true)
Annie.setDisplayName( "Anniedroid" )

//STOP MOVING WHEN TALKING
def stopMoving(event) {
	Annie.pause()
}

//START MOVING AFTER DONE TALKING
def startMoving(event) {
	Annie.pauseResume()
}

def annieTalk = Annie.createConversation("adminTalk", true, "!QuestStarted_38:2")
annieTalk.setUrgent( false )

def dialog1 = [id:1]
dialog1.npctext = "Loading...Please wait... Ah, there you are. What may I do for you, %p?"
//Although not technically a computer movie reference, the "Loading...Please wait..." is from Everquest...the first hugely successful MMORPG computer game.
dialog1.options = []
dialog1.options << [text: "Are you a robot or something?", result: 3, exec: { event -> stopMoving() } ]
dialog1.options << [text: "Uh...nothing. You seem pretty busy. I'll come back another time.", result: 14, exec: { event -> stopMoving() } ]
annieTalk.addDialog(dialog1, Annie)

def dialog2 = [id:2]
dialog2.npctext = "I am not a robot. I am a self-cognitive thinking construct able to calculate functions at lightning speed and access any database, anywhere."
dialog2.playertext = "Really? Because, well, you look like a girl in a robot suit to me."
dialog2.result = 3
annieTalk.addDialog(dialog2, Annie)

def dialog3 = [id:3]
dialog3.npctext = "Negative. My call letters are ANI-DR-OID-182 and I exist to preserve the life of my crew and to fulfill my mission."
//movie reference to MU-TH-R-182, the computer in the Nostromo in "Alien"
dialog3.playertext = "What?"
dialog3.options = []
dialog3.options << [text: "Your crew?", result: 4]
dialog3.options << [text: "Your mission?", result: 8]
dialog3.options << [text: "I think you've got a screw loose. See ya.", result: 14]
annieTalk.addDialog(dialog3, Annie)

def dialog4 = [id:4]
dialog4.npctext = "My crew consists of Dave and Frank. Have you seen them?"
//movie reference to Frank Poole and Dave Bowman from "2001: A Space Odyssey"
dialog4.playertext = "I've never heard of them. Are you sure your circuits aren't scrambled?"
dialog4.result = 5
annieTalk.addDialog(dialog4, Annie)

def dialog5 = [id:5]
dialog5.npctext = "...no. However, that would be...unfortunate. Perhaps my ghost has been hacked."
//movie refernce "Ghost in the Shell"
dialog5.playertext = "Your ghost? What?!? Talk sense!"
dialog5.result = 6
annieTalk.addDialog(dialog5, Annie)

def dialog6 = [id:6]
dialog6.npctext = "Listen bede-bede-bede-bub. Your stress levels register as cautionary. Please be calm."
//buck rodgers
dialog6.playertext = "I *am* calm. Shutup!"
dialog6.result = 7
annieTalk.addDialog(dialog6, Annie)

def dialog7 = [id:7]
dialog7.npctext = "By your command."
//battlestar galactica
dialog7.exec = { event ->
	startMoving()
}
dialog7.result = DONE
annieTalk.addDialog(dialog7, Annie)

def dialog8 = [id:8]
dialog8.npctext = "Yes. I must decipher an alien communication that I have recently received."
dialog8.playertext = "Aliens? You spoke to the Zurg?"
dialog8.result = 9
annieTalk.addDialog(dialog8, Annie)

def dialog9 = [id:9]
dialog9.npctext = "Negative. Not the Zurg. The message I received was garbled, but it started 'Klaatu Bar...' The transmission was lost thereafter."
//The Day the Earth Stood Still
dialog9.playertext = "Wait a minute, do you mean 'Klaatu Barada...?"
dialog9.result = 10
annieTalk.addDialog(dialog9, Annie)

def dialog10 = [id:10]
dialog10.npctext = "Danger! Danger! Danger, %p!"
//Lost in Space
dialog10.playertext = "Danger? Where? What did you see?"
dialog10.result = 11
annieTalk.addDialog(dialog10, Annie)

def dialog11 = [id:11]
dialog11.npctext = "*buzz* *whirr* *click* /reset"
dialog11.playertext = "ummm..."
dialog11.result = 12
annieTalk.addDialog(dialog11, Annie)

def dialog12 = [id:12]
dialog12.npctext = "Shall we play a game?"
//War Games
dialog12.playertext = "Good lord. You're still not making sense. What else could go wrong?"
dialog12.result = 13
annieTalk.addDialog(dialog12, Annie)

def dialog13 = [id:13]
dialog13.npctext = "Nothing can go wrong...go wrong...go wrong..."
//Westworld
dialog13.playertext = "Oh forget it."
dialog13.exec = { event ->
	startMoving()
}
dialog13.result = DONE
annieTalk.addDialog(dialog13, Annie)

def dialog14 = [id:14]
dialog14.npctext = "Affirmative. /quit. /logoff."
dialog14.playertext = "Weirdo."
dialog14.exec = { event ->
	startMoving()
}
dialog14.result = DONE
annieTalk.addDialog(dialog14, Annie)

//----------------------------------------------------------------------------------------------------------------
//Dialog for Fernando's Secret quest - created by gfern                                                           
//----------------------------------------------------------------------------------------------------------------

def fernandosSecret = Annie.createConversation("fernandosSecret", true, "QuestStarted_38", "!QuestStarted_38:3")
fernandosSecret.setUrgent( true )

def secret1 = [id:1]
secret1.npctext = "Loading...Please wait... Ah, there you are. What may I do for you, %p?"
secret1.result = 2
secret1.exec = { event ->
	stopMoving()
}
fernandosSecret.addDialog(secret1, Annie)

def secret2 = [id:2]
secret2.playertext = "Hey Annie. I was just talking to Fernando about his accident but he won't tell me what happened. Do you have any idea?"
secret2.result = 3
fernandosSecret.addDialog(secret2, Annie)

def secret3 = [id:3]
secret3.npctext = "This information is contained in my data-bank. Shall I access filename fernando_accident?"
secret3.result = 4
fernandosSecret.addDialog(secret3, Annie)

def secret4 = [id:4]
secret4.playertext = "Yes, please."
secret4.result = 5
fernandosSecret.addDialog(secret4, Annie)

def secret5 = [id:5]
secret5.npctext = "One moment... Accessing..."
secret5.result = 6
fernandosSecret.addDialog(secret5, Annie)

def secret6 = [id:6]
secret6.npctext = "...Accessing..."
secret6.result = 8
fernandosSecret.addDialog(secret6, Annie)

def secret8 = [id:8]
secret8.npctext = "...Mounting Data..."
secret8.result = 9
fernandosSecret.addDialog(secret8, Annie)

def secret9 = [id:9]
secret9.npctext = "...Executing Unrelated Functions..."
secret9.result = 10
fernandosSecret.addDialog(secret9, Annie)

def secret10 = [id:10]
secret10.npctext = "...Beginning System Erasure, Cancel or Allow?"
secret10.options = []
secret10.options << [text:"Cancel.", result: 7]
secret10.options << [text:"Allow.", result: 18]
fernandosSecret.addDialog(secret10, Annie)

def secret7 = [id: 7]
secret7.npctext = "...Cancelling System Erasure..."
secret7.result = 11
fernandosSecret.addDialog(secret7, Annie)

def secret11 = [id:11]
secret11.playertext = "Could this take any longer?"
secret11.result = 12
fernandosSecret.addDialog(secret11, Annie)

def secret12 = [id:12]
secret12.npctext = "...Taking Longer..."
secret12.result = 13
fernandosSecret.addDialog(secret12, Annie)

def secret13 = [id:13]
secret13.playertext = "GAH! Display already!"
secret13.result = 14
fernandosSecret.addDialog(secret13, Annie)

def secret14 = [id:14]
secret14.npctext = "...Displaying File... fernando_accident..."
secret14.result = 15
fernandosSecret.addDialog(secret14, Annie)

def secret15 = [id:15]
secret15.npctext = "Fernando (A.K.A. Fabulously Flying Fernando), performed with zipper down. Refers to incident as 'the accident.' Last recorded instance of fernando_dance. End of file."
secret15.result = 16
fernandosSecret.addDialog(secret15, Annie)

def secret16 = [id:16]
secret16.playertext = "Were you waiting for me to say 'display' the whole time?"
secret16.result = 17
fernandosSecret.addDialog(secret16, Annie)

def secret17 = [id:17]
secret17.npctext = "Affirmative."
secret17.result = 19
fernandosSecret.addDialog(secret17, Annie)

def secret18 = [id:18]
secret18.npctext = "Error... No Disassemble Anniedroid... Exiting."
secret18.exec = { event ->
	startMoving()
}
secret18.result = DONE
fernandosSecret.addDialog(secret18, Annie)

def secret19 = [id:19]
secret19.playertext = "Uhg. Your interface is worse than DOS."
secret19.result = 20
fernandosSecret.addDialog(secret19, Annie)

def secret20 = [id:20]
secret20.npctext = "Error... Unknown Command... Exiting."
secret20.result = 21
fernandosSecret.addDialog(secret20, Annie)

def secret21 = [id:21]
secret21.playertext = "What?! I wasn't even trying to give a command! Oh, nevermind. I need to go find Fernando some zipper-less pants, anyway."
secret21.quest = 38
secret21.exec = { event ->
	startMoving()
}
secret21.result = DONE
fernandosSecret.addDialog(secret21, Annie)