import com.gaiaonline.mmo.battle.script.*;

/*
//DICTIONARY STUFF FOR EVENTUAL LOCALIZATION

import com.gaiaonline.mmo.battle.l10n.ResourceFactory;

// Load dictionary of strings for the script.
// (TODO: get the locale from the BattleUser instead.)
def locale = java.util.Locale.getDefault();
def dict = ResourceFactory.getResources( "scripts.bartontrench.clara", locale );
*/

def Clara = spawnNPC("BFG-Clara", myRooms.BARTON_303, 200, 600)
Clara.setRotation( 45 )
Clara.setDisplayName( "Lt. Clara" )

//---------------------------------------------------------
// DEFAULT CONVERSATION                                    
//---------------------------------------------------------

def GateInfo = Clara.createConversation("GateInfo", true, "!Z22GoTalkToClara", "!QuestStarted_54", "!QuestStarted_84:2")
GateInfo.setUrgent( false )

def SouthGate1 = [id:1]
SouthGate1.npctext = "Good day. I'm Clara and this is South Gate. Beyond this gate lie the villages of Barton Town. How may I be of assistance, citizen?"
SouthGate1.playertext = "Well..."
SouthGate1.options = []
SouthGate1.options << [text:"Do you know where Leon is at the moment?", result: 2]
SouthGate1.options << [text:"Have you been busy fighting against the Animated?", result: 5]
SouthGate1.options << [text:"...no thanks. I shouldn't have bothered you.", result: DONE]
GateInfo.addDialog( SouthGate1, Clara)

def SouthGate2 = [id:2]
SouthGate2.npctext = "Of course. Commander Leon defends the villages beyond this Gate. If you care about your Town's well-being, I recommend that you seek him at once."
SouthGate2.options = []
SouthGate2.options << [text:"I'll go look for him immediately!", result: 4]
SouthGate2.options << [text:"Nice tin can armor...", result: 3]
SouthGate2.options << [text:"Uh, nevermind. That's not really my speed.", result: DONE]
GateInfo.addDialog( SouthGate2, Clara)

def SouthGate3 = [id:3]
SouthGate3.npctext = "What?!? Oh, err. Listen. Don't give me any lip. You know as well as I do we wear this sweaty 'tin can armor' because Leon wants it that way, and what Leon wants, he gets. Don't bust my hump about it...citizen."
SouthGate3.result = DONE
GateInfo.addDialog( SouthGate3, Clara)

def SouthGate4 = [id:4]
SouthGate4.npctext = "Good for you! The Guard can use all the help it can get!"
SouthGate4.result = DONE
SouthGate4.flag = "Z1FindLeon"
GateInfo.addDialog( SouthGate4, Clara)

def SouthGate5 = [id:5]
SouthGate5.npctext = "No, not really. Most of the action is down by the Villages. The North and West gates get more action than this post, but we stay vigilant here...just in case!"
SouthGate5.result = DONE
GateInfo.addDialog( SouthGate5, Clara)

//---------------------------------------------------------
// LUNCH QUEST (Clara's Part) (Also acts as an interim 
// conversation until the player hands the lunch pail to
// Leon.)
//---------------------------------------------------------
// After the lunch pail quest has started, but before Leon actually has it in his hands.

def LunchClara = Clara.createConversation("LunchClara", true, "QuestStarted_54:2", "!QuestStarted_54:3", "!QuestStarted_84:2")
LunchClara.setUrgent( true )

def Lunch1 = [id:1]
Lunch1.npctext = "Hi there, %p!"
Lunch1.playertext = "Hi there, Clara! Olivia sent me over to ask you about Leon."
Lunch1.result = 2
LunchClara.addDialog(Lunch1, Clara)

def Lunch2 = [id:2]
Lunch2.npctext = "Oh? What do you need to know about?"
Lunch2.result = 3
LunchClara.addDialog(Lunch2, Clara)

def Lunch3 = [id:3]
Lunch3.options = []
Lunch3.options << [text:"What is Leon like?", result: 4]
Lunch3.options << [text:"Do you know where he is?", result: 7]
Lunch3.options << [text:"Is it safe out there?", result: 10]
Lunch3.options << [text:"I guess not. Thanks anyway.", result: DONE]
LunchClara.addDialog(Lunch3, Clara)

def Lunch4 = [id:4]
Lunch4.npctext = "Leon? He's extremely friendly, but he's also quite firm, which helps make him a good leader. He's helped each of us guards on so many occasions that we'd do just about anything for him by now."
Lunch4.playertext = "He's tough?"
Lunch4.result = 5
LunchClara.addDialog(Lunch4, Clara)

def Lunch5 = [id:5]
Lunch5.npctext = "Oh yeah. I mean, he's a big guy and everything, but you carried those cookies around earlier, so you know what I'm saying. He's eaten Olivia's cooking ALL OF HIS LIFE!!!"
Lunch5.playertext = "Ah. Right. Then he's *really* tough."
Lunch5.result = 6
LunchClara.addDialog(Lunch5, Clara)

def Lunch6 = [id:6]
Lunch6.npctext = "Yup. Tougher than me, that's for sure. Is there anything else I can help you with?"
Lunch6.playertext = "Well..."
Lunch6.result = 3
LunchClara.addDialog(Lunch6, Clara)

def Lunch7 = [id:7]
Lunch7.npctext = "Leon tends to roam around, but he's probably near one of the entrances to the Village enclave to the south. Just follow the road down from Barton's South Gate and you should eventually bump into Leon."
Lunch7.playertext = "Okay. Sounds great. His mom asked me to take this lunch pail to him. He left it behind."
Lunch7.result = 8
LunchClara.addDialog(Lunch7, Clara)

def Lunch8 = [id:8]
Lunch8.npctext = "Lunch pail? From Olivia? Wow. Well, if you get in trouble out there, don't be tempted to open it up as a weapon. The smell alone would probably knock you out before you got a chance to use it against a foe."
Lunch8.playertext = "Oh, don't worry. I do NOT plan to open this thing. I remember the Cookies!"
Lunch8.result = 9
LunchClara.addDialog(Lunch8, Clara)

def Lunch9 = [id:9]
Lunch9.npctext = "Good, good. Thanks...and be safe!"
Lunch9.quest = 54 //Once you find out where Leon is, the quest is updated for this step and step 3 is begun.
Lunch9.exec = { event ->
	event.player.removeMiniMapQuestActorName( "BFG-Clara" )
	event.player.addMiniMapQuestActorName( "Leon-VQS" )
}
Lunch9.result = DONE
LunchClara.addDialog(Lunch9, Clara)

def Lunch10 = [id:10]
Lunch10.npctext = "No. It's definitely NOT safe out there."
Lunch10.playertext = "Any words of warning?"
Lunch10.result = 11
LunchClara.addDialog(Lunch10, Clara)

def Lunch11 = [id:11]
Lunch11.npctext = "Well, the road between the gate and the villages is pretty clear and safe...mostly. But once you head west a bit, there are attacks by things that used to be lawn ornaments."
Lunch11.playertext = "Lawn ornaments? You're kidding!"
Lunch11.result = 12
LunchClara.addDialog(Lunch11, Clara)

def Lunch12 = [id:12]
Lunch12.npctext = "Not kidding at all. They're some of the new Animated, and they seem to be led by a contingent of Lawn Gnomes. They've set up an outpost of sorts west and south of the villages. You should steer clear of that area unless you're with a group, and even then, you need to be prepared for trouble when you get there. They're definitely hostile!"
Lunch12.playertext = "Anything else?"
Lunch12.result =13
LunchClara.addDialog(Lunch12, Clara)

def Lunch13 = [id:13]
Lunch13.npctext = "Some of the other guards told me the gnomes they've even set up these big mushroom cannons! If you see any, you should watch yourself."
Lunch13.playertext = "Wow. I'm glad I asked. Thanks, Clara!"
Lunch13.result = 14
LunchClara.addDialog(Lunch13, Clara)

def Lunch14 = [id:14]
Lunch14.npctext = "No problem. Is there anything else?"
Lunch14.result = 3
LunchClara.addDialog(Lunch14, Clara)

//--------------------------------------------------------------
//A CALL TO ARMS - QUEST CONVERSATION                           
//--------------------------------------------------------------
def claraArmsConvo = Clara.createConversation("claraArmsConvo", true, "QuestStarted_84:2")
claraArmsConvo.setUrgent( true )
claraArmsConvo.setUrgent( true )

def armsConvo1 = [id:1]
armsConvo1.npctext = "What is this? Leon, it is? No, one of his little friends! Can I help you?"
armsConvo1.playertext = "Leon sent me. We extracted these battle plans from a lawn gnome courier."
armsConvo1.result = 2
claraArmsConvo.addDialog(armsConvo1, Clara)

def armsConvo2 = [id:2]
armsConvo2.npctext = "Battle plans? Let me have a look."
armsConvo2.result = 3
claraArmsConvo.addDialog(armsConvo2, Clara)

def armsConvo3 = [id:3]
armsConvo3.npctext = "Exactly what leads you to believe these are anything but random doodles?"
armsConvo3.options = []
armsConvo3.options << [text:"Actually, they might be nothing more than that. We just wanted your opinion.", result: 4]
armsConvo3.options << [text:"Are you kidding? This is clear-cut proof of a planned gnome invasion!", result: 10]
claraArmsConvo.addDialog(armsConvo3, Clara)

def armsConvo4 = [id:4]
armsConvo4.npctext = "I've been watching the gnomes for a while now, and I think I can shed some light on what this really means."
armsConvo4.result = 5
claraArmsConvo.addDialog(armsConvo4, Clara)

def armsConvo5 = [id:5]
armsConvo5.npctext = "When the gnomes first became Animated, they just kind of bumbled around aimlessly and attacked us when we got near. When they started to multiply, we set up some patrols to check up on them."
armsConvo5.result = 6
claraArmsConvo.addDialog(armsConvo5, Clara)

def armsConvo6 = [id:6]
armsConvo6.npctext = "Then they set up patrols to check up on us. In response, we sent a few guards over to knock them around, so they sent some gnomes over to knock us around."
armsConvo6.result = 7
claraArmsConvo.addDialog(armsConvo6, Clara)

def armsConvo7 = [id:7]
armsConvo7.npctext = "Seeing a pattern? I think the silly little guys are just trying to be people. I even saw one pretending to go to the bathroom on a tree! Goodness knows where he learned that."
armsConvo7.playertext = "Great. You've been so helpful. *sigh*"
armsConvo7.result = 8
claraArmsConvo.addDialog(armsConvo7, Clara)

def armsConvo8 = [id:8]
armsConvo8.npctext = "They must have seen us send a courier down to Leon, so they started sending couriers of their own. They're just mindlessly aping us. It's as simple as that."
armsConvo8.options = []
armsConvo8.options << [text:"So, no grand invasion plans?", result: 9]
armsConvo8.options << [text:"Do you think they learned to be violent from us, too?", result: 11]
claraArmsConvo.addDialog(armsConvo8, Clara)

def armsConvo9 = [id:9]
armsConvo9.npctext = "Not today. You can tell Leon to stop worrying his big old goofy head about it."
armsConvo9.playertext = "Thanks, I'll do that."
armsConvo9.quest = 84
armsConvo9.exec = { event -> 
	event.player.removeMiniMapQuestActorName("BFG-Clara")
	event.player.addMiniMapQuestActorName("Leon-VQS")
}
armsConvo9.result = DONE
claraArmsConvo.addDialog(armsConvo9, Clara)

def armsConvo10 = [id:10]
armsConvo10.npctext = "Gnome invasion? Are you kidding me? They're three feet tall. The walls are twenty feet. What are they going to do, pole vault in?"
armsConvo10.result = 4
claraArmsConvo.addDialog(armsConvo10, Clara)

def armsConvo11 = [id:11]
armsConvo11.npctext = "That would make a fun little philosophical point, but no. We thought they were hilarious at first, and all we wanted to do was hug them. Then came the ankle-biting."
armsConvo11.playertext = "But, I suppose that doesn't mean they're planning to invade."
armsConvo11.result = 9
claraArmsConvo.addDialog(armsConvo11, Clara)

