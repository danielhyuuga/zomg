import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def PoloPaul = spawnNPC("Polo Shirt Paul", myRooms.BARTON_102, 310, 675)
PoloPaul.setRotation( 135 )
PoloPaul.setDisplayName( "Polo Shirt Paul" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	PoloPaul.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/paul_strip.png")
}

//======================================
// RANDOM CONVERSATIONS                 
//======================================

def Random1 = PoloPaul.createConversation("Random1", true, "Z0PoloPaulR1" )

def rand1 = [id:1]
rand1.npctext = "Like the shirt? I couldn't find one with an alligator, but this one is quite snazzy."
rand1.flag = "!Z0PoloPaulR1"
rand1.exec = { event ->
	selectRandomFlag( event )
}
rand1.result = DONE
Random1.addDialog(rand1, PoloPaul)
//--------------------
def Random2 = PoloPaul.createConversation("Random2", true, "Z0PoloPaulR2" )

def rand2 = [id:1]
rand2.npctext = "Have you been to the Music Box yet? It's a new shop that just opened up in the northwest corner of the town. Check it out!"
rand2.flag = "!Z0PoloPaulR2"
rand2.exec = { event ->
	selectRandomFlag( event )
}
rand2.result = DONE
Random2.addDialog(rand2, PoloPaul)
//--------------------
def Random3 = PoloPaul.createConversation("Random3", true, "Z0PoloPaulR3" )

def rand3 = [id:1]
rand3.npctext = "Can you believe that Durem and Aekea are totally cut off from us because of the Animated?"
rand3.flag = "!Z0PoloPaulR3"
rand3.exec = { event ->
	selectRandomFlag( event )
}
rand3.result = DONE
Random3.addDialog(rand3, PoloPaul)
//--------------------
def Random4 = PoloPaul.createConversation("Random4", true, "Z0PoloPaulR4" )

def rand4 = [id:1]
rand4.npctext = "I've heard that the recent Animated spawnings are due to the Zurg returning to Gaia!"
rand4.flag = "!Z0PoloPaulR4"
rand4.exec = { event ->
	selectRandomFlag( event )
}
rand4.result = DONE
Random4.addDialog(rand4, PoloPaul)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

randomFlag = [ "Z0PoloPaulR1", "Z0PoloPaulR2", "Z0PoloPaulR3", "Z0PoloPaulR4" ]

convoFlag = random( randomFlag ) //initializing it just for the hell of it. This shouldn't be necessary.

playerList = []
fluffList = []

myManager.onEnter( myRooms.BARTON_102 ) { event ->
	if( isPlayer( event.actor) ) {
		playerList << event.actor 
		event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" )
		convoFlag = random( randomFlag )
		event.actor.setQuestFlag( GLOBAL, convoFlag )
	}
}

myManager.onExit( myRooms.BARTON_102 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
		playerList.remove( event.actor )
	}
}

def selectRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( randomFlag )
	if( lastFlag == convoFlag ) {
		selectRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}

if (eventStart != 'HALLOWEEN') return

//======================================
// HALLOWEEN TRICK OR TREATING SCRIPT   
//======================================

//Pumpkin Fluff Spawner
pumpFluff102 = myRooms.BARTON_102.spawnSpawner( "pumpFluff102", "pumpkin_fluff", 6 )
pumpFluff102.setPos( 360, 830 )
pumpFluff102.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
pumpFluff102.setMonsterLevelForChildren( 1.0 )

pumpFluff102.stopSpawning()

//=========================================
// JACK-O-LANTERN SWITCH                   
//=========================================

//TODO : Set things up so that trick or treating only occurs at night (when the pumpkins light up)
// GST test : Turns ON pumpkins at night, turns them OFF during day.
// Pumpkins are locked during the day.
// Pumpkins turn off when activated at night, but immediately turn back ON for the next player. (Light flickers when used.)

jack102 = makeSwitch( "jack102", myRooms.BARTON_102, 325, 740 )
jack102.setRange( 200 )

def startTrickOrTreating = { event ->
	jack102.on()
	if( !event.actor.hasQuestFlag( GLOBAL, "TrickOrTreatActive" ) ) {
		//set a preventative flag that expires in 5 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "TrickOrTreatActive" ) 
		myManager.schedule(5) { event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" ) }
		//now push the trick or treat dialog
		event.actor.setQuestFlag( GLOBAL, "Z01TrickOrTreat102A" )
		PoloPaul.pushDialog( event.actor, "trickOrTreat" )
	}
}
	
jack102.whenOff( startTrickOrTreating )

//Variable Initialization & Master Timer
fluffsAlreadySpawned = false
hourCounter = System.currentTimeMillis()
trickTreatTimer()
jackOLanternTimer()

//Every hour, update the hourCounter variable
def trickTreatTimer() {
	myManager.schedule(4200){ hourCounter = System.currentTimeMillis(); trickTreatTimer() }
	
}

//Turn on and unlock Jack O'Lanterns at night. Turn them off during day and lock them.
def jackOLanternTimer() {
//	println "**** GST = ${gst()} ****"
	if( ( gst() > 1800 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 600 ) ) { //trick or treating allowed between 6pm and 6am.
		jack102.unlock()
		jack102.on()
	} else {
		jack102.lock()
		jack102.off()
	}
	myManager.schedule(120) { jackOLanternTimer() } //check time of day again in five minutes
}
	
//=========================================
// DIALOG AND SCRIPT LOGIC                 
//=========================================

//determine how to respond to the player
def trickOrTreat = PoloPaul.createConversation( "trickOrTreat", true, "Z01TrickOrTreat102A" )

TT1 = [id:1]
TT1.npctext = "Happy Halloween!"
TT1.flag = "!Z01TrickOrTreat102A"
TT1.exec = { event ->
	if( event.player.getPlayerVar( "Z01VisitedPoloPaul" ).longValue() == null || event.player.getPlayerVar( "Z01VisitedPoloPaul" ).longValue() < hourCounter ) {
		def awardList = playerList.clone().intersect( event.player.getCrew() )
		awardList.each{
			//don't allow crewmembers that already got treats from this pumpkin to get them again just because they're in a different crew.
			if( it.getPlayerVar( "Z01VisitedPoloPaul" ).longValue() >= hourCounter ) {
				it.setQuestFlag( GLOBAL, "Z01NotYet" )
				PoloPaul.pushDialog( it, "notYet" )
			}
		} 
		event.player.setQuestFlag( GLOBAL, "Z01TrickOrTreat102B" )
		PoloPaul.pushDialog( event.player, "trickOrTreat2" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z01NotYet" )
		PoloPaul.pushDialog( event.player, "notYet" )
	}
}
trickOrTreat.addDialog( TT1, PoloPaul )

//If the player has received a treat too recently then don't give him a treat
def notYet = PoloPaul.createConversation( "notYet", true, "Z01NotYet" )

nYet1 = [id:1]
nYet1.npctext = "Wait a sec! I talked to you already tonight! Try again tomorrow night."
nYet1.flag = "!Z01NotYet"
nYet1.result = DONE
notYet.addDialog( nYet1, PoloPaul )

//if the player qualifies for a treat or trick, then get this conversation instead
def trickOrTreat2 = PoloPaul.createConversation( "trickOrTreat2", true, "Z01TrickOrTreat102B" )

TT2 = [id:1]
TT2.playertext = "Trick or Treat?"
TT2.flag = "!Z01TrickOrTreat102B"
TT2.result = DONE
TT2.exec = { event ->
	def awardList = playerList.clone().intersect( event.player.getCrew() )
	awardList.clone().each{ if( it.getPlayerVar( "Z01VisitedPoloPaul" ).longValue() >= hourCounter ) { awardList.remove( it ) } }
	awardList.each{
		//reset the playerVar hourCounter in case people try to exploit by joining the Crew after the first conversation dialog.
		it.setPlayerVar( "Z01VisitedPoloPaul", hourCounter )

		//increment the number of pumpkins clicked counter
		it.addToPlayerVar( "Z01JackOLanternsClicked", 1 )

		//check for Badge Updates
		if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 50 && it.getPlayerVar( "Z01JackOLanternsClicked" ) < 200 && !it.isDoneQuest(276) ) {
			it.updateQuest( 276, "Story-VQS" ) //Update the "Jack O'Smasher" badge
		} else if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 200 && !it.isDoneQuest(277) ) {
			it.updateQuest( 277, "Story-VQS" ) //Update the "Pumpkin King" badge
		}

		//determine if it's a trick or treat.
		roll = random( 100 )
		def player = it
		if( roll <= 40 ) { //This value is the chance of a trick. Remainder is the treat percentage.
			player.centerPrint( "You get TRICKED!" )
			trick( player, awardList )
		} else {
			player.centerPrint( "You get a TREAT!" )
			treat( player, awardList )
		}
	}
}
trickOrTreat2.addDialog( TT2, PoloPaul )

//=========================================
// TRICKS!!!                               
//=========================================
def synchronized trick( player, awardList ) {
	roll =  random( 100 )
	if( fluffsAlreadySpawned == true && roll <= 45 ) {
		roll = random( 46, 100 )
	}
	if( roll <= 45 ) {
		//pumpkin fluff spawn
		player.centerPrint( "From out of the pumpkin come pumpkin-costumed fluffs to attack you!" )
		fluffsAlreadySpawned = true
		fluffSpawnNum = awardList.size()
		spawnPumpkinFluffs()
	} else if( roll > 45 && roll <= 90 ) {
		//give the player a dentist gift
		dentistGift = random( 100 )
		if( dentistGift <= 50 ) {
			dentistGift = "100479"
			player.centerPrint( "Oh man, are you kidding? Dental Floss?!? Who gives dental floss instead of candy?" )
		} else { 
			dentistGift = "100481"
			player.centerPrint( "No way! A toothbrush? WTF?!? It's Halloween, people!" )
		}
		player.grantItem( dentistGift )
	} else if( roll > 90 ) {
		//warp the player
		player.centerPrint( "You feel a strange energy pulling you away to...somewhere else!" )
		roll = random( 100 )
		if( roll <= 50 ) {
			//somewhere in Barton (one of three places)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "BARTON_301", 580, 200 ) //behind statue
			} else if( roll == 2 ) {
				player.warp( "BARTON_4", 1400, 700 ) //near Dusty in corner
			} else if( roll == 3 ) {
				player.warp( "BARTON_1", 1360, 290 ) //in trees above Maestro
			} 
		} else if( roll > 50 && roll <= 80 ) {
			//somewhere in Village Greens (three places, one is nasty)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "VILLAGE_3", 310, 380 ) //in quiet corner at top of map
			} else if( roll == 2 ) {
				player.warp( "VILLAGE_201", 155, 375 ) //Way over by the goof marker at top of west side of map
			} else if( roll == 3 ) {
				player.warp( "VILLAGE_1004", 900, 590 ) //Down by the flamingo spiral at the bottom, right of the map. DANGER!!!
			} 
		} else if( roll > 80 && roll <= 95 ) {
			//danger spots in other zones near Crystals (pull a random one from a map with coords for warping)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "ZENGARDEN_502", 500, 625 ) //in Katsumi's shrine
			} else if( roll == 2 ) {
				player.warp( "BASSKEN_403", 350, 330 ) //By Logan
			} else if( roll == 3 ) {
				player.warp( "Aqueduct_602", 1330, 530 ) //Up by the True Believers
			} 
		} else if( roll > 95 ) {
			player.warp( "Hive_1", 735, 530 ) //YIKES! THE HIVE!!
		}
	}
}

def spawnPumpkinFluffs() {
	if( fluffSpawnNum > 0 ) {
		fluffList << pumpFluff102.forceSpawnNow() //This list starts at position 0, so subtract one from the list size to get the correct list position
		fluffList.get( fluffList.size() - 1 ).addHate( random ( playerList ), 1 ) 
		runOnDeath( fluffList.get( fluffList.size() - 1 ) ) { event -> fluffList.remove( fluffList.get( fluffList.size() - 1 ) ); checkForLoot( event ) }
		fluffSpawnNum -- 
		myManager.schedule(1) { spawnPumpkinFluffs() } 
	} else {
		myManager.schedule(30) { disposalTimer() }
	}
}

def synchronized checkForLoot( event ) {
	def awardList = playerList.clone().intersect( event.killer.getCrew() )
	awardList.each {
		roll = random( 100 )
		if( !it.hasQuestFlag( GLOBAL, "Z01GotOhMyGumball10" ) ) {
			if( roll <= 25 ) { //trick is 50% and pumpkin fluffs are 50% of that and this is 20% of that, so 2.5% chance of getting the item for each fluff killed.
				it.centerPrint( "Happy Halloween! You found the 'Oh My Gumball'!" )
				it.grantItem( "28768" ) //TODO: Fill in with the correct item ID
				it.setQuestFlag( GLOBAL, "Z01GotOhMyGumball10" )
			}
		} else { //if player has already received the 'Oh My Gumball' item
			if( roll <= 25 ) { //only tell them about the 'Oh My Gumball' if they would have normally received it
				it.centerPrint( "You've already found the 'Oh My Gumball', so all you find this time is pumpkin guts." )
			}
		}			
	}
}

def disposalTimer() {
	if( !fluffList.isEmpty() ) {
		fluffList.each{ it.dispose() }
	}
	fluffsAlreadySpawned = false
}

//=========================================
// TREATS!!!                               
//=========================================

commonList = [ 100272, 100289, 100385, 100297, 100289, 100397, 100291, 100262, 100263, 100298, 100388, 100278, 100275, 100283, 100281, 100367, 100411, 100394, 100284, 100405, 100267, 100265, 100285, 100398, 100397, 100376, 100296 ]

uncommonList = [ 100280, 100279, 100270, 100380, 100279, 100384, 100378, 100261, 100271, 100276, 100258, 100268, 100381, 100382, 100282, 100383, 100390, 100299, 100286, 100369, 100400, 100387 ]

recipeList = [ "17766", "17764", "17772", "17758", "17756", "17861", "17857", "17755", "17848", "17849", "17852", "17851", "17850", "17753", "17833", "17831", "17754", "17836", "17835", "17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779" ]

ringList = [ "17716", "17722", "17723", "17737", "17738", "17741", "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "20253" ]

def synchronized treat( player, awardList ) {
	//scale the returned results by relative con levels for each Crew member
	roll = random( 100 )

	lootMultiplier = player.getConLevel() / 10 //the lower the overall CL of a player in relation to player max CL (currently 10), the lower the reward so the trick or treating can be meaningful to all CLs
	//grant gold
	if( roll <= 50 ) {
		goldGrant = ( random( 100000, 300000 ) * lootMultiplier ).intValue()
		player.grantCoins( goldGrant ) 
		player.centerPrint( "It's raining gold!" )
	//grant common item
	} else if( roll > 50 && roll <= 65 ) {
		player.grantQuantityItem( random( commonList ), random(2,5) )
		player.centerPrint( "All right! A loot item!" )
	//grant uncommon item
	} else if( roll > 65 && roll <= 75 ) {
		player.grantQuantityItem( random( uncommonList ), random(1,3) )
		player.centerPrint( "An uncommon loot item!" )
	//grant recipes
	} else if( roll > 75 && roll <= 85 ) {
		player.grantItem( random( recipeList ) )
		player.centerPrint( "Awesome! A recipe!" )
	//grant orbs
	} else if( roll > 85 && roll <= 95 ) {
		orbGrant = ( 10 * lootMultiplier ).intValue()
		player.grantQuantityItem( 100257, orbGrant )
		player.centerPrint( "Look! Charge Orbs!!" )
	//rings
	} else if( roll > 95 ) {
		player.grantRing( random( ringList ), true )
		player.centerPrint( "Woohoo! A Ring!" )
	}
}
