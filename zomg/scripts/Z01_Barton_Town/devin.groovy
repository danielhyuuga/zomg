import com.gaiaonline.mmo.battle.script.*;

def Devin = spawnNPC("Devon-VQS", myRooms.BARTON_104, 1420, 545)

Devin.setRotation( 135 )
Devin.setDisplayName( "Devon" )

Devin.startWander( true )

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}
//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Devin.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/devon_strip.png")
}
