//==================================================
//NOTES WHEN DISABLING THIS SCRIPT AFTER CHRISTMAS: 
//	- Turn the water fountain sound back on.    
//	- Turn the Fountain Game back on            
//==================================================

//TODO : Turn on the badge stuff after setting up Badge missions and obtaining art for those badges.

/*
//=======================================
// SETTING AND UNSETTING THE GIFTBOXES   
//=======================================

def stage0 = {
	println "**** Setting up the gifts for xmasTree state = 0 ****"
	gift1A.setUsable( true )
	gift2A.setUsable( false )
	gift2B.setUsable( false )
	gift3A.setUsable( false )
	gift3B.setUsable( false )
	gift3C.setUsable( false )
	gift4A.setUsable( false )
	gift4B.setUsable( false )
	gift4C.setUsable( false )
	gift4D.setUsable( false )
	gift5A.setUsable( false )
	gift5B.setUsable( false )
	gift5C.setUsable( false )
	gift5D.setUsable( false )
	gift5E.setUsable( false )
}

def stage1 = {
	println "**** Setting up the gifts for xmasTree state = 1 ****"
	gift1A.setUsable( false )
	gift2A.setUsable( true )
	gift2B.setUsable( true )
	gift3A.setUsable( false )
	gift3B.setUsable( false )
	gift3C.setUsable( false )
	gift4A.setUsable( false )
	gift4B.setUsable( false )
	gift4C.setUsable( false )
	gift4D.setUsable( false )
	gift5A.setUsable( false )
	gift5B.setUsable( false )
	gift5C.setUsable( false )
	gift5D.setUsable( false )
	gift5E.setUsable( false )
}

def stage2 = {
	println "**** Setting up the gifts for xmasTree state = 2 ****"
	gift1A.setUsable( false )
	gift2A.setUsable( false )
	gift2B.setUsable( false )
	gift3A.setUsable( true )
	gift3B.setUsable( true )
	gift3C.setUsable( true )
	gift4A.setUsable( false )
	gift4B.setUsable( false )
	gift4C.setUsable( false )
	gift4D.setUsable( false )
	gift5A.setUsable( false )
	gift5B.setUsable( false )
	gift5C.setUsable( false )
	gift5D.setUsable( false )
	gift5E.setUsable( false )
}

def stage3 = {
	println "**** Setting up the gifts for xmasTree state = 3 ****"
	gift1A.setUsable( false )
	gift2A.setUsable( false )
	gift2B.setUsable( false )
	gift3A.setUsable( false )
	gift3B.setUsable( false )
	gift3C.setUsable( false )
	gift4A.setUsable( true )
	gift4B.setUsable( true )
	gift4C.setUsable( true )
	gift4D.setUsable( true )
	gift5A.setUsable( false )
	gift5B.setUsable( false )
	gift5C.setUsable( false )
	gift5D.setUsable( false )
	gift5E.setUsable( false )
}

def stage4 = {
	println "**** Setting up the gifts for xmasTree state = 4 ****"
	gift1A.setUsable( false )
	gift2A.setUsable( false )
	gift2B.setUsable( false )
	gift3A.setUsable( false )
	gift3B.setUsable( false )
	gift3C.setUsable( false )
	gift4A.setUsable( false )
	gift4B.setUsable( false )
	gift4C.setUsable( false )
	gift4D.setUsable( false )
	gift5A.setUsable( true )
	gift5B.setUsable( true )
	gift5C.setUsable( true )
	gift5D.setUsable( true )
	gift5E.setUsable( true )
}


//////////////////////
///  Define Times  ///
//////////////////////

// Test times, just for QA
/*
stages = [
[time:"12/16/09 5:30 pm", stage:1, func:stage1], 
[time:"12/16/09 6:00 pm", stage:2, func:stage2], 
[time:"12/16/09 6:30 pm", stage:3, func:stage3], 
[time:"12/16/09 7:00 pm", stage:4, func:stage4], 
[time:"12/16/09 7:30 pm", stage:3, func:stage3], 
[time:"12/16/09 8:00 pm", stage:2, func:stage2], 
[time:"12/16/09 8:30 pm", stage:1, func:stage1],
[time:"12/16/09 9:00 pm", stage:0, func:stage0]
]
*/
/*
/// Times for tree transitions
stages = [
[time:"12/18/09 7:15 pm", stage:1, func:stage1], 
[time:"12/20/09 2:25 pm", stage:2, func:stage2], 
[time:"12/22/09 6:45 pm", stage:3, func:stage3], 
[time:"12/24/09 3:00 am", stage:4, func:stage4], 
[time:"1/1/10 3:00 am", stage:3, func:stage3], 
[time:"1/1/10 2:25 pm", stage:2, func:stage2], 
[time:"1/2/10 4:45 pm", stage:1, func:stage1],
[time:"1/4/10 8:15 pm", stage:0, func:stage0]
]



//=========================================================
// THE CHRISTMAS TREE SWITCHES                             
//=========================================================


//Christmas Tree
xmasTree = makeSwitch( "xmasTree", 5, myRooms.BARTON_103, 0, 0 ) 
xmasTree.setUsable( false )

//Lights
lights1 = makeSwitch( "cl1", myRooms.BARTON_103, 0, 0 )
lights1.setUsable( false )
lights1.off()

lights2 = makeSwitch( "cl2", myRooms.BARTON_103, 0, 0 )
lights2.setUsable( false )
lights2.off()

lights3 = makeSwitch( "cl3", myRooms.BARTON_103, 0, 0 )
lights3.setUsable( false )
lights3.off()

lights4 = makeSwitch( "cl4", myRooms.BARTON_103, 0, 0 )
lights4.setUsable( false )
lights4.off()

lights5 = makeSwitch( "cl5", myRooms.BARTON_103, 0, 0 )
lights5.setUsable( false )
lights5.off()

tooFastMessage = "Don't shred all your presents at once. Wait 10 seconds or so between each one."

//Gifts, Stage 1
gift1A = makeSwitch( "gift1A", myRooms.BARTON_103, 630, 665 )
gift1A.setUsable( false )
gift1A.setRange( 200 )
gift1A.off()

def open1A = { event ->
	gift1A.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift1A"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift1A.whenOn( open1A )

//Gifts, Stage 2
gift2A = makeSwitch( "gift2A", myRooms.BARTON_103, 560, 770 )
gift2A.setUsable( false )
gift2A.setRange( 200 )
gift2A.off()

def open2A = { event ->
	gift2A.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift2A"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift2A.whenOn( open2A )

gift2B = makeSwitch( "gift2B", myRooms.BARTON_103, 695, 765 )
gift2B.setUsable( false )
gift2B.setRange( 200 )
gift2B.off()

def open2B = { event ->
	gift2B.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift2B"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift2B.whenOn( open2B )


//Gifts, Stage 3
gift3A = makeSwitch( "gift3A", myRooms.BARTON_103, 400, 720 )
gift3A.setUsable( false )
gift3A.setRange( 200 )
gift3A.off()

def open3A = { event ->
	gift3A.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift3A"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift3A.whenOn( open3A )

gift3B = makeSwitch( "gift3B", myRooms.BARTON_103, 560, 770 )
gift3B.setUsable( false )
gift3B.setRange( 200 )
gift3B.off()

def open3B = { event ->
	gift3B.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift3B"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift3B.whenOn( open3B )

gift3C = makeSwitch( "gift3C", myRooms.BARTON_103, 695, 765 )
gift3C.setUsable( false )
gift3C.setRange( 200 )
gift3C.off()

def open3C = { event ->
	gift3C.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift3C"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift3C.whenOn( open3C )

//Gifts, Stage 4
gift4A = makeSwitch( "gift4A", myRooms.BARTON_103, 375, 860 )
gift4A.setUsable( false )
gift4A.setRange( 200 )
gift4A.off()

def open4A = { event ->
	gift4A.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift4A"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift4A.whenOn( open4A )

gift4B = makeSwitch( "gift4B", myRooms.BARTON_103, 590, 825 )
gift4B.setUsable( false )
gift4B.setRange( 200 )
gift4B.off()

def open4B = { event ->
	gift4B.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift4B"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift4B.whenOn( open4B )

gift4C = makeSwitch( "gift4C", myRooms.BARTON_103, 855, 820 )
gift4C.setUsable( false )
gift4C.setRange( 200 )
gift4C.off()

def open4C = { event ->
	gift4C.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift4C"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift4C.whenOn( open4C )

gift4D = makeSwitch( "gift4D", myRooms.BARTON_103, 960, 720 )
gift4D.setUsable( false )
gift4D.setRange( 200 )
gift4D.off()

def open4D = { event ->
	gift4D.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift4D"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift4D.whenOn( open4D )

//Gifts, Stage 5
gift5A = makeSwitch( "gift5A", myRooms.BARTON_103, 375, 865 )
gift5A.setUsable( false )
gift5A.setRange( 200 )
gift5A.off()

def open5A = { event ->
	gift5A.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift5A"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift5A.whenOn( open5A )

gift5B = makeSwitch( "gift5B", myRooms.BARTON_103, 590, 820 )
gift5B.setUsable( false )
gift5B.setRange( 200 )
gift5B.off()

def open5B = { event ->
	gift5B.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift5B"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift5B.whenOn( open5B )

gift5C = makeSwitch( "gift5C", myRooms.BARTON_103, 730, 850 )
gift5C.setUsable( false )
gift5C.setRange( 200 )
gift5C.off()

def open5C = { event ->
	gift5C.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift5C"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift5C.whenOn( open5C )

gift5D = makeSwitch( "gift5D", myRooms.BARTON_103, 850, 820 )
gift5D.setUsable( false )
gift5D.setRange( 200 )
gift5D.off()

def open5D = { event ->
	gift5D.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift5D"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift5D.whenOn( open5D )

gift5E = makeSwitch( "gift5E", myRooms.BARTON_103, 960, 720 )
gift5E.setUsable( false )
gift5E.setRange( 200 )
gift5E.off()

def open5E = { event ->
	gift5E.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) ) {
		//set a preventative flag that expires in 10 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) 
		myManager.schedule(10) { event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" ) }
		//Now do the box logic
		boxFlag = "Z01TimeOpenedGift5E"
		openGift( event )
	} else {
		event.actor.centerPrint( tooFastMessage )
	}
}

gift5E.whenOn( open5E )

//======================================
// XMAS GIFTBOX SPAWNERS                
//======================================

//Christmas Giftbox Spawners
spawn1A = myRooms.BARTON_103.spawnSpawner( "spawn1A", "xmas_giftbox", 5 )
spawn1A.setPos( 630, 665 )
spawn1A.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn1A.setMonsterLevelForChildren( 1.0 )

spawn1A.stopSpawning()

spawn2A = myRooms.BARTON_103.spawnSpawner( "spawn2A", "xmas_giftbox", 5 )
spawn2A.setPos( 560, 770 )
spawn2A.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn2A.setMonsterLevelForChildren( 1.0 )

spawn2A.stopSpawning()

spawn2B = myRooms.BARTON_103.spawnSpawner( "spawn2B", "xmas_giftbox", 5 )
spawn2B.setPos( 695, 765 )
spawn2B.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn2B.setMonsterLevelForChildren( 1.0 )

spawn2B.stopSpawning()

spawn3A = myRooms.BARTON_103.spawnSpawner( "spawn3A", "xmas_giftbox", 5 )
spawn3A.setPos( 400, 720 )
spawn3A.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn3A.setMonsterLevelForChildren( 1.0 )

spawn3A.stopSpawning()

spawn3B = myRooms.BARTON_103.spawnSpawner( "spawn3B", "xmas_giftbox", 5 )
spawn3B.setPos( 560, 770 )
spawn3B.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn3B.setMonsterLevelForChildren( 1.0 )

spawn3B.stopSpawning()

spawn3C = myRooms.BARTON_103.spawnSpawner( "spawn3C", "xmas_giftbox", 5 )
spawn3C.setPos( 695, 765 )
spawn3C.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn3C.setMonsterLevelForChildren( 1.0 )

spawn3C.stopSpawning()

spawn4A = myRooms.BARTON_103.spawnSpawner( "spawn4A", "xmas_giftbox", 5 )
spawn4A.setPos( 375, 860 )
spawn4A.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn4A.setMonsterLevelForChildren( 1.0 )

spawn4A.stopSpawning()

spawn4B = myRooms.BARTON_103.spawnSpawner( "spawn4B", "xmas_giftbox", 5 )
spawn4B.setPos( 590, 825 )
spawn4B.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn4B.setMonsterLevelForChildren( 1.0 )

spawn4B.stopSpawning()

spawn4C = myRooms.BARTON_103.spawnSpawner( "spawn4C", "xmas_giftbox", 5 )
spawn4C.setPos( 855, 820 )
spawn4C.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn4C.setMonsterLevelForChildren( 1.0 )

spawn4C.stopSpawning()

spawn4D = myRooms.BARTON_103.spawnSpawner( "spawn4D", "xmas_giftbox", 5 )
spawn4D.setPos( 960, 720 )
spawn4D.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn4D.setMonsterLevelForChildren( 1.0 )

spawn4D.stopSpawning()

spawn5A = myRooms.BARTON_103.spawnSpawner( "spawn5A", "xmas_giftbox", 5 )
spawn5A.setPos( 375, 865 )
spawn5A.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn5A.setMonsterLevelForChildren( 1.0 )

spawn5A.stopSpawning()

spawn5B = myRooms.BARTON_103.spawnSpawner( "spawn5B", "xmas_giftbox", 5 )
spawn5B.setPos( 590, 820 )
spawn5B.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn5B.setMonsterLevelForChildren( 1.0 )

spawn5B.stopSpawning()

spawn5C = myRooms.BARTON_103.spawnSpawner( "spawn5C", "xmas_giftbox", 5 )
spawn5C.setPos( 730, 850 )
spawn5C.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn5C.setMonsterLevelForChildren( 1.0 )

spawn5C.stopSpawning()

spawn5D = myRooms.BARTON_103.spawnSpawner( "spawn5D", "xmas_giftbox", 5 )
spawn5D.setPos( 850, 820 )
spawn5D.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn5D.setMonsterLevelForChildren( 1.0 )

spawn5D.stopSpawning()

spawn5E = myRooms.BARTON_103.spawnSpawner( "spawn5E", "xmas_giftbox", 5 )
spawn5E.setPos( 960, 720 )
spawn5E.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
spawn5E.setMonsterLevelForChildren( 1.0 )

spawn5E.stopSpawning()

//=========================================
// CHRISTMAS GIFT LOGIC                    
//=========================================

//Logic:
//	- Gifts are openable once per Gaian day
//	- Some gifts spawn giftboxes
//	- Some gifts give rewards


//determine how the gift responds to the player
def openGift( event ) { 
	//figure out what time it is right now
	def hourCounter = System.currentTimeMillis()
	
	//debug statements
	//println "**** boxFlag = ${boxFlag} ****"
	def msecTillOpen = event.actor.getPlayerVar( boxFlag ).longValue() - hourCounter


	
	//DEBUG !!!!!!!!!!!!!!!!!!!!!!
	//if( true ) { 
	//check to see if the player has already opened the box today or not. If so, then tell him to wait, otherwise, create a gift
	if( msecTillOpen <= 0 ) {
		event.actor.setPlayerVar( boxFlag, hourCounter + 8400000 ) //once per Gaian day = 140 real minutes = 8400000 ms
		
		//Keep track of giftboxes opened for Badge count
		event.actor.addToPlayerVar( "Z01GiftBoxesOpened", 1 )
		checkForBadgeUpdates( event )
		
		//Now determine whether the package is naughty or nice
		roll = random( 100 )
		if( roll <= 5 ) { 
			// WELCOME TO SWARFMAS BITCHES
			event.actor.centerPrint("WATCH OUT!") 
			def x=event.actor.getX().intValue()
			def y=event.actor.getY().intValue()
			myRooms.BARTON_103.spawnMonster("alien_death_bug", x, y).smite()

			def aoeList = myRooms.BARTON_103.getActorsInWedgeRange(event.actor, 360, 100)
			aoeList.keySet().each {
				if( isPlayer(it)) {
					it.grantCoins( random( 400, 500 ).intValue() ) 
					it.instantPercentDamage( random( 2, (20 + 5 * it.getConLevel()).intValue() )  )
				}
			}
		} else if( roll <= 40 ) { //This value is NAUGHTY
			event.actor.centerPrint( "The package is NAUGHTY!" )
			//save the name of the actor that opened the box
			ownerID = event.actor.getID()
			owner = event.actor
			myManager.schedule(1) { animateThatGiftbox() }
		} else {
			event.actor.centerPrint( "The package is NICE!" )
			myManager.schedule(1) { getAPresent( event ) }
		}
	} else {
		event.actor.centerPrint( "You can only open each present once per Gaian day." )
		//println "**** lastTimeOpen ${lastTimeOpened} ****"
		//println "**** hourCounter ${hourCounter} ****"
		//println "**** diff ${lastTimeOpened - hourCounter} ****"
		//println "**** ${((lastTimeOpened - hourCounter)/60000).intValue()} ****"
		myManager.schedule(3) { event.actor.centerPrint( "You have ${ ( msecTillOpen/60000 ).intValue() } real minutes before you can open this gift again." ) }
	}
}


def checkForBadgeUpdates( event ) {
	if( event.actor.getPlayerVar( "Z01GiftBoxesOpened" ) >= 100 && !event.actor.isDoneQuest(288) ) {
		event.actor.updateQuest( 288, "Story-VQS" ) //Update the "SANTA'S LITTLE HELPER" badge
	}
}

//=========================================
// NAUGHTY!!!                              
//=========================================

naughtyMessage = "That toy's not yours! Off to the corner with you!"

animationSayings = [ "Quick! Turn that present into the past!", "Look out! The present Animated!", "That box has bite! Watch it!", "That gift is WAY too snappy!", "When giftboxes attack!", "Don't look a giftbox in the mouth!", "Its bite is worse than it's bow!" ]

def synchronized animateThatGiftbox() {
	//alert the player via centerPrint to the fact that the box is spawning
	owner.centerPrint( random( animationSayings ) )
	//determine which spawner to check
	if( boxFlag == "Z01TimeOpenedGift1A" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn1A.spawnsInUse() <= 4 ) {
			curSpawn = spawn1A.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift2A" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn2A.spawnsInUse() <= 4 ) {
			curSpawn = spawn2A.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift2B" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn2B.spawnsInUse() <= 4 ) {
			curSpawn = spawn2B.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift3A" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn3A.spawnsInUse() <= 4 ) {
			curSpawn = spawn3A.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift3B" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn3B.spawnsInUse() <= 4 ) {
			curSpawn = spawn3B.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift3C" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn3C.spawnsInUse() <= 4 ) {
			curSpawn = spawn3C.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift4A" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn4A.spawnsInUse() <= 4 ) {
			curSpawn = spawn4A.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift4B" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn4B.spawnsInUse() <= 4 ) {
			curSpawn = spawn4B.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift4C" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn4C.spawnsInUse() <= 4 ) {
			curSpawn = spawn4C.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift4D" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn4D.spawnsInUse() <= 4 ) {
			curSpawn = spawn4D.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift5A" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn5A.spawnsInUse() <= 4 ) {
			curSpawn = spawn5A.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift5B" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn5B.spawnsInUse() <= 4 ) {
			curSpawn = spawn5B.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift5C" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn5C.spawnsInUse() <= 4 ) {
			curSpawn = spawn5C.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift5D" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn5D.spawnsInUse() <= 4 ) {
			curSpawn = spawn5D.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
	if( boxFlag == "Z01TimeOpenedGift5E" ) { 
		//if <= 4 spawnsInUse, then spawn another one
		if( spawn5E.spawnsInUse() <= 4 ) {
			curSpawn = spawn5E.forceSpawnNow()
			finishSpawn()
		//if too many spawned already, then just give coal
		} else {
			justCoal()
		}
	}
}

def finishSpawn() {
	//println "****owner = ${owner} at start of finishSpawn() ****"
	curSpawn.addHate( owner, 50 )
	runOnDeath( curSpawn, { event ->
		//println "**** killer ID = ${event.killer.getID()} and ownerID = ${ownerID} ****"
		//if( event.killer.getID() != ownerID ) {
		//	event.killer.centerPrint( naughtyMessage )
		//	roll = random( 2 )
		//	if( roll == 1 ) {
		//		event.killer.warp( "BARTON_301", 800, 500 )
		//	} else {
		//		event.killer.warp( "BARTON_4", 1400, 600 )
		//	}
		//}
		checkForRewards()
	} )
}
	
def justCoal() {
	owner.centerPrint( "There's nothing here but coal." )
	owner.grantItem( "100428" )
}
	

/*
//loot drop items for Xmas Giftboxes
100428 Coal-ossal Fail
100429 Festive Ribbon
100430 Jingle Bell
100431 Peppermint

//recipe drops from Xmas Giftboxes
31503 - Coal Sprite
31717 - Ribbon thing (name subject to change)
31555 - Reindurr
31699 - Peppermint Squid

//------------------------------------------------------------
//RECIPES                                                     
//------------------------------------------------------------

Coal Spirit - Common Recipe
Loot - Coalossul Fail x 30

Hair Ribbon - Uncommon Recipe
Loot - Festive Ribbon x 1, Copper x 3, Garlic Peel X 2, Silk x1

Reindeer Pack - Rare Recipe
Loot - Jingle Bell x1, Cotton x 4, Leather x2, Ribbon x2, Stitches x1

Peppermint Squid - Super Rare Recipe
Loot - Peppermint x1, Ribbon x2, Foam Sponge x 6, Stitches x3, Wetware Plastic x2, Sailor Ink x 1, Synthetic Fiber x1 Alien Tentacle x1
*/
/*
giftboxCommonList = [ "100428", "100429" ]
giftboxUncommonList = [ "100430", "100431" ]
giftboxRecipeList = [ "18034", "18036", "18038", "18040" ]

//NOTE: We grant rewards this way so we can ensure that they only go to the player that spawned the monster...not anyone else.
// This discourages camping and kill stealing since it has no reward.

def checkForRewards() {
	//check for common
	roll = random( 100 )
	//println "**** common roll = ${roll} ****"
	if( roll <= 40 ) {
		item = random( giftboxCommonList )
		owner.grantItem( item ) 
		if( item == "100428" ) {
			owner.centerPrint( "You get a Coal-ossul Fail!" )
		} else if( item == "100429" ) {
			owner.centerPrint( "You get a Festive Ribbon!" )
		}
	}
		
	//check for uncommon
	roll = random( 100 )
	//println "**** uncommon roll = ${roll} ****"
	if( roll <= 20 ) {
		item = random( giftboxUncommonList )
		owner.grantItem( item )
		if( item == "100430" ) {
			owner.centerPrint( "You get a Jingle Bell!" )
		} else if( item == "100431" ) {
			owner.centerPrint( "You get a Peppermint!" )
		}
	}
	
	//check for recipe
	roll = random( 100 )
	//println "**** recipe roll = ${roll} ****"
	if( roll <= 10 ) {
		item = random( giftboxRecipeList )
		owner.grantItem( item )
		if( item == "18034" ) {
			owner.centerPrint( "You receive a Coal Sprite Recipe!" )
		} else if( item == "18036" ) {
			owner.centerPrint( "You receive a Ribbon Recipe!" )
		} else if( item == "18038" ) {
			owner.centerPrint( "You receive a Reindurr Recipe!" )
		} else if( item == "18040" ) {
			owner.centerPrint( "You receive a Peppermint Squid Recipe!" )
		}
	}
}
	
	
//=========================================
// NICE !!!!                               
//=========================================

commonList = [ "100272", "100289", "100385", "100297", "100289", "100397", "100291", "100262", "100263", "100298", "100388", "100278", "100275", "100283", "100281", "100367", "100411", "100394", "100284", "100405", "100267", "100265", "100285", "100398", "100397", "100376", "100296" ]

uncommonList = [ "100280", "100279", "100270", "100380", "100279", "100384", "100378", "100261", "100271", "100276", "100258", "100268", "100381", "100382", "100282", "100383", "100390", "100299", "100286", "100369", "100400", "100387" ]

recipeList = [ "17766", "17764", "17772", "17758", "17756", "17861", "17857", "17755", "17848", "17849", "17852", "17851", "17850", "17753", "17833", "17831", "17754", "17836", "17835", "17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779" ]

ringList = [ "17716", "17722", "17723", "17737", "17738", "17741", "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866" ]

def getAPresent( event ) {
	roll = random( 100 )

	lootMultiplier = event.actor.getConLevel() / 10 //the lower the overall CL of a player in relation to player max CL (currently 10), the lower the reward so the trick or treating can be meaningful to all CLs
	//grant gold
	if( roll <= 45 ) {
		def goldGrant = ( random( 100, 200 ) * lootMultiplier ).intValue()
		event.actor.grantCoins( goldGrant ) 
		event.actor.centerPrint( "This box has gold in it! No scrooge here!" )
	//grant common item
	} else if( roll > 45 && roll <= 65 ) {
		event.actor.grantItem( random( commonList ) )
		event.actor.centerPrint( "Cool! I can use this as part of a recipe!" )
	//grant uncommon item
	} else if( roll > 65 && roll <= 75 ) {
		event.actor.grantItem( random( uncommonList ) )
		event.actor.centerPrint( "Jinglin'! This component is pretty rare!" )
	//grant recipes
	} else if( roll > 75 && roll <= 90 ) {
		event.actor.grantItem( random( recipeList ) )
		event.actor.centerPrint( "An elf left a recipe in this box!" )
	//grant orbs
	} else if( roll > 90 && roll <= 93 ) {
		orbGrant = ( 10 * lootMultiplier ).intValue()
		event.actor.grantQuantityItem( 100257, orbGrant )
		event.actor.centerPrint( "Holy Christmas! Charge Orbs to power up my rings!!" )
	//power-ups
	} else if( roll <= 94 ) {
		roll = random( 2 )
		if( roll == 1 ) {
			//grant Supercharger
			event.actor.grantItem( "18104" )
		} else {
			//grant Ring Polisher
			event.actor.grantItem( "18102" )
		}
		event.actor.centerPrint( "A PowerUp Pack! This is awesome! Just what I wanted!" )
	// more money, instead of power-ups
	} else if( roll <= 97 ) {
		def goldGrant = ( random( 200, 400 ) * lootMultiplier ).intValue()
		event.actor.grantCoins( goldGrant ) 
		event.actor.centerPrint( "This box has extra gold in it! Score!" )
	//rings
	} else {
		event.actor.grantRing( random( ringList ), true )
		event.actor.centerPrint( "Woohoo! A Ring! It *is* a wonderful life!" )
	}
}


//================================================
// AQUATIC PETE (sharing the onEnter/onExit stuff 
//================================================

def AquaticPete = spawnNPC("Aquatic Pete", myRooms.BARTON_103, 1350, 530)
AquaticPete.setRotation( 135 )
AquaticPete.setDisplayName( "Aquatic Pete" )


//======================================
// RANDOM CONVERSATIONS                 
//======================================

def Random1 = AquaticPete.createConversation("Random1", true, "Z0AquaticPeteR1" )
Random1.setUrgent( false )

def rand1 = [id:1]
rand1.npctext = "Man, I was gonna go to the beach, but now there are monsters everywhere! I guess I'll swim in the moat instead."
rand1.flag = "!Z0AquaticPeteR1"
rand1.exec = { event ->
	selectRandomFlag( event )
}
rand1.result = DONE
Random1.addDialog(rand1, AquaticPete)
//--------------------
def Random2 = AquaticPete.createConversation("Random2", true, "Z0AquaticPeteR2" )
Random2.setUrgent( false )

def rand2 = [id:1]
rand2.npctext = "Gold Beach is my favorite hangout. Where's that? It's up by the new Bucanneer Boardwalk, of course!"
rand2.flag = "!Z0AquaticPeteR2"
rand2.exec = { event ->
	selectRandomFlag( event )
}
rand2.result = DONE
Random2.addDialog(rand2, AquaticPete)
//--------------------
def Random3 = AquaticPete.createConversation("Random3", true, "Z0AquaticPeteR3" )
Random3.setUrgent( false )

def rand3 = [id:1]
rand3.npctext = "I know, I know, I look silly in my diving gear. But I swear that I laid my clothes around here SOMEplace!"
rand3.flag = "!Z0AquaticPeteR3"
rand3.exec = { event ->
	selectRandomFlag( event )
}
rand3.result = DONE
Random3.addDialog(rand3, AquaticPete)
//--------------------
def Random4 = AquaticPete.createConversation("Random4", true, "Z0AquaticPeteR4" )
Random4.setUrgent( false )

def rand4 = [id:1]
rand4.npctext = "So if I promise to scrape the barnacles off their ship, do you think the Pirates of the Shallow Sea would take me onto their crew?"
rand4.flag = "!Z0AquaticPeteR4"
rand4.exec = { event ->
	selectRandomFlag( event )
}
rand4.result = DONE
Random4.addDialog(rand4, AquaticPete)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

randomFlag = []
randomFlag << "Z0AquaticPeteR1"
randomFlag << "Z0AquaticPeteR2"
randomFlag << "Z0AquaticPeteR3"
randomFlag << "Z0AquaticPeteR4"


def selectRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( randomFlag )
	if( lastFlag == convoFlag ) {
		selectRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}


//============================================
// onEnter / onExit LOGIC                     
//============================================

xmasStage = 0 //DEBUG VARIABLE

myManager.onEnter( myRooms.BARTON_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		//in case the player quit out during the 10-second timeout after opening a giftbox, this will unset the flag so it works again
		event.actor.unsetQuestFlag( GLOBAL, "Z01JustOpenedAGiftBox" )
		
//		//DEBUG TESTING SCRIPT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//		//gift1A.setUsable( true )
//		xmasStage ++
//		if( xmasStage > 4 ) {
//			xmasStage = 0
//		}
//		event.actor.setPlayerVar( "Z01TimeOpenedGift1A", 0 )
//		println "**** current xmasStage = ${xmasStage} ****"
//		xmasTree.setState( xmasStage )
//		if( xmasStage == 0 ) {
//			stage0()
//		} else if( xmasStage == 1 ) {
//			stage1()
//		} else if( xmasStage == 2 ) {
//			stage2()
//		} else if( xmasStage == 3 ) {
//			stage3()
//		} else if( xmasStage == 4 ) {
//			stage4()
//		}
//		println "**** next xmasStage = ${xmasStage} ****"
//		//DEBUG TESTING SCRIPT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

		//the following is for Aquatic Pete's random conversation
		convoFlag = random( randomFlag )
		event.actor.setQuestFlag( GLOBAL, convoFlag )
	}
}

myManager.onExit( myRooms.BARTON_103 ) { event ->
	if( isPlayer( event.actor ) ) {
		//the following is for Aquatic Pete's random conversation
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
	}
}

//=========================================
// TIMES FOR TREE STATE CHANGES            
//=========================================

//initialize the tree state
xmasTree.setState( 0 )
xmasStage = 0
stage0()

checkDateAndSetTree()


/*
//==========================================
// QA TESTING SCRIPT                        
// !!!Comment this out before going live!!! 
// Rotates the tree through every stage,    
// with one stage per 10 minutes            
//==========================================
myManager.schedule(600) { constantChange() }

def constantChange() {
	xmasStage ++
	if( xmasStage > 4 ) {
		xmasStage = 0
	}
	println "**** moving to stage ${xmasStage} ****"
	xmasTree.setState( xmasStage )
	if( xmasStage == 0 ) {
		stage0()
	} else if( xmasStage == 1 ) {
		stage1()
	} else if( xmasStage == 2 ) {
		stage2()
	} else if( xmasStage == 3 ) {
		stage3()
	} else if( xmasStage == 4 ) {
		stage4()
	}
	myManager.schedule(600) { constantChange() }
}	
//DEBUG ONLY !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*/	

/*
//Server initialization script to figure out what day it is and to set the tree appropriately
def checkDateAndSetTree() {

	log("checkDateAndSetTree()")

	stages.each {
		if( isLaterThan(it.time) ) {
			log("later than ${it.time}.  setting to ${it.stage}")
			xmasTree.setState( it.stage )
			it.func()
		}
	}

/* Old way
	//bracket each date and set the same stuff that the "scheduleAt" stuff, below, would do normally
	if( isLaterThan( "12/18/09 7:15 pm" ) ) {
		xmasTree.setState( 1 ); xmasStage = 1; stage1()
	}
	if( isLaterThan( "12/20/09 2:25 pm" ) ) {
		xmasTree.setState( 2 ); xmasStage = 2; stage2()
	}
	if( isLaterThan( "12/22/09 6:45 pm" ) ) {
		xmasTree.setState( 3 ); xmasStage = 3; stage3()
	}
	if( isLaterThan( "12/24/09 3:00 am" ) ) {
		xmasTree.setState( 4 ); xmasStage = 4; stage4()
	}
	if( isLaterThan( "1/1/10 3:00 am" ) ) {
		xmasTree.setState( 3 ); xmasStage = 3; stage3()
	}
	if( isLaterThan( "1/1/10 2:25 pm" ) ) {
		xmasTree.setState( 2 ); xmasStage = 2; stage2()
	}
	if( isLaterThan( "1/2/10 4:45 pm" ) ) {
		xmasTree.setState( 1 ); xmasStage = 1; stage1()
	}
	if( isLaterThan( "1/4/10 8:15 pm" ) ) {
		xmasTree.setState( 0 ); xmasStage = 0; stage0()
	}
	*/
/*
}

stages.each {
	def stageMap = it
	myManager.scheduleAt(it.time) { 
		log("running scheduled task"); xmasTree.setState(stageMap.stage); xmasStage=stageMap.stage; stageMap.func() 
	}
}

/*
myManager.scheduleAt("12/15/09 1:30 pm") { xmasTree.setState( 1 ); xmasStage = 1; stage1() }
myManager.scheduleAt("12/15/09 2:30 pm") { xmasTree.setState( 2 ); xmasStage = 2; stage2() }
myManager.scheduleAt("12/15/09 3:30 pm") { xmasTree.setState( 3 ); xmasStage = 3; stage3() }
myManager.scheduleAt("12/15/09 4:30 am") { xmasTree.setState( 4 ); xmasStage = 4; stage4() }
myManager.scheduleAt("12/15/09 5:30 pm") { xmasTree.setState( 3 ); xmasStage = 3; stage3() }
myManager.scheduleAt("12/15/09 6:30 pm") { xmasTree.setState( 2 ); xmasStage = 2; stage2() }
myManager.scheduleAt("12/16/09 10:30 am") { xmasTree.setState( 1 ); xmasStage = 1; stage1() }
myManager.scheduleAt("12/16/09 11:30 am") { xmasTree.setState( 0 ); xmasStage = 0; stage0() }
*/

/* Commenting out real times, for now
//The tree grows until full the day before Christmas, and then fades away after New Years Eve.
myManager.scheduleAt("12/18/09 7:15 pm") { xmasTree.setState( 1 ); xmasStage = 1; stage1() }
myManager.scheduleAt("12/20/09 2:25 pm") { xmasTree.setState( 2 ); xmasStage = 2; stage2() }
myManager.scheduleAt("12/22/09 6:45 pm") { xmasTree.setState( 3 ); xmasStage = 3; stage3() }
myManager.scheduleAt("12/24/09 3:00 am") { xmasTree.setState( 4 ); xmasStage = 4; stage4() }
myManager.scheduleAt("1/1/10 3:00 am") { xmasTree.setState( 3 ); xmasStage = 3; stage3() }
myManager.scheduleAt("1/1/10 2:25 pm") { xmasTree.setState( 2 ); xmasStage = 2; stage2() }
myManager.scheduleAt("1/2/10 4:45 pm") { xmasTree.setState( 1 ); xmasStage = 1; stage1() }
myManager.scheduleAt("1/4/10 8:15 pm") { xmasTree.setState( 0 ); xmasStage = 0; stage0() }
*/

//=======================================
// LIGHTING THE LIGHTS (at night)        
//=======================================
/*
checkForNight() //start it up

def checkForNight() {
	//if it's nighttime then...
	if( ( gst() > 1800 && gst() <=2359 ) || ( gst() >= 0 && gst() < 600 ) ) { 
		////println "**** It's Nighttime! Xmas Lights ON! ****"
		if( xmasStage == 0 ) {
			//println "**** turning on lights1 ****"
			lights1.on()
			lights2.off()
			lights3.off()
			lights4.off()
			lights5.off()
		} else if( xmasStage == 1 ) {
			//println "**** turning on lights2 ****"
			lights1.off()
			lights2.on()
			lights3.off()
			lights4.off()
			lights5.off()
		} else if( xmasStage == 2 ) {
			//println "**** turning on lights3 ****"
			lights1.off()
			lights2.off()
			lights3.on()
			lights4.off()
			lights5.off()
		} else if( xmasStage == 3 ) {
			//println "**** turning on lights4 ****"
			lights1.off()
			lights2.off()
			lights3.off()
			lights4.on()
			lights5.off()
		} else if( xmasStage == 4 ) {
			//println "**** turning on lights5 ****"
			lights1.off()
			lights2.off()
			lights3.off()
			lights4.off()
			lights5.on()
		}
	//if it's daytime, then turn the lights off
	} else {
		//println "**** It's Daytime...Douse those Xmas Lights! ****"
		lights1.off()
		lights2.off()
		lights3.off()
		lights4.off()
		lights5.off()
	}
	myManager.schedule(5) { checkForNight() }
}


/*
//=====================================================
// ORPHAN SCRIPTS (Stopped in-progress)                
//=====================================================

def Jackie = spawnNPC("Jackie", myRooms.BARTON_103, 1350, 530)
Jackie.setRotation( 135 )
Jackie.setDisplayName( "Jackie" )

def Sunako = spawnNPC("Sunako", myRooms.BARTON_103, 1350, 530)
Sunako.setRotation( 135 )
Sunako.setDisplayName( "Sunako" )

def MothBite = spawnNPC("MothBite", myRooms.BARTON_103, 1350, 530)
MothBite.setRotation( 135 )
MothBite.setDisplayName( "Moth Bite" )

def Scuppers = spawnNPC("Scuppers", myRooms.BARTON_103, 1350, 530)
Scuppers.setRotation( 135 )
Scuppers.setDisplayName( "Scuppers" )

def Jay = spawnNPC("Jay", myRooms.BARTON_103, 1350, 530)
Jay.setRotation( 135 )
Jay.setDisplayName( "Jay" )

def Angela = spawnNPC("Angela", myRooms.BARTON_103, 1350, 530)
Angela.setRotation( 135 )
Angela.setDisplayName( "Angela" )

def EustaceNEthel = spawnNPC("EustaceNEthel", myRooms.BARTON_103, 1350, 530)
EustaceNEthel.setRotation( 135 )
EustaceNEthel.setDisplayName( "Eustace and Ethel" )


jackieTalk = Jackie.createConversation( "jackieTalk", true )
jackieTalk.setUrgent( true )

def jackie1 = [id:1]
jackie1.npctext = "'ello there, gubner. Spare some gold for the Christmas Tree?"
jackie1.options = []
jackie1.options << [text: "I'm feeling magnanimous. I'll add 5,000 gold to the cause!", result: 2]
jackie1.options << [text: "I want to see that tree get huge! Here's 1,000 gold to help.", result: 3]
jackie1.options << [text: "You bet! A bit of Christmas cheer for everyone! Here's 100 gold!", result: 4]
jackie1.options << [text: "Sure. Whatever. Happy Holidays. Here's 10 gold.", result: 5]
jackie1.options << [text: "Bah, humbug! Go away kid. Y'bother me.", result: 6]
jackieTalk.addDialog( jackie1, Jackie )

def jackie2 = [id:2]
jackie2.npctext = "Cor, blimey! Thankee SO much, generous soul!"
jackie2.exec = { event ->
	runOnDeduct( event.actor, "gold", 5000, takeGoldSuccess, takeGoldFailure )
	Jackie.say( "%p is a generous soul with great holiday spirit!" )
}
jackie2.result = DONE
jackieTalk.addDialog( jackie2, Jackie )

def jackie3 = [id:3]
jackie3.npctext = "The elves will be quite pleased. Thankee!"
jackie3.result = DONE
jackieTalk.addDialog( jackie3, Jackie )

def jackie4 = [id:4]
jackie4.npctext = "Every donation is a kindness. Thankee!"
jackie4.result = DONE
jackieTalk.addDilaog( jackie4, Jackie )

def jackie5 = [id:5]
jackie5.npctext = "Everyone does the best that they can."
jackie5.result = DONE
jackieTalk.addDialog( jackie5, Jackie )

def jackie6 = [id:6]
jackie6.npctext = "And a Merry Christmas to you, too!"
jackie6.result = DONE
jackieTalk.addDialog( jackie6, Jackie )*/
