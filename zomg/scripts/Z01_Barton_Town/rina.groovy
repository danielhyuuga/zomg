import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

Rina = spawnNPC("Rina-VQS", myRooms.BARTON_303, 710, 360)
Rina.setRotation( 135 )
Rina.setDisplayName( "Rina" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Rina.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/rina_strip.png")
}
///--------------------------------------------------------
// DEFAULT CONVERSATION                                    
//---------------------------------------------------------

def RinaDefault = Rina.createConversation("RinaDefault", true, "!Z1CandiceFriend", "!Z1RinaEnemy", "!QuestStarted_35", "!QuestCompleted_35", "!QuestStarted_45", "!QuestCompleted_45")
RinaDefault.setUrgent( true )

def Default1 = [id:1]
Default1.npctext = "Hi there! Isn't it a GREAT day today?"
Default1.playertext = "Ummm..."
Default1.options = []
Default1.options << [text:"It sure is!", result: 2]
Default1.options << [text:"It's all right. Why?", result: 23]
Default1.options << [text:"Ugh. You are WAY too cheery for me!", result: 33]
RinaDefault.addDialog(Default1, Rina)

def Default2 = [id:2]
Default2.npctext = "That's great to hear! Gosh, I wish everyone was as happy as we are! Wheeeee! What do you want to talk about? My shop? Favorite colors? Best pets to have? What?"
Default2.options = []
Default2.options << [text: "Let's talk about your flower shop.", result: 3]
Default2.options << [text: "Oooh. Colors sound fun.", result: 11]
Default2.options << [text: "Pets! Animals are great!", result: 26]
Default2.options << [text: "Y'know, I just realized I need to be somewhere...else. Bye for now!", result: 33]
RinaDefault.addDialog(Default2, Rina)

def Default3 = [id:3]
Default3.npctext = "Okay! The Barton Flowershoppe has been here a long time, but I'm so lucky! I get to RUN it! All these beautiful flowers to sell and smell all day long! I'm just so excited I could SQUEEEE!"
Default3.playertext = "You could squee?"
Default3.result = 4
RinaDefault.addDialog(Default3, Rina)

def Default4 = [id:4]
Default4.npctext = "No! I could just SQUEEEE! See? SQUEEEE! SQUEEEE!"
Default4.playertext = "Right! Let me try again! SQUEEEEEEeeeee!"
Default4.result = 40
RinaDefault.addDialog(Default4, Rina)

def Default40 = [id:40]
Default40.npctext = "No, no, no. That's not right at all! Try again!"
Default40.result = 5
RinaDefault.addDialog( Default40, Rina )

def Default5 = [id:5]
Default5.playertext = "Okay! Here goes..."
Default5.options = []
Default5.options << [text: "SQUEE!", result: 6]
Default5.options << [text: "SQUEEEE!", result: 8]
Default5.options << [text: "SQUEEEEEEEE!", result: 7]
RinaDefault.addDialog(Default5, Rina)

def Default6 = [id:6]
Default6.npctext = "Nope! Too short! Try again!"
Default6.result = 5
RinaDefault.addDialog(Default6, Rina)

def Default7 = [id:7]
Default7.npctext = "Silly! That's way too long! Try again!"
Default7.result = 5
RinaDefault.addDialog(Default7, Rina)

def Default8 = [id:8]
Default8.npctext = "Yay! That's SO good! You're a natural!"
Default8.playertext = "I'm a natural! Fantastic! Thanks, Rina!"
Default8.result = 9
RinaDefault.addDialog(Default8, Rina)

def Default9 = [id:9]
Default9.npctext = "You're very welcome, %p! How about we talk about something else now?"
Default9.playertext = "Like what?"
Default9.result = 10
RinaDefault.addDialog(Default9, Rina)

def Default10 = [id:10]
Default10.npctext = "Oh! Let's see...how about my shop, our favorite colors, or what the best pets would be?"
Default10.playertext = "Okay then, how about..."
Default10.options = []
Default10.options << [text:"Your flower shop!", result: 3]
Default10.options << [text:"Colors sounds nice!", result: 11]
Default10.options << [text:"Oooh! Pets are cool!", result: 26]
Default10.options << [text:"I think I'm all talked out for now. Thanks, Rina!", result: 33]
RinaDefault.addDialog(Default10, Rina)

def Default11 = [id:11]
Default11.npctext = "My favorite color is yellow! It's so bright and sunny and cheery and golden and warm and fun and, well, it's just WONDERFUL!"
Default11.playertext = "Hmmm, my favorite is definitely..."
Default11.options = []
Default11.options << [text:"Blue", result: 12]
Default11.options << [text:"Green", result: 13]
Default11.options << [text:"Pink", result:16]
Default11.options << [text:"Paisley", result: 18]
RinaDefault.addDialog(Default11, Rina)

def Default12 = [id:12]
Default12.npctext = "Blue? That's such a quiet color. You must be a really interesting, deep person that thinks a lot. I don't think much, so that should make us GREAT friends! You can do all my thinking for me!"
Default12.playertext = "Hmmmm, let me think about that. But meanwhile, what else did you want to talk about?"
Default12.result = 10
RinaDefault.addDialog(Default12, Rina)

def Default13 = [id:13]
Default13.npctext = "Green? Ooooh! I like green! I mean, it's no yellow, but so much wonderful stuff in the world is green. There's grass, and lawn mowers, and cucumbers, and sour apple bubblegum, and the starting flag in a race, and the 'go' light, and frogs, and..."
Default13.playertext = "How about..."
Default13.options = []
Default13.options << [text: "Pears?", result: 14]
Default13.options << [text: "Limes?", result: 14]
Default13.options << [text: "Boogers?", result: 15]
RinaDefault.addDialog(Default13, Rina)

def Default14 = [id:14]
Default14.npctext = "Very nice! That's a good one!"
Default14.playertext = "Thanks! What else can we talk about?"
Default14.result = 10
RinaDefault.addDialog(Default14, Rina)

def Default15 = [id:15]
Default15.npctext = "Oh. That's not very nice at all. I don't think I want to talk to you anymore."
Default15.result = DONE
RinaDefault.addDialog(Default15, Rina)

def Default16 = [id:16]
Default16.npctext = "PINK!! That's my second favorite! Wheeeee! We're so compatible! You're SUCH a pink person! Happy, bubbly and nice! We should be friends!"
Default16.playertext = "Thanks, Rina! I like you too!"
Default16.flag = "Z1RinaFriend"
Default16.result = 17
RinaDefault.addDialog(Default16, Rina)

def Default17 = [id:17]
Default17.npctext = "Oh we will be! Forever and ever! BFFs! Let's see, what else could I talk about with my new BFF?"
Default17.result = 10
RinaDefault.addDialog(Default17, Rina)

def Default18 = [id:18]
Default18.npctext = "..."
Default18.playertext = "What's the matter? You don't like Paisley?"
Default18.result = 19
RinaDefault.addDialog(Default18, Rina)

def Default19 = [id:19]
Default19.npctext = "Isn't he dead?"
Default19.playertext = "???"
Default19.result = 20
RinaDefault.addDialog(Default19, Rina)

def Default20 = [id:20]
Default20.npctext = "Wasn't he the King of Rock and Roll or something?"
Default20.playertext = "Oh! NO! Not ELVIS!!!"
Default20.result = 21
RinaDefault.addDialog(Default20, Rina)

def Default21 = [id:21]
Default21.npctext = "Oh. That's good. I don't like dead things very much. They're sad."
Default21.playertext = "It's okay, Rina. You don't have to be sad. Paisley is just a bunch of colors together in a neat pattern."
Default21.result = 22
RinaDefault.addDialog(Default21, Rina)

def Default22 = [id:22]
Default22.npctext = "That sounds nicer. But let's talk about something else instead. How about..."
Default22.result = 10
RinaDefault.addDialog(Default22, Rina)

def Default23 = [id:23]
Default23.npctext = "All right?!? Really? Look around you! The world is changing! It's beautiful and exciting and wonderful and SQUEEEfully different now!"
Default23.options = []
Default23.options << [text:"Gosh! I see what you mean! I'm excited about it too!", result: 2]
Default23.options << [text:"SQUEEEfully? Is that a word?", result: 24]
Default23.options << [text:"Wow. Just wow. I don't know what to say, so I'm just going to walk away now...", result: 33]
RinaDefault.addDialog(Default23, Rina)

def Default24 = [id:24]
Default24.npctext = "It is in MY world! Would you like to be in my world?"
Default24.options = []
Default24.options << [text: "Your world seems bright and cheery. Let me in!", result: 25]
Default24.options << [text: "Oh...no...I think I'll just stroll along now. Goodbye crazy girl.", result: 33]
RinaDefault.addDialog(Default24, Rina)

def Default25 = [id:25]
Default25.npctext = "Okay! Wheeeee! What should we talk about?"
Default25.playertext = "I don't know! What do YOU want to talk about?"
Default25.result = 10
RinaDefault.addDialog(Default25, Rina)

def Default26 = [id:26]
Default26.npctext = "I like kittens and doggies and bunnies and hamsters and fish and birdies and gwees. Which do you like?"
Default26.options = []
Default26.options << [text:"Kitties!!", result: 27]
Default26.options << [text:"Fishies!", result: 28]
Default26.options << [text:"Birdies!", result: 29]
Default26.options << [text:"Grunnies!", result: 30]
Default26.options << [text:"I like everything!", result: 32]
RinaDefault.addDialog(Default26, Rina)

def Default27 = [id:27]
Default27.npctext = "Ooooo! Those are great! I like them too! They're so warm and cuddly! What else?"
Default27.result = 26
RinaDefault.addDialog(Default27, Rina)

def Default28 = [id:28]
Default28.npctext = "Fishies are fun! They're like birdies in the way they fly through the water like it was air, tumbling and playing all day! What else do you like?"
Default28.result = 26
RinaDefault.addDialog(Default28, Rina)

def Default29 = [id:29]
Default29.npctext = "Birdies are the best! They fly around through the air, like fishies swimming in the water, diving and climbing all day! They're so pretty. What else?"
Default29.result = 26
RinaDefault.addDialog(Default29, Rina)

def Default30 = [id:30]
Default30.npctext = "Grunnies! Yay, Grunnies! They're so...oh, wait...they're not nice at all. Are you trying to trick me?"
Default30.playertext = "Yes I am."
Default30.result = 31
RinaDefault.addDialog(Default30, Rina)

def Default31 = [id:31]
Default31.npctext = "Oh. That's fun then! Hee hee. I like you!"
Default31.playertext = "Okay then, what else can we talk about?"
Default31.result = 10
RinaDefault.addDialog(Default31, Rina)

def Default32 = [id:32]
Default32.npctext = "Me too! Wheee! It's not everyday I find someone as happy as I am. Please come by and say hi again another time, my shiny new FRIEND!"
Default32.playertext = "I will, Rina. Thanks!"
Default32.result = 33
Default32.flag = "Z1RinaFriend"
RinaDefault.addDialog(Default32, Rina)

def Default33 = [id:33]
Default33.npctext = "Okay, %p. By-eee!"
Default33.result = DONE
RinaDefault.addDialog(Default33, Rina)

//---------------------------------------------------------
// I HATE CANDICE! (If the player has accepted Candice     
// as a friend.                                            
//---------------------------------------------------------
//If the player becomes friends with Candice, Rina lays it on the line with the player, forcing them to be a friend with her, Candice, or both of them.
//Choosing which person to be friends with will determine which quests are unlocked by them.

def CandiceHate = Rina.createConversation("CandiceHate", true, "Z1CandiceFriend", "!Z1RinaFriend2", "!Z1BothFriends", "!Z1RinaEnemy")
CandiceHate.setUrgent( true )

def CHate1 = [id:1]
CHate1.npctext = "Hello...%p..."
CHate1.playertext = "Hi there, Rina!"
CHate1.result = 2
CandiceHate.addDialog(CHate1, Rina)

def CHate2 = [id:2]
CHate2.npctext = "...*glare*..."
CHate2.playertext = "What?!? What's wrong? Where's the bubbly Rina that I've always known?"
CHate2.result = 3
CandiceHate.addDialog(CHate2, Rina)

def CHate3 = [id:3]
CHate3.npctext = "Oh, you! How could you?"
CHate3.playertext = "How could I do what?!?"
CHate3.result = 4
CandiceHate.addDialog(CHate3, Rina)

def CHate4 = [id:4]
CHate4.npctext = "You...SPOKE...to that pink-haired vixen across the street! And I've heard that now she's even calling you her FRIEND!"
CHate4.playertext = "Wow. Word gets around fast in this Town. But...so? Why is that a problem?"
CHate4.result = 5
CandiceHate.addDialog(CHate4, Rina)

def CHate5 = [id:5]
CHate5.npctext = "Because she imitates *everything* I do and I'm tired of people confusing us in conversations!"
CHate5.options = []
CHate5.options << [text:"But, how could people get confused between you two?", result: 6]
CHate5.options << [text:"I think maybe you two are getting a little carried away.", result: 9]
CHate5.options << [text:"Hmmm, I'm going to go away and come back later.", result: 20]
CandiceHate.addDialog(CHate5, Rina)

def CHate6 = [id:6]
CHate6.npctext = "I just don't know! I mean, I'm a sunny blonde and she has that cheap pink dye job!"
CHate6.options = []
CHate6.options << [text:"hehehe. Don't worry, Rina. I'd never be friends with that imitator!", result: 7]
CHate6.options << [text:"I don't want to be in your feud, Rina. If I can't be both of your friends, then I'm going to be friends with neither of you so I can stay out of the fight.", result: 8]
CHate6.options << [text:"The only thing cheap was that comment. I think Candice is cuter and nicer and I'm going to stay friends with her!", result: 19]
CandiceHate.addDialog(CHate6, Rina)

//This response starts the RINA'S LEAVES (Z1FlowerQuest)
def CHate7 = [id:7]
CHate7.npctext = "Oh that's just wonderful, %p! SQUEEE! You and I will have ever so much fun together!"
CHate7.playertext = "We will?"
CHate7.flag = ["Z1RinaFriend2", "Z1FirstTimeFlowerTalk"]
CHate7.exec = { event ->
	Rina.pushDialog(event.player, "FlowerQuestStart")
}
CandiceHate.addDialog(CHate7, Rina)

def CHate8 = [id:8]
CHate8.npctext = "Is that so?"
CHate8.playertext = "Yeah. That's so."
CHate8.result = 9
CandiceHate.addDialog(CHate8, Rina)

def CHate9 = [id:9]
CHate9.npctext = "Oh."
CHate9.playertext = "I'm sorry. I just don't want to be the ping-pong ball in your little paddle battle with Candice."
CHate9.result = 12  // Yes...I skipped 10 and 11. Sue me.
CandiceHate.addDialog(CHate9, Rina)

def CHate12 = [id:12]
CHate12.npctext = "...was I being mean, %p?"
CHate12.options = []
CHate12.options << [text: "Yes, I suppose you could say that. I'm sure you didn't *mean* to be mean, but making me choose between you and Candice isn't nice.", result: 13]
CHate12.options << [text: "Not mean, really. More like just...well...silly.", result: 14]
CHate12.options << [text: "Oh. Nevermind. I didn't mean it. You're my friend, Rina. I shouldn't have said anything else!", result: 7]
CandiceHate.addDialog(CHate12, Rina)

def CHate13 = [id:13]
CHate13.npctext = "I guess I can see that puts you in a hard place...but..."
CHate13.playertext = "No buts, Rina. It's just not nice."
CHate13.result = 14
CandiceHate.addDialog(CHate13, Rina)

//This response starts the FEATHER QUEST (Z1FeatherQuest)
def CHate14 = [id:14]
CHate14.npctext = "Okay. Well, maybe, just maybe, you could be friends with me AND Candice at the same time?"
CHate14.playertext = "That would be *really* nice, Rina."
CHate14.flag = ["Z1BothFriends", "Z1FirstTimeFeatherTalk"]
CHate14.exec = { event ->
	Rina.pushDialog(event.player, "FeatherQuestStart")
}
CandiceHate.addDialog(CHate14, Rina)

//skipping 16

def CHate17 = [id:17]
CHate17.npctext = "Okay, %p! By-eee!"
CHate17.result = DONE
CandiceHate.addDialog(CHate17, Rina)

def CHate18 = [id:18]
CHate18.npctext = "Thank you, %p! Ooo! Hurry back! This will be FUN!"
CHate18.result = DONE
CandiceHate.addDialog(CHate18, Rina)

//This response cuts Rina loose as a friend and cements in the Candice Quest possibilities
def CHate19 = [id:19]
CHate19.npctext = "Ohhhh, grr. Well...FINE! Be *her* friend then. See if I care! GOODBYE!"
CHate19.result = DONE
CHate19.flag = "Z1RinaEnemy" // RINA IS LOST AS A FRIEND FOREVER!
CandiceHate.addDialog(CHate19, Rina)

def CHate20 = [id:20]
CHate20.npctext = "Hmpphf! Be that way then! I'll be here if you want to apologize later!"
CHate20.result = DONE //Conversation can be reentered for a different ending from here.
CandiceHate.addDialog(CHate20, Rina)

//---------------------------------------------------------
// START FOR RINA'S LEAVES                                 
//---------------------------------------------------------

def FlowerQuestStart = Rina.createConversation("FlowerQuestStart", true, "Z1RinaFriend2", "Z1FirstTimeFlowerTalk", "!QuestStarted_35", "!QuestCompleted_35")
FlowerQuestStart.setUrgent( true )

def FlowerQuest1 = [id:1]
FlowerQuest1.npctext = "Now that we're such good friends and all, could I ask you for a favor?"
FlowerQuest1.playertext = "Of course, Rina! What can I do for you?"
FlowerQuest1.result = 2
FlowerQuestStart.addDialog(FlowerQuest1, Rina)

def FlowerQuest2 = [id:2]
FlowerQuest2.npctext = "If it's not too much trouble, could you gather a couple of those fabulous Wing Tree Leaves for me from the Village Green? They used to just be laying all over the ground, but I've heard that those nasty little Lawn Gnomes have started hoarding them for some reason. I need them for the orders I've been receiving recently."
FlowerQuest2.options = []
FlowerQuest2.options << [text:"Okay, Rina. I'll go get the leaves and come back soon!", result: DONE, quest: 35 ] //Rina's Leaves Start
FlowerQuest2.options << [text:"I'd be happy to help you, Rina, but not right now. I'll come back another time though!", result: 3]
FlowerQuestStart.addDialog(FlowerQuest2, Rina)

def FlowerQuest3 = [id:3]
FlowerQuest3.npctext = "Okay, %p! By-eee!"
FlowerQuest3.flag = "!Z1FirstTimeFlowerTalk"
FlowerQuest3.result = DONE
FlowerQuestStart.addDialog(FlowerQuest3, Rina)

//This is a "return" version of the above conversation.

def ShortHelpHer = Rina.createConversation("ShortHelpHer", true, "Z1RinaFriend2", "!Z1FirstTimeFlowerTalk", "!QuestStarted_35", "!QuestCompleted_35")
ShortHelpHer.setUrgent( true )

def Ready1 = [id:1]
Ready1.npctext = "Hi there, %p! Are you ready to help me out with that favor now?"
Ready1.options = []
Ready1.options << [text:"You bet! What do you need?", result: 2]
Ready1.options << [text:"Oops! Not yet! I'm going to do something else instead.", result: 3]
ShortHelpHer.addDialog(Ready1, Rina)

def Ready2 = [id:2]
Ready2.npctext = "If it's not too much trouble, could you gather a few of those fabulous Wing Tree Leaves for me from the Village Green? They used to just be laying all over the ground, but I've heard that those nasty little Lawn Gnomes have started hording them for some reason. I need at least five of the leaves!"
Ready2.options = []
Ready2.options << [text:"Okay, Rina. I'll go get the leaves and come back soon!", result: DONE, quest: 35 ] //Rina's Leaves Start
Ready2.options << [text:"I'd be happy to help you, Rina, but not right now. I'll come back another time though!", result: 3]
ShortHelpHer.addDialog(Ready2, Rina)

def Ready3 = [id:3]
Ready3.npctext = "Okay, %p! By-eee!"
Ready3.result = DONE
ShortHelpHer.addDialog(Ready3, Rina)

//---------------------------------------------------------
// INTERIM FOR RINA'S LEAVES                              
//---------------------------------------------------------

def FlowerInterim = Rina.createConversation("FlowerInterim", true, "QuestStarted_35", "!QuestStarted_35:3", "!QuestCompleted_35")

def Flower1 = [id:1]
Flower1.npctext = "Hi there, %p! How are you doing finding those leaves for me?"
Flower1.playertext = "I haven't found them all yet, but it's just a matter of keeping to it!"
Flower1.result = 2
FlowerInterim.addDialog(Flower1, Rina)

def Flower2 = [id:2]
Flower2.npctext = "Thanks! There's no huge rush for them right now, but it'll be great when you find them!"
Flower2.playertext = "I'm trying! 'Bye for now!"
Flower2.result = 3
FlowerInterim.addDialog(Flower2, Rina)

def Flower3 = [id:3]
Flower3.npctext = "By-eee!"
Flower3.result = DONE
FlowerInterim.addDialog(Flower3, Rina)

//---------------------------------------------------------
// SUCCESS FOR RINA'S LEAVES                               
//---------------------------------------------------------

def FlowerSuccess = Rina.createConversation("FlowerSuccess", true, "QuestStarted_35:3", "!QuestCompleted_35")
FlowerSuccess.setUrgent( true )

def FlowerSucc1 = [id:1]
FlowerSucc1.npctext = "Oh, goodie! You're so great, %p!! I can't wait to make these arrangements! Everyone has been such sourpusses since the crazy stuff started happening. These leaves are just the thing to surprise them into smiling again! You'll see!"
FlowerSucc1.playertext = "I'm sure they will, Rina. I'm sure they will."
FlowerSucc1.result = DONE
FlowerSucc1.exec = { event ->
	runOnDeduct( event.player, 100280, 10, leafhappy, leafsad ) // Removes ten wing tree leaves from player's inventory
}
FlowerSuccess.addDialog(FlowerSucc1, Rina)

//Happy about the Wing Tree Leaves
def happyLeaves = Rina.createConversation( "happyLeaves", true, "QuestStarted_35:3", "Z1WingTreeLeavesPresent" )

def leaf1 = [id:1]
leaf1.npctext = "You're the best, %p! BFFs forever!"
leaf1.flag = "Z01RinaBFF"
leaf1.quest = 35 // Send the completion update for the Wing Tree Leaf Quest
leaf1.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Rina-VQS" )
	event.player.centerPrint( "You are awarded a Lemon Ring Pop!" )
}
leaf1.result = DONE
happyLeaves.addDialog(leaf1, Rina)

//Sad about the Wing Tree Leaves
def sadLeaves = Rina.createConversation( "sadLeaves", true, "QuestStarted_35:3", "Z1WingTreeLeavesNotPresent" )

def leafsad1 = [id:1]
leafsad1.npctext = "Ummm, did you lose the leaves somewhere along the way?"
leafsad1.playertext = "I must have done something with them. They were right here in my bag earlier. Hmmm..."
leafsad1.result = 2
sadLeaves.addDialog( leafsad1, Rina )

def leafsad2 = [id:2]
leafsad2.npctext = "Well, I'm sure you can get some more where those came from! I need just ten leaves to finish my arrangements."
leafsad2.playertext = "Yup. I sure can. See you soon, Rina!"
leafsad2.result = 3
sadLeaves.addDialog( leafsad2, Rina )

def leafsad3 = [id:3]
leafsad3.npctext = "Okay, %p! By-eee!"
leafsad3.flag = "!Z1WingTreeLeavesNotPresent" //unset the flag so it defaults back to before the feather check occurs
leafsad3.result = DONE
sadLeaves.addDialog( leafsad3, Rina )

//Closures to check to ensure that the player actually still has five pink flamingo feathers at the time of the transaction
leafhappy = { event ->
	event.player.setQuestFlag( GLOBAL, "Z1WingTreeLeavesPresent" )
	Rina.pushDialog( event.player, "happyLeaves" )
}

leafsad = { event ->
	event.player.setQuestFlag( GLOBAL, "Z1WingTreeLeavesNotPresent" )
	Rina.pushDialog( event.player, "sadLeaves" )
}


//---------------------------------------------------------
// START FOR FEATHER QUEST ("Bury the Hatchet")            
//---------------------------------------------------------

def FeatherQuestStart = Rina.createConversation("FeatherQuestStart", true, "Z1BothFriends", "Z1FirstTimeFeatherTalk", "!QuestStarted_45", "!QuestCompleted_45")
FeatherQuestStart.setUrgent( true )

def FeatherQuest1 = [id:1]
FeatherQuest1.npctext = "Maybe you could help me bury the hatchet with Candice? Maybe act as a kind of peace maker between us?"
FeatherQuest1.options = []
FeatherQuest1.options << [text:"That would be great, Rina! What can I do to help?", result: 2]
FeatherQuest1.options << [text:"Possibly.. I'll have to come back another time to help though. I'm busy right now.", result: 3]
FeatherQuestStart.addDialog(FeatherQuest1, Rina)

def FeatherQuest2 = [id:2]
FeatherQuest2.npctext = "Well, Candice likes pink. A LOT. I've heard of pink flamingos running around near the road out of the South Gate in the Village Greens. Do you think you could collect up some pink flamingo feathers and bring them back to me so I can make a feather boa for Candice?"
FeatherQuest2.playertext = "I'd be happy to help out, Rina. I'll get those feathers and bring them back for you!"
FeatherQuest2.quest = 45 //Start the Pink Flamingo Feather Quest
FeatherQuest2.result = DONE
FeatherQuestStart.addDialog(FeatherQuest2, Rina)

def FeatherQuest3 = [id:3]
FeatherQuest3.npctext = "Okay, %p! By-eee!"
FeatherQuest3.flag = "!Z1FirstTimeFeatherTalk"
FeatherQuest3.result = DONE
FeatherQuestStart.addDialog(FeatherQuest3, Rina)


//This is a "return" version of the longer "Help Both" conversation above.
def ShortHelpBoth = Rina.createConversation("ShortHelpBoth", true, "Z1BothFriends", "!Z1FirstTimeFeatherTalk", "!QuestStarted_45", "!QuestCompleted_45")
ShortHelpBoth.setUrgent( true )

def Ready4 = [id:4]
Ready4.npctext = "Are you ready to help me make peace with Candice yet?"
Ready4.options = []
Ready4.options << [text:"Sure! What can I do to help?", result: 5]
Ready4.options << [text:"Oh drat. Not yet. Sorry, Rina, it'll have to be another time.", result: 6]
ShortHelpBoth.addDialog(Ready4, Rina)

def Ready5 = [id:5]
Ready5.npctext = "Well, Candice likes pink. A LOT. I've heard of pink flamingos flying around down near the villages. Do you think you could collect up some pink flamingo feathers and bring them back to me for Candice?"
Ready5.playertext = "I'd be happy to help out, Rina. I'll get those feathers and bring them back for you!"
Ready5.quest = 45 //Start the Pink Flamingo Feather Quest (alternate start point)
Ready5.result = DONE
ShortHelpBoth.addDialog(Ready5, Rina)

def Ready6 = [id:17]
Ready6.npctext = "Okay, %p! By-eee!"
Ready6.result = DONE
ShortHelpBoth.addDialog(Ready6, Rina)


//NOTE: It would be fun, in the "Bury the Hatchet" string to finally get all quests accomplished, and then have them start one-upping each other on gifts until they finally break out into a feud again.

//---------------------------------------------------------
// INTERIM FOR PINK FLAMINGO FEATHER QUEST                 
//---------------------------------------------------------

def FeatherInterim = Rina.createConversation("FeatherInterim", true, "QuestStarted_45", "!QuestStarted_45:3") //45 is the Feather Quest

def Feather1 = [id:1]
Feather1.npctext = "Hi there, %p! Have you found those flamingo feathers for Candice?"
Feather1.playertext = "It seems like the only way to get those feathers is to pull them right off the flamingos themselves!"
Feather1.result = 2
FeatherInterim.addDialog(Feather1, Rina)

def Feather2 = [id:2]
Feather2.npctext = "And...?"
Feather2.playertext = "And they don't seem to like it. Not a bit."
Feather2.result = 3
FeatherInterim.addDialog(Feather2, Rina)

def Feather3 = [id:3]
Feather3.npctext = "Oh!"
Feather3.playertext = "But never fear! I'll get them!"
Feather3.result = 4
FeatherInterim.addDialog(Feather3, Rina)

def Feather4 = [id:4]
Feather4.npctext = "Okay then, %p! By-eee!"
Feather4.result = DONE
FeatherInterim.addDialog(Feather4, Rina)

//---------------------------------------------------------
// SUCCESS FOR PINK FLAMINGO FEATHER QUEST                 
//---------------------------------------------------------

def FeatherSuccess = Rina.createConversation("FeatherSuccess", true, "QuestStarted_45:3", "!QuestCompleted_45")
FeatherSuccess.setUrgent( true )

def FeatherSucc1 = [id:1]
FeatherSucc1.npctext = "Hi again, %p! Did you get them? Did you?"
FeatherSucc1.playertext = "I sure did!"
FeatherSucc1.exec = { event ->
	println "**** Doing the runOnDeduct exec statement now ****"
	runOnDeduct( event.player, 100270, 10, happiness, sadness ) // Removes ten pink flamingo feathers from the player's inventory
}
FeatherSuccess.addDialog(FeatherSucc1, Rina)

//Happy Rina
def happyRina = Rina.createConversation( "happyRina", true, "QuestStarted_45:3", "Z1FeathersPresent" )

def happy1 = [id:1]
happy1.npctext = "Yay! That's great!! The feathers are so soft. They'll be easy to weave together. In fact, just a sec...yeah, I think I can do this right now!"
happy1.playertext = "Neat, Rina! You're great with weaving."
happy1.result = 2
happyRina.addDialog(happy1, Rina)

def happy2 = [id:2]
happy2.npctext = "Thanks! It's going really fast. I get lots of practice arranging flowers all day. It makes my fingers nimble!"
happy2.playertext = "I'll bet."
happy2.result = 3
happyRina.addDialog(happy2, Rina)

def happy3 = [id:3]
happy3.npctext = "There! It's done!"
happy3.quest = 45 // Send the completion update for the Feather Quest
happy3.flag = "Z1FeatherQuestComplete"
happy3.exec = { event ->
	Rina.pushDialog(event.player, "LastStep")
	event.player.removeMiniMapQuestActorName( "Rina-VQS" )
}
happyRina.addDialog(happy3, Rina)

//Sad Rina
def sadRina = Rina.createConversation( "sadRina", true, "QuestStarted_45:3", "Z1FeathersNotPresent" )

def sad1 = [id:1]
sad1.npctext = "Hey! I thought you said you had a bunch of feathers for me!"
sad1.playertext = "Ummmm, I could swear I had them earlier!"
sad1.result = 2
sadRina.addDialog( sad1, Rina )

def sad2 = [id:2]
sad2.npctext = "Well, I can't very well make a present out of non-existent feathers, can I?"
sad2.playertext = "Who can say what is real and what is not real? Perhaps a present made out of make-believe feathers would be subtler and more intriguing?"
sad2.result = 3
sadRina.addDialog( sad2, Rina )

def sad3 = [id:3]
sad3.npctext = "Silly! I can't weave if I can't see the feathers! You'll just have to go back and get some more! I need ten feathers, please!"
sad3.playertext = "*sigh* I thought that might be what you said. Okay. I'm on my way!"
sad3.result = 4
sadRina.addDialog( sad3, Rina )

def sad4 = [id:4]
sad4.npctext = "Okay, %p! By-eee!"
sad4.flag = "!Z1FeathersNotPresent" //unset the flag so it defaults back to before the feather check occurs
sad4.result = DONE
sadRina.addDialog( sad4, Rina )

//Closures to check to ensure that the player actually still has five pink flamingo feathers at the time of the transaction
happiness = { event ->
	println "**** entering 'happiness' routine ****"
	event.player.setQuestFlag( GLOBAL, "Z1FeathersPresent" )
	Rina.pushDialog( event.player, "happyRina" )
}

sadness = { event ->
	println "**** entering 'sadness' routine ****"
	event.player.setQuestFlag( GLOBAL, "Z1FeathersNotPresent" )
	Rina.pushDialog( event.player, "sadRina" )
}

//---------------------------------------------------------
// START CANDY'S BOA QUEST                                 
//---------------------------------------------------------

def LastStep = Rina.createConversation("LastStep", true, "Z1FeatherQuestComplete", "!QuestStarted_46", "!QuestCompleted_46")
LastStep.setUrgent( true )

def Last1 = [id:1]
Last1.npctext = "Could you do me one last favor?"
Last1.options = []
Last1.options << [text:"Sure, Rina! Why would I stop now?", result: 2]
Last1.options << [text:"Maybe later. I'm bushed right now.", result: 4]
LastStep.addDialog(Last1, Rina)

def Last2 = [id:2]
Last2.npctext = "Fabutastic! I can't take this boa over to Candice myself. She'd take one look at me and start throwing things. Could you do it for me?"
Last2.playertext = "Sure, Rina! One more trip across the street!"
Last2.result = 3
LastStep.addDialog(Last2, Rina)

def Last3 = [id:3]
Last3.npctext = "Ooh, I can't wait to hear if she likes it! Good luck, %p!"
Last3.quest = 46 //Candy's Boa quest
Last3.exec = { event ->
	event.player.addMiniMapQuestActorName( "Candice-VQS" )
}
Last3.result = DONE
LastStep.addDialog(Last3, Rina)

def Last4 = [id:4]
Last4.npctext = "Okay! See you soon, I hope! By-eee!"
Last4.result = DONE
LastStep.addDialog(Last4, Rina)

//---------------------------------------------------------
// CANDY'S BOA QUEST (Step 3)                              
//---------------------------------------------------------

def RinaThankful = Rina.createConversation("RinaThankful", true, "QuestStarted_46:3", "!QuestCompleted_46")
RinaThankful.setUrgent( true )

def Thanks1 = [id:1]
Thanks1.npctext = "Well? How did it go? Did Candice like the boa?!?"
Thanks1.playertext = "She sure did, Rina. She LOVED it...and better yet, she wants to be friends with you now!"
Thanks1.result = 2
RinaThankful.addDialog(Thanks1, Rina)

def Thanks2 = [id:2]
Thanks2.npctext = "YAY! We did it, %p! This will be so cool! It'll be like having a sister!"
Thanks2.playertext = "It's true. You're like reflections of the same person sometimes."
Thanks2.result = 3
RinaThankful.addDialog(Thanks2, Rina)

def Thanks3 = [id:3]
Thanks3.npctext = "That'll be so much FUN! I have a present for you, %p. I want you to take it, even though it's a lot. You've done a lot for me and I think you deserve a nice reward, so here's a recipe you can use to make something special."
Thanks3.playertext = "Wow. Thanks, Rina!"
Thanks3.result = 4
RinaThankful.addDialog(Thanks3, Rina)

def Thanks4 = [id:4]
Thanks4.npctext = "You deserve that and more. Thanks again, %p, for giving me a sister to share with!"
Thanks4.playertext = "Good luck to you, Rina. Stay happy!"
Thanks4.result = 5
RinaThankful.addDialog(Thanks4, Rina)

def Thanks5 = [id:5]
Thanks5.npctext = "Okay, %p! By-eee!"
Thanks5.quest = 46 //Send completed quest update for step 3, resulting in QuestCompleted_46
Thanks5.flag = "Z01RinaBFF"
Thanks5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Rina-VQS" )
}
Thanks5.result = DONE
RinaThankful.addDialog(Thanks5, Rina)

//---------------------------------------------------------
// RINA'S AFTERMATH CONVERSATION                           
// (Occurs after either the Boa Quest or Rina's Leaves)    
//---------------------------------------------------------

def RinaAftermath = Rina.createConversation( "RinaAftermath", true, "Z01RinaBFF" )

def After1 = [id:1]
After1.npctext = "Hi there, %p! Are you just sun-sationally happygolicious?"
After1.playertext = "I sure am, Rina! Thanks for asking!"
After1.result = 2
RinaAftermath.addDialog( After1, Rina )

def After2 = [id:2]
After2.npctext = "You bet! I like to keep track of my friends!"
After2.playertext = "That's one of your best characteristics, Rina."
After2.result = 3
RinaAftermath.addDialog( After2, Rina )

def After3 = [id:3]
After3.npctext = "I know, I know. And I never get tired of doing it! You have a nice day, %p! By-eee!"
After3.playertext = "See you later, Rina!"
After3.result = DONE
RinaAftermath.addDialog( After3, Rina )
