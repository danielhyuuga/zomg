//Script created by gfern

import com.gaiaonline.mmo.battle.actor.Player;
import com.gaiaonline.mmo.battle.script.*;
//import com.gaiaonline.mmo.battle.l10n.ResourceFactory;

// Load dictionary of strings for the script.
//def locale = java.util.Locale.getDefault();
//def dict = ResourceFactory.getResources( "scripts.bartontrench.clara", locale );

BarnacleJim = spawnNPC("Barnacle Jim", myRooms.BARTON_3, 175, 470)

BarnacleJim.setWanderBehavior( 50, 150, 3, 7, 250 )
BarnacleJim.startWander(true)
BarnacleJim.setDisplayName( "Barnacle Jim" )

//STOP MOVING WHEN TALKING
def stopMoving(event) {
	BarnacleJim.pause()
}

//START MOVING AFTER DONE TALKING
def startMoving(event) {
	BarnacleJim.pauseResume()
}

//----------------------------------------------------------------------------------------------
//Default dialog
//----------------------------------------------------------------------------------------------
def barnacleJimDefault = BarnacleJim.createConversation("barnacleJimDefault", true)
barnacleJimDefault.setUrgent( false )

def default1 = [id:1]
default1.npctext = "Ahoy %p! Ye have the look of a landlubber!"
default1.options = []
default1.options << [text:"A landlubber? What does that mean?", result: 2, exec: { event -> stopMoving() } ]
default1.options << [text:"Who are you and why are you insulting me?", result: 4, exec: { event -> stopMoving() } ]
default1.options << [text:"Who ye be callin' a landlubber, ya scurvy dog!", result: 6, exec: { event -> stopMoving() } ]
barnacleJimDefault.addDialog(default1, BarnacleJim)

//----------------------------------------------------------------------------------------------
//Dialog option 1
//----------------------------------------------------------------------------------------------
def default2 = [id:2]
default2.npctext = "A landlubber be one what has n'sealegs and n'love fer swashbucklin'."
default2.options = []
default2.options << [text:"Swashbucklin'? Is that some sort of velcro alternative?", result: 3]
default2.options << [text:"Would you mind not insulting me?", result: 4]
default2.options << [text:"Arrr! Avast yer insultin' lest ye find yerself walkin' the plank straight ta Davy Jones' locker!", result: 6]
barnacleJimDefault.addDialog(default2, BarnacleJim)

def default3 = [id:3]
default3.npctext = "Swashbucklin' be the life of adventurin', fightin, an' sailin' the high seas. Even a squiffy be knowin' this."
default3.options = []
default3.options << [text:"Seriously, are the insults necessary?", result: 4]
default3.options << [text:"By the powers! Ye must be lookin' ta be keelhauled!", result: 6]
barnacleJimDefault.addDialog(default3, BarnacleJim)

//----------------------------------------------------------------------------------------------
//Dialog option 2
//----------------------------------------------------------------------------------------------
def default4 = [id:4]
default4.npctext = "Yar, tis just the way I be talkin'. Nothin' personal, me hearty."
default4.options = []
default4.options << [text:"Hearty? Are you coming on to me now?", result: 5]
default4.options << [text:"Oh, like this? 'By the blue blazes, yer nothin' but a grog-besodden dog!'", result: 6]
barnacleJimDefault.addDialog(default4, BarnacleJim)

def default5 = [id:5]
default5.npctext = "Yer and odd one."
default5.exec = { event ->
	startMoving()
}
default5.result = DONE
barnacleJimDefault.addDialog(default5, BarnacleJim)

//----------------------------------------------------------------------------------------------
//Dialog option 3
//----------------------------------------------------------------------------------------------
def default6 = [id:6]
default6.npctext = "Har! Now yer gettin' the 'ang of it, ya rum-soaked swab!"
default6.options = []
default6.options << [text:"Who ye be callin' a rum-soaked swab, ya yellow-bellied bilge-rat!", result: 7]
default6.options << [text:"Okay, that's enough silliness for now.", exec:{ event -> startMoving() }, result: DONE]
barnacleJimDefault.addDialog(default6, BarnacleJim)

def default7 = [id:7]
default7.npctext = "I be calling ye a rum-soaked swab, ya lily-livered kraken-lover!"
default7.options = []
default7.options << [text:"Ye got some nerve, ya lice-infested scalawag!", result: 8]
default7.options << [text:"Alright, that's enough.", exec:{ event -> startMoving() }, result: DONE]
barnacleJimDefault.addDialog(default7, BarnacleJim)

def default8 = [id:8]
default8.npctext = "Arrr! Ye win! I've got n'more insults!"
default8.exec = { event ->
	startMoving()
}
default8.result = DONE
barnacleJimDefault.addDialog(default8, BarnacleJim)