//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

/*Deva = spawnNPC("Deva-VQS", myRooms.BARTON_3, 1315, 945)
Deva.setRotation( 115 )
Deva.setDisplayName( "Deva" )

coliseumIntro = Deva.createConversation("coliseumIntro", true)

coliseumIntro1 = [id:1]
coliseumIntro1.npctext = "Let me be the first to welcome you to the new Barton Coliseum!"
coliseumIntro1.playertext = "What do you mean 'new?'"
coliseumIntro1.result = 2
coliseumIntro.addDialog(coliseumIntro1, Deva)

coliseumIntro2 = [id:2]
coliseumIntro2.npctext = "A wealthy investor recently came into possession of the Coliseum. As with all his investments, he set out to create something remarkable in the Coliseum; a grand arena offering challenges to the bravest Gaians."
coliseumIntro2.playertext = "Challenges? Interesting. What kind?"
coliseumIntro2.result = 3
coliseumIntro.addDialog(coliseumIntro2, Deva)

coliseumIntro3 = [id:3]
coliseumIntro3.npctext = "While I cannot release specific details now, I can tell you there will be three challenge types: Time, Survival, and Protection trials."
coliseumIntro3.options = []
coliseumIntro3.options << [text:"What are Time trials?", result: 4]
coliseumIntro3.options << [text:"Tell me about Survival trials.", result: 5]
coliseumIntro3.options << [text:"How do Protection trials work?", result: 6]
coliseumIntro.addDialog(coliseumIntro3, Deva)

coliseumIntro4 = [id:4]
coliseumIntro4.npctext = "Time trials are a test of speed. Challengers will be given a limited amount of time to complete a series of goals and must race against the clock to win."
coliseumIntro4.options = []
coliseumIntro4.options << [text:"I see. What about Survival trials?", result: 5]
coliseumIntro4.options << [text:"What do challengers do in Protection trials?", result: 6]
coliseumIntro4.options << [text:"Cool. When will the Coliseum be open?", result: 7]
coliseumIntro.addDialog(coliseumIntro4, Deva)

coliseumIntro5 = [id:5]
coliseumIntro5.npctext = "Survival trials are a test of endurance. Challengers will be presented with scenarios of increasing difficulty to see how long they can survive."
coliseumIntro5.options = []
coliseumIntro5.options << [text:"That makes sense. Can you tell me about Time trials?", result: 4]
coliseumIntro5.options << [text:"Alright, how about Protection trials?", result: 6]
coliseumIntro5.options << [text:"When can I do this?", result: 7]
coliseumIntro.addDialog(coliseumIntro5, Deva)

coliseumIntro6 = [id:6]
coliseumIntro6.npctext = "Protection trials are a test of strategy. Challengers must weigh the need to protect an object against other goals presented."
coliseumIntro6.options = []
coliseumIntro6.options << [text:"Hmm, what are Time trials like?", result: 4]
coliseumIntro6.options << [text:"And what about Survival trials?", result: 5]
coliseumIntro6.options << [text:"I can't wait! When does the Coliseum open?", result: 7]
coliseumIntro.addDialog(coliseumIntro6, Deva)

coliseumIntro7 = [id:7]
coliseumIntro7.npctext = "As you can see, construction is nearly complete! We still have some work left to do for the challenges, but the Coliseum's grand opening should be coming soon!"
coliseumIntro7.options = []
coliseumIntro7.options << [text:"Okay. Can you tell me about the challenges again?", result: 3]
coliseumIntro7.options << [text:"Exciting! I'll come back then.", result: DONE]
coliseumIntro.addDialog(coliseumIntro7, Deva)*/