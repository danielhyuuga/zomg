import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

pizzaGuy = spawnNPC("Pizza Guy-VQS", myRooms.BARTON_202, 446, 814)
pizzaGuy.setDisplayName( "Pizza Guy" )
//pizzaGuy.setBaseSpeed( 860 )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	pizzaGuy.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/pizzaguy_strip.png")
}

def aroundTown = makeNewPatrol()
aroundTown.addPatrolPoint( "BARTON_202", 705, 420, 0)
aroundTown.addPatrolPoint( "BARTON_202", 335, 420, 0) //30
aroundTown.addPatrolPoint( "BARTON_102", 190, 495, 0)
aroundTown.addPatrolPoint( "BARTON_2", 515, 820, 0)
aroundTown.addPatrolPoint( "BARTON_2", 1450, 680, 0)
aroundTown.addPatrolPoint( "BARTON_3", 250, 760, 0)
aroundTown.addPatrolPoint( "BARTON_103", 365, 230, 0)
aroundTown.addPatrolPoint( "BARTON_102", 1450, 410, 0)
aroundTown.addPatrolPoint( "BARTON_202", 1095, 100, 0)
aroundTown.addPatrolPoint( "BARTON_202", 705, 420, 0)
aroundTown.addPatrolPoint( "BARTON_202", 935, 895, 0)
aroundTown.addPatrolPoint( "BARTON_302", 1360, 102, 0)
aroundTown.addPatrolPoint( "BARTON_303", 220, 135, 0)
aroundTown.addPatrolPoint( "BARTON_203", 800, 870, 0)
aroundTown.addPatrolPoint( "BARTON_203", 1325, 845, 0)
aroundTown.addPatrolPoint( "BARTON_203", 1045, 360, 0)
aroundTown.addPatrolPoint( "BARTON_103", 1150, 880, 0)
aroundTown.addPatrolPoint( "BARTON_103", 1215, 205, 0)
aroundTown.addPatrolPoint( "BARTON_103", 365, 230, 0)
aroundTown.addPatrolPoint( "BARTON_102", 1450, 410, 0)
aroundTown.addPatrolPoint( "BARTON_202", 1095, 100, 0)
pizzaGuy.setPatrol( aroundTown )
pizzaGuy.startPatrol()

/*
//START MOVING AFTER DONE TALKING
def startMoving(event) {
	pizzaGuy.pauseResume()
	event.player.setQuestFlag( GLOBAL, "Z1PizzaTimer" )
	myManager.schedule(60) { event.player.unsetQuestFlag( GLOBAL, "Z1PizzaTimer" ) }

}


//PlayerList for the area he stops
myManager.onEnter( myRooms.BARTON_202 ) { event ->
	playerList202 << event.actor
}

myManager.onExist( myRooms.BARTON_202 ) { event ->
	playerList202.remove( event.actor )
	event.actor.unsetQuestFlag( GLOBAL, "Z01PizzaGuyCanTalk" )
}

//Trigger zone to make Pizza Guy stop and become able to talk
def convoTrigger = "convoTrigger"
myRooms.BARTON_202.createTriggerZone( convoTrigger, 260, 360, 520, 470 )

okayToTalk = false

myManager.onTriggerIn(myRooms.BARTON_202, convoTrigger) { event ->
	if( event.actor == pizzaGuy ) {
		event.actor.pause()
		okayToTalk = true
		println "**** Pizza Guy in trigger zone ****"
		myManager.schedule(1) { println "****Starting conversation****"; activatePizzaTalk() }
		myManager.schedule(30) { println "****Trying to shut off conversations****"; okayToTalk = false; event.actor.pauseResume() }
	}
}

//========================================================
// PIZZA GUY CONVERSATION                                 
//========================================================
def PizzaConvo = pizzaGuy.createConversation( "PizzaConvo", true, "!Z1PlayerLiedToMe", "!Z1PlayerTruth", "!Z1PizzaTimer" )

def dialog1 = [id:1]
dialog1.npctext = "Hi there! Have you seen a house with the black roof? These pizzas are getting stone cold and I still can't find it. Any ideas?"
dialog1.options = []
dialog1.options << [text:"Oh yeah...totally! That's over by the Art Arena!", result:3 ]
dialog1.options << [ text:"I think you're in trouble, buddy. I haven't seen a black roof anywhere.", result:2 ]
PizzaConvo.addDialog(dialog1, pizzaGuy)

def dialog2 = [id:2]
dialog2.npctext = "Oh, man...I'm so dead. Thanks, anyway!"
dialog2.flag = "Z1PizzaTimer"
dialog2.result = DONE
PizzaConvo.addDialog(dialog2, pizzaGuy)

def dialog3 = [id:3]
dialog3.npctext = "w00t! I'm SO there! Thanks!"
dialog3.playertext = "No problem!"
dialog3.flag = "Z1PlayerTruth"
dialog3.result = 4
PizzaConvo.addDialog(dialog3, pizzaGuy)

def dialog4 = [id:4]
dialog4.playertext = "*snicker*"
dialog4.flag = ["Z1PlayerLiedToMe", "Z1PizzaTimer"]
dialog4.result = DONE
PizzaConvo.addDialog(dialog4, pizzaGuy)

//========================================================
// IF THE PLAYER LIES TO PIZZAGUY THE FIRST TIME          
//========================================================
def PizzaLiedToMe = pizzaGuy.createConversation( "PizzaLiedToMe", true, "Z1PlayerLiedToMe", "!Z1PizzaWillNotTalk", "!Z1PizzaTimer" )

def lied1 = [id:1]
lied1.npctext = "You again? You've got a lot of gall. Buzz off!"
lied1.options = []
lied1.options << [text:"What do you mean? Did you find the black roof or not?", result: 3 ]
lied1.options << [text:"LOL. You fell for that hook, line, and sinker!", result: 2 ]
PizzaLiedToMe.addDialog( lied1, pizzaGuy )

def lied2 = [id:2]
lied2.npctext = "Yeah...well...not anymore. I wouldn't order pizza if I were you. You might not like what you find on it when it arrives."
lied2.flag = ["Z1PizzaWillNotTalk", "Z1PizzaTimer"]
lied2.result = DONE
PizzaLiedToMe.addDialog( lied2, pizzaGuy )

def lied3 = [id:3]
lied3.npctext = "What? You mean you really thought it was over there?"
lied3.options = []
lied3.options << [text:"Yeah. I guess I was mixed up. That's a purple roof over there, isn't it? The black roof is down by Olivia's guild building. Sorry.", result: 5]
lied3.options << [text:"Oh man...you are SO gullible. Enjoy your cold pizza!", result: 4]
PizzaLiedToMe.addDialog( lied3, pizzaGuy )

def lied4 = [id:4]
lied4.npctext = "ha. ha. ha. You're a real card."
lied4.flag = ["Z1PizzaWillNotTalk", "Z1PizzaTimer"]
lied4.result = DONE
PizzaLiedToMe.addDialog( lied4, pizzaGuy )

def lied5 = [id:5]
lied5.npctext = "Oh. Really? heh. Okay. No biggie. Thanks. I'm headed down there now!"
lied5.playertext = "Good luck!"
lied5.result = 6
PizzaLiedToMe.addDialog( lied5, pizzaGuy )

def lied6 = [id:6]
lied6.playertext = "hehehe. Dummy."
lied6.flag = ["Z1LiedTwice", "Z1PizzaTimer"]
lied6.result = DONE
PizzaLiedToMe.addDialog( lied6, pizzaGuy )

//========================================================
// IF THE PLAYER LIES TO PIZZAGUY AGAIN                   
//========================================================
def PizzaLiedAgain = pizzaGuy.createConversation( "PizzaLiedAgain", true, "Z1LiedTwice", "!Z1PizzaTimer" )

def Twice1 = [id:1]
Twice1.npctext = "Jerk. Don't even talk to me! I can't believe I fell for that...TWICE!!!"
Twice1.playertext = "LOL!"
Twice1.flag = "Z1PizzaTimer"
Twice1.result = DONE
PizzaLiedAgain.addDialog( Twice1, pizzaGuy )

//========================================================
// IF THE PLAYER TOLD THE TRUTH ABOUT THE BLACK ROOF      
//========================================================
def PizzaTruth = pizzaGuy.createConversation( "PizzaTruth", true, "Z1PlayerTruth", "!Z1PizzaTimer" )

def Truth1 = [id:1]
Truth1.playertext = "Still looking for that building?"
Truth1.result = 2
PizzaTruth.addDialog( Truth1, pizzaGuy )

def Truth2 = [id:2]
Truth2.npctext = "Yeah...I don't have a choice. My boss said 'Deliver that pizza, or don't come back', so I'm not quitting until I find it!"
Truth2.playertext = "Well...keep looking. Maybe someone will paint their roof black soon for you."
Truth2.result = 3
PizzaTruth.addDialog( Truth2, pizzaGuy )

def Truth3 = [id:3]
Truth3.npctext = "Hehehe. Thanks. I needed a laugh. Okay...I must just be missing it somewhere. See ya!"
Truth3.playertext = "Good luck!"
Truth3.flag = "Z1PizzaTimer"
Truth3.result = DONE
PizzaTruth.addDialog( Truth3, pizzaGuy)

*/
