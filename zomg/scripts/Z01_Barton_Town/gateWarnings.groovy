import com.gaiaonline.mmo.battle.script.*;

Jan = spawnNPC("BFG-Jan", myRooms.BARTON_3, 895, 345)
Jan.setDisplayName( "Jan" )

George = spawnNPC("BFG-George", myRooms.BARTON_201, 310, 430)
George.setRotation( 10 )
George.setDisplayName( "George" )

//TODO : Adjust the Total Warning levels so they're appropriate.


//---------------------------------------------------------
// EAST AND NORTH GATE WARNING TRIGGERS                    
// (Designed to warn players if their Total Charge is too  
// low for the areas they are about to enter.)             
//---------------------------------------------------------

def northGateTrigger = "northGateTrigger"
myRooms.BARTON_3.createTriggerZone( northGateTrigger, 600, 1, 1020, 250 )

myManager.onTriggerIn(myRooms.BARTON_3, northGateTrigger) { event ->
	if( isPlayer( event.actor ) && event.actor.getConLevel() < 4 && !event.actor.hasQuestFlag( GLOBAL, "Z01ComingFromBassken" ) ) { 
		event.actor.setQuestFlag( GLOBAL, "Z01NorthGateWarning" )
		controlPlayer( event.actor )
		Jan.pushDialog( event.actor, "northGateWarning" )
		myManager.schedule(5) { freePlayer( event.actor ) }
	} else {
		event.actor.warp( "BASSKEN_607", 580, 300 ) //if the player doesn't need the warning, then warp them out to Bass'ken
	}
}

def northGateWarning = Jan.createConversation( "northGateWarning", true, "Z01NorthGateWarning" )
	
def northWarning1 = [id:1]
northWarning1.npctext = "Whoa there, friend! You're about to enter Bass'ken Lake and it's a pretty rough area."
northWarning1.playertext = "Yeah? So?"
northWarning1.result = 2
northGateWarning.addDialog( northWarning1, Jan )

def northWarning2 = [id:2]
northWarning2.npctext = "Well, I'm sorry, but I'm under orders not to let anyone out there unless they can handle themselves well enough. No offense. I'd suggest exploring the Village Greens south of Barton Town first."
northWarning2.playertext = "Okay. I understand. Thanks."
northWarning2.result = 3
northGateWarning.addDialog( northWarning2, Jan )

def northWarning3 = [id:3]
northWarning3.npctext = "No problem. Good day to you!"
northWarning3.flag = "!Z01NorthGateWarning"
northWarning3.exec = { event ->
	freePlayer( event.player )
}
northWarning3.result = DONE
northGateWarning.addDialog( northWarning3, Jan )

//------------------------------------------------------------------------------------

def eastGateTrigger = "eastGateTrigger"
myRooms.BARTON_201.createTriggerZone( eastGateTrigger, 1, 300, 290, 750 )


myManager.onTriggerIn(myRooms.BARTON_201, eastGateTrigger) { event ->
	if( isPlayer( event.actor ) && event.actor.getConLevel() < 3 && !event.actor.hasQuestFlag( GLOBAL, "Z01ComingFromZenGardens" ) ) { 
		event.actor.setQuestFlag( GLOBAL, "Z01EastGateWarning" )
		controlPlayer( event.actor )
		George.pushDialog( event.actor, "eastGateWarning" )
		myManager.schedule(5) { freePlayer( event.actor ) }
	} else {
		event.actor.warp( "ZENGARDEN_305", 760, 400 ) //if the player doesn't need the warning, then warp them out to Zen Gardens
	}
}

def eastGateWarning = George.createConversation( "eastGateWarning", true, "Z01EastGateWarning" )
	
def eastWarning1 = [id:1]
eastWarning1.npctext = "Hey there! You're headed toward the Zen Gardens!"
eastWarning1.playertext = "Cool!"
eastWarning1.result = 2
eastGateWarning.addDialog( eastWarning1, George )

def eastWarning2 = [id:2]
eastWarning2.npctext = "Maybe not so cool. That area is crawling with Animated and it can be pretty rough out there. If you're new to using your rings, you might try the South Gate instead."
eastWarning2.playertext = "What if I don't want to?"
eastWarning2.result = 3
eastGateWarning.addDialog( eastWarning2, George )

def eastWarning3 = [id:3]
eastWarning3.npctext = "I'm sorry to tell you, but I can't let you out this gate until you can handle things a bit better. Try outside the South Gate for a while and build up your rings first."
eastWarning3.playertext = "Okay. I guess you're just doing your job. Thanks."
eastWarning3.flag = "!Z01EastGateWarning"
eastWarning3.exec = { event ->
	freePlayer( event.player )
}
eastWarning3.result = DONE
eastGateWarning.addDialog( eastWarning3, George )