//Script created by gfern

import com.gaiaonline.mmo.battle.actor.Player;
import com.gaiaonline.mmo.battle.script.*;

def bfgFernando = spawnNPC("BFG-Fernando", myRooms.BARTON_302, 385, 425)
bfgFernando.setRotation( 45 )
bfgFernando.setDisplayName( "Fernando" )

onQuestStep(38, 2) { event -> event.player.addMiniMapQuestActorName( "BFG-Fernando" ) }

onQuestStep(39, 2) { event -> event.player.addMiniMapQuestActorName( "BFG-Fernando" ) } 

//---------------------------------------------------------------------------------------------------
//Default Conversation                                                                               
//---------------------------------------------------------------------------------------------------
def bfgFernandoConversationA = bfgFernando.createConversation("bfgFernandoDialog", true, "!QuestStarted_38", "!QuestStarted_39", "!QuestCompleted_38", "!QuestCompleted_39")
bfgFernandoConversationA.setUrgent( false )

def dialogA1 = [id:1]
dialogA1.npctext = "Who'd have thought, when I was a child, that I would have become a guard? I was destined for so much more...before the accident."
dialogA1.options = []
dialogA1.options << [text: "An accident? Were you injured?", result:2]
//dialogA1.options << [text: "What did you do before your accident?", result:3]
dialogA1.options << [text: "Can you give me directions?", result:4]
bfgFernandoConversationA.addDialog(dialogA1, bfgFernando)

def dialogA2 = [id:2]
dialogA2.npctext = "Suffice to say, I have not been on stage since it happened. My life in the spotlight ended that day."
dialogA2.options = []
dialogA2.options << [text: "That's terrible! What happened?", result:5]
//dialogA2.options << [text: "Stage? Were you some kind of performer?", result:3]
dialogA2.options << [text: "Oh, well, I really only came over here for directions.", result:4]
bfgFernandoConversationA.addDialog(dialogA2, bfgFernando)

def dialogA4 = [id:4]
dialogA4.npctext = "Oh, very well. Perhaps I can help. Where are you trying to go?"
dialogA4.playertext = "Do you know where the Clan House is?"
dialogA4.result = 7
bfgFernandoConversationA.addDialog(dialogA4, bfgFernando)

def dialogA5 = [id:5]
dialogA5.npctext = "No. I do not talk about the specifics of that incident. It is far too painful to recall."
dialogA5.options =[]
dialogA5.options << [text: "Oh, come on. I have to know!", result:8]
//dialogA5.options << [text: "Very well. Can you talk about what you did before you were a guard?", result:3]
dialogA5.options << [text: "Well, I won't make you talk about it then. Just help me with some directions and I'll be on my way.", result:4]
bfgFernandoConversationA.addDialog(dialogA5, bfgFernando)

def dialogA7 = [id:7]
dialogA7.npctext = "Uhm, was that a joke? This IS the Clan House!"
dialogA7.result = DONE
bfgFernandoConversationA.addDialog(dialogA7, bfgFernando)

def dialogA8 = [id:8]
dialogA8.npctext = "I told you I'm not talking about it. Only an unemotional robot could talk about something so painful."
dialogA8.quest = 38
dialogA8.result = DONE
bfgFernandoConversationA.addDialog(dialogA8, bfgFernando)

//---------------------------------------------------------------------------------------------------------------------------------
//Conversation given while player is on Fernando's Secret                                                                          
//---------------------------------------------------------------------------------------------------------------------------------
def bfgFernandoConversationK = bfgFernando.createConversation("bfgFernandoConversationK", true, "QuestStarted_38:2" )
bfgFernandoConversationK.setUrgent( false )

def dialogK1 = [id:1]
dialogK1.npctext = "Who'd have thought, when I was a child, that I would have become a guard? I was destined for so much more...before the accident."
dialogK1.playertext = "Could I trouble you for some directions?"
dialogK1.result = 3
bfgFernandoConversationK.addDialog(dialogK1, bfgFernando)

def dialogK3 = [id:3]
dialogK3.npctext = "Oh, very well. Perhaps I can help. Where are you trying to go?"
dialogK3.options = []
dialogK3.options << [text:"Do you know where the Clan House is?", result:6]
dialogK3.options << [text:"Who can tell me what happened to you?", result:4]
bfgFernandoConversationK.addDialog(dialogK3, bfgFernando)

def dialogK4 = [id:4]
dialogK4.npctext = "You're kidding, right? You don't honestly believe I'd tell you that, do you?"
dialogK4.playertext = "It was worth a shot..."
dialogK4.result = DONE
bfgFernandoConversationK.addDialog(dialogK4, bfgFernando)

def dialogK6 = [id:6]
dialogK6.npctext = "Uhm, was that a joke? This IS the Clan House!"
dialogK6.result = DONE
bfgFernandoConversationK.addDialog(dialogK6, bfgFernando)

//---------------------------------------------------------------------------------------------------------------------------------
//Conversation given if player has completed Fernando's Secret quest                                                               
//---------------------------------------------------------------------------------------------------------------------------------
def bfgFernandoConversationJ = bfgFernando.createConversation("bfgFernandoConversationJ", true, "QuestStarted_38:3")
bfgFernandoConversationJ.setUrgent( true )

def dialogJ1 = [id:1]
dialogJ1.npctext = "Please don't ask me about my accident."
dialogJ1.playertext = "Accident? You mean when you forgot to pull your zipper up?"
dialogJ1.exec = { event -> event.player.removeMiniMapQuestActorName("BFG-Fernando") }
dialogJ1.result = 2
bfgFernandoConversationJ.addDialog(dialogJ1, bfgFernando)

def dialogJ2 = [id:2]
dialogJ2.npctext = "What?! Who told you?!"
dialogJ2.options = []
dialogJ2.options << [text: "Anniedroid.", result:4]
dialogJ2.options << [text: "It doesn't matter. Don't worry, your secret is safe with me, Fernando.", result:3]
bfgFernandoConversationJ.addDialog(dialogJ2, bfgFernando)

def dialogJ3 = [id:3]
dialogJ3.npctext = "Oh, sure. That's what they all say right before they tell the whole town!"
dialogJ3.options = []
dialogJ3.options << [text: "Look, I'll prove it to you. I'll find a pair of zipper-less pants for you. Then you can dance again!", result:5]
dialogJ3.options << [text: "Bah, quit being so dramatic, you big baby.", result:6]
bfgFernandoConversationJ.addDialog(dialogJ3, bfgFernando)

def dialogJ4 = [id:4]
dialogJ4.npctext = "I knew it! That little robot-wannabe is always gossiping about me! Oooh, I'm so mad I could pull her plug!"
dialogJ4.options = []
dialogJ4.options << [text:"Don't worry. Your secret is safe with me. I'll even track down a pair of zipper-less pants for you to prove it.", result:5]
dialogJ4.options << [text:"Jeez. You're quite a whiner, aren't you?", result:6]
bfgFernandoConversationJ.addDialog(dialogJ4, bfgFernando)

def dialogJ5 = [id:5]
dialogJ5.npctext = "Good luck with that. I've been looking all over!"
dialogJ5.playertext = "Surely one of the shops in Barton Town will have something."
dialogJ5.quest = 39
dialogJ5.exec = { event ->
	event.actor.updateQuest(38, "BFG-Fernando")
}
dialogJ5.result = DONE
bfgFernandoConversationJ.addDialog(dialogJ5, bfgFernando)

def dialogJ6 = [id:6]
dialogJ6.npctext = "I'm no baby. Leave me alone."
dialogJ6.quest = 38
dialogJ6.result = DONE
bfgFernandoConversationJ.addDialog(dialogJ6, bfgFernando)

//---------------------------------------------------------------------------------------------------------------------------------
//Conversation given if player has completed Fernando's Secret quest but never accepted Fabulously Flying Fernando Quest           
//---------------------------------------------------------------------------------------------------------------------------------
def bfgFernandoConversationI = bfgFernando.createConversation("bfgFernandoConversationI", true, "!QuestStarted_39", "QuestCompleted_38", "!QuestCompleted_39")
bfgFernandoConversationI.setUrgent( false )

def dialogI1 = [id:1]
dialogI1.npctext = "Back to poke fun some more?"
dialogI1.options = []
dialogI1.options << [text:"No. I'm sorry about that. Look, I'll even track down a pair of zipper-less pants for you to prove it.", result: 2]
dialogI1.options << [text:"Poke, poke, poke.", result: 3]
bfgFernandoConversationI.addDialog(dialogI1, bfgFernando)

def dialogI2 = [id:2]
dialogI2.npctext = "Good luck with that. I've been looking all over!"
dialogI2.playertext = "Surely one of the shops in Barton Town will have something."
dialogI2.quest = 39
dialogI2.result = DONE
bfgFernandoConversationI.addDialog(dialogI2, bfgFernando)

def dialogI3 = [id:3]
dialogI3.npctext = "You just go away! I'm far too fabulous for this sort of treatment."
dialogI3.result = DONE
bfgFernandoConversationI.addDialog(dialogI3, bfgFernando)

//---------------------------------------------------------------------------------------------------------------------------------
//Conversation given while player is on Flying Fernando                                                                            
//---------------------------------------------------------------------------------------------------------------------------------
def bfgFernandoConversationL = bfgFernando.createConversation("bfgFernandoConversationL", true, "QuestStarted_39:2")
bfgFernandoConversationL.setUrgent( false )

def dialogL1 = [id:1]
dialogL1.npctext = "Who'd have thought, when I was a child, that I would have become a guard? I was destined for so much more...before the accident."
dialogL1.playertext = "Could I trouble you for some directions?"
dialogL1.result = 3
bfgFernandoConversationL.addDialog(dialogL1, bfgFernando)

def dialogL3 = [id:3]
dialogL3.npctext = "Oh, very well. Perhaps I can help. Where are you trying to go?"
dialogL3.options = []
dialogL3.options << [text:"Do you know where the Clan House is?", result:6]
dialogL3.options << [text:"Any idea where I can find some zipperless pants?", result:4]
bfgFernandoConversationL.addDialog(dialogL3, bfgFernando)

def dialogL4 = [id:4]
dialogL4.npctext = "I told you I've been looking for a pair myself. If I knew where to find zipper-less pants I'd already have them!"
dialogL4.result = DONE
bfgFernandoConversationL.addDialog(dialogL4, bfgFernando)

def dialogL6 = [id:6]
dialogL6.npctext = "Uhm, was that a joke? This IS the Clan House!"
dialogL6.result = DONE
bfgFernandoConversationL.addDialog(dialogL6, bfgFernando)

//---------------------------------------------------------------------------------------------------------------------------------
//Conversation given after player retrieves pants from Rufus                                                                       
//---------------------------------------------------------------------------------------------------------------------------------
def bfgFernandoConversationD = bfgFernando.createConversation("bfgFernandoConversationD", true, "QuestStarted_39:3")
bfgFernandoConversationD.setUrgent( true )

def dialogD1 = [id:1]
dialogD1.npctext = "What are these? OMG! No zipper! These are perfect. Where did you find them?"
dialogD1.result = 2
bfgFernandoConversationD.addDialog(dialogD1, bfgFernando)

def dialogD2 = [id:2]
dialogD2.playertext = "Rufus gave them to me."
dialogD2.result = 3
bfgFernandoConversationD.addDialog(dialogD2, bfgFernando)

def dialogD3 = [id:3]
dialogD3.npctext = "Rufus? He never wears pants though..."
dialogD3.result = 4
bfgFernandoConversationD.addDialog(dialogD3, bfgFernando)

def dialogD4 = [id:4]
dialogD4.playertext = "Exactly, that's why he was willing to part with them."
dialogD4.quest = 39
dialogD4.exec = { event -> event.player.removeMiniMapQuestActorName( "BFG-Fernando" ) }
dialogD4.result = DONE
bfgFernandoConversationD.addDialog(dialogD4, bfgFernando)

//---------------------------------------------------------------------------------------------------------------------------------
//Conversation given after Fabulously Flying Fernando quest's complete                                                             
//---------------------------------------------------------------------------------------------------------------------------------
def bfgFernandoConversationH = bfgFernando.createConversation("bfgFernandoConversationH", true, "QuestCompleted_38", "QuestCompleted_39")
bfgFernandoConversationH.setUrgent( false )

def dialogH1 = [id:1]
dialogH1.npctext = "%p! The pants are working out great! Thanks again!"
dialogH1.playertext = "I'm glad to hear that. Could I trouble you for some directions?"
dialogH1.result = 3
bfgFernandoConversationH.addDialog(dialogH1, bfgFernando)

def dialogH3 = [id:3]
dialogH3.npctext = "Oh, very well. Perhaps I can help. Where are you trying to go?"
dialogH3.playertext = "Do you know where the Clan House is?"
dialogH3.result = 6
bfgFernandoConversationH.addDialog(dialogH3, bfgFernando)

def dialogH6 = [id:6]
dialogH6.npctext = "Uhm, was that a joke? This IS the Clan House!"
dialogH6.result = DONE
bfgFernandoConversationH.addDialog(dialogH6, bfgFernando)