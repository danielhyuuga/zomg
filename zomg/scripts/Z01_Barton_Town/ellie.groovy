import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def Ellie = spawnNPC("Ellie-VQS", myRooms.BARTON_104, 820, 655)
Ellie.setRotation( 45 )
Ellie.setDisplayName( "Ellie" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Ellie.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/ellie_strip.png")
}
//------------------------------------------
//ELLIE'S FRIENDS QUEST (START)             
//------------------------------------------

def EllieQuest = Ellie.createConversation("EllieQuest", true, "!QuestStarted_31", "!QuestCompleted_31") //Don't play this convo if the quest has started already.
EllieQuest.setUrgent( true )

def Ellie1 = [id:1]
Ellie1.npctext = "...can't talk. Busy."
Ellie1.playertext = "Painting, eh?"
Ellie1.result = 2
EllieQuest.addDialog(Ellie1, Ellie)

def Ellie2 = [id:2]
Ellie2.npctext = "It's my passion. I was supposed to meet some friends for lunch, but the light is *just* right at the moment."
Ellie2.playertext = "That's...not good?"
Ellie2.result = 3
EllieQuest.addDialog(Ellie2, Ellie)

def Ellie3 = [id:3]
Ellie3.npctext = "It's very good! But this light only happens once a day!"
Ellie3.playertext = "But what about your friends?"
Ellie3.result = 4
EllieQuest.addDialog(Ellie3, Ellie)

def Ellie4 = [id:4]
Ellie4.npctext = "Oh, drat, you're right. Would you mind doing me a favor?"
Ellie4.options = []
Ellie4.options << [text:"Sure. I'd be glad to help. What do you need?", result: 5]
Ellie4.options << [text:"I don't have time right now. But good luck with your light!", result: 7]
EllieQuest.addDialog(Ellie4, Ellie)

def Ellie5 = [id:5]
Ellie5.npctext = "I was supposed to meet up with Maestro and Agatha for a chat. Maestro is at the Music Box and Agatha runs Barton Jewelers. Could you run over to each of their stores and let them know I'll be late?"
Ellie5.playertext = "The Maestro at the Music Box, and Agatha at Barton Jewelers?"
Ellie5.result = 6
EllieQuest.addDialog(Ellie5, Ellie)

def Ellie6 = [id:6]
Ellie6.npctext = "That's right! Thanks for doing this. I hate to be rude, but I've got to get back to it before the light changes!"
Ellie6.playertext = "No problem. See you soon, Ellie!"
Ellie6.quest = 31 //push the start of the Ellie's Friends quest
Ellie6.exec = { event ->
	event.player.addMiniMapQuestActorName( "Agatha-VQS" )
	event.player.addMiniMapQuestActorName( "The Maestro-VQS" )
	if( event.player.isOnQuest( 287, 2 ) ) {
		event.player.removeMiniMapQuestActorName( "Ellie-VQS" )
	}
}
Ellie6.flag = "Z1EllieConvosAreActive" //This flag bypasses the default convos for the Maestro and Agatha.
Ellie6.result = DONE
EllieQuest.addDialog(Ellie6, Ellie)

def Ellie7 = [id:7]
Ellie7.npctext = "Fine then. As long as you stay out of my light."
Ellie7.result = DONE
EllieQuest.addDialog(Ellie7, Ellie)

//------------------------------------------
//INTERIM CONVERSATIONS                     
//(During the Ellie's Friends quest)        
//------------------------------------------

//This conversation occurs AFTER the quest has been started, and after speaking to AGATHA but before the player has talked to MAESTRO.
def EllieInterim = Ellie.createConversation("EllieInterim", true, "QuestStarted_31:2", "Z1AgathaGoodbye", "!Z1MaestroGoodbye")
EllieInterim.setUrgent( false )

def Interim1 = [id:1]
Interim1.npctext = "Hello, %p. I'm still busy here. Have you been to see Agatha and the Maestro yet?"
Interim1.playertext = "I've spoken to Agatha, but I still have to talk to the Maestro."
Interim1.result = 2
EllieInterim.addDialog(Interim1, Ellie)

def Interim2 = [id:2]
Interim2.npctext = "Well, I'd hate for the Maestro to worry, so it'll be great when he hears from you! Thanks!"
Interim2.result = DONE
EllieInterim.addDialog(Interim2, Ellie)

//This conversation occurs AFTER the quest has been started, and after speaking to MAESTRO but before the player has talked to AGATHA.
def EllieInterim2 = Ellie.createConversation("EllieInterim2", true, "QuestStarted_31:2", "Z1MaestroGoodbye", "!Z1AgathaGoodbye")
EllieInterim2.setUrgent( false )

def InterimTwo1 = [id:1]
InterimTwo1.npctext = "Hi, %p! I'm still painting away. Did you talk to Agatha and the Maestro yet?"
InterimTwo1.playertext = "I found the Maestro, but I haven't talked to Agatha quite yet."
InterimTwo1.result = 2
EllieInterim2.addDialog(InterimTwo1, Ellie)

def InterimTwo2 = [id:2]
InterimTwo2.npctext = "She has a tendency to fret if she doesn't hear from me, so I'm glad you're still heading over there to see her! Thanks!"
InterimTwo2.result = DONE
EllieInterim2.addDialog(InterimTwo2, Ellie)

//------------------------------------------
//SUCCESS CONVERSATION                      
//AFTER speaking to both other NPCs, a      
//player can get a nice thank you from      
//Ellie when they return.                   
//------------------------------------------
def EllieThankYou = Ellie.createConversation("EllieThankYou", true, "QuestStarted_31:3")
EllieThankYou.setUrgent( true )

def Ellie8 = [id:8]
Ellie8.npctext = "Oh, hi again, %p! Did you get a chance to talk to Agatha and the Maestro yet?"
Ellie8.playertext = "I sure did! They know all about the situation with your painting and the light."
Ellie8.result = 9
EllieThankYou.addDialog(Ellie8, Ellie)

def Ellie9 = [id:9]
Ellie9.npctext = "Thank you so much! This piece I'm working on now is SO worth the effort. You've been a real help."
Ellie9.playertext = "Thanks! It was nothing. Good luck with your painting!"
Ellie9.result = DONE
Ellie9.flag = "!Z1EllieConvosAreActive" //Turn off the Ellie flag so that Agatha and Maestro resume default convos
Ellie9.quest = 31 //push the completion for Ellie's Friends quest
Ellie9.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Ellie-VQS" )
}
EllieThankYou.addDialog(Ellie9, Ellie)

//------------------------------------------
//DEFAULT CONVERSATION                      
//This convo doesn't occur until AFTER the  
//player has completed the Ellie's Friends  
//quest.                                    
//(Ellie's Quest is one of the first things 
//the player can do after starting the game.
//------------------------------------------
def EllieDefault = Ellie.createConversation("EllieDefault", true, "QuestCompleted_31")
EllieDefault.setUrgent( false )

def Default1 = [id:1]
Default1.npctext = "Hi there! Are you interested in the Art Arena, or did you just stop to see what I'm painting?"
Default1.options = []
Default1.options << [text:"Can you tell me about the Art Arena?", result: 2]
Default1.options << [text:"Are you painting something interesting?", result: 5]
Default1.options << [text:"Oh nothing. Nevermind.", result: 9]
EllieDefault.addDialog(Default1, Ellie)

def Default2 = [id:2]
Default2.npctext = "Sure! We use the old coliseum here as a gigantic art gallery, unrivaled anywhere else on Gaia. People from all over the world bring their best works here to display in the exhibits, whether illustration, writing, video, or even group costume displays."
Default2.playertext = "Wow! That sounds amazing!"
Default2.result = 3
EllieDefault.addDialog(Default2, Ellie)

def Default3 = [id:3]
Default3.npctext = "It is! We also let guests write their comments in logs beneath each exhibit, even lodging their own vote about whether they like it or not. It's a great way for our creative community to get almost immediate feedback on their work after they display it in the Arena."
Default3.playertext = "I've never seen anything like that before!"
Default3.result = 4
EllieDefault.addDialog(Default3, Ellie)

def Default4 = [id:4]
Default4.npctext = "Well you can now! Just step through the archway over there and you'll be in the Arena. It's free, so enjoy! Now, if you don't mind, I'll get back to my painting."
Default4.playertext = "No problem! Thanks for your help!"
Default4.result = DONE
EllieDefault.addDialog(Default4, Ellie)

def Default5 = [id:5]
Default5.npctext = "No. I always paint extremely boring, mundane things."
Default5.playertext = "...really?"
Default5.result = 6
EllieDefault.addDialog(Default5, Ellie)

def Default6 = [id:6]
Default6.npctext = "Were you dropped as a child? Of course not! For instance, look at that rock. Don't you see the excessive veining and coloration in it? See how the light plays across the texture on its surface? See how the placement of that rock changes your aesthetic reaction to everything else around it? See how INTERESTING it is?!?"
Default6.playertext = "Ummm...yes?"
Default6.result = 7
EllieDefault.addDialog(Default6, Ellie)

def Default7 = [id:7]
Default7.npctext = "That answers your question then, doesn't it?"
Default7.playertext = "Of...course. Then...*everything* is interesting?"
Default7.result = 8
EllieDefault.addDialog(Default7, Ellie)

def Default8 = [id:8]
Default8.npctext = "Yes! You have it precisely!"
Default8.result = DONE
EllieDefault.addDialog(Default8, Ellie)

def Default9 = [id:9]
Default9.npctext = "All right then! Have a good day!"
Default9.result = DONE
EllieDefault.addDialog(Default9, Ellie)

if (eventStart != 'HALLOWEEN') return

//======================================
// HALLOWEEN TRICK OR TREATING SCRIPT   
//======================================

playerList = []
fluffList = []

myManager.onEnter( myRooms.BARTON_104 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList << event.actor 
		event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" )
	}
}

myManager.onExit( myRooms.BARTON_104 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList.remove( event.actor )
	}
}

//Pumpkin Fluff Spawner
pumpFluff104 = myRooms.BARTON_104.spawnSpawner( "pumpFluff104", "pumpkin_fluff", 6 )
pumpFluff104.setPos( 600, 705 )
pumpFluff104.setWanderBehaviorForChildren( 50, 200, 3, 7, 100 )
pumpFluff104.setMonsterLevelForChildren( 1.0 )

pumpFluff104.stopSpawning()

//=========================================
// JACK-O-LANTERN SWITCH                   
//=========================================

//TODO : Set things up so that trick or treating only occurs at night (when the pumpkins light up)
// GST test : Turns ON pumpkins at night, turns them OFF during day.
// Pumpkins are locked during the day.
// Pumpkins turn off when activated at night, but immediately turn back ON for the next player. (Light flickers when used.)

jack104 = makeSwitch( "jack104", myRooms.BARTON_104, 700, 665 )
jack104.setRange( 200 )

def startTrickOrTreating = { event ->
	jack104.on()
	if( !event.actor.hasQuestFlag( GLOBAL, "TrickOrTreatActive" ) ) {
		//set a preventative flag that expires in 5 seconds...keeps the player from double-clicking the switch
		event.actor.setQuestFlag( GLOBAL, "TrickOrTreatActive" ) 
		myManager.schedule(5) { event.actor.unsetQuestFlag( GLOBAL, "TrickOrTreatActive" ) }
		//now push the trick or treat dialog
		event.actor.setQuestFlag( GLOBAL, "Z01TrickOrTreat104A" )
		Ellie.pushDialog( event.actor, "trickOrTreat" )
	}
}
	
jack104.whenOff( startTrickOrTreating )

//Variable Initialization & Master Timer
fluffsAlreadySpawned = false
hourCounter = System.currentTimeMillis()
trickTreatTimer()
jackOLanternTimer()

//Every hour, update the hourCounter variable
def trickTreatTimer() {
	myManager.schedule(4200){ hourCounter = System.currentTimeMillis(); trickTreatTimer() }
	
}

//Turn on and unlock Jack O'Lanterns at night. Turn them off during day and lock them.
def jackOLanternTimer() {
//	println "**** GST = ${gst()} ****"
	if( ( gst() > 1800 && gst() <= 2359 ) || ( gst() >=0 && gst() <= 600 ) ) { //trick or treating allowed between 6pm and 6am.
		jack104.unlock()
		jack104.on()
	} else {
		jack104.lock()
		jack104.off()
	}
	myManager.schedule(120) { jackOLanternTimer() } //check time of day again in five minutes
}
	
//=========================================
// DIALOG AND SCRIPT LOGIC                 
//=========================================

//determine how to respond to the player
def trickOrTreat = Ellie.createConversation( "trickOrTreat", true, "Z01TrickOrTreat104A" )

TT1 = [id:1]
TT1.npctext = "Happy Halloween!"
TT1.flag = "!Z01TrickOrTreat104A"
TT1.exec = { event ->
	if( event.player.getPlayerVar( "Z01VisitedEllie" ).longValue() == null || event.player.getPlayerVar( "Z01VisitedEllie" ).longValue() < hourCounter ) {
		def awardList = playerList.clone().intersect( event.player.getCrew() )
		awardList.each{
			//don't allow crewmembers that already got treats from this pumpkin to get them again just because they're in a different crew.
			if( it.getPlayerVar( "Z01VisitedEllie" ).longValue() >= hourCounter ) {
				it.setQuestFlag( GLOBAL, "Z01NotYet" )
				Ellie.pushDialog( it, "notYet" )
			}
		} 
		event.player.setQuestFlag( GLOBAL, "Z01TrickOrTreat104B" )
		Ellie.pushDialog( event.player, "trickOrTreat2" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z01NotYet" )
		Ellie.pushDialog( event.player, "notYet" )
	}
}
trickOrTreat.addDialog( TT1, Ellie )

//If the player has received a treat too recently then don't give him a treat
def notYet = Ellie.createConversation( "notYet", true, "Z01NotYet" )

nYet1 = [id:1]
nYet1.npctext = "Wait a sec! I talked to you already tonight! Try again tomorrow night."
nYet1.flag = "!Z01NotYet"
nYet1.result = DONE
notYet.addDialog( nYet1, Ellie )

//if the player qualifies for a treat or trick, then get this conversation instead
def trickOrTreat2 = Ellie.createConversation( "trickOrTreat2", true, "Z01TrickOrTreat104B" )

TT2 = [id:1]
TT2.playertext = "Trick or Treat?"
TT2.flag = "!Z01TrickOrTreat104B"
TT2.result = DONE
TT2.exec = { event ->
	def awardList = playerList.clone().intersect( event.player.getCrew() )
	awardList.clone().each{ if( it.getPlayerVar( "Z01VisitedEllie" ).longValue() >= hourCounter ) { awardList.remove( it ) } }
	awardList.each{
		//reset the playerVar hourCounter in case people try to exploit by joining the Crew after the first conversation dialog.
		it.setPlayerVar( "Z01VisitedEllie", hourCounter )

		//increment the number of pumpkins clicked counter
		it.addToPlayerVar( "Z01JackOLanternsClicked", 1 )

		//check for Badge Updates
		if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 50 && it.getPlayerVar( "Z01JackOLanternsClicked" ) < 200 && !it.isDoneQuest(276) ) {
			it.updateQuest( 276, "Story-VQS" ) //Update the "Jack O'Smasher" badge
		} else if( it.getPlayerVar( "Z01JackOLanternsClicked" ) >= 200 && !it.isDoneQuest(277) ) {
			it.updateQuest( 277, "Story-VQS" ) //Update the "Pumpkin King" badge
		}

		//determine if it's a trick or treat.
		roll = random( 100 )
		def player = it
		if( roll <= 40 ) { //This value is the chance of a trick. Remainder is the treat percentage.
			player.centerPrint( "You get TRICKED!" )
			trick( player, awardList )
		} else {
			player.centerPrint( "You get a TREAT!" )
			treat( player, awardList )
		}
	}
}
trickOrTreat2.addDialog( TT2, Ellie )

//=========================================
// TRICKS!!!                               
//=========================================
def synchronized trick( player, awardList ) {
	roll =  random( 100 )
	if( fluffsAlreadySpawned == true && roll <= 45 ) {
		roll = random( 46, 100 )
	}
	if( roll <= 45 ) {
		//pumpkin fluff spawn
		player.centerPrint( "From out of the pumpkin come pumpkin-costumed fluffs to attack you!" )
		fluffsAlreadySpawned = true
		fluffSpawnNum = awardList.size()
		spawnPumpkinFluffs()
	} else if( roll > 45 && roll <= 90 ) {
		//give the player a dentist gift
		dentistGift = random( 100 )
		if( dentistGift <= 50 ) {
			dentistGift = "100479"
			player.centerPrint( "Oh man, are you kidding? Dental Floss?!? Who gives dental floss instead of candy?" )
		} else { 
			dentistGift = "100481"
			player.centerPrint( "No way! A toothbrush? WTF?!? It's Halloween, people!" )
		}
		player.grantItem( dentistGift )
	} else if( roll > 90 ) {
		//warp the player
		player.centerPrint( "You feel a strange energy pulling you away to...somewhere else!" )
		roll = random( 100 )
		if( roll <= 50 ) {
			//somewhere in Barton (one of three places)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "BARTON_301", 580, 200 ) //behind statue
			} else if( roll == 2 ) {
				player.warp( "BARTON_4", 1400, 700 ) //near Dusty in corner
			} else if( roll == 3 ) {
				player.warp( "BARTON_1", 1360, 290 ) //in trees above Maestro
			} 
		} else if( roll > 50 && roll <= 80 ) {
			//somewhere in Village Greens (three places, one is nasty)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "VILLAGE_3", 310, 380 ) //in quiet corner at top of map
			} else if( roll == 2 ) {
				player.warp( "VILLAGE_201", 155, 375 ) //Way over by the goof marker at top of west side of map
			} else if( roll == 3 ) {
				player.warp( "VILLAGE_1004", 900, 590 ) //Down by the flamingo spiral at the bottom, right of the map. DANGER!!!
			} 
		} else if( roll > 80 && roll <= 95 ) {
			//danger spots in other zones near Crystals (pull a random one from a map with coords for warping)
			roll = random( 3 )
			if( roll == 1 ) {
				player.warp( "ZENGARDEN_502", 500, 625 ) //in Katsumi's shrine
			} else if( roll == 2 ) {
				player.warp( "BASSKEN_403", 350, 330 ) //By Logan
			} else if( roll == 3 ) {
				player.warp( "Aqueduct_602", 1330, 530 ) //Up by the True Believers
			} 
		} else if( roll > 95 ) {
			player.warp( "Hive_1", 735, 530 ) //YIKES! THE HIVE!!
		}
	}
}

def spawnPumpkinFluffs() {
	if( fluffSpawnNum > 0 ) {
		fluffList << pumpFluff104.forceSpawnNow() //This list starts at position 0, so subtract one from the list size to get the correct list position
		fluffList.get( fluffList.size() - 1 ).addHate( random ( playerList ), 1 ) 
		runOnDeath( fluffList.get( fluffList.size() - 1 ) ) { event -> fluffList.remove( fluffList.get( fluffList.size() - 1 ) ); checkForLoot( event ) }
		fluffSpawnNum -- 
		myManager.schedule(1) { spawnPumpkinFluffs() } 
	} else {
		myManager.schedule(30) { disposalTimer() }
	}
}

def synchronized checkForLoot( event ) {
	def awardList = playerList.clone().intersect( event.killer.getCrew() )
	awardList.each {
		roll = random( 100 )
		if( !it.hasQuestFlag( GLOBAL, "Z01GotOhMyGumball10" ) ) {
			if( roll <= 25 ) { //trick is 50% and pumpkin fluffs are 50% of that and this is 20% of that, so 2.5% chance of getting the item for each fluff killed.
				it.centerPrint( "Happy Halloween! You found the 'Oh My Gumball'!" )
				it.grantItem( "28768" ) //TODO: Fill in with the correct item ID
				it.setQuestFlag( GLOBAL, "Z01GotOhMyGumball10" )
			}
		} else { //if player has already received the 'Oh My Gumball' item
			if( roll <= 25 ) { //only tell them about the 'Oh My Gumball' if they would have normally received it
				it.centerPrint( "You've already found the 'Oh My Gumball', so all you find this time is pumpkin guts." )
			}
		}			
	}
}

def disposalTimer() {
	if( !fluffList.isEmpty() ) {
		fluffList.each{ it.dispose() }
	}
	fluffsAlreadySpawned = false
}

//=========================================
// TREATS!!!                               
//=========================================

commonList = [ 100272, 100289, 100385, 100297, 100289, 100397, 100291, 100262, 100263, 100298, 100388, 100278, 100275, 100283, 100281, 100367, 100411, 100394, 100284, 100405, 100267, 100265, 100285, 100398, 100397, 100376, 100296 ]

uncommonList = [ 100280, 100279, 100270, 100380, 100279, 100384, 100378, 100261, 100271, 100276, 100258, 100268, 100381, 100382, 100282, 100383, 100390, 100299, 100286, 100369, 100400, 100387 ]

recipeList = [ "17766", "17764", "17772", "17758", "17756", "17861", "17857", "17755", "17848", "17849", "17852", "17851", "17850", "17753", "17833", "17831", "17754", "17836", "17835", "17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779" ]

ringList = [ "17716", "17722", "17723", "17737", "17738", "17741", "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "20253" ]

def synchronized treat( player, awardList ) {
	//scale the returned results by relative con levels for each Crew member
	roll = random( 100 )

	lootMultiplier = player.getConLevel() / 10 //the lower the overall CL of a player in relation to player max CL (currently 10), the lower the reward so the trick or treating can be meaningful to all CLs
	//grant gold
	if( roll <= 50 ) {
		goldGrant = ( random( 100000, 300000 ) * lootMultiplier ).intValue()
		player.grantCoins( goldGrant ) 
		player.centerPrint( "It's raining gold!" )
	//grant common item
	} else if( roll > 50 && roll <= 65 ) {
		player.grantQuantityItem( random( commonList ), random(2,5) )
		player.centerPrint( "All right! A loot item!" )
	//grant uncommon item
	} else if( roll > 65 && roll <= 75 ) {
		player.grantQuantityItem( random( uncommonList ), random(1,3) )
		player.centerPrint( "An uncommon loot item!" )
	//grant recipes
	} else if( roll > 75 && roll <= 85 ) {
		player.grantItem( random( recipeList ) )
		player.centerPrint( "Awesome! A recipe!" )
	//grant orbs
	} else if( roll > 85 && roll <= 95 ) {
		orbGrant = ( 10 * lootMultiplier ).intValue()
		player.grantQuantityItem( 100257, orbGrant )
		player.centerPrint( "Look! Charge Orbs!!" )
	//rings
	} else if( roll > 95 ) {
		player.grantRing( random( ringList ), true )
		player.centerPrint( "Woohoo! A Ring!" )
	}
}