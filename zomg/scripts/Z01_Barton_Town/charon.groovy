import com.gaiaonline.mmo.battle.script.*;

// timer start - lanzer
def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
end = startDay + 20
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
    return
} else {
    // place this version of charon on the map
    eventStart = 'HALLOWEEN'
}


def Charon = spawnNPC("[NPC] Charon", myRooms.BARTON_202, 294, 610)
Charon.setRotation( 45 )
Charon.setDisplayName( "[NPC] Charon" )

def charonDefault = Charon.createConversation("charonDefault", true)

def charonDefault1 = [id:1]
charonDefault1.npctext = "Well, look who's here."
charonDefault1.options = []
charonDefault1.options << [text:"Why are you here?", result: 2]
charonDefault1.options << [text:"Bring me to the Jackyard.", result: 4]
charonDefault1.options << [text:"Nevermind, see ya!", result: 99]
charonDefault.addDialog(charonDefault1, Charon)

def charonDefault2 = [id:2]
charonDefault2.npctext = "Since many of you want to spend time in the Jackyard, I've decided to drop by and offer you a lift."
charonDefault2.result = 3
charonDefault.addDialog(charonDefault2, Charon)

def charonDefault3 = [id:3]
charonDefault3.npctext = "So, what will it be?"
charonDefault3.options = []
charonDefault3.options << [text:"Bring me to the Jackyard.", result: 4]
charonDefault3.options << [text:"Nevermind, see ya!", result: 99]
charonDefault.addDialog(charonDefault3, Charon)

def charonDefault16 = [id:4]
charonDefault16.npctext = "Have fun down there.  Go talk to Pancake again when you want to come back."
charonDefault16.exec = { event ->
        event.actor.warp( "TestIsland1_201", 400, 510 )
}
charonDefault16.result = DONE
charonDefault.addDialog(charonDefault16, Charon)

def charonDefault17 = [id:99]
charonDefault17.npctext = "I'll see you later, don't stay alive too long!"
charonDefault17.result = DONE
charonDefault.addDialog(charonDefault17, Charon)
