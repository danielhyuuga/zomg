//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

timerMap = new HashMap()
offlineResetSet = [] as Set

toColiseum = makeGeneric("ColiseumDoorCenter", myRooms.BARTON_4, 380, 740)
toColiseum.setStateful()
toColiseum.setUsable(true)
toColiseum.setMouseoverText("Coliseum Survival Trials")
toColiseum.setRange(200)

//Coliseum Protection transport logic
toColiseum.onUse() { event ->
	if(isPlayer(event.player)) {
		//Check that the player, or a crewmate, is on or has done Gnome General quest
		event.player.getCrew().each() {
			if(!event.player.hasQuestFlag(GLOBAL, "Z01_Coliseum_AllowEntry")) {
				event.player.setQuestFlag(GLOBAL, "Z01_Coliseum_AllowEntry")
			}
		}
		if(event.player.hasQuestFlag(GLOBAL, "Z01_Coliseum_AllowEntry")) {
			//Check that player is not too high a level
			//Check that none of the crew are already in General instance
			if(event.player.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty") == false) {
				if(event.player.getCrewVar("Z33_Coliseum_Survival_DifficultySelectionInProgress") == 0 && event.player.getCrewVar("Z33_Coliseum_Time_DifficultySelectionInProgress") == 0 && event.player.getCrewVar("Z33_Coliseum_Protection_DifficultySelectionInProgress") == 0) {
					//Set crew var to prevent multiple difficulty selection menus
					event.player.setCrewVar("Z33_Coliseum_Survival_DifficultySelectionInProgress", 1)
				
					//Make sure the crew leader is in Village
					if(isInZone(event.player.getTeam().getLeader())) {
						event.player.unsetQuestFlag(GLOBAL, "Z01_Coliseum_AllowEntry")
					
						//Set leader and give them the menu
						leader = event.player.getTeam().getLeader()
						makeMenu()
					
						//Message the crew that the leader is choosing difficulty
						event.player.getCrew().each() {
							if(it != leader) { it.centerPrint("Your Crew Leader is choosing the challenge level for Coliseum Survival Trials.") }
						}
					
						//Schedule decision timeout
						decisionTimer = myManager.schedule(30) {
							event.player.getCrew().each() { it.centerPrint("No choice was made within 30 seconds. Click on Coliseum Survival Trials again.") }
					
							event.player.setCrewVar("Z33_Coliseum_Survival_DifficultySelectionInProgress", 0)
							timerMap.remove(event.player.getTeam().getLeader())
						}
						timerMap.put(event.player.getTeam().getLeader(), decisionTimer)
					} else {
						event.player.getCrew().each() {
							if(it != event.player.getTeam().getLeader()) {
								it.centerPrint("Your Crew Leader must be in Barton Town before you can enter Coliseum Survival Trials.")
							} else {
								it.centerPrint("Your Crew is trying to enter Coliseum Survival Trials. You must be in Barton Town for this.")
							}
						}
						event.player.setCrewVar("Z33_Coliseum_Survival_DifficultySelectionInProgress", 0)
					}
				} else if(event.player.getCrewVar("Z33_Coliseum_Protection_DifficultySelectionInProgress") == 1) {
					if(event.player != event.player.getTeam().getLeader()) {
						event.player.centerPrint("Your Crew Leader is already making a challenge level choice for the Coliseum Protection Trials. One moment, please...")
					}
				} else if(event.player.getCrewVar("Z33_Coliseum_Survival_DifficultySelectionInProgress") == 1) {
					if(event.player != event.player.getTeam().getLeader()) {
						event.player.centerPrint("Your Crew Leader is already making a challenge level choice for the Coliseum Survival Trials. One moment, please...")
					}
				} else if(event.player.getCrewVar("Z33_Coliseum_Time_DifficultySelectionInProgress") == 1) {
					if(event.player != event.player.getTeam().getLeader()) {
						event.player.centerPrint("Your Crew Leader is already making a challenge level choice for the Coliseum Time Trials. One moment, please...")
					}
				}
			} else {
				event.player.centerPrint("The roar of the crowd near knocks you backward as the gate Coliseum gate swings open.")
				event.player.unsetQuestFlag(GLOBAL, "Z01_Coliseum_AllowEntry")
				event.player.warp("BartonColiseum_1", 1190, 850, 6)
			}
		} else {
			event.player.centerPrint("You slam your shoulder against the gate, but it's barred shut.")
		}
	}
}

def synchronized makeMenu() {
	descripString = "You must choose a Challenge Level to enter Coliseum Survival Trials. You have 30 seconds to decide."
	diffOptions = ["Easy", "Normal", "Hard", "Extreme", "Impossible", "Cancel"]
	
	//println "**** trying to create the menu ****"
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty", 1 )
			event.actor.setCrewVar( "Z33_Coliseum_Survival_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty", 2 )
			event.actor.setCrewVar( "Z33_Coliseum_Survival_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty", 3 )
			event.actor.setCrewVar( "Z33_Coliseum_Survival_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Extreme" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Extreme' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				} else {
					it.centerPrint( "You chose the 'Extreme' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty", 4 )
			event.actor.setCrewVar( "Z33_Coliseum_Survival_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Impossible" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Impossible' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				} else {
					it.centerPrint( "You chose the 'Impossible' challenge setting for this scenario. Click Coliseum Survival Trials to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty", 5 )
			event.actor.setCrewVar( "Z33_Coliseum_Survival_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar("Z33_Coliseum_Survival_DifficultySelectionInProgress", 0)
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}