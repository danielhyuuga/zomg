//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

Ian = spawnNPC("BartonIan", myRooms.BARTON_104, 265, 865)
Ian.setDisplayName( "Ian" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Ian.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/ian_strip.png")
}
onQuestStep(114, 2) { event -> event.player.addMiniMapQuestActorName("BartonIan") }

//---------------------------------------------------------------------------------------------------------
//Default conversation - grants Best Served Cold quest                                                     
//---------------------------------------------------------------------------------------------------------
def ianDefault = Ian.createConversation("ianDefault", true, "!QuestStarted_114", "!QuestCompleted_114", "!Z1_Ian_Break")

def default1 = [id:1]
default1.npctext = "Welcome to Barton Boutique, %p! Sure is good to be back."
default1.options = []
default1.options << [text:"Back? Were you gone?", result: 2]
default1.options << [text:"How's life, Ian?", result: 3]
default1.options << [text:"How's your brother?", result: 4]
default1.options << [text:"See you around, Ian.", result: 5]
ianDefault.addDialog(default1, Ian)

def default2 = [id:2]
default2.npctext = "Yeah, I was out for quite a while. Sorry, I figured you'd heard - it was all over the news. Vampires, shootings, family drama... lots of bad memories. I guess you could read about it in the news archives if you really want to know."
default2.options = []
default2.options << [text:"Sounds rough. How are you holding up?", result: 3]
default2.options << [text:"Maybe I'll do that. See you later, Ian.", result: 5]
ianDefault.addDialog(default2, Ian)

def default3 = [id:3]
default3.npctext = "Pretty well, actually. Life's getting back to normal. I've had a few moments where I worry that my past is coming back to haunt me, though."
default3.options = []
default3.options << [text:"Anything I can do to help?", result: 7]
default3.options << [text:"Your past?", result: 12]
default3.options << [text:"Hang in there, buddy.", result: 5]
ianDefault.addDialog(default3, Ian)

def default4 = [id:4]
default4.npctext = "Louie? Last I heard, he was doing great. Staying in Durem and trying to build up some kind of normal life. Contact with Durem has been cut off due to all this stuff with the Animated, but I know he can take care of himself."
default4.options = []
default4.options << [text:"And, how are you?", result: 3]
default4.options << [text:"See you around, Ian.", result: 5]
ianDefault.addDialog(default4, Ian)

def default5 = [id:5]
default5.npctext = "Always a pleasure, %p."
default5.flag = "Z1_Ian_Break"
default5.result = DONE
ianDefault.addDialog(default5, Ian)

def default7 = [id:7]
default7.npctext = "Actually, I could really use a hand. Out in Deadman's Pass, past Bill's Ranch, there's an old mansion on the hill. It used to be the home of a particularly nasty figure from my past, and I've heard rumors that something ugly has taken up roost there."
default7.result = 8
ianDefault.addDialog(default7, Ian)

def default8 = [id:8]
default8.npctext = "Some kind of giant, flying Animated creature has been terrorizing the area, and I'd prefer to see it destroyed. I don't want the owner of the mansion using that thing as his guard dog."
default8.options = []
default8.options << [text:"I'll make sure of it.", result: 9]
default8.options << [text:"Who lives in the old mansion?", result: 11]
default8.options << [text:"Sounds a little out of my league.", result: 10]
ianDefault.addDialog(default8, Ian)

def default9 = [id:9]
default9.npctext = "Really? That would certainly give me some peace of mind. Anything to keep the owner of that mansion from gaining any power. Let me know when you've taken care of it."
default9.quest = 114
default9.result = DONE
ianDefault.addDialog(default9, Ian)

def default10 = [id:10]
default10.npctext = "I don't blame you. I certainly wouldn't want to face that thing. Let me know if you change your mind."
default10.flag = "Z1_Ian_Break"
default10.result = DONE
ianDefault.addDialog(default10, Ian)

def default11 = [id:11]
default11.npctext = "I don't know. Maybe nobody, anymore. But the guy who used to live there was bad, bad news. A vampire. I don't want him using the Animated to his advantage, least of all this monstrosity."
default11.options = []
default11.options << [text:"I'll take care of it.", result: 9]
default11.options << [text:"Sounds a little out of my league.", result: 10]
ianDefault.addDialog(default11, Ian)

def default12 = [id:12]
default12.npctext = "I'm not sure I want to delve into all those bad memories, but my life's kind of an open book. If you check out the news archives, you'll find out all about it."
default12.options = []
default12.options << [text:"Anything I can do to help?", result: 7]
default12.options << [text:"Maybe I'll do that. Bye, Ian.", result: 5]
ianDefault.addDialog(default12, Ian)

//---------------------------------------------------------------------------------------------------------
//Best Served Cold break                                                                                   
//---------------------------------------------------------------------------------------------------------
def ianColdBreak = Ian.createConversation("ianColdBreak", true, "Z1_Ian_Break")

def coldBreak1 = [id:1]
coldBreak1.npctext = "Hi again. How's it going?"
coldBreak1.playertext = "Just fine, thanks. And you?"
coldBreak1.result = 2
ianColdBreak.addDialog(coldBreak1, Ian)

def coldBreak2 = [id:2]
coldBreak2.npctext = "To be honest, I'm still a little worried about what's going on at that mansion. I need to find someone to deal with the Animated up there."
coldBreak2.options = []
coldBreak2.options << [text:"Maybe I can help.", result: 3]
coldBreak2.options << [text:"Good luck with that.", result: 5]
coldBreak2.options << [text:"Better find someone dumber than me. That place is a deathtrap.", result: 6]
ianColdBreak.addDialog(coldBreak2, Ian)

def coldBreak3 = [id:3]
coldBreak3.npctext = "That would be great. That place used to be the home of a particularly nasty figure from my past, and I've heard rumors that something ugly has taken up roost there."
coldBreak3.result = 4
ianColdBreak.addDialog(coldBreak3, Ian)

def coldBreak4 = [id:4]
coldBreak4.npctext = "Some kind of giant, flying Animated creature has been terrorizing the area, and I'd prefer to see it destroyed. I don't want the owner of the mansion using that thing as his guard dog."
coldBreak4.options = []
coldBreak4.options << [text:"I'll take care of it.", result: 7]
coldBreak4.options << [text:"Who lives in the mansion?", result: 8]
coldBreak4.options << [text:"Sounds a little out of my league.", result: 6]
ianColdBreak.addDialog(coldBreak4, Ian)

def coldBreak5 = [id:5]
coldBreak5.npctext = "Thanks. I hope someone takes it out before the vampires can tame it."
coldBreak5.result = DONE
ianColdBreak.addDialog(coldBreak5, Ian)

def coldBreak6 = [id:6]
coldBreak6.npctext = "Ugh, I'm a little creeped out myself. I just hope someone takes out that flying thing before it really hurts someone."
coldBreak6.result = DONE
ianColdBreak.addDialog(coldBreak6, Ian)

def coldBreak7 = [id:7]
coldBreak7.npctext = "Really? That would certainly give me some peace of mind. Anything to keep the owner of that mansion from gaining any power. Let me know when you've taken care of it."
coldBreak7.quest = 114
coldBreak7.flag = "!Z1_Ian_Break"
coldBreak7.result = DONE
ianColdBreak.addDialog(coldBreak7, Ian)

def coldBreak8 = [id:8]
coldBreak8.npctext = "I don't know. Maybe nobody, anymore. But the guy who used to live there was bad, bad news. A vampire, I don't want him using the Animated to his advantage."
coldBreak8.options = []
coldBreak8.options << [text:"I'll take care of it.", result: 7]
coldBreak8.options << [text:"Sounds a little out of my league.", result: 6]
ianColdBreak.addDialog(coldBreak8, Ian)

//---------------------------------------------------------------------------------------------------------
//Best Served Cold active                                                                                  
//---------------------------------------------------------------------------------------------------------
def ianColdActive = Ian.createConversation("ianColdActive", true, "QuestStarted_114:2")

def coldActive1 = [id:1]
coldActive1.npctext = "Hey! Did you get rid of that nasty flying thing yet?"
coldActive1.options = []
coldActive1.options << [text:"Not yet. I'll be back when I do.", result: 2]
coldActive1.options << [text:"Where is it, again?", result: 3]
coldActive1.options << [text:"Why do you need it destroyed?", result: 4]
ianColdActive.addDialog(coldActive1, Ian)

def coldActive2 = [id:2]
coldActive2.npctext = "Good luck out there, %p."
coldActive2.result = DONE
ianColdActive.addDialog(coldActive2, Ian)

def coldActive3 = [id:3]
coldActive3.npctext = "It's on a hill above a place called Deadman's Pass. You can find it out west of Bill's Ranch, through an Iron gate. Let me know when you've taken care of it."
coldActive3.playertext = "I'll go track it down."
coldActive3.result = DONE
ianColdActive.addDialog(coldActive3, Ian)

def coldActive4 = [id:4]
coldActive4.npctext = "It's bad enough that there's some kind of a monster lurking on that hill, but I'm mostly worried about the guy who used to live in the old mansion up there. If he's still around, I wouldn't want him using that kind of beast as his personal guard dog."
coldActive4.options = []
coldActive4.options << [text:"I'll take care of it.", result: 2]
coldActive4.options << [text:"Who lives in the mansion?",result: 5]
ianColdActive.addDialog(coldActive4, Ian)

def coldActive5 = [id:5]
coldActive5.npctext = "I don't know. Maye nobody, anymore. But the guy who used to live there was bad, bad news. A vampire. I don't want him using the Animated to his advantage."
coldActive5.playertext = "I see."
coldActive5.result = DONE
ianColdActive.addDialog(coldActive5, Ian)

//---------------------------------------------------------------------------------------------------------
//Best Served Cold complete                                                                                
//---------------------------------------------------------------------------------------------------------
def ianColdComplete = Ian.createConversation("ianColdComplete", true, "QuestStarted_114:3")

def coldComplete1 = [id:1]
coldComplete1.npctext = "What's the word, %p? Taken care of that beast on the hill yet?"
coldComplete1.playertext = "It's done."
coldComplete1.result = 2
ianColdComplete.addDialog(coldComplete1, Ian)

def coldComplete2 = [id:2]
coldComplete2.npctext = "That's a huge relief. Anything to keep that area free of exploitable resources is a step in the right direction."
coldComplete2.playertext = "That thing was a resource?"
coldComplete2.result = 3
ianColdComplete.addDialog(coldComplete2, Ian)

def coldComplete3 = [id:3]
coldComplete3.npctext = "I don't know if you're familiar with vampires, but they've got a real knack for recruiting minions. If they found a way to take control of the Animated, we'd be in a hell of a lot more trouble than we are now."
coldComplete3.playertext = "I'm glad I could help."
coldComplete3.result = 4
ianColdComplete.addDialog(coldComplete3, Ian)

def coldComplete4 = [id:4]
coldComplete4.npctext = "Me too. In fact, Rufus and I dug into our savings a little and came up with this reward. You definitely earned it."
coldComplete4.playertext = "Thanks!"
coldComplete4.quest = 114
coldComplete4.exec = { event -> event.player.removeMiniMapQuestActorName("BartonIan") }
coldComplete4.result = DONE
ianColdComplete.addDialog(coldComplete4, Ian)