import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//COOKIE QUEST (John's Part)

def John = spawnNPC("BFG-John", myRooms.BARTON_3, 410, 360)
John.setDisplayName( "John" )
John.setRotation( 45 )

//--------------------------------------
//DEFAULT CONVERSATION                  
//--------------------------------------
def JohnNeutral = John.createConversation("JohnNeutral", true, "!QuestStarted_34:2")

def Neutral1 = [id:1]
Neutral1.npctext = "Hello there. What can I do for you?"
Neutral1.result = 2
JohnNeutral.addDialog(Neutral1, John)

def Neutral2 = [id:2]
Neutral2.playertext = "Hmmm..."
Neutral2.options = []
Neutral2.options << [text: "Is this the North Gate?", result: 3]
Neutral2.options << [text: "Who are you?", result: 6]
Neutral2.options << [text: "No thanks!", result: 10]
JohnNeutral.addDialog(Neutral2, John)

def Neutral3 = [id:3]
Neutral3.npctext = "Yes it is. Bass'ken Lake is just a bit north from here, but I really don't recommend that you run out there. It'd be a lot smarter for you to head south and talk to my Commander before you leave the Town walls."
Neutral3.playertext = "Who is your Commander?"
Neutral3.result = 4
JohnNeutral.addDialog(Neutral3, John)

def Neutral4 = [id:4]
Neutral4.npctext = "Leon is the Commander of the Barton Town Regulars. He's down south, near the villages, helping to defend them against the Animated."
Neutral4.playertext = "Thanks!"
Neutral4.result = 5
JohnNeutral.addDialog(Neutral4, John)

def Neutral5 = [id:5]
Neutral5.npctext = "Is there anything else I can help you with?"
Neutral5.result = 2
JohnNeutral.addDialog(Neutral5, John)

def Neutral6 = [id:6]
Neutral6.npctext = "I'm John and I've been a Regular for a year and a half now, ever since I moved here from Durem. Barton Town is a lot quieter than Durem and even though we have to wear this silly armor all the time, the change in pace is more my speed."
Neutral6.playertext = "Yeah, that armor looks like a drag."
Neutral6.result = 7
JohnNeutral.addDialog(Neutral6, John)

def Neutral7 = [id:7]
Neutral7.npctext = "You don't know the half of it. But still, there are a lot of perks to working in this town. I wouldn't trade this armor for Ninja pjs and I'm not cut out for the stiffness of G-Corp, so it's not bad, really."
Neutral7.playertext = "How about joining the Pirates of the Shallow Sea? They've got great uniforms..."
Neutral7.result = 8
JohnNeutral.addDialog(Neutral7, John)

def Neutral8 = [id:8]
Neutral8.npctext = "You've got something there! But no, part of me will always be in Barton Town now. My lot is with the Regulars."
Neutral8.playertext = "Ah ha! That smells like a story!"
Neutral8.result = 9
JohnNeutral.addDialog(Neutral8, John)

def Neutral9 = [id:9]
Neutral9.npctext = "Not a particularly interesting one. I'm just very loyal to my friends."
Neutral9.playertext = "Nice. Thanks for the information, John."
Neutral9.result = DONE
JohnNeutral.addDialog(Neutral9, John)

def Neutral10 = [id:10]
Neutral10.npctext = "All right then. Good day to you!"
Neutral10.result = DONE
JohnNeutral.addDialog(Neutral10, John)


//--------------------------------------
// OLIVIA'S COOKIES (John's Part)       
//--------------------------------------
def JohnCookies = John.createConversation("JohnCookies", true, "QuestStarted_34:2", "!john133")

def Cookies1 = [id:1]
Cookies1.playertext = "Hi there! I've got a present for you!"
Cookies1.result = 2
JohnCookies.addDialog(Cookies1, John)

def Cookies2 = [id:2]
Cookies2.npctext = "Oh yeah? What've you got?"
Cookies2.playertext = "I've got..."
Cookies2.options = []
Cookies2.options << [text:"A wonderful melt-in-your-mouth cookie extravaganza!", result: 3]
Cookies2.options << [text:"A friend of mine baked a few cookies for you hard-working guards!", result: 7]
Cookies2.options << [text:"The heaviest cookies I have EVER carried.", result: 8]
JohnCookies.addDialog(Cookies2, John)

def Cookies3 = [id:3]
Cookies3.npctext = "Oh, let me at them! *chew, groffle, scarp*"
Cookies3.playertext = "You might try to at least chew once every third bite..."
Cookies3.result = 4
JohnCookies.addDialog(Cookies3, John)

def Cookies4 = [id:4]
Cookies4.npctext = "Mmrmrph. Thrztf rr orfalmph."
Cookies4.playertext = "What's that?"
Cookies4.result = 5
JohnCookies.addDialog(Cookies4, John)

def Cookies5 = [id:5]
Cookies5.npctext = "*spits* I said 'Those are awful!' Are you trying to kill me?"
Cookies5.playertext = "Ummm, no. Not me, anyway. They're from Leon's Mom."
Cookies5.result = 6
JohnCookies.addDialog(Cookies5, John)

def Cookies6 = [id:6]
Cookies6.npctext = "LEON'S MOM! Great googly moogly! Don't EVER bring me cookies from that woman again. She's the sweetest mother in the world to Leon, but she's a biological hazard in the kitchen. Holeee catz. I'm lucky to be alive."
Cookies6.playertext = "I...see. Well, I'll just mosey along then."
Cookies6.quest = 34 //push John's part of 34:2
Cookies6.flag = "Z1JohnGoodbye"
Cookies6.exec = { event ->
	John.pushDialog(event.player, "JohnGoodbye")
	event.player.removeMiniMapQuestActorName( "BFG-John" )
}
JohnCookies.addDialog(Cookies6, John)

def Cookies7 = [id:7]
Cookies7.npctext = "A friend of yours, eh? Would that friend be a friend of mine?"
Cookies7.playertext = "I'm sure of it! It's Olivia, Leon's Mom."
Cookies7.result = 6
JohnCookies.addDialog(Cookies7, John)

def Cookies8 = [id:8]
Cookies8.npctext = "And why would I want to eat cookies whose best descriptive adjective would be 'heavy'?"
Cookies8.playertext = "Because they'll stick to your ribs? Or they'll keep your feet on the ground? Or you've been feeling light-headed recently?"
Cookies8.result = 9
JohnCookies.addDialog(Cookies8, John)

def Cookies9 = [id:9]
Cookies9.npctext = "Har har har. Funny, funny. Seriously, where did they come from?"
Cookies9.playertext = "Leon's Mom."
Cookies9.result = 6
JohnCookies.addDialog(Cookies9, John)

//--------------------------------------
//JOHN'S GOODBYE                        
//Remind the player to go back to Olivia
//--------------------------------------
def JohnGoodbye = John.createConversation("JohnGoodbye", true, "!QuestCompleted_34", "Z1JohnGoodbye", "Z1MarthaGoodbye", "Z1WilliamGoodbye")

def Goodbye1 = [id:1]
Goodbye1.npctext = "I see that your plate is empty. If I know Olivia, you'll be sorry if you throw that away. She treasures every dish. Take it back to her though, and she's almost always generous. Maybe she'll give you a cookie."
Goodbye1.playertext = "Yeah, okay. Sorry about that."
Goodbye1.result = 2
JohnGoodbye.addDialog(Goodbye1, John)

def Goodbye2 = [id:2]
Goodbye2.npctext = "At least what's left of my teeth have a permanent protective lining now. Do yourself a favor. DON'T EAT WHAT SHE COOKS!"
Goodbye2.playertext = "Gotcha."
Goodbye2.result = DONE
Goodbye2.exec = { event ->
	event.player.addMiniMapQuestActorName( "Olivia-VQS" )
}
JohnGoodbye.addDialog(Goodbye2, John)

