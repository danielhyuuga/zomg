//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def julian = spawnNPC("BFG-Julian", myRooms.BARTON_2, 620, 535)
julian.setDisplayName( "Julian" )

//-----------------------------------------------------------------------------------------------
//Default conversation for Julian
//-----------------------------------------------------------------------------------------------
def julianDefault = julian.createConversation("julianDefault", true, "!QuestStarted_52:2")

def default1 = [id:1]
default1.npctext = "Stand back! The well is not safe."
default1.options = []
default1.options << [text:"I think I can handle not falling down a well on my own, but thanks for the concern.", result:2]
default1.options << [text:"Why? Did someone fall down it?", result:6]
julianDefault.addDialog(default1, julian)

def default2 = [id:2]
default2.npctext = "Nevertheless, it is my duty to prevent anyone from falling down the well. I must ask again, do not approach the well."
default2.options = []
default2.options << [text:"Your duty? Who told you to do that?", result:3]
default2.options << [text:"And if I approach the well? What will you do about it?", result:4]
julianDefault.addDialog(default2, julian)

def default3 = [id:3]
default3.npctext = "Leon! The Commander of the Guard ordered me, and I shall not falter or fail him."
default3.playertext = "Where can I find this Leon?"
default3.result = 5
julianDefault.addDialog(default3, julian)

def default4 = [id:4]
default4.npctext = "Leon didn't tell me what to do if someone actually approached the well. He only told me to tell them not to. Maybe you should ask him."
default4.playertext = "Very well, where can I find Leon?"
default4.result = 5
julianDefault.addDialog(default4, julian)

def default5 = [id:5]
default5.npctext = "Oh Leon is easy to find. Just follow the road through the south gate."
default5.result = DONE
julianDefault.addDialog(default5, julian)

def default6 = [id:6]
default6.npctext = "Nobody has fallen down the well. Leon ordered me to be sure nobody did and that is what I have done."
default6.result = 7
julianDefault.addDialog(default6, julian)

def default7 = [id:7]
default7.playertext = "Well, tell me where Leon is and I'll tell him what a fine job you're doing."
default7.result = 5
julianDefault.addDialog(default7, julian)

//--------------------------------------------------------------------------------------------------
//Julian's Channel Nine News conversation
//--------------------------------------------------------------------------------------------------
def julianNews = julian.createConversation("julianNews", true, "QuestStarted_52:2")

def news1 = [id:1]
news1.npctext = "Watch out for the well!"
news1.result = 2
julianNews.addDialog(news1, julian)

def news2 = [id:2]
news2.playertext = "Hello Julian, I am %p, reporting for Channel Nine News. Speaking of the well, would you like to comment on the rumors suggesting that someone recently fell down it?"
news2.result = 3
julianNews.addDialog(news2, julian)

def news3 = [id:3]
news3.npctext = "What?! Who told you that? Was it William? That blasted..."
news3.result = 4
julianNews.addDialog(news3, julian)

def news4 = [id:4]
news4.playertext = "So someone did fall down the well?"
news4.result = 5
julianNews.addDialog(news4, julian)

def news5 = [id:5]
news5.npctext = "NO! Nobody has fallen down the well. That's just William trying to discredit me. As if it isn't bad enough that he acts like a penguin, now he's spreading lies and rumors about me!"
news5.result = 6
julianNews.addDialog(news5, julian)

def news6 = [id:6]
news6.playertext = "What about the strange sounds others have been reporting?"
news6.result = 7
julianNews.addDialog(news6, julian)

def news7 = [id:7]
news7.npctext = "Dreams! Hallucinations! Imaginations run wild! NOBODY IS DOWN THAT WELL! Now, if you'll excuse me, I'm very busy."
news7.result = 8
julianNews.addDialog(news7, julian)

def news8 = [id:8]
news8.playertext = "Very well. Thank you, Julian..."
news8.quest = 52
news8.exec = { event -> 
	event.player.removeMiniMapQuestActorName("BFG-Julian")
	event.player.addMiniMapQuestActorName("Cindy-VQS")
}
news8.result = DONE
julianNews.addDialog(news8, julian)