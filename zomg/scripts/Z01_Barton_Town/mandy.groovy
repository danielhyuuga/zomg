import com.gaiaonline.mmo.battle.script.*;

def eventStart = "none"
Date date = new Date();   // given date
Calendar calendar = GregorianCalendar.getInstance(); // creates a new calendar instance
calendar.setTime(date);   // assigns calendar to given date

year = calendar.get(Calendar.YEAR) - 2000
// WHOOPS, it's start Day at 12:59pm, 13 means it'll start at 14th midnight
startDay = 23
debug = 0
// timer end - lanzer
def end = startDay + 14
def endMonth = 10
if (end > 31) {
	endMonth = 11
	end = end - 31
}
if( isLaterThan( "10/$startDay/$year 11:59 pm" ) && !isLaterThan("$endMonth/$end/$year 11:59 pm") ) {
	eventStart = 'HALLOWEEN'
}

def Mandy = spawnNPC("Mandy L", myRooms.BARTON_101, 520, 460)
Mandy.setRotation( 135 )
Mandy.setDisplayName( "Mandy" )

//HALLOWEEN COSTUME
if (eventStart == 'HALLOWEEN') {
	Mandy.setURL("http://s.cdn.gaiaonline.com/images/zomg/halloween2k8/mandy_strip.png")
}
//======================================
// RANDOM CONVERSATIONS                 
//======================================

def Random1 = Mandy.createConversation("Random1", true, "Z0MandyR1" )

def rand1 = [id:1]
rand1.npctext = "Ah, the tranquility of the garden..."
rand1.flag = "!Z0MandyR1"
rand1.exec = { event ->
	selectRandomFlag( event )
}
rand1.result = DONE
Random1.addDialog(rand1, Mandy)
//--------------------
def Random2 = Mandy.createConversation("Random2", true, "Z0MandyR2" )

def rand2 = [id:1]
rand2.npctext = "I think that I shall never see, a poem as lovely as...ack...I'm *so* blocked on this rhyme!"
rand2.flag = "!Z0MandyR2"
rand2.exec = { event ->
	selectRandomFlag( event )
}
rand2.result = DONE
Random2.addDialog(rand2, Mandy)
//--------------------
def Random3 = Mandy.createConversation("Random3", true, "Z0MandyR3" )

def rand3 = [id:1]
rand3.npctext = "Here's hoping nothing here becomes Animated!"
rand3.flag = "!Z0MandyR3"
rand3.exec = { event ->
	selectRandomFlag( event )
}
rand3.result = DONE
Random3.addDialog(rand3, Mandy)
//--------------------
def Random4 = Mandy.createConversation("Random4", true, "Z0MandyR4" )

def rand4 = [id:1]
rand4.npctext = "If you're wondering where the town gates go, then ask any of the guards near a gate!"
rand4.flag = "!Z0MandyR4"
rand4.exec = { event ->
	selectRandomFlag( event )
}
rand4.result = DONE
Random4.addDialog(rand4, Mandy)

//======================================
// LOGIC FLOW FOR RANDOM LIST           
//======================================

myManager.onEnter( myRooms.BARTON_101 ) { event ->
	if( isPlayer( event.actor) ) {
		convoFlag = random( randomFlag )
		event.actor.setQuestFlag( GLOBAL, convoFlag )
	}
}

myManager.onExit( myRooms.BARTON_101 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, convoFlag )
	}
}

randomFlag = []
randomFlag << "Z0MandyR1"
randomFlag << "Z0MandyR2"
randomFlag << "Z0MandyR3"
randomFlag << "Z0MandyR4"


def selectRandomFlag( event ) {
	lastFlag = convoFlag
	convoFlag = random( randomFlag )
	if( lastFlag == convoFlag ) {
		selectRandomFlag( event )
	}
	event.player.setQuestFlag( GLOBAL, convoFlag )
}
