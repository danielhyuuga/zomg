import com.gaiaonline.mmo.battle.script.*;

def newsSoundTrigger = "newsSoundTrigger"
myRooms.BARTON_304.createTriggerZone( newsSoundTrigger, 280, 470, 825, 800 )

myManager.onTriggerIn(myRooms.BARTON_304, newsSoundTrigger) { event ->
	if( isPlayer( event.actor ) ) {
		sound("newsroomSound").toPlayer( event.actor )
	}
}