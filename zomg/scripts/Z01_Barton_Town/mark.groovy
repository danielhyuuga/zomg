//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def Mark = spawnNPC("BFG-Mark", myRooms.BARTON_103, 610, 190)
Mark.setDisplayName( "Mark" )

markRingList = [ "Z6_RingLake1", "Z6_RingLake2", "Z6_RingLake3", "Z6_RingLake4" ]
markRingFlag = random( markRingList )

/*def markHalloween = Mark.createConversation("markHalloween", true)

def markHalloween1 = [id:1]
markHalloween1.npctext = "Well, hello there %p. I'm Mark."
markHalloween1.playertext = "Nice to meet you Mark. What do you do?"
markHalloween1.result = 2
markHalloween.addDialog(markHalloween1, Mark)

def markHalloween2 = [id:2]
markHalloween2.npctext = "I work for the Barton Guard. With Leon out of the city I manage the daily tasks of the Barton Regulars."
markHalloween2.playertext = "Awesome! Anything I can help with?"
markHalloween2.result = 3
markHalloween.addDialog(markHalloween2, Mark)

def markHalloween3 = [id:3]
markHalloween3.npctext = "Oh, well, I suppose ordinarily there would be, but I'm off-duty until after the Halloween events! You'll have to check back after they're through."
markHalloween3.playertext = "Uhm, okay. I'll do that."
markHalloween3.result = DONE
markHalloween.addDialog(markHalloween3, Mark)*/

//---------------------------------------------------------------
//Init conversation                                              
//---------------------------------------------------------------
def markInit = Mark.createConversation("markInit", true, "!Z1_Mark_Init", "!QuestStarted_256", "!QuestCompleted_256")

def markInit1 = [id:1]
markInit1.npctext = "You there! Stand tall and be recognized! Wait, I don't know you, do I?"
markInit1.playertext = "No. My name is %p."
markInit1.result = 2
markInit.addDialog(markInit1, Mark)

def markInit2 = [id:2]
markInit2.npctext = "And I am Mark. Left alone to manage the daily tasks of the Barton Regulars in the absence of our commander, Leon."
markInit2.options = []
markInit2.options << [text:"Who?", result: 3]
markInit2.options << [text:"What sort of tasks are those?", result: 4]
markInit2.options << [text:"That sounds...incredibly mundane.", result: 5]
markInit.addDialog(markInit2, Mark)

def markInit3 = [id:3]
markInit3.npctext = "Leon, the Captain of the Barton Regulars. You've never heard of him? Nevermind, it doesn't matter. What does matter is that I need help!"
markInit3.options = []
markInit3.options << [text:"Okay. Why don't you tell me what sort of tasks you need help with?", result: 4]
markInit3.options << [text:"Sorry, but I'm really not interested in some boring guard task.", result: 5]
markInit.addDialog(markInit3, Mark)

def markInit4 = [id:4]
markInit4.npctext = "Oh all sorts. Repelling the Animated, gathering information, rescuing Bartonian citizens, and more! We have guards stationed all over the Gaia that are in need of another hand or two...even if they're a finger or two short."
markInit4.options = []
markInit4.options << [text:"Alright, you have my attention. Let's get specific.", result: 6]
markInit4.options << [text:"If you have guards stationed why don't they just take care of these tasks themselves?", result: 7]
markInit.addDialog(markInit4, Mark)

def markInit5 = [id:5]
markInit5.npctext = "But they're not boring or mundane at all...quite the opposite, in fact!"
markInit5.options = []
markInit5.options << [text:"Really? What sort of tasks are they?", result: 4]
markInit5.options << [text:"Nice try, but you'll have to find some other sucker to be your errand boy.", result: 8]
markInit.addDialog(markInit5, Mark)

def markInit6 = [id:6]
markInit6.npctext = "Well... it's a funny story, actually. Things have been pretty quiet around Barton Town lately, so I only have one task. There are Regulars posted throughout Gaia, though, so keep an eye out for them."
markInit6.options = []
markInit6.options << [text:"I might as well start with you. What do you need done?", result: 11]
markInit6.options << [text:"Cool, I'm gonna start looking for the other Regulars.", result: 8]
markInit.addDialog(markInit6, Mark)

def markInit7 = [id:7]
markInit7.npctext = "Between the guards Leon took with him down to the Village and the additional postings he's ordered 'to protect Barton Town' there's far more to be done outside of Barton Town than the few stationed around the outskirts of Gaia can manage. They're just stretched too thin."
markInit7.options = []
markInit7.options << [text:"Alright, tell me what you need help with.", result: 6]
markInit7.options << [text:"Sounds like a problem for you, not for me. Best of luck.", result: 8]
markInit.addDialog(markInit7, Mark)

def markInit8 = [id:8]
markInit8.npctext = "Very well. I doubt you are worth of the honor of wearing the armor of a guard of Barton Town anyway."
markInit8.options = []
markInit8.options << [text:"Wearing the armor? You mean if I help you?", result: 9]
markInit8.options << [text:"Armor? Ewww! I have too much style to prance about like a tin-can waiting to rust.", result: 10]
markInit.addDialog(markInit8, Mark)

def markInit9 = [id:9]
markInit9.npctext = "That's right. I'm so desperate for help that I'll outfit you with a full suit of Barton Regulars armor!"
markInit9.options = []
markInit9.options << [text:"Whoa, that's awesome! What kind of tasks do you need help with?", result: 4]
markInit9.options << [text:"As if someone as stylish as me would be enticed by the prospect of a metal suit. Pfft!", result: 10]
markInit.addDialog(markInit9, Mark)

def markInit10 = [id:10]
markInit10.npctext = "Then we have nothing more to talk about."
markInit10.flag = "Z1_Mark_Init"
markInit10.result = DONE
markInit.addDialog(markInit10, Mark)

def markInit11 = [id:11]
markInit11.npctext = "I'm embarrassed to admit my task is not explicitly a task of the Barton guard. I was on patrol near the Bass'ken Lake recently. When I returned to Barton Town I discovered that I had lost my ring!"
markInit11.playertext = "Let me guess, you want me to look for it?"
markInit11.result = 12
markInit.addDialog(markInit11, Mark)

def markInit12 = [id:12]
markInit12.npctext = "You catch on quickly. I'm sure I must have lost it somewhere on the shore, but I'm not sure where exactly. Don't worry, though, if you're strong enough to manage the Animated there it won't take you long at all to find it."
markInit12.options = []
markInit12.options << [text:"Okay, Mark. I'll help you out.", result: 13]
markInit12.options << [text:"Maybe later. Tell me what needed help with again.", result: 6]
markInit12.options << [text:"I can't help right now. Later.", result: 8]
markInit.addDialog(markInit12, Mark)

def markInit13 = [id:13]
markInit13.npctext = "Great! I hope it doesn't take too long."
markInit13.quest = 256
markInit13.flag = "Z1_Mark_QuestActive"
markInit13.exec = { event ->
	markRingFlag = random( markRingList )
	event.player.setQuestFlag(GLOBAL, markRingFlag )
	/*if( !event.player.isOnQuest( 272 ) ) {
		event.player.updateQuest( 272, "BFG-Mark" )
	}*/
	if(!event.player.hasQuestFlag(GLOBAL, "marksRingStarted1")) {
		event.player.setQuestFlag(GLOBAL, "marksRingStarted1")
	}
}
markInit13.result = DONE
markInit.addDialog(markInit13, Mark)

//---------------------------------------------------------------
//Conversation if player declined to help after Init             
//---------------------------------------------------------------
def markDecline = Mark.createConversation("markDecline", true, "Z1_Mark_Init")

def markDecline1 = [id:1]
markDecline1.npctext = "I see you're back, %p. I still have tasks for you. Interested?"
markDecline1.options = []
markDecline1.options << [text:"Maybe. Can you remind me what tasks you needed help with?", result: 2]
markDecline1.options << [text:"Er, no. I didn't even mean to start a conversation with you.", result: 3]
markDecline.addDialog(markDecline1, Mark)

def markDecline2 = [id:2]
markDecline2.npctext = "I have but one humble task myself, but you may also encounter other Regulars as you travel throughout Gaia."
markDecline2.options = []
markDecline2.options << [text:"I might as well start with you. What do you need done?", result: 4]
markDecline2.options << [text:"Right. I'll keep my eye out for the other Regulars. Bye.", result: DONE]
markDecline.addDialog(markDecline2, Mark)

def markDecline3 = [id:3]
markDecline3.npctext = "Then why are you here?"
markDecline3.result = DONE
markDecline.addDialog(markDecline3, Mark)

def markDecline4 = [id:4]
markDecline4.npctext = "I'm embarrassed to admit my task is not explicitly the task of the Barton guard. I was on patrol near the Bass'ken Lake recently. When I returned to Barton Town I discovered that I had lost my ring!"
markDecline4.playertext = "Let me guess, you want me to look for it?"
markDecline4.result = 5
markDecline.addDialog(markDecline4, Mark)

def markDecline5 = [id:5]
markDecline5.npctext = "You catch on quickly. I'm sure I must have lost it somewhere on the shore, but I'm not sure where exactly. Don't worry, though, if you're strong enough to manage the Animated there it won't take you long at all to find it."
markDecline5.options = []
markDecline5.options << [text:"Okay, Mark. I'll help you out.", result: 6]
markDecline5.options << [text:"Maybe later. Tell me what needed help with again.", result: 2]
markDecline5.options << [text:"I can't help right now. Later.", result: 3]
markDecline.addDialog(markDecline5, Mark)

def markDecline6 = [id:6]
markDecline6.npctext = "Great! I hope it doesn't take too long."
markDecline6.quest = 256
markDecline6.flag = ["Z1_Mark_QuestActive", "!Z1_Mark_Init"]
markDecline6.exec = { event ->
	markRingFlag = random( markRingList )
	event.player.setQuestFlag(GLOBAL, markRingFlag )
	/*if( !event.player.isOnQuest( 272 ) ) {
		event.player.updateQuest( 272, "BFG-Mark" )
	}*/
	if(!event.player.hasQuestFlag(GLOBAL, "marksRingStarted1")) {
		event.player.setQuestFlag(GLOBAL, "marksRingStarted1")
	}
}
markDecline6.result = DONE
markDecline.addDialog(markDecline6, Mark)

//---------------------------------------------------------------
//Conversation if player has accepted ring quest                 
//---------------------------------------------------------------
def markRingActive = Mark.createConversation("markRingActive", true, "QuestStarted_256:2")

def markRingActive1 = [id:1]
markRingActive1.npctext = "%p, I wasn't expecting to see you until you had completed the tasks at hand. Do you require more information?"
markRingActive1.options = []
markRingActive1.options << [text:"Yes, I do.", result: 2]
markRingActive1.options << [text:"No. I was just saying hello. I'll get back to work shortly.", result: 3]
markRingActive.addDialog(markRingActive1, Mark)

def markRingActive2 = [id:2]
markRingActive2.npctext = "Okay, what can I tell you about?"
markRingActive2.options = []
markRingActive2.options << [text:"How do I get to Bass'ken Lake?", result: 4]
markRingActive2.options << [text:"Where is your freaking ring?!", result: 5]
markRingActive.addDialog(markRingActive2, Mark)

def markRingActive3 = [id:3]
markRingActive3.npctext = "See that you do."
markRingActive3.result = DONE
markRingActive.addDialog(markRingActive3, Mark)

def markRingActive4 = [id:4]
markRingActive4.npctext = "Simple, just walk out of Barton Town through the north gate and continue along the path until you see water."
markRingActive4.playertext = "Cool, I can do that."
markRingActive4.result = DONE
markRingActive.addDialog(markRingActive4, Mark)

def markRingActive5 = [id:5]
markRingActive5.npctext = "If I knew that it wouldn't be lost, would it?"
markRingActive5.playertext = "But you don't understand, I've looked everyhwere!"
markRingActive5.result = 6
markRingActive.addDialog(markRingActive5, Mark)

def markRingActive6 = [id:6]
markRingActive6.npctext = "If you'd look everywhere you'd have found it. It's around the lake, somewhere. Keep trying."
markRingActive6.result = DONE
markRingActive.addDialog(markRingActive6, Mark)

//---------------------------------------------------------------
//Conversation if player has completed ring quest                
//---------------------------------------------------------------
def markRingComplete = Mark.createConversation("markRingComplete", true, "QuestStarted_256:3")

def markRingComplete1 = [id:1]
markRingComplete1.playertext = "I found a ring in the lake."
markRingComplete1.result = 2
markRingComplete.addDialog(markRingComplete1, Mark)

def markRingComplete2 = [id:2]
markRingComplete2.npctext = "Let me see it!"
markRingComplete2.playertext = "Okay, take a look."
markRingComplete2.result = 3
markRingComplete.addDialog(markRingComplete2, Mark)

def markRingComplete3 = [id:3]
markRingComplete3.npctext = "Hmmm, this isn't my ring. It's far too heavy. This is terrible. *sigh*"
markRingComplete3.playerext = "I thought that ring was a little big for you. Sorry, Mark, but I did try."
markRingComplete3.result = 4
markRingComplete.addDialog(markRingComplete3, Mark)

def markRingComplete4 = [id:4]
markRingComplete4.npctext = "Well, thanks %p. Who knows, maybe it'll turn up somewhere. Keep an eye out if you're passing through Bass'ken."
markRingComplete4.playertext = "Will do."
markRingComplete4.quest = 256
markRingComplete4.exec = { event ->
	event.player.removeMiniMapQuestActorName("BFG-Mark")
	event.player.setQuestFlag(GLOBAL, "marksRingCompleted")
	event.player.grantItem("17859")
	event.player.grantQuantityItem(100257, 5)
}
markRingComplete4.flag = "Z1_Mark_QuestRingComplete"
markRingComplete4.result = DONE
markRingComplete.addDialog(markRingComplete4, Mark)

//---------------------------------------------------------------
//Ring has already been completed                                
//---------------------------------------------------------------
def markEnd = Mark.createConversation("markEnd", true, "QuestCompleted_256")

def markEnd1 = [id:1]
markEnd1.npctext = "Hello again, %p. What can I do for you?"
markEnd1.playertext = "Any new work around Barton Town?"
markEnd1.result = 2
markEnd.addDialog(markEnd1, Mark)

def markEnd2 = [id:2]
markEnd2.npctext = "Nope, not yet, but don't forget the other Regulars stationed throughout Gaia. They may need your help."
markEnd2.playertext = "I'll keep that in mind."
markEnd2.result = DONE
markEnd.addDialog(markEnd2, Mark)