import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_301.spawnSpawner("spawner1", "taiko_drum", 3)
spawner1.setPos(370, 250)
spawner1.setWanderBehaviorForChildren( 75, 200, 5, 10, 500)
spawner1.setWaitTime( 30, 50 )
spawner1.setMonsterLevelForChildren( 3.2 )

def spawner2 = myRooms.ZENGARDEN_301.spawnSpawner("ZG_301_Spawner2", "cherry_fluff", 2)
spawner2.setPos(940, 100)
spawner2.setWanderBehaviorForChildren( 75, 150, 2, 5, 300)
spawner2.setWaitTime( 50, 70 )
spawner2.setMonsterLevelForChildren( 3.2 )

def spawner3 = myRooms.ZENGARDEN_301.spawnSpawner("ZG_301_Spawner3", "cherry_fluff", 2)
spawner3.setPos(820, 610)
spawner3.setWanderBehaviorForChildren( 75, 150, 2, 5, 300)
spawner3.setWaitTime( 50, 70 )
spawner3.setMonsterLevelForChildren( 3.2 )

//STARTUP LOGIC
spawner1.spawnAllNow()
spawner2.spawnAllNow()
spawner3.spawnAllNow()