import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_601.spawnSpawner("spawner1", "cherry_fluff", 2)
spawner1.setPos(920, 200)
spawner1.setWanderBehaviorForChildren( 50, 120, 2, 5, 200)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 2.9 )

def spawner2 = myRooms.ZENGARDEN_601.spawnSpawner("spawner2", "cherry_fluff", 2)
spawner2.setPos(900, 580)
spawner2.setWanderBehaviorForChildren( 50, 120, 2, 5, 200)
spawner2.setWaitTime( 50, 70 )
spawner2.setMonsterLevelForChildren( 2.9 )

//STARTUP LOGIC
spawner1.spawnAllNow()
spawner2.spawnAllNow()
