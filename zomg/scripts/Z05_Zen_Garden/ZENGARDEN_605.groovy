import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_605.spawnSpawner("spawner1", "cherry_fluff", 3) 
spawner1.setPos(800, 500)
spawner1.setWanderBehaviorForChildren( 25, 75, 2, 5, 200)
spawner1.setWaitTime( 30, 50 )
spawner1.setMonsterLevelForChildren( 3.1 )

//STARTUP LOGIC
spawner1.spawnAllNow()