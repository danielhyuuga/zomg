import com.gaiaonline.mmo.battle.script.*;

//---------------------------------------------------------
// THE NINJA BELL (Ringing the bell)                       
//---------------------------------------------------------

bellToMeadow = makeSwitch( "ninjaBell", myRooms.ZENGARDEN_101, 720, 360 )
bellToMeadow.unlock()
bellToMeadow.setMouseoverText("The Ninja Bell")

def bellTap = { event ->
	if( event.actor.isOnQuest(48) || event.actor.isDoneQuest(48) ) {  //if the player has received the crystal rod from Katsumi, then...
		//play the sound of the bell ringing
		sound("bellSound").toZone()
		//transport the player to the Ninja Meadow
		event.actor.warp( "Ninja_1", 845, 540 )
		bellToMeadow.lock()
		myManager.schedule(2) { bellToMeadow.off(); bellToMeadow.unlock() } //turn the switch back on quickly so it can be used again.
	} else {
		//play a "clank" sound so players can hear that they need something to make the bell ring
		sound("clank").toZone()
		bellToMeadow.lock()
		myManager.schedule(2) { bellToMeadow.off(); bellToMeadow.unlock() }
	}
}

bellToMeadow.whenOn( bellTap )








