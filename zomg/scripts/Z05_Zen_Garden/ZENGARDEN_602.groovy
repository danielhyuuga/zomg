import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_602.spawnSpawner("spawner1", "cherry_fluff", 2) 
spawner1.setPos(270, 610)
spawner1.setWanderBehaviorForChildren( 25, 75, 2, 5, 200)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 3.0 )

//STARTUP LOGIC
spawner1.spawnAllNow()