
//--------------------------------------------
// WARP CRYSTAL TO THE NULL CHAMBER           
//--------------------------------------------

toNullChamber = makeSwitch( "toNullChamber", myRooms.ZENGARDEN_502, 580, 520 )
toNullChamber.unlock()
toNullChamber.setRange( 200 )
toNullChamber.setMouseoverText("To the Null Chamber")

onClickCrystal = new Object()

def goToNullChamber = { event ->
	synchronized( onClickCrystal ) {
		toNullChamber.off()
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "nullChamber1_1", 500, 710, 6 )
		event.actor.setQuestFlag( GLOBAL, "Z21GardensWarpOkay" )
		event.actor.setQuestFlag( GLOBAL, "Z21ComingFromOutside" )
	}
}

toNullChamber.whenOn( goToNullChamber )
