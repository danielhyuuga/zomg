import com.gaiaonline.mmo.battle.script.*;

//==========================================================================================
// THE BATTLE OF THREE BRIDGES                                                              
// When Kin gives the quest flag to the player to start this quest, spawns will be          
// triggered (the same way that Kat's Kokeshi was spawned via quest flag).                  
//                                                                                          
// The spawns only occur when a player with the quest flag appears in the room with the     
// correct bridge. The lieutenant will spawn with minions around it, one on each bridge in  
// succession. Killing one enables the flag for the next bridge.                            
//                                                                                          
// Finishing all three encounters sets the return flag encounter for Kin.                   
//==========================================================================================

//=============================================
//THE FIRST BRIDGE                             
//=============================================

//Trigger zone for the First Ambush
def ambushTriggerOne = "ambushTriggerOne"
myRooms.ZENGARDEN_404.createTriggerZone(ambushTriggerOne, 550, 175, 785, 390)

//FIRST BRIDGE SPAWNER DEFINITIONS
//top of bridge spawn
firstBridge1 = myRooms.ZENGARDEN_404.spawnSpawner("firstBridge1", "kokeshi_doll_nexus", 1)
firstBridge1.setPos(624, 65)
firstBridge1.setHateRadiusForChildren( 2000 )
firstBridge1.setCFH( 400 )
firstBridge1.setInitialMoveForChildren("ZENGARDEN_404", 670, 290)
firstBridge1.setMonsterLevelForChildren( 3.9 )

//bottom of bridge spawn
firstBridge2 = myRooms.ZENGARDEN_404.spawnSpawner("firstBridge2", "kokeshi_doll", 10)
firstBridge2.setPos(1000, 632)
firstBridge2.setHateRadiusForChildren( 2000 )
firstBridge2.setCFH( 400 )
firstBridge2.setInitialMoveForChildren("ZENGARDEN_404", 670, 290)
firstBridge2.setMonsterLevelForChildren( 3.9 )

//extra grunt spawner
firstBridge3 = myRooms.ZENGARDEN_404.spawnSpawner("firstBridge3", "kokeshi_doll", 10)
firstBridge3.setPos(624, 65)
firstBridge3.setHateRadiusForChildren( 2000 )
firstBridge3.setCFH( 400 )
firstBridge3.setInitialMoveForChildren("ZENGARDEN_404", 670, 290)
firstBridge3.setMonsterLevelForChildren( 3.9 )

//stop the spawns until ready
firstBridge1.stopSpawning()
firstBridge2.stopSpawning()
firstBridge3.stopSpawning()

//alliances
firstBridge1.allyWithSpawner( firstBridge2 )
firstBridge1.allyWithSpawner( firstBridge3 )
firstBridge2.allyWithSpawner( firstBridge3 )

firstTime = true

//Count the players in Room 404
playersInZ4_404 = []
numToSpawnZ4_404 = 0

myManager.onEnter(myRooms.ZENGARDEN_404) { event ->
	if( isPlayer(event.actor) ) {
		playersInZ4_404 << event.actor
		event.actor.unsetQuestFlag( GLOBAL, "Z5DoNotRepeat" )
	}
}

myManager.onExit(myRooms.ZENGARDEN_404) { event ->
	if ( isPlayer(event.actor) ) {
		playersInZ4_404.remove( event.actor ) 
	}
}

//what to do when you step into the trigger zone
myManager.onTriggerIn(myRooms.ZENGARDEN_404, ambushTriggerOne) { event ->
	//DEBUG !!!!!!!!!!!!!!!!!!!!!
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z5DoNotRepeat" )
	}
	//DEBUG !!!!!!!!!!!!!!!!!!!!!
	if(isPlayer(event.actor) && event.actor.isOnQuest(62, 2) && !event.actor.hasQuestFlag(GLOBAL, "Z5DoNotRepeat") && firstBridge1.spawnsInUse() == 0 ) {
		event.actor.setQuestFlag(GLOBAL, "Z5DoNotRepeat")
		event.actor.say( "What's that feeling? As if I'm being pressed in from all sides..." )
		myManager.schedule(3) { playerOnQuest = event.actor; firstAmbush() }
	}
}

hateCollector = []

//Ambush Code
def synchronized firstAmbush() {
	
	if( myRooms.ZENGARDEN_404.getSpawnTypeCount( "kokeshi_doll_nexus" ) < 1 ) {
		lieutenant1 = firstBridge1.forceSpawnNow()
		lieutenant1.addHate( playerOnQuest, 1 )
		playerOnQuest.say("It's an ambush!")
		lieutenant1.say("Be a doll and stay right there, would you? I'm a big fan!")
		runOnDeath(lieutenant1, { event ->
			hateCollector.addAll( event.actor.getHated() )
			//look for someone on the Hate Collector list that is on the quest and then have them say something
			hateCollector.each{ 
				if( it.isOnQuest( 62, 2 ) ) {
					it.say( "They must have come from one of those 'ripples' Kin was talking about. I'd better keep patrolling.")
					it.updateQuest( 62, "Kin No Mask-VQS" )
				}
			}			
			playersInZ4_404.clone().each{ it.unsetQuestFlag(GLOBAL, "Z5DoNotRepeat") }
			hateCollector = []
		} )
	}
	if(playersInZ4_404.size() > 6) {
		numToSpawnZ4_404 = 7
	} else {
		numToSpawnZ4_404 = playersInZ4_404.size() + 1 
	}
	spawnBridgeOneGrunts()
}

//When the event is spawned, spawn this many grunts, to compensate for the number of players in the room.
def spawnBridgeOneGrunts() {
	if( numToSpawnZ4_404 > 0 && myRooms.ZENGARDEN_404.getSpawnTypeCount( "kokeshi_doll" ) <= numToSpawnZ4_404 ) {
		if( numToSpawnZ4_404 % 2 == 0 ) {
			fb2 = firstBridge2.forceSpawnNow()
			fb2.addHate( playerOnQuest, 1 )
			numToSpawnZ4_404 --
			myManager.schedule(1) { spawnBridgeOneGrunts() }
		} else {
			fb3 = firstBridge3.forceSpawnNow()
			fb3.addHate( playerOnQuest, 1 )
			numToSpawnZ4_404 --
			myManager.schedule(1) { spawnBridgeOneGrunts() }
		}
	}
}


//=============================================
//THE SECOND BRIDGE                            
//=============================================

//Trigger zone for the Second Ambush
def ambushTriggerTwo = "ambushTriggerTwo"
myRooms.ZENGARDEN_302.createTriggerZone(ambushTriggerTwo, 650, 300, 980, 500)

//SECOND BRIDGE SPAWNER DEFINITIONS
//top of bridge spawn
secondBridge1 = myRooms.ZENGARDEN_302.spawnSpawner("secondBridge1", "taiko_drum_LT", 1)
secondBridge1.setPos(425, 80)
secondBridge1.setHateRadiusForChildren( 2000 )
secondBridge1.setCFH( 400 )
secondBridge1.setInitialMoveForChildren("ZENGARDEN_302", 850, 490)
secondBridge1.setMonsterLevelForChildren( 4.0 )

//bottom of bridge spawn
secondBridge2 = myRooms.ZENGARDEN_302.spawnSpawner("secondBridge2", "taiko_drum", 10)
secondBridge2.setPos(804, 585)
secondBridge2.setHateRadiusForChildren( 2000 )
secondBridge2.setCFH( 400 )
secondBridge2.setInitialMoveForChildren("ZENGARDEN_302", 730, 533)
secondBridge2.setMonsterLevelForChildren( 4.0 )

//extra grunt spawner
secondBridge3 = myRooms.ZENGARDEN_302.spawnSpawner("secondBridge3", "taiko_drum", 10)
secondBridge3.setPos(425, 80)
secondBridge3.setHateRadiusForChildren( 2000 )
secondBridge3.setCFH( 400 )
secondBridge3.setInitialMoveForChildren("ZENGARDEN_302", 850, 490)
secondBridge3.setMonsterLevelForChildren( 4.0 )

//stop the spawns until ready
secondBridge1.stopSpawning()
secondBridge2.stopSpawning()
secondBridge3.stopSpawning()

//alliances
secondBridge1.allyWithSpawner( secondBridge2 )
secondBridge1.allyWithSpawner( secondBridge3 )
secondBridge2.allyWithSpawner( secondBridge3 )

//Count the players in Room 302
playersInZ4_302 = []
numToSpawnZ4_302 = 0

myManager.onEnter(myRooms.ZENGARDEN_302) { event ->
	if( isPlayer(event.actor) ) {
		playersInZ4_302 << event.actor
		event.actor.unsetQuestFlag( GLOBAL, "Z5DoNotRepeat" )
	}
}

myManager.onExit(myRooms.ZENGARDEN_302) { event ->
	if ( isPlayer(event.actor) ) {
		playersInZ4_302.remove( event.actor )
	}
}

//Create the second ambush when the player enters the trigger zone
myManager.onTriggerIn(myRooms.ZENGARDEN_302, ambushTriggerTwo) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(62, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z5DoNotRepeat") && secondBridge1.spawnsInUse() == 0 ) {
		event.actor.setQuestFlag(GLOBAL, "Z5DoNotRepeat")
		event.actor.say( "There it is again...like walking through pudding..." )
		myManager.schedule(3) { playerOnQuest = event.actor; secondAmbush() }
	}
}

//Ambush Code
def synchronized secondAmbush() {

	if( myRooms.ZENGARDEN_302.getSpawnTypeCount( "taiko_drum_LT" ) < 1 ) {
		lieutenant2 = secondBridge1.forceSpawnNow()
		lieutenant2.addHate( playerOnQuest, 1 )
		playerOnQuest.say("It's Taiko Drums this time!")
		lieutenant2.say("Prepare for a beat-ing!")
		runOnDeath(lieutenant2, { event ->
			hateCollector.addAll( event.actor.getHated() )
			//look for someone on the Hate Collector list that is on the quest and then have them say something
			hateCollector.each{ 
				if( it.isOnQuest( 62, 3 ) ) {
					it.say( "Hmmm...that was harder than last time. I hope this isn't a trend..." )
					it.updateQuest( 62, "Kin No Mask-VQS" )
				}
			}			
			playersInZ4_302.clone().each{ it.unsetQuestFlag(GLOBAL, "Z5DoNotRepeat") }
			hateCollector = []
		} )
	}
	if(playersInZ4_302.size() > 6) {
		numToSpawnZ4_302 = 7
	} else {
		numToSpawnZ4_302 = playersInZ4_302.size() + 1
	}
	spawnBridgeTwoGrunts()
}

//When the event is spawned, spawn this many grunts, to compensate for the number of players in the room.
def spawnBridgeTwoGrunts() {
	if( numToSpawnZ4_302 > 0  && myRooms.ZENGARDEN_302.getSpawnTypeCount( "taiko_drum" ) <= numToSpawnZ4_302 ) {
		if( numToSpawnZ4_302 % 2 == 0 ) {
			sb2 = secondBridge2.forceSpawnNow()
			if(!playerOnQuest == null) { sb2.addHate( playerOnQuest, 1 ) }
			numToSpawnZ4_302 --
			myManager.schedule(1) { spawnBridgeTwoGrunts() }
		} else {
			sb3 = secondBridge3.forceSpawnNow()
			if(!playerOnQuest == null) { sb3.addHate( playerOnQuest, 1 ) }
			numToSpawnZ4_302 --
			myManager.schedule(1) { spawnBridgeTwoGrunts() }
		}
	}
}


//=============================================
//THE FINAL (third) BRIDGE                             
//=============================================

//Trigger zone for the Third Ambush
def ambushTriggerThree = "ambushTriggerThree"
myRooms.ZENGARDEN_303.createTriggerZone(ambushTriggerThree, 520, 465, 800, 525)

//SECOND BRIDGE SPAWNER DEFINITIONS
//top of bridge spawn
thirdBridge1 = myRooms.ZENGARDEN_303.spawnSpawner("thirdBridge1", "ghost_lantern_LT", 1)
thirdBridge1.setPos(696, 200)
thirdBridge1.setHateRadiusForChildren( 2000 )
thirdBridge1.setCFH( 400 )
thirdBridge1.setInitialMoveForChildren("ZENGARDEN_303", 670, 505)
thirdBridge1.setMonsterLevelForChildren( 4.1 )

//bottom of bridge spawn
thirdBridge2 = myRooms.ZENGARDEN_303.spawnSpawner("thirdBridge2", "ghost_lantern", 10)
thirdBridge2.setPos(428, 660)
thirdBridge2.setHateRadiusForChildren( 2000 )
thirdBridge2.setCFH( 400 )
thirdBridge2.setInitialMoveForChildren("ZENGARDEN_303", 670, 505)
thirdBridge2.setMonsterLevelForChildren( 4.1 )

//extra grunt spawner for variety
thirdBridge3 = myRooms.ZENGARDEN_303.spawnSpawner("thirdBridge3", "ghost_lantern", 10)
thirdBridge3.setPos(696, 200)
thirdBridge3.setHateRadiusForChildren( 2000 )
thirdBridge3.setCFH( 400 )
thirdBridge3.setInitialMoveForChildren("ZENGARDEN_303", 670, 505)
thirdBridge3.setMonsterLevelForChildren( 4.1 )

//stop the spawns until ready
thirdBridge1.stopSpawning()
thirdBridge2.stopSpawning()
thirdBridge3.stopSpawning()

//alliances
thirdBridge1.allyWithSpawner( thirdBridge2 )
thirdBridge1.allyWithSpawner( thirdBridge3 )
thirdBridge2.allyWithSpawner( thirdBridge3 )

//Count the players in Room 303
playersInZ4_303 = []
numToSpawnZ4_303 = 0

myManager.onEnter(myRooms.ZENGARDEN_303) { event ->
	if( isPlayer(event.actor) ) {
		playersInZ4_303 << event.actor
		event.actor.unsetQuestFlag( GLOBAL, "Z5DoNotRepeat" )
	}
}

myManager.onExit(myRooms.ZENGARDEN_303) { event ->
	if ( isPlayer(event.actor) ) {
		playersInZ4_303.remove( event.actor )
	}
}

//Create the second ambush when the player enters the trigger zone
myManager.onTriggerIn(myRooms.ZENGARDEN_303, ambushTriggerThree) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(62, 4) && !event.actor.hasQuestFlag(GLOBAL, "Z5DoNotRepeat") && thirdBridge1.spawnsInUse() == 0 ) {
		event.actor.setQuestFlag(GLOBAL, "Z5DoNotRepeat")
		event.actor.say( "It's happening again..." )
		myManager.schedule(3) { playerOnQuest = event.actor; thirdAmbush() }
	}
}

//Ambush Event
def synchronized thirdAmbush() {
	if( ( gst() > 1800 && gst() <=2359 ) || ( gst() >=0 && gst() < 600 ) ) {	
		if( myRooms.ZENGARDEN_303.getSpawnTypeCount( "ghost_lantern_LT" ) < 1 ) {
			lieutenant3 = thirdBridge1.forceSpawnNow()
			lieutenant3.addHate( playerOnQuest, 1 )
			playerOnQuest.say( "The Ghost Lanterns! I knew it!" )
			lieutenant3.say( "Your doom awaits! Look into the light!" )
			checkForDaylight()
			runOnDeath(lieutenant3, { event ->
				hateCollector.addAll( event.actor.getHated() )
				//look for someone on the Hate Collector list that is on the quest and then have them say something
				hateCollector.each{ 
					if( it.isOnQuest( 62, 4 ) ) {
						it.say( "I was right. All three groups were represented. I'd better go tell Kin before this starts happening again." )
						it.updateQuest( 62, "Kin No Mask-VQS" )
					}
				}			
				playersInZ4_303.clone().each{ it.unsetQuestFlag( GLOBAL, "Z5DoNotRepeat" ) }
				hateCollector = []
			} )
		}
		if( playersInZ4_303.size() > 6 ) {
			numToSpawnZ4_303 = 7
		} else {
			numToSpawnZ4_303 = playersInZ4_303.size() + 1
		}
		ghostLanternSpawnList = []
		spawnBridgeThreeGrunts()
	
	} else {
		
		playerOnQuest.say(  "It's odd...I can feel the energy build-up like at the other two bridges...but nothing is happening. Maybe the energy hasn't built up enough yet. I should try again later tonight." )
		myManager.schedule(600) { playerOnQuest.unsetQuestFlag( GLOBAL, "Z5DoNotRepeat" ) } //10 minutes after hitting the bridge, allow the player to check again
	}
}

def checkForDaylight() {
	if( lieutenant3.isDead() ) return
	if( gst() >= 600 && gst() < 1800 ) {
		lieutenant3.dispose()
		if( !ghostLanternSpawnList.isEmpty() ) {
			ghostLanternSpawnList.each{ 
				if( !it.isDead() ) {
					it.dispose()
				}
			}
		}
	}
	myManager.schedule(60) { checkForDaylight() }
}

ghostLanternSpawnList = []

//When the event is spawned, spawn this many grunts, to compensate for the number of players in the room.
def spawnBridgeThreeGrunts() {
	if( numToSpawnZ4_303 > 0 && myRooms.ZENGARDEN_303.getSpawnTypeCount( "ghost_lantern" ) <= numToSpawnZ4_303 ) {
		if( numToSpawnZ4_303 % 2 == 0 ) {
			tb2 = thirdBridge2.forceSpawnNow()
			ghostLanternSpawnList << tb2
			tb2.addHate( playerOnQuest, 1 )
			numToSpawnZ4_303 --
			myManager.schedule(1) { spawnBridgeThreeGrunts() }
		} else {
			tb3 = thirdBridge3.forceSpawnNow()
			ghostLanternSpawnList << tb3
			tb3.addHate( playerOnQuest, 1 )
			numToSpawnZ4_303 --
			myManager.schedule(1) { spawnBridgeThreeGrunts() }
		}
	}
}


