import com.gaiaonline.mmo.battle.script.*;

//---------------------------------------------------------
// Default Conversation - The Wish Tree                    
//---------------------------------------------------------

//The tree is not Animated, but the wish notes on it ARE Animated.
//They are a happy bunch, really pleased to see other wishes being added to the Tree (woohoo! Company!)
//They are also quite scattered. They tend to burst forth with babble and talk over each other quite a bit.
//The player has to really force the notes to focus in order to get meaningful dialog out of them

//This onEnter routine clears the convo flags just in case a player had previoiusly used "X" to close a conversation instead of using "OK"
myManager.onEnter( myRooms.ZENGARDEN_204 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z05WishTreeConvoOne" )
		event.actor.unsetQuestFlag( GLOBAL, "Z05WishTreeConvoTwo" )
		event.actor.unsetQuestFlag( GLOBAL, "Z05WishTreeConvoThree" )
		event.actor.unsetQuestFlag( GLOBAL, "Z05WishTreeConvoFour" )
		event.actor.unsetQuestFlag( GLOBAL, "Z05WishTreeConvoFive" )
		event.actor.unsetQuestFlag( GLOBAL, "Z05WishTreeConvoSix" )
	}
}

//The Wish Tree Switch (behavior) 

WishTree = spawnNPC("Wish Tree-VQS", myRooms.ZENGARDEN_204, 375, 290)
WishTree.setDisplayName( "The Wish Tree" )

wishTreeSwitch = makeSwitch( "wishTree", myRooms.ZENGARDEN_204, 380, 350 )

wishTreeSwitch.unlock()
wishTreeSwitch.off()
wishTreeSwitch.setRange( 200 )

def activateWishTreeDialog = { event ->
	wishTreeSwitch.off()
	if( event.actor.isOnQuest( 49 ) || event.actor.isDoneQuest( 49 ) ) {
		activateWishTree( event )
	}
}
	
wishTreeSwitch.whenOn( activateWishTreeDialog )

//now route the conversation appropriately
def activateWishTree( event ) { 
	
	//Player is on the WISH TREE quest from Katsumi
	if( event.actor.isOnQuest( 49, 2 ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05WishTreeConvoOne" )
		WishTree.pushDialog( event.actor, "WishQuestWish" )
	//Player is done with the Wish Tree quest, but hasn't accepted the WISH TREE VISIONS quest yet
	} else if((event.actor.isOnQuest(49, 3) || event.actor.isDoneQuest( 49 )) && !event.actor.isOnQuest( 51 ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05WishTreeConvoTwo" )
		WishTree.pushDialog( event.actor, "PlaceQuestStart" )
	//Player is on the WISH TREE VISIONS quest
	} else if( event.actor.isOnQuest( 51, 2 ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05WishTreeConvoThree" )
		WishTree.pushDialog( event.actor, "PlaceQuestInterim" )
	//Player is ready to complete the WISH TREE VISIONS quest
	} else if( event.actor.isOnQuest( 51, 3 ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05WishTreeConvoFour" )
		WishTree.pushDialog( event.actor, "PlaceQuestSuccess" )
	//Player is done with the WISH TREE VISIONS quest, but has not yet defeated Sealab X
	} else if( event.actor.isDoneQuest( 51 ) && !event.actor.hasQuestFlag( GLOBAL, "Z10SeaLabXDefeated" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05WishTreeConvoFive" )
		WishTree.pushDialog( event.actor, "WishTreeAftermath" )
	//Player has defeated Sealab X
	} else if( event.actor.isDoneQuest( 51 ) && event.actor.hasQuestFlag( GLOBAL, "Z10SeaLabXDefeated" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05WishTreeConvoSix" )
		WishTree.pushDialog( event.actor, "WishTreeAfterSeaLab" )
	}
}
	
//---------------------------------------------------------
// Katsumi's Wish Tree Quest - The Wish Tree's Part        
//---------------------------------------------------------

WishQuestWish = WishTree.createConversation("WishQuestWish", true, "Z05WishTreeConvoOne", "QuestStarted_49:2", "!QuestStarted_49:3" )

def KatWish1 = [id:1]
KatWish1.npctext = "Hi!"
KatWish1.playertext = "Ummm...hi?"
KatWish1.result = 2
WishQuestWish.addDialog(KatWish1, WishTree)

def KatWish2 = [id:2]
KatWish2.npctext = "Hi! {Hello!} =Yo!= [yeah, yeah] (Greetings!)"
KatWish2.playertext = "You...errr...said that already."
KatWish2.result = 3
WishQuestWish.addDialog(KatWish2, WishTree)

def KatWish3 = [id:3]
KatWish3.npctext = "Oh. Hehehe.=ha!= {*giggle*} Sorry (Our apologies, really). We're new to this...talking =speaking= [BABBLING] (prevaricating)...thing."
KatWish3.playertext = "New to talking? We?"
KatWish3.result = 4
WishQuestWish.addDialog(KatWish3, WishTree)

def KatWish4 = [id:4]
KatWish4.npctext = "We! [US!] (The multitude!) The notes on the tree! We're alive! =All of us!= {Each and every!}"
KatWish4.playertext = "Wait. You mean you're not the tree?"
KatWish4.result = 5
WishQuestWish.addDialog(KatWish4, WishTree)

def KatWish5 = [id:5]
KatWish5.npctext = "No, silly! [moron] (how do YOU know it's a moron?) We are the notes ON the tree. =Y'know...the big green thing with leaves?= {The pretty papers tied with string!} [ninny]"
KatWish5.playertext = "So each of you individually is Animated?"
KatWish5.result = 6
WishQuestWish.addDialog(KatWish5, WishTree)

def KatWish6 = [id:6]
KatWish6.npctext = "YES =yeah= Yup! (That's right, dear!) Good guess!"
KatWish6.playertext = "Wow. Okay! But...how come you talk so much and the other Animated hardly ever speak?"
KatWish6.result = 7
WishQuestWish.addDialog(KatWish6, WishTree)

def KatWish7 = [id:7]
KatWish7.npctext = "We don't know. [Yes...we do!] Okay, we're not SURE. =Yeah...but we definitely have an idea.= Well, of COURSE we have an idea...but..."
KatWish7.playertext = "Wait, wait, wait. I didn't mean to start an argument. But you do have a *guess*?"
KatWish7.result = 8
WishQuestWish.addDialog(KatWish7, WishTree)

def KatWish8 = [id:8]
KatWish8.npctext = "From the stories {we *like* stories} that other people (like yourself) =yes...like yourself= have told us =some of the stories sure were boring...but= (hush!) [You hush!] we think it's because (We *think*!) {Yay!} we had writing on us before we =changed= (Came ALIVE!) {Woke up!} became Animated."
KatWish8.playertext = "So...if a book became Animated it might be able to talk also?"
KatWish8.result = 9
WishQuestWish.addDialog(KatWish8, WishTree)

def KatWish9 = [id:9]
KatWish9.npctext = "Sure! =maybe= [Definitely NOT] (What? Why not?) We seem to disagree. (Books can't meld thoughts) [Don't tell it that we can do that!] {But they can't!} =How do you know, you ditz...?= SHUTUP!"
KatWish9.playertext = "Okay...so you're guessing a lot. Still, you're the only Animated that I've met that wasn't trying to kill me."
KatWish9.result = 10
WishQuestWish.addDialog(KatWish9, WishTree)

def KatWish10 = [id:10]
KatWish10.npctext = "Kill you? [hahaHAhaha] {tee-hee} We're tied to these branches! =Couldn't hurt you if we tried.= (We might try though, so be careful.) [UNTIE US!] (hush!)"
KatWish10.playertext = "Okaaaay...since you can't attack me, I'm just going to tie this little wish note to one of these branches."
KatWish10.result = 11
WishQuestWish.addDialog(KatWish10, WishTree)

def KatWish11 = [id:11]
KatWish11.npctext = "Another note! =Always room for one mo-ore...= [More brains for US!] {tieitontieitontieiton!} =yesssssss...="
KatWish11.playertext = "Then I'll just step in here...and...tie the knot...done."
KatWish11.result = 12
WishQuestWish.addDialog(KatWish11, WishTree)

def KatWish12 = [id:12]
KatWish12.npctext = "Thank you! =We feel it!= {A little girl wrote on it!} [HUNGRY] -|wha...?where?who?|- {It's waking!} [EAT IT NOW] (No! We need it! More mind for us!) -|I LIVE!|- =WE live, newb.="
KatWish12.playertext = "That's really quite...disturbing. But I've done what Katsumi asked me to do, so I'll just head back to her now."
KatWish12.quest = 49 //push the completion of the WISH TREE QUEST (step 2)
KatWish12.flag = [ "!Z05WishTreeConvoOne", "Z05WishTreeConvoTwo" ]
KatWish12.exec = { event ->
	event.player.addMiniMapQuestActorName( "Katsumi-VQS" )
	WishTree.pushDialog( event.player, "PlaceQuestStart" )
}
KatWish12.result = DONE
WishQuestWish.addDialog(KatWish12, WishTree)

//---------------------------------------------------------
// THE WISH TREE VISION QUEST (START)                      
//---------------------------------------------------------

PlaceQuestStart = WishTree.createConversation("PlaceQuestStart", true, "Z05WishTreeConvoTwo", "!QuestStarted_51", "!QuestCompleted_51")

def PlaceQuest1 = [id:1]
PlaceQuest1.npctext = "Wait! (Don't go!) [STAY] We can tell you much. =We have more than enough thoughts...= {We love hearing your wishes.}"
PlaceQuest1.options = []
PlaceQuest1.options << [text:"Okay, I'll stay and chat with you for a bit.", result: 3]
PlaceQuest1.options << [text:"Thanks...err...all of you, but I'm going to go now. Good luck to you.", result: 2]
PlaceQuestStart.addDialog(PlaceQuest1, WishTree)

def PlaceQuest2 = [id:2]
PlaceQuest2.npctext = "Okay. Good luck to you! =Seeeeeya!= {Bye now!} [Good riddance] (Goodbye, dear.)"
PlaceQuest2.playertext = "Yikes, that's weird."
PlaceQuest2.result = DONE
PlaceQuestStart.addDialog(PlaceQuest2, WishTree)

def PlaceQuest3 = [id:3]
PlaceQuest3.npctext = "Good! [NO...it's not!] =It is! more info for our brains!= What are your questions?"
PlaceQuest3.result = 4
PlaceQuestStart.addDialog(PlaceQuest3, WishTree)

def PlaceQuest4 = [id:4]
PlaceQuest4.options = []
PlaceQuest4.options << [text:"Have you met any other Animated?", result: 5]
PlaceQuest4.options << [text:"Do you know how you became Animated?", result: 6]
PlaceQuest4.options << [text:"How many of you are there?", result: 8]
PlaceQuest4.options << [text:"Is there anything that YOU all want to talk about?", result: 10]
PlaceQuest4.options << [text:"I'm done for now. Thanks!", result: DONE]
PlaceQuestStart.addDialog(PlaceQuest4, WishTree)

def PlaceQuest5 = [id:5]
PlaceQuest5.npctext = "We're tied to this tree. =Bound like Prometheus= [VULNERABLE! DOOMED!] {We can't move *sob*} We don't meet anyone unless they come to us first. =And they *never* come.= [All alone with nothing but US.] {And the little notes THEY leave behind for us} (Yes, little one, them too!)"
PlaceQuest5.playertext = "Wow. I didn't think of that."
PlaceQuest5.result = 4
PlaceQuestStart.addDialog(PlaceQuest5, WishTree)

def PlaceQuest6 = [id:6]
PlaceQuest6.npctext = "We felt the wind [the LIFE] (*our* lives) {it's everywhere!} move through us all and then {we saw!} (we heard!) =we tasted!= we awoke =Alive, but bound= [HUNGRY] and we knew that we were US and not just paper any longer. [ALIVE]{ALIVE!}=ALIVE!=(ALIVE!)"
PlaceQuest6.playertext = "Yes...but do you know WHY you are alive?"
PlaceQuest6.result = 7
PlaceQuestStart.addDialog(PlaceQuest6, WishTree)

def PlaceQuest7 = [id:7]
PlaceQuest7.npctext = "An odd question (at least ill-conceived). Do YOU know why you're alive? =Maybe to ask silly questions?= [To EAT!!] {So we have someone to talk to so we know we exist?}"
PlaceQuest7.playertext = "Okay. Fair point. You got me there."
PlaceQuest7.result = 4
PlaceQuestStart.addDialog(PlaceQuest7, WishTree)

def PlaceQuest8 = [id:8]
PlaceQuest8.npctext = "We are like the leaves. {many!} [ALL OF US!] As people bring more wishes, we are more. {scores!} =hordes!!= [LEGION!!!]"
PlaceQuest8.playertext = "But all the notes that Animates are still here in the tree? What would happen if I took some of you to a place with more paper?"
PlaceQuest8.result = 9
PlaceQuestStart.addDialog(PlaceQuest8, WishTree)

def PlaceQuest9 = [id:9]
PlaceQuest9.npctext = "More...paper? [we could MULTIPLY!] =oh please oh please oh please...= [Take US!] (We could be ALL!)"
PlaceQuest9.playertext = "I see. Well...maybe later then. It might be scary to have a whole library talking to me all at the same time."
PlaceQuest9.result = 4
PlaceQuestStart.addDialog(PlaceQuest9, WishTree)

def PlaceQuest10 = [id:10]
PlaceQuest10.npctext = "The wind? The world? Our visions? (The visions of otherwheres?!?) [NO! DO NOT TELL!] {We're confused by them!} [TELL!]"
PlaceQuest10.playertext = "You have visions?"
PlaceQuest10.result = 11
PlaceQuestStart.addDialog(PlaceQuest10, WishTree)

def PlaceQuest11 = [id:11]
PlaceQuest11.npctext = "Yes we do. [NO WE DON'T!] {we do we do we do} =Who cares? We are BOUND!="
PlaceQuest11.playertext = "Can you describe those places to me?"
PlaceQuest11.result = 12
PlaceQuestStart.addDialog(PlaceQuest11, WishTree)

def PlaceQuest12 = [id:12]
PlaceQuest12.npctext = "Some of them...[NO!]. We see a high place =we see for miles!= near old stone where water once ran {the fighting!} where now only pain flows. [the pain tastes like FOOD]."
PlaceQuest12.playertext = "Do you see anything else?"
PlaceQuest12.result = 13
PlaceQuestStart.addDialog(PlaceQuest12, WishTree)

def PlaceQuest13 = [id:13]
PlaceQuest13.npctext = "The wind {We love the wind} blows giant leaves around on a giant tree (not a tree) =must be a tree= not made by the world (ancient ancestor) {GAIA!}, but by those that live on it. [feeble frail things] {YOU!}"
PlaceQuest13.playertext = "Interesting. Is that all?"
PlaceQuest13.result = 14
PlaceQuestStart.addDialog(PlaceQuest13, WishTree)

def PlaceQuest14 = [id:14]
PlaceQuest14.npctext = "Yes! [many more...but DO NOT TELL!] We will tell one more. A lance of light {so bright!} stabs out over a grey pond with no shores =big, big, BIG= bellowing out at the darkness."
PlaceQuest14.playertext = "Your visions sound odd, but maybe they're real places, after all."
PlaceQuest14.options = []
PlaceQuest14.options << [text:"Would you like me to try and find those places?", result: 15]
PlaceQuest14.options << [text:"Good luck to you. I'm going to take off now...Tree.", result: 16]
PlaceQuestStart.addDialog(PlaceQuest14, WishTree)

def PlaceQuest15 = [id:15]
PlaceQuest15.npctext = "Yes! Yes! =What if it finds something?= [The visions are OURS!] (Please find them! We must know!)"
PlaceQuest15.playertext = "Okay. I'll keep my eye out for places that match those descriptions. If I find them all, I'll come back and let you know. See ya...Tree."
PlaceQuest15.flag = "!Z05WishTreeConvoTwo" //once this is unset, the player will have to leave the room and return to get the interim
PlaceQuest15.quest = 51 //push the start of the WISH TREE VISION QUEST
PlaceQuest15.result = DONE
PlaceQuestStart.addDialog(PlaceQuest15, WishTree)

def PlaceQuest16 = [id:16]
PlaceQuest16.npctext = "Not Tree. Wishes! (Notes!) =Paper!= {[(=US!=)]}"
PlaceQuest16.playertext = "Wish Tree then. Either way...see ya."
PlaceQuest16.result = DONE
PlaceQuestStart.addDialog(PlaceQuest16, WishTree)

//---------------------------------------------------------
// Place Quest Interim (if come back before seeing all     
// three places described in the visions)                  
//---------------------------------------------------------

PlaceQuestInterim = WishTree.createConversation("PlaceQuestInterim", true, "Z05WishTreeConvoThree", "QuestStarted_51:2", "!QuestStarted_51:3")

def PlaceInterim1 = [id:1]
PlaceInterim1.npctext = "You're back! {Hooray!} [..thought it would be dead by now...]"
PlaceInterim1.playertext = "So I am. Back that is, not dead."
PlaceInterim1.result = 2
PlaceQuestInterim.addDialog(PlaceInterim1, WishTree)

def PlaceInterim2 = [id:2]
PlaceInterim2.npctext = "We knew you would be back.=Ask it! Ask it!="
PlaceInterim2.playertext = "No need to ask. No, I haven't found your vision places yet. Sorry."
PlaceInterim2.result = 3
PlaceQuestInterim.addDialog(PlaceInterim2, WishTree)

def PlaceInterim3 = [id:3]
PlaceInterim3.npctext = "You will! {Have we seen it?} (No! Shhh...it doesn't know that we can see the layers of what might be.) =hehehe...if it did, it would hate us.= [EAT IT! IT KNOWS TOO MUCH!]"
PlaceInterim3.playertext = "Ummm, right. I'll be back later then."
PlaceInterim3.flag = "!Z05WishTreeConvoThree" //unset the flag so the tree has to be clicked to get the conversation each time
PlaceInterim3.result = DONE
PlaceQuestInterim.addDialog(PlaceInterim3, WishTree)

//---------------------------------------------------------
// Place Quest Success (after all three locations are      
// visited.                                                
//---------------------------------------------------------

PlaceQuestSuccess = WishTree.createConversation("PlaceQuestSuccess", true, "Z05WishTreeConvoFour", "QuestStarted_51:3", "!QuestCompleted_51" )

def PlaceSucc1 = [id:1]
PlaceSucc1.npctext = "You've been there! (Seen them!) {TOUCHED them!} [TELL US!]"
PlaceSucc1.playertext = "I guess you know I've found your places then."
PlaceSucc1.result = 2
PlaceQuestSuccess.addDialog(PlaceSucc1, WishTree)

def PlaceSucc2 = [id:2]
PlaceSucc2.npctext = "Tell us! {nownownownownow...} [TELL US!!!]"
PlaceSucc2.playertext = "The stone where water ran was the old aqueduct that carried water to Barton Town before a new piping system was built. Two groups war over those stones now, one out of ignorance and one out of greed."
PlaceSucc2.result = 3
PlaceQuestSuccess.addDialog(PlaceSucc2, WishTree)

def PlaceSucc3 = [id:3]
PlaceSucc3.npctext = "And the tree with giant leaves built by your kind? (the leaves!) {it's so BIG!}"
PlaceSucc3.playertext = "It's just an old abandoned grain mill by Bassken Lake. I don't know why you saw it, but the 'leaves' are just the windmill vanes."
PlaceSucc3.result = 4
PlaceQuestSuccess.addDialog(PlaceSucc3, WishTree)

def PlaceSucc4 = [id:4]
PlaceSucc4.npctext = "The light lance! Tell us of the light lance! (...and the pond that never ends) =So grey, so cold...="
PlaceSucc4.playertext = "I'm pretty sure that's the lighthouse near Gold Beach. It warns passing ships about the reefs and rocks off that point so they don't add to the scores of shipwrecks that have happened there over the years."
PlaceSucc4.result = 5
PlaceQuestSuccess.addDialog(PlaceSucc4, WishTree)

def PlaceSucc5 = [id:5]
PlaceSucc5.playertext = "I really don't know how that helps you all, but if that's what you wanted to know...well...there ya go."
PlaceSucc5.result = 6
PlaceQuestSuccess.addDialog(PlaceSucc5, WishTree)

def PlaceSucc6 = [id:6]
PlaceSucc6.npctext = "Ahhhh...{so happy} =Now what? Do we tell it?= [NO! Tell nothing!] (Tell it *something*. It was nice to us.)"
PlaceSucc6.playertext = "Did you want to say something else to me?"
PlaceSucc6.result = 7
PlaceQuestSuccess.addDialog(PlaceSucc6, WishTree)

def PlaceSucc7 = [id:7]
PlaceSucc7.npctext = "We...see things. [NO! Not the futures!] =If we speak them, do we make them true?= (Tell it! We shouldn't be the only ones that know!)"
PlaceSucc7.playertext = "What?!? What are you saying?"
PlaceSucc7.result = 8
PlaceQuestSuccess.addDialog(PlaceSucc7, WishTree)

def PlaceSucc8 = [id:8]
PlaceSucc8.npctext = "Things of water (grey water...) {Blue water!} =MOVING water!= and things below {down below?} (yes, far below!) are the source that caused us (made us) =accidents! afterthoughts!= to come alive."
PlaceSucc8.playertext = "That barely makes any sense at all? Water? Under? Underground below a river? In the ocean? Made you? Just you? Or all Animated in general?"
PlaceSucc8.result = 9
PlaceQuestSuccess.addDialog(PlaceSucc8, WishTree)

def PlaceSucc9 = [id:9]
PlaceSucc9.npctext = "We don't know. We FEEL (see) {smell} [but don't KNOW!] that these things are true (perhaps) =maybe= [ARE TRUE]. We sense (taste) that what was done is just the start [Animated EVERYWHERE!] unless the one that died is vanquished =beaten= (stopped) [EATEN] again."
PlaceSucc9.playertext = "What else? It sounds like you know something else..."
PlaceSucc9.result = 10
PlaceQuestSuccess.addDialog(PlaceSucc9, WishTree)

def PlaceSucc10 = [id:10]
PlaceSucc10.npctext = "We don't *know* =yes we do= (we just guess) [we KNOW] what will happen, only what *might* happen [DOOMED! YOU ARE DOOMED!] but if we speak it we may make it true, so we will not =speak it!= (hush!)"
PlaceSucc10.playertext = "You must tell me! What do you see?"
PlaceSucc10.result = 11
PlaceQuestSuccess.addDialog(PlaceSucc10, WishTree)

def PlaceSucc11 = [id:11]
PlaceSucc11.npctext = "..."
PlaceSucc11.playertext = "Please! I've gone to all these places to help you. Tell me what you know!"
PlaceSucc11.result = 12
PlaceQuestSuccess.addDialog(PlaceSucc11, WishTree)

def PlaceSucc12 = [id:12]
PlaceSucc12.npctext = "..."
PlaceSucc12.playertext = "Fine! Sheesh. I can't believe it. That's a fine thank you!"
PlaceSucc12.result = 13
PlaceQuestSuccess.addDialog(PlaceSucc12, WishTree)

def PlaceSucc13 = [id:13]
PlaceSucc13.npctext = "We will not speak for fear {bad things from the dark} (Animated of *tremendous* strength), but you have been our friend {our first!} =perhaps our last?= and we would shake our branches to loosen the gifts placed there for you (Not notes, un-live things...)."
PlaceSucc13.result = 14
PlaceQuestSuccess.addDialog(PlaceSucc13, WishTree)

def PlaceSucc14 = [id:14]
PlaceSucc14.npctext = "We may say no more =to you= (at this time) [FOR NOW]."
PlaceSucc14.playertext = "Well...thanks for the gifts anyway."
PlaceSucc14.quest = 51 //push the successful completion of the WISH TREE VISION quest
PlaceSucc14.flag = "!Z05WishTreeConvoFour" //unset the flag so the tree has to be clicked to get the conversation each time
PlaceSucc14.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Wish Tree-VQS" )
}
PlaceSucc14.result = DONE
PlaceQuestSuccess.addDialog(PlaceSucc14, WishTree)

//---------------------------------------------------------
// Looping Aftermath speech                                
//---------------------------------------------------------

WishTreeAftermath = WishTree.createConversation("WishTreeAftermath", true, "Z05WishTreeConvoFive", "QuestCompleted_51" )

def WishAfter1 = [id:1]
WishAfter1.playertext = "Hi there, Wish Tree!"
WishAfter1.result = 2
WishTreeAftermath.addDialog(WishAfter1, WishTree)

def WishAfter2 = [id:2]
WishAfter2.npctext = "..."
WishAfter2.playertext = "Still won't talk to me, eh?"
WishAfter2.result = 3
WishTreeAftermath.addDialog(WishAfter2, WishTree)

def WishAfter3 = [id:3]
WishAfter3.npctext = "We dare not =don't want to= [CANNOT] (only later!) tell you anything further."
WishAfter3.playertext = "I thought not. Is there a time in the future when you *might* talk to me again?"
WishAfter3.result = 4
WishTreeAftermath.addDialog(WishAfter3, WishTree)

def WishAfter4 = [id:4]
WishAfter4.npctext = "Perhaps. [NEVER!] =Yes! Absolutely, or rather...maybe.= (Only if the Shadow that comes is stopped.)"
WishAfter4.playertext = "Clear as mud, as usual. Okay...later then!"
WishAfter4.flag = "!Z05WishTreeConvoFive" //unset the flag so the tree has to be clicked to get the conversation each time
WishAfter4.result = DONE
WishTreeAftermath.addDialog(WishAfter4, WishTree)

//---------------------------------------------------------
// AFTER SEALAB X IS DESTROYED                             
//---------------------------------------------------------

//The Wish Tree will elaborate on its fears further after SeaLabX has been destroyed and those particular visions won't take place any longer.

WishTreeAfterSeaLab = WishTree.createConversation("WishTreeAfterSeaLab", true, "Z05WishTreeConvoSix", "QuestCompleted_51" ) //Add the correct "SeaLabXQuestCompleted" flag here. Replace it in the Aftermath speech also.

def PostSeaLab1 = [id:1]
PostSeaLab1.npctext = "You've done it! {Hooray! We knew you could!} (Fantastic!) =Anyone could have done it= We are very happy for you!"
PostSeaLab1.playertext = "Thanks!"
PostSeaLab1.flag = "!Z05WishTreeConvoSix" //unset the flag so the tree has to be clicked to get the conversation each time
PostSeaLab1.result = DONE
WishTreeAfterSeaLab.addDialog(PostSeaLab1, WishTree)
