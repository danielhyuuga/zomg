import com.gaiaonline.mmo.battle.script.*;

MaxCL = 4.1

taikoDrumSet = [] as Set
ghostLanternSet = [] as Set

//====================================
//Death listener for TaikoDrums       
//====================================
// GIMMEE A BEAT (DOWN)
def zenTaikoDrumDeathScheduler() {
	myRooms.values().each() {
		it.getActorList().each() {
			if(isMonster( it ) && it.getMonsterType() == "taiko_drum" && !taikoDrumSet.contains( it ) ) {
				//add the monster to the list
				taikoDrumSet << it
				//check for quest credit when the monster is killed
				runOnDeath( it ) { event -> checkForTaikoDrumUpdate( event ); taikoDrumSet.remove(event.actor) }
			}
		}
	}
	//check all monsters again in one second to ensure the list is populated with currently available quest monsters
	myManager.schedule(1) { zenTaikoDrumDeathScheduler() }
}

def synchronized checkForTaikoDrumUpdate( event ) {
	event.actor.getHated().each() {
		//if they're on the proper quest, then check for quest credit
		if( isPlayer(it) && it.isOnQuest( 283, 2 ) ) {
			if( it.getConLevel() < MaxCL ) {
				//increment the counter
				it.addToPlayerVar( "Z05TaikoDrumKillTotal", 1 )
				
				//display it to the player
				it.centerPrint( "Taiko Drum ${it.getPlayerVar( "Z05TaikoDrumKillTotal").toInteger()}/20" )
				
				//if they're done, then update the quest
				if( it.getPlayerVar( "Z05TaikoDrumKillTotal" ) == 20 ) {
					it.updateQuest( 283, "BFG-Trip" )
					it.deletePlayerVar( "Z05TaikoDrumKillTotal" )
				}
			} else {
				it.centerPrint( "You must be level 4.0 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

zenTaikoDrumDeathScheduler()

//====================================
//Death listener for Ghost Lanterns   
//====================================
// DIMMING THE LANTERNS
def zenGhostLanternDeathScheduler() {
	myRooms.values().each() {
		it.getActorList().each() {
			if(isMonster( it ) && it.getMonsterType() == "ghost_lantern" && !ghostLanternSet.contains( it ) ) {
				//add the monster to the list
				ghostLanternSet << it
				//check for quest credit when the monster is killed
				runOnDeath( it ) { event -> checkForGhostLanternUpdate( event ); ghostLanternSet.remove(event.actor) }
			}
		}
	}
	//check all monsters again in one second to ensure the list is populated with currently available quest monsters
	myManager.schedule(1) { zenGhostLanternDeathScheduler() }
}

def synchronized checkForGhostLanternUpdate( event ) {
	event.actor.getHated().each() {
		//if they're on the proper quest, then check for quest credit
		if( isPlayer(it) && it.isOnQuest( 284, 2 ) ) {
			if( it.getConLevel() < MaxCL ) {
				//increment the counter
				it.addToPlayerVar( "Z05GhostLanternKillTotal", 1 )
				
				//display it to the player
				it.centerPrint( "Ghost Lantern ${it.getPlayerVar( "Z05GhostLanternKillTotal").toInteger()}/20" )
				
				//if they're done, then update the quest
				if( it.getPlayerVar( "Z05GhostLanternKillTotal" ) == 20 ) {
					it.updateQuest( 284, "BFG-Arnold" )
					it.deletePlayerVar( "Z05GhostLanternKillTotal" )
				}
			} else {
				it.centerPrint( "You must be level 4.0 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

zenGhostLanternDeathScheduler()