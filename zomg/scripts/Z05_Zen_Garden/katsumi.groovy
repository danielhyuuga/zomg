import com.gaiaonline.mmo.battle.script.*;

def Katsumi = spawnNPC("Katsumi-VQS", myRooms.ZENGARDEN_502, 700, 525)
Katsumi.setDisplayName( "Katsumi" )

myManager.onEnter( myRooms.ZENGARDEN_502 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		event.actor.unsetQuestFlag( GLOBAL, "Z05RingRewardInProgress" )
	}
	if( isPlayer( event.actor ) ) {
		if ( event.actor.getConLevel() >= 4.2 ) {
			event.actor.setQuestFlag( GLOBAL, "Z05TooOldForZenQuests" )
		} else {
			event.actor.unsetQuestFlag( GLOBAL, "Z05TooOldForZenQuests" )
		}
	}
	
	//Do the Wish Tree Success to Uncle Kin data error fix
	if( isPlayer( event.actor ) && event.actor.isDoneQuest( 49 ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05ConnectToKin" )
	}
	
	//Do the Uncle Kin Success to Katsumi's Doll data error fix
	if( isPlayer( event.actor ) && event.actor.isDoneQuest( 48 ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z05KatsDollOkay" )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(68, 2) && (event.actor.isOnQuest(49) || event.actor.isDoneQuest(49))) {
		event.actor.updateQuest(68, "Katsumi-VQS")
	}
}

onQuestStep( 285, 2 ) { event -> event.player.addMiniMapQuestActorName( "Katsumi-VQS" ) }

//---------------------------------------------------------
// Kokeshi Doll Quest - START                              
//---------------------------------------------------------

def KatDefault = Katsumi.createConversation("KatDefault", true, "!Z05WishStartOkay", "!QuestStarted_49", "!QuestCompleted_49")

def KatDef1 = [id:1]
KatDef1.npctext = "Hello, stranger. My name is Katsumi."
KatDef1.exec = { event ->
	if( event.player.isOnQuest( 68, 2 ) ) {
		event.player.setQuestFlag( GLOBAL, "Z05HiFromRubella" )
		Katsumi.pushDialog( event.player, "KatRubella" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z05ContinueDefault" )
		Katsumi.pushDialog( event.player, "continueKatDefault" )
	}
}
KatDefault.addDialog( KatDef1, Katsumi )

// If on Rubella's TALK TO KATSUMI quest

def KatRubella = Katsumi.createConversation( "KatRubella", true, "Z05HiFromRubella", "!QuestCompleted_68" )

def kRub1 = [id:1]
kRub1.playertext = "Hello there. Rubella asked me to swing by and check up on you. Is everything okay here?"
kRub1.result = 2
KatRubella.addDialog( kRub1, Katsumi )

def kRub2 = [id:2]
kRub2.npctext = "Rubella? That was sweet of her! Please give her my best when you see her next!"
kRub2.quest = 68
kRub2.flag = [ "Z05ContinueDefault", "!Z05HiFromRubella" ]
kRub2.exec = { event ->
	Katsumi.pushDialog( event.player, "continueKatDefault" )
	event.player.removeMiniMapQuestActorName("Katsumi-VQS")
	event.player.addMiniMapQuestActorName("Rubella-VQS")
}
KatRubella.addDialog( kRub2, Katsumi )


// Continuing the default conversation thereafter

def continueKatDefault = Katsumi.createConversation( "continueKatDefault", true, "!QuestStarted_49", "!QuestCompleted_49", "Z05ContinueDefault", "!Z05WishStartOkay" )

def continue1 = [id:1]
continue1.npctext = "You are welcome to hide here in the Meditation Shrine. It seems to be a safe haven against the Animated."
continue1.options = []
continue1.options << [text:"It is? You mean the Animated don't come in here?", result: 2]
continue1.options << [text:"Good deal. I think I'll take a breather here then. Thanks.", result: DONE]
continueKatDefault.addDialog(continue1, Katsumi)

def continue2 = [id:2]
continue2.npctext = "That's right. I don't know if it's because the Shrine protects us somehow, or if they simply can't see us when we're in here."
continue2.playertext = "Really? What's so special about this  shrine?"
continue2.result = 3
continueKatDefault.addDialog(continue2, Katsumi)

def continue3 = [id:3]
continue3.npctext = "I have no idea. It's definitely not just because we're inside a structure. People have been attacked in their homes all over Gaia."
continue3.playertext = "Do you have any clues at all?"
continue3.result = 4
continueKatDefault.addDialog(continue3, Katsumi)

def continue4 = [id:4]
continue4.npctext = "Well...maybe. Uncle brought me the crystal that you see here in the Shrine. He said it was from an asteroid crater from elsewhere in the world."
continue4.playertext = "Is it becoming Animated?"
continue4.result = 7
continueKatDefault.addDialog(continue4, Katsumi)

//There are no numbers 5 & 6 (removed)

def continue7 = [id:7]
continue7.npctext = "No, no...but it's pulsing with some kind of inner light. And the *really* strange part is that it started glowing right around the same day that the Animated began to appear around here."
continue7.playertext = "Do you think the Animated are staying away from the shrine because of that crystal?"
continue7.result = 8
continueKatDefault.addDialog(continue7, Katsumi)

def continue8 = [id:8]
continue8.npctext = "Yes. I do. It's keeping me safe...but the Animated are everywhere out there, and now I'm trapped here!"
continue8.options = []
continue8.options << [text:"Do you need help against the Animated?  (**NOTE: The reward for completing Katsumi's series of tasks includes the selection of a new ring!**)", result: 9]
continue8.options << [text:"At least you're safe here inside the shrine. Good luck to you!", result: 10]
continueKatDefault.addDialog(continue8, Katsumi)

def continue9 = [id:9]
continue9.npctext = "Only indirectly. I would like you to do something for me which would give me great peace of mind."
continue9.playertext = "Oh, really? What's that?"
continue9.flag = ["Z05KatsumiAlreadyMet", "Z05WishStartOkay"]
continue9.exec = { event ->
	Katsumi.pushDialog( event.player, "WishQuestStart" )
}
continue9.result = DONE
continueKatDefault.addDialog( continue9, Katsumi )

def continue10 = [id:10]
continue10.npctext = "I wish my Uncle was here. I feel so alone."
continue10.flag = "Z05KatsumiAlreadyMet"
continue10.result = DONE
continueKatDefault.addDialog( continue10, Katsumi )

//---------------------------------------------------------
// Wish Tree Quest - START                                 
// Available anytime after Uncle Kin is found              
//---------------------------------------------------------

def WishQuestStart = Katsumi.createConversation("WishQuestStart", true, "Z05WishStartOkay", "!QuestStarted_49", "!QuestCompleted_49")

def WishStart1 = [id:1]
WishStart1.npctext = "I know this may seem silly, but I'd like you to take a moment to aid me in helping *everyone* at once."
WishStart1.playertext = "Fantastic! How do we do that?"
WishStart1.result = 2
WishQuestStart.addDialog(WishStart1, Katsumi)

def WishStart2 = [id:2]
WishStart2.npctext = " I would like you to take this piece of paper and find the Wish Tree to the northeast of this shrine. Tie the wish to a branch and return to me. That is all I desire."
WishStart2.playertext = "A wish?"
WishStart2.result = 3
WishQuestStart.addDialog(WishStart2, Katsumi)

def WishStart3 = [id:3]
WishStart3.npctext = "Yes. It may seem silly to you, but I have left wishes on that tree my entire life. Since everything else in the world is topsy-turvy now, who is to say that wishes will not start being granted. So my wish is for everyone to stay safe even amidst all this chaos." 
WishStart3.playertext = "That's...nice, Katsumi."
WishStart3.options = []
WishStart3.options << [text: "I'd be glad to help you with your wish. I'll find this Wish Tree you mention and tie your note in its branches.", result: DONE, quest: 49] //Start the Wish Tree quest
WishStart3.options << [text: "I'll come back, Katsumi, but for now, I'm busy with a few other things. Sorry.", result: DONE]
WishQuestStart.addDialog(WishStart3, Katsumi)

//---------------------------------------------------------
// Wish Tree Quest - INTERIM                               
//---------------------------------------------------------

def WishQuestInterim = Katsumi.createConversation("WishQuestInterim", true, "QuestStarted_49", "!QuestStarted_49:3")

def WishInterim1 = [id:1]
WishInterim1.npctext = "Is it done? Is my wish securely on the Wish Tree now?"
WishInterim1.playertext = "No, not yet."
WishInterim1.result = 2
WishQuestInterim.addDialog(WishInterim1, Katsumi)

def WishInterim2 = [id:2]
WishInterim2.npctext = "Oh. Okay. I'm sure you will get to it soon."
WishInterim2.result = DONE
WishQuestInterim.addDialog(WishInterim2, Katsumi)

//---------------------------------------------------------
// Wish Tree Quest - SUCCESS                               
//---------------------------------------------------------

def WishQuestSuccess = Katsumi.createConversation("WishQuestSuccess", true, "QuestStarted_49:3")

def WishSuccess1 = [id:1]
WishSuccess1.npctext = "Is my wish delivered, traveler?"
WishSuccess1.playertext = "That was the oddest...tree...I've ever seen, but yes, your wish is in its branches now. And my name is %p."
WishSuccess1.result = 2
WishQuestSuccess.addDialog(WishSuccess1, Katsumi)

def WishSuccess2 = [id:2]
WishSuccess2.npctext = "You're a good person, %p! There are not many that would have stopped to help a small girl with her idle fancy. Thank you. My Uncle will surely like to speak with you."
WishSuccess2.playertext = "Your uncle?"
WishSuccess2.quest = 49 //push the completion of THE WISH TREE quest
WishSuccess2.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z05ConnectToKin" )
	event.player.removeMiniMapQuestActorName( "Katsumi-VQS" )
	Katsumi.pushDialog( event.player, "KinQuestStart" )
}
WishSuccess2.result = DONE
WishQuestSuccess.addDialog(WishSuccess2, Katsumi)


//---------------------------------------------------------
// FIND UNCLE KIN - START                                  
// Should be open anytime after Doll Quest is completed    
//---------------------------------------------------------

def KinQuestStart = Katsumi.createConversation("KinQuestStart", true, "Z05ConnectToKin", "!QuestStarted_48", "!QuestCompleted_48")

def KinStart1 = [id:1]
KinStart1.npctext = "Yes. You see, there is one other thing I could ask you to help me with, if it's not too much trouble."
KinStart1.playertext = "Oh? What would that be?"
KinStart1.result = 2
KinQuestStart.addDialog(KinStart1, Katsumi)

def KinStart2 = [id:2]
KinStart2.npctext = "Now that my wish is made, my head is clearer and I realize that I need to reach my Uncle to let him know I am safe. He is older and worries about me."
KinStart2.options = []
KinStart2.options << [text:"I would be glad to help you out, Katsumi. Where's your Uncle?", result: 3]
KinStart2.options << [text:"Maybe some other time. I've been here too long and I gotta go now.", result: 6]
KinQuestStart.addDialog(KinStart2, Katsumi)

def KinStart3 = [id:3]
KinStart3.npctext = "My Uncle can be hard to find."
KinStart3.playertext = "Why? Was he driven out of his home by the Animated?"
KinStart3.result = 4
KinQuestStart.addDialog(KinStart3, Katsumi)

def KinStart4 = [id:4]
KinStart4.npctext = "Well...no. He belongs to a clandestine organization. He is a powerful man."
KinStart4.playertext = "Secretive, eh? So why is it that YOU know?"
KinStart4.result = 5
KinQuestStart.addDialog(KinStart4, Katsumi)

def KinStart5 = [id:5]
KinStart5.npctext = "My family has been part of the enclave for many generations. My Uncle's name is Kin. He is a ninja."
KinStart5.options = []
KinStart5.options << [text:"Ninjas? Tell me more!", result: 8]
KinStart5.options << [text:"Ninjas?!? Thanks, but no thanks! Next you'll have me working for the mob!", result: 6]
KinQuestStart.addDialog(KinStart5, Katsumi)

def KinStart6 = [id:6]
KinStart6.npctext = "I'm quite sorry that you feel that way. My *Uncle* will be sorry also. Are you sure you don't want to help me?"
KinStart6.options = []
KinStart6.options << [text:"Your Uncle will be sorry, eh? All right. Okay, maybe I can find time to help, after all.", result: 8]
KinStart6.options << [text:"That's it! I never should have even said hello to you. I'm outta here!", result: 7]
KinQuestStart.addDialog(KinStart6, Katsumi)

def KinStart7 = [id:7]
KinStart7.npctext = "As you wish...%p. I'll see to it that Uncle remembers your name."
KinStart7.result = DONE
KinStart7.flag = "Z5LeftKatsumiAlone"
KinQuestStart.addDialog(KinStart7, Katsumi)

def KinStart8 = [id:8]
KinStart8.npctext = "The ninja enclave lies in the forests west of the Zen Gardens. My Uncle ranks high within their order, but how high he does not tell even me. Since my father...passed, Uncle has been the only family that I have."
KinStart8.playertext = "So I have to go into the forest and find your Uncle?"
KinStart8.result = 9
KinQuestStart.addDialog(KinStart8, Katsumi)

def KinStart9 = [id:9]
KinStart9.npctext = "No. Do not go into the forest. If the ninjas found you there without an invitation... well... just do not go there. Instead, you must take this glass rod and ring the iron bell you'll find along the forest's edge to the north."
KinStart9.playertext = "A glass rod? And an iron bell? That doesn't sound like it's going to work."
KinStart9.result = 10
KinQuestStart.addDialog(KinStart9, Katsumi)

def KinStart10 = [id:10]
KinStart10.npctext = "You must trust what you do not understand if you are to converse with the ninja."
KinStart10.playertext = "Oooooo...deep. Okay. I'll go ring the iron bell with the glass rod. And your Uncle will somehow just...know...that I did it?"
KinStart10.result = 11
KinQuestStart.addDialog(KinStart10, Katsumi)

def KinStart11 = [id:11]
KinStart11.npctext = "Yes. Exactly. Good luck, %p."
KinStart11.playertext = "Riiiiight. Okay then. See ya after I ring the bell."
KinStart11.result = DONE
KinStart11.quest = 48 //Start the "Find Uncle Kin" quest
KinQuestStart.addDialog(KinStart11, Katsumi)

//---------------------------------------------------------
// Uncle Kin Quest - INTERIM                               
//---------------------------------------------------------

def KinQuestInterim = Katsumi.createConversation("KinQuestInterim", true, "QuestStarted_48", "!QuestStarted_48:4")

def KinInterim1 = [id:1]
KinInterim1.npctext = "Have you found my Uncle yet?"
KinInterim1.playertext = "No, not yet. But I will soon!"
KinInterim1.result = 2
KinQuestInterim.addDialog(KinInterim1, Katsumi)

def KinInterim2 = [id:2]
KinInterim2.npctext = "Okay. Please hurry. I would not want him to take extra risks because he thought I might be in trouble."
KinInterim2.result = DONE
KinQuestInterim.addDialog(KinInterim2, Katsumi)

//---------------------------------------------------------
// Uncle Kin Quest - SUCCESS                               
//---------------------------------------------------------

def KinQuestSuccess = Katsumi.createConversation("KinQuestSuccess", true, "QuestStarted_48:4")

def KinSuccess1 = [id:1]
KinSuccess1.npctext = "Have you found my Uncle yet?"
KinSuccess1.playertext = "I certainly did. Your Uncle is happy to know you are safe."
KinSuccess1.result = 2
KinQuestSuccess.addDialog( KinSuccess1, Katsumi )

def KinSuccess2 = [id:2]
KinSuccess2.npctext = "Oh good! Since you're alone, I'm guessing that Uncle needed to stay at the enclave. He knows best, I am sure...but why are you here now?"
KinSuccess2.playertext = "I told your Uncle I would come back and see if you needed any other help."
KinSuccess2.result = 3
KinQuestSuccess.addDialog( KinSuccess2, Katsumi )

def KinSuccess3 = [id:3]
KinSuccess3.npctext = "Thank you, %p. I am indebted to you, but first let me give you this small gift as a token of my appreciation."
KinSuccess3.quest = 48 //push the completion flag for the Uncle Kin Quest
KinSuccess3.flag = [ "Z05KatsDollOkay", "Z5KatsumiKnown" ] // NOTE: "Z5KatsumiKnown" flag : After the Wish Tree and Kin quests, this is the flag to set so that Rubella knows you've already met Katsumi.
KinSuccess3.exec = { event -> 
	event.player.removeMiniMapQuestActorName( "Katsumi-VQS" ) 
	Katsumi.pushDialog( event.player, "KatsDollStart" )
}
KinQuestSuccess.addDialog( KinSuccess3, Katsumi )

//=========================================================
// KATSUMI'S KOKESHI DOLL (Start)                          
//=========================================================

def KatsDollStart = Katsumi.createConversation( "KatsDollStart", true, "Z05KatsDollOkay", "!QuestStarted_47", "!QuestCompleted_47" )

def dollStart1 = [id:1]
dollStart1.playertext = "Since I'm here...is there something that I can actually do for you now?"
dollStart1.result = 2
KatsDollStart.addDialog( dollStart1, Katsumi )

def dollStart2 = [id:2]
dollStart2.npctext = "Yes! Oh very much, yes. I had a Kokeshi Doll here in the shrine. It was beautiful and fragile. But then it Animated! It took off like a rabbit...probably because of that crystal...but after that, more of them began to appear out there."
dollStart2.playertext = "And you're just mentioning that *now*?"
dollStart2.result = 3
KatsDollStart.addDialog( dollStart2, Katsumi )

def dollStart3 = [id:3]
dollStart3.npctext = "Well, yes. I admit that perhaps I should have said something earlier, but I have always been one to put the welfare of others before my own."
dollStart3.playertext = "Okay, okay...so you think *your* doll had something to do with all those Kokeshi out there?"
dollStart3.result = 4
KatsDollStart.addDialog(dollStart3, Katsumi)

def dollStart4 = [id:4]
dollStart4.npctext = "Actually, I'm very much afraid that *all* of those dolls somehow came from MY doll. I need to know! Could you go out there and find my doll?"
dollStart4.options = []
dollStart4.options << [text:"Of course! But how would I know if it's your doll or not?", result: 5]
dollStart4.options << [text:"Give it up! It's just a doll! How about I help with something else instead?", result: 6]
KatsDollStart.addDialog(dollStart4, Katsumi)

def dollStart5 = [id:5]
dollStart5.npctext = "That doll was a present from my Father before he left this world. He wrote my name on the bottom before he gave it to me. I watched the doll run off toward the southeast somewhere. You should try that direction first."
dollStart5.playertext = "Okay, Katsumi. That seems a bit more challenging. I'll go look for your doll and tell you what I find!"
dollStart5.result = 7
KatsDollStart.addDialog(dollStart5, Katsumi)

def dollStart6 = [id:6]
dollStart6.npctext = "Oh. Well, if you can't help with that, I will try to find it myself soon. I hope I am not too frail for the task."
dollStart6.playertext = "Okay. Good luck, Katsumi."
dollStart6.result = DONE
KatsDollStart.addDialog(dollStart6, Katsumi)

def dollStart7 = [id:7]
dollStart7.npctext = "Good luck to you, %p. Be careful."
dollStart7.playertext = "Ha! Let the Animated be the careful ones!"
dollStart7.result = 8
KatsDollStart.addDialog(dollStart7, Katsumi)

def dollStart8 = [id:8]
dollStart8.npctext = "<zOMG dialogWidth='250'><![CDATA[<h1><b><font face='Arial' size='14'>Game Difficulty on 'Boss Lairs'</font></b></h1><font face='Arial' size ='12'><br>You're about to embark on a special adventure against a 'boss' that is just for you and your Crew.<br><br>This encounter is balanced so you can be successful as a solo player, but you only gain modest rewards that way.<br><br>On the other hand, the encounter is extremely challenging with a large Crew...but you each get *much* larger rewards if you beat the encounter in that fashion.<br><br>So in other words:<br><br><b>More crewmembers means more challenge, but also means more reward.</b><br><br>Play at the challenge (and reward) level you desire.<br><br>]]></zOMG>"
dollStart8.flag = "Z5KatKokeshiSpawnOK"
dollStart8.exec = { event ->
	event.player.updateQuest( 47, "Katsumi-VQS" )  //Start the Katsumi's Kokeshi Doll quest
}
dollStart8.result = DONE
KatsDollStart.addDialog( dollStart8, tutorialNPC )

//---------------------------------------------------------
// Kokeshi Doll Quest - INTERIM                            
//---------------------------------------------------------

def KatKokInterim = Katsumi.createConversation("KatKokInterim", true, "QuestStarted_47:2", "!QuestStarted_47:3")

def KatInt1 = [id:1]
KatInt1.npctext = "Have you found my doll yet?"
KatInt1.playertext = "Nope. Nothing yet."
KatInt1.result = 2
KatKokInterim.addDialog(KatInt1, Katsumi)

def KatInt2 = [id:2]
KatInt2.npctext = "Well, I do not worry. You will."
KatInt2.result = DONE
KatKokInterim.addDialog(KatInt2, Katsumi)

//---------------------------------------------------------
// Kokeshi Doll Quest - SUCCESS                            
//---------------------------------------------------------

def KatKokSuccess = Katsumi.createConversation("KatKokSuccess", true, "QuestStarted_47:3", "!Z05RingRewardInProgress" )
KatKokSuccess.setUrgent(true)

def KatSucc1 = [id:1]
KatSucc1.npctext = "Have you found my doll yet?"
KatSucc1.playertext = "I know that I found it...but I can't guarantee that I destroyed it. Not permanently anyway. These things just appear out of thin air!"
KatSucc1.result = 2
KatKokSuccess.addDialog(KatSucc1, Katsumi)

def KatSucc2 = [id:2]
KatSucc2.npctext = "I wondered how so many could come from just my one little doll. What did you see?"
KatSucc2.playertext = "I'm not sure. The dolls seem to be marching with great purpose, but just in a big circle, and they don't seem to be...*doing*...anything!"
KatSucc2.result = 3
KatKokSuccess.addDialog(KatSucc2, Katsumi)

def KatSucc3 = [id:3]
KatSucc3.npctext = "Hmmm...so even though they look smart, perhaps they're just playing some pattern over and over again?"
KatSucc3.playertext = "That's a good way to describe it!"
KatSucc3.result = 4
KatKokSuccess.addDialog(KatSucc3, Katsumi)

def KatSucc4 = [id:4]
KatSucc4.npctext = "Okay. That makes me feel better. Like perhaps that means they are less likely to suddenly change their minds about the crystal and come charging in here."
KatSucc4.playertext = "I think you're okay on that front, Katsumi. At least for now."
KatSucc4.result = 5
KatKokSuccess.addDialog(KatSucc4, Katsumi)

def KatSucc5 = [id:5]
KatSucc5.npctext = "Thank you ever so much, %p."
KatSucc5.result = 6
KatKokSuccess.addDialog( KatSucc5, Katsumi )

def KatSucc6 = [id:6]
KatSucc6.npctext = "I have nothing else for you that is urgent. I release you from my Uncle's demand that you assist me. You should go to see him again soon. My Uncle always has tasks for someone as capable as you."
KatSucc6.playertext = "Hmmmm...maybe I will, Katsumi. Thanks for the tip!"
KatSucc6.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z05UncleKinReminder" )
	event.player.centerPrint( "Your PDA has been updated. A flag is set at the Ninja Bell as a reminder to go see Uncle Kin." )
	event.player.addMiniMapQuestLocation( "ninjaBell", "ZENGARDEN_101", 720, 360, "Speak with Uncle Kin" )
}
KatSucc6.result = 7
KatKokSuccess.addDialog( KatSucc6, Katsumi )

def KatSucc7 = [id:7]
KatSucc7.npctext = "One more thing. I would wish you to be better equipped for the trials ahead. Please...take from me one of the rings that have been left here at my Shrine."
KatSucc7.playertext = "Wow. Thanks!"
KatSucc7.exec = { event -> 
	event.player.removeMiniMapQuestActorName( "Katsumi-VQS" ) 
	event.player.setQuestFlag( GLOBAL, "Z05RingRewardInProgress" )
	player = event.player
	makeMainMenu( player )
}
KatKokSuccess.addDialog(KatSucc7, Katsumi)

//---------------------------------------------------------
// Post-Quest-Chain - Repeatable "DREAM WEAVER" Quest      
//---------------------------------------------------------

def DreamStart = Katsumi.createConversation("DreamStart", true, "!Z05TooOldForZenQuests", "QuestCompleted_47", "!QuestStarted_285", "!Z05DidDreamWeaverOnceAlready", "!Z05RingRewardInProgress" )

def DStart1 = [id:1]
DStart1.npctext = "Hello again, %p."
DStart1.playertext = "I know you said you didn't have anything urgent for me to do for you, but does that imply you do need help with something that's *not* urgent?"
DStart1.result = 2
DreamStart.addDialog( DStart1, Katsumi )

def DStart2 = [id:2]
DStart2.npctext = "You are clever, friend. Indeed, I do. There is an item I've been given from one who defeated a Kokeshi Doll. That item is something I call 'Mystic Weave' because it has odd...properties."
DStart2.playertext = "Odd properties? What do you mean?"
DStart2.result = 3
DreamStart.addDialog( DStart2, Katsumi )

def DStart3 = [id:3]
DStart3.npctext = "There are disciplines that I have learned from my father and uncle that allow me to see into this weave. I am almost certain that given enough experimentation, I might discern some pattern or reason behind the force that drives the Animated!"
DStart3.playertext = "That's fantastic!"
DStart3.result = 4
DreamStart.addDialog( DStart3, Katsumi )

def DStart4 = [id:4]
DStart4.npctext = "Also, unfortunately, it is also optimistic. The combination of things that I might try seems countless, and each time I try one of the tests, the Weave I use is destroyed."
DStart4.playertext = "Ah! So you need help gathering more of the Mystic Weave then?"
DStart4.result = 5
DreamStart.addDialog( DStart4, Katsumi )

def DStart5 = [id:5]
DStart5.npctext = "Yes, exactly! Would you do that for me, %p?"
DStart5.options = []
DStart5.options << [text: "Sure! I'll go gather Mystic Weave from the Kokeshis for you. How many do you need?", result: 7]
DStart5.options << [text: "No thanks, Katsumi. Maybe later.", result: 6]
DreamStart.addDialog( DStart5, Katsumi )

def DStart6 = [id:6]
DStart6.npctext = "Okay, %p. If you decide to help, please stop by. I'm sure I will still be working on it."
DStart6.result = DONE
DreamStart.addDialog( DStart6, Katsumi )

def DStart7 = [id:7]
DStart7.npctext = "Please bring me fifteen to start. Thank you for your help!"
DStart7.playertext = "No problem."
DStart7.quest = 285 //Start the DREAM WEAVER quest
DStart7.result = DONE
DStart7.exec = { event -> event.player.deletePlayerVar( "Z05MysticWeavesCollected" ) } // Guarding against bad player data if they've completed this quest before
DreamStart.addDialog( DStart7, Katsumi )

//---------------------------------------------------------
// Post-Quest-Chain - DREAM WEAVER (Alternate Start)       
// (occurs if the player has done this repeatable quest    
//  at least once already to avoid lengthy intro text)     
//---------------------------------------------------------

def DreamStartAlt = Katsumi.createConversation( "DreamStartAlt", true, "!Z05TooOldForZenQuests", "QuestCompleted_47", "!QuestStarted_285", "Z05DidDreamWeaverOnceAlready", "!Z05RingRewardInProgress" )

def DAlt1 = [id:1]
DAlt1.npctext = "Back again, %p? Do you want to help me gather more Mystic Weave for my experiments?"
DAlt1.options = []
DAlt1.options << [text: "Sure, Katsumi. I'd like to help!", result: 2]
DAlt1.options << [text: "Not this time, Katsumi. Thanks, anyway!", result: DONE]
DreamStartAlt.addDialog( DAlt1, Katsumi )

def DAlt2 = [id:2]
DAlt2.npctext = "Good! Thank you! Please gather fifteen more Mystic Weaves from the nearby Kokeshi Dolls."
DAlt2.playertext = "Yeah, I remember. Thanks, Katsumi."
DAlt2.quest = 285 //Start the DREAM WEAVER quest
DAlt2.exec = { event -> event.player.deletePlayerVar( "Z05MysticWeavesCollected" ) } // Guarding against bad player data if they've completed this quest before
DAlt2.result = DONE
DreamStartAlt.addDialog( DAlt2, Katsumi )


//========================================
// This conversation occurs if the player 
// is too old to receive repeatable tasks 
// in Zen Gardens anymore.                
//========================================
def TooOld = Katsumi.createConversation( "TooOld", true, "Z05TooOldForZenQuests", "QuestCompleted_47", "!Z05RingRewardInProgress" )

old1 = [id:1]
old1.npctext = "Hello, %p."
old1.playertext = "Good day to you, Katsumi. How are things with you?"
old1.result = 2
TooOld.addDialog( old1, Katsumi )

old2 = [id:2]
old2.npctext = "Fine as always. I am secure here in my shrine with this null crystal to keep the Animated at bay."
old2.playertext = "Is there anything I can help you with? Some task that needs doing?"
old2.result = 3
TooOld.addDialog( old2, Katsumi )

old3 = [id:3]
old3.npctext = "Oh, no, %p. You have grown beyond the petty tasks that I could present to you. If my Uncle cannot help you further, you should travel to Bass'ken Lake and see if you can aid the people there."
old3.playertext = "Okay, Katsumi. You be safe now."
old3.result = DONE
TooOld.addDialog( old3, Katsumi )

//---------------------------------------------------------
// Post-Quest-Chain - DREAM WEAVER (Interim)               
//---------------------------------------------------------

def DreamInterim = Katsumi.createConversation("DreamInterim", true, "QuestCompleted_47", "QuestStarted_285:2", "!Z05RingRewardInProgress" )

def DInt1 = [id:1]
DInt1.npctext = "Hello there, %p! You have gathered the Mystic Weaves for me?"
DInt1.playertext = "No, not yet. Where do I find the Mystic Weave again?"
DInt1.result = 2
DreamInterim.addDialog( DInt1, Katsumi )

def DInt2 = [id:2]
DInt2.npctext = "You should be able to find it by defeating Kokeshi Dolls in the area."
DInt2.playertext = "That's right. I forgot. Thanks!"
DInt2.result = 3
DreamInterim.addDialog( DInt2, Katsumi )

def DInt3 = [id:3]
DInt3.npctext = "It is never a problem. Thank you again for your help, %p."
DInt3.result = DONE
DreamInterim.addDialog( DInt3, Katsumi )

//---------------------------------------------------------
// Post-Quest-Chain - DREAM WEAVER (Success Attempts)      
//---------------------------------------------------------

def DreamSuccess = Katsumi.createConversation("DreamSuccess", true, "QuestCompleted_47", "QuestStarted_285:3", "!Z05MysticWeavesPresent", "!Z05MysticWeavesNotPresent", "!Z05RingRewardInProgress" )
DreamSuccess.setUrgent( true )

def DSucc1 = [id:1]
DSucc1.npctext = "Hello there, %p! You have gathered the Mystic Weaves for me?"
DSucc1.playertext = "I sure have! All fifteen of them, right here!"
DSucc1.result = 2
DreamSuccess.addDialog( DSucc1, Katsumi )

def DSucc2 = [id:2]
DSucc2.npctext = "Excellent. Thank you for bringing them to me!"
DSucc2.exec = { event ->
	runOnDeduct( event.player, 100381, 15, weavesPresent, weavesNotPresent ) // Removes five mystic weaves from player's inventory
}
DreamSuccess.addDialog(DSucc2, Katsumi)

//The player DOES have the Mystic Weaves
def happyResponse = Katsumi.createConversation( "happyResponse", true, "QuestCompleted_47", "QuestStarted_285:3", "Z05MysticWeavesPresent" )

def happy1 = [id:1]
happy1.npctext = "Well done, %p! I would be happy to do this with you again, if you ever need something to do."
happy1.playertext = "I'll keep that in mind, Katsumi. Thanks!"
happy1.flag = ["!Z05MysticWeavesPresent", "Z05DidDreamWeaverOnceAlready" ] //unset Z05MysticWeavesPresent so the quest can be done again; set the DidDreamWeaverOnceAlready flag so the user gets a shorter introductory speech the next time
happy1.exec = { event ->
	event.player.updateQuest( 285, "Katsumi-VQS" ) // Send the completion update for DREAM WEAVER quest
	event.player.removeMiniMapQuestActorName( "Katsumi-VQS" )
}
happy1.result = DONE
happyResponse.addDialog(happy1, Katsumi)

//The player DOES NOT have the Mystic Weaves
def sadResponse = Katsumi.createConversation( "sadResponse", true, "QuestCompleted_47", "QuestStarted_285:3", "Z05MysticWeavesNotPresent" )

def sad1 = [id:1]
sad1.npctext = "%p...you have not yet passed me the Mystic Weaves."
sad1.playertext = "That's because I can't find them! I must have sold them or given them to somebody else already."
sad1.result = 2
sadResponse.addDialog( sad1, Katsumi )

def sad2 = [id:2]
sad2.npctext = "That is all right. I'm sure that you can gather more. I will reward you when you return."
sad2.playertext = "Okay, Katsumi. Sorry about this."
sad2.result = 3
sadResponse.addDialog( sad2, Katsumi )

def sad3 = [id:3]
sad3.npctext = "It is never a problem, my friend. Good luck!"
sad3.flag = "!Z05MysticWeavesNotPresent" //unset the flag so it defaults back to before the feather check occurs
sad3.result = DONE
sadResponse.addDialog( sad3, Katsumi )

//Closures to check to ensure that the player actually still has five pink flamingo feathers at the time of the transaction
weavesPresent = { event ->
	event.player.setQuestFlag( GLOBAL, "Z05MysticWeavesPresent" )
	Katsumi.pushDialog( event.player, "happyResponse" )
}

weavesNotPresent = { event ->
	event.player.setQuestFlag( GLOBAL, "Z05MysticWeavesNotPresent" )
	Katsumi.pushDialog( event.player, "sadResponse" )
}

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 1
CLdecimal = 5

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["New Rings", "Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "New Rings" ) {
			makeNewRingMenu( player )
		}
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z05RingRewardInProgress" )
		}
	}
}

//====================================================
// NEW RINGS                                          
//====================================================
def makeNewRingMenu( player ) {
	titleString = "New Rings Menu"
	descripString = "These rings are new to the list this time."
	diffOptions = [ "Ghost", "Healing Halo", "Scaredy Cat", "Shuriken", "Turtle", "Main Menu"  ]
	
	uiButtonMenu( player, "newRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Hack", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Hornet Nest", "Hot Foot", "Shuriken", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Quicksand", "Scaredy Cat", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Pot Lid", "Teflon Spray", "Turtle", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Healing Halo", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Ghost", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.setQuestFlag( GLOBAL, "Z05KatsumiRingGrantReceived" )
	event.actor.updateQuest( 47, "Katsumi-VQS" ) //push the completion for Katsumi's Kokeshi Doll
}


