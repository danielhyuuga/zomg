import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_4.spawnSpawner("spawner1", "taiko_drum", 3)
spawner1.setPos( 510, 260 )
spawner1.setWanderBehaviorForChildren( 75, 200, 3, 6, 400)
spawner1.setWaitTime( 30, 50 )
spawner1.setMonsterLevelForChildren( 3.9 )