import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_402.spawnSpawner("spawner1", "cherry_fluff", 2) 
spawner1.setPos(870, 500)
spawner1.setWanderBehaviorForChildren( 25, 75, 2, 5, 200)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 3.2 )

def spawner2 = myRooms.ZENGARDEN_402.spawnSpawner("spawner2", "cherry_fluff", 2)
spawner2.setPos(490, 600)
spawner2.setWanderBehaviorForChildren( 25, 75, 2, 5, 200)
spawner2.setWaitTime( 50, 70 )
spawner2.setMonsterLevelForChildren( 3.2 )

//STARTUP LOGIC
spawner1.spawnAllNow()
spawner2.spawnAllNow()
