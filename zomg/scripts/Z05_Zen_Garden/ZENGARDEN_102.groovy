import com.gaiaonline.mmo.battle.script.*;

//lower half of room spawner
def spawner1 = myRooms.ZENGARDEN_102.spawnSpawner("spawner1", "taiko_drum", 3)
spawner1.setPos(840, 490)
spawner1.setWanderBehaviorForChildren( 75, 200, 2, 5, 450)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 3.6 )

//STARTUP LOGIC
spawner1.spawnAllNow()