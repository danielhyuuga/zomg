import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_201.spawnSpawner("ZG_201_Spawner1", "taiko_drum", 3) 
spawner1.setPos( 420, 255 )
spawner1.setWaitTime( 50, 70 )
spawner1.setWanderBehaviorForChildren( 75, 150, 3, 6, 300)
spawner1.addPatrolPointForChildren( "ZENGARDEN_201", 680, 460, 0.5)	
spawner1.addPatrolPointForChildren( "ZENGARDEN_201", 700, 130, 0.5)	
spawner1.addPatrolPointForChildren( "ZENGARDEN_201", 180, 170, 0.5)	
spawner1.addPatrolPointForChildren( "ZENGARDEN_201", 280, 480, 0.5)
spawner1.setMonsterLevelForChildren( 3.4 )


//STARTUP LOGIC
spawner1.spawnAllNow()
