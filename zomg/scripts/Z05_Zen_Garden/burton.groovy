import com.gaiaonline.mmo.battle.script.*;

def Burton = spawnNPC("BFG-Burton", myRooms.ZENGARDEN_401, 90, 420)
Burton.setDisplayName( "Burton" )

//---------------------------------------------------------
// Default Conversation - Durem Blockade Guard             
//---------------------------------------------------------

def BurtonGuard = Burton.createConversation( "BurtonGuard", true, "!Z5TouchOne", "!Z5TouchTwo", "!Z5TouchThree", "!Z5TouchFour", "!Z5TouchFive" )

def Touch1 = [id:1]
Touch1.npctext = "Thanks for noticing me."
Touch1.playertext = "Do you have any freebies available today?"
Touch1.flag = "Z5TouchOne"
Touch1.exec = { event ->
	checkDailyChance(event.player, 226, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance( event.player, 226 )
			Burton.say( "Seems the only way I can seem to get anyone to notice me is by giving free stuff out." )
		} else {
			event.player.centerPrint( "You've already received your freebie from Burton today. Try again tomorrow!" )
		}
	})
}
Touch1.result = DONE
BurtonGuard.addDialog(Touch1, Burton)

def BurtonGuard2 = Burton.createConversation("BurtonGuard2", true, "Z5TouchOne")

def Touch2 = [id:2]
Touch2.npctext = "Yes, yes. I see you."
Touch2.playertext = "Do you have any freebies available today?"
Touch2.flag = ["Z5TouchTwo", "!Z5TouchOne"]
Touch2.exec = { event ->
	checkDailyChance(event.player, 226, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 226)
			Burton.say("I see you want free stuff too.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Burton today. Try again tomorrow!" )
		}
	})
}
Touch2.result = DONE
BurtonGuard2.addDialog(Touch2, Burton)

def BurtonGuard3 = Burton.createConversation("BurtonGuard3", true, "Z5TouchTwo")

def Touch3 = [id:3]
Touch3.npctext = "What? What do you want?!? Speak up!"
Touch3.playertext = "Do you have any freebies available today?"
Touch3.flag = ["Z5TouchThree", "!Z5TouchTwo"]
Touch3.exec = { event ->
	checkDailyChance(event.player, 226, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 226)
			Burton.say("I should have known you wanted a freebie.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Burton today. Try again tomorrow!" )
		}
	})
}
Touch3.result = DONE
BurtonGuard3.addDialog(Touch3, Burton)

def BurtonGuard4 = Burton.createConversation("BurtonGuard4", true, "Z5TouchThree")

def Touch4 = [id:4]
Touch4.npctext = "Stop poking me!"
Touch4.playertext = "Do you have any freebies available today?"
Touch4.flag = ["Z5TouchFour", "!Z5TouchThree"]
Touch4.exec = { event ->
	checkDailyChance(event.player, 226, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 226)
			Burton.say("Fine! Take this, and stop poking me!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Burton today. Try again tomorrow!" )
		}
	})
}
Touch4.result = DONE
BurtonGuard4.addDialog(Touch4, Burton)

def BurtonGuard5 = Burton.createConversation("BurtonGuard5", true, "Z5TouchFour")

def Touch5 = [id:5]
Touch5.npctext = "Why do you keep touching me? Back away!"
Touch5.playertext = "Do you have any freebies available today?"
Touch5.flag = ["Z5TouchFive", "!Z5TouchFour"]
Touch5.exec = { event ->
	checkDailyChance(event.player, 226, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 226)
			Burton.say("Look, if I give you this you have to promise to stop touching me. It's not polite!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Burton today. Try again tomorrow!" )
		}
	})
}
Touch5.result = DONE
BurtonGuard5.addDialog(Touch5, Burton)

def BurtonGuard6 = Burton.createConversation("BurtonGuard6", true, "Z5TouchFive")

def Touch6 = [id:6]
Touch6.npctext = "That's IT! I'm telling Olivia! Leave me ALONE!"
Touch6.playertext = "Do you have any freebies available today?"
Touch6.flag = ["Z5TouchSix", "!Z5TouchFive"]
Touch6.exec = { event ->
	checkDailyChance(event.player, 226, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 226)
			Burton.say("Oh fine. Take this... but if you keep harassing me I really will tell Olivia!")
		} else {
			event.player.centerPrint( "You've already received your freebie from Burton today. Try again tomorrow!" )
		}
	})
}
Touch6.result = DONE
BurtonGuard6.addDialog(Touch6, Burton)