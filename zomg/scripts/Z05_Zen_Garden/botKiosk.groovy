/*

import com.gaiaonline.mmo.battle.script.*;

//Coordinates for botKiosks

// BASSKEN_304, 920, 430
// Aqueduct_602, 580, 760
// Beach_402, 460, 350

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("kioskPosition", "markerOther", "ZENGARDEN_502", 240, 420, "HyperNet Access")

kiosk = makeSwitch( "botKiosk", myRooms.ZENGARDEN_502, 240, 420 )
kiosk.unlock()
kiosk.off()
kiosk.setRange( 200 )
kiosk.setMouseoverText("Access the HyperNet")

def clickKiosk = { event ->
	kiosk.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk.whenOn( clickKiosk )


//====================================================
// TUTORIAL TREE                                      
//====================================================

kioskWidth = 300

def giveReward( event ) {
	//gold reward
	goldGrant = random( 50, 150 )
	event.player.centerPrint( "You receive ${goldGrant} gold!" )
	event.player.grantCoins( goldGrant )

	//orb reward
	orbGrant = 2
	event.player.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
	event.player.grantQuantityItem( 100257, orbGrant )
}
	
def synchronized makeKioskMenu( player ) {
	titleString = "The HyperNet"
	descripString = "Choose Game Help to access loads of information about the game, or one of the other categories for easy downloads of useful tidbits."
	diffOptions = ["Game Help", "Tutorials", "Area Info", "Exit HyperNet"]
	
	uiButtonMenu( player, "kioskMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Game Help" ) {
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXGameHelpSelected" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXGameHelpSelected" )
				//gold reward
				goldGrant = random( 50, 150 )
				event.actor.centerPrint( "You receive ${goldGrant} gold!" )
				event.actor.grantCoins( goldGrant )

				//orb reward
				orbGrant = 2
				event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				event.actor.grantQuantityItem( 100257, orbGrant )
			}
			showWidget( event.actor, "GameHelp", true, true )
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
		if( event.selection == "Tutorials" ) {
			makeTutorialMenu( player )
		}
		if( event.selection == "Area Info" ) {
			makeAreaMenu( player )
		}
		if( event.selection == "Exit HyperNet" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
	}
}

def makeAreaMenu( player ) {
	titleString = "Area Info"
	descripString = "A small archive of stories and tidbits of the area around you."
	diffOptions = ["Zen Gardens", "Main Menu"]
	
	uiButtonMenu( player, "fictionMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Zen Gardens" ) {
			tutorialNPC.pushDialog( event.actor, "zenGardens" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}
	


def makeTutorialMenu( player ) {
	titleString = "Tutorials"
	descripString = "These tutorials give you quick overviews on various features in the game."
	diffOptions = ["Clans", "Crew Advantages", "Suppressing CL", "Ring Sets", "Main Menu"]
	
	uiButtonMenu( player, "tutorialMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Clans" ) {
			tutorialNPC.pushDialog( event.actor, "clanInfo" )
		}
		if( event.selection == "Crew Advantages" ) {
			tutorialNPC.pushDialog( event.actor, "crewAdv" )
		}
		if( event.selection == "Suppressing CL" ) {
			tutorialNPC.pushDialog( event.actor, "suppressCL" )
		}
		if( event.selection == "Ring Sets" ) {
			tutorialNPC.pushDialog( event.actor, "ringSets" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// TUTORIAL MENU                                                                        
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//ADVANCED COMBAT TUTORIAL                                                              
//--------------------------------------------------------------------------------------
clanInfo = tutorialNPC.createConversation( "clanInfo", true )

def clan1 = [id:1]
clan1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Clans</font></b></h1><br><font face='Arial' size ='12'>Clans offer two main advantages to their members:<br><br>1) Clan members can easily communicate with one another, regardless of world location, by using the 'Clan' chat channel, and;<br><br>2) Clans automatically get their own dedicated forum on the Gaia main site, allowing out-of-game communication between members.<br><br>More advantages to Clans will appear in the future.<br><br>To register a Clan, either click on the door next to Olivia in Barton Town, or go to the 'Guilds' page on the Gaia main site, and look for the 'zOMG! Clans' button.<br><br>]]></zOMG>"
clan1.exec = { event ->
	if( event.actor.getRoom() == myRooms.ZENGARDEN_502 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXClanInfo" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXClanInfo" )
			giveReward( event )
		}
	}
}
clan1.result = DONE
clanInfo.addDialog( clan1, tutorialNPC )

//--------------------------------------------------------------------------------------
//CREW ADVANTAGES TUTORIAL                                                              
//--------------------------------------------------------------------------------------
crewAdv = tutorialNPC.createConversation( "crewAdv", true )

def crew1 = [id:1]
crew1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Advantages to Crewing</font></b></h1><br><font face='Arial' size ='12'>Crews are your friends.<br><br>First of all, you do *not* split loot with your Crewmates. So more loot is created for each monster defeated so that your personal gain is the same as if you were solo. However...<br><br>1) You can defeat more monsters while Crewed, so you'll end up with a lot more loot that way than if you solo, and;<br><br>2) Gold rewards *are* multiplied up, so you will gain more gold while Crewed than you will while soloing.<br><br>Additionally, when you solo, you can't use more than eight rings. But when Crewed, your Crewmates can use their rings on *you* and you can be a lot tougher than when you solo.<br><br>]]></zOMG>"
crew1.exec = { event ->
	if( event.actor.getRoom() == myRooms.ZENGARDEN_502 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXCrewAdv" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXCrewAdv" )
			giveReward( event )
		}
	}
}
crew1.result = DONE
crewAdv.addDialog( crew1, tutorialNPC )

//--------------------------------------------------------------------------------------
//SUPPRESS CL TUTORIAL                                                                  
//--------------------------------------------------------------------------------------
suppressCL = tutorialNPC.createConversation( "suppressCL", true )

def suppress1 = [id:1]
suppress1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Suppressing Your CL</font></b></h1><br><font face='Arial' size ='12'>Once you've built your overall Charge Level up a bit, you'll find that certain events and hunting areas are off-limits to you because you're too powerful.<br><br>Never fear! You can still play all that content!<br><br>Click on the 'Options' button on your Control Bar and then choose the 'SUPPRESS CL' option.<br><br>Then you can easily drag your overall CL down to the level you want to play at and join your friends for fun!<br><br>To reset your CL back up to max, just head to the Null Chamber and choose 'SUPPRESS CL' again. You can't raise your CL level while you're 'in the field', but you can reset it easily while in the Null Chamber.<br><br>]]></zOMG>"
suppress1.exec = { event ->
	if( event.actor.getRoom() == myRooms.ZENGARDEN_502 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXSuppressCL" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXSuppressCL" )
			giveReward( event )
		}
	}
}
suppress1.result = DONE
suppressCL.addDialog( suppress1, tutorialNPC )

//--------------------------------------------------------------------------------------
//RING SET TUTORIAL                                                                     
//--------------------------------------------------------------------------------------
ringSets = tutorialNPC.createConversation( "ringSets", true )

def set1 = [id:1]
set1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Ring Sets</font></b></h1><br><font face='Arial' size ='12'>So you've got a bunch of rings now and you think you've got the hang of it, right?<br><br>But wait! There's more!<br><br>If you wear your Rings in certain combinations, there are bonuses you can unlock for even more power!<br><br>Each Ring Set is composed of four unique rings. If all four of those rings are worn on a single hand (slots 1-4 or 5-8), then you get the bonus automatically.<br><br>The various Ring Sets are fully details in GAME HELP on the 'Ring Sets' page, so check it out!<br><br>Ring Sets are completely optional. Use them if they fit your play style!<br><br>]]></zOMG>"
set1.exec = { event ->
	if( event.actor.getRoom() == myRooms.ZENGARDEN_502 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXRingSets" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXRingSets" )
			giveReward( event )
		}
	}
}
set1.result = DONE
ringSets.addDialog( set1, tutorialNPC )


//======================================================================================
//======================================================================================
// AREA MENU                                                                            
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//BILL'S RANCH BACKGROUND                                                               
//--------------------------------------------------------------------------------------
def zenGardens = tutorialNPC.createConversation( "zenGardens", true )

def zen1 = [id:1]
zen1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>The Zen Gardens</font></b></h1><br><font face='Arial' size ='12'>The Zen Gardens area started as a small picnic location in the middle of a big grassy field.<br><br>Eventually it was formally adopted by the Chyaku Norisu and used by its members for concentration and focus studies, grooming the garden a bit at a time.<br><br>What once was just a sunny glen has been cultivated over time into an area which is now a tourist destination for Barton Town!<br><br>]]></zOMG>"
zen1.exec = { event ->
	if( event.actor.getRoom() == myRooms.ZENGARDEN_502 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXZenGardens" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXZenGardens" )
			giveReward( event )
		}
	}
}
zen1.result = DONE
zenGardens.addDialog( zen1, tutorialNPC )

*/
