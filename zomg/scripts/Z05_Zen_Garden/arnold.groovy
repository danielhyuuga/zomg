import com.gaiaonline.mmo.battle.script.*;

def Arnold = spawnNPC("BFG-Arnold", myRooms.ZENGARDEN_305, 720, 650)
Arnold.setRotation( 135 )
Arnold.setDisplayName( "Arnold" )

onQuestStep( 284, 2 ) { event -> event.player.addMiniMapQuestActorName( "BFG-Arnold" ) }

//---------------------------------------------------------
// Default Conversation - West Gate Guard                  
//---------------------------------------------------------


def ArnoldGuard = Arnold.createConversation("ArnoldGuard", true, "!Z05ArnoldBridge", "!Z05KatsumiAlreadyMet", "!QuestStarted_57", "!QuestCompleted_57")

def Arnold1 = [id:1]
Arnold1.npctext = "Greetings, citizen. Keep your head up out here. This area is dangerous."
Arnold1.playertext = "Dangerous? How so?"
Arnold1.result = 2
ArnoldGuard.addDialog(Arnold1, Arnold)

def Arnold2 = [id:2]
Arnold2.npctext = "The Animated, of course! They're all over out here. For some reason, they don't seem to approach the Gate very closely, but if you stick your neck out there, one of them is bound to try to take it off for you."
Arnold2.playertext = "Hmmm...sounds challenging. Thanks for the warning."
Arnold2.result = 3
ArnoldGuard.addDialog(Arnold2, Arnold)

def Arnold3 = [id:3]
Arnold3.npctext = "No problem. Good luck to you!"
Arnold3.result = DONE
ArnoldGuard.addDialog(Arnold3, Arnold)


//======================================
// DIMMING THE LANTERNS                 
// (Repeatable Kill Quest, only avail-  
// able at night and must be completed  
// that same night.                     
//======================================

playerSet305 = [] as Set

myManager.onEnter( myRooms.ZENGARDEN_305 ) { event ->
	if( isPlayer( event.actor ) ) {
		
		playerSet305 << event.actor
		
		//Check to see if the player is too old for Zen Gardens repeatable quests
		if( event.actor.getConLevel() >= 4.1 ) {
			event.actor.setQuestFlag( GLOBAL, "Z05TooOldForZenQuests" )
		} else {
			event.actor.unsetQuestFlag( GLOBAL, "Z05TooOldForZenQuests" )
		}
		
		//now check to see if the DIMMING THE LANTERNS quest can be active or not (day or night)
		checkLanternQuestStatus( event )
	}	
}	

myManager.onExit( myRooms.ZENGARDEN_305 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet305.remove( event.actor )
	}
}

def checkLanternQuestStatus( event ) {
	//Check to see if it's night-time.
	if( ( gst() >= 1800 && gst() <= 2359 ) || ( gst() >= 0 && gst() < 600 ) ) { 
		event.actor.setQuestFlag( GLOBAL, "Z05NighttimeTrue" )
		
		//Fetch the time stored in the playerVar for when the player started the DIMMING THE LANTERNS task...store it as a longValue() so it can be read properly
		dimmingStartTime = event.actor.getPlayerVar( "Z05DimmingStartTime" ).longValue()
		
		if( event.actor.isOnQuest( 284 ) ) {
			//if the current time is less or equal to that starting time + 70 minutes (the length of Gaia night in minutes), then it IS the same day
			if(  System.currentTimeMillis() <= dimmingStartTime + 4200000 ) { //4200000 ms = 70 minutes, the real-time duration of a full night cycle
				event.actor.setQuestFlag( GLOBAL, "Z05SameDayTrue" )
			//if the current time is greater than the start time + 70 minutes, then it's not the same day
			} else {
				event.actor.unsetQuestFlag( GLOBAL, "Z05SameDayTrue" )
			}
		}
	} else {
		event.actor.unsetQuestFlag( GLOBAL, "Z05NighttimeTrue" )
	}
}

	

//====================================================
// DIMMING THE LANTERNS                               
// Player can get this at any time after finishing    
// Kat's Kokeshi Doll quest.                          
//====================================================
def LanternStart = Arnold.createConversation( "LanternStart", true, "!QuestStarted_284", "Z05NighttimeTrue", "!Z05TooOldForZenQuests" ) 

LanStart1 = [id:1]
LanStart1.npctext = "You look battle hardened, citizen."
LanStart1.options = []
LanStart1.options << [text: "I've been through a fracas or two.", result: 2 ]
LanStart1.options << [text: "Yeah? Who wants to know?", result: 3 ]
LanternStart.addDialog( LanStart1, Arnold )

LanStart2 = [id:2]
LanStart2.npctext = "I thought so. You've got the look. Listen, I could use your help if you're interested."
LanStart2.options = []
LanStart2.options << [text: "I suppose I am. What do you need?", result: 4]
LanStart2.options << [text: "No thanks, friend. I'm a bit tired right now.", result: 10]
LanternStart.addDialog( LanStart2, Arnold )

LanStart3 = [id:3]
LanStart3.npctext = "The name's Arnold. Why? Did I offend you somehow?"
LanStart3.options = []
LanStart3.options << [text: "No, no. I'm just tired. It's crazy out there and I've been fighting a lot.", result: 2]
LanStart3.options << [text: "Nah. I'm just not in the mood to talk right now.", result: 10 ]
LanternStart.addDialog( LanStart3, Arnold )

LanStart4 = [id:4]
LanStart4.npctext = "Well, it's just me and Paula over there to guard this gate into Town, and during the daytime, that's just not a big problem."
LanStart4.playertext = "I'm sensing a 'but...' coming in this conversation."
LanStart4.result = 5
LanternStart.addDialog( LanStart4, Arnold )

LanStart5 = [id:5]
LanStart5.npctext = "But...at nighttime, those creepy, licky lanterns come out, and quite frankly, they're so dang random that I don't trust them to just suddenly decide to storm the gate for no reason at all!"
LanStart5.playertext = "Do you want me to stand guard with you or something?"
LanStart5.result = 6
LanternStart.addDialog( LanStart5, Arnold )

LanStart6 = [id:6]
LanStart6.npctext = "No. We've got that part covered. But it would sure ease my mind if you could sort of 'thin the herd' on the lanterns for me tonight, reducing their numbers before morning. If you could do that, I'm sure I could find a way to reward you for the effort."
LanStart6.options = []
LanStart6.options << [text: "Sure! I'd be glad to help!", result: 7]
LanStart6.options << [text: "Really? What kind of reward are we talking here?", result: 8]
LanStart6.options << [text: "Hmmm...no. I'll help the situation but not this way. Not right now. Good luck to you!", result: 9]
LanternStart.addDialog( LanStart6, Arnold )

LanStart7 = [id:7]
LanStart7.npctext = "Great! Then tonight, before the cherry fluffs start appearing at dawn, I need you to take out at least twenty of those crazy lanterns. Remember...get back to me before dawn, or the deal's off."
LanStart7.playertext = "Okay. I'll be back before dawn!"
LanStart7.quest = 284 //Start the DIMMING THE LANTERNS quest
LanStart7.exec = { event ->
	curTime = System.currentTimeMillis()
	event.actor.setPlayerVar( "Z05DimmingStartTime", curTime )
}
LanStart7.result = DONE
LanternStart.addDialog( LanStart7, Arnold )

LanStart8 = [id:8]
LanStart8.npctext = "Oh, the standard stuff, I guess. Some gold? Maybe an orb or two? How does that sound?"
LanStart8.options = []
LanStart8.options << [text: "Good enough! Sign me up!", result: 7]
LanStart8.options << [text: "I think I can do better somewhere else. Good luck to you!", result: 9]
LanternStart.addDialog( LanStart8, Arnold )

LanStart9 = [id:9]
LanStart9.npctext = "Hmmm...well, okay. Don't blame me if you come back and find Barton covered in saliva after the lanterns invade."
LanStart9.playertext = "Ummm...I'll try real hard not to blame you if that happens. Good luck."
LanStart9.result = DONE
LanternStart.addDialog( LanStart9, Arnold )

LanStart10 = [id:10]
LanStart10.npctext = "Well, okay, citizen. Why don't you take a breather and come back some other time. We'll talk then."
LanStart10.playertext = "Sounds good. Thanks. Be safe."
LanStart10.result = DONE
LanternStart.addDialog( LanStart10, Arnold )

//========================================
// What Arnold says during the daytime,   
// when he won't offer the DIMMING THE    
// LANTERNS quest                         
//========================================
def LanternDayFiller = Arnold.createConversation( "LanternDayFiller", true, "!QuestStarted_284", "!Z05NighttimeTrue", "!QuestStarted_57:3", "QuestCompleted_47", "!Z05TooOldForZenQuests" )

filler1 = [id:1]
filler1.npctext = "Hey there, citizen!"
filler1.playertext = "Hi there! Is there anything that needs doing around here?"
filler1.result = 2
LanternDayFiller.addDialog( filler1, Arnold )

filler2 = [id:2]
filler2.npctext = "Nah. It's pretty boring here most of the time during the day. The dolls keep to themselves, and the cherry fluffs don't bother anyone if you just leave 'em alone."
filler2.playertext = "Oh, that's too bad. I was kinda hoping to find something to do."
filler2.result = 3
LanternDayFiller.addDialog( filler2, Arnold )

filler3 = [id:3]
filler3.npctext = "Well, if that's all you need, I'd be happy to fill your time. Come by tonight, after it gets dark...when those creepy lanterns come out. I'll have something for you then."
filler3.playertext = "After nightfall. Okay. Got it. Thanks!"
filler3.result = 4
LanternDayFiller.addDialog( filler3, Arnold )

filler4 = [id:4]
filler4.npctext = "See you then!"
filler4.exec = { event ->
	myManager.schedule(2) { checkLanternQuestStatus( event ) } //just in case the player sat in the room with Arnold between the transition of day to night, check the time of day after rejecting the player once and then allow the quest thereafter
}
filler4.result = DONE
LanternDayFiller.addDialog( filler4, Arnold )

//========================================
// What Arnold says when the player is    
// too old to get the quests anymore.     
//========================================
def TooOld = Arnold.createConversation( "TooOld", true, "!QuestStarted_57:3", "QuestCompleted_47", "Z05TooOldForZenQuests" )

old1 = [id:1]
old1.npctext = "Greetings! How's the road treating you?"
old1.playertext = "The road's fine. It's the Animated that take it out of you."
old1.result = 2
TooOld.addDialog( old1, Arnold )

old2 = [id:2]
old2.npctext = "I hear ya there!"
old2.playertext = "Have any tasks that need doing?"
old2.result = 3
TooOld.addDialog( old2, Arnold )

old3 = [id:3]
old3.npctext = "Nope. Nothing that would be worthy of your skills anyway. I've heard that Bass'ken Lake has some fierce badness going on. Maybe you should head up there and talk to Old Man Logan!"
old3.playertext = "Thanks, Arnold. I might just do that."
old3.exec = { event ->
	event.player.centerPrint( "You must be level 4.0 or lower to do Arnold's repeatable task." )
	myManager.schedule(2) { event.player.centerPrint( "Use the CHANGE LEVEL option in the MENU to lower your level." ) }
	suppressSet << event.player
	checkOldPlayersForSuppression()
}
old3.result = DONE
TooOld.addDialog( old3, Arnold )


suppressSet = [] as Set

def checkOldPlayersForSuppression() {
	if( !suppressSet.isEmpty() && !playerSet305.isEmpty() ) {
		suppressSet.clone().each{ 
			if( it.getConLevel() < 4.1 ) {
				it.unsetQuestFlag( GLOBAL, "Z05tooOldForZenQuests" )
				suppressSet.remove( it )
			}
		}
		if( playerSet305.isEmpty() ) { suppressSet.clear() }
		myManager.schedule(2) { checkOldPlayersForSuppression() }
	}
}

//========================================
// DIMMING THE LANTERNS (Interim 1)       
// (If the player still has time to       
//  complete the quest before dawn)       
//========================================

def LanternInterimA = Arnold.createConversation( "LanternInterimA", true, "QuestStarted_284:2", "Z05NighttimeTrue" ) 

LanIntA1 = [id:1]
LanIntA1.npctext = "So...how's it going?"
LanIntA1.playertext = "I haven't hit your quota yet, but I'm working on it."
LanIntA1.result = 2
LanternInterimA.addDialog( LanIntA1, Arnold )

LanIntA2 = [id:2]
LanIntA2.npctext = "Good, good! Every one fewer of those lunatic lamps makes me feel a bit safer. Thanks!"
LanIntA2.playertext = "You bet. See you soon."
LanIntA2.result = 3
LanternInterimA.addDialog( LanIntA2, Arnold )

LanIntA3 = [id:3]
LanIntA3.npctext = "Remember...before dawn, or it just doesn't matter to me!"
LanIntA3.exec = { event ->
	myManager.schedule(2) { checkLanternQuestStatus( event ) }
}
LanIntA3.result = DONE
LanternInterimA.addDialog( LanIntA3, Arnold )

//========================================
// DIMMING THE LANTERNS (Interim 2)       
// (If the player has failed the time     
//  limit already )                       
//========================================

def LanternInterimB = Arnold.createConversation( "LanternInterimB", true, "QuestStarted_284:2", "!Z05NighttimeTrue" )

LanIntB1 = [id:1]
LanIntB1.npctext = "Oh! Too late! You had to finish up by dawn, remember?"
LanIntB1.playertext = "But..."
LanIntB1.result = 2
LanternInterimB.addDialog( LanIntB1, Arnold )

LanIntB2 = [id:2]
LanIntB2.npctext = "I don't mind you trying again tonight, but I warned you...there's no reward if you can't do the job during the night, remember?"
LanIntB2.playertext = "Okay, okay. I remember."
LanIntB2.result = 3
LanternInterimB.addDialog( LanIntB2, Arnold )

LanIntB3 = [id:3]
LanIntB3.npctext = "But try again tonight! I'm sure you'll do better this time!"
LanIntB3.exec = { event ->
	event.player.removeQuest( 284 )
	event.player.centerPrint( "The DIMMING THE LANTERNS task has been removed from your task list." )
	myManager.schedule(2) { checkLanternQuestStatus( event ) }
}
LanIntB3.flag = "!Z05NighttimeTrue" //unset the flag so the quest can be done again
LanIntB3.result = DONE
LanternInterimB.addDialog( LanIntB3, Arnold )


//========================================
// DIMMING THE LANTERNS (Success)         
//========================================

def LanternSuccess = Arnold.createConversation( "LanternSuccess", true, "QuestStarted_284:3", "Z05NighttimeTrue", "Z05SameDayTrue" ) 

LanSucc1 = [id:1]
LanSucc1.npctext = "Well?"
LanSucc1.playertext = "I punched their lights out!"
LanSucc1.result = 2
LanternSuccess.addDialog( LanSucc1, Arnold )

LanSucc2 = [id:2]
LanSucc2.npctext = "Fantastic! And you got it done before dawn too! Great job!"
LanSucc2.playertext = "Thanks! That was kind of fun!"
LanSucc2.result = 3
LanternSuccess.addDialog( LanSucc2, Arnold )

LanSucc3 = [id:3]
LanSucc3.npctext = "Well...the job's open any time you want it! And here's your reward for being so prompt!"
LanSucc3.flag = ["!Z05NighttimeTrue", "!Z05SameDayTrue"]//unset the flags so the quest can be done again
LanSucc3.quest = 284 //complete the DIMMING THE LANTERNS quest
LanSucc3.exec = { event ->
	event.player.setPlayerVar( "Z05DimmingStartTime", 0 ) //reinitialize this variable to prevent issues in the future
	myManager.schedule(2) { checkLanternQuestStatus( event ) }
	event.player.removeMiniMapQuestActorName( "BFG-Arnold" )
}
LanSucc3.result = DONE
LanternSuccess.addDialog( LanSucc3, Arnold ) 

//========================================
// DIMMING THE LANTERNS (Time Failure)    
//========================================

def LanternFailure = Arnold.createConversation( "LanternFailure", true, "QuestStarted_284:3", "!Z05NighttimeTrue" )

LanFail1 = [id:1]
LanFail1.npctext = "Oh...too late! You had to finish up by dawn, remember?"
LanFail1.playertext = "But..."
LanFail1.result = 2
LanternFailure.addDialog( LanFail1, Arnold )

LanFail2 = [id:2]
LanFail2.npctext = "I don't mind you trying again tonight, but I warned you...no reward if you can't do the job during the night, remember?"
LanFail2.playertext = "Okay, okay. I remember."
LanFail2.result = 3
LanternFailure.addDialog( LanFail2, Arnold )

LanFail3 = [id:3]
LanFail3.npctext = "But try again tonight! I'm sure you'll do better this time!"
LanFail3.exec = { event ->
	event.player.removeQuest( 284 ) //the players fails the quest and it is removed from the task list
	event.player.centerPrint( "The DIMMING THE LANTERNS task has been removed from your task list." )
	event.player.removeMiniMapQuestActorName( "BFG-Arnold" )
	myManager.schedule(2) { checkLanternQuestStatus( event ) }
}
LanFail3.flag = "!Z05NighttimeTrue" //unset the flag so the quest can be done again
LanFail3.result = DONE
LanternFailure.addDialog( LanFail3, Arnold ) 

//========================================
// DIMMING THE LANTERNS (Time Failure - Not Same Day)
//========================================

def LanternFailure2 = Arnold.createConversation( "LanternFailure2", true, "QuestStarted_284:3", "!Z05SameDayTrue" )

LanFailTwo1 = [id:1]
LanFailTwo1.npctext = "Oh...too late! You had to finish up by dawn, remember?"
LanFailTwo1.playertext = "But..."
LanFailTwo1.result = 2
LanternFailure2.addDialog( LanFailTwo1, Arnold )

LanFailTwo2 = [id:2]
LanFailTwo2.npctext = "I don't mind you trying again tonight, but you need to do it all during a single evening, okay? Otherwise, I'm afraid the impact of reducing their numbers won't do much."
LanFailTwo2.playertext = "Okay, okay. I'll remember."
LanFailTwo2.result = 3
LanternFailure2.addDialog( LanFailTwo2, Arnold )

LanFailTwo3 = [id:3]
LanFailTwo3.npctext = "But try again tonight! I'm sure you'll do better this time!"
LanFailTwo3.exec = { event ->
	event.player.removeQuest( 284 ) //the players fails the quest and it is removed from the task list
	event.player.centerPrint( "The DIMMING THE LANTERNS task has been removed from your task list." )
	event.player.removeMiniMapQuestActorName( "BFG-Arnold" )
	myManager.schedule(2) { checkLanternQuestStatus( event ) }
}
LanFailTwo3.flag = "!Z05NighttimeTrue" //unset the flag so the quest can be done again
LanFailTwo3.result = DONE
LanternFailure2.addDialog( LanFailTwo3, Arnold ) 









