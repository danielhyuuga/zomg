import com.gaiaonline.mmo.battle.script.*;

def spawner2 = myRooms.ZENGARDEN_603.spawnSpawner("spawner2", "cherry_fluff", 2) 
spawner2.setPos(790, 200)
spawner2.setWanderBehaviorForChildren( 25, 75, 2, 5, 200)
spawner2.setWaitTime( 50, 70 )
spawner2.setMonsterLevelForChildren( 3.1 )

//STARTUP LOGIC
spawner2.spawnAllNow()
