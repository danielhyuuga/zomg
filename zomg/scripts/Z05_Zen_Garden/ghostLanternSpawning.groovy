import com.gaiaonline.mmo.battle.script.*;

//===========================================================
// GHOST LANTERN SPAWNERS                                    
//===========================================================

lantern103A = myRooms.ZENGARDEN_103.spawnSpawner("lantern103A", "ghost_lantern", 2) 
lantern103A.setPos(300, 210)
lantern103A.setWanderBehaviorForChildren( 75, 200, 2, 5, 350)
lantern103A.setWaitTime( 50, 70 )
lantern103A.setMonsterLevelForChildren( 3.7 )

lantern103B = myRooms.ZENGARDEN_103.spawnSpawner("lantern103B", "ghost_lantern", 3) 
lantern103B.setPos(725, 430)
lantern103B.setWanderBehaviorForChildren( 75, 200, 2, 5, 350)
lantern103B.setWaitTime( 30, 50 )
lantern103B.setMonsterLevelForChildren( 3.7 )

lantern104A = myRooms.ZENGARDEN_104.spawnSpawner("lantern104A", "ghost_lantern", 2) 
lantern104A.setPos(856, 473)
lantern104A.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
lantern104A.setWaitTime( 50, 70 )
lantern104A.setMonsterLevelForChildren( 3.8 )

lantern104B = myRooms.ZENGARDEN_104.spawnSpawner("lantern104B", "ghost_lantern", 3) 
lantern104B.setPos(230, 250)
lantern104B.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
lantern104B.setWaitTime( 30, 50 )
lantern104B.setMonsterLevelForChildren( 3.8 )

lantern105A = myRooms.ZENGARDEN_105.spawnSpawner("lantern105A", "ghost_lantern", 2) 
lantern105A.setPos(775, 390)
lantern105A.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
lantern105A.setWaitTime( 50, 70 )
lantern105A.setMonsterLevelForChildren( 3.9 )

lantern105B = myRooms.ZENGARDEN_105.spawnSpawner("lantern105B", "ghost_lantern", 2)
lantern105B.setPos(310, 140)
lantern105B.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
lantern105B.setWaitTime( 50, 70 )
lantern105B.setMonsterLevelForChildren( 3.9 )

lantern105C = myRooms.ZENGARDEN_105.spawnSpawner("lantern105C", "ghost_lantern", 2)
lantern105C.setPos(492, 416)
lantern105C.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
lantern105C.setWaitTime( 50, 70 )
lantern105C.setMonsterLevelForChildren( 3.9 )

lantern203A = myRooms.ZENGARDEN_203.spawnSpawner("lantern203A", "ghost_lantern", 4) 
lantern203A.setPos(865, 305)
lantern203A.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
lantern203A.setWaitTime( 10, 30 )
lantern203A.setMonsterLevelForChildren( 3.6 )

lantern203B = myRooms.ZENGARDEN_203.spawnSpawner("lantern203B", "ghost_lantern", 2)
lantern203B.setPos(290, 320)
lantern203B.setWanderBehaviorForChildren( 75, 200, 2, 5, 350)
lantern203B.setWaitTime( 50, 70 )
lantern203B.setMonsterLevelForChildren( 3.6 )

lantern205A = myRooms.ZENGARDEN_205.spawnSpawner("lantern205A", "ghost_lantern", 2) 
lantern205A.setPos(360, 150)
lantern205A.setWanderBehaviorForChildren( 40, 150, 2, 5, 300)
lantern205A.setWaitTime( 50, 70 )
lantern205A.setMonsterLevelForChildren( 3.8 )

lantern205B = myRooms.ZENGARDEN_205.spawnSpawner("lantern205B", "ghost_lantern", 2)
lantern205B.setPos(50, 160)
lantern205B.setWanderBehaviorForChildren( 40, 150, 2, 5, 300)
lantern205B.setWaitTime( 50, 70 )
lantern205B.setMonsterLevelForChildren( 3.8 )

lantern4 = myRooms.ZENGARDEN_4.spawnSpawner("lantern4", "ghost_lantern", 1)
lantern4.setPos( 600, 600 )
lantern4.setWanderBehaviorForChildren( 25, 150, 3, 6, 200)
lantern4.setWaitTime( 50, 70 )
lantern4.setMonsterLevelForChildren( 3.9 )

lantern5 = myRooms.ZENGARDEN_5.spawnSpawner("lantern5", "ghost_lantern", 1)
lantern5.setPos( 700, 600 )
lantern5.setWanderBehaviorForChildren( 25, 100, 3, 6, 150)
lantern5.setWaitTime( 50, 70 )
lantern5.setMonsterLevelForChildren( 4.0 )



//===========================================================
// CHERRY FLUFF SPAWNERS                                     
//===========================================================

cherry103A = myRooms.ZENGARDEN_103.spawnSpawner("cherry103A", "cherry_fluff", 2) 
cherry103A.setPos(300, 210)
cherry103A.setWanderBehaviorForChildren( 75, 200, 2, 5, 350)
cherry103A.setWaitTime( 50, 70 )
cherry103A.setMonsterLevelForChildren( 3.4 )

cherry103B = myRooms.ZENGARDEN_103.spawnSpawner("cherry103B", "cherry_fluff", 3) 
cherry103B.setPos(725, 430)
cherry103B.setWanderBehaviorForChildren( 75, 200, 2, 5, 350)
cherry103B.setWaitTime( 30, 50 )
cherry103B.setMonsterLevelForChildren( 3.4 )

cherry104A = myRooms.ZENGARDEN_104.spawnSpawner("cherry104A", "cherry_fluff", 2) 
cherry104A.setPos(856, 473)
cherry104A.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
cherry104A.setWaitTime( 50, 70 )
cherry104A.setMonsterLevelForChildren( 3.4 )

cherry104B = myRooms.ZENGARDEN_104.spawnSpawner("cherry104B", "cherry_fluff", 3) 
cherry104B.setPos(230, 250)
cherry104B.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
cherry104B.setWaitTime( 30, 50 )
cherry104B.setMonsterLevelForChildren( 3.4 )

cherry105A = myRooms.ZENGARDEN_105.spawnSpawner("cherry105A", "cherry_fluff", 2) 
cherry105A.setPos(775, 390)
cherry105A.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
cherry105A.setWaitTime( 50, 70 )
cherry105A.setMonsterLevelForChildren( 3.3 )

cherry105B = myRooms.ZENGARDEN_105.spawnSpawner("cherry105B", "cherry_fluff", 2)
cherry105B.setPos(310, 140)
cherry105B.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
cherry105B.setWaitTime( 50, 70 )
cherry105B.setMonsterLevelForChildren( 3.3 )

cherry105C = myRooms.ZENGARDEN_105.spawnSpawner("cherry105C", "cherry_fluff", 2)
cherry105C.setPos(492, 416)
cherry105C.setWanderBehaviorForChildren( 50, 100, 2, 5, 200)
cherry105C.setWaitTime( 50, 70 )
cherry105C.setMonsterLevelForChildren( 3.3 )

cherry203A = myRooms.ZENGARDEN_203.spawnSpawner("cherry203A", "cherry_fluff", 4) 
cherry203A.setPos(865, 305)
cherry203A.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
cherry203A.setWaitTime( 10, 30 )
cherry203A.setMonsterLevelForChildren( 3.4 )

cherry203B = myRooms.ZENGARDEN_203.spawnSpawner("cherry203B", "cherry_fluff", 2)
cherry203B.setPos(290, 320)
cherry203B.setWanderBehaviorForChildren( 75, 200, 2, 5, 350)
cherry203B.setWaitTime( 50, 70 )
cherry203B.setMonsterLevelForChildren( 3.4 )

cherry205A = myRooms.ZENGARDEN_205.spawnSpawner("cherry205A", "cherry_fluff", 2) 
cherry205A.setPos(360, 150)
cherry205A.setWanderBehaviorForChildren( 40, 150, 2, 5, 300)
cherry205A.setWaitTime( 50, 70 )
cherry205A.setMonsterLevelForChildren( 3.2 )

cherry205B = myRooms.ZENGARDEN_205.spawnSpawner("cherry205B", "cherry_fluff", 2)
cherry205B.setPos(50, 160)
cherry205B.setWanderBehaviorForChildren( 40, 150, 2, 5, 300)
cherry205B.setWaitTime( 50, 70 )
cherry205B.setMonsterLevelForChildren( 3.2 )


//===========================================================
// GST Spawning Script -- Ghost Lanterns only spawn at night 
//===========================================================
spawnedNightAlready = false
spawnedDayAlready = false

//When the server starts up, check to see what time of day it is and route accordingly.
def checkForNight() {
	if( ( gst() > 1800 && gst() <=2359 ) || ( gst() >= 0 && gst() < 600 ) ) { //if it's nighttime then...
		if( !spawnedNightAlready ) {
			spawnedNightAlready = true
			spawnedDayAlready = false
			spawnNightShift()
		}
	} else { //otherwise it's daytime...
		if( !spawnedDayAlready ) {
			spawnedDayAlready = true
			spawnedNightAlready = false
			spawnDayShift()
		}
	}
	myManager.schedule(300) { checkForNight() }
}

//The first task before spawning is to count how many ghosts are in the room.
def spawnNightShift() {
	//spawn the lanterns
	lantern103A.unStopSpawning(); lantern103A.spawnAllNow()
	lantern103B.unStopSpawning(); lantern103B.spawnAllNow()
	lantern104A.unStopSpawning(); lantern104A.spawnAllNow()
	lantern104B.unStopSpawning(); lantern104B.spawnAllNow()
	lantern105A.unStopSpawning(); lantern105A.spawnAllNow()
	lantern105B.unStopSpawning(); lantern105B.spawnAllNow()
	lantern105C.unStopSpawning(); lantern105C.spawnAllNow()
	lantern203A.unStopSpawning(); lantern203A.spawnAllNow()
	lantern203B.unStopSpawning(); lantern203B.spawnAllNow()
	lantern205A.unStopSpawning(); lantern205A.spawnAllNow()
	lantern205B.unStopSpawning(); lantern205B.spawnAllNow()
	lantern4.unStopSpawning(); lantern4.spawnAllNow()
	lantern5.unStopSpawning(); lantern5.spawnAllNow()
	//stop spawning the fluffs
	cherry103A.stopSpawning()
	cherry103B.stopSpawning()
	cherry104A.stopSpawning()
	cherry104B.stopSpawning()
	cherry105A.stopSpawning()
	cherry105B.stopSpawning()
	cherry105C.stopSpawning()
	cherry203A.stopSpawning()
	cherry203B.stopSpawning()
	cherry205A.stopSpawning()
	cherry205B.stopSpawning()
	//dispose of all existing fluffs
	myRooms.ZENGARDEN_103.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "cherry_fluff" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_104.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "cherry_fluff" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_105.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "cherry_fluff" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_203.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "cherry_fluff" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_205.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "cherry_fluff" ) {
			it.dispose()
		}
	}
}


//This script cleans up the ghosts after daytime occurs.
def spawnDayShift() {
	//spawn the fluffs
	cherry103A.unStopSpawning(); cherry103A.spawnAllNow()
	cherry103B.unStopSpawning(); cherry103B.spawnAllNow()
	cherry104A.unStopSpawning(); cherry104A.spawnAllNow()
	cherry104B.unStopSpawning(); cherry104B.spawnAllNow()
	cherry105A.unStopSpawning(); cherry105A.spawnAllNow()
	cherry105B.unStopSpawning(); cherry105B.spawnAllNow()
	cherry105C.unStopSpawning(); cherry105C.spawnAllNow()
	cherry203A.unStopSpawning(); cherry203A.spawnAllNow()
	cherry203B.unStopSpawning(); cherry203B.spawnAllNow()
	cherry205A.unStopSpawning(); cherry205A.spawnAllNow()
	cherry205B.unStopSpawning(); cherry205B.spawnAllNow()
	//stop spawning the lanterns
	lantern103A.stopSpawning()
	lantern103B.stopSpawning()
	lantern104A.stopSpawning()
	lantern104B.stopSpawning()
	lantern105A.stopSpawning()
	lantern105B.stopSpawning()
	lantern105C.stopSpawning()
	lantern203A.stopSpawning()
	lantern203B.stopSpawning()
	lantern205A.stopSpawning()
	lantern205B.stopSpawning()
	lantern4.stopSpawning()
	lantern5.stopSpawning()
	myRooms.ZENGARDEN_103.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "ghost_lantern" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_104.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "ghost_lantern" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_105.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "ghost_lantern" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_203.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "ghost_lantern" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_205.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "ghost_lantern" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_4.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "ghost_lantern" ) {
			it.dispose()
		}
	}
	myRooms.ZENGARDEN_5.getActorList().each { 
		if( isMonster( it ) && it.getMonsterType() == "ghost_lantern" ) {
			it.dispose()
		}
	}
}

//Clock that runs between time-checks
def alarmClockReset() {
	myManager.schedule(300) { checkForNight() } //Check the time of day every 5 minutes
}

//STARTUP LOGIC

checkForNight()