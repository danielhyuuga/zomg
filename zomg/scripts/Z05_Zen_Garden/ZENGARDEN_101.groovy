import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_101.spawnSpawner("ZG_101_Spawner1", "taiko_drum", 3) 
spawner1.setPos(250, 315)
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 5, 300)
spawner1.setWaitTime( 30, 50 )
spawner1.setMonsterLevelForChildren( 3.5 )


//STARTUP LOGIC
spawner1.spawnAllNow()