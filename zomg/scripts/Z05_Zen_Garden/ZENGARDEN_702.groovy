import com.gaiaonline.mmo.battle.script.*;

def spawner2 = myRooms.ZENGARDEN_702.spawnSpawner("spawner2", "cherry_fluff", 2) 
spawner2.setPos( 20, 250 )
spawner2.setWaitTime( 50, 70 )
spawner2.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
spawner2.setMonsterLevelForChildren( 2.9 )

//STARTUP LOGIC
spawner2.spawnAllNow()
