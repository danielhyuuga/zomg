import com.gaiaonline.mmo.battle.script.*;

def Paula = spawnNPC("BFG-Paula", myRooms.ZENGARDEN_305, 720, 300)
Paula.setRotation( 135 )
Paula.setDisplayName( "Paula" )

//---------------------------------------------------------
// Default Conversation - West Gate Guard                  
//---------------------------------------------------------

def PaulaGuard = Paula.createConversation("PaulaGuard", true)

def Paula1 = [id:1]
Paula1.npctext = "Greets. Been out in the Gardens yet?"
Paula1.options = []
Paula1.options << [text:"Oh yeah. I've been all over already.", result: 2]
Paula1.options << [text:"No. Is that a problem?", result: 3]
PaulaGuard.addDialog(Paula1, Paula)

def Paula2 = [id:2]
Paula2.npctext = "That's all right then. Good luck to you."
Paula2.result = DONE
PaulaGuard.addDialog(Paula2, Paula)

def Paula3 = [id:3]
Paula3.npctext = "No problem...but you'll want to watch your keester out there. If you're not real good yet with those new rings, then I advise you to head down to Village Greens instead of coming out here."
Paula3.options = []
Paula3.options << [text:"It's okay. I've got the hang of them now. I'll be fine.", result: 4]
Paula3.options << [text:"Why? What's special about the rings?", result: 5]
PaulaGuard.addDialog(Paula3, Paula)

def Paula4 = [id:4]
Paula4.npctext = "Okay. Stay sharp. See ya when I see ya."
Paula4.result = DONE
PaulaGuard.addDialog(Paula4, Paula)

def Paula5 = [id:5]
Paula5.npctext = "hehehe. That tells me exactly what I need to know. You need more practice. Head on down to speak with Leon in the Village Greens and he'll get you set up."
Paula5.playertext = "I'll take it under consideration. Thanks."
Paula5.result = 6
PaulaGuard.addDialog(Paula5, Paula)

def Paula6 = [id:6]
Paula6.npctext = "No problem at all. I'm just happy not to have to drag another well-intentioned, but under-skilled citizen out of the Gardens."
Paula6.playertext = "Heh. Gotcha."
Paula6.result = DONE
PaulaGuard.addDialog(Paula6, Paula)