import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.ZENGARDEN_401.spawnSpawner("spawner1", "cherry_fluff", 3)
spawner1.setPos(800, 200)
spawner1.setWanderBehaviorForChildren( 50, 120, 2, 5, 200)
spawner1.setWaitTime( 30, 50 )
spawner1.setMonsterLevelForChildren( 3.0 )

//STARTUP LOGIC
spawner1.spawnAllNow()
