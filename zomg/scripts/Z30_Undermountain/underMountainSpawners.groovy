//Script created by gfern

import spawner.createSpawner;
import difficulty.setLevel;

//-----------------------------------------------------
//Variable & Constant Init
//-----------------------------------------------------
level = 10;
underDifficulty = 1;
ltCounter = 0;
areaInit = false;

HATE_RADIUS = 500;
MIN_LEVEL = 10.5;
MAX_LEVEL = 10.5;
MIN_LEVEL_LT = 11;
MAX_LEVEL_LT = 11;
LT_MAX = 5;

//-----------------------------------------------------
//Maps and lists
//-----------------------------------------------------
underSpawnerList = [] as Set;
ltSpawnerRoomList = [] as Set;
underSpawnerListLT = [] as Set;
underMountainMonsters = [
	[name:"vampire_male_um",	level:10.5, spawner_list:underSpawnerList,	quantity:1], 
	[name:"vampire_female_um",	level:10.5, spawner_list:underSpawnerList,	quantity:1], 
	[name:"werewolf_um",		level:10.5, spawner_list:underSpawnerList,	quantity:1]
];
underMountainLTMonsters = [
	[name:"werewolf_LT_um",			level:11, spawner_list:underSpawnerListLT,	quantity:1], 
	[name:"vampire_batmonster_um",	level:11, spawner_list:underSpawnerListLT,	quantity:1]
];

difficultyMap = [1:0, 2:0.5, 3:1.0];
creatorOfSpawners = new createSpawner();

doNotSpawnLTsInRooms = [ myRooms.Under_2, myRooms.Under_101 ];

//-----------------------------------------------------
//Level adjusting
//-----------------------------------------------------
l = new setLevel();

def checkLevel() {
	if(myArea.getAllPlayers().size() > 0) {
		player = random(myArea.getAllPlayers());
		underDifficulty = player.getTeam().getAreaVar("Z30_Undermountain", "Z30DeadmansDifficulty");
		if(!underDifficulty) { underDifficulty = 2 }

		player.getCrew().each() {
			if(it.getConLevel() + difficultyMap.get(underDifficulty) > level) {
				levelAdjust();
			}
		}
	}
	
	myManager.schedule(30) { checkLevel(); }
}

def levelAdjust() {
	level = l.setSpawnerLevel(MIN_LEVEL_LT, MAX_LEVEL_LT, underDifficulty, difficultyMap, player, underSpawnerListLT);
	level = l.setSpawnerLevel(MIN_LEVEL, MAX_LEVEL, underDifficulty, difficultyMap, player, underSpawnerList);
}

onZoneIn() { event ->
	if(areaInit == false) {
		areaInit = true;
		checkLevel();
	}
}

//---------------------------------------------------
// Room activator
//---------------------------------------------------

activatedRooms = [] as Set
activationTasks = [:]
def activateRoom(room) {
	if(! activatedRooms.contains(room)) {
		log("Activating {}", room)
		activatedRooms << room
		myManager.stopListenForEnterExit(room) // [rc] Manage this separately if we ever add another onEnter to this room
		if(activationTasks.containsKey(room)) {
			activationTasks[room].cancel()
		}
		room.getActorList().each {
			if(isMonster(it)) {
				it.setAwarenessArc(360)
				it.getAI().setMiniEventSpec(null)
			}
		}
	}
	else {
		log("Room {} already activated", room)
	}
}

attackActivator = [
isAllowed : { attacker, target ->
	if(isMonster(target)) {
		activateRoom(target.getRoom())
	} else {
		log("ERROR! Got mini event spec check for {} by {}", target, attacker)
	}
	return true;
}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec;


//-----------------------------------------------------
//Spawners
//-----------------------------------------------------

//Guards 2
spawner2_1 = randomStoppedGuardSpawner(myRooms.Under_2, "spawner2_1", 1, 270, 540, "Under_2", HATE_RADIUS, 90);
spawner2_2 = randomStoppedGuardSpawner(myRooms.Under_2, "spawner2_2", 1, 280, 880, "Under_2", HATE_RADIUS, 350);
spawner2_3 = randomStoppedGuardSpawner(myRooms.Under_2, "spawner2_3", 1, 790, 580, "Under_2", HATE_RADIUS, 90);
spawner2_4 = randomStoppedGuardSpawner(myRooms.Under_2, "spawner2_4", 1, 1080, 380, "Under_2", HATE_RADIUS, 145);

//Roamers 2
spawner2_5 = randomStoppedSpawner(myRooms.Under_2, "spawner2_5", 1, 440, 810, "Under_2", HATE_RADIUS);
spawner2_5.addPatrolPointForChildren("Under_2", 800, 720, 0);
spawner2_5.addPatrolPointForChildren("Under_2", 990, 500, 10);
spawner2_5.addPatrolPointForChildren("Under_2", 800, 720, 0);
spawner2_5.addPatrolPointForChildren("Under_2", 440, 810, 10);

//Guards 5
spawner5_1 = randomStoppedGuardSpawner(myRooms.Under_5, "spawner5_1", 1, 340, 480, "Under_5", HATE_RADIUS, 90);
spawner5_2 = randomStoppedGuardSpawner(myRooms.Under_5, "spawner5_2", 1, 1130, 460, "Under_5", HATE_RADIUS, 145);
spawner5_3 = randomStoppedGuardSpawner(myRooms.Under_5, "spawner5_3", 1, 1020, 700, "Under_5", HATE_RADIUS, 90);
spawner5_4 = randomStoppedGuardSpawner(myRooms.Under_5, "spawner5_4", 1, 490, 650, "Under_5", HATE_RADIUS, 145);

//Roamers 5
spawner5_5 = randomStoppedSpawner(myRooms.Under_5, "spawner5_5", 1, 530, 360, "Under_5", HATE_RADIUS);
spawner5_5.addPatrolPointForChildren("Under_5", 830, 360, 0);
spawner5_5.addPatrolPointForChildren("Under_5", 1000, 480, 10);
spawner5_5.addPatrolPointForChildren("Under_5", 830, 360, 0);
spawner5_5.addPatrolPointForChildren("Under_5", 530, 360, 10);

spawner5_6 = randomStoppedSpawner(myRooms.Under_5, "spawner5_6", 1, 300, 600, "Under_5", HATE_RADIUS);
spawner5_6.addPatrolPointForChildren("Under_5", 540, 750, 0);
spawner5_6.addPatrolPointForChildren("Under_5", 780, 850, 10);
spawner5_6.addPatrolPointForChildren("Under_5", 540, 750, 0);
spawner5_6.addPatrolPointForChildren("Under_5", 300, 600, 10);

//Guards 7
spawner7_1 = randomStoppedGuardSpawner(myRooms.Under_7, "spawner7_1", 1, 1230, 380, "Under_7", HATE_RADIUS, 120);
spawner7_2 = randomStoppedGuardSpawner(myRooms.Under_7, "spawner7_2", 1, 390, 600, "Under_7", HATE_RADIUS, 120);
spawner7_3 = randomStoppedGuardSpawner(myRooms.Under_7, "spawner7_3", 1, 740, 600, "Under_7", HATE_RADIUS, 45);
spawner7_4 = randomStoppedGuardSpawner(myRooms.Under_7, "spawner7_4", 1, 850, 350, "Under_7", HATE_RADIUS, 90);
spawner7_5 = randomStoppedGuardSpawner(myRooms.Under_7, "spawner7_5", 1, 1120, 710, "Under_7", HATE_RADIUS, 260);

//Roamers 7
spawner7_6 = randomStoppedSpawner(myRooms.Under_7, "spawner7_6", 1, 950, 510, "Under_7", HATE_RADIUS);
spawner7_6.addPatrolPointForChildren("Under_7", 920, 680, 0);
spawner7_6.addPatrolPointForChildren("Under_7", 510, 720, 10);
spawner7_6.addPatrolPointForChildren("Under_7", 920, 680, 0);
spawner7_6.addPatrolPointForChildren("Under_7", 950, 510, 10);

//Guards 101
spawner101_1 = randomStoppedGuardSpawner(myRooms.Under_101, "spawner101_1", 1, 270, 390, "Under_101", HATE_RADIUS, 350);
spawner101_2 = randomStoppedGuardSpawner(myRooms.Under_101, "spawner101_2", 1, 610, 150, "Under_101", HATE_RADIUS, 240);
spawner101_3 = randomStoppedGuardSpawner(myRooms.Under_101, "spawner101_3", 1, 1050, 310, "Under_101", HATE_RADIUS, 350);
spawner101_4 = randomStoppedGuardSpawner(myRooms.Under_101, "spawner101_4", 1, 1380, 380, "Under_101", HATE_RADIUS, 240);

//Guards 203
spawner203_1 = randomStoppedGuardSpawner(myRooms.Under_203, "spawner203_1", 1, 900, 740, "Under_203", HATE_RADIUS, 90);
spawner203_2 = randomStoppedGuardSpawner(myRooms.Under_203, "spawner203_2", 1, 610, 740, "Under_203", HATE_RADIUS, 350);
spawner203_3 = randomStoppedGuardSpawner(myRooms.Under_203, "spawner203_3", 1, 510, 550, "Under_203", HATE_RADIUS, 90);

//Roamers 203
spawner203_4 = randomStoppedSpawner(myRooms.Under_203, "spawner203_4", 1, 520, 320, "Under_203", HATE_RADIUS);
spawner203_4.addPatrolPointForChildren("Under_203", 650, 540, 0);
spawner203_4.addPatrolPointForChildren("Under_203", 700, 590, 10);
spawner203_4.addPatrolPointForChildren("Under_203", 650, 540, 0);
spawner203_4.addPatrolPointForChildren("Under_203", 520, 320, 10);

spawner203_5 = randomStoppedSpawner(myRooms.Under_203, "spawner203_5", 1, 1200, 640, "Under_203", HATE_RADIUS);
spawner203_5.addPatrolPointForChildren("Under_203", 1190, 470, 0);
spawner203_5.addPatrolPointForChildren("Under_203", 1060, 350, 10);
spawner203_5.addPatrolPointForChildren("Under_203", 1190, 470, 0);
spawner203_5.addPatrolPointForChildren("Under_203", 1200, 640, 10);

//Guards 205
spawner205_1 = randomStoppedGuardSpawner(myRooms.Under_205, "spawner205_1", 1, 460, 460, "Under_205", HATE_RADIUS, 260);
spawner205_2 = randomStoppedGuardSpawner(myRooms.Under_205, "spawner205_2", 1, 1140, 640, "Under_205", HATE_RADIUS, 90);
spawner205_3 = randomStoppedGuardSpawner(myRooms.Under_205, "spawner205_3", 1, 1080, 320, "Under_205", HATE_RADIUS, 120);
spawner205_4 = randomStoppedGuardSpawner(myRooms.Under_205, "spawner205_4", 1, 600, 780, "Under_205", HATE_RADIUS, 350);
spawner205_5 = randomStoppedGuardSpawner(myRooms.Under_205, "spawner205_5", 1, 220, 520, "Under_205", HATE_RADIUS, 90);
spawner205_6 = randomStoppedGuardSpawner(myRooms.Under_205, "spawner205_6", 1, 830, 580, "Under_205", HATE_RADIUS, 350);

//Guards 207
spawner207_1 = randomStoppedGuardSpawner(myRooms.Under_207, "spawner207_1", 1, 360, 520, "Under_207", HATE_RADIUS, 90);
spawner207_2 = randomStoppedGuardSpawner(myRooms.Under_207, "spawner207_2", 1, 620, 330, "Under_207", HATE_RADIUS, 90);
spawner207_3 = randomStoppedGuardSpawner(myRooms.Under_207, "spawner207_3", 1, 720, 790, "Under_207", HATE_RADIUS, 90);
spawner207_4 = randomStoppedGuardSpawner(myRooms.Under_207, "spawner207_4", 1, 1190, 610, "Under_207", HATE_RADIUS, 90);
spawner207_5 = randomStoppedGuardSpawner(myRooms.Under_207, "spawner207_5", 1, 980, 420, "Under_207", HATE_RADIUS, 260);

//Roamers
spawner207_6 = randomStoppedSpawner(myRooms.Under_207, "spawner207_6", 1, 1100, 690, "Under_207", HATE_RADIUS);
spawner207_6.addPatrolPointForChildren("Under_207", 820, 640, 0);
spawner207_6.addPatrolPointForChildren("Under_207", 600, 460, 10);
spawner207_6.addPatrolPointForChildren("Under_207", 820, 640, 0);
spawner207_6.addPatrolPointForChildren("Under_207", 1100, 690, 10);

//Guards 401
spawner401_1 = randomStoppedGuardSpawner(myRooms.Under_401, "spawner401_1", 1, 860, 390, "Under_401", HATE_RADIUS, 120);
spawner401_2 = randomStoppedGuardSpawner(myRooms.Under_401, "spawner401_2", 1, 1030, 570, "Under_401", HATE_RADIUS, 120);
spawner401_3 = randomStoppedGuardSpawner(myRooms.Under_401, "spawner401_3", 1, 340, 460, "Under_401", HATE_RADIUS, 45);
spawner401_4 = randomStoppedGuardSpawner(myRooms.Under_401, "spawner401_4", 1, 540, 270, "Under_401", HATE_RADIUS, 45);
spawner401_5 = randomStoppedGuardSpawner(myRooms.Under_401, "spawner401_5", 1, 960, 760, "Under_401", HATE_RADIUS, 260);
spawner401_6 = randomStoppedGuardSpawner(myRooms.Under_401, "spawner401_6", 1, 600, 780, "Under_401", HATE_RADIUS, 350);

//Guards 403
spawner403_1 = randomStoppedGuardSpawner(myRooms.Under_403, "spawner403_1", 1, 1290, 530, "Under_403", HATE_RADIUS, 120);
spawner403_2 = randomStoppedGuardSpawner(myRooms.Under_403, "spawner403_2", 1, 1040, 530, "Under_403", HATE_RADIUS, 350);
spawner403_3 = randomStoppedGuardSpawner(myRooms.Under_403, "spawner403_3", 1, 430, 550, "Under_403", HATE_RADIUS, 260);
spawner403_4 = randomStoppedGuardSpawner(myRooms.Under_403, "spawner403_4", 1, 590, 270, "Under_403", HATE_RADIUS, 120);

//Roamers 403
spawner403_5 = randomStoppedSpawner(myRooms.Under_403, "spawner403_5", 1, 410, 630, "Under_403", HATE_RADIUS);
spawner403_5.addPatrolPointForChildren("Under_403", 520, 800, 0);
spawner403_5.addPatrolPointForChildren("Under_403", 940, 800, 0);
spawner403_5.addPatrolPointForChildren("Under_403", 1120, 690, 10);
spawner403_5.addPatrolPointForChildren("Under_403", 940, 800, 0);
spawner403_5.addPatrolPointForChildren("Under_403", 520, 800, 0);
spawner403_5.addPatrolPointForChildren("Under_403", 410, 630, 10);

//Guards 405
spawner405_1 = randomStoppedGuardSpawner(myRooms.Under_405, "spawner405_1", 1, 1350, 300, "Under_405", HATE_RADIUS, 120);
spawner405_2 = randomStoppedGuardSpawner(myRooms.Under_405, "spawner405_2", 1, 930, 250, "Under_405", HATE_RADIUS, 90);
spawner405_3 = randomStoppedGuardSpawner(myRooms.Under_405, "spawner405_3", 1, 180, 410, "Under_405", HATE_RADIUS, 90);
spawner405_4 = randomStoppedGuardSpawner(myRooms.Under_405, "spawner405_4", 1, 700, 590, "Under_405", HATE_RADIUS, 120);
spawner405_5 = randomStoppedGuardSpawner(myRooms.Under_405, "spawner405_5", 1, 1200, 760, "Under_405", HATE_RADIUS, 120);

spawner405_6 = randomStoppedSpawner(myRooms.Under_405, "spawner405_6", 1, 360, 520, "Under_405", HATE_RADIUS);
spawner405_6.addPatrolPointForChildren("Under_405", 570, 680, 0);
spawner405_6.addPatrolPointForChildren("Under_405", 890, 710, 10);
spawner405_6.addPatrolPointForChildren("Under_405", 570, 680, 0);
spawner405_6.addPatrolPointForChildren("Under_405", 360, 520, 10);

//Guards 407
spawner407_1 = randomStoppedGuardSpawner(myRooms.Under_407, "spawner407_1", 1, 240, 430, "Under_407", HATE_RADIUS, 90);
spawner407_2 = randomStoppedGuardSpawner(myRooms.Under_407, "spawner407_2", 1, 240, 710, "Under_407", HATE_RADIUS, 90);
spawner407_3 = randomStoppedGuardSpawner(myRooms.Under_407, "spawner407_3", 1, 790, 600, "Under_407", HATE_RADIUS, 120);
spawner407_4 = randomStoppedGuardSpawner(myRooms.Under_407, "spawner407_4", 1, 1360, 780, "Under_407", HATE_RADIUS, 90);
spawner407_5 = randomStoppedGuardSpawner(myRooms.Under_407, "spawner407_5", 1, 1080, 290, "Under_407", HATE_RADIUS, 90);
spawner407_6 = randomStoppedGuardSpawner(myRooms.Under_407, "spawner407_6", 1, 1440, 330, "Under_407", HATE_RADIUS, 120);

//Roamers 407
spawner407_7 = randomStoppedSpawner(myRooms.Under_407, "spawner407_7", 1, 450, 620, "Under_407", HATE_RADIUS);
spawner407_7.addPatrolPointForChildren("Under_407", 820, 730, 0);
spawner407_7.addPatrolPointForChildren("Under_407", 1020, 720, 10);
spawner407_7.addPatrolPointForChildren("Under_407", 820, 730, 0);
spawner407_7.addPatrolPointForChildren("Under_407", 450, 620, 10);

//Guards 601
spawner601_1 = randomStoppedGuardSpawner(myRooms.Under_601, "spawner601_1", 1, 930, 240, "Under_601", HATE_RADIUS, 90);
spawner601_2 = randomStoppedGuardSpawner(myRooms.Under_601, "spawner601_2", 1, 190, 430, "Under_601", HATE_RADIUS, 90);
spawner601_3 = randomStoppedGuardSpawner(myRooms.Under_601, "spawner601_3", 1, 460, 520, "Under_601", HATE_RADIUS, 120);
spawner601_4 = randomStoppedGuardSpawner(myRooms.Under_601, "spawner601_4", 1, 720, 720, "Under_601", HATE_RADIUS, 90);
spawner601_5 = randomStoppedGuardSpawner(myRooms.Under_601, "spawner601_5", 1, 1140, 820, "Under_601", HATE_RADIUS, 260);\

//Patrollers 601
spawner601_6 = randomStoppedSpawner(myRooms.Under_601, "spawner601_6", 1, 890, 350, "Under_601", HATE_RADIUS);
spawner601_6.addPatrolPointForChildren("Under_601", 1110, 380, 0);
spawner601_6.addPatrolPointForChildren("Under_601", 1480, 370, 10);
spawner601_6.addPatrolPointForChildren("Under_601", 1110, 380, 0);
spawner601_6.addPatrolPointForChildren("Under_601", 890, 350, 10);

spawner601_7 = randomStoppedSpawner(myRooms.Under_601, "spawner601_7", 1, 360, 620, "Under_601", HATE_RADIUS);
spawner601_7.addPatrolPointForChildren("Under_601", 460, 800, 0);
spawner601_7.addPatrolPointForChildren("Under_601", 790, 800, 0);
spawner601_7.addPatrolPointForChildren("Under_601", 940, 730, 10);
spawner601_7.addPatrolPointForChildren("Under_601", 790, 800, 0);
spawner601_7.addPatrolPointForChildren("Under_601", 460, 800, 0);
spawner601_7.addPatrolPointForChildren("Under_601", 360, 620, 10);

//Guards 603
spawner603_1 = randomStoppedGuardSpawner(myRooms.Under_603, "spawner603_1", 1, 500, 500, "Under_603", HATE_RADIUS, 90);
spawner603_2 = randomStoppedGuardSpawner(myRooms.Under_603, "spawner603_2", 1, 550, 730, "Under_603", HATE_RADIUS, 350);
spawner603_3 = randomStoppedGuardSpawner(myRooms.Under_603, "spawner603_3", 1, 980, 180, "Under_603", HATE_RADIUS, 90);
spawner603_4 = randomStoppedGuardSpawner(myRooms.Under_603, "spawner603_4", 1, 1110, 790, "Under_603", HATE_RADIUS, 260);

//Patrollers 603
spawner603_5 = randomStoppedSpawner(myRooms.Under_603, "spawner603_5", 1, 860, 330, "Under_603", HATE_RADIUS);
spawner603_5.addPatrolPointForChildren("Under_603", 1160, 380, 0);
spawner603_5.addPatrolPointForChildren("Under_603", 1490, 380, 10);
spawner603_5.addPatrolPointForChildren("Under_603", 1160, 380, 0);
spawner603_5.addPatrolPointForChildren("Under_603", 860, 330, 10);

spawner603_6 = randomStoppedSpawner(myRooms.Under_603, "spawner603_6", 1, 610, 570, "Under_603", HATE_RADIUS);
spawner603_6.addPatrolPointForChildren("Under_603", 750, 650, 0);
spawner603_6.addPatrolPointForChildren("Under_603", 920, 700, 10);
spawner603_6.addPatrolPointForChildren("Under_603", 750, 650, 0);
spawner603_6.addPatrolPointForChildren("Under_603", 610, 570, 10);

//Guards 605
spawner605_1 = randomStoppedGuardSpawner(myRooms.Under_605, "spawner605_1", 1, 1390, 380, "Under_605", HATE_RADIUS, 120);
spawner605_2 = randomStoppedGuardSpawner(myRooms.Under_605, "spawner605_2", 1, 990, 560, "Under_605", HATE_RADIUS, 90);
spawner605_3 = randomStoppedGuardSpawner(myRooms.Under_605, "spawner605_3", 1, 1060, 780, "Under_605", HATE_RADIUS, 350);
spawner605_4 = randomStoppedGuardSpawner(myRooms.Under_605, "spawner605_4", 1, 380, 800, "Under_605", HATE_RADIUS, 90);
spawner605_5 = randomStoppedGuardSpawner(myRooms.Under_605, "spawner605_5", 1, 570, 180, "Under_605", HATE_RADIUS, 120);
spawner605_6 = randomStoppedGuardSpawner(myRooms.Under_605, "spawner605_6", 1, 240, 410, "Under_605", HATE_RADIUS, 90);

//Guards 607
spawner607_1 = randomStoppedGuardSpawner(myRooms.Under_607, "spawner607_1", 1, 1320, 470, "Under_607", HATE_RADIUS, 120);
spawner607_2 = randomStoppedGuardSpawner(myRooms.Under_607, "spawner607_2", 1, 1380, 890, "Under_607", HATE_RADIUS, 350);
spawner607_3 = randomStoppedGuardSpawner(myRooms.Under_607, "spawner607_3", 1, 800, 600, "Under_607", HATE_RADIUS, 90);
spawner607_4 = randomStoppedGuardSpawner(myRooms.Under_607, "spawner607_4", 1, 410, 820, "Under_607", HATE_RADIUS, 350);
spawner607_5 = randomStoppedGuardSpawner(myRooms.Under_607, "spawner607_5", 1, 600, 180, "Under_607", HATE_RADIUS, 120);
spawner607_6 = randomStoppedGuardSpawner(myRooms.Under_607, "spawner607_6", 1, 250, 420, "Under_607", HATE_RADIUS, 120);

//Patrollers 607
spawner607_7 = randomStoppedSpawner(myRooms.Under_607, "spawner607_7", 1, 1240, 730, "Under_607", HATE_RADIUS);
spawner607_7.addPatrolPointForChildren("Under_607", 1050, 640, 0);
spawner607_7.addPatrolPointForChildren("Under_607", 590, 690, 10);
spawner607_7.addPatrolPointForChildren("Under_607", 1050, 640, 0);
spawner607_7.addPatrolPointForChildren("Under_607", 1240, 730, 10);

//Guards 801
spawner801_1 = randomStoppedGuardSpawner(myRooms.Under_801, "spawner801_1", 1, 400, 150, "Under_801", HATE_RADIUS, 90);
spawner801_2 = randomStoppedGuardSpawner(myRooms.Under_801, "spawner801_2", 1, 170, 310, "Under_801", HATE_RADIUS, 90);
spawner801_3 = randomStoppedGuardSpawner(myRooms.Under_801, "spawner801_3", 1, 400, 810, "Under_801", HATE_RADIUS, 350);
spawner801_4 = randomStoppedGuardSpawner(myRooms.Under_801, "spawner801_4", 1, 930, 770, "Under_801", HATE_RADIUS, 350);
spawner801_5 = randomStoppedGuardSpawner(myRooms.Under_801, "spawner801_5", 1, 1150, 490, "Under_801", HATE_RADIUS, 260);
spawner801_6 = randomStoppedGuardSpawner(myRooms.Under_801, "spawner801_6", 1, 1370, 740, "Under_801", HATE_RADIUS, 145);

//Patrollers 801
spawner801_7 = randomStoppedSpawner(myRooms.Under_801, "spawner801_7", 1,400, 330, "Under_801", HATE_RADIUS);
spawner801_7.addPatrolPointForChildren("Under_801", 590, 350, 0);
spawner801_7.addPatrolPointForChildren("Under_801", 780, 330, 10);
spawner801_7.addPatrolPointForChildren("Under_801", 590, 350, 0);
spawner801_7.addPatrolPointForChildren("Under_801", 400, 330, 10);

//Guards 803
spawner803_1 = randomStoppedGuardSpawner(myRooms.Under_803, "spawner803_1", 1, 1190, 240, "Under_803", HATE_RADIUS, 145);
spawner803_2 = randomStoppedGuardSpawner(myRooms.Under_803, "spawner803_2", 1, 350, 300, "Under_803", HATE_RADIUS, 90);
spawner803_3 = randomStoppedGuardSpawner(myRooms.Under_803, "spawner803_3", 1, 390, 810, "Under_803", HATE_RADIUS, 350);
spawner803_4 = randomStoppedGuardSpawner(myRooms.Under_803, "spawner803_4", 1, 150, 490, "Under_803", HATE_RADIUS, 90); //LT location
spawner803_5 = randomStoppedGuardSpawner(myRooms.Under_803, "spawner803_5", 1, 950, 810, "Under_803", HATE_RADIUS, 260);

//Patrols 803
spawner803_6 = randomStoppedSpawner(myRooms.Under_803, "spawner803_6", 1, 1210, 620, "Under_803", HATE_RADIUS);
spawner803_6.addPatrolPointForChildren("Under_803", 990, 650, 0);
spawner803_6.addPatrolPointForChildren("Under_803", 680, 810, 10);
spawner803_6.addPatrolPointForChildren("Under_803", 990, 650, 0);
spawner803_6.addPatrolPointForChildren("Under_803", 1210, 620, 10);

spawner803_7 = randomStoppedSpawner(myRooms.Under_803, "spawner803_7", 1, 590, 350, "Under_803", HATE_RADIUS);
spawner803_7.addPatrolPointForChildren("Under_803", 840, 460, 0);
spawner803_7.addPatrolPointForChildren("Under_803", 1080, 350, 10);
spawner803_7.addPatrolPointForChildren("Under_803", 840, 460, 0);
spawner803_7.addPatrolPointForChildren("Under_803", 590, 350, 10);

//Guards 805
spawner805_1 = randomStoppedGuardSpawner(myRooms.Under_805, "spawner805_1", 1, 1250, 580, "Under_805", HATE_RADIUS, 145);
spawner805_2 = randomStoppedGuardSpawner(myRooms.Under_805, "spawner805_2", 1, 1030, 830, "Under_805", HATE_RADIUS, 350);
spawner805_3 = randomStoppedGuardSpawner(myRooms.Under_805, "spawner805_3", 1, 1010, 300, "Under_805", HATE_RADIUS, 90);
spawner805_4 = randomStoppedGuardSpawner(myRooms.Under_805, "spawner805_4", 1, 600, 600, "Under_805", HATE_RADIUS, 145);
spawner805_5 = randomStoppedGuardSpawner(myRooms.Under_805, "spawner805_5", 1, 300, 300, "Under_805", HATE_RADIUS, 90);
spawner805_6 = randomStoppedGuardSpawner(myRooms.Under_805, "spawner805_6", 1, 140, 690, "Under_805", HATE_RADIUS, 350);

//Guards 807
spawner807_1 = randomStoppedGuardSpawner(myRooms.Under_807, "spawner807_1", 1, 1270, 200, "Under_807", HATE_RADIUS, 145);
spawner807_2 = randomStoppedGuardSpawner(myRooms.Under_807, "spawner807_2", 1, 920, 360, "Under_807", HATE_RADIUS, 90);
spawner807_3 = randomStoppedGuardSpawner(myRooms.Under_807, "spawner807_3", 1, 130, 630, "Under_807", HATE_RADIUS, 90); //LT location
spawner807_4 = randomStoppedGuardSpawner(myRooms.Under_807, "spawner807_4", 1, 460, 870, "Under_807", HATE_RADIUS, 350);
spawner807_5 = randomStoppedGuardSpawner(myRooms.Under_807, "spawner807_5", 1, 1070, 610, "Under_807", HATE_RADIUS, 145);
spawner807_6 = randomStoppedGuardSpawner(myRooms.Under_807, "spawner807_6", 1, 430, 470, "Under_807", HATE_RADIUS, 90);

spawner807_7 = randomStoppedSpawner(myRooms.Under_807, "spawner807_7", 1, 770, 790, "Under_807", HATE_RADIUS);
spawner807_7.addPatrolPointForChildren("Under_807", 720, 550, 0);
spawner807_7.addPatrolPointForChildren("Under_807", 620, 370, 10);
spawner807_7.addPatrolPointForChildren("Under_807", 720, 550, 0);
spawner807_7.addPatrolPointForChildren("Under_807", 770, 790, 10);

//Guards 1001
spawner1001_1 = randomStoppedGuardSpawner(myRooms.Under_1001, "spawner1001_1", 1, 340, 440, "Under_1001", HATE_RADIUS, 90);
spawner1001_2 = randomStoppedGuardSpawner(myRooms.Under_1001, "spawner1001_2", 1, 340, 710, "Under_1001", HATE_RADIUS, 350);
spawner1001_3 = randomStoppedGuardSpawner(myRooms.Under_1001, "spawner1001_3", 1, 750, 680, "Under_1001", HATE_RADIUS, 90);
spawner1001_4 = randomStoppedGuardSpawner(myRooms.Under_1001, "spawner1001_4", 1, 1220, 230, "Under_1001", HATE_RADIUS, 145); //LT location

//Patrollers 1001
spawner1001_5 = randomStoppedSpawner(myRooms.Under_1001, "spawner1001_5", 1, 750, 840, "Under_1001", HATE_RADIUS);
spawner1001_5.addPatrolPointForChildren("Under_1001", 1040, 790, 0);
spawner1001_5.addPatrolPointForChildren("Under_1001", 1310, 670, 10);
spawner1001_5.addPatrolPointForChildren("Under_1001", 1040, 790, 0);
spawner1001_5.addPatrolPointForChildren("Under_1001", 750, 840, 10);

spawner1001_6 = randomStoppedSpawner(myRooms.Under_1001, "spawner1001_6", 1, 1070, 370, "Under_1001", HATE_RADIUS);
spawner1001_6.addPatrolPointForChildren("Under_1001", 960, 500, 0);
spawner1001_6.addPatrolPointForChildren("Under_1001", 570, 510, 10);
spawner1001_6.addPatrolPointForChildren("Under_1001", 960, 500, 0);
spawner1001_6.addPatrolPointForChildren("Under_1001", 1070, 370, 10);

//Guards 1003
spawner1003_1 = randomStoppedGuardSpawner(myRooms.Under_1003, "spawner1003_1", 1, 400, 250, "Under_1003", HATE_RADIUS, 90);
spawner1003_2 = randomStoppedGuardSpawner(myRooms.Under_1003, "spawner1003_2", 1, 330, 600, "Under_1003", HATE_RADIUS, 90);
spawner1003_3 = randomStoppedGuardSpawner(myRooms.Under_1003, "spawner1003_3", 1, 850, 520, "Under_1003", HATE_RADIUS, 120);
spawner1003_4 = randomStoppedGuardSpawner(myRooms.Under_1003, "spawner1003_4", 1, 1320, 640, "Under_1003", HATE_RADIUS, 90);

//Patrols 1003
spawner1003_5 = randomStoppedSpawner(myRooms.Under_1003, "spawner1003_5", 1, 550, 410, "Under_1003", HATE_RADIUS);
spawner1003_5.addPatrolPointForChildren("Under_1003", 740, 460, 0);
spawner1003_5.addPatrolPointForChildren("Under_1003", 950, 310, 10);
spawner1003_5.addPatrolPointForChildren("Under_1003", 740, 460, 0);
spawner1003_5.addPatrolPointForChildren("Under_1003", 550, 410, 10);

spawner1003_6 = randomStoppedSpawner(myRooms.Under_1003, "spawner1003_6", 1, 1200, 780, "Under_1003", HATE_RADIUS);
spawner1003_6.addPatrolPointForChildren("Under_1003", 860, 700, 0);
spawner1003_6.addPatrolPointForChildren("Under_1003", 530, 640, 10);
spawner1003_6.addPatrolPointForChildren("Under_1003", 860, 700, 0);
spawner1003_6.addPatrolPointForChildren("Under_1003", 1200, 780, 10);

//Guards 1005
spawner1005_1 = randomStoppedGuardSpawner(myRooms.Under_1005, "spawner1005_1", 1, 390, 170, "Under_1005", HATE_RADIUS, 120);
spawner1005_2 = randomStoppedGuardSpawner(myRooms.Under_1005, "spawner1005_2", 1, 490, 720, "Under_1005", HATE_RADIUS, 120);
spawner1005_3 = randomStoppedGuardSpawner(myRooms.Under_1005, "spawner1005_3", 1, 790, 390, "Under_1005", HATE_RADIUS, 90);
spawner1005_4 = randomStoppedGuardSpawner(myRooms.Under_1005, "spawner1005_4", 1, 1140, 590, "Under_1005", HATE_RADIUS, 120);
spawner1005_5 = randomStoppedGuardSpawner(myRooms.Under_1005, "spawner1005_5", 1, 710, 850, "Under_1005", HATE_RADIUS, 350);
spawner1005_6 = randomStoppedGuardSpawner(myRooms.Under_1005, "spawner1005_6", 1, 1140, 860, "Under_1005", HATE_RADIUS, 260);

//Guards 1007
spawner1007_1 = randomStoppedGuardSpawner(myRooms.Under_1007, "spawner1007_1", 1, 250, 220, "Under_1007", HATE_RADIUS, 90);
spawner1007_2 = randomStoppedGuardSpawner(myRooms.Under_1007, "spawner1007_2", 1, 380, 620, "Under_1007", HATE_RADIUS, 120);
spawner1007_3 = randomStoppedGuardSpawner(myRooms.Under_1007, "spawner1007_3", 1, 340, 840, "Under_1007", HATE_RADIUS, 350);
spawner1007_4 = randomStoppedGuardSpawner(myRooms.Under_1007, "spawner1007_4", 1, 1130, 830, "Under_1007", HATE_RADIUS, 260);
spawner1007_5 = randomStoppedGuardSpawner(myRooms.Under_1007, "spawner1007_5", 1, 1100, 440, "Under_1007", HATE_RADIUS, 260);

//Roamers 1007
spawner1007_6 = randomStoppedSpawner(myRooms.Under_1007, "spawner1007_6", 1, 900, 370, "Under_1007", HATE_RADIUS);
spawner1007_6.addPatrolPointForChildren("Under_1007", 970, 580, 0);
spawner1007_6.addPatrolPointForChildren("Under_1007", 840, 810, 10);
spawner1007_6.addPatrolPointForChildren("Under_1007", 970, 580, 0);
spawner1007_6.addPatrolPointForChildren("Under_1007", 900, 370, 10);

//Spawn everything
underSpawnerList.each() {
	it.spawnAllNow();
	it.getChildren().each() { monster ->
		monster.setAwarenessArc(0);
		monster.getAI().overrideCanAttackForMiniEvent(true)
	}
}
underSpawnerListLT.each() {
	it.spawnAllNow();
	it.getChildren().each() { monster ->
		monster.setAwarenessArc(0);
		monster.getAI().overrideCanAttackForMiniEvent(Boolean.TRUE)
	}
}


// [rc] Set up all rooms as listeners for on enter to activate the room
myRooms.each { roomName, room ->
	if(roomName=="Under_1") return;

	myManager.onEnter(room) { event ->
		if( isPlayer( event.actor ) ) {
			def task = myManager.schedule(10) {
				activateRoom(room)
			}

			activationTasks[room] = task
			// [rc] It's possible that we overwrite an existing one if two onEnter events were queued up before we could stopListenForEnterExit.  And that's totally fine.  I'd rather the method be called twice than risk not calling stopListenForEnterExit by checking for the key in activationTasks
		}
	}
}




def randomStoppedGuardSpawner(room, name, maxSpawn, posX, posY, homeRoom, hateRadius, angle) {
	def randomMonster = ltMonsterRandomizer(room);
	def randomSpawner = creatorOfSpawners.createStoppedGuardSpawner(room, name, randomMonster.name, maxSpawn, posX, posY, homeRoom, randomMonster.level, hateRadius, angle);
	randomSpawner.setMaxNumberOfMonsters(randomMonster.quantity);
	randomSpawner.setMiniEventSpec(attackActivator)
	randomMonster.spawner_list << randomSpawner;
	return randomSpawner;
}

def randomStoppedSpawner(room, name, maxSpawn, posX, posY, homeRoom, hateRadius) {
	def randomMonster = ltMonsterRandomizer(room);
	def randomSpawner = creatorOfSpawners.createStoppedSpawner(room, name, randomMonster.name, maxSpawn, posX, posY, homeRoom, randomMonster.level, hateRadius);
	randomSpawner.setMaxNumberOfMonsters(randomMonster.quantity);
	randomSpawner.setMiniEventSpec(attackActivator)
	randomMonster.spawner_list << randomSpawner;
	return randomSpawner;
}

def ltMonsterRandomizer(room) {
	def randomMonster = null;
	if( ltCounter < LT_MAX && !ltSpawnerRoomList.contains(room) && !doNotSpawnLTsInRooms.contains(room) ) {
		ltRoll = random(1,10);
		if(ltRoll > 9) {
			ltCounter++;
			randomMonster = random(underMountainLTMonsters);
			ltSpawnerRoomList << room;
		} else {
			randomMonster = random(underMountainMonsters);
		}
	} else {
		randomMonster = random(underMountainMonsters);
	}
	return randomMonster;
}

