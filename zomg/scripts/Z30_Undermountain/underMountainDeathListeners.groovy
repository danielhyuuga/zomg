//Script created by gfern

/*import com.gaiaonline.mmo.battle.script.*;

vampireList = [] as Set
werewolfList = [] as Set

//-----------------------------------------------------------------------------------
//Death listener for Vampires                                                        
//-----------------------------------------------------------------------------------
def underMountainVampireDeathScheduler() {
	myManager.schedule(1) { underMountainVampireDeathScheduler() }
	myRooms.values().each() {
		//println "#### Eaching through bf1RoomList ####"
		it.getActorList().each() {
			if(isMonster(it) && (it.getMonsterType() == "vampire_male" || it.getMonsterType() == "vampire_female" || it.getMonsterType() == "vampire_batmonster" ) && !vampireList.contains(it)) {
				//println "#### Scheduling runOnDeath event for ${it} ####"
				vampireList << it
				runOnDeath(it) { event -> checkForVampireUpdate(event); vampireList.remove(event.actor) }
			}
		}
	}
}

def checkForVampireUpdate(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && !it.isDoneQuest(334) ) {
			it.addToPlayerVar("Halloween_Vampire_Counter", 1)
			//it.centerPrint("${it.getPlayerVar("Halloween_Vampire_Counter").intValue()} Vampires Defeated")
		}
		if(isPlayer(it) && it.getPlayerVar("Halloween_Vampire_Counter") >= 250 && !it.isDoneQuest(334)) {
			it.updateQuest(334, "Story-VQS")
		}
	}
}

underMountainVampireDeathScheduler()

//-----------------------------------------------------------------------------------
//Death listener for Werewolves                                                      
//-----------------------------------------------------------------------------------
def underMountainWerewolfDeathScheduler() {
	myManager.schedule(1) { underMountainWerewolfDeathScheduler() }
	myRooms.values().each() {
		//println "#### Eaching through bf1RoomList ####"
		it.getActorList().each() {
			if(isMonster(it) && ( it.getMonsterType() == "werewolf" || it.getMonsterType() == "werewolf_LT" ) && !werewolfList.contains(it)) {
				//println "#### Scheduling runOnDeath event for ${it} ####"
				werewolfList << it
				runOnDeath(it) { event -> checkForWerewolfUpdate(event); werewolfList.remove(event.actor) }
			}
		}
	}
}

def checkForWerewolfUpdate(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && !it.isDoneQuest(335) ) {
			it.addToPlayerVar("Halloween_Werewolf_Counter", 1)
			//it.centerPrint("${it.getPlayerVar("Halloween_Werewolf_Counter").intValue()} Werewolves Defeated")
		}
		if(isPlayer(it) && it.getPlayerVar("Halloween_Werewolf_Counter") >= 250 && !it.isDoneQuest(335)) {
			it.updateQuest(335, "Story-VQS")
		}
	}
}

underMountainVampireDeathScheduler()
underMountainWerewolfDeathScheduler()*/