import com.gaiaonline.mmo.battle.script.*
import com.gaiaonline.mmo.battle.*
import com.gaiaonline.mmo.battle.actor.*
import com.gaiaonline.mmo.battle.actor.ai.*
import com.gaiaonline.mmo.battle.Utils

/*
 	TODO:
 		- what has to be thread safe? e.g. the omg death counter.
 		- internal/external global suck. :-(
 */

gTrainingExitRoom = myRooms.VillageTraining_1
gTrainingEntranceRoom = myRooms.VillageTraining_2 // must match exit room script.
gRoomWidth = gTrainingExitRoom.getWidth()
gRoomHeight = gTrainingExitRoom.getHeight()
gLeonsDestinationRoomName = "VILLAGE_406"
gSmallOMGCount = 3
gSmallOMGsRadius = 125
long gRequiredRing = 17747 // must match entrance room script.

gKilledSmallOMGsFlagName = "Z31KilledSmallOMGs"

// note: apparently, overlapping trigger zones
// don't work so well, so Leon's exit area must
// not overlap that for the Player.
gPlayerExitLeft = 5
gPlayerExitTop = 0
gPlayerExitRight = 200

gAttackHintName = "Attack the Monsters"
gAttackHintText = "Double-click the monsters to attack."
gSecondsUntilAttackHint = 8

// --- set up Leon ---

gLeonsRadius = 50
gLeonExitLeft = 0
gLeonExitTop = (int)(gRoomHeight*(2/6))
gLeonExitRight = gPlayerExitLeft
gLeonExitBottom = (int)(gRoomHeight*(4/6))
gLeonHomeX = (int)(gRoomWidth*(1/4)) // try to NOT show the edge-enemy-hinting when the player is in the entrance room.
gLeonHomeY = (int)(gRoomHeight/2)
gLeon = spawnLeon( gTrainingExitRoom, gLeonHomeX, gLeonHomeY, gLeonsRadius )

gBasePhrasingPauseSec = 6
gPhrasingPauseBufferSec = 4

// --- kick off the dynamic scene ---

gExitArrow = makeSwitch( "TrainingArrow", gTrainingExitRoom, 100, (int)(gRoomHeight*(1/3)) )
gExitArrow.off()
gExitArrow.setUsable( false )

gAttackHint = null

myManager.onEnter( gTrainingExitRoom )
{
	event ->
	if( isPlayer( event.actor ) )
	{
		Player p = (Player)event.actor
		gAttackHint = createAttackHint( p, myManager, gAttackHintName, gAttackHintText, gSecondsUntilAttackHint, gRequiredRing )

		gLeon.choosePhrases( p, p.getRings().getSlotByID( gRequiredRing ) != null )
		leonSpeakingLoop( gLeon, gBasePhrasingPauseSec, gPhrasingPauseBufferSec )
		attackHintStartTiming( p, gAttackHint, gRequiredRing )
	}
}

// --- set up the first fight ---

gSmallDeathCount = 0
fightSmallOMGs(
		gSmallOMGCount,
		gLeon.getJavaLeon().getPoint().x,
		gLeon.getJavaLeon().getPoint().y,
		gSmallOMGsRadius,
		createIncrementSmallOMGDeathCountFn( gLeon, gLeonHomeX, gLeonHomeY, gLeonsRadius, gAttackHint )
	)

def createIncrementSmallOMGDeathCountFn( PhrasingLeon phrasingLeon, int lx, int ly, int radius, def attackHint ) 
{
	return {
		Player p ->
		
		gSmallDeathCount++
		
		int remaining = gSmallOMGCount - gSmallDeathCount
		if( remaining > 0 )
		{
			phrasingLeon.getJavaLeon().say( "Only " + remaining + " to go!" )
		}
		else if( remaining == 0 )
		{
			p.setGlobalFlag( gKilledSmallOMGsFlagName )
			attackHint?.dismiss()
			animateLeonAfterSmallOMGs( phrasingLeon, lx, ly, radius )
		}
	}
}

def animateLeonAfterSmallOMGs( PhrasingLeon phrasingLeon, int lx, int ly, int radius )
{
	phrasingLeon.getJavaLeon().stopPatrol()
	phrasingLeon.getJavaLeon().setRotation( 45 )
	phrasingLeon.getJavaLeon().startMoving( lx+radius*2, ly+radius*2 ) // force him to show his face; setRotation wasn't working for me.
	phrasingLeon.stopSpeaking()
	myManager.schedule( 3 )
	{
		phrasingLeon.getJavaLeon().say( "Many thanks, citizen." )
		myManager.schedule( 3 )
		{
			phrasingLeon.getJavaLeon().say( "I didn't have my Rings!" )
			myManager.schedule( 3 )
			{
				phrasingLeon.getJavaLeon().say( "Oh no, what's that?!" )
				fightOMGLT( phrasingLeon )
		    	phrasingLeon.getJavaLeon().startMoving( lx+radius*2+100, ly+radius*2+10 )
			}
		}
	}	
}

//--- avoid globals when not-too-ugly here below ---

//--- hints ---

def attackHintStartTiming( Player p, def hint, long requiredRing )
{
	if( ! p.hasGlobalFlag( gKilledSmallOMGsFlagName ) &&
		p.getRings().getSlotByID( requiredRing ) != null )
	{
		hint?.startTiming( p )
	}
}

def createAttackHint( Player player, ScriptManager manager, String name, String text, int delaySeconds, long requiredRing )
{
	def attackHint = null;

	if( ! player.hasGlobalFlag( gKilledSmallOMGsFlagName ) &&
		player.getRings().getSlotByID( requiredRing ) != null &&
		getZoneBroadcast( name ) == null )
	{
		attackHint = new DelayedPlayerHint( manager, delaySeconds,
		{ // how to show the hint.
			p -> zoneBroadcast( "Leon-VQS", name, text )
		},
		{ // how to remove the hint.
			p -> removeZoneBroadcast( name )
		} )
	}

	return attackHint
}

// --- Patrols ---

def generatePatrolCircleCW( def numberOfPoints, def centerX, def centerY, def raidus, def rStart )
{
	def points = generatePatrolPointsCW( numberOfPoints, centerX, centerY, raidus, rStart )
	return generatePatrol( points )
}

def generatePatrolCircleCounterCW( def numberOfPoints, def centerX, def centerY, def raidus, def rStart )
{
	def points = generatePatrolPointsCW( numberOfPoints, centerX, centerY, raidus, rStart )
	Collections.reverse( points )
	return generatePatrol( points )
}

def generatePatrolPointsCW( def numberOfPoints, def centerX, def centerY, def raidus, def rStart )
{
	def points = new ArrayList<java.awt.Point>()
	for( def r = rStart; r < rStart + 2*Math.PI; r += 2*Math.PI/numberOfPoints ) 
	{
		def x = (int)(centerX + Math.round( Math.cos( r ) * raidus ))
		def y = (int)(centerY + (int)Math.round( Math.sin( r ) * raidus ))
		points.add( new java.awt.Point( x, y ) )
	}
	return points
}

def generatePatrol( def points )
{
	def patrol = makeNewPatrol()
	for( p in points )
	{
		patrol.addPatrolPoint( gTrainingExitRoom, p, 0 )
	}
	return patrol
}

// --- Leon ---

def spawnLeon( def room, def lx, def ly, def radius )
{
	def leon = spawnNPC( "Leon-VQS", room, lx, ly )
	leon.setRotation( 45 )
	leon.setDisplayName( "Commander Leon" )
	
	def patrol = generatePatrolCircleCounterCW( 6, lx, ly, radius, 0 )
	leon.setPatrol( patrol )
	leon.startPatrol()
	
	return new PhrasingLeon( leon, myManager )
}

def leonSpeakingLoop( PhrasingLeon phrasingLeon, int basePauseSec, int pauseBufferSec )
{
	phrasingLeon.speakNext()
	if( phrasingLeon.isSpeaking() )
	{
		int pause = (int)(basePauseSec + (Math.random()*pauseBufferSec))
		myManager.schedule( pause )
		{
			leonSpeakingLoop( phrasingLeon, basePauseSec, pauseBufferSec )
		}
	}
}

// -- small omgs ---

def fightSmallOMGs( int monsterCount, def centerX, def centerY, def radius, Closure incrementOMGDeathCountFn )
{
	for( int r = 0; r < monsterCount; r++ )
	{
		double angle = r * (2*Math.PI/monsterCount)
		int x = (int)(centerX + Math.round( Math.cos( angle ) * radius ))
		int y = (int)(centerY + Math.round( Math.sin( angle ) * radius ))
		def omg = spawnOneOMG( "omg" + r, x, y, incrementOMGDeathCountFn )		
		omg.setPatrol( generatePatrolCircleCW( monsterCount * 3, centerX, centerY, radius, angle ) )
		omg.startPatrol()
	}
}

def spawnOneOMG( String name, def x, def y, Closure incrementOMGDeathCountFn )
{	
	def omgSpawner = gTrainingExitRoom.spawnStoppedSpawner( name, "omg_training", 1 )
	omgSpawner.setPos( x, y )
	def omg = omgSpawner.forceSpawnNow()
	omg.setDisplayName( "OMG" )
	runOnDeath( omg, {
		event ->
	    if( isPlayer(event.killer) )
	    {
	    	Player p = (Player)event.killer
	    	incrementOMGDeathCountFn.call( p )
	    }
	})
	return omg
}

// -- omg lt ---

def fightOMGLT( PhrasingLeon phrasingLeon )
{
	def ltSpawner = gTrainingExitRoom.spawnStoppedSpawner( "lt", "omg_training_lt", 1 )	
	ltSpawner.setPos( 100, (int)(gRoomHeight/2) )
	def lt = ltSpawner.forceSpawnNow()
	lt.setDisplayName( "Bigger OMG" )
	runOnDeath( lt, {
		event ->
	    if( isPlayer(event.killer) )
	    {
	    	Player p = (Player)event.killer
	    	phrasingLeon.getJavaLeon().say( "I hope that's over, now" )
	    	phrasingLeon.getJavaLeon().startMoving( gLeonExitLeft, (int)(gLeonExitTop+(gLeonExitBottom-gLeonExitTop)/2) ) 
	    	enableLeonExitZone( phrasingLeon )
	    }
		enableExit()
	})
}

// --- exit ---

def enableExit()
{
	enableExitZone()
	enableExitArrow()
}

def enableExitArrow()
{
	gExitArrow.on()
}

def enableExitZone()
{
	String exitTriggerName = "exitTrigger"
	gTrainingExitRoom.createTriggerZone( exitTriggerName, gPlayerExitLeft, gPlayerExitTop, gPlayerExitRight, gRoomHeight )
	myManager.onTriggerIn( gTrainingExitRoom, exitTriggerName )
	{
		event ->
	    if( isPlayer(event.actor) )
	    {
	    	Player p = (Player)event.actor
	    	setTrainingDoneFlags( p )
	        p.warp( gLeonsDestinationRoomName, 910, 220, PlatformRoom.TRANSITION_IRIS )
	    }
	}
}

def enableLeonExitZone( def phrasingLeon )
{
	NPC javaLeon = phrasingLeon.getJavaLeon()	
	String leonExitTriggerName = "leonExitTrigger"
	gTrainingExitRoom.createTriggerZone( leonExitTriggerName, gLeonExitLeft, gLeonExitTop, gLeonExitRight, gLeonExitBottom )
	myManager.onTriggerIn( gTrainingExitRoom, leonExitTriggerName )
	{
		event ->
	    if( event.actor == javaLeon )
	    {
	    	javaLeon.dispose()
	    }
	}
}

def setTrainingDoneFlags( Player p )
{
	/* i used a guest account with old training and saw this as
	 * the users state after they got through training to Leon.
	 * talking with Garrett, apparently we only care about n00b_state.
	+[0] => n00b_state|2|Player
    [1] => ghi|0.0|Player
    [2] => exhaustion|0|Player
    [3] => server|6|Player
    [4] => health|100|Player
    [5] => y|323.0|Player
    [6] => dazed|false|Player
    [7] => consumableWarning|kNotSeen|Player
    [8] => x|740.0|Player
    [9] => room|VILLAGE_405|Player
    [10] => ladderMinutes|1.5|Player
    [11] => Leon-VQS|1|MiniMap_Actors
    ?[12] => Z25ComingFromDani|on|questFlags
    [13] => ZXXOnHealthStarted|on|questFlags
    ?[14] => Z25ReceivedAttackRing|on|questFlags
    [15] => ZXXCrewUpCounterStarted|on|questFlags
    ?[16] => Z25ReceivedDefendRing|on|questFlags
    [17] => ZXXPowerupCounterStarted|on|questFlags
    [18] => ZXXOnDeathStarted|on|questFlags
    ?[19] => Z22AfterMovie|on|questFlags
    ?[20] => Z22HasLeftTrainStationOnceBefore|on|questFlags
    ?[21] => Z22MoviePlayingNow|on|questFlags
    [22] => SewersTraining|"1,101"|RestrictedStore
    ?[23] => hello|on|convDone
    ?[24] => finish|on|convDone
    ?[25] => map|on|convDone
    ?[26] => barryHello|on|convDone
    ?[27] => defend|on|convDone
    ?[28] => movementDialog|on|convDone
    ?[29] => afterMovie|on|convDone 
    */
	p.leftNoobZone()
	
	// fixing 2 bugs:
	// FS#37025: "Save Your Game" ui.
	// FS#37023: make Leon have something to say.
	p.setQuestFlag( GLOBAL, "Z25ComingFromDani" )
}

//
// --- supporting classes ---
//

class PhrasingLeon
{
	private final NPC javaLeon
	private final ScriptManager myManager
	
	private final PhraseCycle phrasesNoRing = new PhraseCycle( "Help!", "Go back and get the Ring!", "The Ring is in the chest!", "Then you can save me!" )
	private final PhraseCycle phrasesWithRing = new PhraseCycle( "Help!", "Get them off of me!", "Use your Ring!" )
	enum SayState { kSilent, kNoRing, kWithRing }
	private PhrasingLeon.SayState sayState

	public PhrasingLeon( NPC javaLeon, ScriptManager myManager )
	{
		this.javaLeon = javaLeon
		this.myManager = myManager
		this.sayState = PhrasingLeon.SayState.kNoRing
	}
	
	public NPC getJavaLeon()
	{
		return javaLeon
	}
	
	public void choosePhrases( Player p, boolean hasRing )
	{
		if( sayState != PhrasingLeon.SayState.kSilent )
		{
			sayState = hasRing ? PhrasingLeon.SayState.kWithRing : PhrasingLeon.SayState.kNoRing
		}
	}

	public void stopSpeaking()
	{
		sayState = PhrasingLeon.SayState.kSilent
	}
	
	public boolean isSpeaking()
	{
		return sayState != PhrasingLeon.SayState.kSilent
	}
	
	public void speakNext()
	{
		switch( sayState )
		{
			case PhrasingLeon.SayState.kNoRing:
				getJavaLeon().say( phrasesNoRing.getNextPhrase() )
				break
				
			case PhrasingLeon.SayState.kWithRing:
				getJavaLeon().say( phrasesWithRing.getNextPhrase() )
				break
		}
	}
}
