import com.gaiaonline.mmo.battle.script.*
import com.gaiaonline.mmo.battle.actor.*

gTrainingExitRoom = myRooms.VillageTraining_1

watching = false;
health_hits = 0;
monsters_killed = 0

monstersDeathWatch = [] as Set

onDeath = { 
	event ->
	Player player = (Player)event.actor
	player.trackClientStat("training_death")
}

onHealth = {
	event ->
	Player player = (Player)event.actor
	player.trackClientStat("training_health_${health_hits}")
	++health_hits
}

onMonsterDeath = {
	event ->
	monstersDeathWatch.remove( event.actor )
	
	getAreaTeam().getLeader().trackClientStat("training_killed_${monsters_killed}")
	++monsters_killed
}

trackTargetOMG = {
	event ->
	Player p = (Player)event.actor
	p.trackClientStat("target_omg")
}
trackTargetBiggerOMG = {
	event ->
	Player p = (Player)event.actor
	p.trackClientStat("target_bigger_omg")
}

myManager.onEnter( gTrainingExitRoom ) {
	e ->
	if( isPlayer( e.actor ) )
	{
		if( !watching )
		{
			watching = true;
			Player player = (Player)e.actor
			
			runOnDeath( player, onDeath ) 
			myManager.onHealth( player, onHealth )
			runOnUiEvent( player, "targetActor", "OMG",  trackTargetOMG )
			runOnUiEvent( player, "targetActor", "Bigger OMG",  trackTargetBiggerOMG )
		}
	}
	else if(isMonster(e.actor))
	{
		Actor actor = e.actor;
		if( !monstersDeathWatch.contains(actor) )
		{
			runOnDeath( actor, onMonsterDeath )
			monstersDeathWatch.add(actor)
		}
	}
}

