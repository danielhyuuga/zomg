import com.gaiaonline.mmo.battle.script.*
import com.gaiaonline.mmo.battle.*
import com.gaiaonline.mmo.battle.actor.*
import com.gaiaonline.mmo.battle.actor.ai.*
import com.gaiaonline.mmo.battle.Utils

/*
 	TODO:
 		- what has to be thread safe?
 		- internal/external global suck. :-(
*/

gTrainingEntranceRoom = myRooms.VillageTraining_2
gTrainingExitRoom = myRooms.VillageTraining_1 // must match exit room script.
gRoomWidth = gTrainingEntranceRoom.getWidth()
gRoomHeight = gTrainingEntranceRoom.getHeight()
long gRingToAward = 17747 // must match exit room script.

gChestFlagName = "Z31ChestOpened"
gSlideFlagName = "Z31DidSlide"

gChestHintName = "Open the Chest"
gChestHintText = "Click the chest to open it."
gSecondsUntilChestHint = 8
gSlideHintName = "Move to the Left."
gSlideHomeText = "Click on the left room edge to move."
gSecondsUntilSlideHint = 8

def gSlideHint = slideHintInit( myManager, gSlideHintName, gSlideHomeText, gSecondsUntilSlideHint )		
	
gChest = new TrainingChest( "chest", 5, 7, "Open me!", "What's inside?", "Dare you!", "Peekaboo!" )
gChest.setDisplayName( "Chest" )
addCritterToRoom( gChest, gTrainingEntranceRoom, 175, 445 )
gChest.setUsable()
gChest.close()
gChest.unlock()
gChest.setRange( 400 )
gChest.setMouseoverText( "Fabulous Treasure Chest" )
gChestHint = chestHintInit( myManager, gChestHintName, gChestHintText, gSecondsUntilChestHint, gRingToAward, createTestHasRingFn( gRingToAward ) )		
gChest.onUse( createTurnChestOneFn( gChest, gRingToAward, gChestHint, gSlideHint ) )

myManager.precurtainHook() {
	actor ->
	if( isPlayer(actor) )
	{
		Player p = (Player)actor
		
		if( p.hasGlobalFlag( gChestFlagName ) )
		{
			gChest.open()
			gChest.lock()
			gChest.setIsSpeaking( false )
		}
		else
		{
			gChest.close()
			gChest.unlock()
			gChest.setIsSpeaking( true )
		}
	}
}

myManager.onEnter( gTrainingEntranceRoom )
{
	enterEvent ->
	if( isPlayer( enterEvent.actor ) )
	{
		Player p = (Player)enterEvent.actor
		
		p.unRoot() // in case old training with Frank has frozen Player.
		
		gChestHint.startTiming( p )		
		chestOnEnter( p, gChest, gSlideHint )			
	}
}

myManager.onExit( gTrainingEntranceRoom )
{
	exitEvent ->
	if( isPlayer( exitEvent.actor ) )
	{
		if( exitEvent.destination == gTrainingExitRoom &&
			exitEvent.transitionType == PlatformRoom.TRANSITION_SLIDE_WEST )
		{
			Player p = (Player)exitEvent.actor
			if( ! p.hasGlobalFlag( gSlideFlagName ) )
			{
				p.setGlobalFlag( gSlideFlagName )
				gSlideHint.dismiss()
			}
		}
	}
}

//--- try to avoid globals here below ---

def chestHintInit( ScriptManager manager, String name, String text, int delaySeconds, long ringToAward, Closure ringCheckFn  )
{
	def chestHint = new DelayedPlayerHint( manager, delaySeconds,
		{ // how to show the hint.
			p ->
			if( ! p.hasGlobalFlag( gChestFlagName ) &&
				! ringCheckFn.call( p ) &&
				getZoneBroadcast( name ) == null )
			{
				zoneBroadcast( "Leon-VQS", name, text )
				p.trackClientStat("hint_chest")
			}
		},
		{ // how to remove the hint.
			p ->
			removeZoneBroadcast( name )
		} )
	return chestHint
}

def slideHintInit( ScriptManager manager, String name, String text, int delaySeconds )
{
	def slideHint = new DelayedPlayerHint( manager, delaySeconds,
		{ // how to show the hint.
			p ->
			if( ! p.hasGlobalFlag( gSlideFlagName ) &&
				getZoneBroadcast( name ) == null )
			{
				zoneBroadcast( "Leon-VQS", name, text )
				p.trackClientStat("hint_slide")
			}
		},
		{ // how to remove the hint.
			p ->
			removeZoneBroadcast( name )
		} )
	return slideHint
}

def chestOnEnter( Player p, def chest, def slideHint )
{
	if( p.hasGlobalFlag( gChestFlagName ) )
	{
		slideHint.startTiming( p )
	}
}

def createTestHasRingFn( long ringToAward )
{
	return {
		Player p ->
		def has = ( p.getRings().getSlotByID( ringToAward ) != null )
		return has
	}
}

def createTurnChestOneFn( def chest, long ringToAward, def chestHint, def slideHint )
{
	return {
		chestEvent ->
		if( isPlayer( chestEvent.player ) )
		{
			Player p = (Player)chestEvent.player

			if( ! p.hasGlobalFlag( gChestFlagName ) )
			{
				chest.open()
				chest.lock()
				chest.setIsSpeaking( false )
		        if( ! createTestHasRingFn(ringToAward).call(p) )
		        {
		        	p.grantRing( String.valueOf(ringToAward), true )
		        }
				p.setGlobalFlag( gChestFlagName )
				slideHint.startTiming( p )
			}
			chestHint.dismiss()
		}
	}
}
