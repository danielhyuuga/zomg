//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

buccExitRight = makeGeneric("PierRightExit", myRooms.BuccFull_706, 640, 630)
buccExitRight.setStateful(false)
buccExitRight.setUsable(true)
buccExitRight.setRange(200)
buccExitRight.setMouseoverText("Exit Buccaneer Boardwalk")

buccExitRight.onUse { event ->
	event.player.warp("Boardwalk_806", 690, 100, 7)
}

buccExitLeft = makeGeneric("PierLeftExit", myRooms.BuccFull_803, 690, 620)
buccExitLeft.setStateful(false)
buccExitLeft.setUsable(true)
buccExitLeft.setRange(200)
buccExitLeft.setMouseoverText("Exit Buccaneer Boardwalk")

buccExitLeft.onUse { event ->
	event.player.warp("Boardwalk_903", 700, 280, 7)
}

shipEntrance = makeGeneric("ShipEntrance", myRooms.BuccFull_206, 1000, 40)
shipEntrance.setStateful(false)
shipEntrance.setUsable(true)
shipEntrance.setRange(200)
shipEntrance.setMouseoverText("To Ship")

shipEntrance.onUse { event ->
	event.player.warp("BuccFull_7", 1130, 3250, 7)
}

shipExit = makeGeneric("ShipExit", myRooms.BuccFull_7, 1130, 3300)
shipExit.setStateful(false)
shipExit.setUsable(true)
shipExit.setRange(200)
shipExit.setMouseoverText("Exit Ship")

shipExit.onUse { event ->
	event.player.warp("BuccFull_206", 1000, 80, 7)
}

ferrisEntrance = makeGeneric("FerrisEntrance", myRooms.BuccFull_205, 550, 30)
ferrisEntrance.setStateful(false)
ferrisEntrance.setUsable(true)
ferrisEntrance.setRange(200)
ferrisEntrance.setMouseoverText("To Ferris Wheel")

ferrisEntrance.onUse { event ->
	event.player.warp("BuccFull_4", 4180, 3260, 7)
}

ferrisExit = makeGeneric("FerrisExit", myRooms.BuccFull_4, 4180, 3310)
ferrisExit.setStateful(false)
ferrisExit.setUsable(true)
ferrisExit.setRange(200)
ferrisExit.setMouseoverText("Exit Ferris Wheel")

ferrisExit.onUse { event ->
	event.player.warp("BuccFull_205", 550, 70, 7)
}

ferryEntrance = makeGeneric("FerryEntrance", myRooms.BuccFull_201, 570, 30)
ferryEntrance.setStateful(false)
ferryEntrance.setUsable(true)
ferryEntrance.setRange(200)
ferryEntrance.setMouseoverText("To Ferry")

ferryEntrance.onUse { event ->
	event.player.warp("BuccFull_1", 840, 1440, 7)
}

ferryExit = makeGeneric("FerryExit", myRooms.BuccFull_1, 840, 1480)
ferryExit.setStateful(false)
ferryExit.setUsable(true)
ferryExit.setRange(200)
ferryExit.setMouseoverText("Exit Ferry")

ferryExit.onUse { event ->
	event.player.warp("BuccFull_201", 570, 70, 7)
}