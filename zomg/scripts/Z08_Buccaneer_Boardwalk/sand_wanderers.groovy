import com.gaiaonline.mmo.battle.script.*;


//------------------------------------------
// Sand Golem Wandering Spawner             
//------------------------------------------

def anchor_902 = myRooms.Boardwalk_902.spawnSpawner( "anchor_902", "anchor_bug", 3 )
anchor_902.setPos( 420, 340 )
anchor_902.setWaitTime( 50, 70 )
anchor_902.setWanderBehaviorForChildren( 100, 200, 2, 5, 300)
anchor_902.setMonsterLevelForChildren( 5.6 )

def anchor_903 = myRooms.Boardwalk_903.spawnSpawner( "anchor_903", "anchor_bug", 4 )
anchor_903.setPos( 230, 490 )
anchor_903.setWaitTime( 50, 70 )
anchor_903.setWanderBehaviorForChildren( 100, 200, 2, 5, 300)
anchor_903.setMonsterLevelForChildren( 5.6 )

def anchor_904 = myRooms.Boardwalk_904.spawnSpawner( "anchor_904", "anchor_bug", 3 )
anchor_904.setPos( 460, 420 )
anchor_904.setWaitTime( 50, 70 )
anchor_904.setWanderBehaviorForChildren( 100, 200, 2, 5, 300)
anchor_904.setMonsterLevelForChildren( 5.6 )

//------------------------------------------
// Desert Island Chest Logic (Room 804)     
//------------------------------------------

bugIsland = myRooms.Boardwalk_804.spawnStoppedSpawner( "bugIsland", "anchor_bug", 5 )
bugIsland.setPos( 600, 170 )
bugIsland.setCFH( 400 )
bugIsland.setWanderBehaviorForChildren( 50, 125, 2, 5, 200)
bugIsland.setMonsterLevelForChildren( 5.3 )

//======Chest 804======
playerSet804 = []
grantList804 = []

myManager.onEnter( myRooms.Boardwalk_804 ) { event ->
	if(isPlayer(event.actor)) {
		playerSet804 << event.actor
	}
}

myManager.onExit( myRooms.Boardwalk_804 ) { event ->
	if(isPlayer(event.actor)) {
		playerSet804.remove( event.actor )
	}
}

islandChest = makeSwitch( "islandChest", myRooms.Boardwalk_804, 765, 205 )
islandChest.lock()
islandChest.off()
islandChest.setUsable( false )

playerSet804 = [] as Set
hateCollector = [] as Set
zone = 8 //Boardwalk

def openIslandChest() {
	//look at every player in the room and grant them loot after the chest opens
	myRooms.Boardwalk_804.getActorList().each { if( isPlayer( it ) ) { playerSet804 << it } }
	playerSet804.each{
		if( hateCollector.contains( it ) ) {
			chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe

			//scale the returned results down by relative con levels if the player is below the normal CL for the area
			if( it.getConLevel() > CLMap[zone] ) {
				lootMultiplier = 1.0
			} else {
				lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
			}

			//grant gold
			goldGrant = goldMap[zone] 
			goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
			it.grantCoins( ( goldGrant * lootMultiplier * chestType ).intValue() ) 

			//grant common items
			chance = (commonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				commonGrant = random( commonMap[zone] )
				it.grantItem( commonGrant )
			}

			//grant uncommon items
			chance = (uncommonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				uncommonGrant = random( uncommonMap[zone] )
				it.grantItem( uncommonGrant )
			}

			//grant recipes
			chance = (recipeChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				recipeGrant = random( recipeMap[zone] )
				it.grantItem( recipeGrant )
			}

			//grant orbs
			chance = (orbChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				orbGrant = orbMap[zone]
				orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType ).intValue()
				it.grantQuantityItem( 100257, orbGrant )
			}

			//grant rings
			chance = (ringChance * chestType).intValue()
			roll = random( 1, 100 )
			if( roll <= chance ) {
				ringGrant = random( ringMap[zone] )
				it.grantRing( ringGrant, true )
			}
		} else {
			if( it.getConLevel() > CLMap[zone]+1 ) {
				it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
			} else {
				it.centerPrint( "You did not help defeat the creatures protecting this container. No risk = no reward." )
			}
		}
	}
}

//Respawn Logic
def checkForContainerUnlock() {
	if( bugIsland.spawnsInUse() == 0 ) {
		myManager.schedule(3) {
			islandChest.on()
			openIslandChest()
			//respawn the camp after a suitable delay
			myManager.schedule( random( 300, 600) ) {
				//close and reset the chest
				islandChest.off()
				hateCollector.clear()
				playerSet804.clear()
				bug1 = bugIsland.forceSpawnNow()
				runOnDeath( bug1, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
				bug2 = bugIsland.forceSpawnNow()
				runOnDeath( bug2, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
				bug3 = bugIsland.forceSpawnNow()
				runOnDeath( bug3, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
				bug4 = bugIsland.forceSpawnNow()
				runOnDeath( bug4, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
				bug5 = bugIsland.forceSpawnNow()
				runOnDeath( bug5, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )

				//start watching to unlock the container again
				checkForContainerUnlock()
			}
		}
	} else {
		myManager.schedule(2) { checkForContainerUnlock() }
	}
}


// INITIAL LOGIC

bug1 = bugIsland.forceSpawnNow()
runOnDeath( bug1, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
bug2 = bugIsland.forceSpawnNow()
runOnDeath( bug2, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
bug3 = bugIsland.forceSpawnNow()
runOnDeath( bug3, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
bug4 = bugIsland.forceSpawnNow()
runOnDeath( bug4, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )
bug5 = bugIsland.forceSpawnNow()
runOnDeath( bug5, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector << it } } } )

checkForContainerUnlock()

//====================================================================================
//====================================================================================
commonChance = 20 
uncommonChance = 10 
recipeChance = 1
orbChance = 5
ringChance = 1 

CLMap = [ 2:1, 3:2, 4:2.5, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

goldMap = [ 2:10, 3:20, 4:30, 5:40, 6:50, 7:60, 8:70, 9:80, 10:90, 11:100, 14:10, 16:90, 18:80 ]

commonMap = [ 2:["100272", "100289", "100385", "100297", "100397"], 3:["100291", "100262", "100263", "100298"], 4:["100388", "100278", "100275", "100283"], 5:["100281", "100367", "100411", "100394", "100284", "100405", "100267"], 6:["100265", "100285", "100398", "100397", "100376", "100296"], 7:["100373", "100260"], 8:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 9:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 11:["100371", "100294", "100391", "100277", "100290", "100368", "100386", "100403"], 14:[0], 16:["100287", "100409"], 18:["100393", "100293", "100259", "100292"] ]

uncommonMap = [ 2:["100280", "100279", "100270", "100380", "100384"], 3:["100378", "100261", "100271"], 4:["100276", "100258", "100268"], 5:["100381", "100382", "100282", "100383"], 6:["100390", "100299", "100286", "100369", "100400", "100387"], 7:["100365", "100410"], 8:["100389", "100264", "100406", "100399", "100402"], 9:["100389", "100264", "100406", "100399", "100402"], 10:["100395", "100413", "100407", "100266"], 11:["100396", "100269", "100401", "100372", "100375", "100366", "100379", "100274"], 14:[0], 16:["100404", "100288"], 18:["100395", "100413", "100407", "100266"] ]

recipeMap = [ 2:["17766", "17764", "17772", "17758", "17756"], 3:["17861", "17857", "17755"], 4:["17848", "17849", "17852", "17851", "17850", "17753"], 5:["17833", "17831", "17754", "17836", "17835"], 6:["17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779"], 7:["17785", "17793", "17788", "17787"], 8:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 9:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 10:["17796", "17846", "17803", "17844"], 11:["17819", "17816", "17811", "17808", "17806", "17812", "17813", "17814", "17810", "17809", "17815"], 14:[0], 16:["17786", "17791", "17790", "17794"], 18:["17796", "17846", "17803", "17844"] ]

orbMap = [ 2:1, 3:2, 4:3, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

ringMap = [ 2:["17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 3:["17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 4:["17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 5:["17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 6:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 7:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 8:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 9:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 10:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 11:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 14:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 16:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 18:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"] ]

//====================================================================================
//====================================================================================

