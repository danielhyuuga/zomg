//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//-----------------------------------------------------------------------------------
//Variables, Maps, and Lists for Spawn Info                                          
//-----------------------------------------------------------------------------------
spawnAllowed = false
eventFailed = false

kikiSpawnLocation1 = [myRooms.Boardwalk_802, "Boardwalk_802", 120, 580]
kikiSpawnLocation2 = [myRooms.Boardwalk_804, "Boardwalk_804", 360, 320]
kikiSpawnLocation3 = [myRooms.Boardwalk_805, "Boardwalk_805", 310, 570]
kikiSpawnLocation4 = [myRooms.Boardwalk_904, "Boardwalk_904", 200, 270]
kikiSpawnLocation5 = [myRooms.Boardwalk_903, "Boardwalk_903", 800, 410]
kikiSpawnLocation6 = [myRooms.Boardwalk_902, "Boardwalk_902", 860, 510]

kikiRandomSayings = ["Somebody... help me!", "Meep... meep. I'm soooo afraid.", "Please, I just want to go home.", "Is anyone there?", "CoCo? Is that you?"]

kikiSpawnLocations = [kikiSpawnLocation1, kikiSpawnLocation2, kikiSpawnLocation3, kikiSpawnLocation4, kikiSpawnLocation5, kikiSpawnLocation6]
kikiMap = new HashMap()
carriedMap = new HashMap()
spawnLocationMap = new HashMap()
kikiTalkMap = new HashMap()
kidnapperKillerList = [] as Set
kikiSpawnedList = [] as Set
kikiRescuerList = [] as Set
boardwalkMonsterList = [] as Set
boardwalkOtherList = [] as Set
kikiTalkList = [] as Set

kikiCampsSpawned = 0
kidnappersKilled = 0
numberOfPlayers = 0

MAX_CAMPS_SPAWNED = 2
MIN_EVENT_DELAY = 3600
MAX_EVENT_DELAY = 7200

//-----------------------------------------------------------------------------------
//Spawners                                                                           
//-----------------------------------------------------------------------------------
//Make P3s invulnerable to players over CL 4.5
def kikiEventSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() < 6.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 6.0 or lower to rescue KiKi." ); }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

leaderSpawner = myRooms.Boardwalk_906.spawnStoppedSpawner("leaderSpawner", "sand_golem", 100)
leaderSpawner.setMiniEventSpec(kikiEventSpec)
leaderSpawner.setCFH(2000)
leaderSpawner.setPos(450, 500)
leaderSpawner.childrenWander(false)
leaderSpawner.setHomeTetherForChildren(1000)
leaderSpawner.setSpawnWhenPlayersAreInRoom(true)
leaderSpawner.setMonsterLevelForChildren(5.8)

henchmanSpawner = myRooms.Boardwalk_906.spawnStoppedSpawner("henchmanSpawner", "anchor_bug", 100)
henchmanSpawner.setMiniEventSpec(kikiEventSpec)
henchmanSpawner.setCFH(2000)
henchmanSpawner.setPos(450, 500)
henchmanSpawner.childrenWander(false)
henchmanSpawner.setHomeTetherForChildren(1000)
henchmanSpawner.setSpawnWhenPlayersAreInRoom(true)
henchmanSpawner.setMonsterLevelForChildren(5.8)

leaderSpawner.allyWithSpawner(henchmanSpawner)

//------------------------------------------------------------
// BUTTERFINGERS BART DIALOG                                  
//------------------------------------------------------------
Bart = spawnNPC("Butterfingers Bart-VQS", myRooms.Boardwalk_806, 770, 600)
Bart.setDisplayName( "Butterfingers Bart" )

def DefaultBart = Bart.createConversation("DefaultBart", true)

def Def1 = [id:1]
Def1.npctext = "Ahoy, matey! The name's Butterfingers Bart. Wot kin I do for ye?"
Def1.options = []
Def1.options << [text:"Are you a real pirate?", result: 2]
Def1.options << [text:"What's going on with the boardwalk?", result: 7]
Def1.options << [text:"Are you hiring crew now?", result: 17]
Def1.options << [text:"Nothing now. Thanks.", result: 22]
DefaultBart.addDialog( Def1, Bart )

def Def2 = [id:2]
Def2.npctext = "A real pirate, y'say? A REAL pirate?!? Har! Of course I am! Swashbuckler! Cuthroat! Privateer! A true Pirate of the Shallow Sea and proud of it!"
Def2.playertext = "Wow. So what are you doing standing in front of the boardwalk?"
Def2.result = 3
DefaultBart.addDialog( Def2, Bart )

def Def3 = [id:3]
Def3.npctext = "well...err...ummm...YAR! Thar's secret happenin's on the boardwalk and ye'll not be getting in to see them. YAR! That be the truth! As long as Bart stands here, ye'll never get by to see wot's inside."
Def3.playertext = "Do I *want* to see inside?"
Def3.result = 4
DefaultBart.addDialog( Def3, Bart )

def Def4 = [id:4]
Def4.npctext = "Of course y'do! But y'can't! Upon fear for your life, ye'll not advance e'en a step onto these planks. So says Bart!"
Def4.playertext = "Ummm...okay, Bart. I'll keep off the planks."
Def4.result = 5
DefaultBart.addDialog( Def4, Bart )

def Def5 = [id:5]
Def5.npctext = "Yar. That ye will. So says..."
Def5.playertext = "...Bart. Yeah, yeah. I get it. What else can you tell me?"
Def5.result = 6
DefaultBart.addDialog( Def5, Bart )

def Def6 = [id:6]
Def6.npctext = "Wot would y'know?"
Def6.options = []
Def6.options << [text:"Do you know where the Animated are coming from?", result: 24]
Def6.options << [text:"What's going on with the boardwalk?", result: 7]
Def6.options << [text:"Are you hiring crew now?", result: 17]
Def6.options << [text:"I think I'm good for now. 'Bye Bart.", result: 22]
DefaultBart.addDialog( Def6, Bart )

def Def7 = [id:7]
Def7.npctext = "Wouldn't y'like to know?"
Def7.playertext = "Well...yes. I would like to know."
Def7.result = 8
DefaultBart.addDialog( Def7, Bart )

def Def8 = [id:8]
Def8.npctext = "Well...the Cap'n, she's been readin' a lot and it's given her some mighty strange idears."
Def8.playertext = "What sort of 'idears'?"
Def8.result = 9
DefaultBart.addDialog( Def8, Bart )

def Def9 = [id:9]
Def9.npctext = "She's larned the idear of cleanin' our money so we can give it away easier."
Def9.playertext = "'Cleaning' money? Do you mean 'laundering'?"
Def9.result = 10
DefaultBart.addDialog( Def9, Bart )

def Def10 = [id:10]
Def10.npctext = "Yar! That's wot the Cap'n calls it too. We launder the booty by sinkin' it into a big park and openin' it t'all you landlubbers out there, makin' e'en more gold from ye when y'spend it foolishly here."
Def10.playertext = "Park? What sort of park?"
Def10.result = 11
DefaultBart.addDialog( Def10, Bart )

def Def11 = [id:11]
Def11.npctext = "A 'musement park."
Def11.playertext = "An amusement park? You mean with ferris wheels and boardwalk games? That sort of amusement park?"
Def11.result = 12
DefaultBart.addDialog( Def11, Bart )

def Def12 = [id:12]
Def12.npctext = "YAR! She's a beauty too. The swabbies we've got buildin' things are still workin', so the park is closed for now...but we'll open it soon enuff and then we'll start fleecin' the customers."
Def12.playertext = "Wait a minute. I know you pirates make a lot of cash, but how can you afford something this big? This seems stupendous."
Def12.result = 13
DefaultBart.addDialog( Def12, Bart )

def Def13 = [id:13]
Def13.npctext = "Well...that's whar some of us be disagreein' with th' Cap'n. Mind you...wot she says goes, but we don't much like the idea of being beholden to that thar Gambino fella."
Def13.playertext = "Gambino? As in the Gambinos of G-Corp?"
Def13.result = 14
DefaultBart.addDialog( Def13, Bart )

def Def14 = [id:14]
Def14.npctext = "Yar, that's the ones. The Cap'n has had...some dealin's...with G-Corp in the past. We're told that the Gambinos wonted to expand beyond the confines of their gamblin' island to set up shop here on the mainland too."
Def14.playertext = "So...G-Corp is backing your band of pirates to set up an amusement park? What kind of sense does that make?"
Def14.result = 15
DefaultBart.addDialog( Def14, Bart )

def Def15 = [id:15]
Def15.npctext = "Th' gold kind, ya lubber! We pirates git the money cleanin' place we wont, and the Gambinos get a share of the gold we garner in trade for helpin' us build th' place."
Def15.playertext = "That...makes sense, I suppose."
Def15.result = 16
DefaultBart.addDialog( Def15, Bart )

def Def16 = [id:16]
Def16.npctext = "Yar...that it do. What else do you need helpin' with?"
Def16.options = []
Def16.options << [text: "Do you know where the Animated are coming from?", result: 24]
Def16.options << [text: "Are you a real pirate?", result: 2]
Def16.options << [text: "Are you hiring crew now?", result: 17]
Def16.options << [text: "No thanks. I'm good for now.", result: 22]
DefaultBart.addDialog( Def16, Bart )

def Def17 = [id:17]
Def17.npctext = "So y'wont to be a pirate, do ye?"
Def17.options = []
Def17.options << [text: "Yes!!!", result: 18]
Def17.options << [text: "Ummm...I'm just checking for a friend", result: 19]
Def17.options << [text: "No. I'm just curious.", result: 20]
DefaultBart.addDialog( Def17, Bart )

def Def18 = [id:18]
Def18.npctext = "Enthusiastic little fish, ain't ya? Well...we're not hirin' right now. We've got a full boat. If ye'd like to be a carnie, we'll have work for ye when the buildin' is done, but nuthin' now."
Def18.playertext = "Well..it was worth asking about."
Def18.result = 21
DefaultBart.addDialog( Def18, Bart )

def Def19 = [id:19]
Def19.npctext = "For a friend, eh? I know how things are. Yer a cagey one, thar's no doubt about that. We pirates *like* cagey. But the boat's full right now. Check back in a few months...ye never know."
Def19.playertext = "Thanks, Bart. I will!"
Def19.result = 21
DefaultBart.addDialog( Def19, Bart )

def Def20 = [id:20]
Def20.npctext = "Hmmmm...well, curious is for cats, and I'm all out of milk, so ye'll have to find the answer to yer own question, lubber."
Def20.result = 21
DefaultBart.addDialog( Def20, Bart )

def Def21 = [id:21]
Def21.npctext = "Now...what else be ye needin' to know?"
Def21.options = []
Def21.options << [text: "Do you know where the Animated are coming from?", result: 24]
Def21.options << [text: "Are you a real pirate?", result: 2]
Def21.options << [text: "What's going on with the boardwalk?", result: 7]
Def21.options << [text: "I'm done for now, Bart. Thanks.", result: 22]
DefaultBart.addDialog( Def21, Bart )

def Def22 = [id:22]
Def22.npctext = "Well, until later then, if ye be luffin', then be laffin'. Thar ain't n'uther way."
Def22.playertext = "Ummm...yeah. You too?"
Def22.result = 23
DefaultBart.addDialog( Def22, Bart )

def Def23 = [id:23]
Def23.npctext = "YAR!"
Def23.result = DONE
DefaultBart.addDialog( Def23, Bart )

def Def24 = [id:24]
Def24.npctext = "Har... well, I gots nothin' substantial but to me mind thar was narry an Animated b'fore that big explosion. So says Bart."
Def24.options = []
Def24.options << [text:"What?! Explosion?! What explosion?", result: 25]
Def24.options << [text:"Oh, hang on. I think I remember reading something about that. Didn't it involve Gambino?", result: 26]
Def24.options << [text:"I knew it!! Logan wouldn't say it, but I knew Gambino was behind everything!", result: 27]
DefaultBart.addDialog( Def24, Bart )

def Def25 = [id:25]
Def25.npctext = "Ya mean ya ne'er heard tales o' the explosion? B'Gaia, have ye been livin' under a rock?"
Def25.playertext = "Bah! I guess I just don't read enough. So what about this explosion?"
Def25.result = 28
DefaultBart.addDialog( Def25, Bart )

def Def26 = [id:26]
Def26.npctext = "Yar, sounds like ye might know more 'n me... but I thinks me heard some rumors of such a thing."
Def26.playertext = "Hmm... you're not particularly helpful. Any idea where I can learn more about the explosion?"
Def26.result = 28
DefaultBart.addDialog( Def26, Bart )

def Def27 = [id:27]
Def27.npctext = "Hold a minute thar, yer jumpin' the gun a might. I didn't say nothin' about Gambino and don't ye be repeatin' that I did."
Def27.playertext = "I thought pirates were supposed to be fearless... but you sound like you're terrified of Gambino. What gives?"
Def27.result = 29
DefaultBart.addDialog( Def27, Bart )

def Def28 = [id:28]
Def28.npctext = "Well, perhaps ol' Bart be not the one to ask. The explosion took place just off the north o'here. Were it me, I'd be speakin' with folks on the beach. Maybe one of 'em was watchin' closely enough to shed some more light."
Def28.options = []
Def28.options << [text:"Interesting. I guess I'll head that way and see what I can find.", result: DONE]
Def28.options << [text:"Hmm... I'll keep that in mind. I wanted to ask you about something else, though.", result: 31]
DefaultBart.addDialog( Def28, Bart )

def Def29 = [id:29]
Def29.npctext = "Fearless and foolish be two diff'rent things and gettin' on the wrong side o' Gambino be borderin' on crazy. So says Bart. Besides, these days we pirates have extra incentive to stay on the friendly side o' Gambino."
Def29.options = []
Def29.options << [text:"Good point, Gambino seems like a mean dude. So if it's not Gambino, what is it? How can I learn more about the explosion?", result: 28]
Def29.options << [text:"Extra incentive? What do you mean by that?", result: 30]
DefaultBart.addDialog( Def29, Bart )

def Def30 = [id:30]
Def30.npctext = "Arr, the Cap'n'd slap me in irons and send me straight t' the bottom of the sea if she knew I'd say that much. N'more says Bart!"
Def30.options = []
Def30.options << [text:"Alright, but can you tell me anything else about the explosion?", result: 28]
Def30.options << [text:"Let me ask you about something else.", result: 31]
DefaultBart.addDialog( Def30, Bart )

def Def31 = [id:31]
Def31.npctext = "Wot kin I do for ye?"
Def31.options = []
Def31.options << [text:"Are you a real pirate?", result: 2]
Def31.options << [text:"What's going on with the boardwalk?", result: 7]
Def31.options << [text:"Are you hiring crew now?", result: 17]
Def31.options << [text:"Nothing now. Thanks.", result: 22]
DefaultBart.addDialog( Def31, Bart )

//-----------------------------------------------------------------------------------
//Trigger Zone and Logic                                                             
//-----------------------------------------------------------------------------------
kikiDropZone = "kikiDropZone"
myRooms.Boardwalk_806.createTriggerZone(kikiDropZone, 610, 505, 900, 600)

myManager.onTriggerIn(myRooms.Boardwalk_806, kikiDropZone) { event ->
	if(isPlayer(event.actor) && carriedMap.containsKey(event.actor)) {
		carriedMap.get(event.actor).unLatch()
		
		event.actor.centerPrint("You rescued KiKi!")
		getZoneCounter("KiKi Rescues")?.increment()
		Bart.say("Thanks for rescuing KiKi, ${event.actor}.")
		if(!kikiRescuerList.contains(event.actor)) { kikiRescuerList << event.actor; event.actor.setPlayerVar("Z08_KikiRescues", 0) }
		if(event.actor.getPlayerVar("Z08_KikiRescues") == null && event.actor.getPlayerVar("Z08_KikiRescuesTotal")) {
			event.actor.setPlayerVar("Z08_KikiRescues", 1)
			event.actor.setPlayerVar("Z08_KikiRescuesTotal", 1)
		} else {
			event.actor.addToPlayerVar("Z08_KikiRescues", 1)
			event.actor.addToPlayerVar("Z08_KikiRescuesTotal", 1)
		}
	}
}

//-----------------------------------------------------------------------------------
//Event logic                                                                        
//-----------------------------------------------------------------------------------
def startEvent() {
	println "#### Starting Kikidnapped ####"
	initializeEvent()

	//sound("kikiEventStart").toZone()
	myManager.schedule(2) {
		makeZoneCounter("Kidnappers Defeated", 0)
		makeZoneCounter("KiKi Rescues", 0).setGoal(20).onCompletion({ eventFailed = false; completeEvent() })
		zoneBroadcast("Butterfingers Bart-VQS", "KiKidnapped!", "The golems have kidnapped KiKi! We can't afford a reputation as a place where people, or cats, get kidnapped this close to our grand opening. Please find her!")
		myManager.schedule(30) { 
			println "#### Called spawnCheck() from startEvent() ####"
			spawnCheck()
			addMiniMapMarker("kikiSafeZone", "markerChickenWell", "Boardwalk_806", 770, 480, "Take KiKi Here")
			makeZoneTimer("KiKidnapped!", "0:25:0", "0:0:0").onCompletion({ eventFailed = true; completeEvent() }).start()
		}
	}
}
				/*myManager.schedule(1) {
					//println "#### kikiSpawnedList = ${kikiSpawnedList} ####"
					myRooms.values().each() {
						it.getActorList().each() {
							if(isMonster(it)) { boardwalkMonsterList << it }
						}
					}
					//println "#### boardwalkMonsterList = ${boardwalkMonsterList} ###"
					kikiSpawnedList.clone().each() {
						//println "#### Disposing ${it} ####"
						if(kikiTalkList.contains(it)) {
							kikiTalkList.remove(it)
						}
						if(isMonster(it)) {
							it.instantPercentDamage(100)
						} else {
							if(it?.isLatched()) { 
								it.unLatch()
							} else {
								it.dispose()
							}
						}
					}
					kikiSpawnedList.remove(it)
					removeMiniMapMarker("kikiSafeZone")
					removeZoneTimer("KiKidnapped!")
					removeZoneCounter("Kidnappers Defeated")
					removeZoneCounter("KiKi Rescues")
					removeZoneBroadcast("KiKidnapped!")
					myManager.schedule(15) { removeZoneBroadcast("Failure!"); scheduleNextEvent() }
				}
			
		}
	}
}*/

def initializeEvent() {
	spawnAllowed = true
	kikiCampsSpawned = 0
	kidnappersKilled = 0
	numberOfPlayers = 0
	
	kikiSpawnLocations = [kikiSpawnLocation1, kikiSpawnLocation2, kikiSpawnLocation3, kikiSpawnLocation4, kikiSpawnLocation5, kikiSpawnLocation6]
	kikiMap = new HashMap()
	carriedMap = new HashMap()
	spawnLocationMap = new HashMap()
	kidnapperKillerList = [] as Set
	kikiSpawnedList = [] as Set
	kikiRescuerList = [] as Set
	boardwalkMonsterList = [] as Set
	boardwalkOtherList = [] as Set
	kikiTalkList = [] as Set 
}

def completeEvent() {
	myManager.schedule(1) {
		spawnAllowed = false
		removeMiniMapMarker("kikiSafeZone")
		removeZoneTimer("KiKidnapped!")
		removeZoneCounter("Kidnappers Defeated")
		removeZoneCounter("KiKi Rescues")
		removeZoneBroadcast("KiKidnapped!")
		kikiSpawnedList.clone().each() {
			//println "#### Disposing ${it} ####"
			if(kikiTalkList.contains(it)) {
				kikiTalkList.remove(it)
			}
			if(isMonster(it)) {
				it.instantPercentDamage(100)
			} else {
				if(it?.isLatched()) { 
					it.unLatch()
				} else {
					it.dispose()
				}
			}
			kikiSpawnedList.remove(it)
		}

		if(eventFailed == false && kidnapperKillerList.size() > 0) {
			zoneBroadcast("Butterfingers Bart-VQS", "Safe Again", "Oh, thank you! You dealt with those kidnappers nicely and helped keep our grand opening on schedule!")
			println "#*#*#* BEGIN KIKIDNAPPED PRINTOUT *#*#*#"
			println "#*#*#* kidnapperKillerList = ${kidnapperKillerList} *#*#*#"
			kidnapperKillerList.clone().each() {
				if( it != null && isOnline(it) && !isInOtherLayer(it)) {
					numberOfPlayers++
					kidnapperssKilled = kidnappersKilled + it.getPlayerVar("Z08_KidnappersKilled").intValue()
					println "#*#*#* PlayerID = ${it} *#*#*#"
					println "#*#*#* Kidnappers killed this event = ${it.getPlayerVar("Z08_KidnappersKilled")} *#*#*#"
					println "#*#*#* Kidnappers killed all time = ${it.getPlayerVar("Z08_KidnappersKilledTotal")} *#*#*#"
					if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) > 0 && it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) < 10) { 
						tinyReward = random(3, 4)
						goldReward = random(12, 15)

						it.centerPrint("Event completed successfully. All Participants receive a reward bonus!")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a tiny reward.")
						it.centerPrint("You receive ${goldReward} gold and ${tinyReward} orbs.")
						it.grantQuantityItem(100257, tinyReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 99) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) >= 10 && it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) < 35) {
						moderateReward = random(6, 7)
						goldReward = random(17, 25)
					
						it.centerPrint("Event completed successfully. All Participants receive a reward bonus!")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a moderate reward.")
						it.centerPrint("You receive ${goldReward} gold and ${moderateReward} orbs.")
						it.grantQuantityItem(100257, moderateReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 80) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) >= 35 && it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) < 60) {
						largeReward = random(10, 12)
						goldReward = random(30, 40)

						it.centerPrint("Event completed successfully. All Participants receive a reward bonus!")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a large reward.")
						it.centerPrint("You receive ${goldReward} gold and ${largeReward} orbs.")
						it.grantQuantityItem(100257, largeReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 65) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) >= 60) {
						massiveReward = random(15, 17)
						goldReward = random(61, 75)

						it.centerPrint("Event completed successfully. All Participants receive a reward bonus!")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a massive reward.")
						it.centerPrint("You receive ${goldReward} gold and ${massiveReward} orbs.")
						it.grantQuantityItem(100257, massiveReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 0) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					}
					/*if(it.getPlayerVar("Z08_KidnappersKilledTotal") >= 100 && !it.isDoneQuest(322)) {
						it.centerPrint("You have been awarded the Kidnapper Killer badge.")
						it.updateQuest(322, "Butterfingers Bart-VQS")
					}
					if(it.getPlayerVar("Z08_KidnappersKilledTotal") >= 1000 && !it.isDoneQuest(323)) {
						it.centerPrint("You have been awarded the  badge.")
						it.updateQuest(323, "Butterfingers Bart-VQS")
					}*/
				}
			}
			if(numberOfPlayers > 0) { println "*#*#*# Average = ${kidnappersKilled / numberOfPlayers} *#*#*#" }
			println "*#*#*# END KIKIDNAPPED PRINTOUT *#*#*#"
		}
		
		if(eventFailed == true && kidnapperKillerList.size() > 0) {
			zoneBroadcast("Butterfingers Bart-VQS", "Failure!", "This is horrible! The kidnappers got away. Now we'll have to delay the opening of Buccaneer Boardwalk until this blows over.")
			println "#*#*#* BEGIN KIKIDNAPPED PRINTOUT *#*#*#"
			println "#*#*#* kidnapperKillerList = ${kidnapperKillerList} *#*#*#"
			kidnapperKillerList.clone().each() {
				if( it != null && isOnline(it) && !isInOtherLayer(it)) {
					numberOfPlayers++
					kidnapperssKilled = kidnappersKilled + it.getPlayerVar("Z08_KidnappersKilled").intValue()
					println "#*#*#* PlayerID = ${it} *#*#*#"
					println "#*#*#* Kidnappers killed this event = ${it.getPlayerVar("Z08_KidnappersKilled")} *#*#*#"
					println "#*#*#* Kidnappers killed all time = ${it.getPlayerVar("Z08_KidnappersKilledTotal")} *#*#*#"
					if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) > 0 && it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) < 10) { 
						tinyReward = random(1, 2)
						goldReward = random(7, 11)

						it.centerPrint("Event completed unsuccessfully. Participants do not receive a reward bonus.")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a tiny reward.")
						it.centerPrint("You receive ${goldReward} gold and ${tinyReward} orbs.")
						it.grantQuantityItem(100257, tinyReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 99) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) >= 10 && it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) < 35) {
						moderateReward = random(4, 5)
						goldReward = random(10, 16)
					
						it.centerPrint("Event completed unsuccessfully. Participants do not receive a reward bonus.")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a moderate reward.")
						it.centerPrint("You receive ${goldReward} gold and ${moderateReward} orbs.")
						it.grantQuantityItem(100257, moderateReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 80) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) >= 35 && it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) < 60) {
						largeReward = random(8, 9)
						goldReward = random(20, 29)

						it.centerPrint("Event completed unsuccessfully. Participants do not receive a reward bonus.")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a large reward.")
						it.centerPrint("You receive ${goldReward} gold and ${largeReward} orbs.")
						it.grantQuantityItem(100257, largeReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 65) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z08_KidnappersKilled") + (it.getPlayerVar("Z08_KikiRescues") * 5) >= 60) {
						massiveReward = random(13, 14)
						goldReward = random(50, 60)

						it.centerPrint("Event completed unsuccessfully. Participants do not receive a reward bonus.")
						it.centerPrint("You killed ${it.getPlayerVar("Z08_KidnappersKilled").intValue()} kidnapper(s), rescued KiKi ${it.getPlayerVar("Z08_KikiRescues").intValue()} time(s), and earned a massive reward.")
						it.centerPrint("You receive ${goldReward} gold and ${massiveReward} orbs.")
						it.grantQuantityItem(100257, massiveReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 0) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					}
					/*if(it.getPlayerVar("Z08_KidnappersKilledTotal") >= 100 && !it.isDoneQuest(322)) {
						it.centerPrint("You have been awarded the Kidnapper Killer badge.")
						it.updateQuest(322, "Butterfingers Bart-VQS")
					}
					if(it.getPlayerVar("Z08_KidnappersKilledTotal") >= 1000 && !it.isDoneQuest(323)) {
						it.centerPrint("You have been awarded the  badge.")
						it.updateQuest(323, "Butterfingers Bart-VQS")
					}*/
				}
			}
			if(numberOfPlayers > 0) { println "*#*#*# Average = ${kidnappersKilled / numberOfPlayers} *#*#*#" }
			println "*#*#*# END KIKIDNAPPED PRINTOUT *#*#*#"
		}
		myManager.schedule(15) { if(eventFailed == false) { removeZoneBroadcast("Safe Again") }; if(eventFailed == true) { removeZoneBroadcast("Failure!") }; scheduleNextEvent() }
	}
}

def synchronized spawnCheck() {
	if(spawnAllowed == true) { //If spawn is allowed, and there are Gruckens to spawn, spawn one
		//println "#### kikiCampsSpawned = ${kikiCampsSpawned} ####"
		if(kikiCampsSpawned < MAX_CAMPS_SPAWNED && kikiSpawnLocations.size() > 4) {
			//println "#### Called spawnRandom() ####"
			spawnRandom()
			myManager.schedule(random(20, 40)) { spawnCheck() } //If no Grucken spawns are available, schedule a check again in 30 seconds
		}
	} 
}
 
def spawnRandom() {
	if(kikiSpawnLocations.size < 5) { return }
	kikiCampsSpawned++
	//println "#### spawnLocations = ${kikiSpawnLocations} ####"
	kikiSpawnLocation = random(kikiSpawnLocations)
	kikiSpawnLocations.remove(kikiSpawnLocation)
	
	kiki = null
	kidnapper1 = null
	kidnapper2 = null
	kidnapper3 = null
	kidnapper4 = null
	
	//println "#### spawnLocation = ${spawnLocation} ####"
	
	//Grab Room and X, Y from map
	spawnRoom = kikiSpawnLocation?.get(0)
	spawnX = kikiSpawnLocation?.get(2)
	spawnY = kikiSpawnLocation?.get(3)
	
	kiki = makeCritter("kiki", spawnRoom, spawnX, spawnY)
	kiki.pause()
	kikiWarpRoom = kikiSpawnLocation?.get(1)
	kiki.warp(kikiWarpRoom.toString(), spawnX, spawnY, 6)
	kiki.setDisplayName("KiKi")
	
	//println "#### Spawned ${kiki} ####"
	
	kikiSpawnedList << kiki
	spawnLocationMap.put(kiki, kikiSpawnLocation)
	
	kiki.onUse { event ->
		if(whoIsLatchedTo(event.player).size() > 0) {
			whoIsLatchedTo(event.player).each {
				it.unLatch()
			}
		}
		if(event.player.getConLevel() < 6.1) {
			if(!event.critter.isLatched() && !carriedMap.containsKey(event.player)) {
				//println "#### ${event.player} picked up ${event.critter} ####"
				event.critter.latchOnTo(event.player)
				kikiTalkList.remove(event.critter)
				//myManager.schedule(random(10, 20)) { spawnCheck() }
				carriedMap.put(event.player, event.critter)
				event.player.centerPrint("Quickly, take KiKi to safety before more kidnappers arrive!")
				//println "#### Adding ${spawnLocationMap?.get(event.critter)} ####"
				kikiSpawnLocations << spawnLocationMap?.get(event.critter)
				spawnLocationMap.remove(event.critter)
		
				myManager.schedule(120) {
					if(event.critter.isLatched()) {
						event.critter.unLatch()
						//event.critter.dispose()
						//carriedMap.remove(event.player)
					}
				}
			}
		} else { event.player.centerPrint("You must be level 6.0 or lower to rescue KiKi.") }
		
		event.critter.onDrop { dropEvent ->
			if(dropEvent.reason == "dazed" || dropEvent.reason == "zoned" || dropEvent.reason == "script") {
				//println "#### Rand onDrop event for ${dropEvent.dropper} ####"
				//println "#### Called spawnCheck() from onDrop event for ${dropEvent.actor} in 20, 40 seconds ####"
				kikiSpawnedList.remove(dropEvent.actor)
				carriedMap.remove(dropEvent.dropper)
				//println "#### removed ${dropEvent.dropper} from carriedMap. carriedMap is now ${carriedMap} ####"
				if(kikiCampsSpawned > 0) { kikiCampsSpawned-- }
				dropEvent.actor.startWander(true)
				dropEvent.actor?.dispose()
				myManager.schedule(random(20, 40)) { spawnCheck() }
			}
		}
	}
	
	spawnRoom = kikiSpawnLocation?.get(1)
	spawnX = kikiSpawnLocation?.get(2)
	spawnY = kikiSpawnLocation?.get(3) - 75
	
	leaderSpawner.warp(spawnRoom.toString(), spawnX, spawnY) //move the spawner into position
	leaderSpawner.setHomeForChildren(spawnRoom.toString(), spawnX, spawnY)
	leaderSpawner.setGuardPostForChildren(spawnRoom.toString(), spawnX, spawnY, 90)
	
	kidnapper1 = leaderSpawner.forceSpawnNow()
	kikiSpawnedList << kidnapper1
	
	//println "#### Creating runOnDeath for ${kidnapper1} ####"
	runOnDeath(kidnapper1) { event ->
		//println "#### ${event.actor} just died ####"
		allDead = true
		getZoneCounter("Kidnappers Defeated")?.increment()
		kikiSpawnedList.remove(event.actor)
		kikiMap.get(event.actor.toString()).each() {
			if(isMonster(it) && !it.isDead()) { allDead = false }
		}
		event.actor.getHated().each() {
			if(it.getConLevel() < 6.1) {
				if(!kidnapperKillerList.contains(it)) { kidnapperKillerList << it; it.setPlayerVar("Z08_KidnappersKilled", 0) }
				if(it.getPlayerVar("Z08_KidnappersKilled") == null && it.getPlayerVar("Z08_KidnappersKilledTotal")) {
					it.setPlayerVar("Z08_KidnappersKilled", 1)
					it.setPlayerVar("Z08_KidnappersKilledTotal", 1)
				} else {
					it.addToPlayerVar("Z08_KidnappersKilled", 1)
					it.addToPlayerVar("Z08_KidnappersKilledTotal", 1)
				}
			}
		}
		if(allDead == true) {
			kikiMap.get(event.actor.toString()).get(0).setUsable(true)
			kikiTalkList << kikiMap.get(event.actor.toString()).get(0)
			kikiTalkMap.put(kikiMap.get(event.actor.toString()).get(0), 0)
		}
		//println "#### kikiMap = ${kikiMap} ####"
	}
	
	spawnX = kikiSpawnLocation?.get(2)
	spawnY = kikiSpawnLocation?.get(3) + 75
	
	henchmanSpawner.warp(spawnRoom.toString(), spawnX, spawnY)
	henchmanSpawner.setHomeForChildren(spawnRoom.toString(), spawnX, spawnY)
	henchmanSpawner.setGuardPostForChildren(spawnRoom.toString(), spawnX, spawnY, 135)
	
	kidnapper2 = henchmanSpawner.forceSpawnNow()
	kikiSpawnedList << kidnapper2
	
	//println "#### Creating runOnDeath for ${kidnapper2} ####"
	runOnDeath(kidnapper2) { event ->
		//println "#### ${event.actor} just died ####"
		allDead = true
		getZoneCounter("Kidnappers Defeated")?.increment()
		kikiSpawnedList.remove(event.actor)
		kikiMap.get(event.actor.toString()).each() {
			if(isMonster(it) && !it.isDead()) { allDead = false }
		}
		event.actor.getHated().each() {
			if(it.getConLevel() < 6.1) {
				if(!kidnapperKillerList.contains(it)) { kidnapperKillerList << it; it.setPlayerVar("Z08_KidnappersKilled", 0) }
				if(it.getPlayerVar("Z08_KidnappersKilled") == null && it.getPlayerVar("Z08_KidnappersKilledTotal")) {
					it.setPlayerVar("Z08_KidnappersKilled", 1)
					it.setPlayerVar("Z08_KidnappersKilledTotal", 1)
				} else {
					it.addToPlayerVar("Z08_KidnappersKilled", 1)
					it.addToPlayerVar("Z08_KidnappersKilledTotal", 1)
				}
			}
		}
		if(allDead == true) {
			kikiMap.get(event.actor.toString()).get(0).setUsable(true)
			kikiTalkList << kikiMap.get(event.actor.toString()).get(0)
			kikiTalkMap.put(kikiMap.get(event.actor.toString()).get(0), 0)
		}
		//println "#### kikiMap = ${kikiMap} ####"
	}
	
	spawnX = kikiSpawnLocation?.get(2) - 75
	spawnY = kikiSpawnLocation?.get(3)
	
	henchmanSpawner.warp(spawnRoom.toString(), spawnX, spawnY)
	henchmanSpawner.setHomeForChildren(spawnRoom.toString(), spawnX, spawnY)
	henchmanSpawner.setGuardPostForChildren(spawnRoom.toString(), spawnX, spawnY, 135)
	
	kidnapper3 = henchmanSpawner.forceSpawnNow()
	kikiSpawnedList << kidnapper3
	
	//println "#### Creating runOnDeath for ${kidnapper3} ####"
	runOnDeath(kidnapper3) { event ->
		//println "#### ${event.actor} just died ####"
		allDead = true
		getZoneCounter("Kidnappers Defeated")?.increment()
		kikiSpawnedList.remove(event.actor)
		kikiMap.get(event.actor.toString()).each() {
			if(isMonster(it) && !it.isDead()) { allDead = false }
		}
		event.actor.getHated().each() {
			if(it.getConLevel() < 6.1) {
				if(!kidnapperKillerList.contains(it)) { kidnapperKillerList << it; it.setPlayerVar("Z08_KidnappersKilled", 0) }
				if(it.getPlayerVar("Z08_KidnappersKilled") == null && it.getPlayerVar("Z08_KidnappersKilledTotal")) {
					it.setPlayerVar("Z08_KidnappersKilled", 1)
					it.setPlayerVar("Z08_KidnappersKilledTotal", 1)
				} else {
					it.addToPlayerVar("Z08_KidnappersKilled", 1)
					it.addToPlayerVar("Z08_KidnappersKilledTotal", 1)
				}
			}
		}
		if(allDead == true) {
			kikiMap.get(event.actor.toString()).get(0).setUsable(true)
			kikiTalkList << kikiMap.get(event.actor.toString()).get(0)
			kikiTalkMap.put(kikiMap.get(event.actor.toString()).get(0), 0)
		}
		//println "#### kikiMap = ${kikiMap} ####"
	}
	
	spawnX = kikiSpawnLocation?.get(2) + 75
	spawnY = kikiSpawnLocation?.get(3)
	
	henchmanSpawner.warp(spawnRoom.toString(), spawnX, spawnY)
	henchmanSpawner.setHomeForChildren(spawnRoom.toString(), spawnX, spawnY)
	henchmanSpawner.setGuardPostForChildren(spawnRoom.toString(), spawnX, spawnY, 90)
	
	kidnapper4 = henchmanSpawner.forceSpawnNow()
	kikiSpawnedList << kidnapper4
	
	//println "#### Creating runOnDeath for ${kidnapper4} ####"
	runOnDeath(kidnapper4) { event ->
		//println "#### ${event.actor} just died ####"
		allDead = true
		getZoneCounter("Kidnappers Defeated")?.increment()
		kikiSpawnedList.remove(event.actor)
		kikiMap.get(event.actor.toString()).each() {
			if(isMonster(it) && !it.isDead()) { allDead = false }
		}
		event.actor.getHated().each() {
			if(it.getConLevel() < 6.1) {
				if(!kidnapperKillerList.contains(it)) { kidnapperKillerList << it; it.setPlayerVar("Z08_KidnappersKilled", 0) }
				if(it.getPlayerVar("Z08_KidnappersKilled") == null && it.getPlayerVar("Z08_KidnappersKilledTotal")) {
					it.setPlayerVar("Z08_KidnappersKilled", 1)
					it.setPlayerVar("Z08_KidnappersKilledTotal", 1)
				} else {
					it.addToPlayerVar("Z08_KidnappersKilled", 1)
					it.addToPlayerVar("Z08_KidnappersKilledTotal", 1)
				}
			}
		}
		if(allDead == true) {
			kikiMap.get(event.actor.toString()).get(0).setUsable(true)
			kikiTalkList << kikiMap.get(event.actor.toString()).get(0)
			kikiTalkMap.put(kikiMap.get(event.actor.toString()).get(0), 0)
		}
		//println "#### kikiMap = ${kikiMap} ####"
	}
	
	kikiMap.put(kidnapper1.toString(), [kiki, kidnapper2, kidnapper3, kidnapper4])
	kikiMap.put(kidnapper2.toString(), [kiki, kidnapper1, kidnapper3, kidnapper4])
	kikiMap.put(kidnapper3.toString(), [kiki, kidnapper1, kidnapper2, kidnapper4])
	kikiMap.put(kidnapper4.toString(), [kiki, kidnapper1, kidnapper2, kidnapper3])
	
	//println "#### kikiSpawnedList = ${kikiSpawnedList} ####"
}

def kikiTalkLoop() {
	myManager.schedule(30) { kikiTalkLoop() }
	kikiTalkList.clone().each() {
		if(!it.isLatched()) {
			if(kikiTalkMap.get(it) < 3) {
				it.say(random(kikiRandomSayings))
				kikiTalkMap.put(it, kikiTalkMap.get(it) + 1)
			} else {
				//println "#### ${it} was left to rot ####"
				//println "### Called spawnCheck() from kikiTalkLoop() for ${it} in 15,30 seconds ####"
				it.say("Oh bother!")
				//println "#### Adding ${spawnLocationMap?.get(it)} to spawnLocations ####"
				kikiSpawnLocations << spawnLocationMap?.get(it)
				spawnLocationMap.remove(it)
				kikiSpawnedList.remove(it)
				kikiTalkList.remove(it)
				kikiTalkMap.remove(it)
				if(kikiCampsSpawned > 0) { kikiCampsSpawned-- }
				it.startWander(true)
				it.dispose()
				myManager.schedule(random(20, 40)) { spawnCheck() }
			}
		}
	}
}

kikiTalkLoop()

def scheduleNextEvent() {
	kikiEventDelay = random(MIN_EVENT_DELAY, MAX_EVENT_DELAY)
	println "#### Scheduling next kikiKidnap in ${kikiEventDelay / 60} minutes."
	myManager.schedule(kikiEventDelay) { startEvent() }
}

scheduleNextEvent()