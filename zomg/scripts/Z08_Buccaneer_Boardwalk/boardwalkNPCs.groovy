import com.gaiaonline.mmo.battle.script.*;

//=============================================
// TRIGGER ZONES                               
//=============================================

def tedStopTrigger = "tedStopTrigger"
myRooms.Boardwalk_906.createTriggerZone( tedStopTrigger, 330, 240, 435, 310 )

def tedDisposalTrigger = "tedDisposalTrigger"
myRooms.Boardwalk_906.createTriggerZone( tedDisposalTrigger, 705, 195, 890, 325 )

//def bobStopTrigger = "bobStopTrigger"
//myRooms.Boardwalk_905.createTriggerZone( bobStopTrigger, 510, 430, 620, 500 )

//def bobDisposalTrigger = "bobDisposalTrigger"
//myRooms.Boardwalk_905.createTriggerZone( bobDisposalTrigger, 220, 190, 380, 360 )

def bartStopTrigger = "bartStopTrigger"
myRooms.Boardwalk_806.createTriggerZone( bartStopTrigger, 690, 560, 845, 625 )

def bartDisposalTrigger = "bartDisposalTrigger"
myRooms.Boardwalk_806.createTriggerZone( bartDisposalTrigger, 230, 525, 360, 630 )


//=============================================
// WALK ON / WALK OFF PATHS                    
//=============================================

tedWalkOn = makeNewPatrol()
tedWalkOn.addPatrolPoint( "Boardwalk_906", 795, 255, 0 )
tedWalkOn.addPatrolPoint( "Boardwalk_906", 390, 285, 0 )

tedWalkOff = makeNewPatrol()
tedWalkOff.addPatrolPoint( "Boardwalk_906", 390, 285, 0 )
tedWalkOff.addPatrolPoint( "Boardwalk_906", 795, 255, 0 )

//bobWalkOn = makeNewPatrol()
//bobWalkOn.addPatrolPoint( "Boardwalk_905", 305, 275, 0 )
//bobWalkOn.addPatrolPoint( "Boardwalk_905", 560, 470, 0 )

//bobWalkOff = makeNewPatrol()
//bobWalkOff.addPatrolPoint( "Boardwalk_905", 560, 470, 0  )
//bobWalkOff.addPatrolPoint( "Boardwalk_905", 305, 275, 0 )

bartWalkOn = makeNewPatrol()
bartWalkOn.addPatrolPoint( "Boardwalk_806", 330, 555, 0 )
bartWalkOn.addPatrolPoint( "Boardwalk_806", 745, 510, 0 )
bartWalkOn.addPatrolPoint( "Boardwalk_806", 770, 600, 0 )

bartWalkOff = makeNewPatrol()
bartWalkOff.addPatrolPoint( "Boardwalk_806", 770, 610, 0 )
bartWalkOff.addPatrolPoint( "Boardwalk_806", 295, 580, 0 )

//=============================================
// WALK ON TRIGGER LOGIC                       
//=============================================

myManager.onTriggerIn(myRooms.Boardwalk_906, tedStopTrigger) { event ->
	if( event.actor == Ted && night == false ) {
		Ted.pause()
		Ted.setRotation( 135 )
		activateTedSpeech()
	}
}

//myManager.onTriggerIn(myRooms.Boardwalk_905, bobStopTrigger) { event ->
//	if( event.actor == Bob && night == false ) {
//		Bob.pause()
//		Bob.setRotation( 90 )
//		activateBobSpeech()
//	}
//}

myManager.onTriggerIn(myRooms.Boardwalk_806, bartStopTrigger) { event ->
	if( event.actor == Bart && night == false ) {
		Bart.pause()
		Bart.setRotation( 90 )
		activateBartSpeech()
	}
}


//=============================================
// WALK OFF LOGIC                              
//=============================================

myManager.onTriggerIn(myRooms.Boardwalk_906, tedDisposalTrigger) { event ->
	if( event.actor == Ted && night == true ) {
		Ted.dispose()
		Ted = null
	}
}

//myManager.onTriggerIn(myRooms.Boardwalk_905, bobDisposalTrigger) { event ->
//	if( event.actor == Bob && night == true ) {
//		Bob.dispose()
//		Bob = null
//	}
//}

/*myManager.onTriggerIn(myRooms.Boardwalk_806, bartDisposalTrigger) { event ->
	if( event.actor == Bart && night == true ) {
		Bart.dispose()
		Bart = null
	}
}*/


Ted = null
Bob = null
Bart = null

night = false
spawnedNPCs = false

//check the clock every 10 minutes to see if the spawn or despawn should occur.

def clockChecker() { 
	myManager.schedule(600) { spawnOrNoSpawn() }
}

def nightOrDay() {
	if( ( gst() > 1800 && gst() <= 2359 ) || ( gst() >=0 && gst() < 600 ) ) {
		night = true
//		println "**** It's NIGHT *****"
	} else {
		night = false
//		println "**** It's DAY *****"
	}
}	

def spawnOrNoSpawn() {
	nightOrDay()
//	time = gst()
//	println "<<<<<< time = ${time} and night = ${night} and spawnedNPCs = ${spawnedNPCs} >>>>>>"
	if( night == true && spawnedNPCs == true ) {
		Ted.setPatrol( tedWalkOff )
		Ted.startPatrol()
		
		//Bob.setPatrol( bobWalkOff )
		//Bob.startPatrol()

		//Bart.setPatrol( bartWalkOff )
		//Bart.startPatrol()
		
		spawnedNPCs = false
		clockChecker()
	} else if( night == false && spawnedNPCs == false ) { 
		Ted = spawnNPC("Ted-VQS", myRooms.Boardwalk_906, 795, 255)
		Ted.setDisplayName( "Ted" )
		Ted.setPatrol( tedWalkOn )
		Ted.startPatrol()

		//Bob = spawnNPC("Bob-VQS", myRooms.Boardwalk_905, 305, 275)
		//Bob.setDisplayName( "Bob" )
		//Bob.setPatrol( bobWalkOn )
		//Bob.startPatrol()
		
//		println "****** NPCs are spawned on the Boardwalk Beach ******"
		spawnedNPCs = true
		clockChecker()
	} else {
		clockChecker()
	}
}

spawnOrNoSpawn()

//------------------------------------------------------------
// TED THE TOURIST DIALOG                                     
//------------------------------------------------------------

def activateTedSpeech() {

	def DefaultTed = Ted.createConversation( "DefaultTed", true )

	default1 = [id:1]
	default1.npctext = "Hey, friend. My name is Ted. Got a sec?"
	default1.options = []
	default1.options << [text:"Sure thing! What do you need?", result: 2]
	default1.options << [text:"Nope! Not right now!", result: 10]
	DefaultTed.addDialog( default1, Ted )

	default2 = [id:2]
	default2.npctext = "Well...I'll tell ya. I think I'm lost. A friend of mine told me to head up here to meet him at his house, but I'll be darned if I can find his house or any of the town guards to ask about it."
	default2.playertext = "Ummmm...unless your friend lives in a bungalow on the beach, I think you might be more than a *little* lost."
	default2.result = 3
	DefaultTed.addDialog( default2, Ted )

	default3 = [id:3]
	default3.npctext = "Oh, drat. Where do you think I went wrong?"
	default3.playertext = "Did your friend say anything about Barton's *south* gate?"
	default3.result = 4
	DefaultTed.addDialog( default3, Ted )

	default4 = [id:4]
	default4.npctext = "Well...yes."
	default4.playertext = "And did you know that you're very far NORTH of Barton Town now?"
	default4.result = 5
	DefaultTed.addDialog( default4, Ted )

	default5 = [id:5]
	default5.npctext = "...I have to admit that the beach had me a tad concerned. I didn't remember that on the brochure..."
	default5.playertext = "No. I can see why. You should probably head back down past Bass'ken Lake to get back to Town."
	default5.result = 6
	DefaultTed.addDialog( default5, Ted )

	default6 = [id:6]
	default6.npctext = "Oh, I don't know. Now that I'm up here, I might as well wait for the 'pirate' cruise."
	default6.playertext = "PIRATE Cruise?"
	default6.result = 7
	DefaultTed.addDialog( default6, Ted )

	default7 = [id:7]
	default7.npctext = "Yes...that nice fellow by the boardwalk offered to take me onto his ship sometime soon for a long cruise in his 'pirate ship'. It sounds fantastic! I can't wait!"
	default7.playertext = "Nice fellow? Long 'pirate' cruise? Where did you say you met this fellow?"
	default7.result = 8
	DefaultTed.addDialog( default7, Ted )

	default8 = [id:8]
	default8.npctext = "Up by where the boardwalk sticks out into the water. The rightmost of the two boardwalks as you face the ocean. Nice fellow. The man by the boardwalk, I mean. Lots of bright costuming for the tourists, but VERY authentic. I'm impressed!"
	default8.playertext = "Ooookay. Well...good luck on your cruise there, Ted. That cruise might be longer than you think it's going to be."
	default8.result = 9
	DefaultTed.addDialog( default8, Ted )

	default9 = [id:9]
	default9.npctext = "Do you think so? Really? Fan-tastic!!"
	default9.result = DONE
	DefaultTed.addDialog( default9, Ted )
	
	default10 = [id:10]
	default10.npctext = "Okay then! Thanks anyway."
	default10.result = DONE
	DefaultTed.addDialog( default10, Ted )
}


//------------------------------------------------------------
// BOB THE TOURIST DIALOG                                     
//------------------------------------------------------------

Bob = spawnNPC("Bob-VQS", myRooms.Boardwalk_905, 560, 470)
Bob.setDisplayName( "Bob" )
Bob.setRotation( 90 )

DefaultBob = Bob.createConversation( "DefaultBob", true )

def bob1 = [id:1]
bob1.npctext = "Hello der."
bob1.options = []
bob1.options << [text:"Hello yourself.", result: 2]
bob1.options << [text:"Go away. Ya bother me.", result: DONE]
bob1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 236, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 236)
			Bob.say("Der you are. Dat freebie's fer you.")
		} else {
			Bob.say("Nuttin' now. Maybe later, eh?")
			event.player.centerPrint( "You've already received your freebie from Bob today. Try again tomorrow!" )
		}
	})
}, result: DONE]
DefaultBob.addDialog( bob1, Bob )

def bob2 = [id:2]
bob2.npctext = "And why would I want t'do dat den?"
bob2.playertext = "Do what?"
bob2.result = 3
DefaultBob.addDialog( bob2, Bob )

def bob3 = [id:3]
bob3.npctext = "Say hello to myself."
bob3.playertext = "No. Not *to* yourself. I was just saying hi back."
bob3.result = 4
DefaultBob.addDialog( bob3, Bob )

def bob4 = [id:4]
bob4.npctext = "Is dat a local greeting den?"
bob4.playertext = "What?!?"
bob4.result = 5
DefaultBob.addDialog( bob4, Bob )

def bob5 = [id:5]
bob5.npctext = "Dis 'hi back' yer sayin'."
bob5.playertext = "Oh, I see...no! I was just saying hello to you because you said it first to me!"
bob5.result = 6
DefaultBob.addDialog( bob5, Bob )

def bob6 = [id:6]
bob6.npctext = "Oh, right den! But...when did I say it first to you?"
bob6.playertext = "Just a minute ago! When you said 'Hello der!'"
bob6.result = 7
DefaultBob.addDialog( bob6, Bob )

def bob7 = [id:7]
bob7.npctext = "Right, right. I thought you was sayin' dat I said 'hi back' first. But what yer sayin' is I shoulda said 'hi back' instead of 'hello der', eh?"
bob7.playertext = "No, that's not it at all! You *never* say 'hi back'."
bob7.result = 8
DefaultBob.addDialog( bob7, Bob )

def bob8 = [id:8]
bob8.npctext = "Oh, really. Well if I don't learn the local talk how am I gonna fit in den?"
bob8.playertext = "IT'S NOT LOCAL TALK! Just don't say 'hi back'"
bob8.result = 9
DefaultBob.addDialog( bob8, Bob )

def bob9 = [id:9]
bob9.npctext = "Sure, sure. But it don't seem very sociable, y'know. What if someone says 'hi' to me first?"
bob9.playertext = "Gah. Are you kidding me?!?"
bob9.result = 10
DefaultBob.addDialog( bob9, Bob )

def bob10 = [id:10]
bob10.npctext = "Oh no. I never kid. I'm not a goat, don't y'know."
bob10.playertext = "Oh we are SO done talking now."
bob10.result = 11
DefaultBob.addDialog( bob10, Bob )

def bob11 = [id:11]
bob11.npctext = "Okay den. Lo back!"
bob11.playertext = "...all right...I have to ask. 'Lo back'?"
bob11.result = 12
DefaultBob.addDialog( bob11, Bob )

def bob12 = [id:12]
bob12.npctext = "Well, I figured that if 'hi back' was a greeting, then 'lo back' must be a way to say 'see ya!'"
bob12.playertext = "...yes. Yes that's it exactly. 'lo back' DOES mean goodbye. You should use it ALL the time."
bob12.result = 13
DefaultBob.addDialog( bob12, Bob )

def bob13 = [id:13]
bob13.npctext = "Sounds good den. Y'know...if you don't mind me sayin' so, you locals seem to be a tiny bit high-strung. Maybe you should relax a bit once in a while, eh? Take in a sauna or something?"
bob13.playertext = "*twitch* Yeah. I'll do that."
bob13.result = 14
DefaultBob.addDialog( bob13, Bob )

def bob14 = [id:14]
bob14.npctext = "Good for you. Lo back den!"
bob14.result = DONE
DefaultBob.addDialog( bob14, Bob )