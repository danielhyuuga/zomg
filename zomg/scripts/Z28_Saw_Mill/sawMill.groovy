import com.gaiaonline.mmo.battle.script.*;

ML = 4.0
minML = 4.0
MaxML = 5.1

playerForHazard = null
newSpawner = null
oldSpawner = null
hazardDelay = 3

//Papa Saw
papaSaw = myRooms.SawMill_1.spawnStoppedSpawner( "papaSaw", "buzz_saw_demi", 1 )
papaSaw.setPos( 950, 470 )
papaSaw.setHateRadiusForChildren( 2000 )
papaSaw.setMonsterLevelForChildren( ML )

papa = null

//Mama Saws
lilMamaSaw = myRooms.SawMill_1.spawnStoppedSpawner( "lilMamaSaw", "buzz_saw_LT", 1 )
lilMamaSaw.setPos( 950, 470 )
lilMamaSaw.setHateRadiusForChildren( 2000 )
lilMamaSaw.setMonsterLevelForChildren( ML )

lilMama = null

bigMamaSaw = myRooms.SawMill_1.spawnStoppedSpawner( "bigMamaSaw", "buzz_saw_LT", 1 )
bigMamaSaw.setPos( 180, 560 )
bigMamaSaw.setHateRadiusForChildren( 2000 )
bigMamaSaw.setMonsterLevelForChildren( ML )

bigMama = null

// Buzz Saw Hazards
hazardGrunt1 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardGrunt1", "buzz_saw", 200)
hazardGrunt1.setPos( 250, 500 )
hazardGrunt1.setGuardPostForChildren( "SawMill_1", 250, 500, 45 )
hazardGrunt1.setHateRadiusForChildren( 10 )
hazardGrunt1.setMonsterLevelForChildren( ML )

hazardGrunt2 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardGrunt2", "buzz_saw", 200)
hazardGrunt2.setPos( 510, 460 )
hazardGrunt2.setGuardPostForChildren( "SawMill_1", 510, 460, 135 )
hazardGrunt2.setHateRadiusForChildren( 10 )
hazardGrunt2.setMonsterLevelForChildren( ML )

hazardGrunt3 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardGrunt3", "buzz_saw", 200)
hazardGrunt3.setPos( 830, 380 )
hazardGrunt3.setGuardPostForChildren( "SawMill_1", 830, 380, 135 )
hazardGrunt3.setHateRadiusForChildren( 10 )
hazardGrunt3.setMonsterLevelForChildren( ML )

hazardGrunt4 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardGrunt4", "buzz_saw", 200)
hazardGrunt4.setPos( 1080, 540 )
hazardGrunt4.setGuardPostForChildren( "SawMill_1", 1080, 540, 225 )
hazardGrunt4.setHateRadiusForChildren( 10 )
hazardGrunt4.setMonsterLevelForChildren( ML )

hazardGrunt5 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardGrunt5", "buzz_saw", 200)
hazardGrunt5.setPos( 720, 765 )
hazardGrunt5.setGuardPostForChildren( "SawMill_1", 720, 765, 225 )
hazardGrunt5.setHateRadiusForChildren( 10 )
hazardGrunt5.setMonsterLevelForChildren( ML )

hazardGrunt6 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardGrunt6", "buzz_saw", 200)
hazardGrunt6.setPos( 310, 710 )
hazardGrunt6.setGuardPostForChildren( "SawMill_1", 310, 710, 315 )
hazardGrunt6.setHateRadiusForChildren( 10 )
hazardGrunt6.setMonsterLevelForChildren( ML )

hazardElite1 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardElite1", "buzz_saw_elite", 200)
hazardElite1.setPos( 250, 500 )
hazardElite1.setGuardPostForChildren( "SawMill_1", 250, 500, 45 )
hazardElite1.setHateRadiusForChildren( 10 )
hazardElite1.setMonsterLevelForChildren( ML )

hazardElite2 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardElite2", "buzz_saw_elite", 200)
hazardElite2.setPos( 510, 460 )
hazardElite2.setGuardPostForChildren( "SawMill_1", 510, 460, 135 )
hazardElite2.setHateRadiusForChildren( 10 )
hazardElite2.setMonsterLevelForChildren( ML )

hazardElite3 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardElite3", "buzz_saw_elite", 200)
hazardElite3.setPos( 830, 380 )
hazardElite3.setGuardPostForChildren( "SawMill_1", 830, 380, 135 )
hazardElite3.setHateRadiusForChildren( 10 )
hazardElite3.setMonsterLevelForChildren( ML )

hazardElite4 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardElite4", "buzz_saw_elite", 200)
hazardElite4.setPos( 1080, 540 )
hazardElite4.setGuardPostForChildren( "SawMill_1", 1080, 540, 225 )
hazardElite4.setHateRadiusForChildren( 10 )
hazardElite4.setMonsterLevelForChildren( ML )

hazardElite5 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardElite5", "buzz_saw_elite", 200)
hazardElite5.setPos( 720, 765 )
hazardElite5.setGuardPostForChildren( "SawMill_1", 720, 765, 225 )
hazardElite5.setHateRadiusForChildren( 10 )
hazardElite5.setMonsterLevelForChildren( ML )

hazardElite6 = myRooms.SawMill_1.spawnStoppedSpawner( "hazardElite6", "buzz_saw_elite", 200)
hazardElite6.setPos( 310, 710 )
hazardElite6.setGuardPostForChildren( "SawMill_1", 310, 710, 315 )
hazardElite6.setHateRadiusForChildren( 10 )
hazardElite6.setMonsterLevelForChildren( ML )

//===========================================
//SPAWNING LOGIC                             
//===========================================

playerSet = [] as Set
difficulty = null

gameStarted = false
papaSawDead = false
mamasSpawned = false
mamasDead = false
hz1Dead = true
hz2Dead = true
hz3Dead = true
hz4Dead = true
hz5Dead = true
hz6Dead = true

lilMama = null
bigMama = null

//===========================================
// onEnter/onExit Logic                      
//===========================================

myManager.onEnter( myRooms.SawMill_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet << event.actor
		if( gameStarted == false ) {
			myManager.schedule(3) { event.actor.centerPrint( "The interior of the Saw Mill smells like wood chips and cleaning oil" ) }
			myManager.schedule(6) { event.actor.centerPrint( "You hear a subdued sound of buzz saws whirring, but it seems muffled somehow." ) }
			myManager.schedule(9) { event.actor.centerPrint( "Almost as if the blades were in the walls...or under the floor..." ) }
		}
		//The first player into the scenario sets the difficulty for everyone else
		if( difficulty == null) {
			difficulty = event.actor.getTeam().getAreaVar( "Z28_Saw_Mill", "Z28SawMillDifficulty" )
			if( difficulty == 0 ) { difficulty = 3 }
			//println "^^^^ difficulty = ${difficulty} ^^^^"
		}			
	}
}

myManager.onExit( myRooms.SawMill_1 ) { event ->
	if(isPlayer(event.actor)) {
		playerSet.remove( event.actor )
	}
}

def adjustML() {
	ML = minML

	//kick players out if they are cheating
	/*playerSet.clone().each{
		if( it.getConLevel() >= MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "BASSKEN_102", 680, 610 )
		}
	}*/
	
	if( playerSet.isEmpty() ) {
		return;
	}
	
	//adjust the encounter ML to the group
	random( playerSet ).getCrew().each() {
		if( it.getConLevel() >= ML && it.getConLevel() < MaxML ) { 
			ML = it.getConLevel()
		} else if( it.getConLevel() >= MaxML ) {
			ML = MaxML
		}
	}

	//DIFFICULTY
	if( difficulty == 1 ) { ML = ML - 0.6 }
	if( difficulty == 3 ) { ML = ML + 0.6 }
	
	//set the monster levels for all spawners and children again
	hazardGrunt1.setMonsterLevelForChildren( ML )
	hazardGrunt2.setMonsterLevelForChildren( ML )
	hazardGrunt3.setMonsterLevelForChildren( ML )
	hazardGrunt4.setMonsterLevelForChildren( ML )
	hazardGrunt5.setMonsterLevelForChildren( ML )
	hazardGrunt6.setMonsterLevelForChildren( ML )
	hazardElite1.setMonsterLevelForChildren(ML)
	hazardElite2.setMonsterLevelForChildren(ML)
	hazardElite3.setMonsterLevelForChildren(ML)
	hazardElite4.setMonsterLevelForChildren(ML)
	hazardElite5.setMonsterLevelForChildren(ML)
	hazardElite6.setMonsterLevelForChildren(ML)
	papaSaw.setMonsterLevelForChildren( ML )
	lilMamaSaw.setMonsterLevelForChildren( ML )
	bigMamaSaw.setMonsterLevelForChildren( ML )
}

def chooseNewSpawner() {
	if(newSpawner != null) { oldSpawner = newSpawner }
	newSpawner = random( hazardList )
	
	hazardList.remove(newSpawner)
	if(oldSpawner != null) { hazardList << oldSpawner }
}

def checkDazedStatus() {
	numDazed = 0
	myRooms.SawMill_1.getActorList().each { if( isPlayer( it ) && it.isDazed() ) { numDazed ++ } }
	if( numDazed >= playerSet.size() ) {
		myRooms.SawMill_1.getActorList().each {
			if( isPlayer( it ) ) {
				it.centerPrint( "After the sawdust clears, you find yourself outside the Saw Mill." )
				it.warp( "BASSKEN_102", 680, 610 )
			}
		}			
	} else {
		myManager.schedule(2) { checkDazedStatus() }
	}
}


//===========================================
// HAZARD SPAWNING ROUTINES                  
//===========================================
//Summary Notes:
// - If Papa is dead, don't spawn Hazards anymore

numHazardAttacks = 0
papaSpawned = false

hazardGruntList = [ hazardGrunt1, hazardGrunt2, hazardGrunt3, hazardGrunt4, hazardGrunt5, hazardGrunt6 ]
hazardEliteList = [ hazardElite1, hazardElite2, hazardElite3, hazardElite4, hazardElite5, hazardElite6 ]

//============================================
// GAME START                                 
//============================================

def startGame() {
	if(difficulty == 3) {
		hazardList = hazardEliteList
	} else {
		hazardList = hazardGruntList
	}
	attackList = hazardList.clone()
	attackList = hazardList.clone()
	if(difficulty == 3) {
		numSpawn = playerSet.size() / 2
		numSpawn = numSpawn.intValue()
		if(numSpawn < 1) {
			numSpawn = 1
		}
	} else {
		numSpawn = playerSet.size()
	}
	chooseNewSpawner()
	if( difficulty == 1 ) { attackWaves = 2; hazardDelay = 3 }
	if( difficulty == 2 ) { attackWaves = 4; hazardDelay = 2 }
	if( difficulty == 3 ) { attackWaves = 6; hazardDelay = 1 }
	adjustML()
	spawnAttackWaves()
}

//spawn attack waves according to difficulty setting
def spawnAttackWaves() {
	if( papaSawDead == true ) return
	//println "^^^^ attackWaves = ${attackWaves} ^^^^"
	if( attackWaves > 0 ) {
		attackWaves --
		//println "^^^^ Decrementing attackWaves to ${attackWaves} ^^^^"
		myManager.schedule( 3 ) {
			attackList = hazardList.clone()
			
			if(difficulty == 3) {
				numSpawn = playerSet.size() / 2
				numSpawn = numSpawn.intValue()
				if(numSpawn < 1) {
					numSpawn = 1
				}
			} else {
				numSpawn = playerSet.size()
			}
			
			chooseNewSpawner()
			adjustML()
			hazardAttack()
		}
	} else {
		myManager.schedule(2) {
			hazardDeathWatch()
		}
	}
}

def hazardAttack() {
	if( papaSawDead == true ) return
	if( numSpawn > 0 ) {
		attackList.remove( newSpawner )
		attacker = newSpawner.forceSpawnNow()
		if( numSpawn == 1 ) { attacker.say ("It's Cuttin' Time!" ) }
		attacker.setHateRadius( 2000 )
		attacker.addHate( random( playerSet ), 200 )
		if( !attackList.isEmpty() ) { newSpawner = random( attackList ) }
		numSpawn --
		hazardAttack()
	} else {
		spawnAttackWaves()
	}
}

//don't start pop-up attacks until all the attack waves are destroyed
def hazardDeathWatch() {
	//println "^^^^ Number spawned = ${myRooms.SawMill_1.getSpawnTypeCount( "buzz_saw" ) + myRooms.SawMill_1.getSpawnTypeCount("buzz_saw_elite")} ^^^^"
	if( myRooms.SawMill_1.getSpawnTypeCount( "buzz_saw" ) + myRooms.SawMill_1.getSpawnTypeCount("buzz_saw_elite" ) == 0 ) {
		numHazardAttacks ++
		//println "^^^^ numHazardAttacks = ${numHazardAttacks} ^^^^"
		//once all attack waves are spawned, check for mamas and papas, then start the hazard cycle again
		if( mamasDead == false ) {
			if( difficulty == 1 && numHazardAttacks == 3 ) {
				myManager.schedule( 3 ) { theMamas() }
			} else if ( difficulty == 2 && numHazardAttacks == 5 ) {
				myManager.schedule( 3 ) { theMamas() }
			} else if ( difficulty == 3 && numHazardAttacks == 7 ) {
				myManager.schedule( 3 ) { theMamas() }
			}
		} else if( papaSpawned == false ) {
			if( difficulty == 1 && numHazardAttacks >= 5 ) {
				myManager.schedule( 3 ) { papaSpawn() }
			} else if( difficulty == 2 && numHazardAttacks >= 7 ) {
				myManager.schedule( 3 ) { papaSpawn() }
			} else if( difficulty == 3 && numHazardAttacks >= 9 ) {
				myManager.schedule( 3 ) { papaSpawn() }
			}
		}
		myManager.schedule(3) { startHazardCycle(); /*println "^^^^ Running startHazardCycle ^^^^"*/ }
	} else {
		myManager.schedule(2) { hazardDeathWatch(); /*println "^^^^ Running hazardDeathWatch ^^^^"*/ }
	}
}

def startHazardCycle() {
	if( papaSawDead == true ) return
	if( difficulty == 1 ) { releaseTime = random( 20, 25 ); attackWaves = 2 }
	if( difficulty == 2 ) { releaseTime = random( 15, 19 ); attackWaves = 4 }
	if( difficulty == 3 ) { releaseTime = random( 13, 17 ); attackWaves = 6 }
	//println "**** time until next attack wave = ${releaseTime} ****"
	releaseTimer = myManager.schedule( releaseTime ) {
		popUpTimer1.cancel()
		popUpTimer2.cancel()
		myRooms.SawMill_1.getActorList().each { if( isMonster( it ) && (it.getMonsterType() == "buzz_saw" || it.getMonsterType() == "buzz_saw_elite") ) { it.dispose() } }
		attackList = hazardList.clone()
		chooseNewSpawner()
		adjustML()
		spawnAttackWaves()
		//println "^^^^ Restarting spawnAttackWaves, timers cancelled ^^^^"
	}
	popUpHazards()
	//println "^^^^ Running popUpHazards ^^^^"
}

hazardSayings = [ "buzz!", "Over here!", "Here now!", "Surprise!", "Gotcha!", "buzz buzz", "Snikety Snak!", "Mayhem!", "Bzzzzt!" ]

//Every five seconds, a Hazard pops up around the edge of the room. This goes on for a period, then do "hazardAttack". Then when everything is killed, start the pop-ups again.
def popUpHazards() {
	if( playerSet.isEmpty() ) return
	if( papaSawDead == true ) return
	//println "^^^^ Spawning popup hazards ^^^^"
	chooseNewSpawner()
	
	playerForHazard = random(playerSet)
	warpX = playerForHazard.getX()
	warpY = playerForHazard.getY()
	
	newSpawner.warp("SawMill_1", warpX, warpY)
	
	hazard = newSpawner.forceSpawnNow()
	hazard.setBaseSpeed(0)
	if(!playerForHazard.isDead()) { hazard.addHate(playerForHazard, 1) }
	
	roll = random( 100 )
	if( roll < 25 ) {
		hazard.say( random( hazardSayings ) )
	}
	popUpTimer1 = myManager.schedule(2) { 
		if( !hazard.isDead() ) {
			hazard.dispose()
		}
		popUpTimer2 = myManager.schedule(hazardDelay) { popUpHazards(); /*println "^^^^ Running popUpHazards ^^^^"*/ }
	}
}		

//The MAMAS (lilMama and bigMama)
def theMamas() {
	adjustML()
	mamaSpawnList = playerSet.clone()
	mamasSpawned = true
	if( playerSet.size() <= 3 ) {
		lilMama = lilMamaSaw.forceSpawnNow()
		myManager.schedule(2) { if( !lilMama.isDead() ) { lilMama.say( "What in confunction're y'all doing here?" ) } }
		myManager.schedule(4) { if( !lilMama.isDead() ) { lilMama.say( "Don't matter! Thar's only one penalty fer trespassin'." ) } }
		myManager.schedule(6) {
			if( !lilMama.isDead() ) {
				lilMama.say( "SLICE'N'DICE!" )
				lilMama.addHate( random( playerSet ), 20 )
				myManager.onHealth( lilMama ) { event ->
					if( event.didTransition( 50 ) && event.isDecrease() ) {
						lilMama.say("Papa! Papa! Help yer li'l mama! *sawb*")
					}
				}
			}
			mamasDeathWatch()
		}
	//if there are four or more players in the Saw Mill	
	} else {
		lilMama = lilMamaSaw.forceSpawnNow()
		myManager.schedule(2) { if( !lilMama.isDead() ) { lilMama.say( "Looks like 'stime t'clean house, Big Mama!" ); bigMama = bigMamaSaw.forceSpawnNow() } }
		myManager.schedule(4) { if( !bigMama.isDead() ) { bigMama.say( "Ayup, Li'l Mama! The clutter'n'here is sumthin' fierce!" ) } }
		myManager.schedule(6) {
			if( !lilMama.isDead() ) {
				lilMama.say( "CLEAN HOUSE!" )
				lilMama.addHate( random( playerSet ), 20 )
				myManager.onHealth( lilMama ) { event ->
					if( event.didTransition( 50 ) && event.isDecrease() ) {
						lilMama.say("Papa! Papa! Whar is ya, Papa? *sawb*")
					}
				}
			}
			if( !bigMama.isDead() ) {
				bigMama.say( "CLEAN HOUSE!" )
				bigMama.addHate( random( playerSet ), 20 )
				myManager.onHealth( bigMama ) { event ->
					if( event.didTransition( 25 ) && event.isDecrease() ) {
						bigMama.say("PAPA! Wher'n tarnations are y'hidin', y'dull blade?")
					}
				}
			}
			mamasDeathWatch()
		}
	}
}

def synchronized mamasDeathWatch() {
	if( myRooms.SawMill_1.getSpawnTypeCount( "buzz_saw_LT" ) == 0 && mamasDead == false ) {
		mamasDead = true
	} else {
		myManager.schedule(2) { mamasDeathWatch() }
	}
}

//Then PAPA
def papaSpawn() {
	if( papaSawDead == true ) return
	adjustML()
	papa = papaSaw.forceSpawnNow()
	papaSpawned = true
	myManager.schedule(2) { if( !papa.isDead() ) { papa.say( "I'm here! I'm here! Where y'all hidin'?" ) } }
	myManager.schedule(4) {
		if( !papa.isDead() ) { 
			papa.say( "Y'all ain't hidin'! Yer dead! And them that did it'r'gonna PAY!" )
			papa.addHate( random( playerSet ), 20 )
			myManager.onHealth( papa ) { event ->
				if( event.didTransition( 66 ) && event.isDecrease() ) {
					papa.say("There b'nuthin' b'scraps a'ya when I'm done!!")
				} else if( event.didTransition( 33 ) && event.isDecrease() ) {
					papa.say("I'm on my way't'ya, Mamas! And I'm bringin' company, so use th'good forks!")
				}
			}
		}
	} 
	runOnDeath( papa, { papaSawDead = true; myManager.schedule(3) { updatesAndRewards() } } )
}

rewardList = []
	
def updatesAndRewards() {
	if( !playerSet.isEmpty() ) {
		playerSet.clone().each() {
			it.centerPrint( "You cleared the Saw Mill and destroyed Papa Saw! Great job!" )
		}
		rewardList = mamaSpawnList.intersect( playerSet )
		myManager.schedule(3) { 
			if( !playerSet.isEmpty() ) {
				playerSet.clone().each {
					//tell people that weren't here for the Mama spawns why they aren't getting a reward
					if( !rewardList.contains( it ) ) {
						it.centerPrint( "You did not receive a reward because you joined the scenario too late after it started." )
					}
				}		
				//REWARDS
				rewardList.clone().each() {
					if( it.getConLevel() <= MaxML ) {
						//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
						if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
						else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
						else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

						//gold reward
						goldGrant = ( random( 25, 75 ) * goldMult ).intValue() 
						it.centerPrint( "You receive ${goldGrant} gold!" )
						it.grantCoins( goldGrant )

						//orb reward
						orbGrant = ( random( 4, 6 ) * orbMult ).intValue()
						it.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
						it.grantQuantityItem( 100257, orbGrant )

						//check to see if anyone in the room is on the quest and update them if they are
						if( it.isOnQuest( 83, 2 ) ) {
							it.updateQuest( 83, "Gustav-VQS" )
						}
					} else {
						it.centerPrint( "Your level is too high for this scenario. No reward for you!" )
					}
				}
			}
		}
	}
}


//===========================================
// THE AMBUSH TRIGGER                        
//===========================================

def ambushTrigger = "ambushTrigger"
myRooms.SawMill_1.createTriggerZone( ambushTrigger, 450, 400, 570, 810 )

myManager.onTriggerIn(myRooms.SawMill_1, ambushTrigger ) { event ->
	if( isPlayer( event.actor ) && gameStarted == false ) {
		gameStarted = true
		startGame()
		checkDazedStatus()
	}
}

//===========================================
// EXIT THE SAW MILL                          
//===========================================
onClickExitSawMill = new Object()

sawMillExit = makeSwitch( "SawMill_Out", myRooms.SawMill_1, 50, 540 )
sawMillExit.unlock()
sawMillExit.off()
sawMillExit.setRange( 250 )

def exitSawMill = { event ->
	synchronized( onClickExitSawMill ) {
		sawMillExit.off()
		if( isPlayer( event.actor ) ) {
			event.actor.warp( "BASSKEN_102", 680, 610 )
			//DEBUG!!!!
			event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 0 )
			//DEBUG!!!!
		}
	}
}

sawMillExit.whenOn( exitSawMill )
