import com.gaiaonline.mmo.battle.script.*;

MinML = 5.0
ML = 5.0
MaxML = 10.0

numAdjusts = 0
totalPlayersPresentAtSpawns = 0

playerRecord = [:]

//------------------------------------------
// SPAWNERS                                 
//------------------------------------------

roachSpawner = myRooms.Hive_1.spawnStoppedSpawner( "roachSpawner", "alien_bug", 100)
roachSpawner.setPos( 420, 910 )
roachSpawner.setHomeForChildren( "Hive_1", 750, 520 )
roachSpawner.setInitialMoveForChildren( "Hive_1", 750, 520 )
roachSpawner.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roachSpawner.setMonsterLevelForChildren( ML )

roachLTSpawner = myRooms.Hive_1.spawnStoppedSpawner( "roachLTSpawner", "alien_bug_LT", 100)
roachLTSpawner.setPos( 420, 910 )
roachLTSpawner.setHomeForChildren( "Hive_1", 750, 520 )
roachLTSpawner.setInitialMoveForChildren( "Hive_1", 750, 520 )
roachLTSpawner.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roachLTSpawner.setMonsterLevelForChildren( ML )

walkerSpawner = myRooms.Hive_1.spawnStoppedSpawner( "walkerSpawner", "alien_walker", 100)
walkerSpawner.setPos( 420, 910 )
walkerSpawner.setHomeForChildren( "Hive_1", 750, 520 )
walkerSpawner.setInitialMoveForChildren( "Hive_1", 750, 520 )
walkerSpawner.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
walkerSpawner.setMonsterLevelForChildren( ML )

walkerLTSpawner = myRooms.Hive_1.spawnStoppedSpawner( "walkerLTSpawner", "alien_walker_LT", 100)
walkerLTSpawner.setPos( 420, 910 )
walkerLTSpawner.setHomeForChildren( "Hive_1", 750, 520 )
walkerLTSpawner.setInitialMoveForChildren( "Hive_1", 750, 520 )
walkerLTSpawner.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
walkerLTSpawner.setMonsterLevelForChildren( ML )

deathRoachSpawner = myRooms.Hive_1.spawnStoppedSpawner( "deathRoachSpawner", "alien_death_bug", 100)
deathRoachSpawner.setPos( 420, 910 )
deathRoachSpawner.setHomeForChildren( "Hive_1", 750, 520 )
deathRoachSpawner.setInitialMoveForChildren( "Hive_1", 750, 520 )
deathRoachSpawner.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
deathRoachSpawner.setMonsterLevelForChildren( ML )

//------------------------------------------
//ALLIANCES                                 
//------------------------------------------
roachSpawner.allyWithSpawner(roachLTSpawner)
roachSpawner.allyWithSpawner(walkerSpawner)
roachSpawner.allyWithSpawner(walkerLTSpawner)
roachSpawner.allyWithSpawner(deathRoachSpawner)

roachLTSpawner.allyWithSpawner(walkerSpawner)
roachLTSpawner.allyWithSpawner(walkerLTSpawner)
roachLTSpawner.allyWithSpawner(deathRoachSpawner)

walkerSpawner.allyWithSpawner(walkerLTSpawner)
walkerSpawner.allyWithSpawner(deathRoachSpawner)

walkerLTSpawner.allyWithSpawner(deathRoachSpawner)

//=======================================================
// RANDOMIZE MONSTER LEVELS                              
//=======================================================
def adjustML() {
	numAdjusts ++
	
	//this is used for orb reward size
	playerSet.each{ if( !it.isDazed() ) { totalPlayersPresentAtSpawns ++ } }
	
	//keep track of the players that start this wave, and also increment the number of spawn waves they have encountered so far	
	//this is used for badge rewards
	playerSet.each{
		if( !it.isDazed() ) { 
			if( !playerRecord.containsKey( it ) ) {
				playerRecord.put( it, 1 )
			} else {
				curAdjusts = playerRecord.get( it ) + 1
				playerRecord.put( it, curAdjusts )
			}
		}
	}
		
	ML = MinML

	//kick players out if they are cheating
	playerSet.clone().each{
		if( it.getConLevel() > MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "Aqueduct_306", 420, 420 )
		}
	}

	//adjust the encounter ML to the group
	random( playerSet ).getCrew().each() {
		if( it.getConLevel() >= ML && it.getConLevel() < MaxML ) { 
			ML = it.getConLevel()
		} else if( it.getConLevel() >= MaxML ) {
			ML = MaxML
		}
	}
	
	//start out a bit easier so players can warm into the game
	if( lap <= 3 ) {
		ML = ML - ( 0.1 * ( 4 - lap ) )
	//Begin increasing the ML of the alien bugs after the deathRoachLap has been reached
	} else if( lap > deathRoachLap ) {
		postDeathLapNum = lap - deathRoachLap
		ML = ML + ( 0.1 * ( (lap - deathRoachLap) ).intValue() ) // + 0.1 ML for every other lap after deathRoachLap
	}
	
	//now set the spawner MLs
	roachSpawner.setMonsterLevelForChildren( ML )
	roachLTSpawner.setMonsterLevelForChildren( ML )
	walkerSpawner.setMonsterLevelForChildren( ML )
	walkerLTSpawner.setMonsterLevelForChildren( ML )
	deathRoachSpawner.setMonsterLevelForChildren( ML )
	
	//Make the playerCount equal to the number of players in the Crew involved in the scenario
	playerCount = random( playerSet ).getCrew().size()
	
	//if the players have managed to sneak in an extra player or two, count them also but never allow a value over 6 to be used
	if( playerSet.size() > 6 ) {
		playerCount = 6
	} else if( playerSet.size() > playerCount ) {
		playerCount = playerSet.size()
	}
}

//=======================================================
// onEnter LOGIC                                         
//=======================================================

playerSet = [] as Set
nonDazedSet = [] as Set

gameStarted = false
someoneOnQuest = false

onEnterBlock = new Object()

myManager.onEnter(myRooms.Hive_1) { event ->
	synchronized( onEnterBlock ) {
		if( isPlayer(event.actor) ) {
			//After the rebalancing, reset all player high scores to zero
			if( event.actor.getPlayerVar( "Z16Reset" ) == null || event.actor.getPlayerVar( "Z16Reset" ) != 1 ) {
				event.actor.setPlayerVar( "Z16Reset", 1 )
				event.actor.setPlayerVar( "newHivePersonalBest", 0 )
			}
			//everytime someone enters the Hive, see if someone in their Crew is on the INTO THE STAR PORTAL task or not
			event.actor.getCrew().each{
				if( it.isOnQuest( 100 ) ) {
					someoneOnQuest = true
				}
			}
			event.actor.centerPrint( "Things are quiet now, but they'll heat up as soon as you step off the portal and alert the Hive." )
			
			myManager.schedule(3) {
				//if no one is on the quest, then publish the personal best score to the player that just entered
				if( someoneOnQuest == false ) {
					//check to see if a personal best is recorded already.
					if( event.actor.getPlayerVar(  "newHivePersonalBest" ) == 0 || event.actor.getPlayerVar( "newHivePersonalBest" ) == null ) {
						event.actor.centerPrint( "Try to kill as many bugs as you can...before you die." )
					} else {
						personalBest = event.actor.getPlayerVar( "newHivePersonalBest" )
						event.actor.centerPrint( "Your highest previous spawn count is ${ personalBest.intValue() }." )
					}
				}
			}				
			playerSet << event.actor
		}
	}
}

myManager.onExit(myRooms.Hive_1) { event ->
	if( isPlayer(event.actor) ) {
		playerSet.remove( event.actor )
	}
}

//=======================================================
// START GAME TRIGGER                                    
//=======================================================

def startGameTrigger = "startGameTrigger"
myRooms.Hive_1.createTriggerZone( startGameTrigger, 560, 420, 900, 590 )

//Once a player steps off the portal pad, the game begins
myManager.onTriggerOut(myRooms.Hive_1, startGameTrigger) { event ->
	if( isPlayer( event.actor ) && gameStarted == false ) {
		gameStarted = true
		initialDelay() //this pushes the start of the game
	}
}

//=======================================================
// INITIAL GAME LOGIC                                    
//=======================================================

//Initial Delay provides an easily adjustable starting timer for all team members to get into the zone before the game starts          
def initialDelay() {
	playerSet.clone().each{ it.centerPrint( "As soon as you step off the pad, you hear skittering start from within the tunnels..." ) }
	myManager.schedule(1) { sound( "tunnelChitter" ).toZone(); playerSet.clone().each { it.centerPrint( "The sounds are getting closer..." ) } }
	myManager.schedule(2) { playerSet.clone().each { it.centerPrint( "Oh no...they're almost here..." ) } }
	myManager.schedule(3) { playerSet.clone().each { it.centerPrint( "Look out!" ) } } //start the game and be ready to end it
	myManager.schedule(3) { startGame() }
}

def startGame() {
	playerDeathWatch()
	lap = 0
	listPosition = 0
	roachCount = 0
	walkerCount = 0
	lapStart()
}

//=======================================================
// WAVE AND LAP LOGIC                                    
//=======================================================

//This defines the amount of time between each wave (resting time between bug assaults)
//key = the lap number, value = interval in seconds
waveIntervalMap = [ 1:20, 2:19, 3:18, 4:17, 5:16, 6:15, 7:14, 8:13, 9:12, 10:11, 11:10, 12:9, 13:8, 14:7, 15:6, 16:5, 17:4, 18:3, 19:3, 20:3 ]

def lapStart() {
	//increment the "lap" number (how many times the player has gone through all five waves)
	lap ++ 
	println "**** LAP #${lap} ****"

	
	if( lap <= 20 ) {
		waveInterval = waveIntervalMap.get( lap )
	} else {
		waveInterval = waveIntervalMap.get( 20 )
	}
	println "**** waveInterval = ${ waveInterval } ****"

	wave = 1
	advanceToNextWave()
}	

//this is a routing routine used at the end of each wave, below, to step to the next wave in sequence (during a lap)
def advanceToNextWave() {
	if( wave == 1 ) {
		randomWave()
	}
	if( wave == 2 ) { 
		sidesWave()
	}
	if( wave == 3 ) {
		quadsWave()
	}
	if( wave == 4 ) {
		surroundWave()
	}
	if( wave == 5 ) {
		walkerWave()
	}
}	

//figure out how many waves have occurred in the game so far
def calcWaveCount() {
	if( wave % 5 > 0 ) { waveNum = wave % 5 } else { waveNum = 5 }
	totalWaves = (((lap - 1) * 5) + waveNum).intValue()
}

//=============================================================
// THE HIVE WAVES (methods & variables used by all wave logic) 
//=============================================================

deathRoachLap = 10 //this is the lap at which deathroaches begin to spawn

//the 10 spawn locations at the tunnel mouths
spawnLocationMap = [ 1: [390, 920], 2: [190, 720], 3: [195, 570], 4: [380, 420], 5: [620, 350], 6: [970, 330], 7: [1220, 410], 8: [1360, 540], 9: [1330, 700], 10: [1120, 910] ]

//chose X, Y coordinates from the "location" passed in from the wave logic
def pickSpawnLocation() {
	//figure out a random room and position
	coords = spawnLocationMap.get( location )
	X = coords.get(0).intValue()
	Y = coords.get(1).intValue()
}

//this method used to determine a non-dazed player so a bug hates someone that's awake, whenever possible, for max effect
def findNonDazedPlayer() {
	nonDazedSet.clear()
	myRooms.Hive_1.getActorList().each { if( isPlayer( it ) && !it.isDazed() ) { nonDazedSet << it } }
	if( !nonDazedSet.isEmpty() ) {
		hatedPlayer = random( nonDazedSet )
	} else {
		hatedPlayer = random( playerSet )
	}
}

//logic for selecting what type of roach is spawned when a roach is desired
def spawnRoach() {
	pickSpawnLocation()
	chanceRoachLTSpawn = ( lap - 1 ) * 5 //+ 5% chance per lap after the first that a roachLT will spawn instead of a roach
	roll = random( 100 )
	if( roll > chanceRoachLTSpawn ) {
		roachSpawner.warp( "Hive_1", X, Y )
		roach = roachSpawner.forceSpawnNow()
	} else {
		//once the players have reached "deathRoachLap", there is an ever-increasing chance thereafter that any LT spawned will be a deathroach instead
		deathRoll = random( 100 )
		if( lap >= deathRoachLap && deathRoll <= ( 5 + ( ( lap - deathRoachLap ) * 5 ) ) ) { //5% + 5% per lap after the deathRoachLap has been reached
			//TODO: It would be cool to do an "alert" sound when a DeathRoach is spawned to help players freak out a little
			deathRoachSpawner.warp( "Hive_1", X, Y )
			roach = deathRoachSpawner.forceSpawnNow()
			roach.setDisplayName( "Death to ${hatedPlayer}" )
		} else {
			roachLTSpawner.warp( "Hive_1", X, Y )
			roach = roachLTSpawner.forceSpawnNow()
		}
	}
	roach.addHate( hatedPlayer, 200 )
	roachCount ++ //keeping track of the total number of roaches spawned during the entire game
}

//logic for selecting what type of walker is spawned when a walker is desired
def spawnWalker() {
	pickSpawnLocation()
	chanceWalkerLTSpawn = ( lap - 1 ) * 5 //+ 5% chance per lap after the first that a walkerLT will spawn instead of a walker
	roll = random( 100 )
	if( roll > chanceWalkerLTSpawn ) {
		walkerSpawner.warp( "Hive_1", X, Y )
		walker = walkerSpawner.forceSpawnNow()
	} else {
		walkerLTSpawner.warp( "Hive_1", X, Y )
		walker = walkerLTSpawner.forceSpawnNow()
	}
	walker.addHate( hatedPlayer, 200 )
	walkerCount ++
}

//=======================================================
// THE RANDOM WAVE                                       
//=======================================================

def randomWave() { 
	adjustML()
	calcWaveCount()
	playerSet.each{ if( !it.isOnQuest( 100, 2 ) ) { it.centerPrint( "Random Assault! (wave ${totalWaves})" ) } }
	if( playerCount == 1 ) { numRoaches = 3; numWalkers = 0 }
	else if ( playerCount == 2 ) { numRoaches = 4; numWalkers = 0 }
	else if ( playerCount == 3 ) { numRoaches = 5; numWalkers = 0 }
	else if ( playerCount == 4 ) { numRoaches = 6; numWalkers = 1 }
	else if ( playerCount == 5 ) { numRoaches = 7; numWalkers = 1 }
	else if ( playerCount >= 6 ) { numRoaches = 8; numWalkers = 1 }
	numTotalSpawn = numRoaches + numWalkers

	spawnRandomWave()
}
                                                                  
//NOTE: As the lap number increases, the chance of an LT version being spawned increases also
def spawnRandomWave() {
	//if you haven't spawned all the critters needed for the wave yet, then get to it!
	if( !playerSet.isEmpty() && numTotalSpawn > 0 ) {
		if( numRoaches > 0 ) {
			//find a player to hate
			findNonDazedPlayer()
			//pick a location to spawn a creature
			location = random( 10 )
			spawnRoach()
			numRoaches --
			numTotalSpawn --
		}
		//spawn a Walker (if available) half the interval later
		myManager.schedule( waveInterval * 0.5 ) {
			if( numWalkers > 0 ) {
				findNonDazedPlayer()
				location = random( 10 )
				spawnWalker()
				numWalkers --
				numTotalSpawn --
			}
		}
			myManager.schedule( waveInterval * 0.5 ) { spawnRandomWave() }
			
	//if all the critters are spawned, then watch for them to die
	} else if( numTotalSpawn <= 0 ) {
		watchMonsterCount() 
	}
}

//=======================================================
// THE SIDES WAVE                                        
//=======================================================

def sidesWave() { 
	sideList = [ 1, 2 ]
	adjustML()
	calcWaveCount()
	playerSet.each{ if( !it.isOnQuest( 100, 2 ) ) { it.centerPrint( "From the Flanks! (wave ${totalWaves})" ) } }
	if( playerCount == 1 ) { numRoaches = 2; numWalkers = 0 }
	else if ( playerCount == 2 ) { numRoaches = 3; numWalkers = 0 }
	else if ( playerCount == 3 ) { numRoaches = 3; numWalkers = 1 }
	else if ( playerCount == 4 ) { numRoaches = 4; numWalkers = 1 }
	else if ( playerCount == 5 ) { numRoaches = 4; numWalkers = 2 }
	else if ( playerCount >= 6 ) { numRoaches = 5; numWalkers = 2 }
	numTotalSpawn = numRoaches + numWalkers

	side = random( sideList )
	sideList.remove( sideList.indexOf( side ) )
	spawnSideGroup()
}

def pickSideLocation() {
	//pick a location to spawn a creature
	if( side == 1 ) {
		location = random( 2, 4 )
	} else {
		location = random( 7, 9 )
	}
}

def spawnSideGroup() {
	//if you haven't spawned all the critters needed for the wave yet, then get to it!
	if( !playerSet.isEmpty() && numTotalSpawn > 0 ) {
		if( numRoaches > 0 ) {
			findNonDazedPlayer()
			pickSideLocation()
			spawnRoach()
			numRoaches --
			numTotalSpawn --
		}
		myManager.schedule(0.25) {
			if( numWalkers > 0 ) {
				findNonDazedPlayer()
				pickSideLocation()
				spawnWalker()
				numWalkers --
				numTotalSpawn --
			}
			spawnSideGroup()
		}
			
	//if all the critters are spawned, then watch for them to die
	} else if( numTotalSpawn <= 0 && !sideList.isEmpty() ) {
		myManager.schedule( waveInterval ) {
			adjustML()
			if( playerCount == 1 ) { numRoaches = 2; numWalkers = 0 }
			else if ( playerCount == 2 ) { numRoaches = 3; numWalkers = 0 }
			else if ( playerCount == 3 ) { numRoaches = 3; numWalkers = 1 }
			else if ( playerCount == 4 ) { numRoaches = 4; numWalkers = 1 }
			else if ( playerCount == 5 ) { numRoaches = 4; numWalkers = 2 }
			else if ( playerCount >= 6 ) { numRoaches = 5; numWalkers = 2 }
			numTotalSpawn = numRoaches + numWalkers
			//pick the remaining side in the list
			side = random( sideList ) 
			sideList.remove( sideList.indexOf( side ) )
			spawnSideGroup()
		}
	} else {
		watchMonsterCount()
	}
}

//=======================================================
// THE QUADRANTS WAVE                                    
//=======================================================

def quadsWave() { 
	quadList = [ 1, 2, 3, 4 ]
	adjustML()
	calcWaveCount()
	playerSet.each{ if( !it.isOnQuest( 100, 2 ) ) { it.centerPrint( "Quadrant Attacks! (wave ${totalWaves})" ) } }
	if( playerCount == 1 ) { numRoaches = 2; numWalkers = 0 }
	else if ( playerCount == 2 ) { numRoaches = 2; numWalkers = 0 }
	else if ( playerCount == 3 ) { numRoaches = 3; numWalkers = 0 }
	else if ( playerCount == 4 ) { numRoaches = 3; numWalkers = 1 }
	else if ( playerCount == 5 ) { numRoaches = 4; numWalkers = 1 }
	else if ( playerCount >= 6 ) { numRoaches = 5; numWalkers = 1 }
	numTotalSpawn = numRoaches + numWalkers

	quad = random( quadList )
	quadList.remove( quadList.indexOf( quad ) )
	spawnQuadGroup()
}

def findQuadLocation() {
	if( quad == 1 ) {
		location = random( 2 ) // 1-2
	} else if( quad == 2 ) {
		location = 2 + random( 3 ) // 3-5
	} else if( quad == 3 ) {
		location = 5 + random( 3 ) // 6-8
	} else if( quad == 4 ) {
		location = 8 + random( 2 ) // 9-10
	}
}

def spawnQuadGroup() {
	//if you haven't spawned all the critters needed for the wave yet, then get to it!
	if( !playerSet.isEmpty() && numTotalSpawn > 0 ) {
		if( numRoaches > 0 ) {
			findNonDazedPlayer()
			findQuadLocation()
			spawnRoach()
			numRoaches --
			numTotalSpawn --
		}
		myManager.schedule(0.25) {
			if( numWalkers > 0 ) {
				findNonDazedPlayer()
				findQuadLocation()
				spawnWalker()
				numWalkers --
				numTotalSpawn --
			}
			spawnQuadGroup()
		}
			
	//if all the critters are spawned, then watch for them to die
	} else if( numTotalSpawn <= 0 && !quadList.isEmpty() ) {
		myManager.schedule( waveInterval * 0.5 ) {
			adjustML()
			if( playerCount == 1 ) { numRoaches = 2; numWalkers = 0 }
			else if ( playerCount == 2 ) { numRoaches = 2; numWalkers = 0 }
			else if ( playerCount == 3 ) { numRoaches = 3; numWalkers = 0 }
			else if ( playerCount == 4 ) { numRoaches = 3; numWalkers = 1 }
			else if ( playerCount == 5 ) { numRoaches = 4; numWalkers = 1 }
			else if ( playerCount >= 6 ) { numRoaches = 5; numWalkers = 1 }
			numTotalSpawn = numRoaches + numWalkers
			
			quad = random( quadList )
			quadList.remove( quadList.indexOf( quad ) )
			spawnQuadGroup()
		}
	} else if( numTotalSpawn <= 0 && quadList.isEmpty() ) {
		watchMonsterCount()
	}
}


//=======================================================
// THE SURROUND WAVE                                     
//=======================================================

def surroundWave() { 
	surroundList = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]
	adjustML()
	calcWaveCount()
	playerSet.each{ if( !it.isOnQuest( 100, 2 ) ) { it.centerPrint( "Surrounded! (wave ${totalWaves})" ) } }
	if( playerCount == 1 ) { numRoaches = 4; numWalkers = 0 }
	else if ( playerCount == 2 ) { numRoaches = 5; numWalkers = 0 }
	else if ( playerCount == 3 ) { numRoaches = 6; numWalkers = 0 }
	else if ( playerCount == 4 ) { numRoaches = 7; numWalkers = 1 }
	else if ( playerCount == 5 ) { numRoaches = 8; numWalkers = 1 }
	else if ( playerCount >= 6 ) { numRoaches = 9; numWalkers = 1 }
	numTotalSpawn = numRoaches + numWalkers
	spawnSurroundWave()
}

def spawnSurroundWave() {
	if( !playerSet.isEmpty() && numTotalSpawn > 0 ) {
		if( numRoaches > 0 ) {
			findNonDazedPlayer()
			location = random( surroundList )
			surroundList.remove( surroundList.indexOf( location ) )
			spawnRoach()
			numRoaches --
			numTotalSpawn --
		}
		myManager.schedule(0.25) {
			if( numWalkers > 0 ) {
				findNonDazedPlayer()
				location = random( surroundList )
				surroundList.remove( surroundList.indexOf( location ) )
				spawnWalker()
				numWalkers --
				numTotalSpawn --
			}
			spawnSurroundWave()
		}
	} else if( numTotalSpawn <= 0 ) {
		watchMonsterCount()
	}
}


//=======================================================
// THE WALKER WAVE                                       
//=======================================================

def walkerWave() {
	playerSet.clone().each(){ it.centerPrint( "The ground trembles!!!" ) }
	myManager.schedule(1) { 
		adjustML()
		calcWaveCount()
		playerSet.each{ if( !it.isOnQuest( 100, 2 ) ) { it.centerPrint( "Walker Wave! (wave ${totalWaves})" ) } }
		if( playerCount == 1 ) { numRoaches = 0; numWalkers = 1 }
		else if ( playerCount == 2 ) { numRoaches = 0; numWalkers = 2 }
		else if ( playerCount == 3 ) { numRoaches = 0; numWalkers = 3 }
		else if ( playerCount == 4 ) { numRoaches = 0; numWalkers = 4 }
		else if ( playerCount == 5 ) { numRoaches = 0; numWalkers = 5 }
		else if ( playerCount >= 6 ) { numRoaches = 0; numWalkers = 6 }
		numTotalSpawn = numRoaches + numWalkers

		spawnWalkerWaves()
	}
}

def spawnWalkerWaves() {
	if( numWalkers > 0 ) {
		findNonDazedPlayer()
		location = random( 10 )
		spawnWalker()
		numWalkers --
		numTotalSpawn --
		myManager.schedule( waveInterval ) { spawnWalkerWaves() }
	} else {
		watchMonsterCount()
	}
}

//=======================================================
// MONSTER COUNT LOGIC (used by all waves)               
//=======================================================

breakMessages = [ "The tunnels seem quieter...but it just can't last...can it?", "The skittering from the tunnels dies down...at least for now...", "The tunnels are quiet. The bugs must be regrouping for another attack...", "It's quiet...too quiet...", "The hive is suddenly eerily silent...you get a bad feeling about this." ]
nextWaveMessages = [ "You hear the skittering in the tunnels begin again! They're coming!", "You can hear them everywhere...there's movement all around...", "The clickings...the zapping and chattering... It's madness!", "The Hive is suddenly alive again...and it's all crawling toward you!", "Oh, no. This is going to be bad..." ]

//TODO: Wave-specific break and restart messages

def watchMonsterCount() {
	if( !playerSet.isEmpty() ) {
		currentMonsterCount = roachSpawner.spawnsInUse() + roachLTSpawner.spawnsInUse() + walkerSpawner.spawnsInUse() + walkerLTSpawner.spawnsInUse() + deathRoachSpawner.spawnsInUse()
		if( currentMonsterCount == 0 ) {
			breakMessage = random( breakMessages )
			playerSet.clone().each{ it.centerPrint( breakMessage ) }
			//after monsters are destroyed, delay for the waveInterval period and then move to the next wave (rest break!)
			myManager.schedule( waveInterval ) {
				playerSet.clone().each { 
					it.centerPrint( random( nextWaveMessages ) )
					sound( "tunnelChitter" ).toZone() 
				}
				myManager.schedule(2) {
					if( wave < 5 ) {
						wave ++
						println "****next wave = ${wave}****"
						advanceToNextWave()
					} else {
						checkForRecipes()
					}
				}
			}
		} else {
			myManager.schedule( 2 ) { watchMonsterCount() }
		}
	}
}

recipeList = [ "17786", "17787", "17788", "17790", "17791", "17793", "17794" ]

//Players get a chance to receive a recipe at the end of each lap
def checkForRecipes() {
	playerSet.clone().each { 
		roll = random( 100 )
		if( roll <= 5 ) { 
			recipe = random( recipeList )
			it.grantItem( recipe )
		}
	}
	//start the next lap after all rewards are given
	lapStart() 
}

//-------------------------------------------
//END GAME CONDITION - DEATHWATCH            
//-------------------------------------------
//This function is used to end the game                                                  
//The Caruthers BEYOND THE PORTAL update is only given to those that die within BugWorld.
//If they pull the plug on the connection and don't die, they won't get the update.      
highTotalSpawn = 0
highScoreHolder = null

def playerDeathWatch() {
	numDazed = 0
	if( playerSet.isEmpty() ) return
	playerSet.clone().each { if( it.isDazed() ) { numDazed ++ } }
	//End the game if everyone is dazed.
	if( numDazed >= playerSet.size() ) { 
		totalSpawn = roachCount + walkerCount
		playerSet.clone().each { curPlayer ->
			if( curPlayer.isOnQuest( 100, 2 ) ) {
				//push the update to step 2 of the INTO THE STAR PORTAL quest
				curPlayer.updateQuest( 100, "Agent Caruthers-VQS" ) 
			} else {
				calcWaveCount()
				curPlayer.centerPrint( "You were overwhelmed during wave ${ totalWaves } buried beneath bugs, but unbroken in spirit." )
				myManager.schedule(2) { curPlayer.centerPrint( "There were ${roachCount.intValue()} roaches and ${walkerCount.intValue()} walkers spawned in the Hive." ) }
				myManager.schedule(4) {
					personalBest = curPlayer.getPlayerVar( "newHivePersonalBest" )

					//if this is the first time you've played when not on the quest...
					if( curPlayer.getPlayerVar( "newHivePersonalBest" ) == 0 || curPlayer.getPlayerVar( "newHivePersonalBest" ) == null ) {
						curPlayer.centerPrint( "Your personal best is now set at ${ totalSpawn.intValue() } total spawns. Try getting even farther next time!" )
						curPlayer.setPlayerVar( "newHivePersonalBest", totalSpawn )

					//if you beat your personal high score
					} else if ( totalSpawn > curPlayer.getPlayerVar( "newHivePersonalBest" ) ) {
						curPlayer.centerPrint( "Congratulations! You beat your personal best! Your new record is now ${totalSpawn.intValue()} total spawns!" )
						curPlayer.setPlayerVar( "newHivePersonalBest", totalSpawn )

					//if you TIE your personal high score
					} else if ( totalSpawn == curPlayer.getPlayerVar( "newHivePersonalBest" ) ) {
						curPlayer.centerPrint( "So close! You tied your old record of ${ personalBest.intValue() } spawns! Break it next time!" )

					//if you don't get to the same level as your last high score
					} else if ( totalSpawn < curPlayer.getPlayerVar( "newHivePersonalBest" ) ) {
						curPlayer.centerPrint( "Your current record is ${ personalBest.intValue() } spawns. You can do even better next time!" )
					}

					//minAdjusts is the minimum number of spawns that a player must be at in order to qualify for orb and badge rewards.
					minAdjusts = numAdjusts / 2

					myManager.schedule(2) {
						if( lap < 2 ) { //no orbs if you don't complete five waves (one full lap)
							curPlayer.centerPrint( "Get past the first Walker Wave to start earning orb rewards." )
						} else {
							if( playerRecord.get( curPlayer ) < minAdjusts ) {
								curPlayer.centerPrint( "You must be present and awake for at least half the fight to earn Orbs." )
							} else {
								averagePlayers = totalPlayersPresentAtSpawns / numAdjusts
								orbGrant = ( averagePlayers * lap ).intValue()
								if( orbGrant > 100 ) { orbGrant = 100 }
								if( orbGrant == 1 ) {
									curPlayer.centerPrint( "You get a charge orb as a reward for your progress through the Hive." )
								} else {
									curPlayer.centerPrint( "Your earned ${orbGrant} additional Charge Orbs! Congratulations!" )
								}
								curPlayer.grantQuantityItem( 100257, orbGrant )
							}
						}	
						myManager.schedule(2) {
							if( !curPlayer.isDoneQuest( 238 ) || !curPlayer.isDoneQuest( 239 ) || !curPlayer.isDoneQuest( 240 ) ) {
								if( playerRecord.get( curPlayer ) < minAdjusts ) {
									curPlayer.centerPrint( "You must be present and awake for at least half the fight to earn Hive Badges." )
								} else {															
									if( !curPlayer.isDoneQuest( 238 ) ) {
										if( lap < 2 ) {
											curPlayer.centerPrint( "Complete at least 5 waves to get your next Hive badge." )
										} else {
											curPlayer.updateQuest( 238, "Story-VQS" )
										}
									} else if( !curPlayer.isDoneQuest( 239 ) ) {
										if( lap < 5 ) {
											curPlayer.centerPrint( "Complete at least 20 waves to get your next Hive badge." )
										} else {
											curPlayer.updateQuest( 239, "Story-VQS" )
										}
									} else if( !curPlayer.isDoneQuest( 240 ) ) {
										if( lap < 11 ) {
											curPlayer.centerPrint( "Complete at least 50 waves to get your next Hive badge " )
										} else {
											curPlayer.updateQuest( 240, "Story-VQS" )
										}
									}
								}
							} else {
								curPlayer.centerPrint( "You have earned all three of the Hive badges. Well done!" )
							}
							//Now kick the player out of the Hive
							myManager.schedule(2) { 
								if( isPlayer( curPlayer ) ) {
									curPlayer.centerPrint( "The bugs pile your bodies on the Star Portal and eject you from their world." )
									curPlayer.warp( "Aqueduct_306", 680, 440 )
								}
							}
						}
					}
				}
			}
		}
		myManager.schedule(12) { 
			//Now kick everyone out of the Hive
			myRooms.Hive_1.getActorList().each {
				if( isPlayer( it ) ) {
					it.centerPrint( "The bugs pile your bodies on the Star Portal and eject you from their world." )
					it.warp( "Aqueduct_306", 680, 440 )
				}
			}	
		}
	} else {
		myManager.schedule(1) { playerDeathWatch() }
	}
}


//------------------------------------------
// Worm Switches                            
//------------------------------------------

wormA = makeSwitch( "wormA", myRooms.Hive_1, 130, 430 )
wormB = makeSwitch( "wormB", myRooms.Hive_1, 415, 265 )
wormC = makeSwitch( "wormC", myRooms.Hive_1, 800, 185 )
wormD = makeSwitch( "wormD", myRooms.Hive_1, 1200, 260 )
wormE = makeSwitch( "wormE", myRooms.Hive_1, 1430, 420 )
wormF = makeSwitch( "wormF", myRooms.Hive_1, 1320, 880 )
wormG = makeSwitch( "wormG", myRooms.Hive_1, 760, 960 )
wormH = makeSwitch( "wormH", myRooms.Hive_1, 560, 920 )

wormA.lock()
wormB.lock()
wormC.lock()
wormD.lock()
wormE.lock()
wormF.lock()
wormG.lock()
wormH.lock()

wormA.off()
wormB.off()
wormC.off()
wormD.off()
wormE.off()
wormF.off()
wormG.off()
wormH.off()

wormA.setUsable( false )
wormB.setUsable( false )
wormC.setUsable( false )
wormD.setUsable( false )
wormE.setUsable( false )
wormF.setUsable( false )
wormG.setUsable( false )
wormH.setUsable( false )


//------------------------------------------
// Worm TRIGGERS                            
//------------------------------------------
def wormTriggerA = "wormTriggerA"
myRooms.Hive_1.createTriggerZone( wormTriggerA, 105, 405, 310, 540 )

def wormTriggerB = "wormTriggerB"
myRooms.Hive_1.createTriggerZone( wormTriggerB, 355, 265, 540, 370 )

def wormTriggerC = "wormTriggerC"
myRooms.Hive_1.createTriggerZone( wormTriggerC, 680, 190, 890, 305 )

def wormTriggerD = "wormTriggerD"
myRooms.Hive_1.createTriggerZone( wormTriggerD, 1105, 225, 1260, 400 )

def wormTriggerE = "wormTriggerE"
myRooms.Hive_1.createTriggerZone( wormTriggerE, 1330, 425, 1435, 505 )

def wormTriggerF = "wormTriggerF"
myRooms.Hive_1.createTriggerZone( wormTriggerF, 1200, 770, 1380, 910 )

def wormTriggerG = "wormTriggerG"
myRooms.Hive_1.createTriggerZone( wormTriggerG, 720, 890, 960, 1000 )

def wormTriggerH = "wormTriggerH"
myRooms.Hive_1.createTriggerZone( wormTriggerH, 470, 890, 718, 1000 )

//------------------------------------------
// Worm SPAWNING LOGIC                      
//------------------------------------------

wormThreshold = 50

noTriggerA = false
noTriggerB = false
noTriggerC = false
noTriggerD = false
noTriggerE = false
noTriggerF = false
noTriggerG = false
noTriggerH = false

wormSound = [ "lunge1", "lunge2", "lunge3" ]

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerA) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerA == false) {
			noTriggerA = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormA.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormA.off(); noTriggerA = false }
		}
	}
}

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerB) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerB == false) {
			noTriggerB = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormB.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormB.off(); noTriggerB = false }
		}
	}
}

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerC) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerC == false) {
			noTriggerC = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormC.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormC.off(); noTriggerC = false }
		}
	}
}

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerD) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerD == false) {
			noTriggerD = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormD.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormD.off(); noTriggerD = false }
		}
	}
}

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerE) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerE == false) {
			noTriggerE = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormE.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormE.off(); noTriggerE = false }
		}
	}
}

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerF) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerF == false) {
			noTriggerF = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormF.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormF.off(); noTriggerF = false }
		}
	}
}

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerG) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerG == false) {
			noTriggerG = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormG.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormG.off(); noTriggerG = false }
		}
	}
}

myManager.onTriggerIn(myRooms.Hive_1, wormTriggerH) { event ->
	if( isPlayer( event.actor ) ) {
		roll = random( 1, 100 )
		if( roll <= wormThreshold && noTriggerH == false) {
			noTriggerH = true
			lunge = random( wormSound )
			sound( lunge ).toZone()
			wormH.on()
			event.actor.instantPercentDamage(20)
			myManager.schedule(4) { wormH.off(); noTriggerH = false }
		}
	}
}
