import com.gaiaonline.mmo.battle.script.*;

def Carla = spawnNPC("Carla-VQS", myRooms.BASSKEN_507, 570, 565)
Carla.setDisplayName( "Carla" )

//---------------------------------------------------------
// Default Conversation - North Gate Guard                 
//---------------------------------------------------------

def CarlaGuard = Carla.createConversation("CarlaGuard", true)

def Carla1 = [id:1]
Carla1.npctext = "Hello there! Are you new to Bass'ken Lake? Do you need directions?"
Carla1.options = []
Carla1.options << [text:"Sure! Lay them on me!", result: 3]
Carla1.options << [text:"Nope. I'm good. Thanks!", result: 2]
CarlaGuard.addDialog(Carla1, Carla)

def Carla2 = [id:2]
Carla2.npctext = "Okay then! Good luck out there. Mind the wolves!"
Carla2.result = DONE
CarlaGuard.addDialog(Carla2, Carla)

def Carla3 = [id:3]
Carla3.npctext = "Well, generally, head north to go to the beach, west to go to the Ole Fishing Hole and eventually to the Old Aqueduct, and you used to be able to head east also, toward Aekea, but that's closed right now."
Carla3.options = []
Carla3.options << [text:"Why is the road to Aekea closed?", result: 9]
Carla3.options << [text:"What's interesting here at Bass'ken Lake?", result: 4]
Carla3.options << [text:"Thanks! That's good enough for me!", result: 2]
CarlaGuard.addDialog(Carla3, Carla)

def Carla4 = [id:4]
Carla4.npctext = "This used to be quite the lumber hub back before the Aqueduct closed down. There's a lumber yard, a fishing shack, a grinding mill over by the waterfall, and the big windmill near the beach."
Carla4.options = []
Carla4.options << [text:"A lumber yard and a windmill?", result: 5]
Carla4.options << [text:"A fishing shack?", result: 7]
Carla4.options << [text:"A grinding mill?", result: 8]
Carla4.options << [text:"Did you mention Aekea?", result: 9]
Carla4.options << [text:"Okay, thanks. That'll get me moving.", result: 2]
CarlaGuard.addDialog(Carla4, Carla)

def Carla5 = [id:5]
Carla5.npctext = "Yeah, that and the windmill have been closed for years now. They're still in surprisingly good shape, but...well...Bass'ken Lake has become quite the backwater (pardon the pun) since the Reclamation Facility went on-line and the water was diverted out of here."
Carla5.playertext = "Does anyone live there now?"
Carla5.result = 6
CarlaGuard.addDialog(Carla5, Carla)

def Carla6 = [id:6]
Carla6.npctext = "Nope. We run by there and check once in a while, just to be sure there are no squatters, but all those old splintery boards and rusty blades and nails seem to keep people out of both places just fine."
Carla6.result = 12
CarlaGuard.addDialog(Carla6, Carla)

def Carla7 = [id:7]
Carla7.npctext = "Sure! Old Man Logan runs the only thriving enterprise on the Lake, his old fishing shack. If you're interested in fishing, or just listening to an old cranky adventurer, head on over and jaw with Logan a bit."
Carla7.result = 12
CarlaGuard.addDialog(Carla7, Carla)

def Carla8 = [id:8]
Carla8.npctext = "Well...it's not actually a grinding mill anymore. The Todd family lives there now and their dad, Marshall, has been running his 'Deep Blue Ventures' diving operation out of there for many months now. I don't *think* anyone is home there now, but you can always swing by and check."
Carla8.result = 12
CarlaGuard.addDialog(Carla8, Carla)

def Carla9 = [id:9]
Carla9.npctext = "To be honest, I don't know much about it. If you want to know more, go talk to Rocko, the guard manning the roadblock on the road to Aekea."
Carla9.playertext = "Where's that roadblock?"
Carla9.result = 10
CarlaGuard.addDialog(Carla9, Carla)

def Carla10 = [id:10]
Carla10.npctext = "Just head north on the road and stay on it as it bends east toward Aekea. That'll take you over to Rocko just fine."
Carla10.playertext = "Thanks!"
Carla10.result = 11
CarlaGuard.addDialog(Carla10, Carla)

def Carla11 = [id:11]
Carla11.npctext = "No problem. Are you interested in knowing anything else about the Bass'ken Lake area?"
Carla11.options = []
Carla11.options << [text:"I've got some time to kill. Sure!", result: 12]
Carla11.options << [text:"Nope. I'm good for now. Thanks!", result: 2]
CarlaGuard.addDialog(Carla11, Carla)

def Carla12 = [id:12]
Carla12.npctext = "What else would you like to hear about? The area has an abandoned lumber yard and windmill, as well as a thriving fishing shack and the old grinding wheel down by the waterfall."
Carla12.options = []
Carla12.options << [text:"A lumber mill and a windmill?", result: 5]
Carla12.options << [text:"A fishing shack?", result: 7]
Carla12.options << [text:"A grinding mill?", result: 8]
Carla12.options << [text:"What did you say about Aekea?", result: 9]
Carla12.options << [text:"That'll do for now. Thanks!", result: 2]
CarlaGuard.addDialog(Carla12, Carla)

