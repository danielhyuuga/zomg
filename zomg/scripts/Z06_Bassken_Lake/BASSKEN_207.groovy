import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BASSKEN_207.spawnSpawner("spawner1", "grass_fluff", 2) 
spawner1.setPos(430, 400)
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 5, 400)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 4.4 )


//STARTUP LOGIC
spawner1.spawnAllNow()