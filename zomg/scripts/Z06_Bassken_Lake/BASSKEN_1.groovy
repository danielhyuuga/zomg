import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BASSKEN_1.spawnSpawner("spawner1", "buzz_saw", 2) 
spawner1.setPos(835, 265)
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 5, 300)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 5.0 )

//STARTUP LOGIC
spawner1.spawnAllNow()