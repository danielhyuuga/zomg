import com.gaiaonline.mmo.battle.script.*;

def Rocko = spawnNPC("Rocko-VQS", myRooms.BASSKEN_407, 900, 300)
Rocko.setDisplayName( "Rocko" )

//---------------------------------------------------------
// Default Conversation - North Gate Guard                 
//---------------------------------------------------------


def RockoGuard = Rocko.createConversation("RockoGuard", true)

def Rocko1 = [id:1]
Rocko1.npctext = "Yo! You donwanna go dis way. It jes' ain't safe fer anyone der now. Dem mangy 'bots is tearin' up the place."
Rocko1.options = []
Rocko1.options << [text:"Bots? You mean the robots in Aekea?", result: 2]
Rocko1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 228, { result -> 
 		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 228)
			Rocko.say("Yo! Take dis fer yer freebie.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Rocko today. Try again tomorrow!" )
		}
	})
}, result: DONE]
RockoGuard.addDialog(Rocko1, Rocko)

def Rocko2 = [id:2]
Rocko2.npctext = "Yah. Dat's wot I hear anyway. De bots dat don't t'ink good are comin' alive. De t'inkin' ones is still good and fightin' em wit us. But it's a real mess."
Rocko2.playertext = "Can I go help?"
Rocko2.result = 3
RockoGuard.addDialog(Rocko2, Rocko)

def Rocko3 = [id:3]
Rocko3.npctext = "No can do. Leon don't wan' anyone gettin' into a bigger mess den we's already in. So until Leon sez it's okay, dis road is closed."
Rocko3.playertext = "Fair enough, I suppose."
Rocko3.options = []
Rocko3.options << [text:"Is there anything I can do to help?", result: 4]
Rocko3.options << [text:"Thanks, Rocko. Good luck out here.", result: 5]
RockoGuard.addDialog(Rocko3, Rocko)

def Rocko4 = [id:4]
Rocko4.npctext = "Well...mebbe. Go talk to Carla near Barton's North Gate. She's de ell-tee out here and she knows what's going on dis here area."
Rocko4.playertext = "Thanks, Rocko."
Rocko4.result = DONE
RockoGuard.addDialog(Rocko4, Rocko)

def Rocko5 = [id:5]
Rocko5.npctext = "Youz too."
Rocko5.result = DONE
RockoGuard.addDialog(Rocko5, Rocko)