import com.gaiaonline.mmo.battle.script.*;

def Gustav = spawnNPC("Gustav-VQS", myRooms.BASSKEN_503, 154, 355)
Gustav.setDisplayName( "Gustav" )

onQuestStep( 80, 2 ) { event -> event.player.addMiniMapQuestActorName( "Gustav-VQS" ) }
onQuestStep( 81, 5 ) { event -> event.player.addMiniMapQuestActorName( "Gustav-VQS" ) }
onQuestStep( 83, 2 ) { event -> event.player.addMiniMapQuestActorName( "Gustav-VQS" ) }
onQuestStep( 281, 2 ) { event -> event.player.addMiniMapQuestActorName( "Gustav-VQS" ) }

//This is a fix for player data that has been screwed up for more than a few players...it ensures that players can continue beyond MON CHERI
myManager.onEnter( myRooms.BASSKEN_503 ) { event ->
	if( isPlayer( event.actor ) && event.actor.isDoneQuest( 80 ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z6GiveTheAxeRecipe" )
	}
}

//---------------------------------------------------------
// Default Conversation - Before FINDING GUSTAV is started 
//---------------------------------------------------------


def GustavDefault = Gustav.createConversation("GustavDefault", true, "!QuestStarted_79", "!QuestCompleted_79")

def Gustav1 = [id:1]
Gustav1.npctext = "'allo! To what do I owe zis...pleasure...of seeing you?"
Gustav1.options = []
Gustav1.options << [text:"Nothing much. I was just exploring the Bass'ken Lake area.", result: 2]
Gustav1.options << [text:"You're just extremely lucky, I guess.", result: 4]
GustavDefault.addDialog(Gustav1, Gustav)

def Gustav2 = [id:2]
Gustav2.npctext = "Zen you should keep moving. The nutheeng to see here is big and you have much to see. Go, go!"
Gustav2.playertext = "Friendly sort, aren't you?"
Gustav2.result = 3
GustavDefault.addDialog(Gustav2, Gustav)

def Gustav3 = [id:3]
Gustav3.npctext = "Friendship, she is something that is earned, non? You have earned nutheeng, so you should go."
Gustav3.result = DONE
GustavDefault.addDialog(Gustav3, Gustav)

def Gustav4 = [id:4]
Gustav4.npctext = "oh ho HO! Zee leetle fish has ze bite petit, non?"
Gustav4.playertext = "Nope. This fish ain't so little. I'm kind of a big fish back home, actually."
Gustav4.result = 5
GustavDefault.addDialog(Gustav4, Gustav)

def Gustav5 = [id:5]
Gustav5.npctext = "So, le poisson grand, eh? You should get to ze water then, fishie, before you no live from flapping ze lips in the dry, dry air."
Gustav5.playertext = "Before I go, is there anything you can tell me about this area?"
Gustav5.result = 6
GustavDefault.addDialog(Gustav5, Gustav)

def Gustav6 = [id:6]
Gustav6.npctext = "Non. I speak only to friends. You are not ze bud-dee, so you should go. Now. At once!"
Gustav6.playertext = "Well. Okay then. See ya, surly one."
Gustav6.result = DONE
GustavDefault.addDialog(Gustav6, Gustav)

//---------------------------------------------------------
// FINDING GUSTAV (step 2)                                 
//---------------------------------------------------------

def FromRyan = Gustav.createConversation("FromRyan", true, "QuestStarted_79:2" )

def Intro1 = [id:1]
Intro1.playertext = "Hello there. Are you Gustav?"
Intro1.result = 2
FromRyan.addDialog(Intro1, Gustav)

def Intro2 = [id:2]
Intro2.npctext = "Who are you that wants to know?"
Intro2.playertext = "My name is %p. A Barton Regular by the name of Ryan asked me to watch out for you and let him know if you were all right."
Intro2.result = 3
FromRyan.addDialog(Intro2, Gustav)

def Intro3 = [id:3]
Intro3.npctext = "So...%p...you are friendly person zat helps much. Tell to Ryan that Gustav is bon. And since you are friendly, Gustav has a favor to ask for you."
Intro3.options = []
Intro3.options << [text:"What sort of favor?", result: 6]
Intro3.options << [text:"No. Not right now. I'll come again another time, okay?", result: 4]
FromRyan.addDialog(Intro3, Gustav)

def Intro4 = [id:4]
Intro4.npctext = "So...%p is friendly, but no so friendly as to help Gustav, eh?"
Intro4.options = []
Intro4.options << [text:"No...that's not it at all. Okay, fine. What do you need?", result: 6]
Intro4.options << [text:"Not exactly...I just don't have time right now. See you later.", result: 5]
FromRyan.addDialog(Intro4, Gustav)

def Intro5 = [id:5]
Intro5.npctext = "Au revoir, friendly person that is named %p. You come back and Gustav tells you of ze She Wolf."
Intro5.quest = 79 //the player has met Gustav and can return to Ryan for quest completion now.
Intro5.flag = "Z6GustavBridgeOkay"
Intro5.exec = { event ->
	event.player.addMiniMapQuestActorName( "Ryan-VQS" )
}
Intro5.result = DONE
FromRyan.addDialog(Intro5, Gustav)

def Intro6 = [id:6]
Intro6.npctext = "Bon, mon ami. Ze dog-gies, they no bite, Gustav. Mebee it is because Gustav smells a bit like wet dog already, no? But mebee it is because Gustav no disturb ze doggeez in ze den."
Intro6.playertext = "Den?"
Intro6.quest = 79 //the player has met Gustav and can return to Ryan for quest completion now.
Intro6.flag = "Z6GustavBridgeOkay"
Intro6.exec = { event ->
	event.player.addMiniMapQuestActorName( "Ryan-VQS" )
	event.player.setQuestFlag( GLOBAL, "Z6GustavBridgeOkay" )
	Gustav.pushDialog( event.player, "GustavBridge" )
}
Intro6.result = DONE
FromRyan.addDialog(Intro6, Gustav)


//---------------------------------------------------------
// Gustav Bridging Conversation                            
// (This convo allows resumption and start of the Mon Cheri
// task.)                                                  
//---------------------------------------------------------

def GustavBridge = Gustav.createConversation("GustavBridge", true , "Z6GustavBridgeOkay", "!QuestStarted_80", "!QuestCompleted_80")

def bridge1 = [id:1]
bridge1.npctext = "Ze pups seem to come from ze mother wolf...what Gustav calls ze She Wolf. She lives by ze river in ze cave wit her packs of wolves. You no should go der unless you want holes in your clothes from chew marks."
bridge1.playertext = "The river to the west from here?"
bridge1.result = 2
GustavBridge.addDialog(bridge1, Gustav)

def bridge2 = [id:2]
bridge2.npctext = "Oui. But Gustav is no playing wit you. She no easy fight. You take other friendly persons wit you or it be fast fight!"
bridge2.options = []
bridge2.options << [text:"Heck...how bad can it be? I'm going to go check this 'She Wolf' out!", result: 4]
bridge2.options << [text:"Right. I'll stay away for sure! Thanks, Gustav!", result: 3] 
GustavBridge.addDialog(bridge2, Gustav)

def bridge3 = [id:3]
bridge3.npctext = "Is no problem for you, %p. Au revoir!"
bridge3.result = DONE
GustavBridge.addDialog(bridge3, Gustav)

def bridge4 = [id:4]
bridge4.npctext = "Is sad day, den. Gustav will miss you. But you know, %p, mebee we strike ze deal? Just in case you no die?"
bridge4.options = []
bridge4.options << [text:"Sure, Gustav! What sort of deal?", result:7]
bridge4.options << [text:"Not right now, Gustav. If I survive, maybe we can speak again.", result: 5]
GustavBridge.addDialog(bridge4, Gustav)

def bridge5 = [id:5]
bridge5.npctext = "Bon, bon. You no worry. When ze She Wolf eats you, Gustav will pick up stuff ze dog no want and take to loved ones for %p."
bridge5.playertext = "Ummm...thanks...Gustav..."
bridge5.result = 6
GustavBridge.addDialog(bridge5, Gustav)

def bridge6 = [id:6]
bridge6.npctext = "Is no worry, %p. Enjoy being ze meal!"
bridge6.playertext = "Oh, I will. Never fear. Is there anything you want me to do while I'm in there? Scratch the She Wolf behind her ears? Train her to roll over and play dead?"
bridge6.flag = [ "Z6GustavFriendly", "!Z6GustavBridgeOkay" ]
bridge6.result = DONE
bridge6.exec = { event ->
	Gustav.pushDialog(event.player, "GustavHelpRequest")
}
GustavBridge.addDialog(bridge6, Gustav)

def bridge7 = [id:7]
bridge7.npctext = "I think mebee...but non. Gustav barely knows you."
bridge7.playertext = "This is no time to be shy, Gustav."
bridge7.flag = [ "Z6GustavFriendly", "!Z6GustavBridgeOkay" ]
bridge7.exec = { event ->
	Gustav.pushDialog(event.player, "GustavHelpRequest")
}
bridge7.result = DONE
GustavBridge.addDialog(bridge7, Gustav)

//---------------------------------------------------------
// Start of the MON CHERI quest                            
//---------------------------------------------------------

def GustavHelpRequest = Gustav.createConversation("GustavHelpRequest", true , "Z6GustavFriendly", "!QuestStarted_80", "!QuestCompleted_80")

def Help1 = [id:1]
Help1.npctext = "Gustav no like to ask for help...but mebee there is one thing %p could help with?"
Help1.options = []
Help1.options << [text:"Spit it out, Gustav! How can I help?", result: 3]
Help1.options << [text:"I'll check back with you another time.", result: 2]
GustavHelpRequest.addDialog(Help1, Gustav)

def Help2 = [id:2]
Help2.npctext = "Of course, of course. Friendly to ze guards, but not to Gustav. Gustav has also made that choice before. Goodbye, %p."
Help2.playertext = "No...it's not like that. Look, I'll come back and help if I can later. Good luck to you, Gustav."
Help2.result = DONE
GustavHelpRequest.addDialog(Help2, Gustav)

def Help3 = [id:3]
Help3.npctext = "Ze doggees...Gustav thinks mebee dey steal his axe. If you live, mebee you will look in She Wolf den to see if my axe is there?"
Help3.playertext = "That's it? An axe? There's one there right in front of you!"
Help3.result = 4
GustavHelpRequest.addDialog(Help3, Gustav)

def Help4 = [id:4]
Help4.npctext = "Phaugh! That no axe! Is hatchet only! Good for snapping twigs and hurling at trees. No good for real cutting. Gustav wants his Cheri back in his hand. Gustav no feels complete without ze smooth oak handle and heavy might of Cheri at ze end of his arm. Cheri...she is ze axe of ages...ze axe of champions...ze hatchet of..."
Help4.playertext = "Ummm...okay. Gotcha. Your axe is a really good axe. Okay...I'll take a look for you...assuming I don't 'get eaten'."
Help4.result = 5
GustavHelpRequest.addDialog(Help4, Gustav)

def Help5 = [id:5]
Help5.npctext = "Is all Gustav can ask. Thank you, %p. Gustav will not see you again, but you are friendly person to see if you can find Cheri for Gustav."
Help5.playertext = "See ya soon, Gustav."
Help5.result = 6
GustavHelpRequest.addDialog(Help5, Gustav)

def Help6 = [id:6]
Help6.npctext = "Non. But it was nice meeting you before you turn into ze Doggee Chow. Au revoir!"
Help6.quest = 80 //start the Mon Cheri quest.
Help6.result = DONE
GustavHelpRequest.addDialog(Help6, Gustav)

//---------------------------------------------------------
// Interim Conversation (MON CHERI quest)                  
//---------------------------------------------------------

//Should the above conversation be broken so that the "Mon Cheri" quest can start after speaking to Gustav initially (and completing the "Looking for Gustav" quest? The answer is yes.

def CheriInterim = Gustav.createConversation("CheriInterim", true, "QuestStarted_80", "!QuestStarted_80:4")

def Interim1 = [id:1]
Interim1.npctext = "Mon Cheri? Did you find her?"
Interim1.playertext = "No...not yet, Gustav. But I'm still looking!"
Interim1.result = 2
CheriInterim.addDialog(Interim1, Gustav)

def Interim2 = [id:2]
Interim2.npctext = "Of course, of course. Gustav knows that an old axe is not so important to a big person like %p."
Interim2.playertext = "Yeah. Uhh...I'm going to get it, Gustav. Just be patient."
Interim2.result = 3
CheriInterim.addDialog(Interim2, Gustav)

def Interim3 = [id:3]
Interim3.npctext = "Oui, oui. Gustav knows. He will wait patiently, like always."
Interim3.playertext = "Okay then. See you soon."
Interim3.result = DONE
CheriInterim.addDialog(Interim3, Gustav)


//---------------------------------------------------------
// Success Conversation (MON CHERI quest)                  
//---------------------------------------------------------

def CheriSucc = Gustav.createConversation("CheriSucc", true, "QuestStarted_80:4")

def Succ1 = [id:1]
Succ1.npctext = "What is that? What is THAT YOU CARRY?!?"
Succ1.playertext = "Yup. It's your axe, Gustav!"
Succ1.result = 2
CheriSucc.addDialog(Succ1, Gustav)

def Succ2 = [id:2]
Succ2.npctext = "MON CHERI! You will give it to Gustav now?"
Succ2.playertext = "You bet! Here you go, Gustav!"
Succ2.result = 3
CheriSucc.addDialog(Succ2, Gustav)

def Succ3 = [id:3]
Succ3.npctext = "At last! Gustav's arm is complete again!"
Succ3.playertext = "Well...you wear it well, Gustav. Glad I could help."
Succ3.result = 4
CheriSucc.addDialog(Succ3, Gustav)

def Succ4 = [id:4]
Succ4.npctext = "Non! You know not how happy Gustav is now. You are Gustav's bon ami now and Gustav will do good things for you now."
Succ4.playertext = "Okay...I admit that taking on the She Wolf was challenging, but really, it's not necessary."
Succ4.result = 5
CheriSucc.addDialog(Succ4, Gustav)

def Succ5 = [id:5]
Succ5.npctext = "Gustav teach you to make a real axe, like Cheri. Mebee if you make ze axe like this, then Gustav will show you to make other things too?"
Succ5.options = []
Succ5.options << [text:"You'll show me how to make an axe like this? You bet! Show me how!", result: 7]
Succ5.options << [text:"No really, Gustav. Just helping was enough. Keep your secrets.", result: 6]
CheriSucc.addDialog(Succ5, Gustav)

def Succ6 = [id:6]
Succ6.npctext = "So be it. %p knows what is best for %p. Bon fortune to you, whereever you go. Gustav will no forget what you do zis day. If you change your mind, you come back to Gustav, oui?"
Succ6.playertext = "Oui. Goodbye for now."
Succ6.flag = "Z6GiveTheAxeRecipe"
Succ6.quest = 80 //push the completion of the "Mon Cheri" quest
Succ6.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Gustav-VQS" )
}
Succ6.result = DONE
CheriSucc.addDialog(Succ6, Gustav)

def Succ7 = [id:7]
Succ7.npctext = "Hmmmm..."
Succ7.quest = 80 //push the completion of the "Mon Cheri" quest
Succ7.flag = "Z6GiveTheAxeRecipe"
Succ7.exec = { event ->
	Gustav.pushDialog(event.player, "CheriSister")
	event.player.removeMiniMapQuestActorName( "Gustav-VQS" )
}
Succ7.result = DONE
CheriSucc.addDialog(Succ7, Gustav)

//---------------------------------------------------------
// Start the CHERI'S SISTER quest                          
//---------------------------------------------------------

def CheriSister = Gustav.createConversation("CheriSister", true, "Z6GiveTheAxeRecipe", "!QuestStarted_81", "!QuestCompleted_81" )

def Sister1 = [id:1]
Sister1.npctext = "So you want Gustav to show you new ways to make things, no?"
Sister1.options = []
Sister1.options << [text: "Yes!", result: 2]
Sister1.options << [text: "Not right now. Thanks.", result: 7]
Sister1.result = 2
CheriSister.addDialog(Sister1, Gustav)

def Sister2 = [id:2]
Sister2.npctext = "Bon. Gustav writes the way on this paper. Read ze directions and do what it says. Soon, Cheri will have a sister!"
Sister2.playertext = "So this is a big list of items. I should go gather these items and then follow directions to make the axe?"
Sister2.result = 3
CheriSister.addDialog(Sister2, Gustav)

def Sister3 = [id:3]
Sister3.npctext = "Non! %p no make an 'axe'...<spits>...%p makes wickedness zat is art, beauty zat is deadly. Not a mere...'axe'."
Sister3.playertext = "Right, right. Sorry about that. I didn't mean to insult your Cheri."
Sister3.result = 4
CheriSister.addDialog(Sister3, Gustav)

def Sister4 = [id:4]
Sister4.npctext = "Skah. No insult was heard. Only Gustav wants %p to appreciate ze tool."
Sister4.playertext = "Gotcha"
Sister4.result = 5
CheriSister.addDialog(Sister4, Gustav)

def Sister5 = [id:5]
Sister5.npctext = "%p do well wit Cheri sister and come back to Gustav. Gustav will show %p ways to make something else. Something...special."
Sister5.playertext = "That sounds intriguing, Gustav. I'll see what I can do with the...with Cheri's sister, and get back as soon as I can."
Sister5.result = 6
CheriSister.addDialog(Sister5, Gustav)

def Sister6 = [id:6]
Sister6.npctext = "Bon, bon. Au revoir, %p! Keep in head zat ze journey is as good as ze goal. %p will like what Gustav will show later. Yes...very much, Gustav thinks."
Sister6.playertext = "Okay. Bye for now!"
Sister6.quest = 81 //start the CHERI'S SISTER quest
Sister6.result = DONE
CheriSister.addDialog(Sister6, Gustav)

def Sister7 = [id:7]
Sister7.npctext = "Okay, %p. We see you another time then."
Sister7.result = DONE
CheriSister.addDialog(Sister7, Gustav)

//---------------------------------------------------------
// Interim for CHERI'S SISTER quest                        
//---------------------------------------------------------

def SisterInterim = Gustav.createConversation("SisterInterim", true, "QuestStarted_81", "!QuestStarted_81:6")

def sisInt1 = [id:1]
sisInt1.npctext = "How does %p do with finding ze elements needed for ze Sister?"
sisInt1.playertext = "Well...I've still got a ways to go, Gustav. Soon though, I hope."
sisInt1.result = 2
SisterInterim.addDialog(sisInt1, Gustav)

def sisInt2 = [id:2]
sisInt2.npctext = "Is okay though, non? Ze journey to ze goal, she keeps you happy, oui?"
sisInt2.playertext = "Yeah, I'm okay about it, Gustav. Thanks for asking."
sisInt2.result = 3
SisterInterim.addDialog(sisInt2, Gustav)

def sisInt3 = [id:3]
sisInt3.npctext = "Is no problem. Bon chance, %p!"
sisInt3.result = DONE
SisterInterim.addDialog(sisInt3, Gustav)


//---------------------------------------------------------
// Success for CHERI'S SISTER quest                        
//---------------------------------------------------------

def SisterSucc = Gustav.createConversation("SisterSucc", true, "QuestStarted_81:6", "!QuestStarted_83")

def sisSucc1 = [id:1]
sisSucc1.npctext = "%p! Gustav is glad to see you! And you have ze Sister already!"
sisSucc1.playertext = "Okay...I have to admit it. This is a mighty fine tool, Gustav. Thank you for showing me how to make it!"
sisSucc1.result = 2
SisterSucc.addDialog( sisSucc1, Gustav )

def sisSucc2 = [id:2]
sisSucc2.npctext = "Gustav very, VERY happy to hear zis, %p. It makes Gustav ready to show plans for making ze special item to go with ze Sister."
sisSucc2.options = []
sisSucc2.options << [text:"What would go well with an axe? Errr...with the Sister?", result: DONE, exec: { event -> event.player.setQuestFlag( GLOBAL, "Z06OkayForPapaSawn" ); event.player.updateQuest( 81, "Gustav-VQS" ); event.player.removeMiniMapQuestActorName( "Gustav-VQS" ); Gustav.pushDialog( event.player, "papaStart" ) } ]
sisSucc2.options << [text:"Thanks, Gustav, but I'm going to take a breather now. Maybe later.", result: 3]
SisterSucc.addDialog( sisSucc2, Gustav )

def sisSucc3 = [id:3]
sisSucc3.npctext = "Okay. %p knows what is best for %p. Come back another time and Gustav show how to make something verra nice."
sisSucc3.flag = "Z06OkayForPapaSawn"
sisSucc3.quest = 81 //complete the CHERI'S SISTER quest (one of two possible endings...see the exec in sisSucc2 (above) for the other)
sisSucc3.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Gustav-VQS" )
}
sisSucc3.result = DONE
SisterSucc.addDialog( sisSucc3, Gustav )


//---------------------------------------------------------
// Start for the PAPA SAWN! quest                          
//---------------------------------------------------------

def papaStart = Gustav.createConversation("papaStart", true, "Z06OkayForPapaSawn", "!QuestStarted_83", "!QuestCompleted_83")

def papa1 = [id:1]
papa1.npctext = "Gustav now shows you to make ze SHIELD! And Gustav knows what kind of thing would make a good shield now, no?"
papa1.playertext = "Well...I'd have to guess that 'oui', Gustav *does* know."
papa1.result = 2
papaStart.addDialog(papa1, Gustav)

def papa2 = [id:2]
papa2.npctext = "Gustav knows! Ze old lumber yard...ze Animated has taken it over."
papa2.playertext = "What sort of Animated?"
papa2.result = 3
papaStart.addDialog(papa2, Gustav)

def papa3 = [id:3]
papa3.npctext = "Ze old rusty buzz saw blades are now...'breeding' is ze word?"
papa3.playertext = "I'm not sure about 'breeding'. Maybe we should just say 'multiplying' instead? At least until we know more about the Animated."
papa3.result = 4
papaStart.addDialog(papa3, Gustav)

def papa4 = [id:4]
papa4.npctext = "Oui. More and more of zem. I call it 'breeding' because zer seems to be a mama and papa of ze buzz saws near ze pier there."
papa4.playertext = "A...mama saw and a papa saw? Does Gustav make ze little joke?"
papa4.result = 5
papaStart.addDialog(papa4, Gustav)

def papa5 = [id:5]
papa5.npctext = "Joke? No joke. Ze papa saw is different then ze others. Stronger. Sturdier. Gustav is thinking, it would make GOOD shield!"
papa5.playertext = "So you're giving me directions on how to construct a buzz saw shield?"
papa5.result = 6
papaStart.addDialog(papa5, Gustav)

def papa6 = [id:6]
papa6.npctext = "Oui! Gustav has no made one before, but ze recipe...*should* work. Would you like ze instructions?"
papa6.options = []
papa6.options << [text:"Yes! Thank you, Gustav!", result: 8]
papa6.options << [text:"No...I've got more than enough to do right now.", result: 7]
papaStart.addDialog(papa6, Gustav)

def papa7 = [id:7]
papa7.npctext = "Is %p sure? Gustav will no give zis to you later. One time offer!"
papa7.options = []
papa7.options << [text:"Okay...I'll take it!", result: 8]
papa7.options << [text:"I'm certain. You should keep your secrets, Gustav.", result: 9]
papaStart.addDialog(papa7, Gustav)

def papa8 = [id:8]
papa8.npctext = "Bon! %p comes back to show Gustav ze shield when it is done, non? Zat would make Gustav very pleased."
papa8.playertext = "Sure. I can do that. Thanks very much, Gustav!"
papa8.quest = 83 //Start the PAPA SAWN! quest
papa8.result = DONE
papaStart.addDialog(papa8, Gustav)

def papa9 = [id:9]
papa9.npctext = "<shrug> Okay. %p knows what is best for %p. Gustav bids you adieu and bon chance!"
papa9.playertext = "Thanks again, Gustav!"
papa9.result = DONE
papaStart.addDialog(papa9, Gustav)

//---------------------------------------------------------
// Interim for PAPA SAWN! quest                            
//---------------------------------------------------------

def SawInterim = Gustav.createConversation("SawInterim", true, "QuestStarted_83", "!QuestStarted_83:3" )

def sawInt1 = [id:1]
sawInt1.npctext = "How is %p doing to gather ze aluminum?"
sawInt1.playertext = "%p could be doing a lot better, I'm sorry to say. But I'll get them eventually, Gustav!"
sawInt1.result = 2
SawInterim.addDialog(sawInt1, Gustav)

def sawInt2 = [id:2]
sawInt2.npctext = "Oui, oui. Gustav has faith!"
sawInt2.playertext = "Thanks. See you!"
sawInt2.result = DONE
SawInterim.addDialog(sawInt2, Gustav)

//---------------------------------------------------------
// Success for PAPA SAWN! quest                            
//---------------------------------------------------------

def SawSuccess = Gustav.createConversation("SawSuccess", true, "QuestStarted_83:3")

def sawSucc1 = [id:1]
sawSucc1.npctext = "Soooo?"
sawSucc1.playertext = "Yup. I defeated the Papa Saw!"
sawSucc1.result = 2
SawSuccess.addDialog(sawSucc1, Gustav)

def sawSucc2 = [id:2]
sawSucc2.npctext = "Voila! It will be a thing of beauty! %p has done Gustav very proud!"
sawSucc2.playertext = "Well...thanks, Gustav! I really appreciate that. Is there anything else you can show me how to build?"
sawSucc2.result = 3
SawSuccess.addDialog(sawSucc2, Gustav)

def sawSucc3 = [id:3]
sawSucc3.npctext = "hahaHA. Ze %p has ze bug to build, eh? But non...Gustav has nothing else to give you now. Mebee you should wear ze scar you get in ze battle proudly, non?"
sawSucc3.playertext = "We'll see. Thanks again, Gustav. This was fun."
sawSucc3.result = 4
SawSuccess.addDialog(sawSucc3, Gustav)

def sawSucc4 = [id:4]
sawSucc4.npctext = "Be well, %p. Take care of ze Sister and Shield and live joyously!"
sawSucc4.quest = 83 //push the completion of the PAPA SAWN! quest.
sawSucc4.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Gustav-VQS" )
}
sawSucc4.result = DONE
SawSuccess.addDialog(sawSucc4, Gustav)

//---------------------------------------------------------
// Post-Quest Default Convo                                
//---------------------------------------------------------

def postQuestDefault = Gustav.createConversation("postQuestDefault", true, "QuestCompleted_83", "!Z6JesseKnown", "!Z6JesseNotKnown" )

def postQuest1 = [id:1]
postQuest1.npctext = "'allo, %p! How you do?"
postQuest1.playertext = "Great, Gustav! How have things been for you?"
postQuest1.result = 2
postQuestDefault.addDialog(postQuest1, Gustav)

def postQuest2 = [id:2]
postQuest2.npctext = "Gustav no complain. Ze doggeez are a tiny bit annoying with all the yapping and ze pooping, but otherwise, Gustav is good."
postQuest2.playertext = "That's great. Is there anything else I can do for you now?"
postQuest2.result = 3
postQuestDefault.addDialog(postQuest2, Gustav)

def postQuest3 = [id:3]
postQuest3.npctext = "Non. Well...mebee you may go and check on a friend for Gustav, no?"
postQuest3.playertext = "Sure, Gustav! What friend would that be?"
postQuest3.result = 4
postQuestDefault.addDialog( postQuest3, Gustav )

def postQuest4 = [id:4]
postQuest4.npctext = "Mebee you cross ze river and check on ze Todd boy downstream a bit?"
postQuest4.exec = { event ->
	if( event.player.isDoneQuest( 70 ) ) {
		event.player.setQuestFlag( GLOBAL, "Z6JesseKnown" )
		Gustav.pushDialog( event.player, "knowJesse" )
	} else {
		event.player.setQuestFlag( GLOBAL, "Z6JesseNotKnown" )
		Gustav.pushDialog( event.player, "notKnowJesse" )
	}
}
postQuestDefault.addDialog( postQuest4, Gustav )


def knowJesse = Gustav.createConversation( "knowJesse", true, "Z6JesseKnown", "QuestCompleted_83", "!Z6HeadedToJesse" )

know1 = [id:1]
know1.playertext = "I know the boy you're talking about."
know1.options = []
know1.options << [text: "I'd be happy to go down and talk to Jesse for you.", result: 2]
know1.options << [text: "I'm afraid I don't have time right now, Gustav.", result: 3]
knowJesse.addDialog( know1, Gustav) 

know2 = [id:2]
know2.npctext = "Merci! Zat house is ze fortress, so ze boy should be fine, but it is good to check once in a while, non?"
know2.playertext = "No problem. See you around, Gustav!"
know2.flag = "Z6HeadedToJesse"
know2.result = DONE
knowJesse.addDialog( know2, Gustav )

know3 = [id:3]
know3.npctext = "Is okay. Gustav will go soon and check on ze boy. Good luck to you anyways, mon ami."
know3.playertext = "And to you, my friend!"
know3.flag = "!Z6JesseKnown"
know3.result = DONE
knowJesse.addDialog( know3, Gustav )


def notKnowJesse = Gustav.createConversation( "notKnowJesse", true, "Z6JesseNotKnown", "QuestCompleted_83", "!Z6HeadedToJesse" )

notKnow1 = [id:1]
notKnow1.playertext = "What boy do you mean, Gustav?"
notKnow1.result = 2
notKnowJesse.addDialog( notKnow1, Gustav )

notKnow2 = [id:2]
notKnow2.npctext = "Ze Todd family lives in ze waterwheel house downstream, across ze river. Gustav knows ze papa of ze family is no home, and that means ze boy is there alone."
notKnow2.playertext = "Do you think he's in danger?"
notKnow2.result = 3
notKnowJesse.addDialog( notKnow2, Gustav )

notKnow3 = [id:3]
notKnow3.npctext = "Non, non. Ze house is very strong and ze boy is not so stupid to go out wit all this going on...but one never knows. Mebee you could check on he sometime soon?"
notKnow3.options = []
notKnow3.options << [text: "Sure, Gustav. I'll go by and check sometime soon.", result: 4]
notKnow3.options << [text: "I can't do that right now, Gustav. Sorry.", result: 5]
notKnowJesse.addDialog( notKnow3, Gustav )

notKnow4 = [id:4]
notKnow4.npctext = "Bon! It will be good to know ze boy is still in one piece and not chew toy for puppies."
notKnow4.playertext = "heh. Okay. Talk to you soon then!"
notKnow4.flag = "Z6HeadedToJesse"
notKnow4.result = DONE
notKnowJesse.addDialog( notKnow4, Gustav )

notKnow5 = [id:5]
notKnow5.npctext = "Okay, %p. Gustav understands. Mebee Gustav will stroll der soon instead."
notKnow5.playertext = "All right then! See you soon!"
notKnow5.flag = "!Z6JesseNotKnown"
notKnow5.result = DONE
notKnowJesse.addDialog( notKnow5, Gustav )

//---------------------------------------------------------
// Final Aftermath Dialog Loop                             
//---------------------------------------------------------

def aftermathGustav = Gustav.createConversation( "aftermathGustav", true, "Z6HeadedToJesse", "!QuestStarted_281", "!Z5_Gustav_Punsh_Allowed", "!Z5_Gustav_Punish_Denied" )

def afterMath1 = [id:1]
afterMath1.npctext = "'allo, %p! How you do?"
afterMath1.playertext = "I'm doing well, Gustav, thanks! How about you?"
afterMath1.result = 2
aftermathGustav.addDialog( afterMath1, Gustav )

def afterMath2 = [id:2]
afterMath2.npctext = "I am, how you say, furious. Ze wolves, ze mischievous things keep taking Mon Cheri!"
afterMath2.playertext = "What are you gonna do about it?"
afterMath2.result = 3
aftermathGustav.addDialog( afterMath2, Gustav )

def afterMath3 = [id:3]
afterMath3.npctext = "I must stop zem! You must help, you will help me take revenge?"
afterMath3.options = []
afterMath3.options << [text:"I will!", exec: { event ->
	if(event.player.getConLevel() < 5.1) {
		event.player.setQuestFlag(GLOBAL, "Z5_Gustav_Punish_Allowed")
		Gustav.pushDialog(event.player, "punishAllowed")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z5_Gustav_Punish_Denied")
		Gustav.pushDialog(event.player, "punishDenied")
	}
}, result: DONE]
afterMath3.options << [text:"It's your battle, you're on your own, Gustav.", result: 4]
aftermathGustav.addDialog( afterMath3, Gustav )

def afterMath4 = [id:4]
afterMath4.npctext = "Oui, I will avenge Mon Cheri!"
afterMath4.result = DONE
aftermathGustav.addDialog(afterMath4, Gustav)

//---------------------------------------------------------
// ALLOWED - Punish Ze Wolves                              
//---------------------------------------------------------
def punishAllowed = Gustav.createConversation("punishAllowed", true, "Z5_Gustav_Punish_Allowed")

def punishAllowed1 = [id:1]
punishAllowed1.npctext = "Ahh yes, ze friend %p is always helping. Punish ze wolves, twenty or so should send ze message."
punishAllowed1.quest = 281
punishAllowed1.flag = "!Z5_Gustav_Punish_Allowed"
punishAllowed1.result = DONE
punishAllowed.addDialog(punishAllowed1, Gustav)

//---------------------------------------------------------
// DENIED - Punish Ze Wolves                               
//---------------------------------------------------------
def punishDenied = Gustav.createConversation("punishDenied", true ,"Z5_Gustav_Punish_Denied")

def punishDenied1 = [id:1]
punishDenied1.npctext = "Gustav is thinking, perhaps %p is a little to strong to be wasting time with ze wolves."
punishDenied1.flag = "!Z5_Gustav_Punish_Denied"
punishDenied1.exec = { event ->
	event.player.centerPrint( "You must be level 5.0 or lower for this task." ) 
	event.player.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
}
punishDenied1.result = DONE
punishDenied.addDialog(punishDenied1, Gustav)

//---------------------------------------------------------
// ACTIVE - Punish Ze Wolves                               
//---------------------------------------------------------
def punishActive = Gustav.createConversation("punishActive", true, "QuestStarted_281:2")

def punishActive1 = [id:1]
punishActive1.npctext = "You shall punish ze wolves soon?"
punishActive1.playertext = "As soon as I can."
punishActive1.result = DONE
punishActive.addDialog(punishActive1, Gustav)

//---------------------------------------------------------
// COMPLETE - Punish Ze Wolves                             
//---------------------------------------------------------
def punishComplete = Gustav.createConversation("punishComplete", true, "QuestStarted_281:3")

def punishComplete1 = [id:1]
punishComplete1.npctext = "Ze wolves have learned a lesson today, no?"
punishComplete1.playertext = "Yup, I taught them a thing or two."
punishComplete1.result = 2
punishComplete.addDialog(punishComplete1, Gustav)

def punishComplete2 = [id:2]
punishComplete2.npctext = "Zey still eye Mon Cheri, though. Perhaps ze lesson has not fully sunk in? Perhaps ze wolves need more instruction."
punishComplete2.options = []
punishComplete2.options << [text:"You are probably right. Class is in session, time for some beatings!", exec: { event ->
	event.player.updateQuest(281, "Gustav-VQS")
	event.player.removeMiniMapQuestActorName( "Gustav-VQS" )
	if(event.player.getConLevel() < 5.1) {
		event.player.setQuestFlag(GLOBAL, "Z5_Gustav_Punish_AllowedAgain")
		Gustav.pushDialog(event.player, "punishAllowedAgain")
	} else {
		event.player.setQuestFlag(GLOBAL, "Z5_Gustav_Punish_Denied")
		Gustav.pushDialog(event.player, "punishDenied")
	}
}, result: DONE] 
punishComplete2.options << [text:"Maybe, but I don't have time for it right now.", result: 4]
punishComplete.addDialog(punishComplete2, Gustav)

def punishComplete4 = [id:4]
punishComplete4.npctext = "Oui, I will avenge Mon Cheri!"
punishComplete4.quest = 281
punishComplete4.result = DONE
punishComplete.addDialog(punishComplete4, Gustav)

//---------------------------------------------------------
// ALLOWED AGAIN - Punish Ze Wolves                        
//---------------------------------------------------------
def punishAllowedAgain = Gustav.createConversation("punishAllowedAgain", true, "Z5_Gustav_Punish_AllowedAgain")

def punishAllowedAgain1 = [id:1]
punishAllowedAgain1.npctext = "Teach them to leave Mon Cheri alone."
punishAllowedAgain1.exec = { event ->
	event.player.updateQuest(281, "Gustav-VQS")
}
punishAllowedAgain1.flag = "!Z5_Gustav_Punish_AllowedAgain"
punishAllowedAgain1.result = DONE
punishAllowedAgain.addDialog(punishAllowedAgain1, Gustav)