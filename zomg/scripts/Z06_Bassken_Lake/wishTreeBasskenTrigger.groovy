import com.gaiaonline.mmo.battle.script.*;

//===============================
//WISH TREE VISION TRIGGER ZONE  
//===============================

onQuestStep(51, 2) { event -> event.player.addMiniMapQuestActorName( "Wish Tree-VQS" ) }

//=========THE WINDMILL==========
def wishTreeWindmillTrigger = "wishTreeWindmillTrigger"
myRooms.BASSKEN_206.createTriggerZone(wishTreeWindmillTrigger, 50, 50, 305, 205)

myManager.onTriggerIn(myRooms.BASSKEN_206, wishTreeWindmillTrigger) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(51, 2) && !event.actor.hasQuestFlag( GLOBAL, "Z06FoundWishTreeBasskenClue") ) {
		event.actor.say("I found the Wish Tree vision area...the Windmill at Bass'ken Lake.")
		event.actor.updateQuest(51, "Story2-VQS" ) //push completion for this leg of the Vision Quest
		event.actor.setQuestFlag( GLOBAL, "Z06FoundWishTreeBasskenClue" )
	}
}