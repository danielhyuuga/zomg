import com.gaiaonline.mmo.battle.script.*;

//===============================
// ENTERING BARTON FROM BASSKEN  
// (Avoiding the Total Charge    
// check when coming from this   
// side. )                       
//===============================

def enteringBartonTownFromBassken = "enteringBartonTownFromBassken"
myRooms.BASSKEN_607.createTriggerZone( enteringBartonTownFromBassken, 420, 490, 1000, 660 )

myManager.onTriggerIn( myRooms.BASSKEN_607, enteringBartonTownFromBassken ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z01ComingFromBassken" )
	}
}