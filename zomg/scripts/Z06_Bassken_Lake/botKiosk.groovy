/*

import com.gaiaonline.mmo.battle.script.*;

myManager.onEnter( myRooms.BASSKEN_304 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
	}
}

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("kioskPosition", "markerOther", "BASSKEN_304", 920, 430, "HyperNet Access")

kiosk = makeSwitch( "botKiosk", myRooms.BASSKEN_304, 920, 430 )
kiosk.unlock()
kiosk.off()
kiosk.setRange( 200 )
kiosk.setMouseoverText("Access the HyperNet")

def clickKiosk = { event ->
	kiosk.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk.whenOn( clickKiosk )


//====================================================
// TUTORIAL TREE                                      
//====================================================

kioskWidth = 300

def giveReward( event ) {
	//gold reward
	goldGrant = random( 50, 150 )
	event.player.centerPrint( "You receive ${goldGrant} gold!" )
	event.player.grantCoins( goldGrant )

	//orb reward
	orbGrant = 2
	event.player.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
	event.player.grantQuantityItem( 100257, orbGrant )
}
	
def synchronized makeKioskMenu( player ) {
	titleString = "The HyperNet"
	descripString = "Choose Game Help to access loads of information about the game, or one of the other categories for easy downloads of useful tidbits."
	diffOptions = ["Game Help", "Tutorials", "Area Info", "Exit HyperNet"]
	
	uiButtonMenu( player, "kioskMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Game Help" ) {
			showWidget( event.actor, "GameHelp", true, true )
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXGameHelpSelected" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXGameHelpSelected" )
				//gold reward
				goldGrant = random( 50, 150 )
				event.actor.centerPrint( "You receive ${goldGrant} gold!" )
				event.actor.grantCoins( goldGrant )

				//orb reward
				orbGrant = 2
				event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				event.actor.grantQuantityItem( 100257, orbGrant )
			}
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
		if( event.selection == "Tutorials" ) {
			makeTutorialMenu( player )
		}
		if( event.selection == "Area Info" ) {
			makeAreaMenu( player )
		}
		if( event.selection == "Exit HyperNet" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
	}
}

def makeAreaMenu( player ) {
	titleString = "Area Info"
	descripString = "A small archive of stories and tidbits of the area around you."
	diffOptions = ["Bass'ken Lake", "Old Man Logan", "Main Menu"]
	
	uiButtonMenu( player, "fictionMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Bass'ken Lake" ) {
			tutorialNPC.pushDialog( event.actor, "basskenLake" )
		}
		if( event.selection == "Old Man Logan" ) {
			tutorialNPC.pushDialog( event.actor, "oldManLogan" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}
	


def makeTutorialMenu( player ) {
	titleString = "Tutorials"
	descripString = "These tutorials give you quick overviews on various features in the game."
	diffOptions = ["Total Charge", "Crew Advantages", "Suppressing CL", "Ring Sets", "Main Menu"]
	
	uiButtonMenu( player, "tutorialMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Total Charge" ) {
			tutorialNPC.pushDialog( event.actor, "totalCharge" )
		}
		if( event.selection == "Crew Advantages" ) {
			tutorialNPC.pushDialog( event.actor, "crewAdv" )
		}
		if( event.selection == "Suppressing CL" ) {
			tutorialNPC.pushDialog( event.actor, "suppressCL" )
		}
		if( event.selection == "Ring Sets" ) {
			tutorialNPC.pushDialog( event.actor, "ringSets" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// TUTORIAL MENU                                                                        
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//ADVANCED COMBAT TUTORIAL                                                              
//--------------------------------------------------------------------------------------
totalCharge = tutorialNPC.createConversation( "totalCharge", true )

def tc1 = [id:1]
tc1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>What is Total Charge?</font></b></h1><br><font face='Arial' size ='12'>Total Charge is simply a counter that keeps track of every charge orb that you've ever gained while playing zOMG!.<br><br>It doesn't do anything directly for you, but it's a clear indicator of 'veteran status' when comparing yourself against other players.<br><br>Since players can suppress their CL, or choose to wear low-CL rings instead of their highest-CL ones, Total Charge is a more reliable indicator of the 'experience' of other players.<br><br>]]></zOMG>"
tc1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BASSKEN_304 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXTotalCharge" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXTotalCharge" )
			giveReward( event )
		}
	}
}
tc1.result = DONE
totalCharge.addDialog( tc1, tutorialNPC )

//--------------------------------------------------------------------------------------
//CREW ADVANTAGES TUTORIAL                                                              
//--------------------------------------------------------------------------------------
crewAdv = tutorialNPC.createConversation( "crewAdv", true )

def crew1 = [id:1]
crew1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Advantages to Crewing</font></b></h1><br><font face='Arial' size ='12'>Crews are your friends.<br><br>First of all, you do *not* split loot with your Crewmates. So more loot is created for each monster defeated so that your personal gain is the same as if you were solo. However...<br><br>1) You can defeat more monsters while Crewed, so you'll end up with a lot more loot that way than if you solo, and;<br><br>2) Gold rewards *are* multiplied up, so you will gain more gold while Crewed than you will while soloing.<br><br>Additionally, when you solo, you can't use more than eight rings. But when Crewed, your Crewmates can use their rings on *you* and you can be a lot tougher than when you solo.<br><br>]]></zOMG>"
crew1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BASSKEN_304 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXCrewAdv" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXCrewAdv" )
			giveReward( event )
		}
	}
}
crew1.result = DONE
crewAdv.addDialog( crew1, tutorialNPC )

//--------------------------------------------------------------------------------------
//SUPPRESS CL TUTORIAL                                                                  
//--------------------------------------------------------------------------------------
suppressCL = tutorialNPC.createConversation( "suppressCL", true )

def suppress1 = [id:1]
suppress1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Suppressing Your CL</font></b></h1><br><font face='Arial' size ='12'>Once you've built your overall Charge Level up a bit, you'll find that certain events and hunting areas are off-limits to you because you're too powerful.<br><br>Never fear! You can still play all that content!<br><br>Click on the 'Options' button on your Control Bar and then choose the 'SUPPRESS CL' option.<br><br>Then you can easily drag your overall CL down to the level you want to play at and join your friends for fun!<br><br>To reset your CL back up to max, just head to the Null Chamber and choose 'SUPPRESS CL' again. You can't raise your CL level while you're 'in the field', but you can reset it easily while in the Null Chamber.<br><br>]]></zOMG>"
suppress1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BASSKEN_304 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXSuppressCL" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXSuppressCL" )
			giveReward( event )
		}
	}
}
suppress1.result = DONE
suppressCL.addDialog( suppress1, tutorialNPC )

//--------------------------------------------------------------------------------------
//RING SET TUTORIAL                                                                     
//--------------------------------------------------------------------------------------
ringSets = tutorialNPC.createConversation( "ringSets", true )

def set1 = [id:1]
set1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Ring Sets</font></b></h1><br><font face='Arial' size ='12'>So you've got a bunch of rings now and you think you've got the hang of it, right?<br><br>But wait! There's more!<br><br>If you wear your Rings in certain combinations, there are bonuses you can unlock for even more power!<br><br>Each Ring Set is composed of four unique rings. If all four of those rings are worn on a single hand (slots 1-4 or 5-8), then you get the bonus automatically.<br><br>The various Ring Sets are fully details in GAME HELP on the 'Ring Sets' page, so check it out!<br><br>Ring Sets are completely optional. Use them if they fit your play style!<br><br>]]></zOMG>"
set1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BASSKEN_304 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXRingSets" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXRingSets" )
			giveReward( event )
		}
	}
}
set1.result = DONE
ringSets.addDialog( set1, tutorialNPC )


//======================================================================================
//======================================================================================
// AREA MENU                                                                            
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//BASS'KEN LAKE BACKGROUND                                                              
//--------------------------------------------------------------------------------------
def basskenLake = tutorialNPC.createConversation( "basskenLake", true )

def lake1 = [id:1]
lake1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Bass'ken Lake</font></b></h1><br><font face='Arial' size ='12'>The Bass'ken Lake area was once a thriving lumber area, but that all dried up when the Old Aqueduct was shut down in favor of the Durem Reclamation Facility water treatment system.<br><br>Now, the few buildings left in the area are either abandoned, or utilized by the 'Deep Blue Ventures' exploration company as their base of operations.<br><br>]]></zOMG>"
lake1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BASSKEN_304 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXBasskenLake" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXBasskenLake" )
			giveReward( event )
		}
	}
}
lake1.result = DONE
basskenLake.addDialog( lake1, tutorialNPC )

//--------------------------------------------------------------------------------------
//OLD MAN LOGAN BACKGROUND                                                              
//--------------------------------------------------------------------------------------
def oldManLogan = tutorialNPC.createConversation("oldManLogan", true)

def oldManLogan1 = [id:1]
oldManLogan1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Old Man Logan</font></b></h1><br><font face='Arial' size ='12'>Before becoming owner of 'The Hole,' Old Man Logan worked alongside Johnny Gambino and Edmund as a member of G-Team. <br><br>With Gambino as the ambition, Edmund as the brains, and Logan as the muscle G-Team seemed an unstoppable partnership. Eventually, differences between the three led to Logan's departure from the group and its subsequent disbanding. <br><br>No one knows for certain what caused the rift, but many have speculated that Logan became disgusted with Gambino's insatiable thirst for power and immortality - a thirst some believe led to the experiments which caused the Grunny disaster.<br><br>]]></zOMG>"
oldManLogan1.exec = { event ->
	if( event.actor.getRoom() == myRooms.BASSKEN_304 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXOldManLogan" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXOldManLogan" )
			giveReward( event )
		}
	}
}
oldManLogan1.result = DONE
oldManLogan.addDialog(oldManLogan1, tutorialNPC)

*/
