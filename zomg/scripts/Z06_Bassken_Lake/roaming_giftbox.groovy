import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// This giftbox only spawns once every one  
// hour. It roams on its patrol path until  
// destroyed. Once destroyed, the timer     
// begins and the giftbox spawns again when 
// the timer expires.                       
// THIS MONSTER *ALWAYS* DROPS A RING!      
//------------------------------------------

roamSpawner = myRooms.BASSKEN_407.spawnSpawner( "roamSpawner", "flying_giftbox_special", 1 )
roamSpawner.setPos( 270, 220 )
roamSpawner.setMonsterLevelForChildren( 4.5 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_407", 270, 230, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_406", 780, 210, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_506", 760, 330, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_505", 600, 370, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_605", 450, 225, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_604", 200, 330, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_504", 490, 390, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_505", 230, 220, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_405", 470, 550, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_406", 285, 525, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_506", 300, 450, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_606", 300, 270, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_606", 670, 265, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_506", 720, 430, 0 )
roamSpawner.addPatrolPointForChildren( "BASSKEN_507", 620, 325, 0 )

ringList = [ "17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866" ]

def boxDeathWatch() {
	
	runOnDeath(ringBox, { event ->
		//initialize the variable, if necessary
		if( event.killer.getPlayerVar( "Z06LastTimeRingBoxKilled" ) == null ) {
			event.killer.setPlayerVar( "Z06LastTimeRingBoxKilled", 0 ) 
		}
		
		//Don't allow the player to get loot if the player has killed a Ring Box less than (ringBoxSpawnTimer + 2 hours) ago.
		if( event.killer.getPlayerVar( "Z06LastTimeRingBoxKilled" ).longValue() < ringBoxSpawnTimer ) { 
			//Reset the playerVar so the ring grant can't occur again for two hours
			event.killer.setPlayerVar( "Z06LastTimeRingBoxKilled", System.currentTimeMillis() + 7200000 ) //7200000 milliseconds = 2 hours
			//grant a random ring
			event.killer.grantRing( random( ringList ), true )
		}
		myManager.schedule(7200) { //7200 seconds = 2 hours
			ringBox = roamSpawner.forceSpawnNow()
			ringBoxSpawnTimer = System.currentTimeMillis()
			boxDeathWatch()
		}
	} )

}

//==========================
//INITIAL LOGIC STARTS HERE 
//==========================

ringBox = roamSpawner.forceSpawnNow()
ringBoxSpawnTimer = System.currentTimeMillis()

roamSpawner.stopSpawning()

boxDeathWatch()