import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BASSKEN_2.spawnSpawner("spawner1", "buzz_saw", 2) 
spawner1.setPos(210, 400)
spawner1.setWanderBehaviorForChildren( 50, 150, 3, 9, 300)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 4.9 )

def spawner2 = myRooms.BASSKEN_2.spawnSpawner("spawner2", "buzz_saw", 2) 
spawner2.setPos(890, 320)
spawner2.setWanderBehaviorForChildren( 50, 150, 3, 9, 300)
spawner2.setWaitTime( 50, 70 )
spawner2.setMonsterLevelForChildren( 4.9 )


//STARTUP LOGIC
spawner1.spawnAllNow()
spawner2.spawnAllNow()