import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BASSKEN_4.spawnSpawner("spawner1", "buzz_saw", 2) 
spawner1.setPos(280, 450)
spawner1.setWanderBehaviorForChildren( 50, 150, 3, 9, 300)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 4.8 )

def spawner2 = myRooms.BASSKEN_4.spawnSpawner("spawner2", "buzz_saw", 2) 
spawner2.setPos(585, 240)
spawner2.setWanderBehaviorForChildren( 50, 150, 3, 9, 300)
spawner2.setWaitTime( 50, 70 )
spawner2.setMonsterLevelForChildren( 4.8 )


//STARTUP LOGIC
spawner1.spawnAllNow()
spawner2.spawnAllNow()