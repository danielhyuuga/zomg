import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// The Den Packs (Room 502)                 
//------------------------------------------

// This group allies with itself.
wolfPackOneLT = myRooms.BASSKEN_502.spawnSpawner( "wolfPackOneLT", "outlaw_pup_LT", 1)
wolfPackOneLT.setPos( 290, 440 )
wolfPackOneLT.setWaitTime( 90, 180 )
wolfPackOneLT.setHomeTetherForChildren( 4000 )
wolfPackOneLT.setHateRadiusForChildren( 2000 )
wolfPackOneLT.setWanderBehaviorForChildren( 50, 125, 4, 9, 250)
wolfPackOneLT.setMonsterLevelForChildren( 4.4 )

wolfPackOneA = myRooms.BASSKEN_502.spawnSpawner( "wolfPackOneA", "outlaw_pup", 1)
wolfPackOneA.setPos( 290, 440 )
wolfPackOneA.setWaitTime( 90, 180 )
wolfPackOneA.setHomeTetherForChildren( 4000 )
wolfPackOneA.setHateRadiusForChildren( 2000 )
wolfPackOneA.setWanderBehaviorForChildren( 50, 125, 4, 9, 250)
wolfPackOneA.setMonsterLevelForChildren( 4.4 )

wolfPackOneB = myRooms.BASSKEN_502.spawnSpawner( "wolfPackOneB", "outlaw_pup", 1)
wolfPackOneB.setPos( 290, 440 )
wolfPackOneB.setWaitTime( 90, 180 )
wolfPackOneB.setHomeTetherForChildren( 4000 )
wolfPackOneB.setHateRadiusForChildren( 2000 )
wolfPackOneB.setWanderBehaviorForChildren( 50, 125, 4, 9, 250)
wolfPackOneB.setMonsterLevelForChildren( 4.4 )

// This group allies with itself.
wolfPackTwoLT = myRooms.BASSKEN_502.spawnSpawner( "wolfPackTwoLT", "outlaw_pup_LT", 1)
wolfPackTwoLT.setPos( 965, 140 )
wolfPackTwoLT.setWaitTime( 90, 180 )
wolfPackTwoLT.setHomeTetherForChildren( 4000 )
wolfPackTwoLT.setHateRadiusForChildren( 2000 )
wolfPackTwoLT.setWanderBehaviorForChildren( 50, 125, 4, 9, 250)
wolfPackTwoLT.setMonsterLevelForChildren( 4.4 )

wolfPackTwoA = myRooms.BASSKEN_502.spawnSpawner( "wolfPackTwoA", "outlaw_pup", 1)
wolfPackTwoA.setPos( 970, 145 )
wolfPackTwoA.setWaitTime( 90, 180 )
wolfPackTwoA.setHomeTetherForChildren( 4000 )
wolfPackTwoA.setHateRadiusForChildren( 2000 )
wolfPackTwoA.setWanderBehaviorForChildren( 50, 125, 4, 9, 250)
wolfPackTwoA.setMonsterLevelForChildren( 4.4 )

wolfPackTwoB = myRooms.BASSKEN_502.spawnSpawner( "wolfPackTwoB", "outlaw_pup", 1)
wolfPackTwoB.setPos( 965, 145 )
wolfPackTwoB.setWaitTime( 90, 180 )
wolfPackTwoB.setHomeTetherForChildren( 4000 )
wolfPackTwoB.setHateRadiusForChildren( 2000 )
wolfPackTwoB.setWanderBehaviorForChildren( 50, 125, 4, 9, 250)
wolfPackTwoB.setMonsterLevelForChildren( 4.4 )

//------------------------------------------
// Wolf Pickets (Room 503, 603 & 501)       
//------------------------------------------

def wolfPicketOne = myRooms.BASSKEN_502.spawnSpawner( "wolfPicketOne", "outlaw_pup", 1 )
wolfPicketOne.setPos( 500, 150 )
wolfPicketOne.setWaitTime( 50, 70 )
wolfPicketOne.setHomeTetherForChildren( 4000 )
wolfPicketOne.setHome( wolfPackTwoA )
wolfPicketOne.setGuardPostForChildren( "BASSKEN_503", 655, 590, 0 )
wolfPicketOne.setRFH( true )
wolfPicketOne.setCFH( 800 )
wolfPicketOne.setDispositionForChildren( "coward" )
wolfPicketOne.setCowardLevelForChildren( 100 )
wolfPicketOne.setMonsterLevelForChildren( 4.3 )

def wolfPicketTwo = myRooms.BASSKEN_502.spawnSpawner( "wolfPicketTwo", "outlaw_pup", 1 )
wolfPicketTwo.setPos( 500, 150 )
wolfPicketTwo.setWaitTime( 50, 70 )
wolfPicketTwo.setHomeTetherForChildren( 4000 )
wolfPicketTwo.setHome( wolfPackOneA )
wolfPicketTwo.setGuardPostForChildren( "BASSKEN_602", 845, 315, 90 )
wolfPicketTwo.setRFH( true )
wolfPicketTwo.setCFH( 800 )
wolfPicketTwo.setDispositionForChildren( "coward" )
wolfPicketTwo.setCowardLevelForChildren( 100 )
wolfPicketTwo.setMonsterLevelForChildren( 4.3 )

def wolfPicketThree = myRooms.BASSKEN_502.spawnSpawner( "wolfPicketThree", "outlaw_pup", 1 )
wolfPicketThree.setPos( 500, 150 )
wolfPicketThree.setWaitTime( 50, 70 )
wolfPicketThree.setHomeTetherForChildren( 4000 )
wolfPicketThree.setHome( wolfPackOneLT )
wolfPicketThree.setGuardPostForChildren( "BASSKEN_501", 745, 410, 90 )
wolfPicketThree.setRFH( true )
wolfPicketThree.setCFH( 800 )
wolfPicketThree.setDispositionForChildren( "coward" )
wolfPicketThree.setCowardLevelForChildren( 100 )
wolfPicketThree.setMonsterLevelForChildren( 4.3 )

//------------------------------------------
// Wolf Pack Patrols                        
//------------------------------------------
// Global defs for these dens so they can have scripted behavior.
// No timers on them because they only spawn by script.

def longPatrol = myRooms.BASSKEN_602.spawnSpawner( "longPatrol", "outlaw_pup", 3 )
longPatrol.setPos( 850, 105 )
longPatrol.setWaitTime( 20, 40 )
longPatrol.setHomeTetherForChildren( 3000 )
longPatrol.setBaseSpeed( 160 )
longPatrol.setChildrenToFollow( longPatrol )
longPatrol.setMonsterLevelForChildren( 4.2 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_602", 850, 110, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_602", 820, 320, 3 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_603", 560, 430, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_604", 75, 400, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_604", 450, 200, 3 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_605", 505, 280, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_605", 650, 170, 3 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_505", 565, 320, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_505", 520, 240, 3 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_504", 750, 270, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_504", 380, 475, 3 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_503", 955, 540, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_503", 760, 415, 3 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_503", 550, 370, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_503", 329, 472, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_502", 964, 536, 3 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_502", 762, 396, 0 )
longPatrol.addPatrolPointForSpawner( "BASSKEN_502", 710, 540, 3 )
longPatrol.spawnAllNow();
longPatrol.startPatrol();

//------------------------------------------
// ALLIANCES                                
//------------------------------------------

wolfPackOneLT.allyWithSpawner( wolfPackOneA )
wolfPackOneLT.allyWithSpawner( wolfPackOneB )
wolfPackOneA.allyWithSpawner( wolfPackOneB )

wolfPackTwoLT.allyWithSpawner( wolfPackTwoA )
wolfPackTwoLT.allyWithSpawner( wolfPackTwoB )
wolfPackTwoA.allyWithSpawner( wolfPackTwoB )

wolfPicketOne.allyWithSpawner( wolfPackTwoA )
wolfPicketOne.allyWithSpawner( wolfPackTwoB )
wolfPicketTwo.allyWithSpawner( wolfPackOneA )
wolfPicketTwo.allyWithSpawner( wolfPackOneB )
wolfPicketThree.allyWithSpawner( wolfPackOneLT )

//==========================
//INITIAL SPAWN LOGIC       
//==========================
wolfPackOneA.forceSpawnNow()
wolfPackOneB.forceSpawnNow()
wolfPackOneLT.forceSpawnNow()
wolfPackTwoA.forceSpawnNow()
wolfPackTwoB.forceSpawnNow()
wolfPackTwoLT.forceSpawnNow()

wolfPicketOne.forceSpawnNow()
wolfPicketTwo.forceSpawnNow()
wolfPicketThree.forceSpawnNow()

longPatrol.spawnAllNow()


//------------------------------------------
// CAVE ENTRANCE SWITCH LOGIC               
//------------------------------------------

timerMap = [:]
onClickCaveEntrance = new Object()

caveEntrance = makeSwitch( "Cave_In", myRooms.BASSKEN_502, 510, 170 )
caveEntrance.unlock()
caveEntrance.off()
caveEntrance.setRange( 300 )
caveEntrance.setMouseoverText("The Wolf Den")

def enterCave = { event ->
	synchronized( onClickCaveEntrance ) {
		caveEntrance.off()
		if( isPlayer( event.actor ) && wolfPackOneLT.spawnsInUse() + wolfPackOneA.spawnsInUse() + wolfPackOneB.spawnsInUse() + wolfPackTwoLT.spawnsInUse() + wolfPackTwoA.spawnsInUse() + wolfPackTwoB.spawnsInUse() == 0 ) {
			event.actor.getCrew().each() { 
				if( it.isOnQuest( 80 ) || it.isDoneQuest( 80 ) ) {
					event.actor.setQuestFlag( GLOBAL, "Z06TempSheWolfEntranceAllowed" )
				}
			}
			if( event.actor.hasQuestFlag( GLOBAL, "Z06TempSheWolfEntranceAllowed" ) ) {
				event.actor.unsetQuestFlag( GLOBAL, "Z06TempSheWolfEntranceAllowed" )
				//********************* START DIFFICULTY SETTINGS ****************************
				if( event.actor.getTeam().hasAreaVar( "Z27_Wolf_Den", "Z27WolfDenDifficulty" ) == false ) {
					if( event.actor.getCrewVar( "Z27WolfDenDifficultySelectionInProgress" ) == 0 ) {
						//Prevent multiple players from initiating the process of selecting difficulties
						event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 1 )

						//check to see if the Crew leader is in the zone
						if( isInZone( event.actor.getTeam().getLeader() ) ) {
							leader = event.actor.getTeam().getLeader()
							makeMenu()

							//Everyone else gets the following message while the leader chooses a setting. 
							event.actor.getCrew().each{
								if( it != event.actor.getTeam().getLeader() ) {
									it.centerPrint( "Your Crew Leader is choosing the challenge level for this encounter." )
								}
							}
							
							//If no choice is made after 10 seconds, then everyone gets a message that says:
							decisionTimer = myManager.schedule(30) {
								event.actor.getCrew().each{
									it.centerPrint( "No choice was made within 30 seconds. Click the cave entrance to try again." )
								}
								event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 0 )
								//if no selection was made, then remove the leader and timer from the timerMap
								timerMap.remove( event.actor.getTeam().getLeader() )
							}
							
							//add the leader and timer to a map so the timer can be canceled later, if necessary
							timerMap.put( event.actor.getTeam().getLeader(), decisionTimer )

						//If the leader is not in the zone then tell the crew the leader must be present
						} else {
							event.actor.getCrew().each{
								if( it != event.actor.getTeam().getLeader() ) {
									it.centerPrint( "Your Crew Leader must be in Bass'ken Lake before you can enter the Wolf Den." )
								} else {
									it.centerPrint( "Your Crew is trying to enter the Wolf Den. You must be in Bass'ken Lake for that to occur." )
								}
							}
							event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 0 )
						}
					} else if( event.actor.getCrewVar( "Z27WolfDenDifficultySelectionInProgress" ) == 1 ) {
						if( event.actor != event.actor.getTeam().getLeader() ) {
							event.actor.centerPrint( "Your Crew Leader is already making a challenge level choice. One moment, please..." )
						}
					}

				//If the Difficulty is not zero, then it has been set and the player can enter the Shrine
				} else if( event.actor.getTeam().hasAreaVar( "Z27_Wolf_Den", "Z27WolfDenDifficulty" ) == true ) { 					
					event.actor.centerPrint( "You edge carefully into the dark, murky depths of the cave." )
					event.actor.warp( "WolfDen_1", 1350, 760 )	
				} 
				//********************* END DIFFICULTY SETTINGS ****************************
			} else {
				event.actor.centerPrint( "You may not enter yet. Speak with Gustav or Crew with someone that has previously entered the Wolf Den." )
			}
		} else {
			event.actor.centerPrint( "You must first defeat the Outlaw Pup defenders before you can enter the Wolf Den." )
		}
	}
}

caveEntrance.whenOn( enterCave )

def synchronized makeMenu() {
	descripString = "You must choose a Challenge Level to enter the Wolf Den. You have 30 seconds to decide."
	diffOptions = ["Easy (small rewards)", "Normal (moderate rewards)", "Hard (large rewards)", "Cancel"]
	
	println "**** trying to create the menu ****"
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy (small rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click the cave again to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click the cave again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z27_Wolf_Den", "Z27WolfDenDifficulty", 1 )
			event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal (moderate rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click the cave again to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click the cave again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z27_Wolf_Den", "Z27WolfDenDifficulty", 2 )
			event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard (large rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click the cave again to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click the cave again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z27_Wolf_Den", "Z27WolfDenDifficulty", 3 )
			event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}

