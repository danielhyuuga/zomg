//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def dockUpdater1 = "dockUpdater1"
myRooms.BASSKEN_205.createTriggerZone(dockUpdater1, 480, 20, 910, 220)
myManager.onTriggerIn(myRooms.BASSKEN_205, dockUpdater1) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(226) && !event.actor.hasQuestFlag(GLOBAL, "Z6_Dock1_Explored")) {
		event.actor.setQuestFlag(GLOBAL, "Z6_Dock1_Explored")
	}
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(226) && event.actor.hasQuestFlag(GLOBAL, "Z6_Dock2_Explored")) {
		event.actor.updateQuest(226, "Story-VQS")
	}
}

def dockUpdater2 = "dockUpdater2"
myRooms.BASSKEN_204.createTriggerZone(dockUpdater2, 10, 10, 69, 240)
myManager.onTriggerIn(myRooms.BASSKEN_204, dockUpdater2) { event ->
	//println "#### ${event.actor} entered trigger zone ####"
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(226) && !event.actor.hasQuestFlag(GLOBAL, "Z6_Dock2_Explored")) {
		event.actor.setQuestFlag(GLOBAL, "Z6_Dock2_Explored")
	}
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(226) && event.actor.hasQuestFlag(GLOBAL, "Z6_Dock1_Explored")) {
		//println "### Updating ${event.actor} on quest 226 ####"
		event.actor.updateQuest(226, "Story-VQS")
	}
}