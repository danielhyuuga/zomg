import com.gaiaonline.mmo.battle.script.*;

//the fluffs on the end of the mud point
def spawner1 = myRooms.BASSKEN_203.spawnSpawner("spawner1", "grass_fluff", 4) 
spawner1.setPos(535, 570)
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 5, 200)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 4.7 )


//STARTUP LOGIC
spawner1.spawnAllNow()

