import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// Central Saw Blade Spawner (sawHome)      
// (This is the reserve pool of saws to     
// respond to any threats reported by the   
// guard posts.)                            
//------------------------------------------

sawHome = myRooms.BASSKEN_102.spawnSpawner( "sawHome", "buzz_saw", 3)
sawHome.setPos( 910, 600 )
sawHome.setWaitTime( 50, 70 )
sawHome.setHomeTetherForChildren( 2000 )
sawHome.setHateRadiusForChildren( 1500 )
sawHome.setInitialMoveForChildren( "BASSKEN_102", 365, 435 )
sawHome.setHome( "BASSKEN_102", 365, 435 )
sawHome.setWanderBehaviorForChildren( 75, 200, 4, 9, 300)
sawHome.setMonsterLevelForChildren( 4.9 )

sawHomeSister = myRooms.BASSKEN_102.spawnSpawner( "sawHomeSister", "buzz_saw_LT", 1 )
sawHomeSister.setPos( 910, 600 )
sawHomeSister.setWaitTime( 1, 2 )
sawHomeSister.setHomeTetherForChildren( 2000 )
sawHomeSister.setHateRadiusForChildren( 1500 )
sawHomeSister.setInitialMoveForChildren( "BASSKEN_102", 365, 435 )
sawHomeSister.setHome( "BASSKEN_102", 365, 435 )
sawHomeSister.setWanderBehaviorForChildren( 75, 200, 4, 9, 300)
sawHomeSister.setMonsterLevelForChildren( 4.9 )

playerSet102 = [] as Set

myManager.onEnter( myRooms.BASSKEN_102 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet102 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_102 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet102.remove( event.actor )
	}
}

sawSister = null
sawHomeSister.stopSpawning()
sawHome.allyWithSpawner( sawHomeSister )
checkForSister()

//The sister randomly spawns. She's fairly rare, but often enough to help break up "door camping" at the lumber yard.
def checkForSister() {
	if( sawHomeSister.spawnsInUse() == 0 ) {
		roll = random( 1, 100 )
		if( roll <= 33 ) {
			if( sawSister == null || sawSister.isDead() ) {
				//spawn the Sister
				sawSister = sawHomeSister.forceSpawnNow()
				sawSister.setDisplayName( "Sister Saw" )
				//if the room is empty, make her hate someone and say something
				if( !playerSet102.isEmpty() ) {
					sawSister.addHate( random( playerSet102 ), 1 )
					sawSister.say( "You dullards git away from mah doorway!" ) 
				}
				//when she's down to 25% health, make her cry out for Papa
				myManager.onHealth( sawSister ) { event ->
					if( event.didTransition( 25 ) && event.isDecrease() ) {
						sawSister.say( "That cuts me deeply! Y'all are mighty rude guests!" )
					}
				}
			}
		}
	} else {
		myManager.schedule( random( 300, 600 ) ) { checkForSister() }
	}
}


/*
//TODO: Set up a trigger zone and logic so that the sawHome reserves are not present unless summoned by road or bridge guards.
def reinforcementsTrigger = "reinforcementsTrigger"
myRooms.BASSKEN_102.createTriggerZone( reinforcementsTrigger, 680, 545, 1020, 660 )

myManager.onTriggerIn(myRooms.BASSKEN_102, reinforcementsTrigger ) { event ->
	if( isMonster(event.actor) && event.actor.getMonsterType() == "buzz_saw" ) {
		sawHome.forceSpawnNow()
		myManager.schedule(1) { sawHome.forceSpawnNow() }
		myManager.schedule(2) { sawHome.forceSpawnNow() }
	}
}
*/

//------------------------------------------
// ROAD GUARDS (Guarding against incursion  
// from the Old Aqueduct.)                  
//------------------------------------------

//One Guard fights, while the other runs to Room 102 to alert the camp there.
def roadGuardOne = myRooms.BASSKEN_102.spawnSpawner( "roadGuardOne", "buzz_saw", 1 )
roadGuardOne.setPos( 910, 600 )
roadGuardOne.setHomeTetherForChildren( 2000 )
roadGuardOne.setHateRadiusForChildren( 2000 )
roadGuardOne.setWaitTime( 30, 50 )
roadGuardOne.setHome( sawHome )
roadGuardOne.setGuardPostForChildren( "BASSKEN_101", 640, 565, 270 )
roadGuardOne.setRFH( true )
roadGuardOne.setCFH( 400 )
roadGuardOne.setDispositionForChildren( "coward" )
roadGuardOne.setCowardLevelForChildren( 100 )
roadGuardOne.setMonsterLevelForChildren( 4.9 )

def roadGuardTwo = myRooms.BASSKEN_102.spawnSpawner( "roadGuardTwo", "buzz_saw", 1 )
roadGuardTwo.setPos( 910, 600 )
roadGuardTwo.setWaitTime( 30, 50 )
roadGuardTwo.setGuardPostForChildren( "BASSKEN_101", 870, 370, 225)
roadGuardTwo.setMonsterLevelForChildren( 4.9 )

//------------------------------------------
// SIDE-TO-SIDE GUARDS (Room 202)           
//------------------------------------------

def patrolOne = myRooms.BASSKEN_202.spawnSpawner( "patrolOne", "buzz_saw", 1 )
patrolOne.setPos( 90, 100 )
patrolOne.setWaitTime( 50, 70 )
patrolOne.setMonsterLevelForChildren( 4.9 )
patrolOne.addPatrolPointForChildren( "BASSKEN_202", 310, 120, 0 )
patrolOne.addPatrolPointForChildren( "BASSKEN_202", 850, 120, 0 )

def patrolTwo = myRooms.BASSKEN_202.spawnSpawner( "patrolTwo", "buzz_saw", 1 )
patrolTwo.setPos( 960, 230 )
patrolTwo.setWaitTime( 50, 70 )
patrolTwo.setMonsterLevelForChildren( 4.9 )
patrolTwo.addPatrolPointForChildren( "BASSKEN_202", 670, 240, 0 )
patrolTwo.addPatrolPointForChildren( "BASSKEN_202", 60, 240, 0 )

def patrolThree = myRooms.BASSKEN_202.spawnSpawner( "patrolThree", "buzz_saw", 1 )
patrolThree.setPos( 100, 360 )
patrolThree.setWaitTime( 50, 70 )
patrolThree.setMonsterLevelForChildren( 4.9 )
patrolThree.addPatrolPointForChildren( "BASSKEN_202", 460, 360, 0 )
patrolThree.addPatrolPointForChildren( "BASSKEN_202", 970, 360, 0 )

def patrolFour = myRooms.BASSKEN_202.spawnSpawner( "patrolFour", "buzz_saw", 1 )
patrolFour.setPos( 90, 450 )
patrolFour.setWaitTime( 50, 70 )
patrolFour.setMonsterLevelForChildren( 4.9 )
patrolFour.addPatrolPointForChildren( "BASSKEN_202", 550, 480, 0 )
patrolFour.addPatrolPointForChildren( "BASSKEN_202", 60, 480, 0 )

//------------------------------------------
// UP AND DOWN GUARDS (Room 3)              
//------------------------------------------

def upAndDown1 = myRooms.BASSKEN_3.spawnSpawner( "upAndDown1", "buzz_saw", 1 )
upAndDown1.setPos( 380, 225 )
upAndDown1.setWaitTime( 50, 70 )
upAndDown1.setMonsterLevelForChildren( 4.9 )
upAndDown1.addPatrolPointForChildren( "BASSKEN_3", 225, 560, 0 )
upAndDown1.addPatrolPointForChildren( "BASSKEN_3", 225, 170, 0 )

def upAndDown2 = myRooms.BASSKEN_3.spawnSpawner( "upAndDown2", "buzz_saw", 1 )
upAndDown2.setPos( 380, 225 )
upAndDown2.setWaitTime( 50, 70 )
upAndDown2.setMonsterLevelForChildren( 4.9 )
upAndDown2.addPatrolPointForChildren( "BASSKEN_3", 500, 170, 0 )
upAndDown2.addPatrolPointForChildren( "BASSKEN_3", 470, 500, 0 )

def upAndDown3 = myRooms.BASSKEN_3.spawnSpawner( "upAndDown3", "buzz_saw", 1 )
upAndDown3.setPos( 380, 225 )
upAndDown3.setWaitTime( 50, 70 )
upAndDown3.setMonsterLevelForChildren( 4.9 )
upAndDown3.addPatrolPointForChildren( "BASSKEN_3", 710, 470, 0 )
upAndDown3.addPatrolPointForChildren( "BASSKEN_3", 710, 200, 0 )

def upAndDown4 = myRooms.BASSKEN_3.spawnSpawner( "upAndDown4", "buzz_saw", 1 )
upAndDown4.setPos( 380, 225 )
upAndDown4.setWaitTime( 50, 70 )
upAndDown4.setMonsterLevelForChildren( 4.9 )
upAndDown4.addPatrolPointForChildren( "BASSKEN_3", 830, 310, 0 )
upAndDown4.addPatrolPointForChildren( "BASSKEN_3", 830, 630, 0 )

//------------------------------------------
// COAST PATROLLERS (Room 104)              
//------------------------------------------

def coast1 = myRooms.BASSKEN_104.spawnSpawner( "coast1", "buzz_saw", 1 )
coast1.setPos( 600, 130 )
coast1.setWaitTime( 50, 70 )
coast1.setMonsterLevelForChildren( 4.8 )
coast1.addPatrolPointForChildren( "BASSKEN_104", 800, 160, 0 )
coast1.addPatrolPointForChildren( "BASSKEN_104", 375, 120, 0 )

def coast2 = myRooms.BASSKEN_104.spawnSpawner( "coast2", "buzz_saw", 1 )
coast2.setPos( 600, 130 )
coast2.setWaitTime( 50, 70 )
coast2.setMonsterLevelForChildren( 4.8 )
coast2.addPatrolPointForChildren( "BASSKEN_104", 130, 180, 0 )
coast2.addPatrolPointForChildren( "BASSKEN_104", 660, 375, 0 )
coast2.addPatrolPointForChildren( "BASSKEN_104", 850, 310, 0 )
coast2.addPatrolPointForChildren( "BASSKEN_104", 660, 375, 0 )

//------------------------------------------
// LAKESIDE OF LUMBER YARD (Room 103)       
//------------------------------------------

sideSaws = myRooms.BASSKEN_103.spawnSpawner( "sideSaws", "buzz_saw", 4 )
sideSaws.setPos( 200, 375 )
sideSaws.setCFH( 500 )
sideSaws.setWaitTime( 50, 70 )
sideSaws.setInitialMoveForChildren( "BASSKEN_103", 550, 200 )
sideSaws.setHome( "BASSKEN_103", 550, 200 )
sideSaws.setWanderBehaviorForChildren( 100, 200, 3, 6, 300)
sideSaws.setMonsterLevelForChildren( 4.9 )


//------------------------------------------
// ALLIANCES                                
//------------------------------------------

roadGuardOne.allyWithSpawner( roadGuardTwo )
roadGuardOne.allyWithSpawner( sawHome )

patrolOne.allyWithSpawner( patrolTwo )
patrolOne.allyWithSpawner( patrolThree )
patrolOne.allyWithSpawner( patrolFour )
patrolTwo.allyWithSpawner( patrolThree )
patrolTwo.allyWithSpawner( patrolFour )
patrolThree.allyWithSpawner( patrolFour )

//==========================
//INITIAL LOGIC STARTS HERE    
//==========================

roadGuardOne.forceSpawnNow()
roadGuardTwo.forceSpawnNow()
patrolOne.forceSpawnNow()
patrolTwo.forceSpawnNow()
patrolThree.forceSpawnNow()
patrolFour.forceSpawnNow()
upAndDown1.forceSpawnNow()
upAndDown2.forceSpawnNow()
upAndDown3.forceSpawnNow()
upAndDown4.forceSpawnNow()
coast1.forceSpawnNow()
coast2.forceSpawnNow()
sawHome.spawnAllNow()

//=====================================
// ENTER THE SAW MILL                  
//=====================================
timerMap = [:]
onClickSawMillEntrance = new Object()

sawMillEntrance = makeSwitch( "SawMill_In", myRooms.BASSKEN_102, 940, 500 )
sawMillEntrance.unlock()
sawMillEntrance.off()
sawMillEntrance.setRange( 250 )
sawMillEntrance.setMouseoverText("The Saw Mill")

def enterSawMill = { event ->
	synchronized( onClickSawMillEntrance ) {
		sawMillEntrance.off()
		if( isPlayer( event.actor ) ) {
			event.actor.getCrew().each{
				if( it.isOnQuest( 83 ) || it.isDoneQuest( 83 ) ) {
					event.actor.setQuestFlag( GLOBAL, "Z05TempLumberYardEntranceAllowed" )
				}
			}
			if( sawHome.spawnsInUse() == 0 && event.actor.hasQuestFlag( GLOBAL, "Z05TempLumberYardEntranceAllowed" ) ) {
				event.actor.unsetQuestFlag( GLOBAL, "Z05TempLumberYardEntranceAllowed" )
				//********************* START DIFFICULTY SETTINGS ****************************
				if( event.actor.getTeam().hasAreaVar( "Z28_Saw_Mill", "Z28SawMillDifficulty" ) == false ) {
					if( event.actor.getCrewVar( "Z28SawMillDifficultySelectionInProgress" ) == 0 ) {
						//Prevent multiple players from initiating the process of selecting difficulties
						event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 1 )

						//check to see if the Crew leader is in the zone
						if( isInZone( event.actor.getTeam().getLeader() ) ) {
							leader = event.actor.getTeam().getLeader()
							makeMenu()

							event.actor.getCrew().each{ if( it != event.actor.getTeam().getLeader() ) { it.centerPrint( "Your Crew Leader is choosing the challenge level for this encounter." ) } }

							//Everyone else gets the following message while the leader chooses a setting. If no choice is made after 10 seconds, then everyone gets a message that says:
							decisionTimer = myManager.schedule(30) {
								event.actor.getCrew().each{
									it.centerPrint( "No choice was made within 30 seconds. Click the Saw Mill door to try again." )
								}
								event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 0 )
								//if no selection was made, then remove the leader and timer from the timerMap
								timerMap.remove( event.actor.getTeam().getLeader() )
							}
							//add the leader and timer to a map so the timer can be canceled later, if necessary
							timerMap.put( event.actor.getTeam().getLeader(), decisionTimer )

						//If the leader is not in the zone then tell the crew the leader must be present
						} else {
							event.actor.getCrew().each{
								if( it != event.actor.getTeam().getLeader() ) {
									it.centerPrint( "Your Crew Leader must be in Bass'ken Lake before you can enter the Saw Mill." )
								} else {
									it.centerPrint( "Your Crew is trying to enter the Saw Mill. You must be in Bass'ken Lake for that to occur." )
								}
							}
							event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 0 )
						}
					} else if( event.actor.getCrewVar( "Z28SawMillDifficultySelectionInProgress" ) == 1 ) {
						if( event.actor != event.actor.getTeam().getLeader() ) {
							event.actor.centerPrint( "Your Crew Leader is already making a challenge level choice. One moment, please..." )
						}
					}

				//If the Difficulty is not zero, then it has been set and the player can enter the Shrine
				} else if( event.actor.getTeam().hasAreaVar( "Z28_Saw_Mill", "Z28SawMillDifficulty" ) == true ) { 					
					event.actor.centerPrint( "You open the creaky door lumber yard door and carefully walk inside." )
					event.actor.warp( "SawMill_1", 150, 560 )	
				//********************* END DIFFICULTY SETTINGS ****************************
				} 
			} else if( !event.actor.hasQuestFlag( GLOBAL, "Z05TempLumberYardEntranceAllowed" ) ) {
				event.actor.centerPrint( "The door won't open for you. Talk to Gustav, or Crew with someone that has previously entered the Saw Mill." )
			} else if( sawHome.spawnsInUse() > 0 ) {
				event.actor.centerPrint( "You must defeat all the Buzz Saws guarding the Saw Mill before you can enter." )
			}
		}
	}
}

sawMillEntrance.whenOn( enterSawMill )

def synchronized makeMenu() {
	descripString = "You must choose a Challenge Level to enter the Saw Mill. You have 30 seconds to decide."
	diffOptions = ["Easy (small rewards)", "Normal (moderate rewards)", "Hard (large rewards)", "Cancel"]
	
	println "**** trying to create the menu ****"
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy (small rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click the door again to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click the door again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z28_Saw_Mill", "Z28SawMillDifficulty", 1 )
			event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal (moderate rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click the door again to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click the door again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z28_Saw_Mill", "Z28SawMillDifficulty", 2 )
			event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard (large rewards)" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click the door again to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click the door again to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z28_Saw_Mill", "Z28SawMillDifficulty", 3 )
			event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			if( event.actor != event.actor.getTeam().getLeader() ) return
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar( "Z28SawMillDifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}
