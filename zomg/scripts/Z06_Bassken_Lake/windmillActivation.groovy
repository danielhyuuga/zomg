//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//Final positions
position1 = myRooms.BASSKEN_206.spawnSpawner("position1", "flying_giftbox", 1)
position1.setPos(950, 460)
position1.setMonsterLevelForChildren( 4.5 )

position2 = myRooms.BASSKEN_206.spawnSpawner("position2", "flying_giftbox", 1)
position2.setPos(965, 325)
position2.setMonsterLevelForChildren( 4.5 )

position3 = myRooms.BASSKEN_206.spawnSpawner("position3", "flying_giftbox", 1)
position3.setPos(855, 190)
position3.setMonsterLevelForChildren( 4.5 )

position4 = myRooms.BASSKEN_206.spawnSpawner("position4", "flying_giftbox", 1)
position4.setPos(630, 110)
position4.setMonsterLevelForChildren( 4.5 )

//Defining gift box spawners
windmillBox1 = myRooms.BASSKEN_305.spawnSpawner("windmillBox1", "flying_giftbox", 1)
windmillBox1.setPos(435, 360)
windmillBox1.setGuardPostForChildren(position1, 125)
windmillBox1.setMonsterLevelForChildren( 4.5 )

windmillBox2 = myRooms.BASSKEN_305.spawnSpawner("windmillBox2", "flying_giftbox", 1)
windmillBox2.setPos(970, 440)
windmillBox2.setGuardPostForChildren(position2, 125)
windmillBox2.setMonsterLevelForChildren( 4.5 )

windmillBox3 = myRooms.BASSKEN_306.spawnSpawner("windmillBox3", "flying_giftbox", 1)
windmillBox3.setPos(75, 375)
windmillBox3.setGuardPostForChildren(position3, 125)
windmillBox3.setMonsterLevelForChildren( 4.5 )

windmillBox4 = myRooms.BASSKEN_306.spawnSpawner("windmillBox4", "flying_giftbox", 1)
windmillBox4.setPos(480, 220)
windmillBox4.setGuardPostForChildren(position4, 125)
windmillBox4.setMonsterLevelForChildren( 4.5 )

//Setting spawners to spawn only when called
position1.stopSpawning()
position2.stopSpawning()
position3.stopSpawning()
position4.stopSpawning()

windmillBox1.stopSpawning()
windmillBox2.stopSpawning()
windmillBox3.stopSpawning()
windmillBox4.stopSpawning()

windmillBoxesSpawned = false
disposeCalled = false

//Defining windmill trigger zone
def windmillTrigger = "windmillTrigger"
myRooms.BASSKEN_206.createTriggerZone(windmillTrigger, 100, 65, 350, 170)

myManager.onTriggerIn(myRooms.BASSKEN_206, windmillTrigger) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(73, 2) ) {
		if( windmillBoxesSpawned == false ) {
			windmillBoxesSpawned = true
			myManager.schedule(3) { 
				box1 = windmillBox1.forceSpawnNow()
				box2 = windmillBox2.forceSpawnNow()
				box3 = windmillBox3.forceSpawnNow()
				box4 = windmillBox4.forceSpawnNow()
			
			
				runOnDeath(box1) { 
					box1 = null
					if(box2 == null && box3 == null && box4 == null) { windmillBoxesSpawned = false }
				}
				runOnDeath(box2) { 
					box2 = null
					if(box1 == null && box3 == null && box4 == null) { windmillBoxesSpawned = false }
				}
				runOnDeath(box3) { 
					box3 = null 
					if(box1 == null && box2 == null && box4 == null) { windmillBoxesSpawned = false }
				}
				runOnDeath(box4) { 
					box4 = null
					if(box1 == null && box2 == null && box3 == null) { windmillBoxesSpawned = false }
				}
			}
			
			myManager.schedule(180) { boxesDispose() }
		}
		event.actor.updateQuest(73, "[NPC] Old Man Logan")
		event.actor.centerPrint("You hook Logan's contraption into the windmill.")
		myManager.schedule(1) { sound( "windmillScreech" ).toPlayer( event.actor ) }
	}
}

def boxesDispose() {
	disposeCalled = false
	if( box1 != null ) {
		if( box1.getHated().isEmpty() ) { 
			//println "##### box1.getHated.size() = ${box1?.getHated().size()} #####"
			box1.dispose() 
			box1 = null
			//println "##### box1 = ${box1} #####"
		} else { 
			disposeCalled = true
			//println "##### box1.getHated.size() = ${box1?.getHated().size()} #####"
			//println "##### box1 = ${box1} #####"
		}
	}
	if( box2 != null ) {
		if( box2.getHated().isEmpty() ) { 
			//println "##### box2.getHated.size() = ${box2?.getHated().size()} #####"
			box2.dispose() 
			box2 = null
			//println "##### box2 = ${box2} #####"
		} else { 
			disposeCalled = true
			//println "##### box2.getHated.size() = ${box2?.getHated().size()} #####"
			//println "##### box2 = ${box2} #####"
		} 
	}
	if( box3 != null ) {
		if( box3.getHated().isEmpty() ) { 
			//println "##### box3.getHated.size() = ${box3?.getHated().size()} #####"
			box3.dispose() 
			box3 = null
			//println "##### box3 = ${box3} #####"
		} else { 
			disposeCalled = true 
			//println "##### box3.getHated.size() = ${box3?.getHated().size()} #####"
			//println "##### box3 = ${box3} #####"
		}
	}
	if( box4 != null ) {
		if( box4.getHated().isEmpty() ) { 
			//println "##### box4.getHated.size() = ${box4?.getHated().size()} #####"
			box4.dispose() 
			box4 = null
			//println "##### box4 = ${box4} #####"
		} else { 
			disposeCalled = true
			//println "##### box4.getHated.size() = ${box4?.getHated().size()} #####"
			//println "##### box4 = ${box4} #####"
		} 
	}
	if( disposeCalled == true ) {
		//println "##### boxesDispose() called because disposeCalled == ${disposeCalled}"
		myManager.schedule(15) { boxesDispose() }
	}
	if( box1 == null && box2 == null && box3 == null && box4 == null) { windmillBoxesSpawned = false }
}