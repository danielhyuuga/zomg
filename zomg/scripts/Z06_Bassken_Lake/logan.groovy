import com.gaiaonline.mmo.battle.script.*;

Logan = spawnNPC("[NPC] Old Man Logan", myRooms.BASSKEN_403, 890, 300)
Logan.setDisplayName( "Logan" )

onQuestStep(42, 2) { event -> event.player.addMiniMapQuestActorName("[NPC] Old Man Logan") }
onQuestStep(70, 2) { event -> event.player.addMiniMapQuestActorName("[NPC] Old Man Logan") }
onQuestStep(71, 2) { event -> event.player.addMiniMapQuestActorName("[NPC] Old Man Logan") }
onQuestStep(72, 2) { event -> event.player.addMiniMapQuestActorName("[NPC] Old Man Logan") }
onQuestStep(73, 2) { event -> event.player.addMiniMapQuestActorName("[NPC] Old Man Logan") }

//onEnter logic to remove the connector flag from Uncle Kin
myManager.onEnter( myRooms.BASSKEN_403 ) { event ->
	if( isPlayer( event.actor ) ) {
		if( event.actor.hasQuestFlag( GLOBAL, "Z17KinToLoganConnector" ) ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z17KinToLoganConnector" )
			event.actor.removeMiniMapQuestLocation( "loganConnector" )
		}
		if(!event.actor.isOnQuest(73) && !event.actor.isDoneQuest(73) && event.actor.isDoneQuest(72) && !event.actor.hasQuestFlag(GLOBAL, "Z6_Logan_Windmill_Break")) {
			event.actor.setQuestFlag(GLOBAL, "Z6_Logan_Windmill_Break")
		}
		event.actor.unsetQuestFlag( GLOBAL, "Z06RingRewardInProgress" )
	}
}

//------------------------------------------------------------------------------------------------------------------------
// Default Conversation - Old Man Logan                                                                                   
//------------------------------------------------------------------------------------------------------------------------
def LoganDefault = Logan.createConversation("LoganDefault", true, "!QuestStarted_61:2", "!Z6LoganQuestTree", "!QuestStarted_42", "!QuestCompleted_42", "!Z6_Logan_FishinForAnswers_Complete", "!Z6_Logan_TheBoy_Active", "!Z6_Logan_TheBoy_Break", "!Z6_Logan_TheBoy_Visited", "!Z6_Logan_TheBoy_Complete", "!Z6_Logan_GTeam", "!Z6_Logan_Gambino", "!Z6_Logan_Experiments")
LoganDefault.setUrgent(true)

def Logan1 = [id:1]
Logan1.npctext = "Hey there, bub. Welcome to the Hole! I'm Logan. What can I do for you today?"
Logan1.playertext = "The Hole?"
Logan1.result = 2
LoganDefault.addDialog(Logan1, Logan)

def Logan2 = [id:2]
Logan2.npctext = "The Ole Fishing Hole, of course! We're world-famous!"
Logan2.playertext = "World famous?"
Logan2.result = 3
LoganDefault.addDialog(Logan2, Logan)

def Logan3 = [id:3]
Logan3.npctext = "Well, famous around Barton Town, anyway. But still, it's a pretty lake and it *used* to be a quiet neighborhood."
Logan3.playertext = "Used to be?"
Logan3.result = 4
LoganDefault.addDialog(Logan3, Logan)

def Logan4 = [id:4]
Logan4.npctext = "What? Did you swallow a parrot or something? You need to be all tongue-tied around me."
Logan4.playertext = "Tongue-ti... err... yeah. Or rather... no... I didn't swallow a parrot. I mean... do you run this store?"
Logan4.result = 5
LoganDefault.addDialog(Logan4, Logan)

def Logan5 = [id:5]
Logan5.npctext = "Yup. I own the old windmill and lumber yard too, but I don't have time for anything other'n the Hole nowadays. Business had been booming until the Animated started showing up."
Logan5.playertext = "They've been trouble everywhere."
Logan5.result = 6
LoganDefault.addDialog(Logan5, Logan)

def Logan6 = [id:6]
Logan6.npctext = "True, but at least there's plenty to do around here nowadays, especially if you aren't afraid of a little rough and tumble. Whatcha lookin' for, kid?"
Logan6.result = 7
LoganDefault.addDialog(Logan6, Logan)

def Logan7 = [id:7]
Logan7.options = []
//Logan7.options << [text:"What's your store like?", result: 8]
//Logan7.options << [text:"Is it all right to fish in the lake?", result: 9]
Logan7.options << [text:"Aren't you afraid to be out here alone with all the Animated roaming around?", result: 11]
Logan7.options << [text:"Nothing right now. Thanks though!", result: DONE]
LoganDefault.addDialog(Logan7, Logan)

def Logan8 = [id:8]
Logan8.npctext = "The Hole is always open, night or day. If you're in need of a pole, a bit of bait, or you just want to sell some of the catch you've made in the Lake, then come on by and I'll take care of you. Just step right through that door!"
Logan8.playertext = "Thanks, Logan. I'll check it out sometime."
Logan8.result = 7
LoganDefault.addDialog(Logan8, Logan)

def Logan9 = [id:9]
Logan9.npctext = "Well, of course! It wouldn't be much of a fishing hole if you couldn't drop a line in the water, now would it? Just go out to the end of any of the piers on the Lake and you'll be able to cast out and start catching fish!"
Logan9.playertext = "Wow. No fishing licenses or anything?"
Logan9.result = 10
LoganDefault.addDialog(Logan9, Logan)

def Logan10 = [id:10]
Logan10.npctext = "Well, if the gaming warden comes around, you just send him over to me and we'll work things out. Go catch some fish!"
Logan10.playertext = "Awesome. Thanks, Logan!"
Logan10.result = 7
LoganDefault.addDialog(Logan10, Logan)

def Logan11 = [id:11]
Logan11.npctext = "Ol' Logan, afraid? I'm not helpless. I saw more than my share of danger back in my G... before I retired."
Logan11.playertext = "Retired? I assumed you always ran the Fishin' Hole. What'd you do before you retired?"
Logan11.flag = "Z6LoganQuestTree"
Logan11.exec = { event ->
	Logan.pushDialog(event.player, "loganGainingTrust")
}
Logan11.result = DONE
LoganDefault.addDialog(Logan11, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//KIN'S TRIALS (Part 3) Logan's Leg                                                                                             
//------------------------------------------------------------------------------------------------------------------------------

def kinTrialTalk = Logan.createConversation("kinTrialTalk", true, "QuestStarted_61:2", "!Z6DelayedAgathaConnection")

def Trial1 = [id:1]
Trial1.npctext = "Hey there, bub. How are ya? Care to take in some fishing?"
Trial1.playertext = "Actually, a friend of yours sent me. Do you know Kin?"
Trial1.result = 2
kinTrialTalk.addDialog(Trial1, Logan)

def Trial2 = [id:2]
Trial2.npctext = "Tall intimidating type, way too serious, with a really strange mask?"
Trial2.playertext = "Yup. That describes him pretty well. He says he knows you."
Trial2.result = 3
kinTrialTalk.addDialog(Trial2, Logan)

def Trial3 = [id:3]
Trial3.npctext = "Yeah. He and I traveled together a bit after I left...well...after I left a previous engagement."
Trial3.playertext = "'Previous engagement'? Traveled together? I smell stories!"
Trial3.result = 4
kinTrialTalk.addDialog(Trial3, Logan)

def Trial4 = [id:4] 
Trial4.npctext = "Yeah...well, mebbee. But my past is valuable to me and you've got to earn the right to hear about it. What does Kin want?"
Trial4.playertext = "We've been learning as much as we can about the Animated, and he asked me to share what we know."
Trial4.result = 5
kinTrialTalk.addDialog(Trial4, Logan)

def Trial5 = [id:5]
Trial5.npctext = "WE? You're a ninja then?"
Trial5.playertext = "Well...no. Not yet anyway."
Trial5.result = 6
kinTrialTalk.addDialog(Trial5, Logan)

def Trial6 = [id:6]
Trial6.npctext = "I thought not. Okay...tell me what ya know and maybe I'll share a bit of my past with ya."
Trial6.playertext = "Well, in a nutshell, different groups of Animated don't seem to share purposes with each other, but they do seem to evolve over time into bigger, stronger versions of themselves."
Trial6.result = 7
kinTrialTalk.addDialog(Trial6, Logan)

def Trial7 = [id:7]
Trial7.npctext = "How the heck do they do that?"
Trial7.playertext = "The ninjas say it has to do with some sort of energy fluctuations that waver back and forth across the countryside, sometimes eddying together and combining to create something larger and tougher than normal."
Trial7.result = 8
kinTrialTalk.addDialog(Trial7, Logan)

def Trial8 = [id:8]
Trial8.npctext = "Ha! In other words, Kin and his crew don't know either. They're just guessing and covering it with their usual mystic mumbo-jumbo."
Trial8.playertext = "It could be...but they warned me about the energy buildups in time for me to intercept some of them from forming and taking over the groups already in the Zen Gardens."
Trial8.result = 9
kinTrialTalk.addDialog(Trial8, Logan)

def Trial9 = [id:9]
Trial9.npctext = "Heh. I still say it could be coincidence, but I guess that adds up to what I see here at the Lake, too. The wolves, saws, and fluffs don't seem to care too much about each other...and once in a while, something major shows up and romps the field for a bit before it disappears. Hmmmm..."
Trial9.playertext = "Still, it doesn't explain much, does it? Things appear alive for no particular reason and just start attacking things? It's really strange..."
Trial9.result = 10
kinTrialTalk.addDialog(Trial9, Logan)

def Trial10 = [id:10]
Trial10.npctext = "You might have something there...it reminds me of the time that the Gambino mansion got overrun with zombies. That was a hell of a mess...and it all started because of...damn."
Trial10.playertext = "Damn?"
Trial10.result = 11
kinTrialTalk.addDialog(Trial10, Logan)

def Trial11 = [id:11]
Trial11.npctext = "Yeah. Damn. If that situation connects with what's going on now, then Gambino's got to be mixed up in this thing somehow. And I'm sick to death of cleaning up his messes."
Trial11.playertext = "Gambino? *Johnny* Gambino? The head of G-Corp?"
Trial11.result = 12
kinTrialTalk.addDialog(Trial11, Logan)

def Trial12 = [id:12]
Trial12.npctext = "That's the one, bub. Listen, I've got my hands full here at the Lake and I've got responsibilities I can't just abandon. But maybe you could do me a favor?"
Trial12.options = []
Trial12.options << [text:"Sure. What do you need?", result: 14]
Trial12.options << [text:"I need a break right now. I'll come back later and see how I might help.", result: 13]
kinTrialTalk.addDialog(Trial12, Logan)

def Trial13 = [id:13]
Trial13.npctext = "Okay. Mind the wolves, and I'll see ya when I see ya."
Trial13.flag = "Z6DelayedAgathaConnection" // create a flag to avoid the bulk of the above conversation upon the return of the player to Logan later on
Trial13.result = DONE
kinTrialTalk.addDialog(Trial13, Logan)

def Trial14 = [id:14]
Trial14.npctext = "I need to get in touch with my old friends that know Johnny really well so we can figure out what he's up to. Edmund is cut off over in Durem, with no way to get here right now, but Agatha might know how to reach him. Do you know Agatha?"
Trial14.playertext = "She runs the jewelry shop in Barton Town, right?"
Trial14.result = 15
kinTrialTalk.addDialog(Trial14, Logan)

def Trial15 = [id:15]
Trial15.npctext = "That's our girl! Go tell her what you just told me. She'll know whether this means anything and she'll get the word to Edmund...somehow."
Trial15.playertext = "So just go talk to Agatha at the Barton Jeweler's?"
Trial15.result = 16
kinTrialTalk.addDialog(Trial15, Logan)

def Trial16 = [id:16]
Trial16.npctext = "That's the stuff! Good luck, kid!"
Trial16.playertext = "Thanks. And the name's %p."
Trial16.result = 17
kinTrialTalk.addDialog(Trial16, Logan)

def Trial17 = [id:17]
Trial17.npctext = "hehehe. Sure it is, kid. Now get scootin'!"
Trial17.quest = 61 //push the completion step for "Kin's Trials (part 3)" for Logan's leg
Trial17.exec = { event ->
	event.player.removeMiniMapQuestActorName( "[NPC] Old Man Logan" )
	event.player.addMiniMapQuestActorName( "Agatha-VQS" )
}
Trial17.result = DONE
kinTrialTalk.addDialog(Trial17, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Delayed Conversation with Logan (before going to talk to Agatha on the Kin's Travel Quest)                                    
//------------------------------------------------------------------------------------------------------------------------------
def loganDelayedTalk = Logan.createConversation("loganDelayedTalk", true, "QuestStarted_61:2", "Z6DelayedAgathaConnection")

def Delayed1 = [id:1]
Delayed1.npctext = "You ready to help me out with that favor yet?"
Delayed1.options = []
Delayed1.options << [text:"Yup! What do you need?", result: 2]
Delayed1.options << [text:"Oops. Not yet. I'll be back.", result: DONE]
loganDelayedTalk.addDialog(Delayed1, Logan)

def Delayed2 = [id:2]
Delayed2.npctext = "I need to get in touch with my old friends that know Johnny really well so we can figure out what he's up to. Edmund is cut off over in Durem, with no way to get here right now, but Agatha might know how to reach him. Do you know Agatha?"
Delayed2.playertext = "She runs the jewelry shop in Barton Town, right?"
Delayed2.result = 3
loganDelayedTalk.addDialog(Delayed2, Logan)

def Delayed3 = [id:3]
Delayed3.npctext = "That's our girl! Go tell her what you just told me. She'll know whether this means anything and she'll get the word to Edmund...somehow."
Delayed3.playertext = "So just go talk to Agatha at the Barton Jeweler's?"
Delayed3.result = 4
loganDelayedTalk.addDialog(Delayed3, Logan)

def Delayed4 = [id:4]
Delayed4.npctext = "That's the stuff! Good luck, kid!"
Delayed4.playertext = "Thanks. And the name's %p."
Delayed4.result = 5
loganDelayedTalk.addDialog(Delayed4, Logan)

def Delayed5 = [id:5]
Delayed5.npctext = "hehehe. Sure it is, kid. Now get scootin'!"
Delayed5.quest = 61 //push the completion step for "Kin's Trials (part 3)" for Logan's leg
Delayed5.exec = { event ->
	event.player.removeMiniMapQuestActorName( "[NPC] Old Man Logan" )
	event.player.addMiniMapQuestActorName( "Agatha-VQS" )
}
Delayed5.result = DONE
loganDelayedTalk.addDialog(Delayed5, Logan)


//------------------------------------------------------------------------------------------------------------------------------
//Gaining Trust - Quest given                                                                                                   
//------------------------------------------------------------------------------------------------------------------------------
def loganGainingTrust = Logan.createConversation("loganGainingTrust", true, "Z6LoganQuestTree", "!QuestStarted_42", "!QuestCompleted_42")

def gainingTrust1 = [id:1]
gainingTrust1.npctext = "How do I even know yer worth my time, bub? Information's got a price, so before you get your motor spun up, why I should tell ya a thing?"
gainingTrust1.options = []
gainingTrust1.options << [text:"Uhm... because I have a kind and generous soul.", result: 2]
//gainingTrust1.options << [text:"Because I can fish!", result: 5]
gainingTrust1.options << [text:"Hmm, I honestly can't think of a reason.", result: 3]
gainingTrust1.options << [text:"Nevermind. I'm going to head off on my own instead.", result: DONE]
loganGainingTrust.addDialog(gainingTrust1, Logan)

def gainingTrust2 = [id:2]
gainingTrust2.npctext = "And I bet you'll kindly and generously relay whatever information I give ya to whoever passes by. See, this is exactly what I'm talkin' about."
gainingTrust2.playertext = "I wouldn't do that, Logan. Just tell me how I can prove myself and I will."
gainingTrust2.result = 3
loganGainingTrust.addDialog(gainingTrust2, Logan)

def gainingTrust3 = [id:3]
gainingTrust3.npctext = "I've got the perfect task for a puff like you. The grass fluffs in the woods to the south are just about the weakest thing you'll find near the lake. If you can't take them out, you've got no business with me."
gainingTrust3.options = []
gainingTrust3.options << [text:"I suppose I can do that. How many fluffs did you have in mind? (**NOTE: The reward for completing Logan's series of tasks includes the selection of a new ring!**)", result: 4]
gainingTrust3.options << [text:"Nah. I'm gonna take off for now.", result: 7]
loganGainingTrust.addDialog(gainingTrust3, Logan)

def gainingTrust4 = [id:4]
gainingTrust4.npctext = "What?! Oh, I guess about ten should do."
gainingTrust4.flag = "!Z6LoganQuestTree"
gainingTrust4.quest = 42
gainingTrust4.result = DONE
loganGainingTrust.addDialog(gainingTrust4, Logan)

/*def gainingTrust5 = [id:5]
gainingTrust5.npctext = "Hoho! Alright, bub, prove it. Bring me some fish!"
gainingTrust5.options = []
gainingTrust5.options << [text:"You got it! How many?", result: 6]
gainingTrust5.options << [text:"Okay, you called my bluff. Is there some other way I can prove myself?", result: 3]
gainingTrust5.options << [text:"On second though, I think I'm gonna take off.", result: 7]
loganGainingTrust.addDialog(gainingTrust5, Logan)*/

/*def gainingTrust6 = [id:6]
gainingTrust6.npctext = "Bring me two red guppies, two orange guppies, and two yellow guppies. That should prove you know how to handle yer rod. **DEV** You have been flagged as completing this quest for the purpose of progression testing."
gainingTrust6.flag = "!Z6LoganQuestTree"
gainingTrust6.result = DONE
loganGainingTrust.addDialog(gainingTrust6, Logan)*/

def gainingTrust7 = [id:7]
gainingTrust7.npctext = "Well, bub, I guess you'll remain curious."
gainingTrust7.result = DONE
loganGainingTrust.addDialog(gainingTrust7, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Fishing For Answers - Quest active - No longer active                                                                         
//------------------------------------------------------------------------------------------------------------------------------
/*def loganFishingActive = Logan.createConversation("loganFishingActive", true, "Z6_Logan_FishinForAnswers_Active", "!Z6_Logan_FishinForAnswers_Complete", "!Z6_Logan_FishinForAnswers_Caught")

def fishingActive1 = [id:1]
fishingActive1.npctext = "Still no fish? What's wrong? Get busy, bub."
fishingActive1.result = DONE
loganFishingActive.addDialog(fishingActive1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Fishing For Answers - Quest completion - No longer active                                                                     
//------------------------------------------------------------------------------------------------------------------------------
def loganFishingComplete = Logan.createConversation("loganFishingComplete", true, "Z6_Logan_FishinForAnswers_Caught", "!Z6_Logan_FishinForAnswers_Complete")

def fishingComplete1 = [id:1]
fishingComplete1.npctext = "Well would ya look at that? Looks like you weren't kidding about your fishing. You may be worth my time, after all."
fishingComplete1.playertext = "Does that mean you'll tell me about what you did before you retired?"
fishingComplete1.result = 2
loganFishingComplete.addDialog(fishingComplete1, Logan)

def fishingComplete2 = [id:2]
fishingComplete2.npctext = "Ahh... so you remembered the bargain. Alright, pull up a chair, bub, I guess it's story time."
fishingComplete2.flag = ["!Z6_Logan_FishinForAnswers_Caught", "Z6_Logan_FishinForAnswers_Complete", "Z6_Logan_GTeam"]
fishingComplete2.exec = { event -> Logan.pushDialog(event.player, "loganGTeam") }
fishingComplete2.result = DONE
loganFishingComplete.addDialog(fishingComplete2, Logan)*/

//------------------------------------------------------------------------------------------------------------------------------
//Fluffs Ya Puff - Quest active                                                                                                 
//------------------------------------------------------------------------------------------------------------------------------
def loganFluffsActive = Logan.createConversation("loganFluffsActive", true, "QuestStarted_42", "!QuestStarted_42:3")

def fluffsActive1 = [id:1]
fluffsActive1.npctext = "Having a little trouble with the fluffs? I'm sure you can handle the tiny little things, ya puff."
fluffsActive1.result = DONE
loganFluffsActive.addDialog(fluffsActive1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Fluffs Ya Puff - Quest completion                                                                                             
//------------------------------------------------------------------------------------------------------------------------------
def loganFluffsComplete = Logan.createConversation("loganFluffsComplete", true, "QuestStarted_42:3")

def fluffsComplete1 = [id:1]
fluffsComplete1.npctext = "Hoho! Took out some grass fluffs and now you think you're tough? Hah."
fluffsComplete1.playertext = "Bah, you're the one that wanted me to prove myself!"
fluffsComplete1.result = 2
loganFluffsComplete.addDialog(fluffsComplete1, Logan)

def fluffsComplete2 = [id:2]
fluffsComplete2.npctext = "Ahh... I suppose you've done that."
fluffsComplete2.flag = "Z6_Logan_GTeam"
fluffsComplete2.quest = 42
fluffsComplete2.exec = { event -> 
	Logan.pushDialog(event.player, "theBoyGive") 
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
}
fluffsComplete2.result = DONE
loganFluffsComplete.addDialog(fluffsComplete2, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Give The Boy                                                                                                                  
//------------------------------------------------------------------------------------------------------------------------------
def theBoyGive = Logan.createConversation("theBoyGive", true, "Z6_Logan_GTeam")

def theBoyGive1 = [id:1]
theBoyGive1.npctext = "Listen, I'm not big on stories so don't ask. You asked what I did before I retired - I worked for Gambino. That's all you'll get outta me."
theBoyGive1.playertext = "Oh, that's cool Logan. What's next?"
theBoyGive1.result = 2
theBoyGive.addDialog(theBoyGive1, Logan)

def theBoyGive2 = [id:2]
theBoyGive2.npctext = "One of my responsibilities here on the lake is to keep an eye on the Todd boy. His dad is an old friend and asked me to look after him while he's off galavanting on some dangerous adventure."
theBoyGive2.playertext = "What's that have to do with me?"
theBoyGive2.result = 3
theBoyGive.addDialog(theBoyGive2, Logan)

def theBoyGive3 = [id:3]
theBoyGive3.npctext = "Simple, I'm tired of trudging over to the water mill every day so I'm sending you instead. Howzat for an idea?"
theBoyGive3.options = []
theBoyGive3.options << [text:"I suppose it's a start.", result: 4]
theBoyGive3.options << [text:"What? Run your own errands, Logan. I'm outta here.", flag:["!Z6_Logan_GTeam", "Z6_Logan_TheBoy_Break"], result: DONE]
theBoyGive.addDialog(theBoyGive3, Logan)

def theBoyGive4 = [id:4]
theBoyGive4.npctext = "Tell ya what, I'll pay ya. Sound better?"
theBoyGive4.playertext = "Sounds great!"
theBoyGive4.quest = 70
theBoyGive4.exec = { event -> event.player.addMiniMapQuestActorName("Jesse-VQS") }
theBoyGive4.flag = "!Z6_Logan_GTeam"
theBoyGive4.result = DONE
theBoyGive.addDialog(theBoyGive4, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//The Boy - Quest break                                                                                                         
//------------------------------------------------------------------------------------------------------------------------------
def loganTheBoyBreak = Logan.createConversation("loganTheBoyBreak", true, "Z6_Logan_TheBoy_Break")

def boyBreak1 = [id:1]
boyBreak1.npctext = "Hey bub, I think it's about time I checked on the Todd boy. And by 'I checked' I mean 'you checked.'"
boyBreak1.options = []
boyBreak1.options << [text:"Yep. That's why I came back, Logan. So where is the kid?", result: 2]
boyBreak1.options << [text:"Oh... right. That's what you wanted me to do. I'll think about it.", result: 3]
loganTheBoyBreak.addDialog(boyBreak1, Logan)

def boyBreak2 = [id:2]
boyBreak2.npctext = "Take the bridge across the river and head southwest to the water-wheel. You'll find him in the cabin there."
boyBreak2.flag = "!Z6_Logan_TheBoy_Break"
boyBreak2.quest = 70
boyBreak2.exec = { event -> event.player.addMiniMapQuestActorName( "Jesse-VQS" ) }
boyBreak2.result = DONE
loganTheBoyBreak.addDialog(boyBreak2, Logan)

def boyBreak3 = [id:3]
boyBreak3.npctext = "Well, get lost then!"
boyBreak3.result = DONE
loganTheBoyBreak.addDialog(boyBreak3, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//The Boy - Quest active                                                                                                        
//------------------------------------------------------------------------------------------------------------------------------
def loganTheBoyActive = Logan.createConversation("loganTheBoyActive", true, "QuestStarted_70:2")

def boyActive1 = [id:1]
boyActive1.npctext = "What's up with the Todd boy? Wait... you haven't even talked to him yet? Stop being such a slacker."
boyActive1.result = DONE
loganTheBoyActive.addDialog(boyActive1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//The Boy - Quest completion                                                                                                    
//------------------------------------------------------------------------------------------------------------------------------
def loganTheBoyComplete = Logan.createConversation("loganTheBoyComplete", true, "QuestStarted_70:3")

def boyComplete1 = [id:1]
boyComplete1.npctext = "So, I take it you've been to see the Todd boy."
boyComplete1.playertext = "I did see him. Talked to him too."
boyComplete1.flag = "Z6_Logan_Gambino"
boyComplete1.quest = 70
boyComplete1.exec = { event ->
	Logan.pushDialog(event.player, "bladesBrige")
	event.player.removeMiniMapQuestActorName( "[NPC] Old Man Logan" )
}
boyComplete1.result = DONE
loganTheBoyComplete.addDialog(boyComplete1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Bridge between boy and blades quest - to maintain flags                                                                       
//------------------------------------------------------------------------------------------------------------------------------
def bladesBrige = Logan.createConversation("bladesBrige", true, "Z6_Logan_Gambino")

def bladesBrige1 = [id:1]
bladesBrige1.npctext = "Ah, and what did he have to say?"
bladesBrige1.playertext = "He's afraid to leave the cabin because of the wolves. I think you should get rid of them."
bladesBrige1.flag = "!Z6_Logan_Gambino"
bladesBrige1.exec = { event ->
	Logan.pushDialog(event.player, "loganBladesGive")
}
bladesBrige1.result = DONE
bladesBrige.addDialog(bladesBrige1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Blades of Wrath - Quest accept                                                                                                
//------------------------------------------------------------------------------------------------------------------------------
def loganBladesGive = Logan.createConversation("loganBladesGive", true, "QuestCompleted_70", "!Z6_Logan_Gambino", "!QuestStarted_71", "!Z6_Logan_Gambino_Break", "!QuestCompleted_71", "!Z6_Logan_SawBlades_Break")

def bladesGive1 = [id:1]
bladesGive1.npctext = "What?! How am I supposed to do that? Fart?!"
bladesGive1.options = []
bladesGive1.options << [text:"I don't know. You did say you were a problem solver, though.", result: 2]
bladesGive1.options << [text:"Maybe we can try to scare them off with a loud noise?", result: 3]
bladesGive1.options << [text:"LOL! I'm not sure that'll work, Logan. Any other ideas?", result: 4]
loganBladesGive.addDialog(bladesGive1, Logan)

def bladesGive2 = [id:2]
bladesGive2.npctext = "Alright, lemme think. Hmm... okay, maybe I have something. I've noticed the buzz saws have a high pitched whine when they attack. Maybe we can amplify the sound and use it to drive the wolf pups away. What do you think?"
bladesGive2.options = []
bladesGive2.options << [text:"I think that could work.", result: 5]
bladesGive2.options << [text:"Honestly, I'm not sure... but I don't have any better ideas so I guess it's worth a shot.", result: 6]
bladesGive2.options << [text:"I'll come back later and hope you have a better idea.", result: 7]
loganBladesGive.addDialog(bladesGive2, Logan)

def bladesGive3 = [id:3]
bladesGive3.npctext = "Hey! That's not a bad idea. We could try using the windmill as an amplifier... that'd make it loud enough to scare them away."
bladesGive3.options = []
bladesGive3.options << [text:"Yeah! Let's try it!", result: 5]
bladesGive3.options << [text:"Hmm. I don't know what else to try so I guess we should try it.", result: 6]
bladesGive3.options << [text:"Actually, I think that was a really bad idea on my part. Why don't you think about it and I'll come back later?", result: 7]
loganBladesGive.addDialog(bladesGive3, Logan)

def bladesGive4 = [id:4]
bladesGive4.npctext = "Probably a good point. Let's see...okay, maybe I have something. I've noticed the buzz saws have a high pitched whine when they attack. I think it happens as something scrapes along the sides of the blade as it's moving. Maybe we can... I don't know... build some sort of amplifier with the windmill... use the sound to drive the wolf pups away. What do you think?"
bladesGive4.options = []
bladesGive4.options << [text:"Wow. That's a great idea. Let's do it!", result: 5]
bladesGive4.options << [text:"I dunno, I guess it's worth trying.", result: 6]
bladesGive4.options << [text:"That's probably worse than your first idea. Keep thinking and I'll come back.", result: 7]
loganBladesGive.addDialog(bladesGive4, Logan)

def bladesGive5 = [id:5]
bladesGive5.npctext = "Let's get busy, then. First things first, we're going to need some buzz saw parts. Head across the river and northwest to the old mill and fetch us a Saw Blade."
bladesGive5.quest = 71
bladesGive5.result = DONE
loganBladesGive.addDialog(bladesGive5, Logan)

def bladesGive6 = [id:6]
bladesGive6.npctext = "Bah, how about some confidence, bub? It'll work... old Logan's plans never fail. Now, how about you head across the river and northwest to the old mill and hunt some saws?"
bladesGive6.quest = 71
bladesGive6.result = DONE
loganBladesGive.addDialog(bladesGive6, Logan)

def bladesGive7 = [id:7]
bladesGive7.npctext = "Say what? Alright, get outta here, ya puff."
bladesGive7.flag = "Z6_Logan_SawBlades_Break"
bladesGive7.result = DONE
loganBladesGive.addDialog(bladesGive7, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Gambino - break                                                                                                               
//------------------------------------------------------------------------------------------------------------------------------
def loganGambinoBreak = Logan.createConversation("loganGambinoBreak", true, "QuestCompleted_70", "Z6_Logan_Gambino_Break", "!Z6_Logan_Gambino", "!QuestStarted_71")

def gambinoBreak1 = [id:1]
gambinoBreak1.npctext = "Okay, bub, are ya ready to tell me how Jesse is?"
gambinoBreak1.options = []
gambinoBreak1.options << [text:"Oh, I guess so.", result: 2]
gambinoBreak1.options << [text:"You're still on about that?", result: 10]
gambinoBreak1.result = 2
loganGambinoBreak.addDialog(gambinoBreak1, Logan)

def gambinoBreak2 = [id:2]
gambinoBreak2.npctext = "Well don't just stand there gawkin'. Spill it!"
gambinoBreak2.playertext = "He's afraid to come out of the cabin with all the ghost wolf pups around. He wanted me to see if you had any idea how to get rid of them?"
gambinoBreak2.result = 3
loganGambinoBreak.addDialog(gambinoBreak2, Logan)

def gambinoBreak3 = [id:3]
gambinoBreak3.npctext = "What?! How am I supposed to do that, fart?!"
gambinoBreak3.options = []
gambinoBreak3.options << [text:"I don't know. You did say you were a problem solver, though.", result: 3]
gambinoBreak3.options << [text:"I'm not sure. I think wolves are really sensitive to sound. Maybe we can find someone to scare them off with a loud noise?", result: 4]
gambinoBreak3.options << [text:"LOL! I'm not sure that'll work, Logan. Any other ideas?", result: 5]
loganGambinoBreak.addDialog(gambinoBreak3, Logan)

def gambinoBreak4 = [id:4]
gambinoBreak4.npctext = "Oh sure. Throw that in my face... never should have told you. Alright, lemme think. Hmm... okay, maybe I have something. I've noticed the buzz saws have a high pitched whine when they attack. I think it happens as something scrapes along the sides of the blade as it's moving. Maybe we can... I don't know... build some sort of amplifier with the windmill... use the sound to drive the wolf pups away. What do you think?"
gambinoBreak4.options = []
gambinoBreak4.options << [text:"I think that could work.", result: 6]
gambinoBreak4.options << [text:"Honestly, I'm not sure... but I don't have any better ideas so I guess it's worth a shot.", result: 7]
gambinoBreak4.options << [text:"Heh. Sounds futile to me. I'll come back later and hope you have a better idea.", result: 8]
loganGambinoBreak.addDialog(gambinoBreak4, Logan)

def gambinoBreak5 = [id:5]
gambinoBreak5.npctext = "Hey! That's not a bad idea. We could try using the windmill as an amplifier... that'd make it loud enough to scare them away."
gambinoBreak5.options = []
gambinoBreak5.options << [text:"Yeah! Let's try it!", result: 6]
gambinoBreak5.options << [text:"Hmm. I don't know what else to try so I guess we should try it.", result: 7]
gambinoBreak5.options << [text:"Actually, I think that was a really bad idea on my part. Why don't you think about it and I'll come back later?", result: 8]
loganGambinoBreak.addDialog(gambinoBreak5, Logan)

def gambinoBreak6 = [id:6]
gambinoBreak6.npctext = "Probably a good point. Let's see...okay, maybe I have something. I've noticed the buzz saws have a high pitched whine when they attack. Maybe we can... I don't know... build some sort of amplifier with the windmill... use the sound to drive the wolf pups away. What do you think?"
gambinoBreak6.options = []
gambinoBreak6.options << [text:"Wow. That's a great idea. Let's do it!", result: 6]
gambinoBreak6.options << [text:"I dunno, I guess it's worth trying.", result: 7]
gambinoBreak6.options << [text:"That's probably worse than your first idea. Keep thinking and I'll come back.", result: 8]
loganGambinoBreak.addDialog(gambinoBreak6, Logan)

def gambinoBreak7 = [id:7]
gambinoBreak7.npctext = "Let's get busy, then. First things first, we're going to need some buzz saw parts. Head across the river and northwest to the old mill and hunt some saws."
gambinoBreak7.flag = "!Z6_Logan_Gambino_Break"
gambinoBreak7.quest = 71
gambinoBreak7.result = DONE
loganGambinoBreak.addDialog(gambinoBreak7, Logan)

def gambinoBreak8 = [id:8]
gambinoBreak8.npctext = "Bah, how about some confidence, bub? It'll work... old Logan's plans never fail. Now, how about you head across the river and northwest to the old mill and hunt some saws?"
gambinoBreak8.flag = "!Z6_Logan_Gambino_Break"
gambinoBreak8.quest = 71
gambinoBreak8.result = DONE
loganGambinoBreak.addDialog(gambinoBreak8, Logan)

def gambinoBreak9 = [id:9]
gambinoBreak9.npctext = "Say what? Alright, get outta here, ya puff."
gambinoBreak9.flag = ["!Z6_Logan_Gambino_Break", "!Z6_Logan_SawBlades_Break"]
gambinoBreak9.quest = 71
gambinoBreak9.result = DONE
loganGambinoBreak.addDialog(gambinoBreak9, Logan)

def gambinoBreak10 = [id:10]
gambinoBreak10.npctext = "Yeah. I'm still on about THAT."
gambinoBreak10.result = DONE
loganGambinoBreak.addDialog(gambinoBreak10, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Saw Blades - Quest break                                                                                                      
//------------------------------------------------------------------------------------------------------------------------------
def loganSawBladesBreak = Logan.createConversation("loganSawBladesBreak", true, "Z6_Logan_SawBlades_Break")

def bladesBreak1 = [id:1]
bladesBreak1.npctext = "You're back. Ready to get started?"
bladesBreak1.options = []
bladesBreak1.options << [text:"Yeah. I gues your plan is worth a shot after all.", result: 2]
bladesBreak1.options << [text:"You mean you still don't have a new plan? Uhg!", result:3]
loganSawBladesBreak.addDialog(bladesBreak1, Logan)

def bladesBreak2 = [id:2]
bladesBreak2.npctext = "I knew you'd come around. Okay, we need to start by getting some buzz saw parts. Head across the river and northwest to the old mill and hunt some saws."
bladesBreak2.flag = "!Z6_Logan_SawBlades_Break"
bladesBreak2.quest = 71
bladesBreak2.result = DONE
loganSawBladesBreak.addDialog(bladesBreak2, Logan)

def bladesBreak3 = [id:3]
bladesBreak3.npctext = "You're gettin' on my last nerve."
bladesBreak3.result = DONE
loganSawBladesBreak.addDialog(bladesBreak3, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Saw Blades - Quest active                                                                                                     
//------------------------------------------------------------------------------------------------------------------------------
def loganSawBladesActive = Logan.createConversation("loganSawBladesActive", true, "QuestStarted_71:2")

def bladesActive1 = [id:1]
bladesActive1.npctext = "No saw blades no talkie. Hurry up!"
bladesActive1.result = DONE
loganSawBladesActive.addDialog(bladesActive1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Saw Blades - Quest complete                                                                                                   
//------------------------------------------------------------------------------------------------------------------------------
def loganSawBladesComplete = Logan.createConversation("loganSawBladesComplete", true, "QuestStarted_71:3")

def bladesComplete1 = [id:1]
bladesComplete1.npctext = "Great! One step down. You know, you seem pretty useful, after all. I might have to keep you around."
bladesComplete1.options = []
bladesComplete1.options << [text:"I doubt you have that much gold, Logan.", result: 2]
bladesComplete1.options << [text:"I might! As long as you keep paying me!", result: 3]
loganSawBladesComplete.addDialog(bladesComplete1, Logan)

def bladesComplete2 = [id:2]
bladesComplete2.npctext = "Don't you be doubting ol' Logan. I've got more coins than a fish has scales! But stop distracting me, there's work to do."
bladesComplete2.options = []
bladesComplete2.options << [text:"Right. What now?", result: 4]
bladesComplete2.options << [text:"Eh... a little busy. I'll be back.", result: 5]
loganSawBladesComplete.addDialog(bladesComplete2, Logan)

def bladesComplete3 = [id:3]
bladesComplete3.npctext = "Nobody likes a gold digger, bub. But, we've got too much work to do for me to lecture you on the ills of greed."
bladesComplete3.options = []
bladesComplete3.options << [text:"Well, thanks for sparing me. What's left to do?", result: 4]
bladesComplete3.options << [text:"No stories?! I'm outta here!", result: 5]
loganSawBladesComplete.addDialog(bladesComplete3, Logan)

def bladesComplete4 = [id:4]
bladesComplete4.npctext = "Well, after looking at these blades... I think we're going to need to find something really sharp to scrape across them if we're to get enough sound."
bladesComplete4.options = []
bladesComplete4.options << [text:"Where am I supposed to find something like that?", result: 6]
bladesComplete4.options << [text:"I don't think farting is going to work this time either, Logan. LOL!", result: 7]
loganSawBladesComplete.addDialog(bladesComplete4, Logan)

def bladesComplete5 = [id:5]
bladesComplete5.npctext = "Right. You're busy. I'm busy too. Puff."
bladesComplete5.flag = "Z6_Logan_NeedleNose_Break"
bladesComplete5.exec = { event ->
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
}
bladesComplete5.quest = 71
bladesComplete5.result = DONE
loganSawBladesComplete.addDialog(bladesComplete5, Logan)

def bladesComplete6 = [id:6]
bladesComplete6.npctext = "I thought it was your turn to come up with the plan. Bah! Leave it to ol' Logan, then."
bladesComplete6.options = []
bladesComplete6.options << [text:"I know you're all over it, Logan.", result: 8]
bladesComplete6.options << [text:"Well, I could go ask Kin...", result: 9]
loganSawBladesComplete.addDialog(bladesComplete6, Logan)

def bladesComplete7 = [id:7]
bladesComplete7.npctext = "Very funny, bub. Very frickin' funny. How's about I let you come up with the plan, wise guy."
bladesComplete7.options = []
bladesComplete7.options << [text:"Oh come on, don't be that way. Jesse needs your help, Logan.", result: 8]
bladesComplete7.options << [text:"Hmm... maybe I'll just go ask Kin for help...", result: 9]
loganSawBladesComplete.addDialog(bladesComplete7, Logan)

def bladesComplete8 = [id:8]
bladesComplete8.npctext = "Have no fear. I've got the plan all set, bub. Just do what I say and the boy will be fine."
bladesComplete8.options = []
bladesComplete8.options << [text:"Okay. What do I do?", result: 10]
bladesComplete8.options << [text:"I'm busy right now. Be back later.", result: 11]
loganSawBladesComplete.addDialog(bladesComplete8, Logan)

def bladesComplete9 = [id:9]
bladesComplete9.npctext = "Ask Kin for help? Don't make me laugh. I mean, I like the guy and all, but all he could help you with is setting up a pajama party or disappearing into the forest!"
bladesComplete9.options = []
bladesComplete9.options << [text:"Heh. Then tell me what I need to do.", result: 10]
bladesComplete9.options << [text:"We'll see about that...", result: 12]
loganSawBladesComplete.addDialog(bladesComplete9, Logan)

def bladesComplete10 = [id:10]
bladesComplete10.npctext = "A while back, Bill sent me a skeeter nose and asked if I could turn them into fish-hooks. Unfortunately, they were far too brittle to bend to shape, but they might work for this!"
bladesComplete10.playertext = "How does that help us?"
bladesComplete10.result = 13
loganSawBladesComplete.addDialog(bladesComplete10, Logan)

def bladesComplete11 = [id:11]
bladesComplete11.npctext = "I am so tempted to feed you to a grunny right now."
bladesComplete11.flag = "Z6_Logan_NeedleNose_Break"
bladesComplete11.exec = { event ->
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
}
bladesComplete11.quest = 71
bladesComplete11.result = DONE
loganSawBladesComplete.addDialog(bladesComplete11, Logan)

def bladesComplete12 = [id:12]
bladesComplete12.npctext = "Yea... we will. I'll keep some marshmallows ready for your little party."
bladesComplete12.flag = "Z6_Logan_NeedleNose_Break"
bladesComplete12.exec = { event ->
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
}
bladesComplete12.quest = 71
bladesComplete12.result = DONE
loganSawBladesComplete.addDialog(bladesComplete12, Logan)

def bladesComplete13 = [id:13]
bladesComplete13.npctext = "We have to find a bigger one. The needle-nose Bill sent me was too small for what we need. Maybe you should talk to Bill, he might know something."
bladesComplete13.exec = {  event ->
	event.player.updateQuest(71, "[NPC] Old Man Logan")
	event.player.updateQuest(72, "[NPC] Old Man Logan")
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
}
bladesComplete13.result = DONE
loganSawBladesComplete.addDialog(bladesComplete13, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Needle Nose - Quest break                                                                                                     
//------------------------------------------------------------------------------------------------------------------------------
def loganNeedleBreak = Logan.createConversation("loganNeedleBreak", true, "Z6_Logan_NeedleNose_Break")

def needleBreak1 = [id:1]
needleBreak1.npctext = "Oh look, %p has decided to return. Hoo-rah."
needleBreak1.options = []
needleBreak1.options << [text:"Be nice, Logan. I'm here to help with the windmill.", result: 2]
needleBreak1.options << [text:"If you're going to be like that, I'll just leave.", result: 3]
loganNeedleBreak.addDialog(needleBreak1, Logan)

def needleBreak2 = [id:2]
needleBreak2.npctext = "Alright, what's the plan, bub?"
needleBreak2.options = []
needleBreak2.options << [text:"Surely you have one by now.", result: 4]
needleBreak2.options << [text:"I... er... uh...", result: 5]
loganNeedleBreak.addDialog(needleBreak2, Logan)

def needleBreak3 = [id:3]
needleBreak3.npctext = "And there goes %p."
needleBreak3.result = DONE
loganNeedleBreak.addDialog(needleBreak3, Logan)

def needleBreak4 = [id:4]
needleBreak4.npctext = "Yeah! I have a great plan! A while back, Bill sent me a skeeter nose and asked if I could turn them into fish-hooks. Unfortunately, they were far too brittle to bend to shape, but they might work for this!"
needleBreak4.playertext = "How does that help us?"
needleBreak4.result = 5
loganNeedleBreak.addDialog(needleBreak4, Logan)

def needleBreak5 = [id:5]
needleBreak5.npctext = "We have to find a bigger one. The needle-nose Bill sent me was too small for what we need. Maybe you should talk to Bill, he might know something."
needleBreak5.flag = "!Z6_Logan_NeedleNose_Break"
needleBreak5.quest = 72
needleBreak5.result = DONE
loganNeedleBreak.addDialog(needleBreak5, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Needle Nose - Quest active                                                                                                    
//------------------------------------------------------------------------------------------------------------------------------
def loganNeedleActive = Logan.createConversation("loganNeedleActive", true, "QuestStarted_72:2")

def needleActive1 = [id:1]
needleActive1.npctext = "Have you even been to see Bill?"
needleActive1.playertext = "Yeah, yeah. I'll get the nose eventually."
needleActive1.result = DONE
loganNeedleActive.addDialog(needleActive1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Needle Nose - Quest complete                                                                                                  
//------------------------------------------------------------------------------------------------------------------------------
def loganNeedleComplete = Logan.createConversation("loganNeedleComplete", true, "QuestStarted_72:3")

def needleComplete1 = [id:1]
needleComplete1.npctext = "Hey, %p. You got the needle? Give me a minute... Alright, now we should be able to do something useful."
needleComplete1.options = []
needleComplete1.options << [text:"How exactly is this supposed to work?", result: 2]
needleComplete1.options << [text:"Those wolf pups won't know what hit them!", result: 3]
loganNeedleComplete.addDialog(needleComplete1, Logan)

def needleComplete2 = [id:2]
needleComplete2.npctext = "Uhm... I'm not exactly sure. I think you'll just need to hook it up to the windmill's gears and it should work on its own..."
needleComplete2.options = []
needleComplete2.options << [text:"You aren't filling me with a lot of confidence.", result: 4]
needleComplete2.options << [text:"Right. Where do I find this windmill?", result: 5]
loganNeedleComplete.addDialog(needleComplete2, Logan)

def needleComplete3 = [id:3]
needleComplete3.npctext = "Hah! That's right. Once we hook this baby up to the windmill they'll head for the hills!"
needleComplete3.options = []
needleComplete3.options << [text:"I can't wait to see them turn tail.", result: 6]
needleComplete3.options << [text:"Okay... so how do I find the windmill to hook this thing up?", result: 5]
loganNeedleComplete.addDialog(needleComplete3, Logan)

def needleComplete4 = [id:4]
needleComplete4.npctext = "Bah! Don't be such a pansy. This'll go off without a hitch."
needleComplete4.options = []
needleComplete4.options << [text:"I'll trust you Logan... but you better be right. So where is this windmill anyway?", result: 5]
needleComplete4.options << [text:"Bleh. This is a big fat waste of time. Cya later.", result: 7]
loganNeedleComplete.addDialog(needleComplete4, Logan)

def needleComplete5 = [id:5]
needleComplete5.npctext = "Can't you see it from here? It's just over there on the northeast shore of the lake."
needleComplete5.playertext = "Of course I can't... all I can see is this scr... nevermind. You wouldn't get it."
needleComplete5.exec = { event ->
	event.player.updateQuest(72, "[NPC] Old Man Logan")
	event.player.updateQuest(73, "[NPC] Old Man Logan")
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
}
needleComplete5.result = DONE
loganNeedleComplete.addDialog(needleComplete5, Logan)

def needleComplete6 = [id:6]
needleComplete6.npctext = "Should be pretty funny."
needleComplete6.options = []
needleComplete6.options << [text:"LOL yep! Just lemme know where the windmill is and I'll get busy.", result: 5]
needleComplete6.options << [text:"Right. Give me a minute, though. I'm not quite ready.", result: 7]
loganNeedleComplete.addDialog(needleComplete6, Logan)

def needleComplete7 = [id:7]
needleComplete7.npctext = "I always knew you were chicken."
needleComplete7.flag = "Z6_Logan_Windmill_Break"
needleComplete7.exec  = { event ->
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
}
needleComplete7.quest = 72
needleComplete7.result = DONE
loganNeedleComplete.addDialog(needleComplete7, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Windmill - Quest break                                                                                                        
//------------------------------------------------------------------------------------------------------------------------------
def loganWindmillBreak = Logan.createConversation("loganWindmillBreak", true, "Z6_Logan_Windmill_Break")

def windmillBreak1 = [id:1]
windmillBreak1.npctext = "Welcome to the Ole Fishi... oh, it's just %p. Whaddya want, bub?"
windmillBreak1.options = []
windmillBreak1.options << [text:"To scare off the wolf pups! Point me at the windmill!", result: 2]
windmillBreak1.options << [text:"Just came by to laugh at you. Haha!", result: 3]
loganWindmillBreak.addDialog(windmillBreak1, Logan)

def windmillBreak2 = [id:2]
windmillBreak2.npctext = "Can't you see it from here? It's just over there on the northeast shore of the lake."
windmillBreak2.flag = "!Z6_Logan_Windmill_Break"
windmillBreak2.quest = 73
windmillBreak2.result = DONE
loganWindmillBreak.addDialog(windmillBreak2, Logan)

def windmillBreak3 = [id:3]
windmillBreak3.npctext = "You won't be laughing when I put my boot in your backside and throw you in the lake for Bessie."
windmillBreak3.result = DONE
loganWindmillBreak.addDialog(windmillBreak3, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Windmill - Quest active                                                                                                       
//------------------------------------------------------------------------------------------------------------------------------
def loganWindmillActive = Logan.createConversation("loganWindmillActive", true, "QuestStarted_73:2")

def windmillActive1 = [id:1]
windmillActive1.npctext = "No, no, no! You have to put it in the WINDMILL!"
windmillActive1.result = DONE
loganWindmillActive.addDialog(windmillActive1, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Windmill - Quest complete                                                                                                     
//------------------------------------------------------------------------------------------------------------------------------
def loganWindmillComplete = Logan.createConversation("loganWindmillComplete", true, "QuestStarted_73:3")
loganWindmillComplete.setUrgent(true)

def windmillComplete1 = [id:1]
windmillComplete1.npctext = "How'd it work, bub? Wait, don't tell me, perfectly."
windmillComplete1.options = []
windmillComplete1.options << [text:"Some problem solver you are. It didn't work at all!", result: 2]
windmillComplete1.options << [text:"I suppose you could say it worked... as a giftbox beacon!", result: 3]
loganWindmillComplete.addDialog(windmillComplete1, Logan)

def windmillComplete2 = [id:2]
windmillComplete2.npctext = "Uh... what do you mean?"
windmillComplete2.options = []
windmillComplete2.options << [text:"I mean it didn't repell the wolf pups at all! All it did was summon a bunch of gift boxes!", result: 4]
windmillComplete2.options << [text:"Uhg. It's not even worth explaining. Let's just finish up with your G-Team stories so I can move on.", result: 5]
loganWindmillComplete.addDialog(windmillComplete2, Logan)

def windmillComplete3 = [id:3]
windmillComplete3.npctext = "A what? What's that supposed to mean?"
windmillComplete3.options = []
windmillComplete3.options << [text:"I mean it didn't repell the wolf pups at all! All it did was summon a bunch of gift boxes!", result: 4]
windmillComplete3.options << [text:"Uhg. It's not even worth explaining. Let's just finish up with your G-Team stories so I can move on.", result: 5]
loganWindmillComplete.addDialog(windmillComplete3, Logan)

def windmillComplete4 = [id:4]
windmillComplete4.npctext = "Hah! Well, how do you like that? Are you sure you hooked everything up right?"
windmillComplete4.playertext = "Pretty sure!"
windmillComplete4.result = 6
loganWindmillComplete.addDialog(windmillComplete4, Logan)

def windmillComplete5 = [id:5]
windmillComplete5.npctext = "Bah. If that's your attitude, bub. I've got somethin' for you before you go."
windmillComplete5.flag = "Z06LoganExperimentsConvoCompleted"
windmillComplete5.result = DONE
windmillComplete5.exec = { event -> 
	event.player.updateQuest(73, "[NPC] Old Man Logan")
	event.player.updateQuest(253, "[NPC] Old Man Logan")
	event.player.removeMiniMapQuestActorName("[NPC] Old Man Logan")
	Logan.pushDialog(event.player, "ringGrant")
}
loganWindmillComplete.addDialog(windmillComplete5, Logan)

def windmillComplete6 = [id:6]
windmillComplete6.npctext = "Hrm. I guess we'd better try again."
windmillComplete6.playertext = "It's not even worth it. Let's just finish up with your G-Team stories so I can move on."
windmillComplete6.result = 5
loganWindmillComplete.addDialog(windmillComplete6, Logan)

//--------------------------------------------------------------
//RING GRANT CONVERSATION                                       
//--------------------------------------------------------------
def ringGrant = Logan.createConversation( "ringGrant", true, "Z06LoganExperimentsConvoCompleted", "!Z06FifthRingGrantReceived", "!Z06RingRewardInProgress" )

def grant1 = [id:1]
grant1.npctext = "What ring do you want?"
grant1.exec = { event ->
	player = event.player
	event.player.setQuestFlag( GLOBAL, "Z06RingRewardInProgress" )
	makeMainMenu( player )
}
grant1.result = DONE
ringGrant.addDialog( grant1, Logan )

//------------------------------------------------------------------------------------------------------------------------------
//Logan finished                                                                                                                
//------------------------------------------------------------------------------------------------------------------------------
def loganFinished = Logan.createConversation("loganFinished", true, "!QuestStarted_61:2", "QuestCompleted_73", "!Z6_Logan_Experiments", "QuestCompleted_330", "!Z06RingRewardInProgress")

def finished1 = [id:1]
finished1.npctext = "Hey, bub. Good to see you again."
finished1.playertext = "Hey Logan. Anything going on?"
finished1.result = 2
loganFinished.addDialog(finished1, Logan)

def finished2 = [id:2]
finished2.npctext = "Nah, nothing at all."
finished2.playertext = "Okay. Take care."
finished2.result = DONE
loganFinished.addDialog(finished2, Logan)

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 2
CLdecimal = 5

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["New Rings", "Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Debuffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "New Rings" ) {
			makeNewRingMenu( player )
		}
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Debuffs" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z06RingRewardInProgress" )
		}
	}
}

//====================================================
// NEW RINGS                                          
//====================================================
def makeNewRingMenu( player ) {
	titleString = "New Rings Menu"
	descripString = "These rings are new to the list this time."
	diffOptions = [ "Adrenaline", "Dervish", "Duct Tape", "Heavy Water Balloon", "Main Menu"  ]
	
	uiButtonMenu( player, "newRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Adrenaline" ) {
			makeAdrenalineMenu( player )
		}
		if( event.selection == "Dervish" ) {
			makeDervishMenu( player )
		}
		if( event.selection == "Duct Tape" ) {
			makeDuctTapeMenu( player )
		}
		if( event.selection == "Heavy Water Balloon" ) {
			makeHeavyWaterBalloonMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Dervish", "Hack", "Mantis", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Dervish" ) {
			makeDervishMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Mantis" ) {
			makeMantisMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Heavy Water Balloon", "Hornet Nest", "Hot Foot", "Hunter's Bow", "Shuriken", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Heavy Water Balloon" ) {
			makeHeavyWaterBalloonMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Hunter's Bow" ) {
			makeHuntersBowMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Duct Tape", "Quicksand", "Scaredy Cat", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Duct Tape" ) {
			makeDuctTapeMenu( player )
		}
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Pot Lid", "Rock Armor", "Teflon Spray", "Turtle", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Rock Armor" ) {
			makeRockArmorMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Divinity", "Healing Halo", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Divinity" ) {
			makeDivinityMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Fleet Feet", "Ghost", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Fleet Feet" ) {
			makeFleetFeetMenu( player )
		}
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Adrenaline" ) {
			makeAdrenalineMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.setQuestFlag( GLOBAL, "Z06FifthRingGrantReceived" )
	Logan.pushDialog(event.actor, "toCaruthers")
}

//------------------------------------------------------------------------------------------------------------------------------
//Caruthers Grant Dialog                                                                                                        
//------------------------------------------------------------------------------------------------------------------------------
def toCaruthers = Logan.createConversation("toCaruthers", true, "!QuestStarted_61:2", "QuestCompleted_73", "Z06FifthRingGrantReceived", "!QuestStarted_97", "!QuestCompleted_97", "!QuestStarted_330", "!QuestCompleted_330")

def toCaruthers1 = [id:1]
toCaruthers1.npctext = "Well, bub, you're all set here. I have to say, you were a help."
toCaruthers1.playertext = "Thanks, Logan. So, uh, where do I go now?"
toCaruthers1.result = 2
toCaruthers.addDialog(toCaruthers1, Logan)

def toCaruthers2 = [id:2]
toCaruthers2.npctext = "Come on, bub, do I have to do everything for you?"
toCaruthers2.playertext = "Well, you just seem to know what's going on. I figure you can point me in the right direction."
toCaruthers2.result = 3
toCaruthers.addDialog(toCaruthers2, Logan)

def toCaruthers3 = [id:3]
toCaruthers3.npctext = "What? Maybe I used to know what was going on but I've been out of the loop since leaving the G-Team, unless you wanna know about bait! HAH!"
toCaruthers3.playertext = "Well, no, I was more interested in learning about the source of the Animated. Not really interested in bait at all."
toCaruthers3.result = 4
toCaruthers.addDialog(toCaruthers3, Logan)

def toCaruthers4 = [id:4]
toCaruthers4.npctext = "Alright, ya puff, let me think a minute."
toCaruthers4.result = 5
toCaruthers.addDialog(toCaruthers4, Logan)

def toCaruthers5 = [id:5]
toCaruthers5.npctext = "...Mrrrooooff..."
toCaruthers5.result = 6
toCaruthers.addDialog(toCaruthers5, Logan)

def toCaruthers6 = [id:6]
toCaruthers6.npctext = "...Hrrrrrruff..."
toCaruthers6.playertext = "LOL, what are you doing?"
toCaruthers6.result = 7
toCaruthers.addDialog(toCaruthers6, Logan)

def toCaruthers7 = [id:7]
toCaruthers7.npctext = "I'm concentrating. What's it to you, bub?"
toCaruthers7.playertext = "Nothing, Logan. Nothing. Sorry, please continue."
toCaruthers7.result = 8
toCaruthers.addDialog(toCaruthers7, Logan)

def toCaruthers8 = [id:8]
toCaruthers8.npctext = "That's more like it. Right, there's one thing you might look into. I did hear about some new group calling themselves the 'Gaians in Black' who fancy themselves the modern day G-Team."
toCaruthers8.result = 9
toCaruthers.addDialog(toCaruthers8, Logan)

def toCaruthers9 = [id:9]
toCaruthers9.npctext = "Rumor has it one of them has set up near the Old Aqueduct and is investigating the Star Portal left behind by the Zurg."
toCaruthers9.playertext = "Wait, how does that help me learn about the Animated?"
toCaruthers9.result = 10
toCaruthers.addDialog(toCaruthers9, Logan)

def toCaruthers10 = [id:10]
toCaruthers10.npctext = "Rumor has it the GiB are connected the Zurg portal to the Animated, but I'm not sure how, exactly. But, it's a start, and the only lead I have for you."
toCaruthers10.options = []
toCaruthers10.options << [text:"Well, I suppose a soft lead is better than no lead. I'll check it out. Who am I looking for?", result: 11]
toCaruthers10.options << [text:"I think I'm going to try to track down a better lead. Cya, Logan.", result: 12]
toCaruthers.addDialog(toCaruthers10, Logan)

def toCaruthers11 = [id:11]
toCaruthers11.npctext = "Puff calls himself a 'Concerned Citizen.' What a name, eh? You'll find him all dressed in black, imagine that, up by the Aqueduct."
toCaruthers11.exec = { event ->
	event.player.addMiniMapQuestActorName("Agent Caruthers-VQS")
}
toCaruthers11.quest = 330
toCaruthers11.result = DONE
toCaruthers.addDialog(toCaruthers11, Logan)

def toCaruthers12 = [id:12]
toCaruthers12.npctext = "Good luck, bub."
toCaruthers12.flag = "Z06_Logan_Caruthers_Break"
toCaruthers12.result = DONE
toCaruthers.addDialog(toCaruthers12, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Caruthers Break Dialog                                                                                                        
//------------------------------------------------------------------------------------------------------------------------------
def toCaruthersBreak = Logan.createConversation("toCaruthersBreak", true, "Z06_Logan_Caruthers_Break", "!QuestStarted_97", "!QuestCompleted_97")

def toCaruthersBreak1 = [id:1]
toCaruthersBreak1.npctext = "How goes the hunt for leads?"
toCaruthersBreak1.playertext = "Oh, uhm, pretty good."
toCaruthersBreak1.result = 2
toCaruthersBreak.addDialog(toCaruthersBreak1, Logan)

def toCaruthersBreak2 = [id:2]
toCaruthersBreak2.npctext = "So I guess you're still not interested in the GiB then."
toCaruthersBreak2.options = []
toCaruthersBreak2.options << [text:"Well...", result: 3]
toCaruthersBreak2.options << [text:"Nope.", result: 4]
toCaruthersBreak.addDialog(toCaruthersBreak2, Logan)

def toCaruthersBreak3 = [id:3]
toCaruthersBreak3.npctext = "Other leads not panning out so well? You could still go talk to the GiB agent."
toCaruthersBreak3.playertext = "Yeah, I guess I'd better. What was his name again?"
toCaruthersBreak3.result = 5
toCaruthersBreak.addDialog(toCaruthersBreak3, Logan)

def toCaruthersBreak5 = [id:5]
toCaruthersBreak5.npctext = "The puff just calls himself a 'Concerned Citizen.' You'll find him dressed in black and near the Aqueduct."
toCaruthersBreak5.exec = { event ->
	event.player.addMiniMapQuestActorName("Agent Caruthers-VQS")
}
toCaruthersBreak5.quest = 330
toCaruthersBreak5.flag = "!Z06_Logan_Caruthers_Break"
toCaruthersBreak5.result = DONE
toCaruthersBreak.addDialog(toCaruthersBreak5, Logan)

//------------------------------------------------------------------------------------------------------------------------------
//Caruthers Break Dialog                                                                                                        
//------------------------------------------------------------------------------------------------------------------------------
def toCaruthersActive = Logan.createConversation("toCaruthersActive", true, "QuestStarted_330")

def toCaruthersActive1 = [id:1]
toCaruthersActive1.npctext = "Remember, bub: Concerned Citizen, wearing black, near the Old Aqueduct."
toCaruthersActive1.playertext = "Ohhh, right. Thanks, Logan, I'd forgotten."
toCaruthersActive1.result = DONE
toCaruthersActive.addDialog(toCaruthersActive1, Logan)