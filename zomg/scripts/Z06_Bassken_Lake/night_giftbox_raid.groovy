import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// THE NIGHT PATROL!                        
//------------------------------------------
//Giftboxes spawn each night, once an hour and ravage the countryside for 15 minutes before disposing of themselves

def nightPatrolSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() < 5.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 5.0 or lower to attack the Night Patrol." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//The Night Patrol
nightPatrol = myRooms.BASSKEN_206.spawnSpawner( "nightPatrol", "flying_giftbox", 5 )
nightPatrol.setPos(185, 150)
nightPatrol.setWaitTime( 1, 2 )
nightPatrol.setHomeTetherForChildren( 2000 )
nightPatrol.setHateRadiusForChildren( 2000 )
nightPatrol.setBaseSpeed( 180 )
nightPatrol.setMiniEventSpec( nightPatrolSpec )
nightPatrol.setChildrenToFollow( nightPatrol )
nightPatrol.setMonsterLevelForChildren( 4.8 )
nightPatrol.addPatrolPointForSpawner( "BASSKEN_206", 690, 270, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_206", 980, 570, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_306", 890, 190, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_307", 250, 390, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_307", 490, 260, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_207", 250, 280, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_107", 300, 330, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_107", 90, 200, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_106", 890, 360, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_206", 200, 160, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_106", 75, 470, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_105", 460, 200, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_104", 570, 300, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_4", 550, 450, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_5", 470, 370, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_105", 840, 280, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_106", 75, 470, 0)
nightPatrol.addPatrolPointForSpawner( "BASSKEN_206", 200, 160, 0)
nightPatrol.startPatrol()

night1 = null
night2 = null
night3 = null
night4 = null
night5 = null

def checkForNight() {
//	if( true ) { //TESTING ONLY!!!
	if( ( gst() > 2100 && gst() <=2359 ) || ( gst() >= 0 && gst() < 300 ) ) { //if it's dark (9pm - 3am) then ...start the raids!
		if( night1 == null || night1.isDead() ) { night1 = nightPatrol.forceSpawnNow() }
		if( night2 == null || night2.isDead() ) { night2 = nightPatrol.forceSpawnNow() }
		if( night3 == null || night3.isDead() ) { night3 = nightPatrol.forceSpawnNow() }
		if( night4 == null || night4.isDead() ) { night4 = nightPatrol.forceSpawnNow() }
		if( night5 == null || night5.isDead() ) { night5 = nightPatrol.forceSpawnNow() }
		shoutItOut = true
		nightShout()
		//check for a respawn between 25-45 minutes from initial spawn
		myManager.schedule( random( 1500, 2700 ) ) { checkForNight() }
	} else { //if the check came out not in the "dark of night", then check again in five real-time minutes to see if it's dark again yet
		shoutItOut = false
		if( night1 != null ) { if( !night1.isDead() ){ night1.dispose() } }
		if( night1 != null ) { if( !night2.isDead() ){ night2.dispose() } }
		if( night1 != null ) { if( !night3.isDead() ){ night3.dispose() } }
		if( night1 != null ) { if( !night4.isDead() ){ night4.dispose() } }
		if( night1 != null ) { if( !night5.isDead() ){ night5.dispose() } }
		myManager.schedule(300) { checkForNight() }
	}
}
		
def nightShout() {
	if( shoutItOut == true ) {
		myManager.schedule(1){ if( !night1.isDead() ){ night1.say( "We are...") } }
		myManager.schedule(2){ if( !night2.isDead() ){ night2.say("...endlessly...") } }
		myManager.schedule(3){ if( !night3.isDead() ){ night3.say("...cavernously...") } }
		myManager.schedule(4){ if( !night4.isDead() ){ night4.say("...achingly...") } }
		myManager.schedule(5){ if( !night5.isDead() ){ night5.say("...EMPTY!") } }
		myManager.schedule(8){
			if( !night1.isDead() ){ night1.say("FILL THE BOX!") }
			if( !night1.isDead() ){ night2.say("FILL THE BOX!") }
			if( !night1.isDead() ){ night3.say("FILL THE BOX!") }
			if( !night1.isDead() ){ night4.say("FILL THE BOX!") }
			if( !night1.isDead() ){ night5.say("FILL THE BOX!") }
		}
		myManager.schedule( 15 ) { nightShout() }
	}
}
	

//INITIAL LOGIC

nightPatrol.stopSpawning()

checkForNight()
