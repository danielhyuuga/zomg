import com.gaiaonline.mmo.battle.script.*;

MaxCL = 5.1

carrionFluffSet = [] as Set
outlawPupSet = [] as Set

//====================================
//Death listener for CarrionFluffs    
//====================================
def basskenCarrionFluffDeathScheduler() {
	myRooms.values().each() {
		it.getActorList().each() {
			if(isMonster( it ) && it.getMonsterType() == "grass_fluff_rabid" && !carrionFluffSet.contains( it ) ) {
				//add the monster to the list
				carrionFluffSet << it
				//check for quest credit when the monster is killed
				runOnDeath( it ) { event -> checkForCarrionFluffUpdate( event ); carrionFluffSet.remove(event.actor) }
			}
		}
	}
	//check all monsters again in one second to ensure the list is populated with currently available quest monsters
	myManager.schedule(1) { basskenCarrionFluffDeathScheduler() }
}

def synchronized checkForCarrionFluffUpdate( event ) {
	event.actor.getHated().each() {
		//if they're on the proper quest, then check for quest credit
		if( isPlayer(it) && it.isOnQuest( 282, 2 ) ) {
			if( it.getConLevel() < MaxCL ) {
				//increment the counter
				it.addToPlayerVar( "Z06_CarrionFluffKillTotal", 1 )
				
				//display it to the player
				it.centerPrint( "Carrion Flower Fluff ${it.getPlayerVar( "Z06_CarrionFluffKillTotal").toInteger()}/20" )
				
				//if they're done, then update the quest
				if( it.getPlayerVar( "Z06_CarrionFluffKillTotal" ) == 20 ) {
					it.updateQuest( 282, "Ryan-VQS" )
					it.deletePlayerVar( "Z06_CarrionFluffKillTotal" )
				}
			} else {
				it.centerPrint( "You must be level 5.0 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

basskenCarrionFluffDeathScheduler()

//====================================
//Death listener for Outlaw Pups      
//====================================
// PUNISH ZE WOLVES
def basskenOutlawPupDeathScheduler() {
	myRooms.values().each() {
		it.getActorList().each() {
			if(isMonster( it ) && ( it.getMonsterType() == "outlaw_pup" || it.getMonsterType() == "outlaw_pup_LT" ) && !outlawPupSet.contains( it ) ) {
				//add the monster to the list
				outlawPupSet << it
				//check for quest credit when the monster is killed
				runOnDeath( it ) { event -> checkForOutlawPupUpdate( event ); outlawPupSet.remove(event.actor) }
			}
		}
	}
	//check all monsters again in one second to ensure the list is populated with currently available quest monsters
	myManager.schedule(1) { basskenOutlawPupDeathScheduler() }
}

def synchronized checkForOutlawPupUpdate( event ) {
	event.actor.getHated().each() {
		//if they're on the proper quest, then check for quest credit
		if( isPlayer(it) && it.isOnQuest( 281, 2 ) ) {
			if( it.getConLevel() < MaxCL ) {
				//increment the counter
				it.addToPlayerVar( "Z06OutlawPupKillTotal", 1 )
				
				//display it to the player
				it.centerPrint( "Outlaw Pups and Wolves ${it.getPlayerVar( "Z06OutlawPupKillTotal").toInteger()}/20" )
				
				//if they're done, then update the quest
				if( it.getPlayerVar( "Z06OutlawPupKillTotal" ) == 20 ) {
					it.updateQuest( 281, "Gustav-VQS" )
					it.deletePlayerVar( "Z06OutlawPupKillTotal" )
				}
			} else {
				it.centerPrint( "You must be level 5.0 or lower for this task." ) 
				it.centerPrint( "Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily." )
			}
		}
	}
}

basskenOutlawPupDeathScheduler()