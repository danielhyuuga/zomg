import com.gaiaonline.mmo.battle.script.*;

def Jesse = spawnNPC("Jesse-VQS", myRooms.BASSKEN_401, 410, 540 )
Jesse.setDisplayName( "Jesse" )

//---------------------------------------------------------
// Default Conversation - Jesse (inside the house)         
//---------------------------------------------------------

def JesseDefault = Jesse.createConversation("JesseDefault", true, "!QuestStarted_70:2", "!QuestCompleted_70")

def Jesse1 = [id:1]
Jesse1.npctext = "G'wan! Get outta here! No trespassers allowed!"
Jesse1.playertext = "Why? What's wrong?"
Jesse1.result = 2
JesseDefault.addDialog(Jesse1, Jesse)

def Jesse2 = [id:2]
Jesse2.npctext = "Are you kidding?!? The wolves and saws are EVERYWHERE. I'm not comin' out 'til Blaze and my pa come back!"
Jesse2.playertext = "Are you alone in there?"
Jesse2.result = 3
JesseDefault.addDialog(Jesse2, Jesse)

def Jesse3 = [id:3]
Jesse3.npctext = "Ummm...alone? Me?!? N-no! My dog...errr, Killer...is in here too! He's an attack Mastiff and he'll eat you like a hamburger if you open that door!"
Jesse3.playertext = "You have a dog named 'Killer'? Okay...let's hear it bark."
Jesse3.result = 4
JesseDefault.addDialog(Jesse3, Jesse)

def Jesse4 = [id:4]
Jesse4.npctext = "Bark? Ummm...I can't! Killer is a trained attack dog and his...err...tonsils were removed so he could sneak up on things before biting them!"
Jesse4.playertext = "Tonsils? Heh. Well...you keep the door closed then and I hope your pa comes home soon."
Jesse4.result = 5
JesseDefault.addDialog(Jesse4, Jesse)

def Jesse5 = [id:5]
Jesse5.npctext = "yeah...me too..."
Jesse5.playertext = "What's that? Did you say something?"
Jesse5.result = 6
JesseDefault.addDialog(Jesse5, Jesse)

def Jesse6 = [id:6]
Jesse6.npctext = "Nope! Nothing! Now you git before I open the door and let Killer chew your legs off!"
Jesse6.playertext = "hehehe. Okay. I'm 'gitting'."
Jesse6.result = DONE
JesseDefault.addDialog(Jesse6, Jesse)

//---------------------------------------------------------
// Logan's Check Up Conversation                           
// For when Logan sends the players to the house to check  
// up on Jesse.                                            
//---------------------------------------------------------

def LoganCheckUp = Jesse.createConversation("LoganCheckUp", true, "QuestStarted_70:2")

def CheckUp1 = [id:1]
CheckUp1.playertext = "Jesse? Can we talk, Jesse?"
CheckUp1.result = 2
LoganCheckUp.addDialog(CheckUp1, Jesse)

def CheckUp2 = [id:2]
CheckUp2.npctext = "Who are you? What are you doing here? Go away or I'll sic my dog on you!"
CheckUp2.options = []
CheckUp2.options << [text:"Woah! Calm down, Logan sent me. I'm friendly!", result:3]
CheckUp2.options << [text:"Don't be silly. I know your father left you here alone. No dog, just you!", result:4]
LoganCheckUp.addDialog(CheckUp2, Jesse)

def CheckUp3 = [id:3]
CheckUp3.npctext = "How do I know Logan sent you?"
CheckUp3.options = []
CheckUp3.options << [text:"How else would I know you're home alone?", result:5]
CheckUp3.options << [text:"Does it really matter? You obviously need my help.", result:6]
CheckUp3.options << [text:"Hey, bub, just open'er up ya puff.", result:14]
LoganCheckUp.addDialog(CheckUp3, Jesse)

def CheckUp4 = [id:4]
CheckUp4.npctext = "Uh... no he didn't. Killer's here right now!"
CheckUp4.options = []
CheckUp4.options << [text:"Don't worry, Logan sent me to help.", result:3]
CheckUp4.options << [text:"Okay, prove it. Make Killer bark or something.", result:7]
LoganCheckUp.addDialog(CheckUp4, Jesse)

def CheckUp5 = [id:5]
CheckUp5.npctext = "Er, okay that's a good point. So what do you want?"
CheckUp5.options = []
CheckUp5.options << [text:"Just to know how you're doing.", result:8]
CheckUp5.options << [text:"I guess I should ask you the same thing. Logan wants me to find out how you are and help you if you need any.", result: 9]
LoganCheckUp.addDialog(CheckUp5, Jesse)

def CheckUp6 = [id:6]
CheckUp6.npctext = "Well... I guess that's true. I'm still not coming out. Not with all these wolves around."
CheckUp6.options = []
CheckUp6.options << [text:"Wolves? I can get rid of those.", result:10]
CheckUp6.options << [text:"I'm afraid of wolves too. What should we do about them?", result:11]
LoganCheckUp.addDialog(CheckUp6, Jesse)

def CheckUp7 = [id:7]
CheckUp7.npctext = "I will! Bark, Killer. Show %p how mean you are! *gruffly* BARK BARK!"
CheckUp7.playertext = "That was you!"
CheckUp7.result = 13
LoganCheckUp.addDialog(CheckUp7, Jesse)

def CheckUp8 = [id:8]
CheckUp8.npctext = "I'd be doing a lot better if you got rid of these wolves."
CheckUp8.options = []
CheckUp8.options << [text:"I can do that.", result:10]
CheckUp8.options << [text:"Those things terrify me! How am I supposed to get rid of them?", result:11]
LoganCheckUp.addDialog(CheckUp8, Jesse)

def CheckUp9 = [id:9]
CheckUp9.npctext = "Hmm, how about getting these wolves to leave me alone? The howling is unbearable."
CheckUp9.options = []
CheckUp9.options << [text:"I can handle the wolves.", result: 10]
CheckUp9.options << [text:"Heh... those things are ferocious. How am I supposed to do anything about them?", result:11]
LoganCheckUp.addDialog(CheckUp9, Jesse)

def CheckUp10 = [id:10]
CheckUp10.npctext = "How are you going to manage that?"
CheckUp10.playertext = "I'm sure Logan and I can come up with a plan."
CheckUp10.result = 12
LoganCheckUp.addDialog(CheckUp10, Jesse)

def CheckUp11 = [id:11]
CheckUp11.npctext = "I don't know, maybe Logan has a plan?"
CheckUp11.playertext = "Oh! Good idea. I'll go ask him."
CheckUp11.result = 12
LoganCheckUp.addDialog(CheckUp11, Jesse)

def CheckUp12 = [id:12]
CheckUp12.npctext = "Okay, let me know when you and Logan have dealt with the wolves."
CheckUp12.quest = 70
CheckUp12.exec = { event -> 
	event.player.removeMiniMapQuestActorName("Jesse-VQS")
}
CheckUp12.result = DONE
LoganCheckUp.addDialog(CheckUp12, Jesse)

def CheckUp13 = [id:13]
CheckUp13.npctext = "Bah! Okay, you caught me. I'm still not coming out, though. The wolves'll get me!"
CheckUp13.options = []
CheckUp13.options << [text:"Yeesh. I can get rid of the wolves.", result: 10]
CheckUp13.options << [text:"Well, I'll be glad to help... any idea how to get rid of them?", result: 11]
LoganCheckUp.addDialog(CheckUp13, Jesse)

def CheckUp14 = [id:14]
CheckUp14.npctext = "LOL! That was a great impression of Logan! So why did Logan send you here?"
CheckUp14.playertext = "To find out how you are and if you need any help."
CheckUp14.result = 8
LoganCheckUp.addDialog(CheckUp14, Jesse)

//---------------------------------------------------------
// AFTER JESSE TRUSTS YOU                                  
//---------------------------------------------------------

def jesseTrust = Jesse.createConversation("jesseTrust", true, "QuestCompleted_70")

def trust1 = [id:1]
trust1.playertext = "Are you still in there, Jesse?"
trust1.result = 2
jesseTrust.addDialog( trust1, Jesse  )

def trust2 = [id:2]
trust2.npctext = "Yup. Still here! So is my dog, Rex!"
trust2.playertext = "Rex? Wait a sec...I thought your dog's name was Killer!"
trust2.result = 3
jesseTrust.addDialog( trust2, Jesse )

def trust3 = [id:3]
trust3.npctext = "Err...ummm...it is! Rex is his *real* name, but we call him Killer because...err...he likes to kill things real dead-like!"
trust3.playertext = "Right. That sounds totally believable, Jesse. Are you ever going to come out of there?"
trust3.result = 4
jesseTrust.addDialog( trust3, Jesse )

def trust4 = [id:4]
trust4.npctext = "No way. Not until either Blaze or my pa comes home."
trust4.playertext = "Oh yeah?"
trust4.options = []
trust4.options << [text: "Who is this 'Blaze' you mention?", result: 5]
trust4.options << [text: "Where's your pa?", result: 10]
trust4.options << [text: "Okay...good luck in there, Jesse. You be safe and I'll talk to you later!", result: 16]
jesseTrust.addDialog( trust4, Jesse )

def trust5 = [id:5]
trust5.npctext = "Blaze is this older girl that helps my pa out on his expedition stuff."
trust5.playertext = "Oh yeah? What does she do?"
trust5.result = 6
jesseTrust.addDialog( trust5, Jesse )

def trust6 = [id:6]
trust6.npctext = "Mostly paperwork and hauling stuff around, but once in a while, pa lets Blaze come with him on his trips, but she won't go out on the water much. She gets awful seasick when she does."
trust6.playertext = "Is that all she does?"
trust6.result = 7
jesseTrust.addDialog( trust6, Jesse )

def trust7 = [id:7]
trust7.npctext = "Well...sometimes pa makes her sit for me when he goes on long trips, but that's silly. I don't need no one to watch over ME!"
trust7.playertext = "Except Rex...err, Killer...there, right?"
trust7.result = 8
jesseTrust.addDialog( trust7, Jesse )

def trust8 = [id:8]
trust8.npctext = "What? Oh. My dog! Right! Good boy! Sit! Oh yeah...Blaze likes the jungle...a LOT. When pa doesn't have her working on stuff, she spends a heap of time over there. She says she's trying to find remnants of the old Otami folks that lived there a long time ago. But personally, I think she just likes pretending she's an adventurer."
trust8.playertext = "So you think she's over there now?"
trust8.result = 9
jesseTrust.addDialog( trust8, Jesse )

def trust9 = [id:9]
trust9.npctext = "Probably. Unless she's with pa...but I doubt it. He didn't mention anything, so he probably figgers that Blaze is here with me now. Is that what you wanted to know?"
trust9.playertext = "Yup. Mostly."
trust9.options = []
trust9.options << [text: "But...what about your pa? Where is he?", result: 10]
trust9.options << [text: "See ya later, Jesse! Thanks! Stay safe!", result: 16]
jesseTrust.addDialog( trust9, Jesse )

def trust10 = [id:10]
trust10.npctext = "Who knows? Pa owns 'Deep Blue Ventures', our undersea research company. He could be anywhere now."
trust10.playertext = "Really? What does your pa do?"
trust10.result = 11
jesseTrust.addDialog( trust10, Jesse )

def trust11 = [id:11]
trust11.npctext = "He's an itchyologist!"
trust11.playertext = "Your pa studies itches?"
trust11.result = 12
jesseTrust.addDialog( trust11, Jesse )

def trust12 = [id:12]
trust12.npctext = "What? No! He studies fishes and stuff under the water. He even has one of them round bathtubs and stuff."
trust12.playertext = "Round...? Do you mean a bathysphere?"
trust12.result = 13
jesseTrust.addDialog( trust12, Jesse )

def trust13 = [id:13]
trust13.npctext = "Yeah! That's it! He's out chasing some undersea lights or something he saw in the water recently. But most of our gear is locked down over at the boardwalk, and until Cap'n Marina lets us get our stuff, he's only got the gear we had here at the house."
trust13.options = []
trust13.options << [text: "What undersea lights?", result: 18]
trust13.options << [text: "Who is Captain Marina?", result: 14]
trust13.result = DONE
jesseTrust.addDialog( trust13, Jesse )

def trust14 = [id:14]
trust14.npctext = "You don't know who the Pirate Queen is? Sheesh. Yeah...she knows pa from way back. Logan spilled the beans to me once that she's Blaze's ma, too. But I don't know if Logan was pulling my leg or not."
trust14.playertext = "Whoa!"
trust14.result = 15
jesseTrust.addDialog( trust14, Jesse )

def trust15 = [id:15]
trust15.npctext = "Nah. It ain't no big thing. So Blaze is a pirate's daughter. Big hairy deal. I'm not jealous. My dad's an itchyologist and I live in a house with a water wheel! That's better than a stupid pirate ma any day!"
trust15.playertext = "Well..."
trust15.options = []
trust15.options << [text: "What about those lights your Pa saw?", result: 18]
trust15.options << [text: "How is Blaze related to you and your pa?", result: 5]
trust15.options << [text: "Thanks for talking to me, Jesse. You stay safe now!", result: 16]
jesseTrust.addDialog( trust15, Jesse )

def trust16 = [id:16]
trust16.npctext = "I know, I know. Everyone says that. I'll be fine!"
trust16.playertext = "Of course you will, with that big ferocious dog to guard you!"
trust16.result = 17
jesseTrust.addDialog( trust16, Jesse )

def trust17 = [id:17]
trust17.npctext = "Yeah. Dog. Ferocious. Lucky me."
trust17.result = DONE
jesseTrust.addDialog( trust17, Jesse )

def trust18 = [id:18]
trust18.npctext = "Pa's been trying to get folks to believe him about strange stuff he's been seeing under the surface for months now, but no one would give him the time of day."
trust18.playertext = "What sorts of things did he see?"
trust18.result = 19
jesseTrust.addDialog( trust18, Jesse )

def trust19 = [id:19]
trust19.npctext = "When the Animated started appearing here recently, he got really excited. He said he'd been seeing stuff like that in the water for a long time, and now that it was confirmed, he was going to check out those lights."
trust19.playertext = "What were the lights like?"
trust19.result = 20
jesseTrust.addDialog( trust19, Jesse )

def trust20 = [id:20]
trust20.npctext = "He said they were too deep for his light submersible to reach. He needed to take the deep gear to go look at 'em, and since that gear is gone now, I'm guessing that's where he went."
trust20.playertext = "So who is this Captain Marina?"
trust20.result = 14
jesseTrust.addDialog( trust20, Jesse )