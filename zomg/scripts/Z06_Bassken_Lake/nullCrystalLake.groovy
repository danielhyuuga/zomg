
//--------------------------------------------
// WARP CRYSTAL TO THE NULL CHAMBER           
//--------------------------------------------

toNullChamber = makeSwitch( "toNullChamber", myRooms.BASSKEN_304, 400, 520 )
toNullChamber.unlock()
toNullChamber.setRange( 200 )
toNullChamber.setMouseoverText("To the Null Chamber")

onClickCrystal = new Object()

def goToNullChamber = { event ->
	synchronized( onClickCrystal ) {
		toNullChamber.off()
		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "nullChamber1_1", 1055, 660, 6 )
		event.actor.setQuestFlag( GLOBAL, "Z21LakeWarpOkay" )
		event.actor.setQuestFlag( GLOBAL, "Z21ComingFromOutside" )
	}
}

toNullChamber.whenOn( goToNullChamber )
