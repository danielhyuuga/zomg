import com.gaiaonline.mmo.battle.script.*;

def spawner1 = myRooms.BASSKEN_201.spawnSpawner("spawner1", "grass_fluff", 3) 
spawner1.setPos(720, 590)
spawner1.setWanderBehaviorForChildren( 50, 150, 2, 5, 400)
spawner1.setWaitTime( 50, 70 )
spawner1.setMonsterLevelForChildren( 4.8 )


//STARTUP LOGIC
spawner1.spawnAllNow()