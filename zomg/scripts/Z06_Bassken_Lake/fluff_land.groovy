import com.gaiaonline.mmo.battle.script.*;

carrionSayings = [ "*grumble*", "*grouse*", "*cough* *cough* *spit*.", "Blech.", "Blargh!", "Ergh.", "*censored*", "Flurgle" ]

fluffSayings = [ "Hee hee!", "Whee!", "Yippee!", "Yay!", "*chortle*", "*giggle*", "*blush*", "Weehah!" ]

minTrashTime = 120 //min time until a trash can respawns after being destroyed
maxTrashTime = 240 //max time until trash respawns
trashHealth = 200 //use this to conform to the max health set for the trash can monster
grassPortion = 100 //use this to adjust the amount of grass fluffs vs carrion fluffs spawned at each trash can

//SCRIPT LOGIC
// * Mother Fluffs are zone hazards that spawn periodically, along with their Guardian patrols. They have large CFH radii and ALL fluffs in the zone are allied to the Mothers. When they call, EVERYONE comes a'runnin'.
// * Put trash cans around the zone. Beating on the trash cans can trigger a fluff spawn. 4-5 hits on a can should destroy it and fluffs can't be summoned again until you hit find a new can to hit.

//Logic for determining which type of spawn you'll get
//============================================
// TRASH CAN 506                              
//============================================

trash506 = myRooms.BASSKEN_506.spawnSpawner( "trash506", "trashcan", 1 )
trash506.setPos( 430, 400 )
trash506.setGuardPostForChildren( "BASSKEN_506", 430, 400, 45 )
trash506.setMonsterLevelForChildren( 4.2 )
trash506.stopSpawning()

fluff506 = myRooms.BASSKEN_506.spawnSpawner( "fluff506", "grass_fluff", 5)
fluff506.setPos( 175, 320 )
fluff506.setInitialMoveForChildren( "BASSKEN_506", 350, 480 )
fluff506.setHome( "BASSKEN_506", 350, 480 )
fluff506.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff506.setMonsterLevelForChildren( 4.2 )
fluff506.stopSpawning()

carrion506 = myRooms.BASSKEN_506.spawnSpawner( "carrion506", "grass_fluff_rabid", 5)
carrion506.setPos( 530, 530 )
carrion506.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion506.setMonsterLevelForChildren( 4.2 )
carrion506.stopSpawning()


//playerList for BASSKEN_506
playerList506 = []

myManager.onEnter( myRooms.BASSKEN_506 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList506 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_506 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList506.remove( event.actor )
	}
}

eventList506 = []

resetTrash506() //Set up the trash can the first time

def resetTrash506() {
	if( eventList506.isEmpty() || eventList506.size() < 5 ) {
		eventList506 << ( ( random( trashHealth ) * 4.2 ) / ( trashHealth * 4.2 ) * 100 ).intValue()
		resetTrash506()
	} else {
		spawnTrash506()
	}
}

def spawnTrash506() {
	trashcan506 = trash506.forceSpawnNow()
	trashcan506.setDisplayName( "...a mug..." )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan506 ) { event ->
//		println "**** trashcan506's MAX HP = ${ trashcan506.getMaxHP() } ****"
//		println "**** OLD HEALTH = ${event.getOldHP()} and NEW HEALTH = ${event.getNewHP()}****"
//		println "**** Old % of MAX HEALTH = ${ ( event.getOldHP() / trashcan506.getMaxHP() * 100 ).intValue() }****"
//		println "**** New % of MAX HEALTH = ${ ( event.getNewHP() / trashcan506.getMaxHP() * 100 ).intValue() }****"
		trashcan506.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList506.get(0) ) && event.isDecrease() ) {
			determineSpawnType506()
			if( spawnGrassFluff506 == true ) { fluff506() } else { carrion506() }
		}
		if( event.didTransition( eventList506.get(1) ) && event.isDecrease() ) {
			determineSpawnType506()
			if( spawnGrassFluff506 == true ) { fluff506() } else { carrion506() }
		}
		if( event.didTransition( eventList506.get(2) ) && event.isDecrease() ) {
			determineSpawnType506()
			if( spawnGrassFluff506 == true ) { fluff506() } else { carrion506() }
		}
		if( event.didTransition( eventList506.get(3) ) && event.isDecrease() ) {
			determineSpawnType506()
			if( spawnGrassFluff506 == true ) { fluff506() } else { carrion506() }
		}
		if( event.didTransition( eventList506.get(4) ) && event.isDecrease() ) {
			determineSpawnType506()
			if( spawnGrassFluff506 == true ) { fluff506() } else { carrion506() }
		}
	}
	runOnDeath( trashcan506, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch506() } } )
}

def determineSpawnType506() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff506 = true
	} else {
		spawnGrassFluff506 = false
	}
}

def fluff506() {
	fluff = fluff506.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion506() {
	carrion = carrion506.forceSpawnNow()
	carrion.addHate( random( playerList506 ), 20 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch506() {
	if( fluff506.spawnsInUse() == 0 && carrion506.spawnsInUse() == 0 ) {
		resetTrash506()
	} else {
		myManager.schedule( 5 ) { roomWatch506() }
	}
}

//============================================
// TRASH CAN 505                              
//============================================

trash505 = myRooms.BASSKEN_505.spawnSpawner( "trash505", "trashcan", 1 )
trash505.setPos( 880, 140 )
trash505.setGuardPostForChildren( "BASSKEN_505", 880, 140, 45 )
trash505.setMonsterLevelForChildren( 4.3 )
trash505.stopSpawning()

fluff505 = myRooms.BASSKEN_505.spawnSpawner( "fluff505", "grass_fluff", 5)
fluff505.setPos( 725, 480 )
fluff505.setInitialMoveForChildren( "BASSKEN_505", 450, 540 )
fluff505.setHome( "BASSKEN_505", 450, 540 )
fluff505.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff505.setMonsterLevelForChildren( 4.3 )
fluff505.stopSpawning()

carrion505 = myRooms.BASSKEN_505.spawnSpawner( "carrion505", "grass_fluff_rabid", 5)
carrion505.setPos( 440, 220 )
carrion505.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion505.setMonsterLevelForChildren( 4.3 )
carrion505.stopSpawning()

//playerList for BASSKEN_505
playerList505 = []

myManager.onEnter( myRooms.BASSKEN_505 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList505 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_505 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList505.remove( event.actor )
	}
}

eventList505 = []

resetTrash505() //Set up the trash can the first time

def resetTrash505() {
	if( eventList505.isEmpty() || eventList505.size() < 5 ) {
		eventList505 << ( ( random( trashHealth ) * 4.3 ) / ( trashHealth * 4.3 ) * 100 ).intValue()
		resetTrash505()
	} else {
		spawnTrash505()
	}
}

def spawnTrash505() {
	trashcan505 = trash505.forceSpawnNow()
	trashcan505.setDisplayName( "For a Greener Tomorrow!" )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan505 ) { event ->	
		trashcan505.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList505.get(0) ) && event.isDecrease() ) {
			determineSpawnType505()
			if( spawnGrassFluff505 == true ) { fluff505() } else { carrion505() }
		}
		if( event.didTransition( eventList505.get(1) ) && event.isDecrease() ) {
			determineSpawnType505()
			if( spawnGrassFluff505 == true ) { fluff505() } else { carrion505() }
		}
		if( event.didTransition( eventList505.get(2) ) && event.isDecrease() ) {
			determineSpawnType505()
			if( spawnGrassFluff505 == true ) { fluff505() } else { carrion505() }
		}
		if( event.didTransition( eventList505.get(3) ) && event.isDecrease() ) {
			determineSpawnType505()
			if( spawnGrassFluff505 == true ) { fluff505() } else { carrion505() }
		}
		if( event.didTransition( eventList505.get(4) ) && event.isDecrease() ) {
			determineSpawnType505()
			if( spawnGrassFluff505 == true ) { fluff505() } else { carrion505() }
		}
	}
	runOnDeath( trashcan505, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch505() } } )
}

def determineSpawnType505() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff505 = true
	} else {
		spawnGrassFluff505 = false
	}
}

def fluff505() {
	fluff = fluff505.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion505() {
	carrion = carrion505.forceSpawnNow()
	carrion.addHate( random( playerList505 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch505() {
	if( fluff505.spawnsInUse() == 0 && carrion505.spawnsInUse() == 0 ) {
		resetTrash505()
	} else {
		myManager.schedule( 5 ) { roomWatch505() }
	}
}

//============================================
// TRASH CAN 405                              
//============================================

trash405 = myRooms.BASSKEN_405.spawnSpawner( "trash405", "trashcan", 1 )
trash405.setPos( 620, 315 )
trash405.setGuardPostForChildren( "BASSKEN_405", 620, 315, 45 )
trash405.setMonsterLevelForChildren( 4.4 )
trash405.stopSpawning()

fluff405 = myRooms.BASSKEN_405.spawnSpawner( "fluff405", "grass_fluff", 5)
fluff405.setPos( 320, 580 )
fluff405.setInitialMoveForChildren( "BASSKEN_405", 510, 590 )
fluff405.setHome( "BASSKEN_405", 510, 590 )
fluff405.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff405.setMonsterLevelForChildren( 4.4 )
fluff405.stopSpawning()

carrion405 = myRooms.BASSKEN_405.spawnSpawner( "carrion405", "grass_fluff_rabid", 5)
carrion405.setPos( 620, 40 )
carrion405.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion405.setMonsterLevelForChildren( 4.4 )
carrion405.stopSpawning()

//playerList for BASSKEN_405
playerList405 = []

myManager.onEnter( myRooms.BASSKEN_405 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList405 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_405 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList405.remove( event.actor )
	}
}

eventList405 = []

resetTrash405() //Set up the trash can the first time

def resetTrash405() {
	if( eventList405.isEmpty() || eventList405.size() < 5 ) {
		eventList405 << ( ( random( trashHealth ) * 4.4 ) / ( trashHealth * 4.4 ) * 100 ).intValue()
		resetTrash405()
	} else {
		spawnTrash405()
	}
}

def spawnTrash405() {
	trashcan405 = trash405.forceSpawnNow()
	trashcan405.setDisplayName( "Give a Hoot! Don't Pollute!" )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan405 ) { event ->	
		trashcan405.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList405.get(0) ) && event.isDecrease() ) {
			determineSpawnType405()
			if( spawnGrassFluff405 == true ) { fluff405() } else { carrion405() }
		}
		if( event.didTransition( eventList405.get(1) ) && event.isDecrease() ) {
			determineSpawnType405()
			if( spawnGrassFluff405 == true ) { fluff405() } else { carrion405() }
		}
		if( event.didTransition( eventList405.get(2) ) && event.isDecrease() ) {
			determineSpawnType405()
			if( spawnGrassFluff405 == true ) { fluff405() } else { carrion405() }
		}
		if( event.didTransition( eventList405.get(3) ) && event.isDecrease() ) {
			determineSpawnType405()
			if( spawnGrassFluff405 == true ) { fluff405() } else { carrion405() }
		}
		if( event.didTransition( eventList405.get(4) ) && event.isDecrease() ) {
			determineSpawnType405()
			if( spawnGrassFluff405 == true ) { fluff405() } else { carrion405() }
		}
	}
	runOnDeath( trashcan405, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch405() } } )
}

def determineSpawnType405() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff405 = true
	} else {
		spawnGrassFluff405 = false
	}
}

def fluff405() {
	fluff = fluff405.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion405() {
	carrion = carrion405.forceSpawnNow()
	carrion.addHate( random( playerList405 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch405() {
	if( fluff405.spawnsInUse() == 0 && carrion405.spawnsInUse() == 0 ) {
		resetTrash405()
	} else {
		myManager.schedule( 5 ) { roomWatch405() }
	}
}


//============================================
// TRASH CAN 406                              
//============================================

trash406 = myRooms.BASSKEN_406.spawnSpawner( "trash406", "trashcan", 1 )
trash406.setPos( 280, 330 )
trash406.setGuardPostForChildren( "BASSKEN_406", 280, 330, 45 )
trash406.setMonsterLevelForChildren( 4.3 )
trash406.stopSpawning()

fluff406 = myRooms.BASSKEN_406.spawnSpawner( "fluff406", "grass_fluff", 5)
fluff406.setPos( 920, 390 )
fluff406.setInitialMoveForChildren( "BASSKEN_406", 820, 190 )
fluff406.setHome( "BASSKEN_406", 820, 190 )
fluff406.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff406.setMonsterLevelForChildren( 4.3 )
fluff406.stopSpawning()

carrion406 = myRooms.BASSKEN_406.spawnSpawner( "carrion406", "grass_fluff_rabid", 5)
carrion406.setPos( 600, 355 )
carrion406.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion406.setMonsterLevelForChildren( 4.3 )
carrion406.stopSpawning()

//playerList for BASSKEN_406
playerList406 = []

myManager.onEnter( myRooms.BASSKEN_406 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList406 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_406 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList406.remove( event.actor )
	}
}

eventList406 = []

resetTrash406() //Set up the trash can the first time

def resetTrash406() {
	if( eventList406.isEmpty() || eventList406.size() < 5 ) {
		eventList406 << ( ( random( trashHealth ) * 4.3 ) / ( trashHealth * 4.3 ) * 100 ).intValue()
		resetTrash406()
	} else {
		spawnTrash406()
	}
}

def spawnTrash406() {
	trashcan406 = trash406.forceSpawnNow()
	trashcan406.setDisplayName( "...that's like a cactus..." )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan406 ) { event ->	
		trashcan406.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList406.get(0) ) && event.isDecrease() ) {
			determineSpawnType406()
			if( spawnGrassFluff406 == true ) { fluff406() } else { carrion406() }
		}
		if( event.didTransition( eventList406.get(1) ) && event.isDecrease() ) {
			determineSpawnType406()
			if( spawnGrassFluff406 == true ) { fluff406() } else { carrion406() }
		}
		if( event.didTransition( eventList406.get(2) ) && event.isDecrease() ) {
			determineSpawnType406()
			if( spawnGrassFluff406 == true ) { fluff406() } else { carrion406() }
		}
		if( event.didTransition( eventList406.get(3) ) && event.isDecrease() ) {
			determineSpawnType406()
			if( spawnGrassFluff406 == true ) { fluff406() } else { carrion406() }
		}
		if( event.didTransition( eventList406.get(4) ) && event.isDecrease() ) {
			determineSpawnType406()
			if( spawnGrassFluff406 == true ) { fluff406() } else { carrion406() }
		}
	}
	runOnDeath( trashcan406, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch406() } } )
}

def determineSpawnType406() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff406 = true
	} else {
		spawnGrassFluff406 = false
	}
}

def fluff406() {
	fluff = fluff406.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion406() {
	carrion = carrion406.forceSpawnNow()
	carrion.addHate( random( playerList406 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch406() {
	if( fluff406.spawnsInUse() == 0 && carrion406.spawnsInUse() == 0 ) {
		resetTrash406()
	} else {
		myManager.schedule( 5 ) { roomWatch406() }
	}
}

//============================================
// TRASH CAN 306                              
//============================================

trash306 = myRooms.BASSKEN_306.spawnSpawner( "trash306", "trashcan", 1 )
trash306.setPos( 890, 390 )
trash306.setGuardPostForChildren( "BASSKEN_306", 280, 330, 45 )
trash306.setMonsterLevelForChildren( 4.4 )
trash306.stopSpawning()

fluff306 = myRooms.BASSKEN_306.spawnSpawner( "fluff306", "grass_fluff", 5)
fluff306.setPos( 490, 250 )
fluff306.setInitialMoveForChildren( "BASSKEN_306", 160, 490 )
fluff306.setHome( "BASSKEN_306", 160, 490 )
fluff306.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff306.setMonsterLevelForChildren( 4.4 )
fluff306.stopSpawning()

carrion306 = myRooms.BASSKEN_306.spawnSpawner( "carrion306", "grass_fluff_rabid", 5)
carrion306.setPos( 715, 95 )
carrion306.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion306.setMonsterLevelForChildren( 4.4 )
carrion306.stopSpawning()

//playerList for BASSKEN_306
playerList306 = []

myManager.onEnter( myRooms.BASSKEN_306 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList306 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_306 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList306.remove( event.actor )
	}
}

eventList306 = []

resetTrash306() //Set up the trash can the first time

def resetTrash306() {
	if( eventList306.isEmpty() || eventList306.size() < 5 ) {
		eventList306 << ( ( random( trashHealth ) * 4.4 ) / ( trashHealth * 4.4 ) * 100 ).intValue()
		resetTrash306()
	} else {
		spawnTrash306()
	}
}

def spawnTrash306() {
	trashcan306 = trash306.forceSpawnNow()
	trashcan306.setDisplayName( "...takes more nerve..." )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan306 ) { event ->	
		trashcan306.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList306.get(0) ) && event.isDecrease() ) {
			determineSpawnType306()
			if( spawnGrassFluff306 == true ) { fluff306() } else { carrion306() }
		}
		if( event.didTransition( eventList306.get(1) ) && event.isDecrease() ) {
			determineSpawnType306()
			if( spawnGrassFluff306 == true ) { fluff306() } else { carrion306() }
		}
		if( event.didTransition( eventList306.get(2) ) && event.isDecrease() ) {
			determineSpawnType306()
			if( spawnGrassFluff306 == true ) { fluff306() } else { carrion306() }
		}
		if( event.didTransition( eventList306.get(3) ) && event.isDecrease() ) {
			determineSpawnType306()
			if( spawnGrassFluff306 == true ) { fluff306() } else { carrion306() }
		}
		if( event.didTransition( eventList306.get(4) ) && event.isDecrease() ) {
			determineSpawnType306()
			if( spawnGrassFluff306 == true ) { fluff306() } else { carrion306() }
		}
	}
	runOnDeath( trashcan306, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch306() } } )
}

def determineSpawnType306() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff306 = true
	} else {
		spawnGrassFluff306 = false
	}
}

def fluff306() {
	fluff = fluff306.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion306() {
	carrion = carrion306.forceSpawnNow()
	carrion.addHate( random( playerList306 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch306() {
	if( fluff306.spawnsInUse() == 0 && carrion306.spawnsInUse() == 0 ) {
		resetTrash306()
	} else {
		myManager.schedule( 5 ) { roomWatch306() }
	}
}

//============================================
// TRASH CAN 307                              
//============================================

trash307 = myRooms.BASSKEN_307.spawnSpawner( "trash307", "trashcan", 1 )
trash307.setPos( 310, 360 )
trash307.setGuardPostForChildren( "BASSKEN_307", 280, 330, 45 )
trash307.setMonsterLevelForChildren( 4.3 )
trash307.stopSpawning()

fluff307 = myRooms.BASSKEN_307.spawnSpawner( "fluff307", "grass_fluff", 5)
fluff307.setPos( 950, 580 )
fluff307.setInitialMoveForChildren( "BASSKEN_307", 500, 400 )
fluff307.setHome( "BASSKEN_307", 500, 400 )
fluff307.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff307.setMonsterLevelForChildren( 4.3 )
fluff307.stopSpawning()

carrion307 = myRooms.BASSKEN_307.spawnSpawner( "carrion307", "grass_fluff_rabid", 5)
carrion307.setPos( 965, 440 )
carrion307.setHateRadiusForChildren( 1000 )
carrion307.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion307.setMonsterLevelForChildren( 4.3 )
carrion307.stopSpawning()

//playerList for BASSKEN_307
playerList307 = []

myManager.onEnter( myRooms.BASSKEN_307 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList307 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_307 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList307.remove( event.actor )
	}
}

eventList307 = []

resetTrash307() //Set up the trash can the first time

def resetTrash307() {
	if( eventList307.isEmpty() || eventList307.size() < 5 ) {
		eventList307 << ( ( random( trashHealth ) * 4.3 ) / ( trashHealth * 4.3 ) * 100 ).intValue()
		resetTrash307()
	} else {
		spawnTrash307()
	}
}

def spawnTrash307() {
	trashcan307 = trash307.forceSpawnNow()
	trashcan307.setDisplayName( "...than it does practice." )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan307 ) { event ->	
		trashcan307.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList307.get(0) ) && event.isDecrease() ) {
			determineSpawnType307()
			if( spawnGrassFluff307 == true ) { fluff307() } else { carrion307() }
		}
		if( event.didTransition( eventList307.get(1) ) && event.isDecrease() ) {
			determineSpawnType307()
			if( spawnGrassFluff307 == true ) { fluff307() } else { carrion307() }
		}
		if( event.didTransition( eventList307.get(2) ) && event.isDecrease() ) {
			determineSpawnType307()
			if( spawnGrassFluff307 == true ) { fluff307() } else { carrion307() }
		}
		if( event.didTransition( eventList307.get(3) ) && event.isDecrease() ) {
			determineSpawnType307()
			if( spawnGrassFluff307 == true ) { fluff307() } else { carrion307() }
		}
		if( event.didTransition( eventList307.get(4) ) && event.isDecrease() ) {
			determineSpawnType307()
			if( spawnGrassFluff307 == true ) { fluff307() } else { carrion307() }
		}
	}
	runOnDeath( trashcan307, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch307() } } )
}

def determineSpawnType307() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff307 = true
	} else {
		spawnGrassFluff307 = false
	}
}

def fluff307() {
	fluff = fluff307.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion307() {
	carrion = carrion307.forceSpawnNow()
	carrion.addHate( random( playerList307 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch307() {
	if( fluff307.spawnsInUse() == 0 && carrion307.spawnsInUse() == 0 ) {
		resetTrash307()
	} else {
		myManager.schedule( 5 ) { roomWatch307() }
	}
}

//============================================
// TRASH CAN 407                              
//============================================

trash407 = myRooms.BASSKEN_407.spawnSpawner( "trash407", "trashcan", 1 )
trash407.setPos( 330, 190 )
trash407.setGuardPostForChildren( "BASSKEN_407", 280, 330, 45 )
trash407.setMonsterLevelForChildren( 4.2 )
trash407.stopSpawning()

fluff407 = myRooms.BASSKEN_407.spawnSpawner( "fluff407", "grass_fluff", 5)
fluff407.setPos( 25, 480 )
fluff407.setInitialMoveForChildren( "BASSKEN_407", 240, 340 )
fluff407.setHome( "BASSKEN_407", 240, 340 )
fluff407.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff407.setMonsterLevelForChildren( 4.2 )
fluff407.stopSpawning()

carrion407 = myRooms.BASSKEN_407.spawnSpawner( "carrion407", "grass_fluff_rabid", 5)
carrion407.setPos( 520, 290 )
carrion407.setHateRadiusForChildren( 1000 )
carrion407.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion407.setMonsterLevelForChildren( 4.2 )
carrion407.stopSpawning()

//playerList for BASSKEN_407
playerList407 = []

myManager.onEnter( myRooms.BASSKEN_407 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList407 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_407 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList407.remove( event.actor )
	}
}

eventList407 = []

resetTrash407() //Set up the trash can the first time

def resetTrash407() {
	if( eventList407.isEmpty() || eventList407.size() < 5 ) {
		eventList407 << ( ( random( trashHealth ) * 4.2 ) / ( trashHealth * 4.2 ) * 100 ).intValue()
		resetTrash407()
	} else {
		spawnTrash407()
	}
}

def spawnTrash407() {
	trashcan407 = trash407.forceSpawnNow()
	trashcan407.setDisplayName( "Burpa Shave" )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan407 ) { event ->	
		trashcan407.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList407.get(0) ) && event.isDecrease() ) {
			determineSpawnType407()
			if( spawnGrassFluff407 == true ) { fluff407() } else { carrion407() }
		}
		if( event.didTransition( eventList407.get(1) ) && event.isDecrease() ) {
			determineSpawnType407()
			if( spawnGrassFluff407 == true ) { fluff407() } else { carrion407() }
		}
		if( event.didTransition( eventList407.get(2) ) && event.isDecrease() ) {
			determineSpawnType407()
			if( spawnGrassFluff407 == true ) { fluff407() } else { carrion407() }
		}
		if( event.didTransition( eventList407.get(3) ) && event.isDecrease() ) {
			determineSpawnType407()
			if( spawnGrassFluff407 == true ) { fluff407() } else { carrion407() }
		}
		if( event.didTransition( eventList407.get(4) ) && event.isDecrease() ) {
			determineSpawnType407()
			if( spawnGrassFluff407 == true ) { fluff407() } else { carrion407() }
		}
	}
	runOnDeath( trashcan407, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch407() } } )
}

def determineSpawnType407() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff407 = true
	} else {
		spawnGrassFluff407 = false
	}
}

def fluff407() {
	fluff = fluff407.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion407() {
	carrion = carrion407.forceSpawnNow()
	carrion.addHate( random( playerList407 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch407() {
	if( fluff407.spawnsInUse() == 0 && carrion407.spawnsInUse() == 0 ) {
		resetTrash407()
	} else {
		myManager.schedule( 5 ) { roomWatch407() }
	}
}

//============================================
// TRASH CAN 507                              
//============================================

trash507 = myRooms.BASSKEN_507.spawnSpawner( "trash507", "trashcan", 1 )
trash507.setPos( 245, 270 )
trash507.setGuardPostForChildren( "BASSKEN_507", 280, 330, 45 )
trash507.setMonsterLevelForChildren( 4.1 )
trash507.stopSpawning()

fluff507 = myRooms.BASSKEN_507.spawnSpawner( "fluff507", "grass_fluff", 5)
fluff507.setPos( 860, 390 )
fluff507.setInitialMoveForChildren( "BASSKEN_507", 380, 160 )
fluff507.setHome( "BASSKEN_507", 380, 160 )
fluff507.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff507.setMonsterLevelForChildren( 4.1 )
fluff507.stopSpawning()

carrion507 = myRooms.BASSKEN_507.spawnSpawner( "carrion507", "grass_fluff_rabid", 5)
carrion507.setPos( 555, 140 )
carrion507.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion507.setMonsterLevelForChildren( 4.1 )
carrion507.stopSpawning()

//playerList for BASSKEN_507
playerList507 = []

myManager.onEnter( myRooms.BASSKEN_507 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList507 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_507 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList507.remove( event.actor )
	}
}

eventList507 = []

resetTrash507() //Set up the trash can the first time

def resetTrash507() {
	if( eventList507.isEmpty() || eventList507.size() < 5 ) {
		eventList507 << ( ( random( trashHealth ) * 4.1 ) / ( trashHealth * 4.1 ) * 100 ).intValue()
		resetTrash507()
	} else {
		spawnTrash507()
	}
}

def spawnTrash507() {
	trashcan507 = trash507.forceSpawnNow()
	trashcan507.setDisplayName( "To kiss..." )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan507 ) { event ->	
		trashcan507.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList507.get(0) ) && event.isDecrease() ) {
			determineSpawnType507()
			if( spawnGrassFluff507 == true ) { fluff507() } else { carrion507() }
		}
		if( event.didTransition( eventList507.get(1) ) && event.isDecrease() ) {
			determineSpawnType507()
			if( spawnGrassFluff507 == true ) { fluff507() } else { carrion507() }
		}
		if( event.didTransition( eventList507.get(2) ) && event.isDecrease() ) {
			determineSpawnType507()
			if( spawnGrassFluff507 == true ) { fluff507() } else { carrion507() }
		}
		if( event.didTransition( eventList507.get(3) ) && event.isDecrease() ) {
			determineSpawnType507()
			if( spawnGrassFluff507 == true ) { fluff507() } else { carrion507() }
		}
		if( event.didTransition( eventList507.get(4) ) && event.isDecrease() ) {
			determineSpawnType507()
			if( spawnGrassFluff507 == true ) { fluff507() } else { carrion507() }
		}
	}
	runOnDeath( trashcan507, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch507() } } )
}

def determineSpawnType507() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff507 = true
	} else {
		spawnGrassFluff507 = false
	}
}

def fluff507() {
	fluff = fluff507.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion507() {
	carrion = carrion507.forceSpawnNow()
	carrion.addHate( random( playerList507 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch507() {
	if( fluff507.spawnsInUse() == 0 && carrion507.spawnsInUse() == 0 ) {
		resetTrash507()
	} else {
		myManager.schedule( 5 ) { roomWatch507() }
	}
}

//============================================
// TRASH CAN 606                              
//============================================

trash606 = myRooms.BASSKEN_606.spawnSpawner( "trash606", "trashcan", 1 )
trash606.setPos( 915, 100 )
trash606.setGuardPostForChildren( "BASSKEN_606", 280, 330, 45 )
trash606.setMonsterLevelForChildren( 4.0 )
trash606.stopSpawning()

fluff606 = myRooms.BASSKEN_606.spawnSpawner( "fluff606", "grass_fluff", 5)
fluff606.setPos( 170, 230 )
fluff606.setInitialMoveForChildren( "BASSKEN_606", 360, 170 )
fluff606.setHome( "BASSKEN_606", 360, 170 )
fluff606.setWanderBehaviorForChildren( 50, 125, 1, 5, 200)
fluff606.setMonsterLevelForChildren( 4.0 )
fluff606.stopSpawning()

carrion606 = myRooms.BASSKEN_606.spawnSpawner( "carrion606", "grass_fluff_rabid", 5)
carrion606.setPos( 560, 65 )
carrion606.setWanderBehaviorForChildren( 50, 125, 4, 9, 200)
carrion606.setMonsterLevelForChildren( 4.0 )
carrion606.stopSpawning()

//playerList for BASSKEN_606
playerList606 = []

myManager.onEnter( myRooms.BASSKEN_606 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList606 << event.actor
	}
}

myManager.onExit( myRooms.BASSKEN_606 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerList606.remove( event.actor )
	}
}

eventList606 = []

resetTrash606() //Set up the trash can the first time

def resetTrash606() {
	if( eventList606.isEmpty() || eventList606.size() < 5 ) {
		eventList606 << ( ( random( trashHealth ) * 4.0 ) / ( trashHealth * 4.0 ) * 100 ).intValue()
		resetTrash606()
	} else {
		spawnTrash606()
	}
}

def spawnTrash606() {
	trashcan606 = trash606.forceSpawnNow()
	trashcan606.setDisplayName( "Property of Barton Town" )
	//predetermine the type of fluff to spawn from this trash can
	
	myManager.onHealth( trashcan606 ) { event ->	
		trashcan606.say( "CLANG!" )
		//TODO: If we can check the target doing the damage, then do NOT allow fluffs to be spawned if the attacking player has overall CL 5.2 or higher.
		if( event.didTransition( eventList606.get(0) ) && event.isDecrease() ) {
			determineSpawnType606()
			if( spawnGrassFluff606 == true ) { fluff606() } else { carrion606() }
		}
		if( event.didTransition( eventList606.get(1) ) && event.isDecrease() ) {
			determineSpawnType606()
			if( spawnGrassFluff606 == true ) { fluff606() } else { carrion606() }
		}
		if( event.didTransition( eventList606.get(2) ) && event.isDecrease() ) {
			determineSpawnType606()
			if( spawnGrassFluff606 == true ) { fluff606() } else { carrion606() }
		}
		if( event.didTransition( eventList606.get(3) ) && event.isDecrease() ) {
			determineSpawnType606()
			if( spawnGrassFluff606 == true ) { fluff606() } else { carrion606() }
		}
		if( event.didTransition( eventList606.get(4) ) && event.isDecrease() ) {
			determineSpawnType606()
			if( spawnGrassFluff606 == true ) { fluff606() } else { carrion606() }
		}
	}
	runOnDeath( trashcan606, { myManager.schedule( random( minTrashTime, maxTrashTime ) ) { roomWatch606() } } )
}

def determineSpawnType606() {
	roll = random( trashHealth )
	if( roll <= grassPortion ) { 
		spawnGrassFluff606 = true
	} else {
		spawnGrassFluff606 = false
	}
}

def fluff606() {
	fluff = fluff606.forceSpawnNow()
	fluff.say( random( fluffSayings ) )
}

def carrion606() {
	carrion = carrion606.forceSpawnNow()
	carrion.addHate( random( playerList606 ), 100 )
	carrion.say( random( carrionSayings ) )
}

def roomWatch606() {
	if( fluff606.spawnsInUse() == 0 && carrion606.spawnsInUse() == 0 ) {
		resetTrash606()
	} else {
		myManager.schedule( 5 ) { roomWatch606() }
	}
}


//------------------------------------------
// Fluff Mothers (and Mother Clumpers)      
//------------------------------------------


motherOneSpawner = myRooms.BASSKEN_505.spawnSpawner( "motherOneSpawner", "grass_fluff_demi", 1 )
motherOneSpawner.setPos( 622, 475 )
motherOneSpawner.setHomeTetherForChildren( 2000 )
motherOneSpawner.setHateRadiusForChildren( 1500 )
motherOneSpawner.setCFH( 1000 )
motherOneSpawner.setBaseSpeed( 100 );
motherOneSpawner.setChildrenToFollow( motherOneSpawner )
motherOneSpawner.setMonsterLevelForChildren( 4.8 )
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_505", 710, 540, 10 ) 
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_504", 410, 330, 10 )
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_604", 190, 140, 10 )
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_605", 790, 215, 10 ) 
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_505", 250, 270, 10 ) 
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_405", 320, 590, 10 )
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_505", 840, 250, 10 ) 
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_506", 310, 560, 10 )
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_606", 310, 175, 10 ) 
motherOneSpawner.addPatrolPointForSpawner( "BASSKEN_605", 790, 215, 10 )
motherOneSpawner.startPatrol()
motherOneSpawner.stopSpawning()

motherOneClumpers = myRooms.BASSKEN_505.spawnSpawner( "motherOneClumpers", "grass_fluff_guard", 3 )
motherOneClumpers.setPos( 622, 480 )
motherOneClumpers.setCFH( 1000 )
motherOneClumpers.setHomeTetherForChildren( 2000 )
motherOneClumpers.setHateRadiusForChildren( 1500 )
motherOneClumpers.startFollow( motherOneSpawner )
motherOneClumpers.setChildrenToFollow( motherOneSpawner )
motherOneClumpers.setMonsterLevelForChildren( 4.8 )
motherOneClumpers.stopSpawning()

motherOneClumpers.allyWithSpawner( motherOneSpawner )

//MOTHER TWO (Room 305)
motherTwoSpawner = myRooms.BASSKEN_305.spawnSpawner( "motherTwoSpawner", "grass_fluff_demi", 1 )
motherTwoSpawner.setPos( 550, 475 )
motherTwoSpawner.setHomeTetherForChildren( 2000 )
motherTwoSpawner.setHateRadiusForChildren( 1500 )
motherTwoSpawner.setCFH( 1000 )
motherTwoSpawner.setBaseSpeed( 100 );
motherTwoSpawner.setChildrenToFollow( motherTwoSpawner )
motherTwoSpawner.setMonsterLevelForChildren( 4.4 )
motherTwoSpawner.addPatrolPointForSpawner( "BASSKEN_306", 360, 340, 10 )
motherTwoSpawner.addPatrolPointForSpawner( "BASSKEN_305", 140, 575, 10 )
motherTwoSpawner.addPatrolPointForSpawner( "BASSKEN_405", 370, 165, 10 ) 
motherTwoSpawner.addPatrolPointForSpawner( "BASSKEN_305", 140, 575, 10 )
motherTwoSpawner.startPatrol()
motherTwoSpawner.stopSpawning()

motherTwoClumpers = myRooms.BASSKEN_305.spawnSpawner( "motherTwoClumpers", "grass_fluff_guard", 2 )
motherTwoClumpers.setPos( 550, 480 )
motherTwoClumpers.setCFH( 1000 )
motherTwoClumpers.setHomeTetherForChildren( 2000 )
motherTwoClumpers.setHateRadiusForChildren( 1500 )
motherTwoClumpers.startFollow( motherOneSpawner )
motherTwoClumpers.setChildrenToFollow( motherTwoSpawner )
motherTwoClumpers.setMonsterLevelForChildren( 4.4 )
motherTwoClumpers.stopSpawning()

motherTwoClumpers.allyWithSpawner( motherTwoSpawner )

//MOTHER THREE (Room 307)
motherThreeSpawner = myRooms.BASSKEN_307.spawnSpawner( "motherThreeSpawner", "grass_fluff_demi", 1 )
motherThreeSpawner.setPos( 575, 260 )
motherThreeSpawner.setHomeTetherForChildren( 2000 )
motherThreeSpawner.setHateRadiusForChildren( 1500 )
motherThreeSpawner.setCFH( 1000 )
motherThreeSpawner.setBaseSpeed( 100 )
motherThreeSpawner.setChildrenToFollow( motherThreeSpawner )
motherThreeSpawner.setMonsterLevelForChildren( 5.0 )
motherThreeSpawner.addPatrolPointForSpawner( "BASSKEN_307", 690, 345, 10 ) 
motherThreeSpawner.addPatrolPointForSpawner( "BASSKEN_407", 250, 445, 10 )
motherThreeSpawner.addPatrolPointForSpawner( "BASSKEN_507", 505, 165, 10 )
motherThreeSpawner.addPatrolPointForSpawner( "BASSKEN_506", 840, 90, 10 ) 
motherThreeSpawner.addPatrolPointForSpawner( "BASSKEN_406", 750, 310, 10 ) 
motherThreeSpawner.addPatrolPointForSpawner( "BASSKEN_306", 810, 205, 10 )
motherThreeSpawner.startPatrol()
motherThreeSpawner.stopSpawning()

motherThreeClumpers = myRooms.BASSKEN_307.spawnSpawner( "motherThreeClumpers", "grass_fluff_guard", 3 )
motherThreeClumpers.setPos( 575, 265 )
motherThreeClumpers.setCFH( 1000 )
motherThreeClumpers.setHomeTetherForChildren( 2000 )
motherThreeClumpers.setHateRadiusForChildren( 1500 )
motherThreeClumpers.startFollow( motherOneSpawner )
motherThreeClumpers.setChildrenToFollow( motherThreeSpawner )
motherThreeClumpers.setMonsterLevelForChildren( 4.8 )
motherThreeClumpers.stopSpawning()

motherThreeClumpers.allyWithSpawner( motherThreeSpawner )



//Mother One
def startMotherOne() {
	if( motherOneSpawner.spawnsInUse() + motherOneClumpers.spawnsInUse() == 0 ) {
		respawnMotherOne()
	} else {
		myManager.schedule( 10 ) { startMotherOne() }
	}
}

def respawnMotherOne() {
	mother1 = motherOneSpawner.forceSpawnNow()
	mother1.setDisplayName( "Lucy" )
	mother1.say( "Ah! Here we are! Back again!" )
	motherOneClumpers.spawnAllNow()
	runOnDeath( mother1, { myManager.schedule( random( 240, 480 ) ) { startMotherOne() } } )
}

//Mother Two
def startMotherTwo() {
	if( motherTwoSpawner.spawnsInUse() + motherTwoClumpers.spawnsInUse() == 0 ) {
		respawnMotherTwo()
	} else {
		myManager.schedule( 10 ) { startMotherTwo() }
	}
}

def respawnMotherTwo() {
	mother2 = motherTwoSpawner.forceSpawnNow()
	mother2.setDisplayName( "Goosey" )
	mother2.say( "hee hee! We're back from the void!" )
	motherTwoClumpers.spawnAllNow()
	runOnDeath( mother2, { myManager.schedule( random( 240, 480 ) ) { startMotherTwo() } } )
}

//Mother Three
def startMotherThree() {
	if( motherThreeSpawner.spawnsInUse() + motherThreeClumpers.spawnsInUse() == 0 ) {
		respawnMotherThree()
	} else {
		myManager.schedule( 10 ) { startMotherThree() }
	}
}

def respawnMotherThree() {
	mother3 = motherThreeSpawner.forceSpawnNow()
	mother3.setDisplayName( "Mary Bussey" )
	mother3.say( "#@&*! It's good to be back!" )
	motherThreeClumpers.spawnAllNow()
	runOnDeath( mother3, { myManager.schedule( random( 240, 480 ) ) { startMotherThree() } } )
}

startMotherOne()
startMotherTwo()
startMotherThree()

fluff506.allyWithSpawner( motherOneClumpers )
carrion506.allyWithSpawner( motherOneClumpers )
fluff505.allyWithSpawner( motherOneClumpers )
carrion505.allyWithSpawner( motherOneClumpers )
fluff405.allyWithSpawner( motherOneClumpers )
carrion405.allyWithSpawner( motherOneClumpers )
fluff406.allyWithSpawner( motherOneClumpers )
carrion406.allyWithSpawner( motherOneClumpers )
fluff306.allyWithSpawner( motherOneClumpers )
carrion306.allyWithSpawner( motherOneClumpers )
fluff307.allyWithSpawner( motherOneClumpers )
carrion307.allyWithSpawner( motherOneClumpers )
fluff407.allyWithSpawner( motherOneClumpers )
carrion407.allyWithSpawner( motherOneClumpers )
fluff507.allyWithSpawner( motherOneClumpers )
carrion507.allyWithSpawner( motherOneClumpers )
fluff606.allyWithSpawner( motherOneClumpers )
carrion606.allyWithSpawner( motherOneClumpers )

fluff506.allyWithSpawner( motherTwoClumpers )
carrion506.allyWithSpawner( motherTwoClumpers )
fluff505.allyWithSpawner( motherTwoClumpers )
carrion505.allyWithSpawner( motherTwoClumpers )
fluff405.allyWithSpawner( motherTwoClumpers )
carrion405.allyWithSpawner( motherTwoClumpers )
fluff406.allyWithSpawner( motherTwoClumpers )
carrion406.allyWithSpawner( motherTwoClumpers )
fluff306.allyWithSpawner( motherTwoClumpers )
carrion306.allyWithSpawner( motherTwoClumpers )
fluff307.allyWithSpawner( motherTwoClumpers )
carrion307.allyWithSpawner( motherTwoClumpers )
fluff407.allyWithSpawner( motherTwoClumpers )
carrion407.allyWithSpawner( motherTwoClumpers )
fluff507.allyWithSpawner( motherTwoClumpers )
carrion507.allyWithSpawner( motherTwoClumpers )
fluff606.allyWithSpawner( motherTwoClumpers )
carrion606.allyWithSpawner( motherTwoClumpers )

fluff506.allyWithSpawner( motherThreeClumpers )
carrion506.allyWithSpawner( motherThreeClumpers )
fluff505.allyWithSpawner( motherThreeClumpers )
carrion505.allyWithSpawner( motherThreeClumpers )
fluff405.allyWithSpawner( motherThreeClumpers )
carrion405.allyWithSpawner( motherThreeClumpers )
fluff406.allyWithSpawner( motherThreeClumpers )
carrion406.allyWithSpawner( motherThreeClumpers )
fluff306.allyWithSpawner( motherThreeClumpers )
carrion306.allyWithSpawner( motherThreeClumpers )
fluff307.allyWithSpawner( motherThreeClumpers )
carrion307.allyWithSpawner( motherThreeClumpers )
fluff407.allyWithSpawner( motherThreeClumpers )
carrion407.allyWithSpawner( motherThreeClumpers )
fluff507.allyWithSpawner( motherThreeClumpers )
carrion507.allyWithSpawner( motherThreeClumpers )
fluff606.allyWithSpawner( motherThreeClumpers )
carrion606.allyWithSpawner( motherThreeClumpers )

