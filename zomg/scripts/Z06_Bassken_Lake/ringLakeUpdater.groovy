//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def ringLake1 = "ringLake1"
myRooms.BASSKEN_203.createTriggerZone(ringLake1, 700, 450, 870, 655)
myManager.onTriggerIn(myRooms.BASSKEN_203, ringLake1) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(256, 2) && event.actor.hasQuestFlag(GLOBAL, "Z6_RingLake1")) {
		event.actor.updateQuest(256, "BFG-Mark")
		event.actor.centerPrint( "You see a ring as you approach the water and pick it up." )
		event.actor.addMiniMapQuestActorName("BFG-Mark")
	}
}

def ringLake2 = "ringLake2"
myRooms.BASSKEN_104.createTriggerZone(ringLake2, 385, 395, 930, 540)
myManager.onTriggerIn(myRooms.BASSKEN_104, ringLake2) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(256, 2) && event.actor.hasQuestFlag(GLOBAL, "Z6_RingLake2")) {
		event.actor.updateQuest(256, "BFG-Mark")
		event.actor.centerPrint( "You see a ring as you approach the water and pick it up." )
		event.actor.addMiniMapQuestActorName("BFG-Mark")
	}
}

def ringLake3 = "ringLake3"
myRooms.BASSKEN_306.createTriggerZone(ringLake3, 530, 30, 800, 150)
myManager.onTriggerIn(myRooms.BASSKEN_306, ringLake3) { event ->
//	println "#### PLAYER ENTERED 306 TRIGGER ####"
//	if( isPlayer(event.actor) && event.actor.isOnQuest(256,2) ) { println "****I'm ON THE FREAKING QUEST ****" }
//	if( isPlayer(event.actor) && event.actor.hasQuestFlag( GLOBAL, "Z6_RingLake3" ) ) { println "****And I have the DAMNED FLAG****" }
	if( isPlayer(event.actor) && event.actor.isOnQuest(256, 2) && event.actor.hasQuestFlag(GLOBAL, "Z6_RingLake3")) {
		event.actor.updateQuest(256, "BFG-Mark")
		event.actor.centerPrint( "You see a ring as you approach the water and pick it up." )
		event.actor.addMiniMapQuestActorName("BFG-Mark")
	}
}

def ringLake4 = "ringLake4"
myRooms.BASSKEN_305.createTriggerZone(ringLake4, 70, 310, 400, 400)
myManager.onTriggerIn(myRooms.BASSKEN_305, ringLake4) { event ->
	if( isPlayer(event.actor) && event.actor.isOnQuest(256, 2) && event.actor.hasQuestFlag(GLOBAL, "Z6_RingLake4")) {
		event.actor.updateQuest(256, "BFG-Mark")
		event.actor.centerPrint( "You see a ring as you approach the water and pick it up." )
		event.actor.addMiniMapQuestActorName("BFG-Mark")
	}
}