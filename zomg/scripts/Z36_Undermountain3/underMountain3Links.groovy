//Script created by gfern

import switches.switchActions;

//-----------------------------------------------------
//Constants & Variable Init
//-----------------------------------------------------
areaInit = false;

//-----------------------------------------------------
//Maps and lists
//-----------------------------------------------------
under2DoorMap = new HashMap();


//-----------------------------------------------------
//Switches
//-----------------------------------------------------
w = new switchActions(scriptObj:this);

def synchronized initRooms() {
	if(areaInit) {
		return false;
	}
	areaInit = true;

	def u2Exit = getScriptObject("under2LinkBack")


	if(u2Exit) {
		w.warpNoAggro([switchName:"CaveDoor", room:'Under3_1', clickX:100, clickY:210], u2Exit);
	}
	else {
		println "ERROR? " + this.getClass().getName() + " - Didn't have an under2LinkBack ref.  Must be test mode?";
	}
}

onZoneIn() { event ->	
	if(isPlayer(event.player) && event.player.isOnQuest(364, 3)) {
		event.player.updateQuest(364, "[NPC] Deva");
	}
	
	initRooms();
}
