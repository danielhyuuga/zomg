//Script created by gfern

//---------------------------------------------------
//Variables & Constants
//---------------------------------------------------
killCounter = 0;
areaInit = false;

KILL_LIMIT_363 = 20;
KILL_LIMIT_366 = 100;
KILL_LIMIT_367 = 150;
KILL_LIMIT_368 = 25;
KILL_LIMIT_369 = 50

//---------------------------------------------------
//Lists
//---------------------------------------------------
ltList = [] as Set;

underMountain3LTMonsters = ["vampire_LT"]

//---------------------------------------------------
//Listeners
//---------------------------------------------------
def ltDeathScheduler() {
	myRooms.values().each() {
		it.getActorList().each() {
			if(isMonster(it) && underMountain3LTMonsters.contains(it.getMonsterType()) && !ltList.contains(it)) {
				ltList << it;
				runOnDeath(it) { event -> 
					checkFor368Update(event);
					checkFor369Update(event);
					
					ltList.remove(event.actor) 
				}
			}
		}
	}
}

onZoneIn() { event ->
	if(areaInit == false) {
		areaInit = true;
		ltDeathScheduler();
	}
}

def checkFor363Update(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(363, 2)) {
			if(!it.getPlayerVar("Z30_Counter_363")) { it.setPlayerVar("Z30_Counter_363", 0) }
			killCounter = it.getPlayerVar("Z30_Counter_363");
			killCounter++;
			
			if(killCounter == KILL_LIMIT_363) {
				it.centerPrint("Monsters Defeated ${killCounter.intValue()}/${KILL_LIMIT_363}")
				it.updateQuest(363, "[NPC] Deva");
				it.deletePlayerVar("Z30_Counter_363");
			} else {
				it.centerPrint("Monsters Defeated ${killCounter.intValue()}/${KILL_LIMIT_363}")
				it.addPlayerVar("Z30_Counter_363", 1);
			}
		}
	}
}

def checkFor366Update(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(366, 2)) {
			if(!it.getPlayerVar("Z30_Counter_366")) { it.setPlayerVar("Z30_Counter_366", 0) }
			killCounter = it.getPlayerVar("Z30_Counter_366");
			killCounter ++;
			
			if(killCounter == KILL_LIMIT_366) {
				it.centerPrint("Kamila's Minions ${killCounter.intValue()}/${KILL_LIMIT_366}")
				it.updateQuest(366, "[NPC] Alastor");
				it.deletePlayerVar("Z30_Counter_366");
			} else {
				it.centerPrint("Kamila's Minions ${killCounter.intValue()}/${KILL_LIMIT_366}")
				it.addPlayerVar("Z30_Counter_366", 1);
			}
		}
	}
}

def checkFor367Update(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(367, 2)) {
			if(!it.getPlayerVar("Z30_Counter_367")) { it.setPlayerVar("Z30_Counter_367", 0) }
			killCounter = it.getPlayerVar("Z30_Counter_367");
			killCounter ++;
			
			if(killCounter == KILL_LIMIT_367) {
				it.centerPrint("Tissue Samples ${killCounter.intValue()}/${KILL_LIMIT_367}")
				it.updateQuest(367, "[NPC] Alastor");
				it.deletePlayerVar("Z30_Counter_367");
			} else {
				it.centerPrint("Tissue Samples ${killCounter.intValue()}/${KILL_LIMIT_367}")
				it.addPlayerVar("Z30_Counter_367", 1);
			}
		}
	}
}

def checkFor368Update(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(368, 2)) {
			if(!it.getPlayerVar("Z30_Counter_368")) { it.setPlayerVar("Z30_Counter_368", 0) }
			killCounter = it.getPlayerVar("Z30_Counter_368");
			killCounter ++;
			
			if(killCounter == KILL_LIMIT_368) {
				it.centerPrint("Tissue Samples ${killCounter.intValue()}/${KILL_LIMIT_368}")
				it.updateQuest(368, "[NPC] Alastor");
				it.deletePlayerVar("Z30_Counter_368");
			} else {
				it.centerPrint("Tissue Samples ${killCounter.intValue()}/${KILL_LIMIT_368}")
				it.addPlayerVar("Z30_Counter_368", 1);
			}
		}
	}
}

def checkFor369Update(event) {
	event.actor.getHated().each() {
		if(isPlayer(it) && it.isOnQuest(369, 2)) {
			if(!it.getPlayerVar("Z30_Counter_369")) { it.setPlayerVar("Z30_Counter_369", 0) }
			killCounter = it.getPlayerVar("Z30_Counter_369");
			killCounter ++;
			
			if(killCounter == KILL_LIMIT_369) {
				it.centerPrint("Tissue Samples ${killCounter.intValue()}/${KILL_LIMIT_369}")
				it.updateQuest(369, "[NPC] Deva");
				it.deletePlayerVar("Z30_Counter_369");
			} else {
				it.centerPrint("Tissue Samples ${killCounter.intValue()}/${KILL_LIMIT_369}")
				it.addPlayerVar("Z30_Counter_369", 1);
			}
		}
	}
}