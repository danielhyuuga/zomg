//Script created by gfern

import spawner.createSpawner;
import difficulty.setLevel;
import switches.switchActions;

//-----------------------------------------------------
//Constants & Variable Init
//-----------------------------------------------------
monsterLevel = 12;
kamilaLevel = 12.2;
level = 10;
underDifficulty = 1;
kamilaActivated = false;
kamilaAttackable = false;
questSpeech = false;
areaInit = false;
alastor = null;

HATE_RADIUS = 2900;
MIN_LEVEL = 12;
MAX_LEVEL = 12;
MAX_COFFIN_SPAWNS = 10;
COFFIN_MONSTER_DELAY = 30;
kSpawnExtensionPerRoom = 5;	// [rc] Time added to the coffin_monster_delay per room with non-activated coffins.  (so it gets faster later on)
COFFIN_ACTIVATE_INTERVAL = 30; // [rc] Time between coffins activating.  Once activated, they start spawning.
COFFIN_MONSTERS_TO_SPAWN = 5;
KAMILA_INVULN_DUR = 15;

//-----------------------------------------------------
//Maps and lists
//-----------------------------------------------------
underMountain4Monsters = ["vampire_female_um", "vampire_male_um", "blood_bat"];
coffinSpawnerList = [] as Set;
monstersSpawned = [] as Set;
underSpawnerList = [] as Set;
kamilaSayings = ["I draw my power from the undead themselves! You cannot hurt me!", "You cannot defeat me while I have souls to feast upon.", "You may have defeated my servants, but their blood only strengthens me.", "And now you see the futility of your efforts. My power grows while you near exhaustion!"];

underCoffinSpawnerMap = new HashMap(0);
underCoffinAttackData = new HashMap(0);
coffinSpawnerToMonsterSpawnerMap = new HashMap(0);
coffinToMonsterSpawnerMap = new HashMap(0);
monsterCounterMap = new HashMap(0);
difficultyMap = [1:0, 2:0.5, 3:1.0];
kamilaLocationMap = ["Under4_5":[950, 730, 90], "Under4_105":[670, 340, 120], "Under4_106":[740, 440, 350]];
kamilaLocationKey = new HashSet(kamilaLocationMap.keySet());

kamilaAttackBonus = [ 1:30, 2:15, 3:7 ]  // [rc] Rooms remaining : Percent attack bonus

//-----------------------------------------------------
//Level adjusting
//-----------------------------------------------------
l = new setLevel();

def torch1 = null;
def torch2 = null;


def synchronized init() {
	if(areaInit) {
		return
	}
	areaInit = true
	checkLevel();
	createCoffins();
	setKamilaArmor()
	
	torch1 = makeSwitch( "PurpleTorch1", myRooms.Under4_5, 53, 88 )
	torch1.off()
	torch1.lock()
	torch1.setUsable( false )

	torch2 = makeSwitch( "PurpleTorch2", myRooms.Under4_6, 53, 88 )
	torch2.off()
	torch2.lock()
	torch2.setUsable( false )
}

def checkLevel() {
	if(myArea.getAllPlayers().size() > 0) {
		player = random(myArea.getAllPlayers());
		underDifficulty = player.getTeam().getAreaVar("Z30_Undermountain", "Z30DeadmansDifficulty");
		if(!underDifficulty) { underDifficulty = 2 }

		player.getCrew().each() {
			if(it.getConLevel() + difficultyMap.get(underDifficulty) > level) {
				levelAdjust();
			}
		}
	}

	myManager.schedule(30) { checkLevel(); }
}

def levelAdjust() {
	level = l.setSpawnerLevel(MIN_LEVEL, MAX_LEVEL, underDifficulty, difficultyMap, player, coffinSpawnerToMonsterSpawnerMap.keySet().asList() + kamila_spawner);
}

//-----------------------------------------------------
//Spawners
//-----------------------------------------------------
c = new createSpawner();

//Coffin spec
def coffinInvuln = [
	isAllowed : { attacker, target ->
		def player = isPlayer(attacker) ? attacker : target;
		def allowed = underCoffinAttackData.get(target) == true;
		if(player == attacker && !allowed) { player.centerPrint("This coffin is not active yet.") }
		return allowed;
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec;

//Coffins
coffin5_1 = c.createStoppedGuardSpawner(myRooms.Under4_5, "coffin5_1", "coffin", 1, 520, 280, "Under4_5", monsterLevel, HATE_RADIUS, 120);
coffin5_2 = c.createStoppedGuardSpawner(myRooms.Under4_5, "coffin5_2", "coffin", 1, 720, 820, "Under4_5", monsterLevel, HATE_RADIUS, 120);
coffin5_3 = c.createStoppedGuardSpawner(myRooms.Under4_5, "coffin5_3", "coffin", 1, 1090, 560, "Under4_5", monsterLevel, HATE_RADIUS, 120);
coffin5_4 = c.createStoppedGuardSpawner(myRooms.Under4_5, "coffin5_4", "coffin", 1, 1200, 730, "Under4_5", monsterLevel, HATE_RADIUS, 120);

spawner5_1 = c.createStoppedGuardSpawner(myRooms.Under4_5, "spawner5_1", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 520, 280, "Under4_5", monsterLevel, HATE_RADIUS, 120);
spawner5_2 = c.createStoppedGuardSpawner(myRooms.Under4_5, "spawner5_2", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 720, 820, "Under4_5", monsterLevel, HATE_RADIUS, 120);
spawner5_3 = c.createStoppedGuardSpawner(myRooms.Under4_5, "spawner5_3", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1090, 560, "Under4_5", monsterLevel, HATE_RADIUS, 120);
spawner5_4 = c.createStoppedGuardSpawner(myRooms.Under4_5, "spawner5_4", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1200, 730, "Under4_5", monsterLevel, HATE_RADIUS, 120);

coffin5_1.setMiniEventSpec(coffinInvuln);
coffin5_2.setMiniEventSpec(coffinInvuln);
coffin5_3.setMiniEventSpec(coffinInvuln);
coffin5_4.setMiniEventSpec(coffinInvuln);

coffinSpawnerToMonsterSpawnerMap.put(coffin5_1,spawner5_1);
coffinSpawnerToMonsterSpawnerMap.put(coffin5_2,spawner5_2);
coffinSpawnerToMonsterSpawnerMap.put(coffin5_3,spawner5_3);
coffinSpawnerToMonsterSpawnerMap.put(coffin5_4,spawner5_4);

coffin6_1 = c.createStoppedGuardSpawner(myRooms.Under4_6, "coffin6_1", "coffin", 1, 1300, 910, "Under4_6", monsterLevel, HATE_RADIUS, 120);
coffin6_2 = c.createStoppedGuardSpawner(myRooms.Under4_6, "coffin6_2", "coffin", 1, 1340, 500, "Under4_6", monsterLevel, HATE_RADIUS, 120);
coffin6_3 = c.createStoppedGuardSpawner(myRooms.Under4_6, "coffin6_3", "coffin", 1, 200, 480, "Under4_6", monsterLevel, HATE_RADIUS, 120);
coffin6_4 = c.createStoppedGuardSpawner(myRooms.Under4_6, "coffin6_4", "coffin", 1, 400, 910, "Under4_6", monsterLevel, HATE_RADIUS, 120);

spawner6_1 = c.createStoppedGuardSpawner(myRooms.Under4_6, "spawner6_1", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1300, 910, "Under4_6", monsterLevel, HATE_RADIUS, 120);
spawner6_2 = c.createStoppedGuardSpawner(myRooms.Under4_6, "spawner6_2", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1340, 500, "Under4_6", monsterLevel, HATE_RADIUS, 120);
spawner6_3 = c.createStoppedGuardSpawner(myRooms.Under4_6, "spawner6_3", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 200, 480, "Under4_6", monsterLevel, HATE_RADIUS, 120);
spawner6_4 = c.createStoppedGuardSpawner(myRooms.Under4_6, "spawner6_4", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 400, 910, "Under4_6", monsterLevel, HATE_RADIUS, 120);

coffin6_1.setMiniEventSpec(coffinInvuln);
coffin6_2.setMiniEventSpec(coffinInvuln);
coffin6_3.setMiniEventSpec(coffinInvuln);
coffin6_4.setMiniEventSpec(coffinInvuln);

coffinSpawnerToMonsterSpawnerMap.put(coffin6_1,spawner6_1);
coffinSpawnerToMonsterSpawnerMap.put(coffin6_2,spawner6_2);
coffinSpawnerToMonsterSpawnerMap.put(coffin6_3,spawner6_3);
coffinSpawnerToMonsterSpawnerMap.put(coffin6_4,spawner6_4);

coffin105_1 = c.createStoppedGuardSpawner(myRooms.Under4_105, "coffin105_1", "coffin", 1, 330, 130, "Under4_105", monsterLevel, HATE_RADIUS, 120);
coffin105_2 = c.createStoppedGuardSpawner(myRooms.Under4_105, "coffin105_2", "coffin", 1, 1270, 170, "Under4_105", monsterLevel, HATE_RADIUS, 120);
coffin105_3 = c.createStoppedGuardSpawner(myRooms.Under4_105, "coffin105_3", "coffin", 1, 450, 740, "Under4_105", monsterLevel, HATE_RADIUS, 120);
coffin105_4 = c.createStoppedGuardSpawner(myRooms.Under4_105, "coffin105_4", "coffin", 1, 1310, 820, "Under4_105", monsterLevel, HATE_RADIUS, 120);

spawner105_1 = c.createStoppedGuardSpawner(myRooms.Under4_105, "spawner105_1", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 330, 130, "Under4_105", monsterLevel, HATE_RADIUS, 120);
spawner105_2 = c.createStoppedGuardSpawner(myRooms.Under4_105, "spawner105_2", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1270, 170, "Under4_105", monsterLevel, HATE_RADIUS, 120);
spawner105_3 = c.createStoppedGuardSpawner(myRooms.Under4_105, "spawner105_3", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 450, 740, "Under4_105", monsterLevel, HATE_RADIUS, 120);
spawner105_4 = c.createStoppedGuardSpawner(myRooms.Under4_105, "spawner105_4", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1310, 820, "Under4_105", monsterLevel, HATE_RADIUS, 120);

coffin105_1.setMiniEventSpec(coffinInvuln);
coffin105_2.setMiniEventSpec(coffinInvuln);
coffin105_3.setMiniEventSpec(coffinInvuln);
coffin105_4.setMiniEventSpec(coffinInvuln);

coffinSpawnerToMonsterSpawnerMap.put(coffin105_1,spawner105_1);
coffinSpawnerToMonsterSpawnerMap.put(coffin105_2,spawner105_2);
coffinSpawnerToMonsterSpawnerMap.put(coffin105_3,spawner105_3);
coffinSpawnerToMonsterSpawnerMap.put(coffin105_4,spawner105_4);

coffin106_1 = c.createStoppedGuardSpawner(myRooms.Under4_106, "coffin106_1", "coffin", 1, 740, 590, "Under4_106", monsterLevel, HATE_RADIUS, 120);
coffin106_2 = c.createStoppedGuardSpawner(myRooms.Under4_106, "coffin106_2", "coffin", 1, 1280, 760, "Under4_106", monsterLevel, HATE_RADIUS, 120);
coffin106_3 = c.createStoppedGuardSpawner(myRooms.Under4_106, "coffin106_3", "coffin", 1, 230, 150, "Under4_106", monsterLevel, HATE_RADIUS, 120);
coffin106_4 = c.createStoppedGuardSpawner(myRooms.Under4_106, "coffin106_4", "coffin", 1, 1210, 130, "Under4_106", monsterLevel, HATE_RADIUS, 120);

spawner106_1 = c.createStoppedGuardSpawner(myRooms.Under4_106, "spawner106_1", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 740, 590, "Under4_106", monsterLevel, HATE_RADIUS, 120);
spawner106_2 = c.createStoppedGuardSpawner(myRooms.Under4_106, "spawner106_2", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1280, 760, "Under4_106", monsterLevel, HATE_RADIUS, 120);
spawner106_3 = c.createStoppedGuardSpawner(myRooms.Under4_106, "spawner106_3", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 230, 150, "Under4_106", monsterLevel, HATE_RADIUS, 120);
spawner106_4 = c.createStoppedGuardSpawner(myRooms.Under4_106, "spawner106_4", random(underMountain4Monsters), COFFIN_MONSTERS_TO_SPAWN, 1210, 130, "Under4_106", monsterLevel, HATE_RADIUS, 120);

coffin106_1.setMiniEventSpec(coffinInvuln);
coffin106_2.setMiniEventSpec(coffinInvuln);
coffin106_3.setMiniEventSpec(coffinInvuln);
coffin106_4.setMiniEventSpec(coffinInvuln);

coffinSpawnerToMonsterSpawnerMap.put(coffin106_1,spawner106_1);
coffinSpawnerToMonsterSpawnerMap.put(coffin106_2,spawner106_2);
coffinSpawnerToMonsterSpawnerMap.put(coffin106_3,spawner106_3);
coffinSpawnerToMonsterSpawnerMap.put(coffin106_4,spawner106_4);

underCoffinSpawnerMapKey = new HashSet(underCoffinSpawnerMap.keySet());

def setKamilaArmor() {
	def coffinCount = kamila.getAI().getHome().getRoom().getMobTypeCount('coffin')
	kamila.setArmorPercent(20 * coffinCount)
}

//Coffin Logic
def createCoffins() {
	coffinSpawnerToMonsterSpawnerMap.each() { coffinSpawner, monsterSpawner ->
		def coffin = coffinSpawner.forceSpawnNow();
		coffin.setAwarenessArcRadius(0) // [rc] Seems to be necessary to stop the AI from freaking out?

		underCoffinAttackData.put(coffin, false);
		coffinToMonsterSpawnerMap.put(coffin, monsterSpawner);

		runOnDeath(coffin) { event -> 
			setKamilaArmor()
			coffinSpawnerList.remove(monsterSpawner)
		}
		
		if(!underCoffinSpawnerMap.containsKey(coffin.getRoomName())) {
			underCoffinSpawnerMap.put(coffin.getRoomName(),[])
		}
		underCoffinSpawnerMap[coffin.getRoomName()] << coffin
		
		/*
		if(underCoffinSpawnerMap.containsKey(coffin.getRoomName())) {
			dataToAdd = underCoffinSpawnerMap.get(coffin.getRoomName()).plus(coffin);
			underCoffinSpawnerMap.put(coffin.getRoomName(), dataToAdd);
		} else {
			underCoffinSpawnerMap.put(coffin.getRoomName(), [coffin]);
		}
		*/
	}
}

def activateCoffin() {
	if(coffinsToActivate.size() > 0) {
		coffinToActivate = random(coffinsToActivate);
		coffinsToActivate.remove(coffinToActivate);
		
		underCoffinAttackData.put(coffinToActivate, true);
		coffinSpawnerList << coffinToMonsterSpawnerMap.get(coffinToActivate);
		
		def delay = COFFIN_ACTIVATE_INTERVAL + (kSpawnExtensionPerRoom * underCoffinSpawnerMap.size())
		log("Setting coffin activation delay at ${delay} seconds")
		nextCoffin = myManager.schedule(COFFIN_ACTIVATE_INTERVAL) { activateCoffin(); }
	}
}

def spawnAtCoffins() {
	myManager.schedule(COFFIN_MONSTER_DELAY) { spawnAtCoffins(); }
	coffinSpawnerList.each() {
		if(!monsterCounterMap.containsKey(it)) { monsterCounterMap.put(it, 0); }

		if(monsterCounterMap.get(it) < COFFIN_MONSTERS_TO_SPAWN) {
			monster = it.forceSpawnNow();
			myArea.getAllPlayers().each { player -> monster.addHate(player, 1) }

			monstersSpawned << monster;
			runOnDeath(monster) { monstersSpawned.remove(monster) }
			def monsterCounter = monsterCounterMap.get(it) + 1;
			monsterCounterMap.put(it, monsterCounter);
		}
	}
}

onZoneIn() { event ->
	init()

	if(!kamilaActivated && !event.player.isDoneQuest(370)) {
		questSpeech = true;
		log("Setting queen's speach to true")
	}
}

//Kamila
kamila_spawner = c.createStoppedGuardSpawner(myRooms.Under4_6, "kamila_spawner", "kamila", 1, 700, 610, "Under4_6", kamilaLevel, HATE_RADIUS, 120);

def kamilaInvuln = [
	isAllowed : { attacker, target ->
		def player = isPlayer(attacker) ? attacker : target;
		def allowed = kamilaAttackable;
		if(player == attacker && allowed == false) { player.centerPrint("A shroud of dark energy surrounds Kamila") }
		return allowed;
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec;

kamila_spawner.setMiniEventSpec(kamilaInvuln);
kamila = kamila_spawner.forceSpawnNow();

w = new switchActions(scriptObj:this);



// [rc] Special case room enter listener because the other script needs to use onEnter

onEnter = [ actorLeaves : { event -> }, actorJoins : { event ->
	if(event.eventType == com.gaiaonline.mmo.battle.event.Events.ACTOR_JOIN && isPlayer(event.actor)) {
		if(event.actor.isOnQuest(370, 2)) {
			event.actor.updateQuest(370, "[NPC] Alastor");
		}

		activateKamila(event);
	}
}] as com.gaiaonline.mmo.battle.event.RoomListener

myRooms.Under4_6.addRoomListener(onEnter)

/* [rc] Needed to use the special case onEnter above because it conflicts with myManager.onEnter in the other script because the enter/exit event system sucks ass.
myManager.onEnter(myRooms.Under4_6) { event ->
if(isPlayer(event.actor)) {
if(event.actor.isOnQuest(370, 2)) {
it.updateQuest(370, "[NPC] Alastor");
questSpeech = true;
}
activateKamila(event);
}
}
 */

//Kamila logic
def synchronized activateKamila(event) {
	if(kamilaActivated) return;
	kamilaActivated = true

	if(questSpeech == true) {
		alastor = spawnNPC("[NPC] Alastor", myRooms.Under4_6, 1210, 460);
		alastor.setRotation(120);
		alastor.setDisplayName("Alastor");
	}

	myManager.schedule(2) {
		if(questSpeech == true) {
			alastor.shout("Hahaha! I can't believe they fell for it, Kamila!");
			myManager.schedule(5) {
				alastor.shout("The blood they spill begins your glorious transforamion. No one will stand between us an our rule of Gaia!");

				myManager.schedule(5) {
					kamila.shout("Us? No, Alastor, it was not they who were foolish. It was you. Did you really believe I would allow you to continue to control me after my ascension?");

					myManager.schedule(5) {
						alastor.shout("How dare you! I will see you dead before I see you rise above me!");

						myManager.schedule(5) {
							kamila.shout("It is too late, Alastor. I am already above you. Begone, before I make you gone.");

							myManager.schedule(5) {
								alastor.shout("You'll regret this, Kamila!");

								myManager.schedule(5) {
									alastor.dispose();

									kamila.shout("One last loose end to tie up: YOU!");

									engageKamila()
								}
							}
						}
					}
				}
			}
		} else {
			kamila.shout("You are in my domain now.  Strengthened by these coffins I am undefeatable!");
			kamila.shout("I will not bore you with a tiresome monologue. You need know only this: your failure to heed my warning will cost you your lives.");
			myManager.schedule(5) { 
				kamila.shout("DIE MORTALS!");

				engageKamila()
			}
		}
	} 
}

def engageKamila()
{
	kamilaAttackable = true; //make her attackable
	kamila.addHate(random(myArea.getAllPlayers()), 1); //kamila aggro on player

	coffinsToActivate = underCoffinSpawnerMap.get("Under4_6");
	underCoffinSpawnerMap.remove("Under4_6");

	activateCoffin(); //activate coffin routine for this room
	kamilaHealthTransition();

	myManager.schedule(2) { spawnAtCoffins(); }
}

def kamilaHealthTransition() {
	healthPercent = underCoffinSpawnerMap.size() * 25;
	log("^^^^^^ kamilaHealthTransition register for ${healthPercent}")
	myManager.onHealth(kamila) { health ->
		if(health.didTransition(healthPercent) && health.isDecrease()) {
			log("^^^^^ got Kamila health transition for ${healthPercent}")

			nextLocation = random(kamilaLocationKey);
			kamilaLocationKey.remove(nextLocation);

			kamila_spawner.setHomeForEveryone(nextLocation, kamilaLocationMap.get(nextLocation).get(0), kamilaLocationMap.get(nextLocation).get(1));
			kamila_spawner.setGuardPostForChildren(nextLocation, kamilaLocationMap.get(nextLocation).get(0), kamilaLocationMap.get(nextLocation).get(1), kamilaLocationMap.get(nextLocation).get(2));

			// [rc] Set her attack power bonus
			def roomsLeft = underCoffinSpawnerMap.size()
			if(kamilaAttackBonus[roomsLeft]) {
				def attackMultiplier = kamilaAttackBonus[roomsLeft]/100f + 1f
				log("Setting Kamila attack multiplier at {}", attackMultiplier)
				kamila.setAttackMultiplier(attackMultiplier)
			}

			kamilaAttackable = false;

			setKamilaArmor()


			coffinSpawnerList.clear();
			coffinsToActivate.clear();
			nextCoffin.cancel();

			monstersSpawned.clone().each() {
				//it.instantPercentDamage(100);
				monstersSpawned.remove(it);
			}

			kamila.shout(random(kamilaSayings));

			myManager.schedule(KAMILA_INVULN_DUR) { kamilaAttackable = true; }

			coffinsToActivate = underCoffinSpawnerMap.get(nextLocation);
			underCoffinSpawnerMap.remove(nextLocation);

			activateCoffin(); //activate coffin routine for this room
			kamilaHealthTransition();
		}
	}	
}

runOnDeath(kamila) { event ->
	coffinSpawnerList.clear();
	coffinsToActivate.clear();
	nextCoffin.cancel();

	monstersSpawned.clone().each() {
		it.instantPercentDamage(100);
		monstersSpawned.remove(it);
	}

	event.actor.getHated().each() {
		it.centerPrint("Flames erupt from the nearby Bell Tower.");
	}
	enableTower()
}

def enableTower()
{
	def onBellTower = { event ->
		myArea.getAllPlayers().each() { player ->
			player.centerPrint("The bell tower was touched, you are being pulled through space.")
			myManager.schedule(5) {
				player.warp("Under5_1", random(100, 300), random(800, 925), 6);
			}
		}
	}

	def tower1 = makeGeneric("BellTower_1", myRooms.Under4_5, 1500, 860);
	tower1.setUsable(true);
	tower1.setRange(200);
	tower1.onUse(onBellTower);

	def tower2 = makeGeneric("BellTower_2", myRooms.Under4_6, 50, 860);
	tower2.setUsable(true);
	tower2.setRange(200);
	tower2.onUse(onBellTower);

	tower1.setStateful(true);
	tower1.setActorState(1);
	tower2.setStateful(true);
	tower2.setActorState(1);

	torch1.on();
	torch2.on();
}

