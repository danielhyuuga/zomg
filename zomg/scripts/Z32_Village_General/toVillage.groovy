//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//---------------------------------------------------------
// SWITCH FOR EXIT                                         
//---------------------------------------------------------
timerMap = new HashMap()
offlineResetSet = [] as Set

generalExit = makeGeneric("Front", myRooms.VillageGeneral_1, 410, 490)
generalExit.setStateful()
generalExit.setUsable(true)
generalExit.setMouseoverText("Village Greens")
generalExit.setRange(200)

generalExit.onUse() { event ->
	event.player.warp("VILLAGE_901", 650, 630, 6)
}