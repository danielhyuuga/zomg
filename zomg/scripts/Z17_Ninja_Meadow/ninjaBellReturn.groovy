//---------------------------------------------------------
// THE NINJA BELL (Going back to Zen Gardens)
//---------------------------------------------------------

bellToZen = makeSwitch( "ninjaBellReturn", myRooms.Ninja_1, 1010, 480 )
bellToZen.unlock()
bellToZen.setMouseoverText("The Ninja Bell")

bellToZen.setRange( 250 )

def bellTap = { event ->
	//play the sound of the bell ringing
	sound("bellSound").toZone()
	//transport the player to the Ninja Meadow
	event.actor.warp( "ZENGARDEN_101", 765, 430 )
	bellToZen.lock()
	myManager.schedule(2) { bellToZen.off(); bellToZen.unlock() } //turn the switch back on quickly so it can be used again.
}

bellToZen.whenOn( bellTap )


