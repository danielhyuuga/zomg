import com.gaiaonline.mmo.battle.script.*;

Kin = spawnNPC( "Kin-VQS", myRooms.Ninja_1, 400, 400 )
Kin.setDisplayName( "Kin" )


def takeOffMask( event ) {
	myManager.schedule(1) {
		Kin.dispose()
		KinNoMask = spawnNPC( "Kin No Mask-VQS", myRooms.Ninja_1, 400, 400 )
		KinNoMask.setDisplayName( "Uncle Kin" )
		activateKinNoMaskDialog()
		myManager.schedule(1) { KinNoMask.pushDialog( event.player, "ThreeBridgesStart" ) }
	}
}


myManager.onEnter(myRooms.Ninja_1) { event ->
	if( isPlayer( event.actor ) ) {
		if(event.actor.isOnQuest( 48, 2 ) ) {
			event.actor.updateQuest( 48, "Kin-VQS" ) //update the quest so that Kin will start speaking with you
			myManager.schedule(4) { Kin.pushDialog(event.actor, "KinQuestKin") }
		}
		if( event.actor.isDoneQuest( 61 ) ) {
			Kin.dispose()
			KinNoMask = spawnNPC( "Kin No Mask-VQS", myRooms.Ninja_1, 400, 400 )
			KinNoMask.setDisplayName( "Uncle Kin" )
			activateKinNoMaskDialog()
		}
		//error fix for when the ringGrant dialog push was incorrect. This fixes it for those people that didn't get their ring originally
		if( event.actor.hasQuestFlag( GLOBAL, "Z17FourthRingGrantOkay" ) && !event.actor.hasQuestFlag( GLOBAL, "Z17FourthRingGrantReceived" ) ) {
			KinNoMask.pushDialog( event.actor, "ringGrant" )
		}
		//BUG FIXES to resolve situations screwed up when we reverted the "finished" flags on these quests...but players had already finished the next quests.
		if( event.actor.isDoneQuest( 60 ) && event.actor.isOnQuest( 59, 3 ) ) {
			event.actor.updateQuest( 59 )
		}
		if( event.actor.isDoneQuest( 61 ) && event.actor.isOnQuest( 60, 3 ) ) {
			event.actor.updateQuest( 60 )
		}
		event.actor.unsetQuestFlag( GLOBAL, "Z17RingRewardInProgress" )
	}
}

//---------------------------------------------------------
// FIND UNCLE KIN Quest (Kin's section)                    
//---------------------------------------------------------

def KinQuestKin = Kin.createConversation("KinQuestKin", true, "QuestStarted_48:3")

//KinQuest1 was eliminated as unnecessary. Didn't feel like renumbering everything else, so we start with KinQuest2 now.

def KinQuest2 = [id:2]
KinQuest2.npctext = "Kat...I don't have time for..."
KinQuest2.result = 3
KinQuestKin.addDialog(KinQuest2, Kin)

def KinQuest3 = [id:3]
KinQuest3.npctext = "You...are NOT Katsumi. How did you come by that glass rod?"
KinQuest3.playertext = "Whoa! Katsumi sent me here! She asked me to find her Uncle Kin and tell him that she's all right!"
KinQuest3.result = 4
KinQuestKin.addDialog(KinQuest3, Kin)

def KinQuest4 = [id:4]
KinQuest4.npctext = "You know Katsumi then?"
KinQuest4.playertext = "Yes! Little girl in a shrine. Likes to make wishes. I helped deliver one for her recently and then she asked me to come find you. She wants you to know that she's safe from harm."
KinQuest4.result = 5
KinQuestKin.addDialog(KinQuest4, Kin)

def KinQuest5 = [id:5]
KinQuest5.npctext = "Yes. Heh. That sounds like her exactly."
KinQuest5.result = 6
KinQuestKin.addDialog(KinQuest5, Kin)

def KinQuest6 = [id:6]
KinQuest6.npctext = "That's good then. You may tell Katsumi that I am assured of her safety. What is your name? I will remember those that you have helped my family."
KinQuest6.playertext = "I'm %p. It's not entirely necessary that you...err...remember me."
KinQuest6.result = 7
KinQuestKin.addDialog(KinQuest6, Kin)

def KinQuest7 = [id:7]
KinQuest7.npctext = "Nonsense. Now, I give you a choice. Choose carefully. I have two needs. One is that my niece may still need protection. The other is that my clan is sorely pressed within the forest. We have need of those that do not fear danger to assist us. Which will you pursue: loyalty or adventure?"
KinQuest7.options = []
KinQuest7.options << [text:"I'll go back and help Katsumi", result: 8]
KinQuest7.options << [text:"I want to help YOU. Katsumi is safe now and won't have any more troubles!", result: 9]
KinQuestKin.addDialog(KinQuest7, Kin)

def KinQuest8 = [id:8]
KinQuest8.npctext = "Good. I am glad that you prize loyalty over irrational adventure. Katsumi needs your help. Ask her what she needs. If she releases you from duty to her, then return to me and I will reward you for your loyalty. Be wary, %p. The world is not what it was."
KinQuest8.playertext = "You've got that right. Thank you. I will return to Katsumi."
KinQuest8.quest = 48 //push the completion of the third step of the FIND UNCLE KIN quest.
KinQuest8.result = DONE
KinQuest8.exec = { event ->
	event.player.addMiniMapQuestActorName( "Katsumi-VQS" ) 
}
KinQuestKin.addDialog(KinQuest8, Kin)

def KinQuest9 = [id:9]
KinQuest9.npctext = "So you have chosen to abandon an innocent to possible doom while you race into the unknown to face foes you know nothing about. You have much to learn about caution and loyalty, young one."
KinQuest9.playertext = "Your niece is just fine! The Kokeshi aren't even a threat if she stays in that shrine, so the need seemed to be greater here, with you."
KinQuest9.result = 10
KinQuestKin.addDialog(KinQuest9, Kin)

def KinQuest10 = [id:10]
KinQuest10.npctext = "I think you have much to learn. Go seek my niece and help her as much as she desires. When you are done there, return to me and we will speak again."
KinQuest10.playertext = "All right..."
KinQuest10.quest = 48 //push the completion of the third step of the FIND UNCLE KIN quest
KinQuest10.result = DONE
KinQuest10.exec = { event ->
	event.player.addMiniMapQuestActorName( "Katsumi-VQS" ) 
}
KinQuestKin.addDialog(KinQuest10, Kin)

//---------------------------------------------------------
// KIN'S TRIALS (part 1)                                   
// During these quests, Kin will relate more info about    
// what is known about the rings and why they work         
//---------------------------------------------------------

//---------------------------------------------------------
//TRIAL ONE - THE TAIKOS (Start)                           
//---------------------------------------------------------

def KinHardDrumKills = Kin.createConversation( "KinHardDrumKills", true, "QuestCompleted_47", "!QuestStarted_59", "!QuestCompleted_59" )

def HardKill1 = [id:1]
HardKill1.npctext = "Greetings again, %p. You would not be bothering me if Katsumi had not released you from her tasks. So, it is my intention to test you with a series of tasks. These tasks will help my clan understand more of what is happening within the Gardens."
HardKill1.playertext = "With respect, why don't you just send ninjas to do these tasks?"
HardKill1.result = 2
KinHardDrumKills.addDialog(HardKill1, Kin)

def HardKill2 = [id:2]
HardKill2.npctext = "My ninjas are currently busy within the woods. We are hard-pressed and we have no one to spare. We are forced to rely on our new friends to accomplish other goals. Friends like yourself."
HardKill2.playertext = "Thanks."
HardKill2.options = []
HardKill2.options << [text:"I'm glad to be considered a friend to your clan. What can I do for you?  (**NOTE: The reward for completing Kin's series of tasks includes the selection of a new ring!**)", result: 4]
HardKill2.options << [text:"I'm pretty busy right now. I'll come back another time to help, okay?", result: 3]
KinHardDrumKills.addDialog(HardKill2, Kin)

def HardKill3 = [id:3]
HardKill3.npctext = "So be it. I will speak to you upon your return. Good luck, %p."
HardKill3.result = DONE
KinHardDrumKills.addDialog(HardKill3, Kin)

def HardKill4 = [id:4]
HardKill4.npctext = "Something is...odd...about the way the Animated in this area are acting. There's a strangeness about the way they are grouping and moving around, almost as if something is about to happen."
HardKill4.playertext = "What sort of thing?"
HardKill4.result = 5
KinHardDrumKills.addDialog(HardKill4, Kin)

def HardKill5 = [id:5]
HardKill5.npctext = "Well...I don't know. That's what we need to find out. You've already encountered the Kokeshi when you helped Katsumi. Now I'd like you to take on the Taiko Drums that seem to have infested the nearby grassland areas north of the bridges. Find out what you can about them and come back to tell me more after you have struggled against them for a time."
HardKill5.quest = 59 //start the player on the kill quest part of the quest
HardKill5.flag = "Z5DrumKillQuestStarted"
HardKill5.exec = { event ->
	if( event.player.hasQuestFlag( GLOBAL, "Z05UncleKinReminder" ) ) {
		event.player.unsetQuestFlag( GLOBAL, "Z05UncleKinReminder" )
		event.player.removeMiniMapQuestLocation( "ninjaBell" )
	}
}
HardKill5.result = DONE
KinHardDrumKills.addDialog(HardKill5, Kin)


//---------------------------------------------------------
// Drum Kill Interim                                       
//---------------------------------------------------------

def DrumKillInterim = Kin.createConversation("DrumKillInterim", true, "QuestStarted_59:2" )

def DrumInt1 = [id:1]
DrumInt1.npctext = "You've done well until now...but you still need to find out more. Get back out there and see what else you can find."
DrumInt1.result = DONE
DrumKillInterim.addDialog(DrumInt1, Kin)

//---------------------------------------------------------
// Drum Kill Success                                       
// Kin should discuss theories about rings here            
//---------------------------------------------------------

def DrumKillSuccess = Kin.createConversation("DrumKillSuccess", true, "QuestStarted_59:3" )

def DrumSucc1 = [id:1]
DrumSucc1.npctext = "Well done, %p. From what you tell me, the Drums are just wandering about, almost aimlessly, which is quite different than the Kokeshi that you spoke about near the Shrine."
DrumSucc1.playertext = "Like night and day, actually. The Kokeshi almost seemed to be an automated military, marching in circles. On the other hand, the Taiko were more like free-ranging carnivores, attacking targets of opportunity as they come across them."
DrumSucc1.result = 2
DrumKillSuccess.addDialog(DrumSucc1, Kin)

def DrumSucc2 = [id:2]
DrumSucc2.npctext = "A good report. Please accept this small reward for the scouting you are doing for us."
DrumSucc2.quest = 59 //push the completion of the "Kin's Trials (part 1)" quest
DrumSucc2.flag = "Z5ConnectToLanterns"
DrumSucc2.exec = { event -> 
	Kin.pushDialog(event.player, "KinHardLanternKills")
}
DrumKillSuccess.addDialog(DrumSucc2, Kin)


//---------------------------------------------------------
// Kill Quest Two (Lanterns)                               
//---------------------------------------------------------

def KinHardLanternKills = Kin.createConversation("KinHardLanternKills", true, "Z5ConnectToLanterns", "!QuestStarted_60", "!QuestCompleted_60" )

def LantStart1 = [id:1]
LantStart1.npctext = "Hmmm...well...there is a third force in this area. The last few nights, the lanterns themselves seem to be...well...they seem almost haunted."
LantStart1.playertext = "There are ghosts haunting the lantern racks?"
LantStart1.result = 2
KinHardLanternKills.addDialog(LantStart1, Kin)

def LantStart2 = [id:2]
LantStart2.npctext = "No, not really. It's another of those strange Animated behaviors. It's like the lanterns come to life, but not really. The ghost lanterns come out of nowhere. The original lanterns still hang on their racks, but the ghost versions keep appearing each night, in greater and greater numbers."
LantStart2.playertext = "So you need me to see if the lanterns are different than the drums and dolls?"
LantStart2.result = 3
KinHardLanternKills.addDialog(LantStart2, Kin)

def LantStart3 = [id:3]
LantStart3.npctext = "That's it exactly. Try to find out what you can about them. There must be some sort of pattern to all this madness. It would help us all to figure out what's going on."
LantStart3.quest = 60 //push the start of the "Kin's Trials (part 2)" quest
LantStart3.result = DONE
KinHardLanternKills.addDialog(LantStart3, Kin)

//---------------------------------------------------------
// Lantern Kill Interim                                    
//---------------------------------------------------------

def LanternKillsInterim = Kin.createConversation("Z5LanternKillsInterim", true, "QuestStarted_60:2" )

def LantInterim1 = [id:1]
LantInterim1.npctext = "Back already? Did you learn anything?"
LantInterim1.playertext = "Err...no. Not yet. I haven't fought that many of them just yet."
LantInterim1.result = 2
LanternKillsInterim.addDialog(LantInterim1, Kin)

def LantInterim2 = [id:2]
LantInterim2.npctext = "Then go and do what must be done."
LantInterim2.result = DONE
LanternKillsInterim.addDialog(LantInterim2, Kin)

//---------------------------------------------------------
// Lantern Kill Success                                    
//---------------------------------------------------------

def LanternKillsSuccess = Kin.createConversation("Z5LanternKillsSuccess", true, "QuestStarted_60:3" )

def LantSucc1 = [id:1]
LantSucc1.npctext = "So...the dolls march, the drums wander, and the lanterns seem to be setting up a haunting of some sort around the lantern racks. That doesn't add up to much."
LantSucc1.playertext = "The only thing that's common is that they seem to appear out of nowhere, as if they're being teleported in from another dimension or something."
LantSucc1.result = 2
LanternKillsSuccess.addDialog(LantSucc1, Kin)

def LantSucc2 = [id:2]
LantSucc2.npctext = "Another dimension...?"
LantSucc2.playertext = "Errr...nevermind. I watch a lot of TV..."
LantSucc2.result = 3
LanternKillsSuccess.addDialog(LantSucc2, Kin)

def LantSucc3 = [id:3]
LantSucc3.npctext = "Right. I see. Well...I suppose that's about as far out of bounds as magic would be, and neither of those explanations seems as outlandish as it should right now."
LantSucc3.playertext = "Yikes. You're seriously considering magic?"
LantSucc3.result = 4
LanternKillsSuccess.addDialog(LantSucc3, Kin)

def LantSucc4 = [id:4]
LantSucc4.npctext = "I have seen many mystical things in my life. At this point, I would not rule out any possible explanation. Thank you for scouting for my clan, the Chyaku Norisu. Let me reward you for your efforts."
LantSucc4.flag = "Z5ConnectToTravelQuest"
LantSucc4.quest = 60 //push the completion of the "Kin's Trials (part 2)" quest
LantSucc4.exec = { event -> 
	Kin.pushDialog(event.player, "KinTravelQuest")
}
LanternKillsSuccess.addDialog(LantSucc4, Kin)

//---------------------------------------------------------
// Travel Quest Start (to Logan)                           
//---------------------------------------------------------

def KinTravelQuest = Kin.createConversation("KinTravelQuest", true, "Z5ConnectToTravelQuest", "!QuestStarted_61", "!QuestCompleted_61") // "QuestCompleted_60", 

def TravelQuest1 = [id:1]
TravelQuest1.npctext = "What you have found so far is mysterious to you and I, but others may be able to make more sense of it. Would you continue to assist me by passing messages to some of my friends that may know more?"
TravelQuest1.options = []
TravelQuest1.options << [text:"Yes, of course! Who do you need me to go see?", result: 3]
TravelQuest1.options << [text:"Sorry, Kin. I'm not going to be able to help right now. Maybe another time.", result: 2]
KinTravelQuest.addDialog(TravelQuest1, Kin)

def TravelQuest2 = [id:2]
TravelQuest2.npctext = "If there is a time that we see each other again, then perhaps. Good luck, %p."
TravelQuest2.result = DONE
KinTravelQuest.addDialog(TravelQuest2, Kin)

def TravelQuest3 = [id:3]
TravelQuest3.npctext = "To the north of the Gardens lies Bass'ken Lake. My old friend Logan has a fishing shack there. Logan and I used to be traveling companions a long time ago and he's surprisingly worldly. If you take what we know and compare it with his own experiences, we might find something that makes more sense."
TravelQuest3.playertext = "So head up to Bass'ken Lake and find Old Man Logan. Got it. I'll come back and let you know what I find out."
TravelQuest3.result = 4
KinTravelQuest.addDialog(TravelQuest3, Kin)

def TravelQuest4 = [id:4]
TravelQuest4.npctext = "Logan may want you to check a few things out. I vouch for him personally. You can trust him...most of the time."
TravelQuest4.playertext = "heh. Most of the time, eh? That's a glowing recommendation."
TravelQuest4.result = 5
KinTravelQuest.addDialog(TravelQuest4, Kin)

def TravelQuest5 = [id:5]
TravelQuest5.npctext = "Logan can be quite...ill-tempered at times, but he's at his best with a challenge, so he should be sharp right now. Good luck to you, %p."
TravelQuest5.playertext = "Thanks...I guess. See you later."
TravelQuest5.result = DONE
TravelQuest5.quest = 61 //Start the "Kin's Trials (part 3)" quest
TravelQuest5.exec = { event ->
	event.player.addMiniMapQuestActorName( "[NPC] Old Man Logan" )
}
KinTravelQuest.addDialog(TravelQuest5, Kin)

//---------------------------------------------------------
// Travel Quest Interim (until return from Agatha)         
//---------------------------------------------------------

def KinTravelInterim = Kin.createConversation("KinTravelInterim", true, "QuestStarted_61", "!QuestStarted_61:4")

def TravelInterim1 = [id:1]
TravelInterim1.npctext = "What did you find out, %p?"
TravelInterim1.playertext = "I'm not sure that I've learned everything that I can find out. I still need more time."
TravelInterim1.result = 2
KinTravelInterim.addDialog(TravelInterim1, Kin)

def TravelInterim2 = [id:2]
TravelInterim2.npctext = "Time is plentiful and free, but its absence can be expensive. Be careful how much of it you use. I shall see you again soon."
TravelInterim2.playertext = "Thanks. I'll be back."
TravelInterim2.result = DONE
KinTravelInterim.addDialog(TravelInterim2, Kin)

//---------------------------------------------------------
// Travel Quest Success (and series quest completion)      
//---------------------------------------------------------

def KinTravelSuccess = Kin.createConversation("KinTravelSuccess", true, "QuestStarted_61:4")

def TravelSucc1 = [id:1]
TravelSucc1.npctext = "What did Logan have to say?"
TravelSucc1.playertext = "I spoke to him...and Agatha..."
TravelSucc1.result = 2
KinTravelSuccess.addDialog(TravelSucc1, Kin)

def TravelSucc2 = [id:2]
TravelSucc2.npctext = "Agatha, too? You've been traveling a lot then. What did you find?"
TravelSucc2.playertext = "Well...Logan thinks the rings we're using come from within the Animated, and Agatha says there are more threats out there than just the Animated."
TravelSucc2.result = 3
KinTravelSuccess.addDialog(TravelSucc2, Kin)

def TravelSucc3 = [id:3]
TravelSucc3.npctext = "This may not be a situation we can easily resolve then. If this is as widespread as it sounds, then this may be a long-term change to our world. If that is true, then we may need to adopt a siege mentality toward this whole disaster, digging in and fighting back against the gathering hordes."
TravelSucc3.playertext = "Cheery outlook you have there, Kin."
TravelSucc3.result = 4
KinTravelSuccess.addDialog(TravelSucc3, Kin)

def TravelSucc4 = [id:4]
TravelSucc4.npctext = "..."
TravelSucc4.playertext = "See? Case in point. So what do we do next?"
TravelSucc4.result = 5
KinTravelSuccess.addDialog(TravelSucc4, Kin)

def TravelSucc5 = [id:5]
TravelSucc5.npctext = "WE don't do anything. YOU do. But your journey to this point should be rewarded. Here. Take these small tokens of my esteem."
TravelSucc5.quest = 61 //push the completion step to "Kin's Trials (part 3)" quest
TravelSucc5.flag = "Z5TravelQuestCompleted"
TravelSucc5.exec = { event -> 
	event.player.removeMiniMapQuestActorName( "Kin-VQS" )
	takeOffMask( event )
}
KinTravelSuccess.addDialog(TravelSucc5, Kin)

def activateKinNoMaskDialog() {

	//---------------------------------------------------------
	// Three Bridges Quest START                               
	//---------------------------------------------------------

	def ThreeBridgesStart = KinNoMask.createConversation("ThreeBridgesStart", true, "!QuestStarted_62", "!QuestCompleted_62", "Z5TravelQuestCompleted", "!Z17PlayerRejectedKin" ) 

	def BridgeStart1 = [id:1]
	BridgeStart1.npctext = "First, you have earned the reward of seeing to whom you are speaking. I unmask myself in honor of your accomplishments for my clan."
	BridgeStart1.playertext = "Oh, wow. Thank you, Master Kin!"
	BridgeStart1.result = 20
	ThreeBridgesStart.addDialog( BridgeStart1, KinNoMask )

	def BridgeStart20 = [id:20]
	BridgeStart20.npctext = "Now we progress to the next step. For this, you must understand that the ninja are grounded not only in the arts martial, but also in the ways of mysticism."
	BridgeStart20.playertext = "Yes?"
	BridgeStart20.result = 2
	ThreeBridgesStart.addDialog(BridgeStart20, KinNoMask)

	def BridgeStart2 = [id:2]
	BridgeStart2.npctext = "We...see things that other people overlook or don't see at all. What we see now are 'ripples', for lack of a better word."
	BridgeStart2.playertext = "Ripples? Ripples in what?"
	BridgeStart2.result = 3
	ThreeBridgesStart.addDialog(BridgeStart2, KinNoMask)

	def BridgeStart3 = [id:3]
	BridgeStart3.npctext = "Ripples in reality. Not just ripples, but eddies as well."
	BridgeStart3.playertext = "You mean like in a river?"
	BridgeStart3.result = 4
	ThreeBridgesStart.addDialog(BridgeStart3, KinNoMask)

	def BridgeStart4 = [id:4]
	BridgeStart4.npctext = "Yes, exactly. It's as if the entire countryside is awash in a sea of energy, and where that energy eddies together, the Animated appear."
	BridgeStart4.playertext = "So you can tell where the Animated are going to appear ahead of time?"
	BridgeStart4.result = 5
	ThreeBridgesStart.addDialog(BridgeStart4, KinNoMask)

	def BridgeStart5 = [id:5]
	BridgeStart5.npctext = "That would be convenient. But no...not often, nor reliably. However, the normal eddies and ripples we've seen recently seem to be gathering power in an area centering around the bridges in the Garden's center."
	BridgeStart5.playertext = "You mean we could be facing a major outpouring of Animated there soon?"
	BridgeStart5.result = 6
	ThreeBridgesStart.addDialog(BridgeStart5, KinNoMask)

	def BridgeStart6 = [id:6]
	BridgeStart6.npctext = "It could be...but those of us with the keenest other sight think that what we're seeing is the force eddying into three major whirlpools of energy, possibly creating three stronger entities."
	BridgeStart6.playertext = "You mean the Animated are getting more powerful leadership?"
	BridgeStart6.result = 7
	ThreeBridgesStart.addDialog(BridgeStart6, KinNoMask)

	def BridgeStart7 = [id:7]
	BridgeStart7.npctext = "Yes. That is what we fear most."
	BridgeStart7.playertext = "Do you know where these whirlpools of power are forming?"
	BridgeStart7.result = 8
	ThreeBridgesStart.addDialog(BridgeStart7, KinNoMask)

	def BridgeStart8 = [id:8]
	BridgeStart8.npctext = "They seem to be centering on the bridges in the middle of the Gardens."
	BridgeStart8.options = []
	BridgeStart8.options << [text:"I'd be happy to go watch the bridges for you. I've fought against the Taikos, the Lanterns, and the Dolls. If they're getting new leaders, I might have a useful edge in the encounter.", result: 11]
	BridgeStart8.options << [text:"You had better send your ninjas in, Kin. I haven't got time to watch a bunch of bridges right now.", result: 9]
	ThreeBridgesStart.addDialog(BridgeStart8, KinNoMask)

	def BridgeStart9 = [id:9]
	BridgeStart9.npctext = "So be it. You may use the crystal rod to return to the Gardens. Goodbye. You are not the hero we had hoped for."
	BridgeStart9.playertext = "But...wait!"
	BridgeStart9.options = []
	BridgeStart9.options << [text:"I was rash, Kin. Please...I'm sorry about that. Let me go guard the bridges. You know I'm the right person for the job!", result: 11]
	BridgeStart9.options << [text:"Nah. Nevermind. I'm not going to reconsider.", result: 10]
	ThreeBridgesStart.addDialog(BridgeStart9, KinNoMask)

	def BridgeStart10 = [id:10]
	BridgeStart10.npctext = "So...you have made your choice. Further discussion has no point until you reconsider. Go now."
	BridgeStart10.flag = "Z17PlayerRejectedKin"
	BridgeStart10.result = DONE
	ThreeBridgesStart.addDialog(BridgeStart10, KinNoMask)

	def BridgeStart11 = [id:11]
	BridgeStart11.npctext = "Very well. I would recommend that you travel back and forth across the bridges in the center of the Gardens. The power of your rings may tip the balance of energy in those whirlpools and make the new entities appear before they are ready. If that is the case, then you must destroy those entities before they become stronger."
	BridgeStart11.playertext = "So wander around looking for ambushes then?"
	BridgeStart11.result = 12
	ThreeBridgesStart.addDialog(BridgeStart11, KinNoMask)

	def BridgeStart12 = [id:12]
	BridgeStart12.npctext = "It is a rare boon that is not purchased with either sweat or pain. You will most likely pay in both those currencies."
	BridgeStart12.playertext = "ooooh! Deep. Okay...well...no time like the present. I hope to see you soon, Kin."
	BridgeStart12.result = 13
	ThreeBridgesStart.addDialog(BridgeStart12, KinNoMask)

	def BridgeStart13 = [id:13]
	BridgeStart13.npctext = "Good luck to you, %p. This is your final trial. Be strong."
	BridgeStart13.quest = 62 //push the start of the Battle of Three Bridges quest
	BridgeStart13.flag = ["Z5ThreeBridgesQuestStageOne", "Z5ThreeBridgesQuestStarted"]
	BridgeStart13.result = DONE
	ThreeBridgesStart.addDialog(BridgeStart13, KinNoMask)

	//-----------------------------------------------------------
	// Three Bridges Quest START (if player rejected Kin before) 
	//-----------------------------------------------------------
	
	def ThreeBridgesStartAlt = KinNoMask.createConversation("ThreeBridgesStartAlt", true, "!QuestStarted_62", "!QuestCompleted_62", "Z5TravelQuestCompleted", "Z17PlayerRejectedKin") 

	def BridgeStartAlt1 = [id:1]
	BridgeStartAlt1.npctext = "I see you have reconsidered and returned. Good. We shall begin again at once."
	BridgeStartAlt1.playertext = "No time like the present!"
	BridgeStartAlt1.result = 2
	ThreeBridgesStartAlt.addDialog( BridgeStartAlt1, KinNoMask )

	def BridgeStartAlt2 = [id:2]
	BridgeStartAlt2.npctext = "I recommend that you travel back and forth across the bridges in the center of the Gardens. The power of your rings may tip the balance of energy in those whirlpools and make the new entities appear before they are ready. If that is the case, then you must destroy those entities before they become stronger."
	BridgeStartAlt2.playertext = "So wander around looking for ambushes then?"
	BridgeStartAlt2.result = 3
	ThreeBridgesStartAlt.addDialog(BridgeStartAlt2, KinNoMask)

	def BridgeStartAlt3 = [id:3]
	BridgeStartAlt3.npctext = "It is a rare boon that is not purchased with either sweat or pain. You will most likely pay in both those currencies."
	BridgeStartAlt3.playertext = "ooooh! Deep. Okay...well...I better get started. I hope to see you soon, Kin."
	BridgeStartAlt3.result = 4
	ThreeBridgesStartAlt.addDialog(BridgeStartAlt3, KinNoMask)

	def BridgeStartAlt4 = [id:4]
	BridgeStartAlt4.npctext = "Good luck to you, %p. This is your final trial. Be strong."
	BridgeStartAlt4.quest = 62 //push the start of the Battle of Three Bridges quest
	BridgeStartAlt4.flag = ["Z5ThreeBridgesQuestStageOne", "Z5ThreeBridgesQuestStarted"]
	BridgeStartAlt4.result = DONE
	ThreeBridgesStartAlt.addDialog(BridgeStartAlt4, KinNoMask)
	
	//---------------------------------------------------------
	// Three Bridges INTERIM(S)                                
	//---------------------------------------------------------

	def ThreeBridgesInterminStageOne = KinNoMask.createConversation("ThreeBridgesInterminStageOne", true, "QuestStarted_62", "!QuestStarted_62:5" )

	def StageOne1 = [id:1]
	StageOne1.npctext = "You've barely begun to fight. Get back out there!"
	StageOne1.result = DONE
	ThreeBridgesInterminStageOne.addDialog(StageOne1, KinNoMask)

	def ThreeBridgesInterminStageTwo = KinNoMask.createConversation("ThreeBridgesInterminStageTwo", true, "QuestStarted_62:3")

	def StageTwo1 = [id:2]
	StageTwo1.npctext = "Good work. One down...two to go! Now is the time to find out what you are made of...so fight!"
	StageTwo1.result = DONE
	ThreeBridgesInterminStageTwo.addDialog(StageTwo1, KinNoMask)

	def ThreeBridgesInterminStageThree = KinNoMask.createConversation("ThreeBridgesInterminStageThree", true, "QuestStarted_62:4")

	def StageThree1 = [id:1]
	StageThree1.npctext = "You are almost done! Two out of three is good...but not good enough. Fight harder!"
	StageThree1.result = DONE
	ThreeBridgesInterminStageThree.addDialog(StageThree1, KinNoMask)


	//---------------------------------------------------------
	// Three Bridges SUCCESS                                   
	//---------------------------------------------------------

	def ThreeBridgesSuccess = KinNoMask.createConversation("ThreeBridgesSuccess", true, "QuestStarted_62:5")

	def BridgeSuccess1 = [id:1]
	BridgeSuccess1.npctext = "You have done it! All three of the entities defeated! You have exceeded my wildest expectations and are quite worthy of our honors and accolades!"
	BridgeSuccess1.playertext = "Thanks!"
	BridgeSuccess1.result = 2
	ThreeBridgesSuccess.addDialog(BridgeSuccess1, KinNoMask)

	def BridgeSuccess2 = [id:2]
	BridgeSuccess2.npctext = "The energy you released is almost certainly still building up, but now that you've discovered the pattern, I'm sure we can have others continue your work at keeping it disrupted."
	BridgeSuccess2.playertext = "I'm glad to have helped you recognize the pattern."
	BridgeSuccess2.result = 3
	ThreeBridgesSuccess.addDialog(BridgeSuccess2, KinNoMask)

	def BridgeSuccess3 = [id:3]
	BridgeSuccess3.npctext = "There will come a day when the Chyaku Norisu clan will have great need of staunch allies. I hope that we can count you as friend when that time comes."
	BridgeSuccess3.playertext = "Thank you, Kin. It would be my honor."
	BridgeSuccess3.result = 4
	ThreeBridgesSuccess.addDialog( BridgeSuccess3, KinNoMask )
	
	def BridgeSuccess4 = [id:4]
	BridgeSuccess4.npctext = "In the meantime, my ninjas have gathered a great number of rings that come from these Animated. I would be honored if you would accept one of them as a small reward for your assistance."
	BridgeSuccess4.playertext = "I'd be honored to do so!"
	BridgeSuccess4.flag = "Z5ChyakuNorisuFriend" //Set this flag for later when we open up the Ninja Enclave and let it be useful at that time.
	BridgeSuccess4.exec = { event ->
		event.player.setQuestFlag( GLOBAL, "Z17FourthRingGrantOkay" )
		event.player.setQuestFlag( GLOBAL, "Z17KinToLoganConnector" )
		event.player.addMiniMapQuestLocation( "loganConnector", "BASSKEN_403", 890, 300, "Kin requests you to help Logan" )
		event.player.updateQuest(252, "Kin No Mask-VQS")
		KinNoMask.pushDialog( event.player, "ringGrant" )
	}
	BridgeSuccess4.result = DONE
	ThreeBridgesSuccess.addDialog(BridgeSuccess4, KinNoMask)
	
	//--------------------------------------------------------------
	//RING GRANT CONVERSATION                                       
	//--------------------------------------------------------------
	def ringGrant = KinNoMask.createConversation( "ringGrant", true, "Z17FourthRingGrantOkay", "!Z17FourthRingGrantReceived", "!Z17RingRewardInProgress" )

	def grant1 = [id:1]
	grant1.npctext = "Choose carefully from the rings that we have gathered."
	grant1.exec = { event ->
		player = event.player
		event.player.setQuestFlag( GLOBAL, "Z17RingRewardInProgress" )
		makeMainMenu( player )
	}
	grant1.result = DONE
	ringGrant.addDialog( grant1, KinNoMask )

	
	//---------------------------------------------------------
	// Default Loop (after all Quests Completed)               
	//---------------------------------------------------------

	def AfterBridges = KinNoMask.createConversation("AfterBridges", true, "Z17FourthRingGrantReceived")

	def AfterBridges1 = [id:1]
	AfterBridges1.npctext = "The fights in the forest still rage beyond control, but when the time is right, I would be glad to have you visit us, %p. You are always welcome among the Chyaku Norisu. Until then, journey widely and live your life, friend."
	AfterBridges1.result = 2
	AfterBridges.addDialog(AfterBridges1, KinNoMask)
	
	def AfterBridges2 = [id:2]
	AfterBridges2.npctext = "In the meantime, if you wish to aid me, then perhaps you would also aid my friend, Logan, in Bass'ken Lake. You have met Logan and despite his roughness, he is honorable. Go to him and offer your assistance. He may even admit that he appreciates it. If you are lucky."
	AfterBridges2.playertext = "Okay, Kin! Will do! And thanks again!"
	AfterBridges.addDialog( AfterBridges2, KinNoMask )
}

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 2
CLdecimal = 0

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["New Rings", "Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "New Rings" ) {
			makeNewRingMenu( player )
		}
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z17RingRewardInProgress" )
		}
	}
}

//====================================================
// NEW RINGS                                          
//====================================================
def makeNewRingMenu( player ) {
	titleString = "New Rings Menu"
	descripString = "These rings are new to the list this time."
	diffOptions = [ "Divinity", "Fleet Feet", "Hunter's Bow", "Mantis", "Rock Armor", "Main Menu"  ]
	
	uiButtonMenu( player, "newRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Divinity" ) {
			makeDivinityMenu( player )
		}
		if( event.selection == "Fleet Feet" ) {
			makeFleetFeetMenu( player )
		}
		if( event.selection == "Hunter's Bow" ) {
			makeHuntersBowMenu( player )
		}
		if( event.selection == "Mantis" ) {
			makeMantisMenu( player )
		}
		if( event.selection == "Rock Armor" ) {
			makeRockArmorMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Hack", "Mantis", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Hack" ) {
			makeHackMenu( player )
		}
		if( event.selection == "Mantis" ) {
			makeMantisMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Guns, Guns, Guns", "Hornet Nest", "Hot Foot", "Hunter's Bow", "Shuriken", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Guns, Guns, Guns" ) {
			makeGunsGunsGunsMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Hunter's Bow" ) {
			makeHuntersBowMenu( player )
		}
		if( event.selection == "Shuriken" ) {
			makeShurikenMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Quicksand", "Scaredy Cat", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Scaredy Cat" ) {
			makeScaredyCatMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Pot Lid", "Rock Armor", "Teflon Spray", "Turtle", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Rock Armor" ) {
			makeRockArmorMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Turtle" ) {
			makeTurtleMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and the...probability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Defibrillate", "Diagnose", "Divinity", "Healing Halo", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Defibrillate" ) {
			makeDefibrillateMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Divinity" ) {
			makeDivinityMenu( player )
		}
		if( event.selection == "Healing Halo" ) {
			makeHealingHaloMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Fleet Feet", "Ghost", "Iron Will", "Keen Aye", "My Density", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Fleet Feet" ) {
			makeFleetFeetMenu( player )
		}
		if( event.selection == "Ghost" ) {
			makeGhostMenu( player )
		}
		if( event.selection == "Iron Will" ) {
			makeIronWillMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "My Density" ) {
			makeMyDensityMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.updateQuest( 62, "Kin No Mask-VQS" ) //push the completion flag for "The Battle of Three Bridges"
	event.actor.setQuestFlag( GLOBAL, "Z17KinToLoganConnector" )
	event.actor.updateQuest(252, "Kin No Mask-VQS") //complete the NINJA TRIALS badge
	event.actor.addMiniMapQuestLocation( "loganConnector", "BASSKEN_403", 890, 300, "Kin requests that you help Logan" )
	event.actor.setQuestFlag( GLOBAL, "Z17FourthRingGrantReceived" )
}
