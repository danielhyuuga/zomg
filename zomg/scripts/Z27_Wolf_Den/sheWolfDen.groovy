import com.gaiaonline.mmo.battle.script.*;

ML = 4.0
minML = 4.0
MaxML = 5.1

//------------------------------------------
// SPAWNERS                                 
//------------------------------------------

// She Wolf
sheWolfSpawner = myRooms.WolfDen_1.spawnStoppedSpawner( "sheWolfSpawner", "outlaw_pup_demi", 1)
sheWolfSpawner.setPos( 160, 440 )
sheWolfSpawner.setCFH( 2000 )
sheWolfSpawner.setMonsterLevelForChildren( ML )

// Pup Spawners
pupSpawner = myRooms.WolfDen_1.spawnStoppedSpawner( "pupSpawner", "outlaw_pup", 20)
pupSpawner.setPos( 900, 320 )
pupSpawner.setHateRadiusForChildren( 2000 )
pupSpawner.setHomeForChildren( "WolfDen_1", 780, 580 )
pupSpawner.setMonsterLevelForChildren( ML )

// Wolf Spawners
wolfSpawner = myRooms.WolfDen_1.spawnStoppedSpawner( "wolfSpawner", "outlaw_pup_LT", 20)
wolfSpawner.setPos( 880, 310 )
wolfSpawner.setHateRadiusForChildren( 2000 )
wolfSpawner.setHomeForChildren( "WolfDen_1", 780, 580 )
wolfSpawner.setMonsterLevelForChildren( ML )

spawnLocationMap = [ 1: [160, 530], 2: [420, 320], 3: [900, 330], 4: [1150, 340], 5: [1360, 460], 6: [1400, 620], 7: [1170, 860], 8: [960, 930], 9: [550, 920], 10: [220, 770] ]

//------------------------------------------
// ALLIANCES                                
//------------------------------------------
sheWolfSpawner.allyWithSpawner( pupSpawner )
sheWolfSpawner.allyWithSpawner( wolfSpawner )

//------------------------------------------
// SHE WOLF DEN LOGIC                       
//------------------------------------------

sheWolfSpawned = false
sheWolf = null
gameStarted = false
difficulty = null
oldSpawn = null

playerSet = [] as Set
hateCollector = [] as Set

//Manager the Player list for the room
myManager.onEnter( myRooms.WolfDen_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet << event.actor
		if( gameStarted == false ) {
			myManager.schedule(3) { event.actor.centerPrint( "Your footsteps echo dully around the cave." ) }
			myManager.schedule(6) { event.actor.centerPrint( "The air is heavy with the smell of old fur and, something...rotten...") }
		} else {
			myManager.schedule(3) { event.actor.centerPrint( "Your crewmates are already embroiled in a fight. Get out there and help!" ) }
		}
		//DIFFICULTY
		//The first player into the scenario sets the difficulty for everyone else
		if( difficulty == null ) {
			difficulty = event.actor.getTeam().getAreaVar( "Z27_Wolf_Den", "Z27WolfDenDifficulty" )
			if( difficulty == 0 ) { difficulty = 1 }
		}			
	}
}

myManager.onExit( myRooms.WolfDen_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet.remove( event.actor )
	}
}

//===========================================
// GAME LOGIC                                
//===========================================

//Game starts when the first player steps into the central trigger zone (see bottom of file)
def startGame( event ) {
	//set the initial Monster Level based on the crew at the time of first player entry
	event.actor.getCrew().clone().each { 
		if( it.getConLevel() > ML && it.getConLevel() <= MaxML ) {
			ML = it.getConLevel()
		}
	}		
	//normal delay between waves BEFORE She Wolf spawns
	if( difficulty == 1 ) { delay = 16 }
	if( difficulty == 2 ) { delay = 13 }
	if( difficulty == 3 ) { delay = 10 }

	//slower spawns while fighting She Wolf
	if( difficulty == 1 ) { harassDelay = 24 }
	if( difficulty == 2 ) { harassDelay = 20 }
	if( difficulty == 3 ) { harassDelay = 15 }

	playerSet.clone().each() { it.centerPrint( "You hear snarls from the cracks in the walls." ) }
	myManager.schedule(3) { playerSet.clone().each() { it.centerPrint( "Something is coming!" ) } }
	myManager.schedule(4) {
		currentWave = 0
		nextWave()
	}
}

def adjustML() {
	ML = minML

	//kick players out if they are cheating
	playerSet.clone().each{
		if( it.getConLevel() >= MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "BASSKEN_502", 520, 210 )
		}
	}

	//adjust the encounter ML to the group
	random( playerSet ).getCrew().each() {
		if( it.getConLevel() >= ML && it.getConLevel() < MaxML ) { 
			ML = it.getConLevel()
		} else if( it.getConLevel() >= MaxML ) {
			ML = MaxML
		}
	}

	//DIFFICULTY
	if( difficulty == 1 ) { ML = ML - 0.6 }
	if( difficulty == 3 ) { ML = ML + 0.6 }

	//set the monster levels for all spawners and children again
	pupSpawner.setMonsterLevelForChildren( ML )
	sheWolfSpawner.setMonsterLevelForChildren( ML )
	wolfSpawner.setMonsterLevelForChildren( ML )
}

def checkDazedStatus() {
	numDazed = 0
	myRooms.WolfDen_1.getActorList().each { if( isPlayer( it ) && it.isDazed() ) { numDazed ++ } }
	if( numDazed >= playerSet.size() ) {
		myRooms.WolfDen_1.getActorList().each {
			if( isPlayer( it ) ) {
				it.centerPrint( "The pups grab you by the collar and drag your unconscious body outside the cave." )
				it.warp( "BASSKEN_502", 520, 210 )
			}
		}			
	} else {
		myManager.schedule(2) { checkDazedStatus() }
	}
}

def nextWave() {
	currentWave ++
	println "**** WAVE ${currentWave} ****"

	//pup wave
	if( currentWave == 1 ) {
		numPups = playerSet.size()
		numWolves = 0
	}
	//light wolf wave
	if( currentWave == 2 ) {
		numPups = 0
		if( playerSet.size() / 2 <= 1 ){ numWolves = 1 } else { numWolves = ( playerSet.size() / 2 ).intValue() }
	}
	//some pups, one wolf
	if( currentWave == 3 ) {
		if( playerSet.size() / 2 <= 2 ){ numPups = 2 } else { numPups = ( playerSet.size() / 2 ).intValue() }
		numWolves = 1
	}
	//big mixed wave, mostly pups with some wolves
	if( currentWave == 4 ) {
		numPups = playerSet.size()
		if( playerSet.size() / 2 <= 1 ){ numWolves = 1 } else { numWolves = ( playerSet.size() / 2 ).intValue() }
	}
	//big pup wave
	if( currentWave == 5 ) {
		numPups = playerSet.size() * 2
		numWolves = 0
	}
	//player sized wolf wave
	if( currentWave == 6 ) {
		numPups = 0
		if( playerSet.size() == 1 ){ numWolves = 2 } else { numWolves = playerSet.size() }
	}
	//few pups, rest is wolves (up to playerSet.size * 1.5)
	if( currentWave == 7 ) {
		if( playerSet.size() * 1.5 / 2 <= 2 ){ numPups = 2 } else { numPups = ( playerSet.size() * 1.5 / 2 ).intValue() }
		if( ( playerSet.size() * 1.5 ) - numPups <= 1 ) { numWolves = 1 } else { numWolves = ( playerSet.size() * 1.5 ).intValue() - numPups }
	}
	if( currentWave < 8 ) {
		spawnPupsAndWolves()
	} else {
		spawnSheWolf()
	}
}

//These are generic routines to spawn Pups and Wolves.
//Just specify the numSpawnPup/Wolves before calling the routine.
def spawnPupsAndWolves() {
	adjustML()
	if( numPups > 0 ) {
		//Warp the spawner to a new location
		findNewSpawnPoint()
		pupSpawner.warp( "WolfDen_1", X, Y )
		pup = pupSpawner.forceSpawnNow()
		pup.addHate( random( playerSet ), 50 )
		numPups --
	}
	if( numWolves > 0 ) {
		findNewSpawnPoint()
		pupSpawner.warp( "WolfDen_1", X, Y )
		wolf = wolfSpawner.forceSpawnNow()
		wolf.addHate( random( playerSet ), 50 )
		numWolves --
	}
	if( numPups > 0 || numWolves > 0 ) {
		spawnPupsAndWolves()
	} else {
		if( sheWolfSpawned == false ) {
			packDeathWatch()
		} else {
			if( sheWolf.isDead() ) return
			//Don't overspawn the room if players ignore the pups (but spawn up to 3, even if there's only one player in the room)
			if( pupSpawner.spawnsInUse() + wolfSpawner.spawnsInUse() < playerSet.size() * 1.5 || pupSpawner.spawnsInUse() + wolfSpawner.spawnsInUse() <= 2 ) {
				//delay and spawn more pups and wolves to harrass the players
				numPups = random( playerSet.size() )
				if( playerSet.size() / 3 < 1 ) { numWolves = 0 } else { numWolves = random( ( playerSet.size() / 2 ).intValue() ) - 1 } //randomize number of wolves between 0 and 2 in full-sized group
				harasserTimer = myManager.schedule( harassDelay ){ spawnPupsAndWolves() }
			} else {
				numPups = 0
				numWolves = 0
				harasserTimer = myManager.schedule( harassDelay ){ spawnPupsAndWolves() }
			}
		}
	}
}

def packDeathWatch() {
	if( pupSpawner.spawnsInUse() + wolfSpawner.spawnsInUse() == 0 ) {
		//Rest Break!
		myManager.schedule( delay ){ nextWave() }
	} else {
		myManager.schedule(2) { packDeathWatch() }
	}
}

//pull a new X, Y from the "spawnLocationMap"
def findNewSpawnPoint() {
	newSpawn = spawnLocationMap.get( random( 10 ) )
	if( newSpawn != oldSpawn ) {
		oldSpawn = newSpawn
		X = newSpawn.get(0).intValue()
		Y = newSpawn.get(1).intValue()
	} else {
		findNewSpawnPoint()
	}
}
	

def spawnSheWolf() {
	//println "#### Ran spawnSheWolf() ####"
	playerSet.clone().each(){ it.centerPrint( "You hear a sound that chills the marrow in your bones..." ) }
	myManager.schedule(1) { sound( "sheWolfHowl" ).toZone() }
	myManager.schedule(2) { 
		sheWolf = sheWolfSpawner.forceSpawnNow()
		sheWolfSpawned = true
		sheWolf.addHate( random( playerSet ), 50 )
		runOnDeath( sheWolf ) { event ->
			event.actor.getHated().each{ hateCollector << it }
			sound( "sheWolfHowl" ).toZone()
			myManager.schedule(3) { updatesAndRewards() } 
			harasserTimer.cancel()
		}
		myManager.onHealth( sheWolf ) { event ->
			if( event.didTransition( 66 ) && event.isDecrease() ) {
				sound( "sheWolfHowl" ).toZone()
			} else if( event.didTransition( 33 ) && event.isDecrease() ) {
				sound( "sheWolfHowl" ).toZone()
			}
		}
		spawnPupsAndWolves() //harassers during She Wolf
	}
}

def updatesAndRewards() {
	if( !playerSet.isEmpty() ) {
		playerSet.clone().each() {
			it.centerPrint( "Congratulations! The She Wolf menace has been destroyed!" )
		}

		myManager.schedule(3) {
			playerSet.clone().each{ 
				//now give everyone a reward so the end of the fight is gratifying and encourages repeat play
				if( it.getConLevel() <= MaxML && hateCollector.contains( it ) ) {
					//update the task (give the axe) if necessary
					if( it.isOnQuest( 80 ) && hateCollector.contains( it ) ) {
						it.updateQuest( 80, "Gustav-VQS" )
						it.centerPrint( "You spy Gustav's axe, Mon Cheri, on the floor of the cave and pick it up." )
					}

					//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
					if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
					else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
					else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

					//gold reward
					goldGrant = ( random( 20, 45 ) * goldMult ).intValue() 
					it.centerPrint( "You receive ${goldGrant} gold!" )
					it.grantCoins( goldGrant )

					//orb reward
					orbGrant = ( random( 3, 5 ) * orbMult ).intValue()
					it.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
					it.grantQuantityItem( 100257, orbGrant )

				} else if( it.getConLevel() > MaxML && hateCollector.contains( it ) ) {
					it.centerPrint( "Your level is too high for this scenario. No reward for you!" )
				} else {
					it.centerPrint( "You need to help defeat She Wolf through damage or support in order to receive rewards." )
				}
			}
		}
	}
}
	

//=====================================
// EXIT CAVE SWITCH                    
//=====================================
onClickCaveExit = new Object()

caveExit = makeSwitch( "Cave_Out", myRooms.WolfDen_1, 1440, 910 )
caveExit.unlock()
caveExit.off()
caveExit.setRange( 250 )

def exitCave = { event ->
	synchronized( onClickCaveExit ) {
		caveExit.off()
		if( isPlayer( event.actor ) ) {
			//DEBUG!!!!
			event.actor.setCrewVar( "Z27WolfDenDifficultySelectionInProgress", 0 )
			//DEBUG!!!!
			event.actor.warp( "BASSKEN_502", 520, 210 )
		}
	}
}

caveExit.whenOn( exitCave )

//=====================================
// GAME START TRIGGER ZONE             
//=====================================
enterBlock = new Object()

def stageAreaTrigger = "stageAreaTrigger"
myRooms.WolfDen_1.createTriggerZone( stageAreaTrigger, 1020, 300, 1120, 1000 )

myManager.onTriggerIn(myRooms.WolfDen_1, stageAreaTrigger) { event ->
	synchronized( enterBlock ) {
		if( isPlayer( event.actor ) && gameStarted == false ) {
			gameStarted = true
			startGame( event )
			checkDazedStatus()
		}
	}
}
