// script copy and pasted together by bhalsted

import com.gaiaonline.mmo.battle.script.*;
import util.DmsPurge;

myScript = this

toDeadmans = makeSwitch( "DeadmansPass_In", myRooms.M1_303, 730, 205)
toDeadmans.setMouseoverText("Deadman Shadow")
toDeadmans.unlock()
toDeadmans.off()
toDeadmans.setRange(250)

def doWarp(player)
{
	player.getTeam().setAreaVar( "Z30_Undermountain", "Z30DeadmansDifficulty", 2 )
	player.centerPrint("The rusty gate opens wide enough to allow entry.")
	sound("deadmansGate").toPlayer(player)
	player.warp("Under_1", 300, 280)
}

def goToDeadmans =  { event ->
	if( isPlayer( event.actor ) ) {
		leader = event.actor.getTeam().getLeader()
		if( leader.getNaturalConLevel() >= 10.0 ) {
			if(DmsPurge.isFreePlay(myScript) || event.actor.hasQuestFlag(GLOBAL, "Z30_Deadmans_Shadow_Access")) {
				doWarp(event.actor);
			}
			else
			{
				descripString = "Would you like to use an item to enter Deadman's Shadow?"
				diffOptions = ["Yes!", "No"]
				
				//println "**** trying to create the menu ****"
				uiButtonMenu( event.actor, "dms_menu", descripString, diffOptions, 200, 30 ) { menuEvent ->
					if( menuEvent.selection == "Yes!" ) {
						runOnSoulbind(event.actor, 80553, 77351) { check ->
							if(check.result) {
								check.player.setQuestFlag(GLOBAL, "Z30_Deadmans_Shadow_Access");					
								doWarp(check.player);
							}
							else {
								check.player.centerPrint("You do not have in item which allows access to Deadman's Shadow");
								myManager.schedule(2) { check.player.centerPrint("Visit the Shop to gain access."); }	
							}
						}
					}
				}
			}
		}
		else {
			event.actor.centerPrint("Your team leader must be at least level 10.");
		}
	}
	myManager.schedule(1) { toDeadmans.off() }
}

toDeadmans.whenOn(goToDeadmans)
