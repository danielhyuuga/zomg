// script created by bhalsted

// Von Helson Twin's ID# 8189430 ([NPC] Von Helson Sisters)
//BFG-Albert

Twins = spawnNPC("[NPC] Von Helson Sisters", myRooms.M1_1, 2450, 1600);
Twins.setRotation(120);
Twins.setDisplayName("Von Helson Twins");
//Twins.setURL("http://a2.cdn.gaiaonline.com/dress-up/avatar/ava/f6/f5/8bf3c9ae7cf5f6_strip.png")

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// ! IF YOU MAKE CHANGES BELOW, MAKE THEM ALSO IN THE M1_303 SCRIPT !
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

def twinConvo = Twins.createConversation( "twinConvo", true, "!Z03VonhelsonAccepted" )
twinConvo.setUrgent(true)

def twinConvoId = 1;
twinConvo.addDialog( [
	npctext: "Anna Corinne: No, Marie, how about YOU go talk to him!",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: Don't be a baby, Anna. You're less threatening, YOU should talk to him.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Exactly the reason I SHOULDN'T! I'm still cute! Your face is already messed up, it won't do any harm if he sics his thugs on you.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: I'LL MESS UP *YOUR* FACE-",
	playertext: ". . .",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Together: WHAT DO YOU WANT?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Get gone, punk.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: Actually, hang on a second, kid.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Marie, what-?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: So kid, you look like the helpful type. You wanna do us a favor?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Oh! Yeah, sweetie, you can help us out can't you? <3",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: It's no big deal, a minor inconvenience for us really...",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Yes, I'm sure it'll be no trouble at all for someone tough like you! <3",
	playertext: "I'm... feeling a little uncomfortable...",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: Relax! You might have heard my sister and I have suffered from some... familial issues in the past few years. We used to run a pretty smooth game but with only the two of us left, it's tricky to keep track of all the pieces.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Or lack thereof...",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: See, past this gate behind us is one of our family mansions. This is ground zero Von Helson territory, got me? Used to be we never had trouble with interlopers, you know, other vampire gangs?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: But now we've got this bigshot down in the caves under Dead Man's Pass. Some kind of kingpin, named Alastor.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: He's running some big operation, right under our noses!",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: In the past Daddy would have thrown a couple of the henches down there and squared everything up. And uh, we'd do the same, except our minions...",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: They're on vacation?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: Currently tied up in other business, is what my sister means. Yeah. So like, we've gotta deal with this mess ourselves.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Or we would have, if YOU didn't show up. <3",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: So that's not the tough bit. This boss guy is pretty well connected, and he's got this assassin-",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: A BAMF assassin, total lady pimp.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: ...anyway. She has some wicked powers. She's cursed the gate so it bars anyone without a certain quality of vampiric blood from entering. It's a powerful barrier, but there's a way around it.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie:  There's these trinkets floating around, Bloodstone Amulets. One of those would let you sneak through the barrier.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: I heard you can buy them in that wagon shop in Barton, you know, from that guy that smells like horses?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	id:twinConvoId++,
	npctext: "Marie: In addition, I've heard that if you perform certain acts that generate positive energy, it weakens the barrier. If enough people build up enough of this positive energy, it knocks out the barrier temporarily, letting anyone in.",
	result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: No problem, right?",
	playertext: "So what am I meant to do, exactly?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: You know, the obvious. Sneak in, mess up their operation, knock some people around...",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Maybe knock off a couple...",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Marie: Either way, Alastor and his thugs GTFO, and we're on top again! You probably won't be injured at all! Much less brutally murdered!",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: You'll help a coupla girls out, won't you, sexy?",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	id:twinConvoId++,
	npctext: "Marie: I mean, we're not actually going to give you a choice. But we'll appreciate your cooperation all the same.",
	options: [
		[text: "Um, OK... sure.", result: twinConvoId],
		[text: "I'll think about it.", result: twinConvoId+2]
	],
], Twins )

// YES
twinConvo.addDialog( [
	npctext: "Marie: Wise choice. Now get going.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Don't get murdered!",
	flag: "Z03VonhelsonAccepted",
	id:twinConvoId++, result: DONE
], Twins )

// NO
twinConvo.addDialog( [
	npctext: "Marie: Okay. We'll give you a couple minutes to change your mind.",
	id:twinConvoId++, result: twinConvoId
], Twins )
twinConvo.addDialog( [
	npctext: "Anna Corinne: Don’t go too far, honey.",
	id:twinConvoId++, result: DONE
], Twins )

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

def doneYet = Twins.createConversation( "doneYet", true, "Z03VonhelsonAccepted", "!Z38KilledKamila" )

def doneYetId = 1;
doneYet.addDialog( [
	npctext: "Marie: Did you oust Alastor yet?",
	id:doneYetId++, result: doneYetId
], Twins )
doneYet.addDialog( [
	npctext: "Anna Corinne: No? Then why are you even speaking?",
	id:doneYetId++, result: DONE
], Twins )

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

def celebration = Twins.createConversation( "celebration", true, "Z38KilledKamila", "!Z03VonhelsonCelebrate" )

def celebrationId = 1;
celebration.addDialog( [
	npctext: "Anna Corinne: Wow! You're alive!",
	id:celebrationId++, result: celebrationId
], Twins )
celebration.addDialog( [
	npctext: "Marie: Er... Impressive. Very Impressive. You didn't exactly oust the guy like we wanted, but I suppose with his operation in shambles it's only a matter of time, right?",
	id:celebrationId++, result: celebrationId
], Twins )
celebration.addDialog( [
	npctext: "Anna Corinne: I can't believe you beat Kamila! Haha, this must be so humiliating for her! What did she say about us? I bet she cursed our names at the end!",
	playertext: "Actually you guys didn't really come up... I don't think they even know you're out here...",
	id:celebrationId++, result: celebrationId
], Twins )
celebration.addDialog( [
	npctext: "Together: What?! That SLUT!!",
	id:celebrationId++, result: celebrationId
], Twins )
celebration.addDialog( [
	npctext: "Anna Corinne: Are you disrespecting us? It sounds like you're disrespecting us!",
	id:celebrationId++, result: celebrationId
], Twins )
celebration.addDialog( [
	npctext: "Marie: Hit the road before we do something irrational, peon!",
	flag: "Z03VonhelsonCelebrate",
	id:celebrationId++, result: DONE
], Twins )

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

def annoyance = Twins.createConversation( "annoyance", true, "Z03VonhelsonCelebrate" )

def annoyanceId = 1;
annoyance.addDialog( [
	npctext: "Together: Didn't we warn you not to come back here?",
	playertext: "You: Uh... No actually.",
	id:annoyanceId++, result: annoyanceId
], Twins )
annoyance.addDialog( [
	npctext: "Anna Corinne: Oh... Well okay then.",
	id:annoyanceId++, result: annoyanceId
], Twins )
annoyance.addDialog( [
	npctext: "Marie: Um... If you feel like taking another crack at Alastor, I think he's still around.",
	id:annoyanceId++, result: DONE
], Twins )
