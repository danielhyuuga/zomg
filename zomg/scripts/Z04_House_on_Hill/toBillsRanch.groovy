import com.gaiaonline.mmo.battle.script.*;


//---------------------------------------------------------
// SWITCH FOR DEADMAN'S PASS EXIT                          
//---------------------------------------------------------
toRanch = makeSwitch("DeadmansPass_Out", myRooms.M1_407, 1000, 370)
toRanch.setMouseoverText("Deadman's Gate")
toRanch.unlock()
toRanch.off()
toRanch.setRange(200)

def goToRanch = { event ->
	if(isPlayer( event.actor)) {
		event.actor.warp("BF1_705", 270, 410)
	}
}

toRanch.whenOn(goToRanch)