//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

stooceLock = false
stoocie = null
m1RoomList = ["M1_1","M1_2","M1_4","M1_102","M1_103","M1_104","M1_204","M1_205","M1_303","M1_304","M1_305","M1_306","M1_403","M1_404","M1_405","M1_406","M1_407"]

//This makes Stooce follow
def stoocieFollow( event ) {
	if(event.actor.isOnQuest(55, 2) && !event.actor.hasQuestFlag(GLOBAL, "Z4_Stooce_Alive") ) {
		myManager.schedule(2) {
			stoocie.say( "Moo!" )
			stoocie.startFollow( event.actor )
			event.actor.centerPrint( "Stoocie will now follow you back to Rancher Bill." )
		}
		event.actor.addMiniMapQuestActorName("Rancher Bill-VQS")
		event.actor.setQuestFlag(GLOBAL, "Z4_Stooce_Alive")
	}
	/*if(event.actor.isOnQuest(55, 3) ) {
		event.actor.say( "Let's try this again, Stoocie.")
		myManager.schedule(2) { stoocie.say( "Moo moo!" ) }
		myManager.schedule(4) {
			stoocie.startFollow( event.actor )
			event.actor.centerPrint( "Stoocie will now follow you back to Rancher Bill." )
		}
		event.actor.setQuestFlag(GLOBAL, "Z4_Stooce_Alive")
	}*/
}

/*stoocie = makeCritter( "cow", myRooms.M1_303, 655, 470 )
stoocie.onUse( { event -> stoocieFollow( event ) } )
stoocie.setUsable( true )*/

//Spawn the Stooce and tell her to follow player on the quest
myManager.onEnter( myRooms.M1_303 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(55, 2) && stooceLock == false && !event.actor.hasQuestFlag(GLOBAL, "Z4_Stooce_Alive"))  {
	
		stooceLock = true
		stoocie = makeCritter( "cow", myRooms.M1_303, 655, 470 )
		stoocie.setDisplayName("Stoocie")
		stoocie.setUsable( true )
		stoocie.onUse( stoocieFollow( event ) )
	}
}

//Defined trigger zone for quest advancement when player brings Stoocie to the exit of House
def stooceTrigger = "stooceTrigger"
myRooms.M1_407.createTriggerZone(stooceTrigger, 775, 200, 1000, 620)

//Event to advance quest when player brings Stoocie to the exit of house
myManager.onTriggerIn(myRooms.M1_407, stooceTrigger) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(55, 2) && event.actor.hasQuestFlag(GLOBAL, "Z4_Stooce_Alive")) {
	
		event.actor.unsetQuestFlag(GLOBAL, "Z4_Stooce_Alive")
		if(stoocie != null) { 
			event.actor.setQuestFlag(GLOBAL, "Z4_Stooce_OutOfHouse")
		}
	}
}

myManager.onExit(myRooms.M1_407) { event ->
	if(isPlayer(event.actor) && event.actor.hasQuestFlag(GLOBAL, "Z4_Stooce_OutOfHouse") && !m1RoomList.contains(event.actor.getRoomName())) {
		//println "##### DISPOSING OF ${stoocie} #####"
		stoocie?.dispose()
	}
}