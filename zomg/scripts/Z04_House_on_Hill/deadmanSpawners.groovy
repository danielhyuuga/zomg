//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;


//---------------------------------------------------------------------
//Variables and constants for difficulty settings                      
//---------------------------------------------------------------------
deadmansDifficulty = null
defaultMonsterLevel = 0
MIN_DEFAULT_MONSTER_LEVEL = 2.5
MAX_DEFAULT_MONSTER_LEVEL = 3.6

adjustedDefaultMonsterLevel = 0
adjustedPatrolMonsterLevel = 0

grueList = []
m1RoomList = [myRooms.M1_1,myRooms.M1_2,myRooms.M1_4,myRooms.M1_102,myRooms.M1_103,myRooms.M1_104,myRooms.M1_204,myRooms.M1_205,myRooms.M1_303,myRooms.M1_304,myRooms.M1_305,myRooms.M1_306,myRooms.M1_403,myRooms.M1_404,myRooms.M1_405,myRooms.M1_406,myRooms.M1_407]
dazedSet = [] as Set

//---------------------------------------------------------------------
//Runs when first player enters DMP                                                    
//---------------------------------------------------------------------
onZoneIn() { event ->
	if(isPlayer( event.player )) {
		if(!grueList.contains( event.player )) {
			grueList << event.player
		}
		if(deadmansDifficulty == null) {
			deadmansDifficulty = event.player.getTeam().getAreaVar( "Z04_House_on_Hill", "Z04DeadmansDifficulty" );
			if(0 == deadmansDifficulty) {
				deadmansDifficulty = 1;
			}
			//println "**** deadmansDifficulty = ${ deadmansDifficulty } ****"

			//Set level and spawn default spawns
			setDeadmanDefaultMonsterLevel(event.player)
			spawnDefaultMonsters()
			
			makePatrolSpawners()
			makeAmbushSpawners()
			
			//Set level and schedule patrol spawn
			setDeadmanPatrolMonsterLevel(event.player)
			myManager.schedule(random(180, 300)) { patrolSpawn() }
		}
	}
}

onZoneOut() { event ->
	if(isPlayer(event.player)) {
		grueList.remove(event.player)
	}
}

def maintainGrueList() {
	grueList.each() {
		if(!isInZone(it)) {
			grueList.remove(it)
		}
	}
	myManager.schedule(60) { maintainGrueList() }
}

maintainGrueList()

//---------------------------------------------------------------------
//Default Spawners                                                     
//---------------------------------------------------------------------
// Spawner for roamer
spawner_2_1 = myRooms.M1_2.spawnStoppedSpawner( "spawner_2_1", "omg", 1)
spawner_2_1.setPos(570, 595)
spawner_2_1.setHome( "M1_2", 570, 595 )
spawner_2_1.setCFH( 250 )
spawner_2_1.setRFH( true )
spawner_2_1.setDispositionForChildren( "coward" )
spawner_2_1.setCowardLevelForChildren( 100 )
spawner_2_1.startWander( false )
spawner_2_1.setSpawnWhenPlayersAreInRoom(true)
spawner_2_1.setWaitTime( 275 , 325 )
spawner_2_1.childrenWander( false )
spawner_2_1.addPatrolPointForChildren( "M1_2", 570, 599, 15)
spawner_2_1.addPatrolPointForChildren( "M1_2", 530, 230, 15)
spawner_2_1.addPatrolPointForChildren( "M1_2", 200, 280, 15)
spawner_2_1.addPatrolPointForChildren( "M1_2", 530, 230, 15)

// Spawner for top right
spawner_2_2 = myRooms.M1_2.spawnStoppedSpawner( "spawner_2_2", "omg", 1)
spawner_2_2.setPos(830, 190)
spawner_2_2.setHome( "M1_2", 830, 190 )
spawner_2_2.setCFH( 100 )
spawner_2_2.startWander( false )
spawner_2_2.setSpawnWhenPlayersAreInRoom(true)
spawner_2_2.setWaitTime( 275 , 325 )
spawner_2_2.childrenWander( false )
spawner_2_2.setRotationForChildren( 270 )
spawner_2_2.setGuardPostForChildren(spawner_2_2, 250)

// Spawner for bottom right
spawner_2_3 = myRooms.M1_2.spawnStoppedSpawner( "spawner_2_3", "omg", 1)
spawner_2_3.setPos(860, 490)
spawner_2_3.setHome( "M1_2", 860, 490 )
spawner_2_3.setCFH( 100 )
spawner_2_3.startWander( false )
spawner_2_3.setSpawnWhenPlayersAreInRoom(true)
spawner_2_3.setWaitTime( 275 , 325 )
spawner_2_3.childrenWander( false )
spawner_2_3.setRotationForChildren( 270 )
spawner_2_3.setGuardPostForChildren(spawner_2_3, 250)

// Spawner for bottom left
spawner_2_4 = myRooms.M1_2.spawnStoppedSpawner( "spawner_2_4", "omg", 1)
spawner_2_4.setPos(250, 600)
spawner_2_4.setHome( "M1_2", 250, 600 )
spawner_2_4.setCFH( 100 )
spawner_2_4.startWander( false )
spawner_2_4.setSpawnWhenPlayersAreInRoom(true)
spawner_2_4.setWaitTime( 275 , 325 )
spawner_2_4.childrenWander( false )
spawner_2_4.setRotationForChildren( 270 )
spawner_2_4.setGuardPostForChildren(spawner_2_4, 45)

// Spawner for bottom right
spawner_102_1 = myRooms.M1_102.spawnStoppedSpawner( "spawner_102_1", "omg", 1)
spawner_102_1.setPos(825, 330)
spawner_102_1.setHome( "M1_102", 825, 330 )
spawner_102_1.setSpawnWhenPlayersAreInRoom(true)
spawner_102_1.setWaitTime( 275 , 325 )
spawner_102_1.childrenWander( false )
spawner_102_1.setRotationForChildren( 270 )
spawner_102_1.setGuardPostForChildren(spawner_102_1, 160)

// Spawner for bottom center
spawner_102_2 = myRooms.M1_102.spawnStoppedSpawner( "spawner_102_2", "omg", 1)
spawner_102_2.setPos(515, 460)
spawner_102_2.setHome( "M1_102", 515, 460 )
spawner_102_2.setSpawnWhenPlayersAreInRoom(true)
spawner_102_2.setWaitTime( 275 , 325 )
spawner_102_2.childrenWander( false )
spawner_102_2.setRotationForChildren( 45 )
spawner_102_2.setGuardPostForChildren(spawner_102_2, 225)

// Spawner for bottom left
spawner_102_3 = myRooms.M1_102.spawnStoppedSpawner( "spawner_102_3", "omg_LT", 1)
spawner_102_3.setPos(190, 465)
spawner_102_3.setHome( "M1_102", 190, 465 )
spawner_102_3.setSpawnWhenPlayersAreInRoom(true)
spawner_102_3.setWaitTime( 275 , 325 )
spawner_102_3.childrenWander( false )
spawner_102_3.setRotationForChildren( 270 )
spawner_102_3.setGuardPostForChildren(spawner_102_3, 160)

// Spawner for top left
spawner_102_4 = myRooms.M1_102.spawnStoppedSpawner( "spawner_102_4", "omg", 1)
spawner_102_4.setPos(250, 220)
spawner_102_4.setHome( "M1_102", 250, 220 )
spawner_102_4.setSpawnWhenPlayersAreInRoom(true)
spawner_102_4.setWaitTime( 275 , 325 )
spawner_102_4.childrenWander( false )
spawner_102_4.setRotationForChildren( 270 )
spawner_102_4.setGuardPostForChildren(spawner_102_4, 45)

// Spawner for bottom left
spawner_103_1 = myRooms.M1_103.spawnStoppedSpawner( "spawner_103_1", "omg", 1)
spawner_103_1.setPos(150, 225)
spawner_103_1.setHome( "M1_103", 150, 225 )
spawner_103_1.setSpawnWhenPlayersAreInRoom(true)
spawner_103_1.setWaitTime( 275 , 325 )
spawner_103_1.childrenWander( false )
spawner_103_1.setGuardPostForChildren(spawner_103_1, 160)

// Spawner for bottom right
spawner_103_2 = myRooms.M1_103.spawnStoppedSpawner( "spawner_103_2", "omg", 1)
spawner_103_2.setPos(930, 270)
spawner_103_2.setHome( "M1_103", 930, 270 )
spawner_103_2.setSpawnWhenPlayersAreInRoom(true)
spawner_103_2.setWaitTime( 275 , 325 )
spawner_103_2.childrenWander( false )
spawner_103_2.setGuardPostForChildren(spawner_103_2, 160)

// Spawner for roamer
spawner_103_3 = myRooms.M1_103.spawnStoppedSpawner( "spawner_103_3", "omg", 1)
spawner_103_3.setPos(750, 160)
spawner_103_3.setHome( "M1_103", 750, 160 )
spawner_103_3.setRFH( true )
spawner_103_3.setDispositionForChildren( "coward" )
spawner_103_3.setCowardLevelForChildren( 100 )
spawner_103_3.setSpawnWhenPlayersAreInRoom(true)
spawner_103_3.setWaitTime( 275 , 325 )
spawner_103_3.childrenWander( false )
spawner_103_3.addPatrolPointForChildren( "M1_103", 750, 164, 10)
spawner_103_3.addPatrolPointForChildren( "M1_103", 460, 75, 1)
spawner_103_3.addPatrolPointForChildren( "M1_102", 765, 140, 1)
spawner_103_3.addPatrolPointForChildren( "M1_103", 485, 165, 10)
spawner_103_3.addPatrolPointForChildren( "M1_102", 765, 140, 1)
spawner_103_3.addPatrolPointForChildren( "M1_103", 460, 75, 1)

// Spawner for right
spawner_104_1 = myRooms.M1_104.spawnStoppedSpawner( "spawner_104_1", "clutch", 1)
spawner_104_1.setPos(165, 210)
spawner_104_1.setHome( "M1_104", 165, 210 )
spawner_104_1.setSpawnWhenPlayersAreInRoom(true)
spawner_104_1.setWaitTime( 275 , 325 )
spawner_104_1.childrenWander( false )
spawner_104_1.setGuardPostForChildren(spawner_104_1, 315)

// Spawner for roamer
spawner_104_2 = myRooms.M1_104.spawnStoppedSpawner( "spawner_104_2", "clutch", 1)
spawner_104_2.setPos(430, 135)
spawner_104_2.setHome( "M1_104", 430, 135 )
spawner_104_2.setRFH( true )
spawner_104_2.setDispositionForChildren( "coward" )
spawner_104_2.setCowardLevelForChildren( 50 )
spawner_104_2.setSpawnWhenPlayersAreInRoom(true)
spawner_104_2.setWaitTime( 275 , 325 )
spawner_104_2.childrenWander( false )
spawner_104_2.addPatrolPointForChildren( "M1_104", 165, 210, 10)
spawner_104_2.addPatrolPointForChildren( "M1_104", 900, 595, 10)
spawner_104_2.addPatrolPointForChildren( "M1_104", 810, 220, 10)
spawner_104_2.addPatrolPointForChildren( "M1_104", 900, 595, 10)

// Spawner for path roamer
spawner_205_1 = myRooms.M1_205.spawnStoppedSpawner( "spawner_205_1", "snake", 1)
spawner_205_1.setPos(610, 640)
spawner_205_1.setHome( "M1_205", 610, 640 )
spawner_205_1.setRFH( true )
spawner_205_1.setDispositionForChildren( "coward" )
spawner_205_1.setCowardLevelForChildren( 50 )
spawner_205_1.setSpawnWhenPlayersAreInRoom(true)
spawner_205_1.setWaitTime( 275 , 325 )
spawner_205_1.childrenWander( false )
spawner_205_1.setGuardPostForChildren(spawner_205_1, 160)

// Spawner for right
spawner_205_2 = myRooms.M1_205.spawnStoppedSpawner( "spawner_205_2", "snake", 1)
spawner_205_2.setPos(820, 590)
spawner_205_2.setHome( "M1_205", 820, 590 )
spawner_205_2.setSpawnWhenPlayersAreInRoom(true)
spawner_205_2.setWaitTime( 275 , 325 )
spawner_205_2.childrenWander( false )
spawner_205_2.setGuardPostForChildren(spawner_205_2, 160)

// Spawner for right
spawner_205_3 = myRooms.M1_205.spawnStoppedSpawner( "spawner_205_3", "snake_LT", 1)
spawner_205_3.setPos(400, 590)
spawner_205_3.setHome( "M1_205", 400, 590 )
spawner_205_3.setSpawnWhenPlayersAreInRoom(true)
spawner_205_3.setWaitTime( 275 , 325 )
spawner_205_3.childrenWander( false )
spawner_205_3.setGuardPostForChildren(spawner_205_3, 90)

// Spawner for path roamer
spawner_303_1 = myRooms.M1_303.spawnStoppedSpawner( "spawner_303_1", "clutch", 1)
spawner_303_1.setPos(545, 365)
spawner_303_1.setHome( "M1_303", 545, 365 )
spawner_303_1.setRFH( true )
spawner_303_1.setDispositionForChildren( "coward" )
spawner_303_1.setCowardLevelForChildren( 50 )
spawner_303_1.setSpawnWhenPlayersAreInRoom(true)
spawner_303_1.setWaitTime( 275 , 325 )
spawner_303_1.childrenWander( false )
spawner_303_1.addPatrolPointForChildren( "M1_303", 385, 285, 10)
spawner_303_1.addPatrolPointForChildren( "M1_303", 685, 425, 10)
spawner_303_1.addPatrolPointForChildren( "M1_304", 90, 380, 10)
spawner_303_1.addPatrolPointForChildren( "M1_303", 685, 425, 10)

// Spawner for bottom left
spawner_303_2 = myRooms.M1_303.spawnStoppedSpawner( "spawner_303_2", "clutch", 1)
spawner_303_2.setPos(450, 410)
spawner_303_2.setHome( "M1_303", 450, 410 )
spawner_303_2.setSpawnWhenPlayersAreInRoom(true)
spawner_303_2.setWaitTime( 275 , 325 )
spawner_303_2.childrenWander( false )
spawner_303_2.setGuardPostForChildren(spawner_303_2, 90)

// Spawner for top right
spawner_303_3 = myRooms.M1_303.spawnStoppedSpawner( "spawner_303_3", "clutch", 1)
spawner_303_3.setPos(820, 330)
spawner_303_3.setHome( "M1_303", 820, 330 )
spawner_303_3.setSpawnWhenPlayersAreInRoom(true)
spawner_303_3.setWaitTime( 275 , 325 )
spawner_303_3.childrenWander( false )
spawner_303_3.setGuardPostForChildren(spawner_303_3, 160)

// Spawner for bottom
spawner_304_1 = myRooms.M1_304.spawnStoppedSpawner( "spawner_304_1", "clutch", 1)
spawner_304_1.setPos(125, 570)
spawner_304_1.setHome( "M1_304", 125, 570 )
spawner_304_1.setSpawnWhenPlayersAreInRoom(true)
spawner_304_1.setWaitTime( 275 , 325 )
spawner_304_1.childrenWander( false )
spawner_304_1.setGuardPostForChildren(spawner_304_1, 160)

// Spawner for top
spawner_304_2 = myRooms.M1_304.spawnStoppedSpawner( "spawner_304_2", "clutch", 1)
spawner_304_2.setPos(145, 270)
spawner_304_2.setHome( "M1_304", 145, 270 )
spawner_304_2.setSpawnWhenPlayersAreInRoom(true)
spawner_304_2.setWaitTime( 275 , 325 )
spawner_304_2.childrenWander( false )
spawner_304_2.setGuardPostForChildren(spawner_304_2, 225)

// Spawner for roamer
spawner_304_3 = myRooms.M1_304.spawnStoppedSpawner( "spawner_304_3", "clutch_LT", 1)
spawner_304_3.setPos(550, 605)
spawner_304_3.setHome( "M1_304", 550, 605 )
spawner_304_3.setRFH( true )
spawner_304_3.setDispositionForChildren( "coward" )
spawner_304_3.setCowardLevelForChildren( 50 )
spawner_304_3.setSpawnWhenPlayersAreInRoom(true)
spawner_304_3.setWaitTime( 275 , 325 )
spawner_304_3.childrenWander( false )
spawner_304_3.addPatrolPointForChildren( "M1_304", 550, 609, 10)
spawner_304_3.addPatrolPointForChildren( "M1_304", 800, 365, 10)
spawner_304_3.addPatrolPointForChildren( "M1_204", 620, 350, 10)
spawner_304_3.addPatrolPointForChildren( "M1_304", 800, 365, 10)

// Spawner for path roamer
spawner_305_1 = myRooms.M1_305.spawnStoppedSpawner( "spawner_305_1", "snake", 1)
spawner_305_1.setPos(655, 455)
spawner_305_1.setHome( "M1_305", 655, 455 )
spawner_305_1.setRFH( true )
spawner_305_1.setDispositionForChildren( "coward" )
spawner_305_1.setCowardLevelForChildren( 50 )
spawner_305_1.setSpawnWhenPlayersAreInRoom(true)
spawner_305_1.setWaitTime( 275 , 325 )
spawner_305_1.childrenWander( false )
spawner_305_1.addPatrolPointForChildren( "M1_405", 190, 135, 15)
spawner_305_1.addPatrolPointForChildren( "M1_305", 600, 590, 1)
spawner_305_1.addPatrolPointForChildren( "M1_305", 730, 325, 15)
spawner_305_1.addPatrolPointForChildren( "M1_305", 600, 590, 1)

// Spawner for bottom left
spawner_305_2 = myRooms.M1_305.spawnStoppedSpawner( "spawner_305_2", "snake", 1)
spawner_305_2.setPos(365, 475)
spawner_305_2.setHome( "M1_305", 365, 475 )
spawner_305_2.setSpawnWhenPlayersAreInRoom(true)
spawner_305_2.setWaitTime( 275 , 325 )
spawner_305_2.childrenWander( false )
spawner_305_2.setGuardPostForChildren(spawner_305_2, 90)

// Spawner for bottom right
spawner_305_3 = myRooms.M1_305.spawnStoppedSpawner( "spawner_305_3", "snake", 1)
spawner_305_3.setPos(850, 455)
spawner_305_3.setHome( "M1_305", 850, 455 )
spawner_305_3.setSpawnWhenPlayersAreInRoom(true)
spawner_305_3.setWaitTime( 275 , 325 )
spawner_305_3.childrenWander( false )
spawner_305_3.setGuardPostForChildren(spawner_305_3, 160)

// Spawner for top left
spawner_305_4 = myRooms.M1_305.spawnStoppedSpawner( "spawner_305_4", "snake", 1)
spawner_305_4.setPos(470, 250)
spawner_305_4.setHome( "M1_305", 470, 250 )
spawner_305_4.setSpawnWhenPlayersAreInRoom(true)
spawner_305_4.setWaitTime( 275 , 325 )
spawner_305_4.childrenWander( false )
spawner_305_4.setGuardPostForChildren(spawner_305_4, 90)

// Spawner for bottom right
spawner_305_5 = myRooms.M1_305.spawnStoppedSpawner( "spawner_305_5", "snake", 1)
spawner_305_5.setPos(955, 220)
spawner_305_5.setHome( "M1_305", 955, 220 )
spawner_305_5.setSpawnWhenPlayersAreInRoom(true)
spawner_305_5.setWaitTime( 275 , 325 )
spawner_305_5.childrenWander( false )
spawner_305_5.setGuardPostForChildren(spawner_305_5, 160)

// Spawner for right
spawner_306_1 = myRooms.M1_306.spawnStoppedSpawner( "spawner_306_1", "snake", 1)
spawner_306_1.setPos(140, 290)
spawner_306_1.setHome( "M1_306", 140, 290 )
spawner_306_1.setSpawnWhenPlayersAreInRoom(true)
spawner_306_1.setWaitTime( 275 , 325 )
spawner_306_1.childrenWander( false )
spawner_306_1.setGuardPostForChildren(spawner_306_1, 160)

// Spawner for right
spawner_306_2 = myRooms.M1_306.spawnStoppedSpawner( "spawner_306_2", "snake", 1)
spawner_306_2.setPos(250, 565)
spawner_306_2.setHome( "M1_306", 250, 565 )
spawner_306_2.setSpawnWhenPlayersAreInRoom(true)
spawner_306_2.setWaitTime( 275 , 325 )
spawner_306_2.childrenWander( false )
spawner_306_2.setGuardPostForChildren(spawner_306_2, 160)

// Spawner for path roamer
spawner_403_1 = myRooms.M1_403.spawnStoppedSpawner( "spawner_403_1", "snake", 1)
spawner_403_1.setPos(890, 445)
spawner_403_1.setHome( "M1_403", 890, 445 )
spawner_403_1.setRFH( true )
spawner_403_1.setDispositionForChildren( "coward" )
spawner_403_1.setCowardLevelForChildren( 50 )
spawner_403_1.setSpawnWhenPlayersAreInRoom(true)
spawner_403_1.setWaitTime( 275 , 325 )
spawner_403_1.childrenWander( false )
spawner_403_1.addPatrolPointForChildren( "M1_403", 890, 450, 20)
spawner_403_1.addPatrolPointForChildren( "M1_303", 670, 490, 20)

// Spawner for path roamer
spawner_404_1 = myRooms.M1_404.spawnStoppedSpawner( "spawner_404_1", "snake", 1)
spawner_404_1.setPos(725, 360)
spawner_404_1.setHome( "M1_404", 725, 360 )
spawner_404_1.setRFH( true )
spawner_404_1.setDispositionForChildren( "coward" )
spawner_404_1.setCowardLevelForChildren( 50 )
spawner_404_1.setSpawnWhenPlayersAreInRoom(true)
spawner_404_1.setWaitTime( 275 , 325 )
spawner_404_1.childrenWander( false )
spawner_404_1.addPatrolPointForChildren( "M1_404", 960, 260, 10)
spawner_404_1.addPatrolPointForChildren( "M1_404", 550, 420, 10)
spawner_404_1.addPatrolPointForChildren( "M1_404", 65, 450, 10)
spawner_404_1.addPatrolPointForChildren( "M1_404", 550, 420, 10)

// Spawner for top left
spawner_404_2 = myRooms.M1_404.spawnStoppedSpawner( "spawner_404_2", "clutch", 1)
spawner_404_2.setPos(280, 350)
spawner_404_2.setHome( "M1_404", 280, 350 )
spawner_404_2.setSpawnWhenPlayersAreInRoom(true)
spawner_404_2.setWaitTime( 275 , 325 )
spawner_404_2.childrenWander( false )
spawner_404_2.setGuardPostForChildren(spawner_404_2, 90)

// Spawner for top right
spawner_404_3 = myRooms.M1_404.spawnStoppedSpawner( "spawner_404_3", "clutch", 1)
spawner_404_3.setPos(675, 200)
spawner_404_3.setHome( "M1_404", 675, 200 )
spawner_404_3.setSpawnWhenPlayersAreInRoom(true)
spawner_404_3.setWaitTime( 275 , 325 )
spawner_404_3.childrenWander( false )
spawner_404_3.setGuardPostForChildren(spawner_404_3, 160)

// Spawner for top right
spawner_404_4 = myRooms.M1_404.spawnStoppedSpawner( "spawner_404_4", "snake", 1)
spawner_404_4.setPos(665, 540)
spawner_404_4.setHome( "M1_404", 665, 540 )
spawner_404_4.setSpawnWhenPlayersAreInRoom(true)
spawner_404_4.setWaitTime( 275 , 325 )
spawner_404_4.childrenWander( false )
spawner_404_4.setGuardPostForChildren(spawner_404_4, 160)

// Spawner for path roamer
spawner_405_1 = myRooms.M1_405.spawnStoppedSpawner( "spawner_405_1", "snake", 1)
spawner_405_1.setPos(375, 360)
spawner_405_1.setHome( "M1_405", 375, 360 )
spawner_405_1.setRFH( true )
spawner_405_1.setDispositionForChildren( "coward" )
spawner_405_1.setCowardLevelForChildren( 50 )
spawner_405_1.setSpawnWhenPlayersAreInRoom(true)
spawner_405_1.setWaitTime( 275 , 325 )
spawner_405_1.childrenWander( false )
spawner_405_1.addPatrolPointForChildren( "M1_405", 750, 360, 25)
spawner_405_1.addPatrolPointForChildren( "M1_405", 100, 360, 25)

// Spawner for bottom center
spawner_405_2 = myRooms.M1_405.spawnStoppedSpawner( "spawner_405_2", "snake", 1)
spawner_405_2.setPos(340, 430)
spawner_405_2.setRotationForChildren(90)
spawner_405_2.setHome( "M1_405", 340, 430 )
spawner_405_2.setSpawnWhenPlayersAreInRoom(true)
spawner_405_2.setWaitTime( 275 , 325 )
spawner_405_2.childrenWander( false )
spawner_405_2.setGuardPostForChildren(spawner_405_2, 90)

// Spawner for top center
spawner_405_3 = myRooms.M1_405.spawnStoppedSpawner( "spawner_405_3", "snake", 1)
spawner_405_3.setPos(510, 310)
spawner_405_3.setRotationForChildren(90)
spawner_405_3.setHome( "M1_405", 510, 310 )
spawner_405_3.setSpawnWhenPlayersAreInRoom(true)
spawner_405_3.setWaitTime( 275 , 325 )
spawner_405_3.childrenWander( false )
spawner_405_3.setGuardPostForChildren(spawner_405_3, 90)

// Spawner for path roamer
spawner_406_1 = myRooms.M1_406.spawnStoppedSpawner( "spawner_406_1", "snake", 1)
spawner_406_1.setPos(500, 360)
spawner_406_1.setHome( "M1_406", 500, 360 )
spawner_406_1.setRFH( true )
spawner_406_1.setDispositionForChildren( "coward" )
spawner_406_1.setCowardLevelForChildren( 50 )
spawner_406_1.setSpawnWhenPlayersAreInRoom(true)
spawner_406_1.setWaitTime( 275 , 325 )
spawner_406_1.childrenWander( false )
spawner_406_1.addPatrolPointForChildren( "M1_406", 900, 360, 25)
spawner_406_1.addPatrolPointForChildren( "M1_406", 80, 360, 25)

// Spawner for upper right
spawner_406_2 = myRooms.M1_406.spawnStoppedSpawner( "spawner_406_2", "snake", 1)
spawner_406_2.setPos(980, 310)
spawner_406_2.setHome( "M1_406", 980, 310 )
spawner_406_2.setSpawnWhenPlayersAreInRoom(true)
spawner_406_2.setWaitTime( 275 , 325 )
spawner_406_2.childrenWander( false )
spawner_406_2.setGuardPostForChildren(spawner_406_2, 90)

// Spawner for upper right
spawner_406_3 = myRooms.M1_406.spawnStoppedSpawner( "spawner_406_3", "snake", 1)
spawner_406_3.setPos(860, 430)
spawner_406_3.setHome( "M1_406", 860, 430 )
spawner_406_3.setSpawnWhenPlayersAreInRoom(true)
spawner_406_3.setWaitTime( 275 , 325 )
spawner_406_3.childrenWander( false )
spawner_406_3.setGuardPostForChildren(spawner_406_3, 90)

// Spawner for upper left
spawner_406_4 = myRooms.M1_406.spawnStoppedSpawner( "spawner_406_4", "snake", 1)
spawner_406_4.setPos(190, 210)
spawner_406_4.setHome( "M1_406", 190, 210 )
spawner_406_4.setSpawnWhenPlayersAreInRoom(true)
spawner_406_4.setWaitTime( 275 , 325 )
spawner_406_4.childrenWander( false )
spawner_406_4.setGuardPostForChildren(spawner_406_4, 160)

// Spawner for lower left
spawner_406_5 = myRooms.M1_406.spawnStoppedSpawner( "spawner_406_5", "snake", 1)
spawner_406_5.setPos(65, 430)
spawner_406_5.setHome( "M1_406", 65, 430 )
spawner_406_5.setSpawnWhenPlayersAreInRoom(true)
spawner_406_5.setWaitTime( 275 , 325 )
spawner_406_5.childrenWander( false )
spawner_406_5.setGuardPostForChildren(spawner_406_5, 180)

// Spawner for path roamer
spawner_407_1 = myRooms.M1_407.spawnStoppedSpawner( "spawner_407_1", "snake", 1)
spawner_407_1.setPos(65, 360)
spawner_407_1.setHome( "M1_407", 65, 360 )
spawner_407_1.setRFH( true )
spawner_407_1.setDispositionForChildren( "coward" )
spawner_407_1.setCowardLevelForChildren( 50 )
spawner_407_1.setSpawnWhenPlayersAreInRoom(true)
spawner_407_1.setWaitTime( 275 , 325 )
spawner_407_1.childrenWander( false )
spawner_407_1.addPatrolPointForChildren( "M1_407", 65, 364, 25)
spawner_407_1.addPatrolPointForChildren( "M1_407", 525, 360, 25)

// Spawner for upper left
spawner_407_2 = myRooms.M1_407.spawnStoppedSpawner( "spawner_407_2", "snake", 1)
spawner_407_2.setPos(100, 220)
spawner_407_2.setHome( "M1_407", 100, 220 )
spawner_407_2.setSpawnWhenPlayersAreInRoom(true)
spawner_407_2.setWaitTime( 275 , 325 )
spawner_407_2.childrenWander( false )
spawner_407_2.addPatrolPointForChildren( "M1_407", 170, 260, 20)
spawner_407_2.addPatrolPointForChildren( "M1_407", 65, 265, 20)
spawner_407_2.addPatrolPointForChildren( "M1_407", 100, 224, 20)

// Spawner for bottom left
spawner_407_3 = myRooms.M1_407.spawnStoppedSpawner( "spawner_407_3", "snake", 1)
spawner_407_3.setPos(220, 620)
spawner_407_3.setHome( "M1_407", 220, 590 )
spawner_407_3.setSpawnWhenPlayersAreInRoom(true)
spawner_407_3.setWaitTime( 275 , 325 )
spawner_407_3.childrenWander( false )
spawner_407_3.addPatrolPointForChildren( "M1_407", 270, 630, 10)
spawner_407_3.addPatrolPointForChildren( "M1_407", 220, 500, 15)
spawner_407_3.addPatrolPointForChildren( "M1_407", 65, 500, 10)
spawner_407_3.addPatrolPointForChildren( "M1_407", 220, 500, 15)
spawner_407_3.addPatrolPointForChildren( "M1_407", 220, 610, 10)

// Spawner for bottom center
spawner_407_4 = myRooms.M1_407.spawnStoppedSpawner( "spawner_407_4", "snake", 1)
spawner_407_4.setPos(665, 595)
spawner_407_4.setHome( "M1_407", 665, 595 )
spawner_407_4.setSpawnWhenPlayersAreInRoom(true)
spawner_407_4.setWaitTime( 275 , 325 )
spawner_407_4.childrenWander( false )
spawner_407_4.addPatrolPointForChildren( "M1_407", 580, 500, 10)
spawner_407_4.addPatrolPointForChildren( "M1_407", 580, 595, 15)
spawner_407_4.addPatrolPointForChildren( "M1_407", 665, 599, 10)

// Spawner for top center
spawner_407_5 = myRooms.M1_407.spawnStoppedSpawner( "spawner_407_5", "snake", 1)
spawner_407_5.setPos(580, 265)
spawner_407_5.setHome( "M1_407", 580, 265 )
spawner_407_5.setSpawnWhenPlayersAreInRoom(true)
spawner_407_5.setWaitTime( 275 , 325 )
spawner_407_5.childrenWander( false )
spawner_407_5.addPatrolPointForChildren( "M1_407", 665, 220, 15)
spawner_407_5.addPatrolPointForChildren( "M1_407", 485, 220, 10)
spawner_407_5.addPatrolPointForChildren( "M1_407", 580, 269, 15)

//---------------------------------------------------------------------
//Default Logic                                                       
//---------------------------------------------------------------------
def setDeadmanDefaultMonsterLevel(player) {
	player.getCrew().clone().each() { //Checks the con level of each crew member and sets ML to that value - if anyone higher sets to max ML
		if(it.getConLevel() > defaultMonsterLevel && it.getConLevel() <= MAX_DEFAULT_MONSTER_LEVEL) {
			if(it.getConLevel() <= MIN_DEFAULT_MONSTER_LEVEL) {
				defaultMonsterLevel = MIN_DEFAULT_MONSTER_LEVEL
			} else {
				defaultMonsterLevel = it.getConLevel()
			}
			if(deadmansDifficulty == 1) { adjustedDefaultMonsterLevel = defaultMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedDefaultMonsterLevel = defaultMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedDefaultMonsterLevel = defaultMonsterLevel + 0.5 }
			changeDeadmanDefaultMonsterLevel()
		} else if(it.getConLevel() > MAX_DEFAULT_MONSTER_LEVEL) {
			defaultMonsterLevel = MAX_DEFAULT_MONSTER_LEVEL
			if(deadmansDifficulty == 1) { adjustedDefaultMonsterLevel = defaultMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedDefaultMonsterLevel = defaultMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedDefaultMonsterLevel = defaultMonsterLevel + 0.5 }
			changeDeadmanDefaultMonsterLevel()
		}
	}
	//println "#### defaultMonsterLevel = ${defaultMonsterLevel} ####"
	
	myManager.schedule(30) { setDeadmanDefaultMonsterLevel(player) }
}

def changeDeadmanDefaultMonsterLevel() {
	//println "#### Changing default monster level to ${defaultMonsterLevel} ####"
	spawner_2_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_2_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_2_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_2_4.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_102_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_102_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_102_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_102_4.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_103_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_103_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_103_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_104_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_104_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_205_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_205_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_205_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_303_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_303_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_303_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_304_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_304_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_304_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_305_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_305_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_305_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_305_4.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_305_5.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_306_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_306_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_403_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_404_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_404_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_404_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_404_4.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_405_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_405_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_405_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_406_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_406_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_406_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_406_4.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_406_5.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_407_1.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_407_2.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_407_3.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_407_4.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
	spawner_407_5.setMonsterLevelForEveryone( adjustedDefaultMonsterLevel )
}

def spawnDefaultMonsters() {
	//M1_2
	spawner_2_1.forceSpawnNow()
	spawner_2_2.forceSpawnNow()
	spawner_2_3.forceSpawnNow()
	spawner_2_4.forceSpawnNow()

	//M1_102
	spawner_102_1.forceSpawnNow()
	spawner_102_2.forceSpawnNow()
	spawner_102_3.forceSpawnNow()
	spawner_102_4.forceSpawnNow()

	//M1_103
	spawner_103_1.forceSpawnNow()
	spawner_103_2.forceSpawnNow()
	spawner_103_3.forceSpawnNow()

	//M1_104
	spawner_104_1.forceSpawnNow()
	spawner_104_2.forceSpawnNow()

	//M1_205
	spawner_205_1.forceSpawnNow()
	spawner_205_2.forceSpawnNow()
	spawner_205_3.forceSpawnNow()

	//M1_303
	spawner_303_1.forceSpawnNow()
	spawner_303_2.forceSpawnNow()
	spawner_303_3.forceSpawnNow()

	//M1_304
	spawner_304_1.forceSpawnNow()
	spawner_304_2.forceSpawnNow()
	spawner_304_3.forceSpawnNow()

	//M1_305
	spawner_305_1.forceSpawnNow()
	spawner_305_2.forceSpawnNow()
	spawner_305_3.forceSpawnNow()
	spawner_305_4.forceSpawnNow()
	spawner_305_5.forceSpawnNow()

	//M1_306
	spawner_306_1.forceSpawnNow()
	spawner_306_2.forceSpawnNow()

	//M1_306
	spawner_403_1.forceSpawnNow()

	//M1_404
	spawner_404_1.forceSpawnNow()
	spawner_404_2.forceSpawnNow()
	spawner_404_3.forceSpawnNow()
	spawner_404_4.forceSpawnNow()

	//M1_405
	spawner_405_1.forceSpawnNow()
	spawner_405_2.forceSpawnNow()
	spawner_405_3.forceSpawnNow()

	//M1_406
	spawner_406_1.forceSpawnNow()
	spawner_406_2.forceSpawnNow()
	spawner_406_3.forceSpawnNow()
	spawner_406_4.forceSpawnNow()
	spawner_406_5.forceSpawnNow()

	//M1_407
	spawner_407_1.forceSpawnNow()
	spawner_407_2.forceSpawnNow()
	spawner_407_3.forceSpawnNow()
	spawner_407_4.forceSpawnNow()
	spawner_407_5.forceSpawnNow()
}

//---------------------------------------------------------------------
//Ambush Spawners                                                      
//---------------------------------------------------------------------
def makeAmbushSpawners() {
	omgAmbushMonsterType = "omg"
	clutchAmbushMonsterType = "clutch"
	
	getAmbushMonsterType()

	//OMG Ambush Spawners
	omgAmbush1Spawner = myRooms.M1_4.spawnStoppedSpawner("omgAmbush1Spawner", omgAmbushMonsterType, 1)
	omgAmbush1Spawner.setPos(250, 585)
	omgAmbush1Spawner.setSpawnWhenPlayersAreInRoom(true)
	omgAmbush1Spawner.setMonsterLevelForChildren(2.7)
	
	getAmbushMonsterType()

	omgAmbush2Spawner = myRooms.M1_103.spawnStoppedSpawner("omgAmbush2Spawner", omgAmbushMonsterType, 1)
	omgAmbush2Spawner.setPos(1000, 165)
	omgAmbush2Spawner.setSpawnWhenPlayersAreInRoom(true)
	omgAmbush2Spawner.setMonsterLevelForChildren(2.8)
	
	getAmbushMonsterType()

	omgAmbush3Spawner = myRooms.M1_104.spawnStoppedSpawner("omgAmbush3Spawner", omgAmbushMonsterType, 1)
	omgAmbush3Spawner.setPos(220, 25)
	omgAmbush3Spawner.setSpawnWhenPlayersAreInRoom(true)
	omgAmbush3Spawner.setMonsterLevelForChildren(2.7)
	
	getAmbushMonsterType()

	//Clutch Ambush Spawners
	clutchAmbush1Spawner1 = myRooms.M1_205.spawnStoppedSpawner( "clutchAmbush1Spawner1", clutchAmbushMonsterType, 1)
	clutchAmbush1Spawner1.setPos(50, 550)
	clutchAmbush1Spawner1.setHome( "M1_205", 50, 550 )
	clutchAmbush1Spawner1.setSpawnWhenPlayersAreInRoom( true )
	clutchAmbush1Spawner1.childrenWander( false )
	clutchAmbush1Spawner1.setGuardPostForChildren(clutchAmbush1Spawner1, 160)
	clutchAmbush1Spawner1.setMonsterLevelForChildren( 2.7 )
	
	getAmbushMonsterType()

	clutchAmbush1Spawner2 = myRooms.M1_104.spawnStoppedSpawner( "clutchAmbush1Spawner2", clutchAmbushMonsterType, 1)
	clutchAmbush1Spawner2.setPos(475, 635)
	clutchAmbush1Spawner2.setHome( "M1_104", 475, 635 )
	clutchAmbush1Spawner2.setSpawnWhenPlayersAreInRoom( true )
	clutchAmbush1Spawner2.childrenWander( false )
	clutchAmbush1Spawner2.setGuardPostForChildren(clutchAmbush1Spawner2, 160)
	clutchAmbush1Spawner2.setMonsterLevelForChildren( 2.7 )
	
	getAmbushMonsterType()

	clutchAmbush2Spawner1 = myRooms.M1_4.spawnStoppedSpawner( "clutchAmbush2Spawner1", clutchAmbushMonsterType, 1)
	clutchAmbush2Spawner1.setPos(605, 575)
	clutchAmbush2Spawner1.setHome( "M1_4", 605, 575 )
	clutchAmbush2Spawner1.setSpawnWhenPlayersAreInRoom( true )
	clutchAmbush2Spawner1.childrenWander( false )
	clutchAmbush2Spawner1.setGuardPostForChildren(clutchAmbush2Spawner1, 160)
	clutchAmbush2Spawner1.setMonsterLevelForChildren( 2.7 )
	
	getAmbushMonsterType()

	clutchAmbush2Spawner2 = myRooms.M1_204.spawnStoppedSpawner( "clutchAmbush2Spawner2", clutchAmbushMonsterType, 1)
	clutchAmbush2Spawner2.setPos(360, 25)
	clutchAmbush2Spawner2.setHome( "M1_204", 360, 25 )
	clutchAmbush2Spawner2.setSpawnWhenPlayersAreInRoom( true )
	clutchAmbush2Spawner2.childrenWander( false )
	clutchAmbush2Spawner2.setGuardPostForChildren(clutchAmbush2Spawner2, 160)
	clutchAmbush2Spawner2.setMonsterLevelForChildren( 2.7 )
	
	getAmbushMonsterType()

	clutchAmbush3Spawner1 = myRooms.M1_303.spawnStoppedSpawner( "clutchAmbush3Spawner1", clutchAmbushMonsterType, 1)
	clutchAmbush3Spawner1.setPos(970, 600)
	clutchAmbush3Spawner1.setHome( "M1_303", 970, 600 )
	clutchAmbush3Spawner1.setSpawnWhenPlayersAreInRoom( true )
	clutchAmbush3Spawner1.childrenWander( false )
	clutchAmbush3Spawner1.setGuardPostForChildren(clutchAmbush3Spawner1, 160)
	clutchAmbush3Spawner1.setMonsterLevelForChildren( 2.6 )
	
	getAmbushMonsterType()

	clutchAmbush3Spawner2 = myRooms.M1_403.spawnStoppedSpawner( "clutchAmbush3Spawner2", clutchAmbushMonsterType, 1)
	clutchAmbush3Spawner2.setPos(325, 35)
	clutchAmbush3Spawner2.setHome( "M1_403", 325, 35 )
	clutchAmbush3Spawner2.setSpawnWhenPlayersAreInRoom( true )
	clutchAmbush3Spawner2.childrenWander( false )
	clutchAmbush3Spawner2.setGuardPostForChildren(clutchAmbush3Spawner2, 160)
	clutchAmbush3Spawner2.setMonsterLevelForChildren( 2.6 )
}

def getAmbushMonsterType() {
	if(deadmansDifficulty == 3) { //Determines whether grunt or LT spawns
		ambushMonsterRoll = random(1, 2)
		
		if(ambushMonsterRoll == 1) { omgAmbushMonsterType = "omg_LT" }
		if(ambushMonsterRoll == 2) { omgAmbushMonsterType = "omg" }
		
		if(ambushMonsterRoll == 1) { clutchAmbushMonsterType = "clutch_LT" }
		if(ambushMonsterRoll == 2) { clutchAmbushMonsterType = "clutch" }
	}
}

//---------------------------------------------------------------------
//Ambush Logic                                                       
//---------------------------------------------------------------------
omgAmbush1Spawned = false
omgAmbush2Spawned = false
omgAmbush3Spawned = false
clutchAmbush1Spawned = false
clutchAmbush2Spawned = false
clutchAmbush3Spawned = false
ambushMonsterRoll = 1

ambushMonsterLevel = 0
MIN_AMBUSH_MONSTER_LEVEL = 2.5
MAX_AMBUSH_MONSTER_LEVEL = 3.5

clutchAmbushMonsterList = ["clutch", "clutch_LT"]

//OMG Ambush Logic
omgAmbush1 = "omgAmbush1"
myRooms.M1_104.createTriggerZone(omgAmbush1, 60, 25, 175, 225)
myManager.onTriggerIn(myRooms.M1_104, omgAmbush1) { event ->
	if(isPlayer(event.actor) && omgAmbush1Spawned == false) { 
		omgAmbush1Spawned = true
		
		setDeadmanAmbushMonsterLevel(event)
		
		omgAmbush1Monster = omgAmbush1Spawner.forceSpawnNow()
		
		event.actor.centerPrint("OMG, what was that sound?!")
		omgAmbush1Monster.goAggroOn(event.actor)
	}
}

omgAmbush2 = "omgAmbush2"
myRooms.M1_103.createTriggerZone(omgAmbush2, 410, 30, 520, 190)
myManager.onTriggerIn(myRooms.M1_103, omgAmbush2) { event ->
	if(isPlayer(event.actor) && omgAmbush2Spawned == false) { 
		omgAmbush2Spawned = true
		
		setDeadmanAmbushMonsterLevel(event)
		
		omgAmbush2Monster = omgAmbush2Spawner.forceSpawnNow()
		
		event.actor.centerPrint("OMG, what was that sound?!")
		omgAmbush2Monster.goAggroOn(event.actor)
	}
}

omgAmbush3 = "omgAmbush3"
myRooms.M1_4.createTriggerZone(omgAmbush3, 320, 380, 560, 480)
myManager.onTriggerIn(myRooms.M1_4, omgAmbush3) { event ->
	if(isPlayer(event.actor) && omgAmbush3Spawned == false) { 
		omgAmbush3Spawned = true
		
		setDeadmanAmbushMonsterLevel(event)
		
		omgAmbush3Monster = omgAmbush3Spawner.forceSpawnNow()
		
		event.actor.centerPrint("OMG, what was that sound?!")
		omgAmbush3Monster.goAggroOn(event.actor)
	}
}

//Clutch Ambush Logic                                                      
clutchAmbush1 = "clutchAmbush1"
myRooms.M1_204.createTriggerZone(clutchAmbush1, 500, 300, 700, 400)
myManager.onTriggerIn(myRooms.M1_204, clutchAmbush1) { event ->
	if(isPlayer(event.actor) && clutchAmbush1Spawned == false) {
		clutchAmbush1Spawned = true
		
		setDeadmanAmbushMonsterLevel(event)

		clutchAmbush1Monster1 = clutchAmbush1Spawner1.forceSpawnNow()

		clutchAmbush1Monster2 = clutchAmbush1Spawner2.forceSpawnNow()
		
		event.actor.centerPrint("Look out below!")
		clutchAmbush1Monster1.goAggroOn(event.actor)
		clutchAmbush1Monster2.goAggroOn(event.actor)
	}
}

clutchAmbush2 = "clutchAmbush2"
myRooms.M1_104.createTriggerZone(clutchAmbush2, 630, 390, 890, 490)
myManager.onTriggerIn(myRooms.M1_104, clutchAmbush2) { event ->
	if(isPlayer(event.actor) && clutchAmbush2Spawned == false) {
		clutchAmbush2Spawned = true
		
		setDeadmanAmbushMonsterLevel(event)

		clutchAmbush2Monster1 = clutchAmbush2Spawner1.forceSpawnNow()

		clutchAmbush2Monster2 = clutchAmbush2Spawner2.forceSpawnNow()
		
		event.actor.centerPrint("Look out below!")
		clutchAmbush2Monster1.goAggroOn(event.actor)
		clutchAmbush2Monster2.goAggroOn(event.actor)
	}
}

clutchAmbush3 = "clutchAmbush3"
myRooms.M1_403.createTriggerZone(clutchAmbush3, 680, 260, 780, 500)
myManager.onTriggerIn(myRooms.M1_403, clutchAmbush3) { event ->
	if(isPlayer(event.actor) && clutchAmbush3Spawned == false) {
		clutchAmbush3Spawned = true
		
		setDeadmanAmbushMonsterLevel(event)

		clutchAmbush3Monster1 = clutchAmbush3Spawner1.forceSpawnNow()

		clutchAmbush3Monster2 = clutchAmbush3Spawner2.forceSpawnNow()
		
		event.actor.centerPrint("Look out below!")
		clutchAmbush3Monster1.goAggroOn(event.actor)
		clutchAmbush3Monster2.goAggroOn(event.actor)
	}
}

def setDeadmanAmbushMonsterLevel(event) {
	event.actor.getCrew().clone().each() { //Checks the con level of each crew member and sets ML to that value - if anyone higher sets to max ML
		if(it.getConLevel() > ambushMonsterLevel && it.getConLevel() <= MAX_AMBUSH_MONSTER_LEVEL) {
			if(it.getConLevel() <= MIN_AMBUSH_MONSTER_LEVEL) {
				ambushMonsterLevel = MIN_AMBUSH_MONSTER_LEVEL
			} else {
				ambushMonsterLevel = it.getConLevel()
			}
			if(deadmansDifficulty == 1) { adjustedAmbushMonsterLevel = ambushMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedAmbushMonsterLevel = ambushMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedAmbushMonsterLevel = ambushMonsterLevel + 0.5 }
			changeDeadmanAmbushMonsterLevel()
		} else if((it.getConLevel() - 0.1) > MAX_AMBUSH_MONSTER_LEVEL) {
			ambushMonsterLevel = MAX_AMBUSH_MONSTER_LEVEL
			if(deadmansDifficulty == 1) { adjustedAmbushMonsterLevel = ambushMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedAmbushMonsterLevel = ambushMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedAmbushMonsterLevel = ambushMonsterLevel + 0.5 }
			changeDeadmanAmbushMonsterLevel()
		}
	}
	//println "#### ambushMonsterLevel = ${ambushMonsterLevel} ####"
}

def changeDeadmanAmbushMonsterLevel() {
	//println "#### Changing ambush monster level to ${ambushMonsterLevel} ####"
	
	omgAmbush1Spawner.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	omgAmbush2Spawner.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	omgAmbush3Spawner.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	clutchAmbush1Spawner1.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	clutchAmbush1Spawner2.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	clutchAmbush2Spawner1.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	clutchAmbush2Spawner2.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	clutchAmbush3Spawner1.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
	clutchAmbush3Spawner2.setMonsterLevelForEveryone(adjustedAmbushMonsterLevel )
}

//---------------------------------------------------------------------
//Patrol Spawners                                                      
//---------------------------------------------------------------------
def makePatrolSpawners() {
	omgPatrolMonsterType = "omg"
	snakePatrolMonsterType = "snake"

	if(deadmansDifficulty >= 2) { omgPatrolMonsterType = "omg_LT" }
	if(deadmansDifficulty == 3) { snakePatrolMonsterType = "snake_LT" }

	// Spawners
	patrol1 = myRooms.M1_407.spawnStoppedSpawner("patrol1", omgPatrolMonsterType, 1)
	patrol1.setPos(550, 620)
	patrol1.setHateRadiusForChildren(1500)
	patrol1.setHomeTetherForChildren(10000)
	patrol1.setMonsterLevelForChildren(2.5)
	patrol1.setCFH(500)
	patrol1.setBaseSpeed(150)
	patrol1.addPatrolPointForChildren( "M1_407", 60, 370, 0 )
	patrol1.addPatrolPointForChildren( "M1_406", 470, 370, 0 )
	patrol1.addPatrolPointForChildren( "M1_405", 250, 370, 5 )
	patrol1.addPatrolPointForChildren( "M1_405", 375, 50, 0 )
	patrol1.addPatrolPointForChildren( "M1_305", 700, 370, 0 )
	patrol1.addPatrolPointForChildren( "M1_305", 670, 50, 10 )
	patrol1.addPatrolPointForChildren( "M1_305", 700, 370, 0 )
	patrol1.addPatrolPointForChildren( "M1_405", 345, 90, 0 )
	patrol1.addPatrolPointForChildren( "M1_404", 580, 450, 0 )
	patrol1.addPatrolPointForChildren( "M1_403", 810, 405, 0 )
	patrol1.addPatrolPointForChildren( "M1_403", 550, 250, 10 )
	patrol1.addPatrolPointForChildren( "M1_403", 810, 405, 0 )
	patrol1.addPatrolPointForChildren( "M1_404", 580, 450, 0 )
	patrol1.addPatrolPointForChildren( "M1_304", 550, 630, 0 )
	patrol1.addPatrolPointForChildren( "M1_304", 870, 325, 0 )
	patrol1.addPatrolPointForChildren( "M1_204", 600, 300, 0 )
	patrol1.addPatrolPointForChildren( "M1_104", 910, 585, 5 )
	patrol1.addPatrolPointForChildren( "M1_104", 250, 60, 0 )
	patrol1.addPatrolPointForChildren( "M1_103", 890, 205, 0 )
	patrol1.addPatrolPointForChildren( "M1_102", 575, 235, 10 )
	patrol1.addPatrolPointForChildren( "M1_103", 890, 205, 0 )
	patrol1.addPatrolPointForChildren( "M1_104", 250, 60, 0 )
	patrol1.addPatrolPointForChildren( "M1_104", 910, 585, 5 )
	patrol1.addPatrolPointForChildren( "M1_204", 600, 300, 0 )
	patrol1.addPatrolPointForChildren( "M1_304", 870, 325, 0 )
	patrol1.addPatrolPointForChildren( "M1_304", 550, 630, 0 )
	patrol1.addPatrolPointForChildren( "M1_404", 580, 450, 0 )
	patrol1.addPatrolPointForChildren( "M1_405", 345, 90, 0 )
	patrol1.addPatrolPointForChildren( "M1_406", 470, 370, 0 )
	patrol1.addPatrolPointForChildren( "M1_407", 60, 370, 0 )

	patrol2 = myRooms.M1_407.spawnStoppedSpawner("patrol2", snakePatrolMonsterType, 1)
	patrol2.setPos(550, 620)
	patrol2.setHateRadiusForChildren(1500)
	patrol2.setHomeTetherForChildren(2000)
	patrol2.setMonsterLevelForChildren(2.0)
	patrol2.setCFH(500)
	patrol2.setBaseSpeed(160)

	patrol3 = myRooms.M1_407.spawnStoppedSpawner("patrol3", snakePatrolMonsterType, 1)
	patrol3.setPos(550, 620)
	patrol3.setHateRadiusForChildren(1500)
	patrol3.setHomeTetherForChildren(2000)
	patrol3.setMonsterLevelForChildren(2.0)
	patrol3.setCFH(500)
	patrol3.setBaseSpeed(160)
}

//---------------------------------------------------------------------
//Patrol Logic                                                         
//---------------------------------------------------------------------
//Variable initialization
patrolSpawned = false
patrolGuard1 = null
patrolGuard2 = null

patrolMonsterLevel = 0
MIN_PATROL_MONSTER_LEVEL = 2.5
MAX_PATROL_MONSTER_LEVEL = 3.5

//Spawn and patrol routine
def patrolSpawn() {
	if(patrolSpawned == false) {
		patrolLeader = patrol1.forceSpawnNow()
		
		if(patrolGuard1 == null || patrolGuard1.isDead()) {
			patrolGuard1 = patrol2.forceSpawnNow()
			patrolGuard1.startFollow(patrolLeader)
		}
		
		if(patrolGuard2 == null || patrolGuard2.isDead()) {
			patrolGuard2 = patrol3.forceSpawnNow()
			patrolGuard2.startFollow(patrolLeader)
		}
		
		patrolSpawned = true
		
		runOnDeath(patrolLeader) { patrolSpawned = false; myManager.schedule(random(180, 300)) { patrolSpawn() } }
	} else {
		myManager.schedule(30) { patrolSpawn() }
	}
}

def setDeadmanPatrolMonsterLevel(player) {
	player.getCrew().clone().each() { //Checks the con level of each crew member and sets ML to that value - if anyone higher sets to max ML
		if(it.getConLevel() > patrolMonsterLevel && it.getConLevel() <= MAX_PATROL_MONSTER_LEVEL) {
			if(it.getConLevel() <= MIN_PATROL_MONSTER_LEVEL) {
				patrolMonsterLevel = MIN_PATROL_MONSTER_LEVEL
			} else {
				patrolMonsterLevel = it.getConLevel()
			}
			if(deadmansDifficulty == 1) { adjustedPatrolMonsterLevel = patrolMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedPatrolMonsterLevel = patrolMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedPatrolMonsterLevel = patrolMonsterLevel + 0.5 }
			changeDeadmanPatrolMonsterLevel()
		} else if(it.getConLevel() > MAX_PATROL_MONSTER_LEVEL) {
			patrolMonsterLevel = MAX_PATROL_MONSTER_LEVEL
			if(deadmansDifficulty == 1) { adjustedPatrolMonsterLevel = patrolMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedPatrolMonsterLevel = patrolMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedPatrolMonsterLevel = patrolMonsterLevel + 0.5 }
			changeDeadmanPatrolMonsterLevel()
		}
	}
	//println "#### patrolMonsterLevel = ${patrolMonsterLevel} ####"
	
	myManager.schedule(30) { setDeadmanPatrolMonsterLevel(player) }
}

def changeDeadmanPatrolMonsterLevel() {	
	//println "#### changing patrol monster level to ${patrolMonsterLevel} ####"
	patrol1.setMonsterLevelForEveryone(adjustedPatrolMonsterLevel )
	patrol2.setMonsterLevelForEveryone(adjustedPatrolMonsterLevel )
	patrol3.setMonsterLevelForEveryone(adjustedPatrolMonsterLevel )
}

//---------------------------------------------------------------------
//Demi Spawners                                                        
//---------------------------------------------------------------------
omgDemiMonsterType = "omg"

//Spawners
omgDemiGruntSpawner1 = myRooms.M1_102.spawnStoppedSpawner("omgDemiGruntSpawner1", "omg", 20)
omgDemiGruntSpawner1.setPos(300, 100)
omgDemiGruntSpawner1.setSpawnWhenPlayersAreInRoom(true)
omgDemiGruntSpawner1.setMonsterLevelForChildren(2.9)
omgDemiGruntSpawner1.setWaitTime(3, 5)

omgDemiGruntSpawner2 = myRooms.M1_102.spawnStoppedSpawner("omgDemiGruntSpawner2", "omg", 20)
omgDemiGruntSpawner2.setPos(800, 100)
omgDemiGruntSpawner2.setSpawnWhenPlayersAreInRoom(true)
omgDemiGruntSpawner2.setMonsterLevelForChildren(2.9)
omgDemiGruntSpawner2.setWaitTime(3, 5)

omgDemiGruntSpawner3 = myRooms.M1_102.spawnStoppedSpawner("omgDemiGruntSpawner3", "omg", 20)
omgDemiGruntSpawner3.setPos(575, 100)
omgDemiGruntSpawner3.setSpawnWhenPlayersAreInRoom(true)
omgDemiGruntSpawner3.setMonsterLevelForChildren(2.9)
omgDemiGruntSpawner3.setWaitTime(3, 5)

omgDemiLTSpawner1 = myRooms.M1_102.spawnStoppedSpawner("omgDemiLTSpawner1", "omg_LT", 20)
omgDemiLTSpawner1.setPos(300, 100)
omgDemiLTSpawner1.setSpawnWhenPlayersAreInRoom(true)
omgDemiLTSpawner1.setMonsterLevelForChildren(2.9)
omgDemiLTSpawner1.setWaitTime(3, 5)

omgDemiLTSpawner2 = myRooms.M1_102.spawnStoppedSpawner("omgDemiLTSpawner2", "omg_LT", 20)
omgDemiLTSpawner2.setPos(800, 100)
omgDemiLTSpawner2.setSpawnWhenPlayersAreInRoom(true)
omgDemiLTSpawner2.setMonsterLevelForChildren(2.9)
omgDemiLTSpawner2.setWaitTime(3, 5)

omgDemiLTSpawner3 = myRooms.M1_102.spawnStoppedSpawner("omgDemiLTSpawner3", "omg_LT", 20)
omgDemiLTSpawner3.setPos(575, 100)
omgDemiLTSpawner3.setSpawnWhenPlayersAreInRoom(true)
omgDemiLTSpawner3.setMonsterLevelForChildren(2.9)
omgDemiLTSpawner3.setWaitTime(3, 5)

omgDemiSpawner = myRooms.M1_102.spawnStoppedSpawner("omgDemiSpawner", "omg_demi", 1)
omgDemiSpawner.setPos(575, 100)
omgDemiSpawner.setSpawnWhenPlayersAreInRoom(true)
omgDemiSpawner.setMonsterLevelForChildren(2.9)
omgDemiSpawner.setWaitTime(3, 5)

//---------------------------------------------------------------------
//Demi Patrols                                                         
//---------------------------------------------------------------------
omgGetInRange1 = makeNewPatrol()
omgGetInRange1.addPatrolPoint( "M1_2", 360, 515, 0 )
omgGetInRange1.addPatrolPoint( "M1_2", 70, 250, 20000 )

omgGetInRange2 = makeNewPatrol()
omgGetInRange2.addPatrolPoint( "M1_2", 610, 430, 0 )
omgGetInRange2.addPatrolPoint( "M1_2", 190, 165, 20000 )

omgGetInRange3 = makeNewPatrol()
omgGetInRange3.addPatrolPoint( "M1_2", 360, 515, 0 )
omgGetInRange3.addPatrolPoint( "M1_2", 190, 305, 20000 )

omgGetInRange4 = makeNewPatrol()
omgGetInRange4.addPatrolPoint( "M1_2", 610, 430, 0 )
omgGetInRange4.addPatrolPoint( "M1_2", 325, 190, 20000 )

omgGetInRange5 = makeNewPatrol()
omgGetInRange5.addPatrolPoint( "M1_2", 565, 500, 0 )
omgGetInRange5.addPatrolPoint( "M1_2", 305, 295, 20000 )

omgGetInRange6 = makeNewPatrol()
omgGetInRange6.addPatrolPoint( "M1_2", 610, 430, 0 )
omgGetInRange6.addPatrolPoint( "M1_2", 100, 190, 20000 )

//---------------------------------------------------------------------
//Demi Logic                                                           
//---------------------------------------------------------------------
omgDemiEngaged = false
omgLock = true
omgEnd = false
omgDemiWaves = 0
omgDemiWaveTimer = 27
omgDemiList = []
omgDemiStartList = []
playerListM1_2 = []
omgDemiMonsterList = [] as Set

demiMonsterLevel = 0
MIN_DEMI_MONSTER_LEVEL = 2.5
MAX_DEMI_MONSTER_LEVEL = 3.5

myManager.onEnter( myRooms.M1_2 ) { event ->
	if(isPlayer(event.actor)) {
		playerListM1_2 << event.actor
		if(playerListM1_2.size() > 0 && omgEnd == false) {
			omgLock = false
		}
	}
}

myManager.onExit( myRooms.M1_2 ) { event ->
	if(isPlayer(event.actor)) {
		playerListM1_2.remove( event.actor )
		if(playerListM1_2.size() == 0) {
			if(omgDemiEngaged == true) {
				event.actor.getCrew().each { it.centerPrint("The alarm ends as your Crew withdraws from the gate.") }
				stopSound("omgwtfAlarm").toZone()
			}
			omgLock = true
			omgDemiEngaged = false
			omgDemiWaves = 0
			
			omgDemiMonsterList.clone().each() {
				if(!it.isDead()) {
					it.instantPercentDamage(100)
				}
				omgDemiMonsterList.remove(it)
			}
		}
	}
}

omgDemiZone = "omgDemiZone"
myRooms.M1_2.createTriggerZone(omgDemiZone, 60, 25, 175, 225)
myManager.onTriggerIn(myRooms.M1_2, omgDemiZone) { event ->
	if(isPlayer(event.actor) && omgDemiEngaged == false && omgEnd == false) { 
		omgDemiEngaged = true
		playerListM1_2.clone().each { it.centerPrint("An alarm sounds as ${event.actor} approaches the gate.") }
		sound("omgwtfAlarm").toZone()
		setDeadmanDemiMonsterLevel(event)
		if(deadmansDifficulty)
		myManager.schedule(3) { nextOMGDemiWave(event) }
		
		m1RoomList.each() {
			it.getActorList().each{
				omgDemiStartList << it
			}
		}
	}
}

def nextOMGDemiWave(event) {
	//Increment wave counter
	omgDemiWaves++
	omgDemiWaveTimer = 27 - (event.actor.getCrew().size() * 2)
	if(omgDemiWaves < 4 && omgDemiEngaged == true && omgLock == false && omgEnd == false) {
		//Spawn two OMGs
		setDemiSpawnType()
		
		if(omgDemiMonsterType ==  "omg_LT") {
			omgDemiMonster1 = omgDemiLTSpawner1.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster1
		} else {
			omgDemiMonster1 = omgDemiGruntSpawner1.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster1
		}
		
		setDemiSpawnType()
		
		if(omgDemiMonsterType == "omg_LT") {
			omgDemiMonster2 = omgDemiLTSpawner2.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster2
		} else {
			omgDemiMonster2 = omgDemiGruntSpawner2.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster2
		}
		
		runOnDeath(omgDemiMonster1) { event2 ->
			if(omgDemiMonsterList.contains(event2.actor)) {
				omgDemiMonsterList.remove(event2.actor)
			}
		}
		
		runOnDeath(omgDemiMonster2) { event2 ->
			if(omgDemiMonsterList.contains(event2.actor)) {
				omgDemiMonsterList.remove(event2.actor)
			}
		}
		
		//Patrol closer
		omgDemiMonster1.setPatrol( omgGetInRange1 )
		omgDemiMonster1.startPatrol()
		
		omgDemiMonster2.setPatrol( omgGetInRange2 )
		omgDemiMonster2.startPatrol()

		
		//Add OMGs to a list
		omgDemiList << omgDemiMonster1
		omgDemiList << omgDemiMonster2
		
		playerListM1_2.clone().each { it.centerPrint("A booming voice screams, 'ATTACK!'") }
		
		//OMGs aggro whoever stepped in
		myManager.schedule(12) { 
			if(playerListM1_2.isEmpty() == false) {
				omgDemiMonster1.addHate( random( playerListM1_2 ), 1 ) 
				omgDemiMonster2.addHate( random( playerListM1_2 ), 1 ) 
			}
		}
		
		//Schedule next wave
		myManager.schedule(omgDemiWaveTimer) { nextOMGDemiWave(event) }
	} else if(omgDemiWaves < 7 && omgDemiEngaged == true && omgLock == false && omgEnd == false) {
		//Spawn three OMGs
		setDemiSpawnType()
		if(omgDemiMonsterType ==  "omg_LT") {
			omgDemiMonster1 = omgDemiLTSpawner1.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster1
			
		} else {
			omgDemiMonster1 = omgDemiGruntSpawner1.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster1
		}
		
		setDemiSpawnType()
		if(omgDemiMonsterType ==  "omg_LT") {
			omgDemiMonster2 = omgDemiLTSpawner2.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster2
		} else {
			omgDemiMonster2 = omgDemiGruntSpawner2.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster2
		}
		
		setDemiSpawnType()
		if(omgDemiMonsterType ==  "omg_LT") {
			omgDemiMonster3 = omgDemiLTSpawner3.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster3
		} else {
			omgDemiMonster3 = omgDemiGruntSpawner3.forceSpawnNow()
			omgDemiMonsterList << omgDemiMonster3
		}
		
		runOnDeath(omgDemiMonster1) { event2 ->
			if(omgDemiMonsterList.contains(event2.actor)) {
				omgDemiMonsterList.remove(event2.actor)
			}
		}
		
		runOnDeath(omgDemiMonster2) { event2 ->
			if(omgDemiMonsterList.contains(event2.actor)) {
				omgDemiMonsterList.remove(event2.actor)
			}
		}
		
		runOnDeath(omgDemiMonster3) { event2 ->
			if(omgDemiMonsterList.contains(event2.actor)) {
				omgDemiMonsterList.remove(event2.actor)
			}
		}
		
		//Patrol closer
		omgDemiMonster1.setPatrol( omgGetInRange3 )
		omgDemiMonster1.startPatrol()
		
		omgDemiMonster2.setPatrol( omgGetInRange4 )
		omgDemiMonster2.startPatrol()
		
		omgDemiMonster3.setPatrol( omgGetInRange5)
		omgDemiMonster3.startPatrol()
		
		//Add OMGs to a list
		omgDemiList << omgDemiMonster1
		omgDemiList << omgDemiMonster2
		omgDemiList << omgDemiMonster3
		
		playerListM1_2.clone().each { it.centerPrint("A booming voice screams, 'ATTACK!'") }
		
		//OMGs aggro whoever stepped in
		myManager.schedule(12) {
			if(playerListM1_2.isEmpty() == false) { 
				omgDemiMonster1.addHate( random( playerListM1_2 ), 1 ) 
				omgDemiMonster2.addHate( random( playerListM1_2 ), 1 ) 
				omgDemiMonster3.addHate( random( playerListM1_2 ), 1 ) 
			}
		}
		
		//Schedule next wave
		myManager.schedule(omgDemiWaveTimer) { nextOMGDemiWave(event) }
	} else if(omgDemiWaves == 7 && omgDemiEngaged == true && omgLock == false && omgEnd == false) {
		//Spawn the OMG demi
		omgDemiBoss = omgDemiSpawner.forceSpawnNow()
		omgDemiMonsterList << omgDemiBoss
		
		omgEnd = true
		
		//Patrol closer
		omgDemiBoss.setPatrol( omgGetInRange6 )
		omgDemiBoss.startPatrol()
		
		//Add Demo to list
		omgDemiList << omgDemiBoss
		
		//Aggro!
		myManager.schedule(12) {
			if(playerListM1_2.isEmpty() == false) { omgDemiBoss.addHate( random( playerListM1_2 ), 1 ) }
		}
		
		//Threatening voice!
		myManager.onEnter( myRooms.M1_2 ) { event2 ->
			if(event2.actor == omgDemiBoss) {
				omgDemiBoss.say("Who dares approach this gate? Your insolence shall be punished!")
			}
		}
		
		runOnDeath(omgDemiBoss) { event3 ->
			//println "##### Caught death of omgDemiBoss #####"
			if(omgDemiEngaged == true) {
				omgDemiEngaged = false
				stopSound("omgwtfAlarm").toZone()
				omgDemiRewardList = event3.actor.getHated()
				deadmanDemiReward()
				omgDemiMonsterList.remove(event3.actor)
			}
		}
	}
}

def deadmanDemiReward() {
	omgDemiRewardList.each() {
		if(it.getConLevel() < 3.6 && omgDemiStartList.contains(it)) {
			if(deadmansDifficulty == 1) {
				rewardModifier = 1
				numGold = random(15, 25) * rewardModifier
				it.grantCoins(numGold)
				numOrbs = random(1, 3) * rewardModifier
				it.grantQuantityItem( 100257, numOrbs )
				it.centerPrint("You defeated the OMGWTF on 'Easy' difficulty, earning ${numGold} gold and ${numOrbs} orbs.")
			}
			if(deadmansDifficulty == 2) {
				rewardModifier = 2
				numGold = random(15, 25) * rewardModifier
				it.grantCoins(numGold)
				numOrbs = random(1, 3) * rewardModifier
				it.grantQuantityItem( 100257, numOrbs )
				it.centerPrint("You defeated the OMGWTF on 'Normal' difficulty, earning ${numGold} gold and ${numOrbs} orbs.")
			}
			if(deadmansDifficulty == 3) {
				rewardModifier = 4
				numGold = random(15, 25) * rewardModifier
				it.grantCoins(numGold)
				numOrbs = random(1, 3) * rewardModifier
				it.grantQuantityItem( 100257, numOrbs )
				it.centerPrint("You defeated the OMGWTF on 'Hard' difficulty, earning ${numGold} gold and ${numOrbs} orbs.")
			} 
		} else {
			if(it.getConLevel() >= 3.6) {
				it.centerPrint( "Your level is too high for this mission. No reward for you!" )
			} else {
				it.centerPrint("You must be in the room at the start of the event to receive a reward.")
			}
		}
	}
}

def setDeadmanDemiMonsterLevel(event) {
	event.actor.getCrew().clone().each() { //Checks the con level of each crew member and sets ML to that value - if anyone higher sets to max ML
		if(it.getConLevel() > demiMonsterLevel && it.getConLevel() <= MAX_DEMI_MONSTER_LEVEL) {
			if(it.getConLevel() <= MIN_DEMI_MONSTER_LEVEL) {
				demiMonsterLevel = MIN_DEMI_MONSTER_LEVEL
			} else {
				demiMonsterLevel = it.getConLevel()
			}
			if(deadmansDifficulty == 1) { adjustedDemiMonsterLevel = demiMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedDemiMonsterLevel = demiMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedDemiMonsterLevel = demiMonsterLevel + 0.5 }
			changeDeadmanDemiMonsterLevel()
		} else if(it.getConLevel() > MAX_DEMI_MONSTER_LEVEL) {
			demiMonsterLevel = MAX_DEMI_MONSTER_LEVEL
			if(deadmansDifficulty == 1) { adjustedDemiMonsterLevel = demiMonsterLevel - 0.6 }
			if(deadmansDifficulty == 2) { adjustedDemiMonsterLevel = demiMonsterLevel }
			if(deadmansDifficulty == 3) { adjustedDemiMonsterLevel = demiMonsterLevel + 0.5 }
			changeDeadmanDemiMonsterLevel()
		}
	}
	//println "#### demiMonsterLevel = ${demiMonsterLevel} ####"
	
	myManager.schedule(30) { setDeadmanDemiMonsterLevel(event) }
}

def changeDeadmanDemiMonsterLevel() {
	//println "#### Changing demi level to ${demiMonsterLevel} ####"
	omgDemiGruntSpawner1.setMonsterLevelForEveryone(adjustedDemiMonsterLevel)
	omgDemiGruntSpawner2.setMonsterLevelForEveryone(adjustedDemiMonsterLevel)
	omgDemiGruntSpawner3.setMonsterLevelForEveryone(adjustedDemiMonsterLevel)
	omgDemiLTSpawner1.setMonsterLevelForEveryone(adjustedDemiMonsterLevel)
	omgDemiLTSpawner2.setMonsterLevelForEveryone(adjustedDemiMonsterLevel)
	omgDemiLTSpawner3.setMonsterLevelForEveryone(adjustedDemiMonsterLevel)
	omgDemiSpawner.setMonsterLevelForEveryone(adjustedDemiMonsterLevel)
}

def setDemiSpawnType() { //Determines whether a grunt or an LT omg will spawn
	demiMonsterRoll = random(1, 5)
	if(deadmansDifficulty == 2) {
		if(demiMonsterRoll == 1) { omgDemiMonsterType = "omg_LT" }
		else { omgDemiMonsterType = "omg" }
	}
	if(deadmansDifficulty == 3) {
		if(demiMonsterRoll <= 2) { omgDemiMonsterType = "omg_LT" }
		else { omgDemiMonsterType = "omg" }
	}
}

//---------------------------------------------------------------------
//Grue Logic                                                           
//---------------------------------------------------------------------
//Better light a torch... but you have no matches!
//Oh, no! You have walked into the slavering fangs of a lurking grue!
def grueCheck() {
	if( ( gst() > 0 && gst() < 50 ) || ( gst() > 2350 && gst() < 2400 ) ) { //Check time
		grueRoll = random(1, 100) //Roll between 1 and 100
		if(grueRoll >= 98) { //Check if roll higher than 98
			grueList.clone().each {
				if(isOnline(it) && isInZone(it)) { //For each person on grueList, check that they are still in M1
					it?.centerPrint("It is pitch black. You are likely to be eaten by a grue!")
					grueList.remove(it)
				}
			}
		} else {
			myManager.schedule(60) { grueCheck() }
		}
	} else {
		myManager.schedule(60) { grueCheck() }
	}
}

myManager.schedule(60) { grueCheck() }

def checkDazedStatus() {
	warpDazed = true
	m1RoomList.each() {
		it.getActorList().each{
			if(isPlayer(it) && it.isDazed()) { dazedSet << it }
			if(isPlayer(it) && !it.isDazed()) { warpDazed = false }
		}
	}
	dazedWarp()
	myManager.schedule(5) { checkDazedStatus() }
}

def dazedWarp() {
	if(warpDazed == true) {
		dazedSet.clone().each() {
			it.centerPrint("The denizens of Deadman's Pass drag your unconcious body back through the gate.")
			it.warp("BF1_705", 270, 405)
			dazedSet.remove(it)
		}
	}	
}

checkDazedStatus()

def checkLevelCap() {
	m1RoomList.each() {
		it.getActorList().each{
			if(isPlayer(it) && it.getConLevel() >= 3.6) {
				it.centerPrint("You must be level 3.5 or lower for this task.")
				it.centerPrint("Click 'MENU' and select 'CHANGE LEVEL' to lower your level temporarily.")
				it.warp("BF1_705", 270, 405)
			}
		}
	}
	myManager.schedule(5) { checkLevelCap() }
}

checkLevelCap()
