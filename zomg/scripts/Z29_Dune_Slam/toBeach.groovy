//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

duneslamExit = makeSwitch( "IslandBoat2", myRooms.DuneSlam_1, 220, 190 )
duneslamExit.setMouseoverText("Return to Gold Beach")
duneslamExit.unlock()
duneslamExit.off()
duneslamExit.setRange(250)

def goToBeach = { event ->
	duneslamExit.lock()
	myManager.schedule(2) { duneslamExit.unlock() }
	event.actor.warp( "Beach_604", 655, 430, 6 )
	
	myManager.schedule(1) { duneslamExit.off() }
}

duneslamExit.whenOn(goToBeach)