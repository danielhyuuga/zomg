//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

monsterLevel = 6.5
duneslamDifficulty = null

//Trigger Zone definitions
duneslamTrigger1 = "duneslamTrigger1"
myRooms.DuneSlam_1.createTriggerZone(duneslamTrigger1, 275, 685, 475, 885)

duneslamTrigger2 = "duneslamTrigger2"
myRooms.DuneSlam_1.createTriggerZone(duneslamTrigger2, 890, 720, 1090, 920)

duneslamTrigger3 = "duneslamTrigger3"
myRooms.DuneSlam_1.createTriggerZone(duneslamTrigger3, 200, 390, 400, 590)

duneslamTrigger4 = "duneslamTrigger4"
myRooms.DuneSlam_1.createTriggerZone(duneslamTrigger4, 700, 265, 900, 465)

duneslamTrigger5 = "duneslamTrigger5"
myRooms.DuneSlam_1.createTriggerZone(duneslamTrigger5, 1200, 390, 1400, 590)

duneslamTrigger6 = "duneslamTrigger6"
myRooms.DuneSlam_1.createTriggerZone(duneslamTrigger6, 400, 65, 600, 265)

duneslamTrigger7 = "duneslamTrigger7"
myRooms.DuneSlam_1.createTriggerZone(duneslamTrigger7, 1030, 175, 1230, 375)

stagingArea = "stagingArea"
myRooms.DuneSlam_1.createTriggerZone(stagingArea, 200, 275, 400, 400)

//Definition of spawners
duneslamSpawn = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamSpawner", "sand_golem_demi", 1)
duneslamSpawn.setPos(770, 580)
duneslamSpawn.childrenWander(false)
duneslamSpawn.setHateRadiusForChildren(3000)
duneslamSpawn.setSpawnWhenPlayersAreInRoom(true)
duneslamSpawn.setMonsterLevelForChildren(monsterLevel)
duneslamSpawn.setWaitTime(3, 5)
duneslamSpawn.stopSpawning()

duneslamGruntSpawner1 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamGruntSpawner1", "sand_golem", 100)
duneslamGruntSpawner1.setPos(375, 785)
duneslamGruntSpawner1.setHateRadiusForChildren(3000)
duneslamGruntSpawner1.setSpawnWhenPlayersAreInRoom(true)
duneslamGruntSpawner1.setMonsterLevelForChildren(monsterLevel)
duneslamGruntSpawner1.setWaitTime(3, 5)
duneslamGruntSpawner1.stopSpawning()

duneslamGruntSpawner2 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamGruntSpawner2", "sand_golem", 100)
duneslamGruntSpawner2.setPos(990, 820)
duneslamGruntSpawner2.setHateRadiusForChildren(3000)
duneslamGruntSpawner2.setSpawnWhenPlayersAreInRoom(true)
duneslamGruntSpawner2.setMonsterLevelForChildren(monsterLevel)
duneslamGruntSpawner2.setWaitTime(3, 5)
duneslamGruntSpawner2.stopSpawning()

duneslamGruntSpawner3 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamGruntSpawner3", "sand_golem", 100)
duneslamGruntSpawner3.setPos(300, 490)
duneslamGruntSpawner3.setHateRadiusForChildren(3000)
duneslamGruntSpawner3.setSpawnWhenPlayersAreInRoom(true)
duneslamGruntSpawner3.setMonsterLevelForChildren(monsterLevel)
duneslamGruntSpawner3.setWaitTime(3, 5)
duneslamGruntSpawner3.stopSpawning()

duneslamGruntSpawner4 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamGruntSpawner4", "sand_golem", 100)
duneslamGruntSpawner4.setPos(800, 365)
duneslamGruntSpawner4.setHateRadiusForChildren(3000)
duneslamGruntSpawner4.setSpawnWhenPlayersAreInRoom(true)
duneslamGruntSpawner4.setMonsterLevelForChildren(monsterLevel)
duneslamGruntSpawner4.setWaitTime(3, 5)
duneslamGruntSpawner4.stopSpawning()

duneslamGruntSpawner5 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamGruntSpawner5", "sand_golem", 100)
duneslamGruntSpawner5.setPos(1300, 490)
duneslamGruntSpawner5.setHateRadiusForChildren(3000)
duneslamGruntSpawner5.setSpawnWhenPlayersAreInRoom(true)
duneslamGruntSpawner5.setMonsterLevelForChildren(monsterLevel)
duneslamGruntSpawner5.setWaitTime(3, 5)
duneslamGruntSpawner5.stopSpawning()

duneslamGruntSpawner6 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamGruntSpawner6", "sand_golem", 100)
duneslamGruntSpawner6.setPos(500, 165)
duneslamGruntSpawner6.setHateRadiusForChildren(3000)
duneslamGruntSpawner6.setSpawnWhenPlayersAreInRoom(true)
duneslamGruntSpawner6.setMonsterLevelForChildren(monsterLevel)
duneslamGruntSpawner6.setWaitTime(3, 5)
duneslamGruntSpawner6.stopSpawning()

duneslamGruntSpawner7 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamGruntSpawner7", "sand_golem", 100)
duneslamGruntSpawner7.setPos(1130, 175)
duneslamGruntSpawner7.setHateRadiusForChildren(3000)
duneslamGruntSpawner7.setSpawnWhenPlayersAreInRoom(true)
duneslamGruntSpawner7.setMonsterLevelForChildren(monsterLevel)
duneslamGruntSpawner7.setWaitTime(3, 5)
duneslamGruntSpawner7.stopSpawning()

duneslamLTSpawner1 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamLTSpawner1", "sand_golem_LT", 100)
duneslamLTSpawner1.setPos(375, 785)
duneslamLTSpawner1.setHateRadiusForChildren(3000)
duneslamLTSpawner1.setSpawnWhenPlayersAreInRoom(true)
duneslamLTSpawner1.setMonsterLevelForChildren(monsterLevel)
duneslamLTSpawner1.setWaitTime(3, 5)
duneslamLTSpawner1.stopSpawning()

duneslamLTSpawner2 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamLTSpawner2", "sand_golem_LT", 100)
duneslamLTSpawner2.setPos(990, 820)
duneslamLTSpawner2.setHateRadiusForChildren(3000)
duneslamLTSpawner2.setSpawnWhenPlayersAreInRoom(true)
duneslamLTSpawner2.setMonsterLevelForChildren(monsterLevel)
duneslamLTSpawner2.setWaitTime(3, 5)
duneslamLTSpawner2.stopSpawning()

duneslamLTSpawner3 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamLTSpawner3", "sand_golem_LT", 100)
duneslamLTSpawner3.setPos(300, 490)
duneslamLTSpawner3.setHateRadiusForChildren(3000)
duneslamLTSpawner3.setSpawnWhenPlayersAreInRoom(true)
duneslamLTSpawner3.setMonsterLevelForChildren(monsterLevel)
duneslamLTSpawner3.setWaitTime(3, 5)
duneslamLTSpawner3.stopSpawning()

duneslamLTSpawner4 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamLTSpawner4", "sand_golem_LT", 100)
duneslamLTSpawner4.setPos(800, 365)
duneslamLTSpawner4.setHateRadiusForChildren(3000)
duneslamLTSpawner4.setSpawnWhenPlayersAreInRoom(true)
duneslamLTSpawner4.setMonsterLevelForChildren(monsterLevel)
duneslamLTSpawner4.setWaitTime(3, 5)
duneslamLTSpawner4.stopSpawning()

duneslamLTSpawner5 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamLTSpawner5", "sand_golem_LT", 100)
duneslamLTSpawner5.setPos(1300, 490)
duneslamLTSpawner5.setHateRadiusForChildren(3000)
duneslamLTSpawner5.setSpawnWhenPlayersAreInRoom(true)
duneslamLTSpawner5.setMonsterLevelForChildren(monsterLevel)
duneslamLTSpawner5.setWaitTime(3, 5)
duneslamLTSpawner5.stopSpawning()

duneslamLTSpawner6 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamLTSpawner6", "sand_golem_LT", 100)
duneslamLTSpawner6.setPos(500, 165)
duneslamLTSpawner6.setHateRadiusForChildren(3000)
duneslamLTSpawner6.setSpawnWhenPlayersAreInRoom(true)
duneslamLTSpawner6.setMonsterLevelForChildren(monsterLevel)
duneslamLTSpawner6.setWaitTime(3, 5)
duneslamLTSpawner6.stopSpawning()

duneslamLTSpawner7 = myRooms.DuneSlam_1.spawnStoppedSpawner("duneslamLTSpawner7", "sand_golem_LT", 100)
duneslamLTSpawner7.setPos(1130, 175)
duneslamLTSpawner7.setHateRadiusForChildren(3000)
duneslamLTSpawner7.setSpawnWhenPlayersAreInRoom(true)
duneslamLTSpawner7.setMonsterLevelForChildren(monsterLevel)
duneslamLTSpawner7.setWaitTime(3, 5)
duneslamLTSpawner7.stopSpawning()

//Variable initialization
playerList = [] as Set
hateList = [] as Set
gruntList = []
ltList = []
duneslamActive = false
Duneslam = null
duneslamWaveCounter = 0
duneslamGruntsActive = false
duneslamLTsActive = false
duneslamGrunt1Spawned = false
duneslamGrunt2Spawned = false
duneslamGrunt3Spawned = false
duneslamGrunt4Spawned = false
duneslamGrunt5Spawned = false
duneslamGrunt6Spawned = false
duneslamGrunt7Spawned = false
duneslamLT1Spawned = false
duneslamLT2Spawned = false
duneslamLT3Spawned = false
duneslamLT4Spawned = false
duneslamLT5Spawned = false
duneslamLT6Spawned = false
duneslamLT7Spawned = false
duneslamGruntSpawnerMap = ["spawner1": [duneslamGruntSpawner1, duneslamLTSpawner1], "spawner2": [duneslamGruntSpawner2, duneslamLTSpawner2], "spawner3": [duneslamGruntSpawner3, duneslamLTSpawner3], "spawner4": [duneslamGruntSpawner4, duneslamLTSpawner4], "spawner5": [duneslamGruntSpawner5, duneslamLTSpawner5], "spawner6": [duneslamGruntSpawner6, duneslamLTSpawner6], "spawner7": [duneslamGruntSpawner7, duneslamLTSpawner7]]

//Constant initialization
DUNESLAM_GRUNT_WAVES = 5
DUNESLAM_LT_WAVES = 10
DUNESLAM_WAVES = 11
DUNESLAM_GRUNT_WAVE_TIMER = 15
DUNESLAM_LT_WAVE_TIMER = 25
DUNESLAM_START = new Object()
MAX_MONSTER_LEVEL = 7.1
MONSTER_LEVEL_MOD = 0.1

//This is run when a player enters the room, if event is not active the event begins
myManager.onTriggerOut(myRooms.DuneSlam_1, stagingArea) { event ->
	if(isPlayer(event.actor)) {
		//println "##### playerList = ${playerList} #####"
		synchronized(DUNESLAM_START) {
			if( duneslamActive == false ) { //Check to see if event is active, if not begin event
				duneslamActive = true
				duneslamDifficulty = event.actor.getTeam().getAreaVar( "Z29_Dune_Slam", "Z29DuneslamDifficulty" )
				if( duneslamDifficulty == 0 ) { duneslamDifficulty = 1 }
				myManager.schedule(3) {
					playerList.clone().each() { it.centerPrint("You feel the ground quake beneath your feet as you set foot on the island.") }
				}
				
				//println "##### crewSize = ${event.actor.getCrew().plus(playerList).size()} #####"
				
				//println "##### event.actor.getConLevel() = ${event.actor.getConLevel()} #####"
				
				nextDuneslamWave(event) //Begin Duneslam event
				checkDazedStatus()
			}
		}
	}
}

myManager.onEnter(myRooms.DuneSlam_1) { event ->
	if(isPlayer(event.actor) && !playerList.contains()) {
		playerList << event.actor
	}
	//println "#### playerList = ${playerList} ####"
}

myManager.onExit(myRooms.DuneSlam_1) { event ->
	if(isPlayer(event.actor)) {
		if(playerList.contains(event.actor)) {
			playerList.remove(event.actor)
		}
	}
}

def nextDuneslamWave(event) {
	duneslamWaveCounter++ //Increment the wave counter
	//println "##### duneslamWaveCounter = ${duneslamWaveCounter} #####"
	if(duneslamWaveCounter <= DUNESLAM_GRUNT_WAVES) { //Check to see if fewer waves have been run than the # of grunt waves. If so, spawns a grunt wave.
		duneslamGruntsActive = true
		setDuneslamLevel(event)
		spawnWave(event)
	}
	if(duneslamWaveCounter > DUNESLAM_GRUNT_WAVES && duneslamWaveCounter <= DUNESLAM_LT_WAVES) {
		duneslamGruntsActive = false
		duneslamLTsActive = true
		setDuneslamLevel(event)
		spawnWave(event)
	}
	if(duneslamWaveCounter == DUNESLAM_WAVES) {
		playerList.clone().each() { it.centerPrint("The ground quakes violently, nearly knocking you from your feet, and then becomes still.") }
		crewSize = event.actor.getCrew().plus(playerList).size()
		setDuneslamLevel(event)
		spawnDuneslam(event)
	}
}

def setDuneslamLevel(event) {
	monsterLevel = 6.5
	event.actor.getCrew().clone().each() { //Checks the con level of each crew member and sets ML to that value - if anyone higher sets to max ML
		if(it.getConLevel() > monsterLevel && it.getConLevel() < MAX_MONSTER_LEVEL) {
			monsterLevel = it.getConLevel()
		} else if(it.getConLevel() > MAX_MONSTER_LEVEL) {
			monsterLevel = MAX_MONSTER_LEVEL
		}
	}
	playerList.clone().each() { //Checks the con level of everyone on the room and sets ML to that value - if anyone higher sets to max ML
		if(it.getConLevel() > monsterLevel && it.getConLevel() < MAX_MONSTER_LEVEL) {
			monsterLevel = it.getConLevel()
		} else if(it.getConLevel() >= MAX_MONSTER_LEVEL && it.getRoomName() == "DuneSlam_1") {
			monsterLevel = MAX_MONSTER_LEVEL
			it.centerPrint( "You must be overall CL 7.0 or lower for this task." ) 
			it.centerPrint( "Click 'MENU' and select 'Suppress CL' to lower your CL temporarily." ) 
			it.warp( "Beach_604", 655, 430, 6 )
		}
	}
	if(duneslamDifficulty == 1) { monsterLevel = monsterLevel - 0.6 }
	if(duneslamDifficulty == 3) { monsterLevel = monsterLevel + 0.5 }
	//println "##### monsterLevel = ${monsterLevel} #####"
}

def spawnWave(event) {
	myManager.schedule(5) { event2 ->
		crewSize = event.actor.getCrew().plus(playerList).size()
		//monsterLevel = monsterLevel + (MONSTER_LEVEL_MOD * (crewSize - 1)) //Adjusts ML based on crew size
		
		myManager.schedule(2) {
			if(duneslamGruntsActive == true) {
				if(duneslamDifficulty == 1) { spawnEasyGruntWave() }
				if(duneslamDifficulty == 2) { spawnMediumGruntWave() }
				if(duneslamDifficulty == 3) { spawnHardGruntWave() }
				myManager.schedule(DUNESLAM_GRUNT_WAVE_TIMER) {nextDuneslamWave(event)}
			}
			if(duneslamLTsActive == true) {
				if(duneslamDifficulty == 1) { spawnEasyLTWave() }
				if(duneslamDifficulty == 2) { spawnMediumLTWave() }
				if(duneslamDifficulty == 3) { spawnHardLTWave() }
				myManager.schedule(DUNESLAM_LT_WAVE_TIMER) {nextDuneslamWave(event)}
			}
		}
	}
}

def spawnEasyGruntWave() {
	gruntReference = random(duneslamGruntSpawnerMap.keySet())
	
	if(playerList.size() > crewSize) { crewSize = playerList.size() }
	
	if(crewSize <= 3) {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(0)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamGrunt1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt1

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamGrunt1.addHate(random(hateList), 1)
		}

		duneslamGrunt1.setDisplayName("Servant of Sand")
	} else {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(0)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamGrunt1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt1

		duneslamGrunt2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt2
	
		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamGrunt1.addHate(random(hateList), 1)
			duneslamGrunt2.addHate(random(hateList), 1)
		}

		duneslamGrunt1.setDisplayName("Servant of Sand")
		duneslamGrunt2.setDisplayName("Servant of Sand")
	}
}

def spawnMediumGruntWave() {
	gruntReference = random(duneslamGruntSpawnerMap.keySet())
	
	if(playerList.size() > crewSize) { crewSize = playerList.size() }
	
	if(crewSize <= 3) {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(0)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamGrunt1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt1
		
		duneslamGrunt2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt2

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamGrunt1.addHate(random(hateList), 1)
			duneslamGrunt2.addHate(random(hateList), 1)
		}

		duneslamGrunt1.setDisplayName("Servant of Sand")
		duneslamGrunt2.setDisplayName("Servant of Sand")
	} else {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(0)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamGrunt1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt1

		duneslamGrunt2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt2
		
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamGrunt3 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt3
	
		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamGrunt1.addHate(random(hateList), 1)
			duneslamGrunt2.addHate(random(hateList), 1)
			duneslamGrunt3.addHate(random(hateList), 1)
		}

		duneslamGrunt1.setDisplayName("Servant of Sand")
		duneslamGrunt2.setDisplayName("Servant of Sand")
		duneslamGrunt3.setDisplayName("Guardian of Dunes")
	}
}

def spawnHardGruntWave() {
	gruntReference = random(duneslamGruntSpawnerMap.keySet())
	
	if(playerList.size() > crewSize) { crewSize = playerList.size() }
	
	if(crewSize <= 3) {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamGrunt1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt1
		
		duneslamGrunt2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt2

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamGrunt1.addHate(random(hateList), 1)
			duneslamGrunt2.addHate(random(hateList), 1)
		}

		duneslamGrunt1.setDisplayName("Guardian of Dunes")
		duneslamGrunt2.setDisplayName("Guardian of Dunes")
	} else {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamGrunt1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt1

		duneslamGrunt2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt2
		
		duneslamGrunt3 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamGrunt3
	
		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamGrunt1.addHate(random(hateList), 1)
			duneslamGrunt2.addHate(random(hateList), 1)
			duneslamGrunt3.addHate(random(hateList), 1)
		}

		duneslamGrunt1.setDisplayName("Guardian of Dunes")
		duneslamGrunt2.setDisplayName("Guardian of Dunes")
		duneslamGrunt3.setDisplayName("Guardian of Dunes")
	}
}

def spawnEasyLTWave() {
	gruntReference = random(duneslamGruntSpawnerMap.keySet())
	
	if(playerList.size() > crewSize) { crewSize = playerList.size() }
	
	if(crewSize <= 3) {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(0)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamLT1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT1

		duneslamLT2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT2

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamLT1.addHate(random(hateList), 1)
			duneslamLT2.addHate(random(hateList), 2)
		}
		
		duneslamLT1.setDisplayName("Servant of Sand")
		duneslamLT2.setDisplayName("Servant of Sand")
	} else {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamLT1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT1

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamLT1.addHate(random(hateList), 1)
		}
		
		duneslamLT1.setDisplayName("Guardian of Dunes")
	}
}

def spawnMediumLTWave() {
	gruntReference = random(duneslamGruntSpawnerMap.keySet())
	
	if(playerList.size() > crewSize) { crewSize = playerList.size() }
	
	if(crewSize <= 3) {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamLT1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT1

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamLT1.addHate(random(hateList), 1)
		}
		
		duneslamLT1.setDisplayName("Guardian of Dunes")
	} else {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamLT1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT1
		
		duneslamLT2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT2

		hateListCleanUp()
		
		if(hateList.size() > 0) {
			duneslamLT1.addHate(random(hateList), 1)
			duneslamLT2.addHate(random(hateList), 1)
		}
		
		duneslamLT1.setDisplayName("Guardian of Dunes")
		duneslamLT2.setDisplayName("Guardian of Dunes")
	}
}

def spawnHardLTWave() {
	gruntReference = random(duneslamGruntSpawnerMap.keySet())
	
	if(playerList.size() > crewSize) { crewSize = playerList.size() }
	
	if(crewSize <= 3) {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamLT1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT1
		
		duneslamLT2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT2
		
		duneslamLT3 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT3

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamLT1.addHate(random(hateList), 1)
			duneslamLT2.addHate(random(hateList), 1)
			duneslamLT3.addHate(random(hateList), 1)
		}
		
		duneslamLT1.setDisplayName("Guardian of Dunes")
		duneslamLT2.setDisplayName("Guardian of Dunes")
		duneslamLT3.setDisplayName("Guardian of Dunes")
	} else {
		gruntSpawner = duneslamGruntSpawnerMap.get(gruntReference).get(1)
		gruntSpawner.setMonsterLevelForChildren(monsterLevel)
		
		duneslamLT1 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT1
		
		duneslamLT2 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT2
		
		duneslamLT3 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT3
		
		duneslamLT4 = gruntSpawner.forceSpawnNow()
		gruntList << duneslamLT4

		hateListCleanUp()

		if(hateList.size() > 0) {
			duneslamLT1.addHate(random(hateList), 1)
			duneslamLT2.addHate(random(hateList), 1)
			duneslamLT3.addHate(random(hateList), 1)
			duneslamLT4.addHate(random(hateList), 1)
		}
		
		duneslamLT1.setDisplayName("Guardian of Dunes")
		duneslamLT2.setDisplayName("Guardian of Dunes")
		duneslamLT3.setDisplayName("Guardian of Dunes")
		duneslamLT4.setDisplayName("Guardian of Dunes")
	}
}

def spawnDuneslam(event) {
	crewSize = event.actor.getCrew().plus(playerList).unique().size()
	duneslamSpawn.setMonsterLevelForChildren(monsterLevel)
	myManager.schedule(5) {
		Duneslam = duneslamSpawn.forceSpawnNow()
		Duneslam.setDisplayName("Duneslam")
		if(hateList.size() > 0) {
			Duneslam.addHate(random(hateList), 1)
		}
		
		runOnDeath(Duneslam) {
			duneslamRewards()
		}
	}
}

def duneslamRewards() {
	//println "##### GRANTING LOOT FOR DUNESLAM #####"
	playerList.clone().each() {
		if( it.getConLevel() <= 7.2 ) {
			if(duneslamDifficulty == 1) {
				rewardModifier = 1
				numGold = random(100, 150) * rewardModifier
				it.grantCoins(numGold)
				numOrbs = random(10, 15) * rewardModifier
				it.grantQuantityItem( 100257, numOrbs )
				it.centerPrint("You defeated Duneslam on 'Easy' difficulty, earning ${numGold} gold and ${numOrbs} orbs.")
			}
			if(duneslamDifficulty == 2) {
				rewardModifier = 2
				numGold = random(100, 150) * rewardModifier
				it.grantCoins(numGold)
				numOrbs = random(10, 15) * rewardModifier
				it.grantQuantityItem( 100257, numOrbs )
				it.centerPrint("You defeated Duneslam on 'Normal' difficulty, earning ${numGold} gold and ${numOrbs} orbs.")
			}
			if(duneslamDifficulty == 3) {
				rewardModifier = 4
				numGold = random(100, 150) * rewardModifier
				it.grantCoins(numGold)
				numOrbs = random(10, 15) * rewardModifier
				it.grantQuantityItem( 100257, numOrbs )
				it.centerPrint("You defeated Duneslam on 'Hard' difficulty, earning ${numGold} gold and ${numOrbs} orbs.")
			}
		} else {
			it.centerPrint( "Your overall CL is too high for this mission. No reward for you!" )
		}
	}
}

def hateListCleanUp() {
	hateList = playerList.clone()
	hateList.clone().each() {
		if(it.isDazed()) { hateList.remove(it) }
	}
}

def checkDazedStatus() {
	allDazed = false
	numDazed = 0
	playerList.clone().each { if( it.isDazed() ) { numDazed ++ } }
	if( numDazed >= playerList.size() ) {
		allDazed = true
		myRooms.DuneSlam_1.getActorList().each{
			if( isPlayer( it ) ) {
				it.centerPrint( "A wave sweeps across the island, pulling your lifeless body toward the shore." )
				it.warp( "Beach_604", 655, 430, 6 )
			}
		}			
	} else { myManager.schedule(5) { checkDazedStatus() } }
}