import com.gaiaonline.mmo.battle.script.*;

//==============================================
// SAVING THE SPIRITS                           
//==============================================

Lin = spawnNPC( "Otomi Lin-VQS", myRooms.OtRuins_807, 300, 580 )
Lin.setDisplayName( "Lin" )

//============================================
// SPAWNERS                                   
//============================================

ML = 7.5

//Make bug patrols with CL max limit
def savingSpiritsSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() < 8.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 8.0 or lower to help save the Otami Spirits from their doom!" ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//Warp the spawners around as needed, setting Homes at the pyres they are trying to rally around

//============================================
// PYRE SWITCHES                              
//============================================

pyre606 = makeSwitch( "Pyre_606", myRooms.OtRuins_606, 250, 340 )
pyre606.lock()
pyre606.setUsable( false )

pyre605 = makeSwitch( "Pyre_605", myRooms.OtRuins_605, 520, 340 )
pyre605.lock()
pyre605.setUsable( false )

pyre405 = makeSwitch( "Pyre_405", myRooms.OtRuins_405, 720, 390 )
pyre405.lock()
pyre405.setUsable( false )

pyre805 = makeSwitch( "Pyre_805", myRooms.OtRuins_805, 670, 230 )
pyre805.lock()
pyre805.setUsable( false )

flame606 = makeSwitch( "Flame_606", myRooms.OtRuins_606, 250, 340 )
flame606.lock()
flame606.setUsable( false )

flame605 = makeSwitch( "Flame_605", myRooms.OtRuins_605, 520, 340 )
flame605.lock()
flame605.setUsable( false )

flame405 = makeSwitch( "Flame_405", myRooms.OtRuins_405, 720, 390 )
flame405.lock()
flame405.setUsable( false )

flame805 = makeSwitch( "Flame_805", myRooms.OtRuins_805, 670, 230 )
flame805.lock()
flame805.setUsable( false )

//DESCRIPTION OF SCENARIO
//Enemies have their Home set at a Pyre position.
// - Trigger zone around the Pyre
// - Whenever a monster enters the Trigger zone, start the "pyre lighting" counter
// - If there are still monsters in that trigger zones after the first one gets there within 60 seconds, then the pyre gets lit, otherwise it stays doused
// - Use countdowns to show when pyres are in the process of being lit and delete the counter when the pyre is prevented from lighting
// - Use zoneBroadcasts to show the status of the pyres, whether lit or unlit
// - Name the pyres with cool names to differentiate them and show their positions on the player PDA maps during the event (get rid of flags if zone out, and add them if players come in after event starts)

//*************************************************************************************
//VARIABLES USED AT ALL PYRES (easy tuning...just change here and it works everywhere) 
//*************************************************************************************
monsterCaptureThreshold = 4
playerCaptureThreshold = 3
normalSpawnRate = 15
fastSpawnRate = 10
insaneSpawnRate = 5
baseML = 7.4

totalSecondsAllPyresCaptured = 0

gameStarted = false

//========================================
// INITIAL SPAWN LOGIC (starting logic)   
//========================================

//All pyres start lit ("on"). TTs are gathering energy. They should already exist in those rooms in-force. (Static once-only spawns.)
// - Move in from hidden spawn locations
// - Turn the pyre switch "on" when the forces arrive
// - Do the initial zoneBroadcast for help once all Pyres are lit

eventEnded = false
totalHateCollected = 0
masterHateCollector = [] as Set

myManager.schedule(3) { startSavingTheSpirits() }

def startSavingTheSpirits() {
	//Spawn the original guard contingents
	pyre606GuardsToSpawn = 5
	pyre606ResetGuardians()
	pyre605GuardsToSpawn = 5
	pyre605ResetGuardians()
	pyre405GuardsToSpawn = 5
	pyre405ResetGuardians()
	pyre805GuardsToSpawn = 5
	pyre805ResetGuardians()
	
	//Give the monsters 15 seconds to get close to the Pyre before turning the rest of the stuff on
	myManager.schedule(15) {
	
		//Turn on all the Pyres
		pyre606.on()
		flame606.on()
		sound( "pyre606Ignite" ).toZone()
		pyre605.on()
		flame605.on()
		sound( "pyre605Ignite" ).toZone()
		pyre405.on()
		flame405.on()
		sound( "pyre405Ignite" ).toZone()
		pyre805.on()
		flame805.on()
		sound( "pyre805Ignite" ).toZone()

		//Set the mini-map markers for the lit pyres
		addMiniMapMarker("pyre606Marker", "markerPyreLit", "OtRuins_606", 250, 340, "Pad Pyre")
		addMiniMapMarker("pyre605Marker", "markerPyreLit", "OtRuins_605", 560, 280, "Waterfall Pyre")
		addMiniMapMarker("pyre405Marker", "markerPyreLit", "OtRuins_405", 1000, 250, "Ledge Pyre")
		addMiniMapMarker("pyre805Marker", "markerPyreLit", "OtRuins_805", 670, 230, "Ground Pyre")
		
		pyre606PlayerRetakePossible = true
		pyre605PlayerRetakePossible = true
		pyre405PlayerRetakePossible = true
		pyre805PlayerRetakePossible = true
		
		sound( "savingTheSpiritsEventStart" ).toZone()
		
		myManager.schedule(3) {	
			gameStarted = true
			
			//then turn on the event once there are combatants 
			zoneBroadcast( "Otomi Lin-VQS", "The Spirits Are In Trouble!", "The Tiny Terrors are siphoning the lifeforce of the Otami Spirits! Douse the Spirit Pyre flames to slow how much power the Terrors can steal away!" )
			
			makeZoneTimer( "Saving Spirits Event", "0:30:0", "0:0:0").onCompletion( {
				removeZoneBroadcast( "The Spirits Are In Trouble!" )
				removeZoneTimer( "Saving Spirits Event" )
				removeZoneTimer("Pad Pyre Quenching")
				removeZoneTimer("Ledge Pyre Quenching")
				removeZoneTimer("Waterfall Pyre Quenching")
				removeZoneTimer("Ground Pyre Quenching")
				removeZoneTimer("Pad Pyre Threatened!")
				removeZoneTimer("Ledge Pyre Threatened!")
				removeZoneTimer("Waterfall Pyre Threatened!")
				removeZoneTimer("Ground Pyre Threatened!")
				
				pyre606SpawnOkay = false
				pyre605SpawnOkay = false
				pyre405SpawnOkay = false
				pyre805SpawnOkay = false

				/* THIS DOESN'T WORK YET
				pyre606TinyTerror.getChildren().values().each{ it.dispose() }
				pyre606FeatheredCoatl.getChildren().values().each{ it.dispose() }
				pyre606TinyWitchDoctor.getChildren().values().each{ it.dispose() }
				pyre605TinyTerror.getChildren().values().each{ it.dispose() }
				pyre605FeatheredCoatl.getChildren().values().each{ it.dispose() }
				pyre605TinyWitchDoctor.getChildren().values().each{ it.dispose() }
				pyre405TinyTerror.getChildren().values().each{ it.dispose() }
				pyre405FeatheredCoatl.getChildren().values().each{ it.dispose() }
				pyre405TinyWitchDoctor.getChildren().values().each{ it.dispose() }
				pyre805TinyTerror.getChildren().values().each{ it.dispose() }
				pyre805FeatheredCoatl.getChildren().values().each{ it.dispose() }
				pyre805TinyWitchDoctor.getChildren().values().each{ it.dispose() }
				*/

				if( !pyre606MonsterList.isEmpty() ) { pyre606MonsterList.clone().each{ if( !it.isDead() ) { it.dispose() } } }
				if( !pyre605MonsterList.isEmpty() ) { pyre605MonsterList.clone().each{ if( !it.isDead() ) { it.dispose() } } }
				if( !pyre405MonsterList.isEmpty() ) { pyre405MonsterList.clone().each{ if( !it.isDead() ) { it.dispose() } } }
				if( !pyre805MonsterList.isEmpty() ) { pyre805MonsterList.clone().each{ if( !it.isDead() ) { it.dispose() } } }

				removeMiniMapMarker( "pyre606Marker" )
				removeMiniMapMarker( "pyre605Marker" )
				removeMiniMapMarker( "pyre405Marker" )
				removeMiniMapMarker( "pyre805Marker" )
				
				pyre606.off()
				flame606.off()
				sound( "pyre606Snuff" ).toZone()
				pyre605.off()
				flame605.off()
				sound( "pyre605Snuff" ).toZone()
				pyre405.off()
				flame405.off()
				sound( "pyre405Snuff" ).toZone()
				pyre805.off()
				flame805.off()
				sound( "pyre805Snuff" ).toZone()
				
				if( pyre606PlayerRetakePossible == false ) {
					pyre606SecondsCaptured = 1800 - getZoneTimer( "pyre606CaptureTimer" ).getRemainingSeconds()
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre606SecondsCaptured
					removeZoneTimer( "pyre606CaptureTimer" )
				}
				if( pyre605PlayerRetakePossible == false ) {
					pyre605SecondsCaptured = 1800 - getZoneTimer( "pyre605CaptureTimer" ).getRemainingSeconds()
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre605SecondsCaptured
					removeZoneTimer( "pyre605CaptureTimer" )
				}
				if( pyre405PlayerRetakePossible == false ) {
					pyre405SecondsCaptured = 1800 - getZoneTimer( "pyre405CaptureTimer" ).getRemainingSeconds()
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre405SecondsCaptured
					removeZoneTimer( "pyre405CaptureTimer" )
				}
				if( pyre805PlayerRetakePossible == false ) {
					pyre805SecondsCaptured = 1800 - getZoneTimer( "pyre805CaptureTimer" ).getRemainingSeconds()
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre805SecondsCaptured
					removeZoneTimer( "pyre805CaptureTimer" )
				}

				myManager.schedule(2) {
					zoneBroadcast( "Otomi Lin-VQS", "They've Given Up!", "The Terrors gained some power from their attempts, but you definitely helped diminish how much they gained! Thank you, Gaians! The Spirits will recover due to your efforts." )
					myManager.schedule(15) {
						removeZoneBroadcast( "They've Given Up!" )
						eventDone()
					}
				}					
				if( !masterHateCollector.isEmpty() ) {
					//print out some data mining so we can look in logs during tests or live events
					println "************************************************"
					println "********** EVENT HATE SUMMARY ***************"
					println "**** The Player Hate Map for this event is: ****"
					println "**********       (OTAMI)     ***************"
					println ""
					println "masterHateCollector = ${masterHateCollector}"
					println ""
					println "totalHateCollected = ${ totalHateCollected }"
					println ""
					println "numPeople in Event = ${masterHateCollector.size()}"
					println ""
					println "average Hate per person = ${ totalHateCollected / masterHateCollector.size() }"
					println ""
					println "totalSecondsAllPyresCaptured = ${totalSecondsAllPyresCaptured} and pyreQuenchPercent = ${ totalSecondsAllPyresCaptured / 7200 } "
					println "********** EVENT HATE SUMMARY ***************"
					println "************************************************"

					rewardPlayers()
				}
			} ).start()
		}
	}
}

//======================
//REWARD ROUTINE        
//======================

//Total Possible Seconds for a Pyre to be Captured = 1800
//Four Pyres, so max possible seconds = 7200
//Seconds the pyres were quenched is "totalSecondsAllPyresCaptured"
//So, percentage of max that the fires were quenched is totalSecondsAllPyresCaptured/7200

averageHatePerPlayer = 15000
rewardHateThreshold = 4000
maxGoldGrant = 500
maxOrbGrant = 30
desiredChanceComponentReceived = 50

def rewardPlayers() {
	pyreQuenchPercent = totalSecondsAllPyresCaptured / 7200
	
	//It's VERY HARD to quench all pyres more than half the time, so double the pyreQuenchPercent
	pyreQuenchPercent = pyreQuenchPercent * 2
	
	//cap it (min & max)
	if( pyreQuenchPercent < 0.20 ) { pyreQuenchPercent = 0.20 } else if( pyreQuenchPercent > 1.0 ) { pyreQuenchPercent = 1.0 }
	masterHateCollector.each() {
		//make sure it's a player (pets are coming!) and make sure that player is still online
		if( isOnline( it ) && isPlayer( it ) && !isInOtherLayer( it ) ) {
			
			//Tell each player how they did against the max
			it.centerPrint( "You helped keep the fires quenched for a total of ${ totalSecondsAllPyresCaptured.intValue() } seconds out of a possible 7200." )
			
			if( it.getPlayerVar("Z10SavingSpiritsCurrentHate" ) >= rewardHateThreshold ) {
				//Gold grants are modified downward according to quenchPercentage
				goldGrant = ( maxGoldGrant * pyreQuenchPercent ).intValue()
				it.grantCoins( goldGrant )
				it.centerPrint( "You earned ${goldGrant.intValue()} gold!" )
			
				//grant orbs for killing LawnShark
				orbGrant = ( maxOrbGrant * pyreQuenchPercent ).intValue()
				it.grantQuantityItem( 100257, orbGrant )
				it.centerPrint( "You also earned ${orbGrant.intValue()} Charge Orbs!")

				//Increment a playerVar (by one) that records the number of times the player has participated in this particular event
				it.addPlayerVar( "Z10SavingSpiritsNumberOfVisits", 1 )

				//Increment the "All-Time" Hate Collector with the amount of hate accumulated during this event
				thisEventHate = it.getPlayerVar( "Z10SavingSpiritsCurrentHate" )
				it.addPlayerVar( "Z10SavingSpiritsAllTimeEventHate", thisEventHate )
				if( thisEventHate > 20000 ) { thisEventHate = 20000 } //cap at 20000 for a single event.
				it.centerPrint( "And you earned ${thisEventHate.intValue()} points toward Saving the Spirit Badges!" )

				//Award badges for fighting Airshark if beyond certain all-time Hate levels...
				if( it.getPlayerVar( "Z10SavingSpiritsCurrentHate" ) > averageHatePerPlayer * 1.2 ) { 
					it.updateQuest( 320, "Otomi Lin-VQS" ) //complete the FLAME QUENCHER badge
				}
				//... played the Event a certain number of times
				if( it.getPlayerVar( "Z10SavingSpiritsAllTimeEventHate" ) > averageHatePerPlayer * 10 ) { ///roughly about 10 aggressive play-throughs
					it.updateQuest( 321, "Otomi Lin-VQS" ) //complete the SERVANT OF THE SPIRITS badge
				}
				/*
				//chance that the loot item is awarded is also based directly on pyreQuenchPercent
				roll = random( 100 ) 
				if( roll < pyreQuenchPercent.intValue() ) { 
					//grant the loot component for this mini-event
					it.grantItem( "100444" ) //grant OTAMI ARTIFACT
					it.centerPrint( "You've found a rare OTAMI ARTIFACT!" )
				}
				*/
			} else {
				it.centerPrint( "You were not active enough during the event to earn rewards" )
			}
		}
	}
}

//==============================================================================================================================================
//PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606 PYRE606       
//==============================================================================================================================================

//Tiny Terror Spawner
pyre606TinyTerror = createStoppedEventSpawner( "OtRuins_606", "pyre606TinyTerror", "tiny_terror", 100 )
pyre606TinyTerror.setPos( 445, 125 )
pyre606TinyTerror.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre606TinyTerror.setHomeTetherForChildren( 3000 )
pyre606TinyTerror.setMiniEventSpec( savingSpiritsSpec )
pyre606TinyTerror.setMonsterLevelForChildren( ML )

//Feathered Coatl Spawner
pyre606FeatheredCoatl = createStoppedEventSpawner( "OtRuins_606", "pyre606FeatheredCoatl", "feathered_coatl", 100 )
pyre606FeatheredCoatl.setPos( 445, 125 )
pyre606FeatheredCoatl.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre606FeatheredCoatl.setHomeTetherForChildren( 3000 )
pyre606FeatheredCoatl.setMiniEventSpec( savingSpiritsSpec )
pyre606FeatheredCoatl.setMonsterLevelForChildren( ML )

//Tiny Witch Doctor Spawner
pyre606TinyWitchDoctor = createStoppedEventSpawner( "OtRuins_606", "pyre606TinyWitchDoctor", "tiny_terror_LT", 100 )
pyre606TinyWitchDoctor.setPos( 445, 125 )
pyre606TinyWitchDoctor.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre606TinyWitchDoctor.setHomeTetherForChildren( 3000 )
pyre606TinyWitchDoctor.setMiniEventSpec( savingSpiritsSpec )
pyre606TinyWitchDoctor.setMonsterLevelForChildren( ML )

//=========================================
// PYRE 606 VARIABLES & TRIGGER LOGIC      
//=========================================

pyre606MonsterList = [] as Set
pyre606MonsterListInTrigger = [] as Set
pyre606Players = [] as Set
pyre606PlayersInTrigger = [] as Set

pyre606Lit = true
pyre606MonsterTimerStarted = false
pyre606PlayerTimerStarted = false
pyre606PlayerRetakePossible = false
pyre606SpawnOkay = false
pyre606AcceleratedTimersStarted = false

pyre606SecondsCaptured = 0
pyre606RespawnTime = 20

pyre606SpawnerList = [ pyre606FeatheredCoatl, pyre606FeatheredCoatl, pyre606TinyTerror, pyre606TinyTerror, pyre606TinyTerror, pyre606TinyTerror, pyre606TinyWitchDoctor ]
pyre606SpawnerMap = [ 1: ["OtRuins_507", 360, 560 ], 2: ["OtRuins_406", 180, 570 ], 3: ["OtRuins_607", 620, 170], 4: ["OtRuins_707", 550, 160], 5: ["OtRuins_707", 190, 510], 6: ["OtRuins_507", 180, 50] ] 
pyre606GuardianPositions = [ 5 : ["OtRuins_606", 180, 270], 4 : ["OtRuins_606", 370, 280], 3: ["OtRuins_606", 130, 350], 2: ["OtRuins_606", 380, 390], 1: ["OtRuins_606", 238, 420] ]

//PYRE 606 TRIGGER ZONE
def pyre606Trigger = "pyre606Trigger"
myRooms.OtRuins_606.createTriggerZone( pyre606Trigger, 10, 160, 520, 480 )

//TRIGGER ZONE LOGIC
myManager.onTriggerIn( myRooms.OtRuins_606, pyre606Trigger ) { event ->
	if( gameStarted == true ) {
		//========= check for player recaptures: Enough players must be in the trigger zone or the recapture won't start ===========
		if( isPlayer( event.actor ) && event.actor.getConLevel() < 8.1 && pyre606PlayerTimerStarted == false && pyre606PlayerRetakePossible == true ) {
			pyre606PlayersInTrigger << event.actor
			//weed out the Dazed players
			if( !pyre606PlayersInTrigger.isEmpty() ) {
				pyre606PlayersInTrigger.clone().each{ 
					if( it.isDazed() || !isOnline( it ) ) { pyre606PlayersInTrigger.remove( it ) }
				}
			}

			//if enough players, then start the recapture attempt
			if( pyre606PlayersInTrigger.size() >= playerCaptureThreshold ) {
				pyre606PlayersInTrigger.clone().each{ it.centerPrint( "Stay in this area and keep the area clear of monsters to quench the Spirit Pyre flame!" ) }
				myManager.schedule(3) { pyre606PlayersInTrigger.clone().each{ it.centerPrint( "Keep it clear until the Pad Pyre timer expires! You have one minute!" ) } }
				pyre606PlayerCapture()
			//otherwise, tell the players they need more players in the area to start the recapture attempt
			} else {
				pyre606PlayersInTrigger.clone().each{ it.centerPrint( "You need ${playerCaptureThreshold - pyre606PlayersInTrigger.size()} more players near the Spirit Pyre to start quenching its flame!" ) }
			}
		}
		//=========== check for monster recaptures (enough monsters must be in the trigger zone or it won't start) ===============
		if( isMonster( event.actor ) && pyre606MonsterTimerStarted == false && pyre606PlayerRetakePossible == false && pyre606MonsterList.contains( event.actor ) ) {
			pyre606MonsterListInTrigger << event.actor
			//weed out the dead actors from the list
			if( !pyre606MonsterListInTrigger.isEmpty() ) {
				pyre606MonsterListInTrigger.clone().each{
					if( it.isDead() ) { pyre606MonsterListInTrigger.remove( it ) }
				}
			}

			//if enough monsters, then start the capture attempt
			if( pyre606MonsterListInTrigger.size() >= monsterCaptureThreshold ) {
				//make a player list of the players in the room and broadcast a message to them
				pyre606Players.clear()
				myRooms.OtRuins_606.getActorList().each{ if( isPlayer( it ) ) { pyre606Players << it } }
				if( !pyre606Players.isEmpty() ) {
					pyre606Players.clone().each{ it.centerPrint( "Look out! Too many Terrors got close to the Spirit Pyre and they've started the rekindling ritual!" ) }
				}
				pyre606MonsterListInTrigger.clear()
				pyre606MonsterCapture()
			}
		}
	}
}

//While the retake hasn't started yet, keep weeding out players from the list so the players have to group around the Pyre to start the retake attempt.
myManager.onTriggerOut( myRooms.OtRuins_606, pyre606Trigger ) { event ->
	if( isPlayer( event.actor ) && pyre606PlayerTimerStarted == false && pyre606PlayerRetakePossible == true ) {
		pyre606PlayersInTrigger.remove( event.actor )
	}
}

//=========================================
// PLAYER RECAPTURE LOGIC                  
//=========================================
def pyre606PlayerCapture() {
	//if the timer is not started, then start it
	if( pyre606PlayerTimerStarted == false ) {
		pyre606PlayerTimerStarted = true
		pyre606PlayersInTrigger.clear() //clear out the playersInTrigger list so that the next time the situation arises, it starts with a null list.

		numMonstersIn606 = 0

		//clear the player list and start checking the number of players in the room now instead of in the trigger zone
		//this allows more freedom to the players as they fight monsters
		pyre606Players.clear()
		myRooms.OtRuins_606.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre606Players << it } }

		//The monsters don't like it when the players try to quench the flame!
		//Crank up the respawn time and start throwing baddies at them!
		pyre606SpawnOkay = true
		
		pyre606BaselineTime = fastSpawnRate
		pyre606MonsterSwarms()
		
		//myManager.schedule(30) { pyre606BaselineTime = insaneSpawnRate }

		makeZoneTimer("Pad Pyre Quenching", "0:1:00", "0:0:0").onCompletion( { event -> 
			removeZoneTimer( "Pad Pyre Quenching" )
			pyre606PlayerTimerStarted = false
			
			
			//blank out the player list and create it again, this time including all AWAKE players in the room
			pyre606Players.clear()
			myRooms.OtRuins_606.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre606Players << it } }
			
			//if enough players are there and undazed, then they quench the pyre!
			if( pyre606Players.size() >= playerCaptureThreshold ) {
				//count the number of pyre606MonsterList that are spawned and actually *in* OtRuins_606 at that time
				myRooms.OtRuins_606.getActorList().each { if( isMonster( it ) && pyre606MonsterList.contains( it ) ) { numMonstersIn606 ++ } }

				//if that number is less than the capture threshold, then the players succeed
				if( numMonstersIn606 < monsterCaptureThreshold ) {
					//Quench that fire!
					flame606.off()
					pyre606Lit = false
					sound( "pyre606Snuff" ).toZone()

					//unset the retake flag so the monsters can start trying to capture it now
					pyre606PlayerRetakePossible = false

					addMiniMapMarker("pyre606Marker", "markerPyreUnlit", "OtRuins_606", 250, 340, "Pad Pyre")

					//announce the the quenching has occurred!
					zoneBroadcast( "Otomi Lin-VQS", "Pad Pyre Retaken!", "Fantastic! The Pad Pyre's flame has been quenched! Keep it that way!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Pad Pyre Retaken!" ) }

					//Start tracking the number of seconds that the players have held the Pyre
					//This timer will never get to 30 minutes.
					//It's intentionally too long so it can be used simply as a counter for how long the players capture the Pyre.
					//NOTE: This timer is stopped when the monsters recapture the Pyre
					makeZoneTimer( "pyre606CaptureTimer", "0:0:0", "0:30:0", false ).start()

					//Delay for a bit to allow players to "succeed", but then start streaming monsters toward this Pyre to retake it
					pyre606SpawnOkay = false
					myManager.schedule(30) { //This timer is the delay until the monsters start trying to retake the Pyre
						pyre606SpawnOkay = true
						pyre606BaselineTime = normalSpawnRate
						//every 30 seconds, increase the spawn rate until the players get overwhelmed
						pyre606AcceleratedTimersStarted = true
						pyre606fasterTimer = myManager.schedule(30) { pyre606BaselineTime = fastSpawnRate }
						pyre606fastestTimer = myManager.schedule(60) { pyre606BaselineTime = insaneSpawnRate }
						pyre606MonsterSwarms()
					}
				} else {
					//if too many players have left or are dazed when the timer hits zero, then the flame is unquenched and the process has to start over again
					pyre606BaselineTime = normalSpawnRate

					zoneBroadcast( "Otomi Lin-VQS", "Pad Pyre Still Lit!", "There were too many Terrors still at the Pad Pyre! Defeat more on your next try!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Pad Pyre Still Lit!" ) }
				}
			} else {
				pyre606BaselineTime = normalSpawnRate

				zoneBroadcast( "Otomi Lin-VQS", "Pad Pyre Still Burning!", "Oh no! There were too few of you at the Pad Pyre to quench the fire! Try again, warriors!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Pad Pyre Still Burning!" ) }
			}
		} ).start()
	}
}

//=========================================
// MONSTER RECAPTURE LOGIC                 
//=========================================
def pyre606MonsterCapture() {
	//if the timer is not started, then start it
	if( pyre606MonsterTimerStarted == false ) {

		//if the timers from the Player capture haven't triggered yet, cancel them
		if( pyre606AcceleratedTimersStarted == true ) {
			pyre606AcceleratedTimersStarted = false
			pyre606fasterTimer.cancel()
			pyre606fastestTimer.cancel()
		}

		//Accelerate the spawning of monsters to "come help" light the pyre
		pyre606BaselineTime = fastSpawnRate
		
		pyre606MonsterTimerStarted = true

		makeZoneTimer("Pad Pyre Threatened!", "0:1:0", "0:0:0").onCompletion( { event ->
			removeZoneTimer( "Pad Pyre Threatened!" )
			pyre606MonsterTimerStarted = false

			pyre606BaselineTime = normalSpawnRate

			numMonstersIn606 = 0
			myRooms.OtRuins_606.getActorList().each { if( isMonster( it ) && pyre606MonsterList.contains( it ) ) { numMonstersIn606 ++ } }
			//If the monsters still exist in the trigger zone, then the fire gets LIT!
			if( numMonstersIn606 >= (monsterCaptureThreshold).intValue() ) {
				//C'mon baby, light that fire
				flame606.on()
				sound( "pyre606Ignite" ).toZone()
				pyre606Lit = true

				//IF the players have captured the pyre previously, then record how long the pyre was captured before it becomes threatened
				if( pyre606PlayerRetakePossible == false ) {
					pyre606SecondsCaptured = 1800 - getZoneTimer( "pyre606CaptureTimer" ).getRemainingSeconds()

					//preserve how many seconds for the reward stuff later on
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre606SecondsCaptured
					removeZoneTimer( "pyre606CaptureTimer" )
				}

				//set a flag so that a player retake is now possible
				pyre606PlayerRetakePossible = true
				
				pyre606GuardsToSpawn = 5
				pyre606ResetGuardians()
				
				//stop the extra guardian spawns after resetting the guards
				pyre606SpawnOkay = false

				addMiniMapMarker("pyre606Marker", "markerPyreLit", "OtRuins_606", 250, 340, "Pad Pyre")


				//tell the players that the fire is lit
				zoneBroadcast( "Otomi Lin-VQS", "Oh no! Pad Pyre Re-lit!", "The Terrors managed to re-light the Spirit Pyre on the Lily Pads! Put it out!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Oh no! Pad Pyre Re-lit!" ) }


			//if all monsters have ALL been killed,the remove the timer and congratulate the players for the successful defense
			} else {
				zoneBroadcast( "Otomi Lin-VQS", "Pad Pyre Defended!", "Congratulations! The Pad Pyre has been successfully defended! Keep it quenched!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Pad Pyre Defended!" ) }

			}
		} ).start()
	}
}

//TODO: Set up alternate spawn locations and logic for feathered coatls so they can come from screwy directions

//=========================================
// MONSTER SWARMING LOGIC                  
//=========================================
def pyre606MonsterSwarms() {
	if( pyre606SpawnOkay == true ) {
		//count the number of players in the room
		pyre606Players.clear()
		myRooms.OtRuins_606.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre606Players << it } }

		//only spawn monsters if there are fewer than the number of players in the room
		maxMonsterSpawnSize = pyre606Players.size() + 4
		if( maxMonsterSpawnSize > 8 ) { maxMonsterSpawnSize = 8 }

		//count the number of monsters on the Pyre list that are actually in the room and spawn more if needed
		if( pyre606MonsterList.size() < maxMonsterSpawnSize ) { 
			
			pyre606FindRandomSpawnLocation()
			
			//spawn a random FC, TT, or Witch Doctor and send it toward the Pyre
			pyre606Spawner = random( pyre606SpawnerList )
			pyre606Spawner.warp( pyre606SpawnRoom, pyre606SpawnX, pyre606SpawnY )
			
			pyre606FindRandomHomeLocation()
			
			pyre606Spawner.setHomeForChildren( pyre606HomeRoom, pyre606HomeX, pyre606HomeY )
			
			//set a random ML
			roll = random( 10 )
			ML = baseML + ( roll * 0.1 )
			pyre606Spawner.setMonsterLevelForChildren( ML )
			
			pyre606Monster = pyre606Spawner.forceSpawnNow()
			//keep track of its Hate list when it dies for use in rewards later on
			runOnDeath( pyre606Monster, { event ->
				pyre606MonsterList.remove( event.actor )
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed monsters in this Event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, terrorHate ->
					if( isPlayer( player ) ) {
						if( terrorHate > 700 ) { terrorHate = 700 }
						totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
					}
				}
			} )
			//add the spawn to the list so you can keep accurate count of the number of monsters in the room
			pyre606MonsterList << pyre606Monster

		}
		//Adjust the base line timer according to the number of players within the room
		pyre606Multiplier = ( ( 1.0 - ( pyre606Players.size() * 0.1 ) ) + 0.1 ).intValue()

		//prevent the multiplier from getting too stupidly small, regardless of how many players are in the room
		if( pyre606Multiplier < 0.5 ) { pyre606Multiplier = 0.5 }
		pyre606RespawnTime = pyre606BaselineTime * pyre606Multiplier

		//create a range of respawn times so it's not so "metronome-like"
		pyre606MinRespawnTime = (pyre606RespawnTime * 0.8).intValue()
		if( pyre606MinRespawnTime < 1 ) { pyre606MinRespawnTime = 1 }

		pyre606MaxRespawnTime = (pyre606RespawnTime * 1.2).intValue()
		if( pyre606MaxRespawnTime < 1 ) { pyre606MaxRespawnTime = 1 }

		myManager.schedule( random( pyre606MinRespawnTime, pyre606MaxRespawnTime ) ) { pyre606MonsterSwarms() }
	}
}

def pyre606FindRandomSpawnLocation() {
	//figure out a random room and position
	pyre606SpawnCoords = pyre606SpawnerMap.get( random( 6 ) )
	pyre606SpawnRoom = pyre606SpawnCoords.get(0)
	pyre606SpawnX = pyre606SpawnCoords.get(1).intValue()
	pyre606SpawnY = pyre606SpawnCoords.get(2).intValue()
}

def pyre606FindRandomHomeLocation() {
	//figure out a random room and position
	pyre606HomeCoords = pyre606GuardianPositions.get( random( 5 ) )
	pyre606HomeRoom = pyre606HomeCoords.get(0)
	pyre606HomeX = pyre606HomeCoords.get(1).intValue()
	pyre606HomeY = pyre606HomeCoords.get(2).intValue()
}	

//=========================================
// PYRE GUARDIAN GROUP SPAWN LOGIC         
//=========================================
def pyre606ResetGuardians() {
	if( pyre606GuardsToSpawn > 0 ) {
		if( pyre606GuardsToSpawn > 1 ) {
			pyre606Spawner = pyre606TinyTerror
		} else {
			pyre606Spawner = pyre606TinyWitchDoctor
		}

		//figure out a random spawn room and position
		pyre606FindRandomSpawnLocation()
		pyre606Spawner.warp( pyre606SpawnRoom, pyre606SpawnX, pyre606SpawnY )
		
		//now figure out which position they should set as their Home
		pyre606GuardCoords = pyre606GuardianPositions.get( pyre606GuardsToSpawn )
		pyre606GuardRoom = pyre606GuardCoords.get( 0 )
		pyre606GuardX = pyre606GuardCoords.get(1).intValue()
		pyre606GuardY = pyre606GuardCoords.get(2).intValue()
		
		pyre606Spawner.setHomeForChildren( pyre606GuardRoom, pyre606GuardX, pyre606GuardY )

		//set a random ML
		roll = random( 10 )
		ML = baseML + ( roll * 0.1 )
		pyre606Spawner.setMonsterLevelForChildren( ML )

		pyre606Monster = pyre606Spawner.forceSpawnNow()
		pyre606MonsterList << pyre606Monster

		//keep track of its Hate list when it dies for use in rewards later on
		runOnDeath( pyre606Monster, { event ->
			pyre606MonsterList.remove( event.actor )
			event.actor.getHated().each{
				//reset the players eventHate collector if this is the first time they've killed monsters in this Event
				if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
					it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
				}
				if( isPlayer( it ) ) { masterHateCollector << it }
			}
			event.actor.getHatedMap().each() { player, terrorHate ->
				if( isPlayer( player ) ) {
					if( terrorHate > 700 ) { terrorHate = 700 }
					totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
					//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
					player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
				}
			}
		} )
		
		pyre606GuardsToSpawn --
		myManager.schedule(1) { pyre606ResetGuardians() }
	}
}

//==============================================================================================================================================
//==============================================================================================================================================
		
//==============================================================================================================================================
//PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605 PYRE605       
//==============================================================================================================================================

//Tiny Terror Spawner
pyre605TinyTerror = createStoppedEventSpawner( "OtRuins_605", "pyre605TinyTerror", "tiny_terror", 100 )
pyre605TinyTerror.setPos( 445, 150 )
pyre605TinyTerror.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre605TinyTerror.setHomeTetherForChildren( 3000 )
pyre605TinyTerror.setMiniEventSpec( savingSpiritsSpec )
pyre605TinyTerror.setMonsterLevelForChildren( ML )

//Feathered Coatl Spawner
pyre605FeatheredCoatl = createStoppedEventSpawner( "OtRuins_605", "pyre605FeatheredCoatl", "feathered_coatl", 100 )
pyre605FeatheredCoatl.setPos( 445, 150 )
pyre605FeatheredCoatl.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre605FeatheredCoatl.setHomeTetherForChildren( 3000 )
pyre605FeatheredCoatl.setMiniEventSpec( savingSpiritsSpec )
pyre605FeatheredCoatl.setMonsterLevelForChildren( ML )

//Tiny Witch Doctor Spawner
pyre605TinyWitchDoctor = createStoppedEventSpawner( "OtRuins_605", "pyre605TinyWitchDoctor", "tiny_terror_LT", 100 )
pyre605TinyWitchDoctor.setPos( 445, 150 )
pyre605TinyWitchDoctor.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre605TinyWitchDoctor.setHomeTetherForChildren( 3000 )
pyre605TinyWitchDoctor.setMiniEventSpec( savingSpiritsSpec )
pyre605TinyWitchDoctor.setMonsterLevelForChildren( ML )

//=========================================
// PYRE 605 VARIABLES & TRIGGER LOGIC      
//=========================================

pyre605MonsterList = [] as Set
pyre605MonsterListInTrigger = [] as Set
pyre605Players = [] as Set
pyre605PlayersInTrigger = [] as Set

pyre605Lit = true
pyre605MonsterTimerStarted = false
pyre605PlayerTimerStarted = false
pyre605PlayerRetakePossible = false
pyre605SpawnOkay = false
pyre605AcceleratedTimersStarted = false

pyre605SecondsCaptured = 0
pyre605RespawnTime = 20

pyre605SpawnerList = [ pyre605FeatheredCoatl, pyre605FeatheredCoatl, pyre605TinyTerror, pyre605TinyTerror, pyre605TinyTerror, pyre605TinyTerror, pyre605TinyWitchDoctor ]
pyre605SpawnerMap = [ 1: ["OtRuins_505", 240, 500 ], 2: ["OtRuins_504", 970, 640 ], 3: ["OtRuins_704", 850, 540], 4: ["OtRuins_706", 140, 200], 5: ["OtRuins_705", 605, 155] ] 
pyre605GuardianPositions = [ 5 : ["OtRuins_605", 410, 260], 4 : ["OtRuins_605", 540, 230], 3: ["OtRuins_605", 610, 310], 2: ["OtRuins_605", 420, 360], 1: ["OtRuins_605", 540, 420] ]

//PYRE 605 TRIGGER ZONE
def pyre605Trigger = "pyre605Trigger"
myRooms.OtRuins_605.createTriggerZone( pyre605Trigger, 275, 120, 710, 440 )

//TRIGGER ZONE LOGIC
myManager.onTriggerIn( myRooms.OtRuins_605, pyre605Trigger ) { event ->
	if( gameStarted == true ) {
		//========= check for player recaptures: Enough players must be in the trigger zone or the recapture won't start ===========
		if( isPlayer( event.actor ) && event.actor.getConLevel() < 8.1 && pyre605PlayerTimerStarted == false && pyre605PlayerRetakePossible == true ) {
			pyre605PlayersInTrigger << event.actor
			//weed out the Dazed players
			if( !pyre605PlayersInTrigger.isEmpty() ) {
				pyre605PlayersInTrigger.clone().each{ 
					if( it.isDazed() || !isOnline( it ) ) { pyre605PlayersInTrigger.remove( it ) }
				}
			}

			//if enough players, then start the recapture attempt
			if( pyre605PlayersInTrigger.size() >= playerCaptureThreshold ) {
				pyre605PlayersInTrigger.clone().each{ it.centerPrint( "Stay in this area and keep the area clear of monsters to quench the Spirit Pyre flame!" ) }
				myManager.schedule(3) { pyre605PlayersInTrigger.clone().each{ it.centerPrint( "Keep it clear until the Waterfall Pyre timer expires! You have one minute!" ) } }
				pyre605PlayerCapture()
			//otherwise, tell the players they need more players in the area to start the recapture attempt
			} else {
				pyre605PlayersInTrigger.clone().each{ it.centerPrint( "You need ${playerCaptureThreshold - pyre605PlayersInTrigger.size()} more players near the Spirit Pyre to start quenching its flame!" ) }
			}
		}
		//=========== check for monster recaptures (enough monsters must be in the trigger zone or it won't start) ===============
		if( isMonster( event.actor ) && pyre605MonsterTimerStarted == false && pyre605PlayerRetakePossible == false && pyre605MonsterList.contains( event.actor ) ) {
			pyre605MonsterListInTrigger << event.actor
			//weed out the dead actors from the list
			if( !pyre605MonsterListInTrigger.isEmpty() ) {
				pyre605MonsterListInTrigger.clone().each{
					if( it.isDead() ) { pyre605MonsterListInTrigger.remove( it ) }
				}
			}

			//if enough monsters, then start the capture attempt
			if( pyre605MonsterListInTrigger.size() >= monsterCaptureThreshold ) {
				//make a player list of the players in the room and broadcast a message to them
				pyre605Players.clear()
				myRooms.OtRuins_605.getActorList().each{ if( isPlayer( it ) ) { pyre605Players << it } }
				if( !pyre605Players.isEmpty() ) {
					pyre605Players.clone().each{ it.centerPrint( "Look out! Too many Terrors got close to the Spirit Pyre and they've started the rekindling ritual!" ) }
				}
				pyre605MonsterListInTrigger.clear()
				pyre605MonsterCapture()
			}
		}
	}
}

//While the retake hasn't started yet, keep weeding out players from the list so the players have to group around the Pyre to start the retake attempt.
myManager.onTriggerOut( myRooms.OtRuins_605, pyre605Trigger ) { event ->
	if( isPlayer( event.actor ) && pyre605PlayerTimerStarted == false && pyre605PlayerRetakePossible == true ) {
		pyre605PlayersInTrigger.remove( event.actor )
	}
}

//=========================================
// PLAYER RECAPTURE LOGIC                  
//=========================================
def pyre605PlayerCapture() {
	//if the timer is not started, then start it
	if( pyre605PlayerTimerStarted == false ) {
		pyre605PlayerTimerStarted = true
		pyre605PlayersInTrigger.clear() //clear out the playersInTrigger list so that the next time the situation arises, it starts with a null list.

		numMonstersIn605 = 0

		//clear the player list and start checking the number of players in the room now instead of in the trigger zone
		//this allows more freedom to the players as they fight monsters
		pyre605Players.clear()
		myRooms.OtRuins_605.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre605Players << it } }

		//The monsters don't like it when the players try to quench the flame!
		//Crank up the respawn time and start throwing baddies at them!
		pyre605SpawnOkay = true
		
		pyre605BaselineTime = fastSpawnRate
		pyre605MonsterSwarms()
				
		//myManager.schedule(30) { pyre605BaselineTime = insaneSpawnRate }

		makeZoneTimer("Waterfall Pyre Quenching", "0:1:00", "0:0:0").onCompletion( { event -> 
			removeZoneTimer( "Waterfall Pyre Quenching" )
			pyre605PlayerTimerStarted = false
			
			//blank out the player list and create it again, this time including all AWAKE players in the room
			pyre605Players.clear()
			myRooms.OtRuins_605.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre605Players << it } }
			
			//if enough players are there and undazed, then they quench the pyre!
			if( pyre605Players.size() >= playerCaptureThreshold ) {
				//count the number of pyre605MonsterList that are spawned and actually *in* OtRuins_605 at that time
				myRooms.OtRuins_605.getActorList().each { if( isMonster( it ) && pyre605MonsterList.contains( it ) ) { numMonstersIn605 ++ } }

				//if that number is less than the capture threshold, then the players succeed
				if( numMonstersIn605 < monsterCaptureThreshold ) {
					//Quench that fire!
					flame605.off()
					pyre605Lit = false
					sound( "pyre605Snuff" ).toZone()

					//unset the retake flag so the monsters can start trying to capture it now
					pyre605PlayerRetakePossible = false

					addMiniMapMarker("pyre605Marker", "markerPyreUnlit", "OtRuins_605", 560, 280, "Waterfall Pyre")

					//announce the the quenching has occurred!
					zoneBroadcast( "Otomi Lin-VQS", "Waterfall Pyre Retaken!", "Fantastic! The Waterfall Pyre's flame has been quenched! Keep it that way!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Waterfall Pyre Retaken!" ) }

					//Start tracking the number of seconds that the players have held the Pyre
					//This timer will never get to 30 minutes.
					//It's intentionally too long so it can be used simply as a counter for how long the players capture the Pyre.
					//NOTE: This timer is stopped when the monsters recapture the Pyre
					makeZoneTimer( "pyre605CaptureTimer", "0:0:0", "0:30:0", false ).start()

					//Delay for a bit to allow players to "succeed", but then start streaming monsters toward this Pyre to retake it
					pyre605SpawnOkay = false
					myManager.schedule(30) { //This timer is the delay until the monsters start trying to retake the Pyre
						pyre605SpawnOkay = true
						pyre605BaselineTime = normalSpawnRate
						//every 30 seconds, increase the spawn rate until the players get overwhelmed
						pyre605AcceleratedTimersStarted = true
						pyre605fasterTimer = myManager.schedule(30) { pyre605BaselineTime = fastSpawnRate }
						pyre605fastestTimer = myManager.schedule(60) { pyre605BaselineTime = insaneSpawnRate }
						pyre605MonsterSwarms()
					}
				} else {
					//if too many players have left or are dazed when the timer hits zero, then the flame is unquenched and the process has to start over again
					pyre605BaselineTime = normalSpawnRate

					zoneBroadcast( "Otomi Lin-VQS", "Waterfall Pyre Still Lit!", "There were too many Terrors still at the Waterfall Pyre! Defeat more on your next try!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Waterfall Pyre Still Lit!" ) }
				}
			} else {
				pyre605BaselineTime = normalSpawnRate

				zoneBroadcast( "Otomi Lin-VQS", "Waterfall Pyre Still Burning!", "Oh no! There were too few of you at the Waterfall Pyre to quench the fire! Try again, warriors!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Waterfall Pyre Still Burning!" ) }
			}
		} ).start()
	}
}

//=========================================
// MONSTER RECAPTURE LOGIC                 
//=========================================
def pyre605MonsterCapture() {
	//if the timer is not started, then start it
	if( pyre605MonsterTimerStarted == false ) {

		//if the timers from the Player capture haven't triggered yet, cancel them
		if( pyre605AcceleratedTimersStarted == true ) {
			pyre605AcceleratedTimersStarted = false
			pyre605fasterTimer.cancel()
			pyre605fastestTimer.cancel()
		}

		//Accelerate the spawning of monsters to "come help" light the pyre
		pyre605BaselineTime = fastSpawnRate
		
		pyre605MonsterTimerStarted = true

		makeZoneTimer("Waterfall Pyre Threatened!", "0:1:0", "0:0:0").onCompletion( { event ->
			removeZoneTimer( "Waterfall Pyre Threatened!" )
			pyre605MonsterTimerStarted = false

			pyre605BaselineTime = normalSpawnRate

			numMonstersIn605 = 0
			myRooms.OtRuins_605.getActorList().each { if( isMonster( it ) && pyre605MonsterList.contains( it ) ) { numMonstersIn605 ++ } }
			//If the monsters still exist in the trigger zone, then the fire gets LIT!
			if( numMonstersIn605 >= (monsterCaptureThreshold).intValue() ) {
				//C'mon baby, light that fire
				flame605.on()
				sound( "pyre605Ignite" ).toZone()
				pyre605Lit = true

				//IF the players have captured the pyre previously, then record how long the pyre was captured before it becomes threatened
				if( pyre605PlayerRetakePossible == false ) {
					pyre605SecondsCaptured = 1800 - getZoneTimer( "pyre605CaptureTimer" ).getRemainingSeconds()
					
					//preserve how many seconds for the reward stuff later on
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre605SecondsCaptured
					removeZoneTimer( "pyre605CaptureTimer" )
				}

				//set a flag so that a player retake is now possible
				pyre605PlayerRetakePossible = true
				
				pyre605GuardsToSpawn = 5
				pyre605ResetGuardians()
				
				//stop the extra guardian spawns after resetting the guards
				pyre605SpawnOkay = false

				addMiniMapMarker("pyre605Marker", "markerPyreLit", "OtRuins_605", 560, 280, "Waterfall Pyre")


				//tell the players that the fire is lit
				zoneBroadcast( "Otomi Lin-VQS", "Oh no! Waterfall Pyre Re-lit!", "The Terrors managed to re-light the Spirit Pyre near the Waterfalls! Put it out!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Oh no! Waterfall Pyre Re-lit!" ) }


			//if all monsters have ALL been killed,the remove the timer and congratulate the players for the successful defense
			} else {
				zoneBroadcast( "Otomi Lin-VQS", "Waterfall Pyre Defended!", "Congratulations! The Waterfall Pyre has been successfully defended! Keep it quenched!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Waterfall Pyre Defended!" ) }

			}
		} ).start()
	}
}

//=========================================
// MONSTER SWARMING LOGIC                  
//=========================================
def pyre605MonsterSwarms() {
	if( pyre605SpawnOkay == true ) {
		//count the number of players in the room
		pyre605Players.clear()
		myRooms.OtRuins_605.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre605Players << it } }

		//only spawn monsters if there are fewer than the number of players in the room
		maxMonsterSpawnSize = pyre605Players.size() + 4
		if( maxMonsterSpawnSize > 8 ) { maxMonsterSpawnSize = 8 }

		//count the number of monsters on the Pyre list that are actually in the room and spawn more if needed
		if( pyre605MonsterList.size() < maxMonsterSpawnSize ) { 
			
			pyre605FindRandomSpawnLocation()
			
			//spawn a random FC, TT, or Witch Doctor and send it toward the Pyre
			pyre605Spawner = random( pyre605SpawnerList )
			pyre605Spawner.warp( pyre605SpawnRoom, pyre605SpawnX, pyre605SpawnY )
			
			pyre605FindRandomHomeLocation()
			
			pyre605Spawner.setHomeForChildren( pyre605HomeRoom, pyre605HomeX, pyre605HomeY )

			//set a random ML
			roll = random( 10 )
			ML = baseML + ( roll * 0.1 )
			pyre605Spawner.setMonsterLevelForChildren( ML )

			pyre605Monster = pyre605Spawner.forceSpawnNow()
			//add the spawn to the list so you can keep accurate count of the number of monsters in the room
			pyre605MonsterList << pyre605Monster

			//keep track of its Hate list when it dies for use in rewards later on
			runOnDeath( pyre605Monster, { event ->
				pyre605MonsterList.remove( event.actor )
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed monsters in this Event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, terrorHate ->
					if( isPlayer( player ) ) {
						if( terrorHate > 700 ) { terrorHate = 700 }
						totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
					}
				}
			} )

		}
		//Adjust the base line timer according to the number of players within the room
		pyre605Multiplier = ( ( 1.0 - ( pyre605Players.size() * 0.1 ) ) + 0.1 ).intValue()

		//prevent the multiplier from getting too stupidly small, regardless of how many players are in the room
		if( pyre605Multiplier < 0.5 ) { pyre605Multiplier = 0.5 }
		pyre605RespawnTime = pyre605BaselineTime * pyre605Multiplier

		//create a range of respawn times so it's not so "metronome-like"
		pyre605MinRespawnTime = (pyre605RespawnTime * 0.8).intValue()
		if( pyre605MinRespawnTime < 1 ) { pyre605MinRespawnTime = 1 }

		pyre605MaxRespawnTime = (pyre605RespawnTime * 1.2).intValue()
		if( pyre605MaxRespawnTime < 1 ) { pyre605MaxRespawnTime = 1 }

		myManager.schedule( random( pyre605MinRespawnTime, pyre605MaxRespawnTime ) ) { pyre605MonsterSwarms() }
	}
}

def pyre605FindRandomSpawnLocation() {
	//figure out a random room and position
	pyre605SpawnCoords = pyre605SpawnerMap.get( random( 5 ) )
	pyre605SpawnRoom = pyre605SpawnCoords.get(0)
	pyre605SpawnX = pyre605SpawnCoords.get(1).intValue()
	pyre605SpawnY = pyre605SpawnCoords.get(2).intValue()
}

def pyre605FindRandomHomeLocation() {
	//figure out a random room and position
	pyre605HomeCoords = pyre605GuardianPositions.get( random( 5 ) )
	pyre605HomeRoom = pyre605HomeCoords.get(0)
	pyre605HomeX = pyre605HomeCoords.get(1).intValue()
	pyre605HomeY = pyre605HomeCoords.get(2).intValue()
}	

//=========================================
// PYRE GUARDIAN GROUP SPAWN LOGIC         
//=========================================
def pyre605ResetGuardians() {
	if( pyre605GuardsToSpawn > 0 ) {
		if( pyre605GuardsToSpawn > 1 ) {
			pyre605Spawner = pyre605TinyTerror
		} else {
			pyre605Spawner = pyre605TinyWitchDoctor
		}

		//figure out a random spawn room and position
		pyre605FindRandomSpawnLocation()
		pyre605Spawner.warp( pyre605SpawnRoom, pyre605SpawnX, pyre605SpawnY )
		
		//now figure out which position they should set as their Home
		pyre605GuardCoords = pyre605GuardianPositions.get( pyre605GuardsToSpawn )
		pyre605GuardRoom = pyre605GuardCoords.get( 0 )
		pyre605GuardX = pyre605GuardCoords.get(1).intValue()
		pyre605GuardY = pyre605GuardCoords.get(2).intValue()
		
		pyre605Spawner.setHomeForChildren( pyre605GuardRoom, pyre605GuardX, pyre605GuardY )

		//set a random ML
		roll = random( 10 )
		ML = baseML + ( roll * 0.1 )
		pyre605Spawner.setMonsterLevelForChildren( ML )

		pyre605Monster = pyre605Spawner.forceSpawnNow()
		pyre605MonsterList << pyre605Monster

		//keep track of its Hate list when it dies for use in rewards later on
		runOnDeath( pyre605Monster, { event ->
			pyre605MonsterList.remove( event.actor )
			event.actor.getHated().each{
				//reset the players eventHate collector if this is the first time they've killed monsters in this Event
				if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
					it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
				}
				if( isPlayer( it ) ) { masterHateCollector << it }
			}
			event.actor.getHatedMap().each() { player, terrorHate ->
				if( isPlayer( player ) ) {
					if( terrorHate > 700 ) { terrorHate = 700 }
					totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
					//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
					player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
				}
			}
		} )
		
		pyre605GuardsToSpawn --
		myManager.schedule(1) { pyre605ResetGuardians() }
	}
}

//==============================================================================================================================================
//==============================================================================================================================================

//==============================================================================================================================================
//PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405 PYRE405       
//==============================================================================================================================================

//Tiny Terror Spawner
pyre405TinyTerror = createStoppedEventSpawner( "OtRuins_405", "pyre405TinyTerror", "tiny_terror", 100 )
pyre405TinyTerror.setPos( 445, 170 )
pyre405TinyTerror.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre405TinyTerror.setHomeTetherForChildren( 3000 )
pyre405TinyTerror.setMiniEventSpec( savingSpiritsSpec )
pyre405TinyTerror.setMonsterLevelForChildren( ML )

//Feathered Coatl Spawner
pyre405FeatheredCoatl = createStoppedEventSpawner( "OtRuins_405", "pyre405FeatheredCoatl", "feathered_coatl", 100 )
pyre405FeatheredCoatl.setPos( 445, 170 )
pyre405FeatheredCoatl.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre405FeatheredCoatl.setHomeTetherForChildren( 3000 )
pyre405FeatheredCoatl.setMiniEventSpec( savingSpiritsSpec )
pyre405FeatheredCoatl.setMonsterLevelForChildren( ML )

//Tiny Witch Doctor Spawner
pyre405TinyWitchDoctor = createStoppedEventSpawner( "OtRuins_405", "pyre405TinyWitchDoctor", "tiny_terror_LT", 100 )
pyre405TinyWitchDoctor.setPos( 445, 170 )
pyre405TinyWitchDoctor.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre405TinyWitchDoctor.setHomeTetherForChildren( 3000 )
pyre405TinyWitchDoctor.setMiniEventSpec( savingSpiritsSpec )
pyre405TinyWitchDoctor.setMonsterLevelForChildren( ML )

//=========================================
// PYRE 405 VARIABLES & TRIGGER LOGIC      
//=========================================

pyre405MonsterList = [] as Set
pyre405MonsterListInTrigger = [] as Set
pyre405Players = [] as Set
pyre405PlayersInTrigger = [] as Set

pyre405Lit = true
pyre405MonsterTimerStarted = false
pyre405PlayerTimerStarted = false
pyre405PlayerRetakePossible = false
pyre405SpawnOkay = false
pyre405AcceleratedTimersStarted = false

pyre405SecondsCaptured = 0
pyre405RespawnTime = 20

pyre405SpawnerList = [ pyre405FeatheredCoatl, pyre405FeatheredCoatl, pyre405TinyTerror, pyre405TinyTerror, pyre405TinyTerror, pyre405TinyTerror, pyre405TinyWitchDoctor ]
pyre405SpawnerMap = [ 1: ["OtRuins_304", 860, 580 ], 2: ["OtRuins_404", 510, 560 ] ] 
pyre405GuardianPositions = [ 5 : ["OtRuins_405", 540, 345], 4 : ["OtRuins_405", 610, 320], 3: ["OtRuins_405", 780, 450], 2: ["OtRuins_405", 810, 390], 1: ["OtRuins_405", 640, 370] ]

//PYRE 405 TRIGGER ZONE
def pyre405Trigger = "pyre405Trigger"
myRooms.OtRuins_405.createTriggerZone( pyre405Trigger, 420, 260, 770, 530 )

//TRIGGER ZONE LOGIC
myManager.onTriggerIn( myRooms.OtRuins_405, pyre405Trigger ) { event ->
	if( gameStarted == true ) {
		//========= check for player recaptures: Enough players must be in the trigger zone or the recapture won't start ===========
		if( isPlayer( event.actor ) && event.actor.getConLevel() < 8.1 && pyre405PlayerTimerStarted == false && pyre405PlayerRetakePossible == true ) {
			pyre405PlayersInTrigger << event.actor
			//weed out the Dazed players
			if( !pyre405PlayersInTrigger.isEmpty() ) {
				pyre405PlayersInTrigger.clone().each{ 
					if( it.isDazed() || !isOnline( it ) ) { pyre405PlayersInTrigger.remove( it ) }
				}
			}

			//if enough players, then start the recapture attempt
			if( pyre405PlayersInTrigger.size() >= playerCaptureThreshold ) {
				pyre405PlayersInTrigger.clone().each{ it.centerPrint( "Stay in this area and keep the area clear of monsters to quench the Spirit Pyre flame!" ) }
				myManager.schedule(3) { pyre405PlayersInTrigger.clone().each{ it.centerPrint( "Keep it clear until the Ledge Pyre timer expires! You have one minute!" ) } }
				pyre405PlayerCapture()
			//otherwise, tell the players they need more players in the area to start the recapture attempt
			} else {
				pyre405PlayersInTrigger.clone().each{ it.centerPrint( "You need ${playerCaptureThreshold - pyre405PlayersInTrigger.size()} more players near the Spirit Pyre to start quenching its flame!" ) }
			}
		}
		//=========== check for monster recaptures (enough monsters must be in the trigger zone or it won't start) ===============
		if( isMonster( event.actor ) && pyre405MonsterTimerStarted == false && pyre405PlayerRetakePossible == false && pyre405MonsterList.contains( event.actor ) ) {
			pyre405MonsterListInTrigger << event.actor
			//weed out the dead actors from the list
			if( !pyre405MonsterListInTrigger.isEmpty() ) {
				pyre405MonsterListInTrigger.clone().each{
					if( it.isDead() ) { pyre405MonsterListInTrigger.remove( it ) }
				}
			}

			//if enough monsters, then start the capture attempt
			if( pyre405MonsterListInTrigger.size() >= monsterCaptureThreshold ) {
				//make a player list of the players in the room and broadcast a message to them
				pyre405Players.clear()
				myRooms.OtRuins_405.getActorList().each{ if( isPlayer( it ) ) { pyre405Players << it } }
				if( !pyre405Players.isEmpty() ) {
					pyre405Players.clone().each{ it.centerPrint( "Look out! Too many Terrors got close to the Spirit Pyre and they've started the rekindling ritual!" ) }
				}
				pyre405MonsterListInTrigger.clear()
				pyre405MonsterCapture()
			}
		}
	}
}

//While the retake hasn't started yet, keep weeding out players from the list so the players have to group around the Pyre to start the retake attempt.
myManager.onTriggerOut( myRooms.OtRuins_405, pyre405Trigger ) { event ->
	if( isPlayer( event.actor ) && pyre405PlayerTimerStarted == false && pyre405PlayerRetakePossible == true ) {
		pyre405PlayersInTrigger.remove( event.actor )
	}
}

//=========================================
// PLAYER RECAPTURE LOGIC                  
//=========================================
def pyre405PlayerCapture() {
	//if the timer is not started, then start it
	if( pyre405PlayerTimerStarted == false ) {
		pyre405PlayerTimerStarted = true
		pyre405PlayersInTrigger.clear() //clear out the playersInTrigger list so that the next time the situation arises, it starts with a null list.

		numMonstersIn405 = 0

		//clear the player list and start checking the number of players in the room now instead of in the trigger zone
		//this allows more freedom to the players as they fight monsters
		pyre405Players.clear()
		myRooms.OtRuins_405.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre405Players << it } }

		//The monsters don't like it when the players try to quench the flame!
		//Crank up the respawn time and start throwing baddies at them!
		pyre405SpawnOkay = true
		
		pyre405BaselineTime = fastSpawnRate
		pyre405MonsterSwarms()
		
		//myManager.schedule(30) { pyre405BaselineTime = insaneSpawnRate }

		makeZoneTimer("Ledge Pyre Quenching", "0:1:00", "0:0:0").onCompletion( { event -> 
			removeZoneTimer( "Ledge Pyre Quenching" )
			pyre405PlayerTimerStarted = false
			
			//blank out the player list and create it again, this time including all AWAKE players in the room
			pyre405Players.clear()
			myRooms.OtRuins_405.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre405Players << it } }
			
			//if enough players are there and undazed, then they quench the pyre!
			if( pyre405Players.size() >= playerCaptureThreshold ) {
				//count the number of pyre405MonsterList that are spawned and actually *in* OtRuins_405 at that time
				myRooms.OtRuins_405.getActorList().each { if( isMonster( it ) && pyre405MonsterList.contains( it ) ) { numMonstersIn405 ++ } }

				//if that number is less than the capture threshold, then the players succeed
				if( numMonstersIn405 < monsterCaptureThreshold ) {
					//Quench that fire!
					flame405.off()
					pyre405Lit = false
					sound( "pyre405Snuff" ).toZone()

					//unset the retake flag so the monsters can start trying to capture it now
					pyre405PlayerRetakePossible = false

					addMiniMapMarker("pyre405Marker", "markerPyreUnlit", "OtRuins_405", 720, 390, "Ledge Pyre")

					//announce the the quenching has occurred!
					zoneBroadcast( "Otomi Lin-VQS", "Ledge Pyre Retaken!", "Fantastic! The Ledge Pyre's flame has been quenched! Keep it that way!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Ledge Pyre Retaken!" ) }

					//Start tracking the number of seconds that the players have held the Pyre
					//This timer will never get to 30 minutes.
					//It's intentionally too long so it can be used simply as a counter for how long the players capture the Pyre.
					//NOTE: This timer is stopped when the monsters recapture the Pyre
					makeZoneTimer( "pyre405CaptureTimer", "0:0:0", "0:30:0", false ).start()

					//Delay for a bit to allow players to "succeed", but then start streaming monsters toward this Pyre to retake it
					pyre405SpawnOkay = false
					myManager.schedule(30) { //This timer is the delay until the monsters start trying to retake the Pyre
						pyre405SpawnOkay = true
						pyre405BaselineTime = normalSpawnRate
						//every 30 seconds, increase the spawn rate until the players get overwhelmed
						pyre405AcceleratedTimersStarted = true
						pyre405fasterTimer = myManager.schedule(30) { pyre405BaselineTime = fastSpawnRate }
						pyre405fastestTimer = myManager.schedule(60) { pyre405BaselineTime = insaneSpawnRate }
						pyre405MonsterSwarms()
					}
				} else {
					//if too many players have left or are dazed when the timer hits zero, then the flame is unquenched and the process has to start over again
					pyre405BaselineTime = normalSpawnRate

					zoneBroadcast( "Otomi Lin-VQS", "Ledge Pyre Still Lit!", "There were too many Terrors still at the Ledge Pyre! Defeat more on your next try!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Ledge Pyre Still Lit!" ) }
				}
			} else {
				pyre405BaselineTime = normalSpawnRate

				zoneBroadcast( "Otomi Lin-VQS", "Ledge Pyre Still Burning!", "Oh no! There were too few of you at the Ledge Pyre to quench the fire! Try again, warriors!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Ledge Pyre Still Burning!" ) }
			}
		} ).start()
	}
}

//=========================================
// MONSTER RECAPTURE LOGIC                 
//=========================================
def pyre405MonsterCapture() {
	//if the timer is not started, then start it
	if( pyre405MonsterTimerStarted == false ) {

		//if the timers from the Player capture haven't triggered yet, cancel them
		if( pyre405AcceleratedTimersStarted == true ) {
			pyre405AcceleratedTimersStarted = false
			pyre405fasterTimer.cancel()
			pyre405fastestTimer.cancel()
		}

		//Accelerate the spawning of monsters to "come help" light the pyre
		pyre405BaselineTime = fastSpawnRate
		
		pyre405MonsterTimerStarted = true

		makeZoneTimer("Ledge Pyre Threatened!", "0:1:0", "0:0:0").onCompletion( { event ->
			removeZoneTimer( "Ledge Pyre Threatened!" )
			pyre405MonsterTimerStarted = false

			pyre405BaselineTime = normalSpawnRate

			numMonstersIn405 = 0
			myRooms.OtRuins_405.getActorList().each { if( isMonster( it ) && pyre405MonsterList.contains( it ) ) { numMonstersIn405 ++ } }
			//If the monsters still exist in the trigger zone, then the fire gets LIT!
			if( numMonstersIn405 >= (monsterCaptureThreshold).intValue() ) {
				//C'mon baby, light that fire
				flame405.on()
				sound( "pyre405Ignite" ).toZone()
				pyre405Lit = true

				//IF the players have captured the pyre previously, then record how long the pyre was captured before it becomes threatened
				if( pyre405PlayerRetakePossible == false ) {
					pyre405SecondsCaptured = 1800 - getZoneTimer( "pyre405CaptureTimer" ).getRemainingSeconds()
					
					//preserve how many seconds for the reward stuff later on
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre405SecondsCaptured
					removeZoneTimer( "pyre405CaptureTimer" )
				}

				//set a flag so that a player retake is now possible
				pyre405PlayerRetakePossible = true
				
				pyre405GuardsToSpawn = 5
				pyre405ResetGuardians()
				
				//stop the extra guardian spawns after resetting the guards
				pyre405SpawnOkay = false

				addMiniMapMarker("pyre405Marker", "markerPyreLit", "OtRuins_405", 720, 390, "Ledge Pyre")


				//tell the players that the fire is lit
				zoneBroadcast( "Otomi Lin-VQS", "Oh no! Ledge Pyre Re-lit!", "The Terrors managed to re-light the Spirit Pyre on the Ledge! Put it out!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Oh no! Ledge Pyre Re-lit!" ) }


			//if all monsters have ALL been killed,the remove the timer and congratulate the players for the successful defense
			} else {
				zoneBroadcast( "Otomi Lin-VQS", "Ledge Pyre Defended!", "Congratulations! The Ledge Pyre has been successfully defended! Keep it quenched!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Ledge Pyre Defended!" ) }

			}
		} ).start()
	}
}

//=========================================
// MONSTER SWARMING LOGIC                  
//=========================================
def pyre405MonsterSwarms() {
	if( pyre405SpawnOkay == true ) {
		//count the number of players in the room
		pyre405Players.clear()
		myRooms.OtRuins_405.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre405Players << it } }

		//only spawn monsters if there are fewer than the number of players in the room
		maxMonsterSpawnSize = pyre405Players.size() + 4
		if( maxMonsterSpawnSize > 8 ) { maxMonsterSpawnSize = 8 }

		//count the number of monsters on the Pyre list that are actually in the room and spawn more if needed
		if( pyre405MonsterList.size() < maxMonsterSpawnSize ) { 
			
			pyre405FindRandomSpawnLocation()
			
			//spawn a random FC, TT, or Witch Doctor and send it toward the Pyre
			pyre405Spawner = random( pyre405SpawnerList )
			pyre405Spawner.warp( pyre405SpawnRoom, pyre405SpawnX, pyre405SpawnY )
			
			pyre405FindRandomHomeLocation()
			
			pyre405Spawner.setHomeForChildren( pyre405HomeRoom, pyre405HomeX, pyre405HomeY )

			//set a random ML
			roll = random( 10 )
			ML = baseML + ( roll * 0.1 )
			pyre405Spawner.setMonsterLevelForChildren( ML )

			pyre405Monster = pyre405Spawner.forceSpawnNow()
			//add the spawn to the list so you can keep accurate count of the number of monsters in the room
			pyre405MonsterList << pyre405Monster

			//keep track of its Hate list when it dies for use in rewards later on
			runOnDeath( pyre405Monster, { event ->
				pyre405MonsterList.remove( event.actor )
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed monsters in this Event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, terrorHate ->
					if( isPlayer( player ) ) {
						if( terrorHate > 700 ) { terrorHate = 700 }
						totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
					}
				}
			} )

		}
		//Adjust the base line timer according to the number of players within the room
		pyre405Multiplier = ( ( 1.0 - ( pyre405Players.size() * 0.1 ) ) + 0.1 ).intValue()

		//prevent the multiplier from getting too stupidly small, regardless of how many players are in the room
		if( pyre405Multiplier < 0.5 ) { pyre405Multiplier = 0.5 }
		pyre405RespawnTime = pyre405BaselineTime * pyre405Multiplier

		//create a range of respawn times so it's not so "metronome-like"
		pyre405MinRespawnTime = (pyre405RespawnTime * 0.8).intValue()
		if( pyre405MinRespawnTime < 1 ) { pyre405MinRespawnTime = 1 }

		pyre405MaxRespawnTime = (pyre405RespawnTime * 1.2).intValue()
		if( pyre405MaxRespawnTime < 1 ) { pyre405MaxRespawnTime = 1 }

		myManager.schedule( random( pyre405MinRespawnTime, pyre405MaxRespawnTime ) ) { pyre405MonsterSwarms() }
	}
}

def pyre405FindRandomSpawnLocation() {
	//figure out a random room and position
	pyre405SpawnCoords = pyre405SpawnerMap.get( random( 2 ) )
	pyre405SpawnRoom = pyre405SpawnCoords.get(0)
	pyre405SpawnX = pyre405SpawnCoords.get(1).intValue()
	pyre405SpawnY = pyre405SpawnCoords.get(2).intValue()
}

def pyre405FindRandomHomeLocation() {
	//figure out a random room and position
	pyre405HomeCoords = pyre405GuardianPositions.get( random( 5 ) )
	pyre405HomeRoom = pyre405HomeCoords.get(0)
	pyre405HomeX = pyre405HomeCoords.get(1).intValue()
	pyre405HomeY = pyre405HomeCoords.get(2).intValue()
}	

//=========================================
// PYRE GUARDIAN GROUP SPAWN LOGIC         
//=========================================
def pyre405ResetGuardians() {
	if( pyre405GuardsToSpawn > 0 ) {
		if( pyre405GuardsToSpawn > 1 ) {
			pyre405Spawner = pyre405TinyTerror
		} else {
			pyre405Spawner = pyre405TinyWitchDoctor
		}

		//figure out a random spawn room and position
		pyre405FindRandomSpawnLocation()
		pyre405Spawner.warp( pyre405SpawnRoom, pyre405SpawnX, pyre405SpawnY )
		
		//now figure out which position they should set as their Home
		pyre405GuardCoords = pyre405GuardianPositions.get( pyre405GuardsToSpawn )
		pyre405GuardRoom = pyre405GuardCoords.get( 0 )
		pyre405GuardX = pyre405GuardCoords.get(1).intValue()
		pyre405GuardY = pyre405GuardCoords.get(2).intValue()
		
		pyre405Spawner.setHomeForChildren( pyre405GuardRoom, pyre405GuardX, pyre405GuardY )

		//set a random ML
		roll = random( 10 )
		ML = baseML + ( roll * 0.1 )
		pyre405Spawner.setMonsterLevelForChildren( ML )

		pyre405Monster = pyre405Spawner.forceSpawnNow()
		pyre405MonsterList << pyre405Monster

		//keep track of its Hate list when it dies for use in rewards later on
		runOnDeath( pyre405Monster, { event ->
			pyre405MonsterList.remove( event.actor )
			event.actor.getHated().each{
				//reset the players eventHate collector if this is the first time they've killed monsters in this Event
				if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
					it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
				}
				if( isPlayer( it ) ) { masterHateCollector << it }
			}
			event.actor.getHatedMap().each() { player, terrorHate ->
				if( isPlayer( player ) ) {
					if( terrorHate > 700 ) { terrorHate = 700 }
					totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
					//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
					player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
				}
			}
		} )
		
		pyre405GuardsToSpawn --
		myManager.schedule(1) { pyre405ResetGuardians() }
	}
}

//==============================================================================================================================================
//==============================================================================================================================================
		
//==============================================================================================================================================
//PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805 PYRE805       
//==============================================================================================================================================

//Tiny Terror Spawner
pyre805TinyTerror = createStoppedEventSpawner( "OtRuins_805", "pyre805TinyTerror", "tiny_terror", 100 )
pyre805TinyTerror.setPos( 445, 125 )
pyre805TinyTerror.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre805TinyTerror.setHomeTetherForChildren( 3000 )
pyre805TinyTerror.setMiniEventSpec( savingSpiritsSpec )
pyre805TinyTerror.setMonsterLevelForChildren( ML )

//Feathered Coatl Spawner
pyre805FeatheredCoatl = createStoppedEventSpawner( "OtRuins_805", "pyre805FeatheredCoatl", "feathered_coatl", 100 )
pyre805FeatheredCoatl.setPos( 445, 125 )
pyre805FeatheredCoatl.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre805FeatheredCoatl.setHomeTetherForChildren( 3000 )
pyre805FeatheredCoatl.setMiniEventSpec( savingSpiritsSpec )
pyre805FeatheredCoatl.setMonsterLevelForChildren( ML )

//Tiny Witch Doctor Spawner
pyre805TinyWitchDoctor = createStoppedEventSpawner( "OtRuins_805", "pyre805TinyWitchDoctor", "tiny_terror_LT", 100 )
pyre805TinyWitchDoctor.setPos( 445, 125 )
pyre805TinyWitchDoctor.setWanderBehaviorForChildren( 25, 75, 2, 5, 100 )
pyre805TinyWitchDoctor.setHomeTetherForChildren( 3000 )
pyre805TinyWitchDoctor.setMiniEventSpec( savingSpiritsSpec )
pyre805TinyWitchDoctor.setMonsterLevelForChildren( ML )

//=========================================
// PYRE 805 VARIABLES & TRIGGER LOGIC      
//=========================================

pyre805MonsterList = [] as Set
pyre805MonsterListInTrigger = [] as Set
pyre805Players = [] as Set
pyre805PlayersInTrigger = [] as Set

pyre805Lit = true
pyre805MonsterTimerStarted = false
pyre805PlayerTimerStarted = false
pyre805PlayerRetakePossible = false
pyre805SpawnOkay = false
pyre805AcceleratedTimersStarted = false

pyre805SecondsCaptured = 0
pyre805RespawnTime = 20

pyre805SpawnerList = [ pyre805FeatheredCoatl, pyre805FeatheredCoatl, pyre805TinyTerror, pyre805TinyTerror, pyre805TinyTerror, pyre805TinyTerror, pyre805TinyWitchDoctor ]
pyre805SpawnerMap = [ 1: ["OtRuins_806", 100, 130 ], 2: ["OtRuins_906", 140, 320 ], 3: ["OtRuins_905", 610, 240], 4: ["OtRuins_904", 630, 410], 5: ["OtRuins_705", 900, 480] ] 
pyre805GuardianPositions = [ 5 : ["OtRuins_805", 600, 130], 4 : ["OtRuins_805", 760, 170], 3: ["OtRuins_805", 560, 260], 2: ["OtRuins_805", 780, 300], 1: ["OtRuins_805", 640, 310] ]

//PYRE 805 TRIGGER ZONE
def pyre805Trigger = "pyre805Trigger"
myRooms.OtRuins_805.createTriggerZone( pyre805Trigger, 380, 50, 920, 390 )

//TRIGGER ZONE LOGIC
myManager.onTriggerIn( myRooms.OtRuins_805, pyre805Trigger ) { event ->
	if( gameStarted == true ) {
		//========= check for player recaptures: Enough players must be in the trigger zone or the recapture won't start ===========
		if( isPlayer( event.actor ) && event.actor.getConLevel() < 8.1 && pyre805PlayerTimerStarted == false && pyre805PlayerRetakePossible == true ) {
			pyre805PlayersInTrigger << event.actor
			//weed out the Dazed players
			if( !pyre805PlayersInTrigger.isEmpty() ) {
				pyre805PlayersInTrigger.clone().each{ 
					if( it.isDazed() || !isOnline( it ) ) { pyre805PlayersInTrigger.remove( it ) }
				}
			}

			//if enough players, then start the recapture attempt
			if( pyre805PlayersInTrigger.size() >= playerCaptureThreshold ) {
				pyre805PlayersInTrigger.clone().each{ it.centerPrint( "Stay in this area and keep the area clear of monsters to quench the Spirit Pyre flame!" ) }
				myManager.schedule(3) { pyre805PlayersInTrigger.clone().each{ it.centerPrint( "Keep it clear until the Ground Pyre timer expires! You have one minute!" ) } }
				pyre805PlayerCapture()
			//otherwise, tell the players they need more players in the area to start the recapture attempt
			} else {
				pyre805PlayersInTrigger.clone().each{ it.centerPrint( "You need ${playerCaptureThreshold - pyre805PlayersInTrigger.size()} more players near the Spirit Pyre to start quenching its flame!" ) }
			}
		}
		//=========== check for monster recaptures (enough monsters must be in the trigger zone or it won't start) ===============
		if( isMonster( event.actor ) && pyre805MonsterTimerStarted == false && pyre805PlayerRetakePossible == false && pyre805MonsterList.contains( event.actor ) ) {
			pyre805MonsterListInTrigger << event.actor
			//weed out the dead actors from the list
			if( !pyre805MonsterListInTrigger.isEmpty() ) {
				pyre805MonsterListInTrigger.clone().each{
					if( it.isDead() ) { pyre805MonsterListInTrigger.remove( it ) }
				}
			}

			//if enough monsters, then start the capture attempt
			if( pyre805MonsterListInTrigger.size() >= monsterCaptureThreshold ) {
				//make a player list of the players in the room and broadcast a message to them
				pyre805Players.clear()
				myRooms.OtRuins_805.getActorList().each{ if( isPlayer( it ) ) { pyre805Players << it } }
				if( !pyre805Players.isEmpty() ) {
					pyre805Players.clone().each{ it.centerPrint( "Look out! Too many Terrors got close to the Spirit Pyre and they've started the rekindling ritual!" ) }
				}
				pyre805MonsterListInTrigger.clear()
				pyre805MonsterCapture()
			}
		}
	}
}

//While the retake hasn't started yet, keep weeding out players from the list so the players have to group around the Pyre to start the retake attempt.
myManager.onTriggerOut( myRooms.OtRuins_805, pyre805Trigger ) { event ->
	if( isPlayer( event.actor ) && pyre805PlayerTimerStarted == false && pyre805PlayerRetakePossible == true ) {
		pyre805PlayersInTrigger.remove( event.actor )
	}
}

//=========================================
// PLAYER RECAPTURE LOGIC                  
//=========================================
def pyre805PlayerCapture() {
	//if the timer is not started, then start it
	if( pyre805PlayerTimerStarted == false ) {
		pyre805PlayerTimerStarted = true
		pyre805PlayersInTrigger.clear() //clear out the playersInTrigger list so that the next time the situation arises, it starts with a null list.

		numMonstersIn805 = 0

		//clear the player list and start checking the number of players in the room now instead of in the trigger zone
		//this allows more freedom to the players as they fight monsters
		pyre805Players.clear()
		myRooms.OtRuins_805.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre805Players << it } }

		//The monsters don't like it when the players try to quench the flame!
		//Crank up the respawn time and start throwing baddies at them!
		pyre805SpawnOkay = true
		
		pyre805BaselineTime = fastSpawnRate
		pyre805MonsterSwarms()
		
		//myManager.schedule(30) { pyre805BaselineTime = insaneSpawnRate }

		makeZoneTimer("Ground Pyre Quenching", "0:1:00", "0:0:0").onCompletion( { event -> 
			removeZoneTimer( "Ground Pyre Quenching" )
			pyre805PlayerTimerStarted = false
			
			//blank out the player list and create it again, this time including all AWAKE players in the room
			pyre805Players.clear()
			myRooms.OtRuins_805.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre805Players << it } }
			
			//if enough players are there and undazed, then they quench the pyre!
			if( pyre805Players.size() >= playerCaptureThreshold ) {
				//count the number of pyre805MonsterList that are spawned and actually *in* OtRuins_805 at that time
				myRooms.OtRuins_805.getActorList().each { if( isMonster( it ) && pyre805MonsterList.contains( it ) ) { numMonstersIn805 ++ } }

				//if that number is less than the capture threshold, then the players succeed
				if( numMonstersIn805 < monsterCaptureThreshold ) {
					//Quench that fire!
					flame805.off()
					pyre805Lit = false
					sound( "pyre805Snuff" ).toZone()

					//unset the retake flag so the monsters can start trying to capture it now
					pyre805PlayerRetakePossible = false

					addMiniMapMarker("pyre805Marker", "markerPyreUnlit", "OtRuins_805", 670, 230, "Ground Pyre")

					//announce the the quenching has occurred!
					zoneBroadcast( "Otomi Lin-VQS", "Ground Pyre Retaken!", "Fantastic! The Ground Pyre's flame has been quenched! Keep it that way!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Ground Pyre Retaken!" ) }

					//Start tracking the number of seconds that the players have held the Pyre
					//This timer will never get to 30 minutes.
					//It's intentionally too long so it can be used simply as a counter for how long the players capture the Pyre.
					//NOTE: This timer is stopped when the monsters recapture the Pyre
					makeZoneTimer( "pyre805CaptureTimer", "0:0:0", "0:30:0", false ).start()

					//Delay for a bit to allow players to "succeed", but then start streaming monsters toward this Pyre to retake it
					pyre805SpawnOkay = false
					myManager.schedule(30) { //This timer is the delay until the monsters start trying to retake the Pyre
						pyre805SpawnOkay = true
						pyre805BaselineTime = normalSpawnRate
						//every 30 seconds, increase the spawn rate until the players get overwhelmed
						pyre805AcceleratedTimersStarted = true
						pyre805fasterTimer = myManager.schedule(30) { pyre805BaselineTime = fastSpawnRate }
						pyre805fastestTimer = myManager.schedule(60) { pyre805BaselineTime = insaneSpawnRate }
						pyre805MonsterSwarms()
					}
				} else {
					//if too many players have left or are dazed when the timer hits zero, then the flame is unquenched and the process has to start over again
					pyre805BaselineTime = normalSpawnRate

					zoneBroadcast( "Otomi Lin-VQS", "Ground Pyre Still Lit!", "There were too many Terrors still at the Ground Pyre! Defeat more on your next try!" )
					myManager.schedule( 10 ) { removeZoneBroadcast( "Ground Pyre Still Lit!" ) }
				}
			} else {
				pyre805BaselineTime = normalSpawnRate

				zoneBroadcast( "Otomi Lin-VQS", "Ground Pyre Still Burning!", "Oh no! There were too few of you at the Ground Pyre to quench the fire! Try again, warriors!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Ground Pyre Still Burning!" ) }
			}
		} ).start()
	}
}

//=========================================
// MONSTER RECAPTURE LOGIC                 
//=========================================
def pyre805MonsterCapture() {
	//if the timer is not started, then start it
	if( pyre805MonsterTimerStarted == false ) {

		//if the timers from the Player capture haven't triggered yet, cancel them so they don't
		if( pyre805AcceleratedTimersStarted == true ) {
			pyre805AcceleratedTimersStarted = false
			pyre805fasterTimer.cancel()
			pyre805fastestTimer.cancel()
		}

		//Accelerate the spawning of monsters to "come help" light the pyre
		pyre805BaselineTime = fastSpawnRate
		
		pyre805MonsterTimerStarted = true

		makeZoneTimer("Ground Pyre Threatened!", "0:1:0", "0:0:0").onCompletion( { event ->
			removeZoneTimer( "Ground Pyre Threatened!" )
			pyre805MonsterTimerStarted = false

			pyre805BaselineTime = normalSpawnRate
			
			numMonstersIn805 = 0
			myRooms.OtRuins_805.getActorList().each { if( isMonster( it ) && pyre805MonsterList.contains( it ) ) { numMonstersIn805 ++ } }
			//If the monsters still exist in the trigger zone, then the fire gets LIT!
			if( numMonstersIn805 >= (monsterCaptureThreshold).intValue() ) {
				//C'mon baby, light that fire
				flame805.on()
				sound( "pyre805Ignite" ).toZone()
				pyre805Lit = true

				//IF the players have captured the pyre previously, then record how long the pyre was captured before it becomes threatened
				if( pyre805PlayerRetakePossible == false ) {
					pyre805SecondsCaptured = 1800 - getZoneTimer( "pyre805CaptureTimer" ).getRemainingSeconds()
					
					//preserve how many seconds for the reward stuff later on
					totalSecondsAllPyresCaptured = totalSecondsAllPyresCaptured + pyre805SecondsCaptured
					removeZoneTimer( "pyre805CaptureTimer" )
				}

				//set a flag so that a player retake is now possible
				pyre805PlayerRetakePossible = true
				
				pyre805GuardsToSpawn = 5
				pyre805ResetGuardians()
				
				//stop the extra guardian spawns after resetting the guards
				pyre805SpawnOkay = false

				addMiniMapMarker("pyre805Marker", "markerPyreLit", "OtRuins_805", 670, 230, "Ground Pyre")


				//tell the players that the fire is lit
				zoneBroadcast( "Otomi Lin-VQS", "Oh no! Ground Pyre Re-lit!", "The Terrors managed to re-light the Spirit Pyre on the ground level! Put it out!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Oh no! Ground Pyre Re-lit!" ) }


			//if all monsters have ALL been killed,the remove the timer and congratulate the players for the successful defense
			} else {
				zoneBroadcast( "Otomi Lin-VQS", "Ground Pyre Defended!", "Congratulations! The Ground Pyre has been successfully defended! Keep it quenched!" )
				myManager.schedule( 10 ) { removeZoneBroadcast( "Ground Pyre Defended!" ) }

			}
		} ).start()
	}
}

//=========================================
// MONSTER SWARMING LOGIC                  
//=========================================
def pyre805MonsterSwarms() {
	if( pyre805SpawnOkay == true ) {
		//count the number of players in the room
		pyre805Players.clear()
		myRooms.OtRuins_805.getActorList().each{ if( isPlayer( it ) && !it.isDazed() ) { pyre805Players << it } }

		//only spawn monsters if there are fewer than the number of players in the room
		maxMonsterSpawnSize = pyre805Players.size() + 4
		if( maxMonsterSpawnSize > 8 ) { maxMonsterSpawnSize = 8 }

		//count the number of monsters on the Pyre list that are actually in the room and spawn more if needed
		if( pyre805MonsterList.size() < maxMonsterSpawnSize ) { 
			
			pyre805FindRandomSpawnLocation()
			
			//spawn a random FC, TT, or Witch Doctor and send it toward the Pyre
			pyre805Spawner = random( pyre805SpawnerList )
			pyre805Spawner.warp( pyre805SpawnRoom, pyre805SpawnX, pyre805SpawnY )
			
			pyre805FindRandomHomeLocation()
			
			pyre805Spawner.setHomeForChildren( pyre805HomeRoom, pyre805HomeX, pyre805HomeY )

			//set a random ML
			roll = random( 10 )
			ML = baseML + ( roll * 0.1 )
			pyre805Spawner.setMonsterLevelForChildren( ML )

			pyre805Monster = pyre805Spawner.forceSpawnNow()
			//add the spawn to the list so you can keep accurate count of the number of monsters in the room
			pyre805MonsterList << pyre805Monster

			//keep track of its Hate list when it dies for use in rewards later on
			runOnDeath( pyre805Monster, { event ->
				pyre805MonsterList.remove( event.actor )
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed monsters in this Event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, terrorHate ->
					if( isPlayer( player ) ) {
						if( terrorHate > 700 ) { terrorHate = 700 }
						totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
					}
				}
			} )

		}
		//Adjust the base line timer according to the number of players within the room
		pyre805Multiplier = ( ( 1.0 - ( pyre805Players.size() * 0.1 ) ) + 0.1 ).intValue()

		//prevent the multiplier from getting too stupidly small, regardless of how many players are in the room
		if( pyre805Multiplier < 0.5 ) { pyre805Multiplier = 0.5 }
		pyre805RespawnTime = pyre805BaselineTime * pyre805Multiplier

		//create a range of respawn times so it's not so "metronome-like"
		pyre805MinRespawnTime = (pyre805RespawnTime * 0.8).intValue()
		if( pyre805MinRespawnTime < 1 ) { pyre805MinRespawnTime = 1 }

		pyre805MaxRespawnTime = (pyre805RespawnTime * 1.2).intValue()
		if( pyre805MaxRespawnTime < 1 ) { pyre805MaxRespawnTime = 1 }

		myManager.schedule( random( pyre805MinRespawnTime, pyre805MaxRespawnTime ) ) { pyre805MonsterSwarms() }
	}
}

def pyre805FindRandomSpawnLocation() {
	//figure out a random room and position
	pyre805SpawnCoords = pyre805SpawnerMap.get( random( 5 ) )
	pyre805SpawnRoom = pyre805SpawnCoords.get(0)
	pyre805SpawnX = pyre805SpawnCoords.get(1).intValue()
	pyre805SpawnY = pyre805SpawnCoords.get(2).intValue()
}

def pyre805FindRandomHomeLocation() {
	//figure out a random room and position
	pyre805HomeCoords = pyre805GuardianPositions.get( random( 5 ) )
	pyre805HomeRoom = pyre805HomeCoords.get(0)
	pyre805HomeX = pyre805HomeCoords.get(1).intValue()
	pyre805HomeY = pyre805HomeCoords.get(2).intValue()
}	

//=========================================
// PYRE GUARDIAN GROUP SPAWN LOGIC         
//=========================================
def pyre805ResetGuardians() {
	if( pyre805GuardsToSpawn > 0 ) {
		if( pyre805GuardsToSpawn > 1 ) {
			pyre805Spawner = pyre805TinyTerror
		} else {
			pyre805Spawner = pyre805TinyWitchDoctor
		}
		
		//figure out a random spawn room and position
		pyre805FindRandomSpawnLocation()
		pyre805Spawner.warp( pyre805SpawnRoom, pyre805SpawnX, pyre805SpawnY )
		
		//now figure out which position they should set as their Home
		pyre805GuardCoords = pyre805GuardianPositions.get( pyre805GuardsToSpawn )
		pyre805GuardRoom = pyre805GuardCoords.get( 0 )
		pyre805GuardX = pyre805GuardCoords.get(1).intValue()
		pyre805GuardY = pyre805GuardCoords.get(2).intValue()
		
		pyre805Spawner.setHomeForChildren( pyre805GuardRoom, pyre805GuardX, pyre805GuardY )

		//set a random ML
		roll = random( 10 )
		ML = baseML + ( roll * 0.1 )
		pyre805Spawner.setMonsterLevelForChildren( ML )

		pyre805Monster = pyre805Spawner.forceSpawnNow()
		pyre805MonsterList << pyre805Monster
		
		//keep track of its Hate list when it dies for use in rewards later on
		runOnDeath( pyre805Monster, { event ->
			pyre805MonsterList.remove( event.actor )
			event.actor.getHated().each{
				//reset the players eventHate collector if this is the first time they've killed monsters in this Event
				if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
					it.setPlayerVar( "Z10SavingSpiritsCurrentHate", 0 )
				}
				if( isPlayer( it ) ) { masterHateCollector << it }
			}
			event.actor.getHatedMap().each() { player, terrorHate ->
				if( isPlayer( player ) ) {
					if( terrorHate > 700 ) { terrorHate = 700 }
					totalHateCollected = totalHateCollected + terrorHate //NOTE: This line is only for data tracking...not needed by the logic of the event
					//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
					player.addPlayerVar( "Z10SavingSpiritsCurrentHate", terrorHate )
				}
			}
		} )
		
		pyre805GuardsToSpawn --
		myManager.schedule(1) { pyre805ResetGuardians() }
	}
}

//==============================================================================================================================================
//==============================================================================================================================================
