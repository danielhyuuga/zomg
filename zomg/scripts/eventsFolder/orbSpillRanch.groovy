import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (BILL'S RANCH)                  
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 3.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 3.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "BF1_502", "orb", "orb_ranch", 100 )
orb.setPos( 600, 150 )
orb.setMonsterLevelForChildren( 2.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )

//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "Purvis-VQS", "Hey! Them Aren't Eggs!", "I thot 'Hey! Look at all them eggs! But them aren't eggs! They's ORBS! Y'all help me clean 'em up afore Pa sees 'em, okay?" )
	makeZoneTimer("Orb Spill Timer", "0:15:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "Hey! Them Aren't Eggs!" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "Purvis-VQS", "Much Gooder!", "Thanks fer pickin' all them orbs up so Pa don't get worked up about trash on th' farm. Y'all are good eggs." )
			orbDisposal()
			myManager.schedule(15) { removeZoneBroadcast( "Much Gooder!" ); eventDone() }
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.BF1_2.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_3.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_4.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_5.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_6.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_101.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_102.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_104.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_105.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_106.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_201.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_205.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_206.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_301.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_302.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_305.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_306.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_401.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_402.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_405.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_406.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_502.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_503.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_504.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_505.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_506.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_603.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_604.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_605.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_606.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_705.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_706.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_803.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_804.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_805.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BF1_806.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.BF1_2.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_3.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_4.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_5.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_6.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_101.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_102.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_104.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_105.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_106.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_201.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_205.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_206.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_301.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_302.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_305.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_306.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_401.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_402.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_404.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_405.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_406.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_502.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_503.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_504.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_505.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_506.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_603.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_604.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_605.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_606.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_705.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_706.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_803.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_804.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_805.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
	myRooms.BF1_806.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_ranch" ) { it.dispose() } }
}

bf2Map = [ 1:[480, 390, 0], 2:[980, 455, 0], 3:[195, 945, 0], 4:[150, 620, 0], 5:"BF1_2" ]
bf3Map = [ 1:[400, 255, 0], 2:[850, 300, 0], 3:[1010, 560, 0], 4:[900, 280, 0], 5:"BF1_3" ]
bf4Map = [ 1:[40, 565, 0], 2:[560, 500, 0], 3:[990, 110, 0], 4:[440, 190, 0], 5:"BF1_4" ]
bf5Map = [ 1:[230, 190, 0], 2:[900, 280, 0], 3:[40, 120, 0], 4:[580, 400, 0], 5:"BF1_5" ]
bf6Map = [ 1:[585, 300, 0], 2:[870, 585, 0], 3:[425, 450, 0], 4:[100, 270, 0], 5:"BF1_6" ]
bf101Map = [ 1:[610, 550, 0], 2:[850, 425, 0], 3:[1010, 645, 0], 4:[250, 520, 0], 5:"BF1_101" ]
bf102Map = [ 1:[625, 150, 0], 2:[330, 600, 0], 3:[180, 100, 0], 4:[40, 200, 0], 5:"BF1_102" ]
bf103Map = [ 1:[40, 445, 0], 2:[30, 210, 0], 3:[705, 200, 0], 4:[30, 230, 0], 5:"BF1_103" ]
bf104Map = [ 1:[510, 100, 0], 2:[750, 490, 0], 3:[640, 235, 0], 4:[180, 640, 0], 5:"BF1_104" ]
bf105Map = [ 1:[350, 640, 0], 2:[510, 430, 0], 3:[1000, 335, 0], 4:[600, 170, 0], 5:"BF1_105" ]
bf106Map = [ 1:[130, 505, 0], 2:[1010, 410, 0], 3:[965, 290, 0], 4:[990, 640, 0], 5:"BF1_106" ]
bf201Map = [ 1:[380, 485, 0], 2:[580, 485, 0], 3:[850, 275, 0], 4:[220, 590, 0], 5:"BF1_201" ]
bf202Map = [ 1:[900, 160, 0], 2:[400, 85, 0], 3:[150, 35, 0], 4:[780, 220, 0], 5:"BF1_202" ]
bf203Map = [ 1:[55, 90, 0], 2:[980, 480, 0], 3:[605, 475, 0], 4:[990, 610, 0], 5:"BF1_203" ]
bf204Map = [ 1:[75, 370, 0], 2:[800, 330, 0], 3:[345, 650, 0], 4:[340, 650, 0], 5:"BF1_204" ]
bf205Map = [ 1:[280, 200, 0], 2:[880, 150, 0], 3:[675, 420, 0], 4:[1010, 250, 0], 5:"BF1_205" ]
bf206Map = [ 1:[805, 530, 0], 2:[80, 140, 0], 3:[470, 30, 0], 4:[970, 630, 0], 5:"BF1_206" ]
bf301Map = [ 1:[400, 600, 0], 2:[535, 100, 0], 3:[745, 230, 0], 4:[180, 600, 0], 5:"BF1_301" ]
bf302Map = [ 1:[845, 260, 0], 2:[45, 230, 0], 3:[490, 550, 0], 4:[650, 650, 0], 5:"BF1_302" ]
bf303Map = [ 1:[70, 40, 0], 2:[560, 430, 0], 3:[980, 610, 0], 4:[320, 100, 0], 5:"BF1_303" ]
bf304Map = [ 1:[165, 150, 0], 2:[430, 240, 0], 3:[150, 340, 0], 4:[710, 460, 0], 5:"BF1_304" ]
bf305Map = [ 1:[900, 635, 0], 2:[35, 290, 0], 3:[500, 40, 0], 4:[240, 40, 0], 5:"BF1_305" ]
bf306Map = [ 1:[990, 550, 0], 2:[685, 630, 0], 3:[180, 485, 0], 4:[30, 600, 0], 5:"BF1_306" ]
bf401Map = [ 1:[120, 130, 0], 2:[890, 250, 0], 3:[400, 25, 0], 4:[1010, 160, 0], 5:"BF1_401" ]
bf402Map = [ 1:[50, 200, 0], 2:[270, 525, 0], 3:[685, 275, 0], 4:[950, 580, 0], 5:"BF1_402" ]
bf404Map = [ 1:[40, 540, 0], 2:[300, 215, 0], 3:[715, 165, 0], 4:[1000, 470, 0], 5:"BF1_404" ]
bf405Map = [ 1:[555, 640, 0], 2:[995, 320, 0], 3:[715, 125, 0], 4:[110, 110, 0], 5:"BF1_405" ]
bf406Map = [ 1:[190, 220, 0], 2:[590, 170, 0], 3:[985, 275, 0], 4:[50, 420, 0], 5:"BF1_406" ]
bf502Map = [ 1:[350, 125, 0], 2:[1010, 370, 0], 3:[715, 50, 0], 4:[750, 540, 0], 5:"BF1_502" ]
bf503Map = [ 1:[1020, 25, 0], 2:[520, 635, 0], 3:[30, 390, 0], 4:[800, 30, 0], 5:"BF1_503" ]
bf504Map = [ 1:[845, 445, 0], 2:[450, 455, 0], 3:[95, 515, 0], 4:[600, 30, 0], 5:"BF1_504" ]
bf505Map = [ 1:[630, 625, 0], 2:[415, 225, 0], 3:[205, 275, 0], 4:[750, 650, 0], 5:"BF1_505" ]
bf506Map = [ 1:[160, 555, 0], 2:[985, 510, 0], 3:[500, 260, 0], 4:[310, 640, 0], 5:"BF1_506" ]
bf603Map = [ 1:[515, 45, 0], 2:[760, 100, 0], 3:[1010, 490, 0], 4:[430, 480, 0], 5:"BF1_603" ]
bf604Map = [ 1:[125, 220, 0], 2:[560, 270, 0], 3:[1000, 445, 0], 4:[270, 250, 0], 5:"BF1_604" ]
bf605Map = [ 1:[50, 425, 0], 2:[390, 610, 0], 3:[750, 100, 0], 4:[980, 520, 0], 5:"BF1_605" ]
bf606Map = [ 1:[850, 540, 0], 2:[330, 245, 0], 3:[160, 40, 0], 4:[1000, 290, 0], 5:"BF1_606" ]
bf705Map = [ 1:[50, 575, 0], 2:[620, 170, 0], 3:[990, 570, 0], 4:[370, 50, 0], 5:"BF1_705" ]
bf706Map = [ 1:[490, 310, 0], 2:[930, 90, 0], 3:[180, 350, 0], 4:[740, 530, 0], 5:"BF1_706" ]
bf803Map = [ 1:[300, 570, 0], 2:[800, 110, 0], 3:[980, 560, 0], 4:[600, 600, 0], 5:"BF1_803" ]
bf804Map = [ 1:[190, 170, 0], 2:[570, 220, 0], 3:[960, 510, 0], 4:[760, 290, 0], 5:"BF1_804" ]
bf805Map = [ 1:[40, 210, 0], 2:[600, 110, 0], 3:[930, 100, 0], 4:[310, 150, 0], 5:"BF1_805" ]
bf806Map = [ 1:[90, 320, 0], 2:[340, 360, 0], 3:[950, 270, 0], 4:[650, 500, 0], 5:"BF1_806" ]

roomList = [ bf2Map, bf3Map, bf4Map, bf5Map, bf6Map, bf101Map, bf102Map, bf103Map, bf104Map, bf105Map, bf106Map, bf201Map, bf202Map, bf203Map, bf204Map, bf205Map, bf206Map, bf301Map, bf302Map, bf303Map, bf304Map, bf305Map, bf306Map, bf401Map, bf402Map, bf404Map, bf405Map, bf406Map, bf502Map, bf503Map, bf504Map, bf505Map, bf506Map, bf603Map, bf604Map, bf605Map, bf606Map, bf705Map, bf706Map, bf803Map, bf804Map, bf805Map, bf806Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.BF1_2.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_3.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_4.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_5.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_6.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_101.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_102.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_103.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_104.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_105.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_106.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_201.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_202.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_203.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_204.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_205.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_206.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_301.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_302.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_303.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_304.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_305.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_306.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_401.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_402.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_404.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_405.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_406.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_502.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_503.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_504.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_505.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_506.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_603.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_604.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_605.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_606.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_705.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_706.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_803.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_804.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_805.getSpawnTypeCount( "orb_ranch" )
	orbCount = orbCount + myRooms.BF1_806.getSpawnTypeCount( "orb_ranch" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 20 ) {
		maxOrbs = 20
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 5 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb, { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			} )
		}
		//now spawn another orb a quarter-second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


