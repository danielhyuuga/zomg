//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//-----------------------------------------------------------------------------------
//Patrol paths                                                                       
//-----------------------------------------------------------------------------------
patrol1 = makeNewPatrol()
patrol1.addPatrolPoint("BF1_402", 480, 240, 0)
patrol1.addPatrolPoint("BF1_302", 560, 410, 0)
patrol1.addPatrolPoint("BF1_301", 830, 330, 0)
patrol1.addPatrolPoint("BF1_201", 520, 570, 0)
patrol1.addPatrolPoint("BF1_202", 170, 200, 0)
patrol1.addPatrolPoint("BF1_102", 200, 300, 0)
patrol1.addPatrolPoint("BF1_103", 100, 100, 0)
patrol1.addPatrolPoint("BF1_3", 300, 400, 0)
patrol1.addPatrolPoint("BF1_4", 320, 520, 0)
patrol1.addPatrolPoint("BF1_104", 80, 570, 0)
patrol1.addPatrolPoint("BF1_103", 400, 630, 0)
patrol1.addPatrolPoint("BF1_203", 70, 510, 0)
patrol1.addPatrolPoint("BF1_303", 550, 280, 0)
patrol1.addPatrolPoint("BF1_302", 800, 600, 0)

patrol2 = makeNewPatrol()
patrol2.addPatrolPoint("BF1_404", 250, 430, 0)
patrol2.addPatrolPoint("BF1_403", 890, 110, 0)
patrol2.addPatrolPoint("BF1_303", 460, 580, 0)
patrol2.addPatrolPoint("BF1_304", 90, 90, 0)
patrol2.addPatrolPoint("BF1_203", 950, 550, 0)
patrol2.addPatrolPoint("BF1_103", 310, 610, 0)
patrol2.addPatrolPoint("BF1_104", 220, 140, 0)
patrol2.addPatrolPoint("BF1_204", 60, 180, 0)
patrol2.addPatrolPoint("BF1_203", 840, 500, 0)
patrol2.addPatrolPoint("BF1_303", 200, 500, 0)
patrol2.addPatrolPoint("BF1_304", 140, 640, 0)

patrol3 = makeNewPatrol()
patrol3.addPatrolPoint("BF1_204", 80, 240, 0)
patrol3.addPatrolPoint("BF1_203", 910, 80, 0)
patrol3.addPatrolPoint("BF1_303", 590, 180, 0)
patrol3.addPatrolPoint("BF1_302", 760, 500, 0)
patrol3.addPatrolPoint("BF1_301", 890, 600, 0)
patrol3.addPatrolPoint("BF1_302", 300, 170, 0)
patrol3.addPatrolPoint("BF1_202", 530, 340, 0)
patrol3.addPatrolPoint("BF1_203", 320, 510, 0)

patrol4 = makeNewPatrol()
patrol4.addPatrolPoint("BF1_103", 410, 410, 0)
patrol4.addPatrolPoint("BF1_104", 300, 110, 0)
patrol4.addPatrolPoint("BF1_4", 530, 600, 0)
patrol4.addPatrolPoint("BF1_3", 660, 310, 0)
patrol4.addPatrolPoint("BF1_103", 250, 40, 0)
patrol4.addPatrolPoint("BF1_102", 250, 100, 0)
patrol4.addPatrolPoint("BF1_101", 680, 620, 0)
patrol4.addPatrolPoint("BF1_201", 480, 620, 0)
patrol4.addPatrolPoint("BF1_202", 580, 320, 0)
patrol4.addPatrolPoint("BF1_302", 760, 250, 0)
patrol4.addPatrolPoint("BF1_301", 950, 110, 0)
patrol4.addPatrolPoint("BF1_201", 960, 420, 0)
patrol4.addPatrolPoint("BF1_202", 290, 170, 0)
patrol4.addPatrolPoint("BF1_102", 140, 350, 0)
patrol4.addPatrolPoint("BF1_2", 960, 620, 0)
patrol4.addPatrolPoint("BF1_3", 580, 570, 0)
patrol4.addPatrolPoint("BF1_4", 220, 300, 0)
patrol4.addPatrolPoint("BF1_104", 70, 600, 0)

patrol5 = makeNewPatrol()
patrol5.addPatrolPoint("BF1_102", 680, 190, 0)
patrol5.addPatrolPoint("BF1_2", 350, 430, 0)
patrol5.addPatrolPoint("BF1_102", 60, 470, 0)
patrol5.addPatrolPoint("BF1_101", 640, 620, 0)
patrol5.addPatrolPoint("BF1_201", 960, 390, 0)
patrol5.addPatrolPoint("BF1_202", 450, 590, 0)
patrol5.addPatrolPoint("BF1_302", 810, 530, 0)
patrol5.addPatrolPoint("BF1_301", 780, 200, 0)
patrol5.addPatrolPoint("BF1_201", 500, 330, 0)
patrol5.addPatrolPoint("BF1_101", 870, 490, 0)

patrol6 = makeNewPatrol()
patrol6.addPatrolPoint("BF1_401", 980, 200, 0)
patrol6.addPatrolPoint("BF1_301", 710, 560, 0)
patrol6.addPatrolPoint("BF1_302", 640, 560, 0)
patrol6.addPatrolPoint("BF1_402", 970, 310, 0)
patrol6.addPatrolPoint("BF1_403", 560, 230, 0)
patrol6.addPatrolPoint("BF1_303", 840, 480, 0)
patrol6.addPatrolPoint("BF1_302", 590, 210, 0)
patrol6.addPatrolPoint("BF1_402", 180, 160, 0)

patrol7 = makeNewPatrol()
patrol7.addPatrolPoint("BF1_403", 945, 555, 0)
patrol7.addPatrolPoint("BF1_303", 390, 500, 0)
patrol7.addPatrolPoint("BF1_203", 960, 500, 0)
patrol7.addPatrolPoint("BF1_103", 330, 630, 0)
patrol7.addPatrolPoint("BF1_203", 180, 280, 0)
patrol7.addPatrolPoint("BF1_202", 560, 570, 0)
patrol7.addPatrolPoint("BF1_302", 180, 300, 0)
patrol7.addPatrolPoint("BF1_303", 100, 530, 0)

patrol8 = makeNewPatrol()
patrol8.addPatrolPoint("BF1_202", 790, 250, 0)
patrol8.addPatrolPoint("BF1_203", 950, 230, 0)
patrol8.addPatrolPoint("BF1_204", 70, 220, 0)
patrol8.addPatrolPoint("BF1_203", 790, 610, 0)
patrol8.addPatrolPoint("BF1_303", 300, 470, 0)
patrol8.addPatrolPoint("BF1_403", 70, 160, 0)
patrol8.addPatrolPoint("BF1_304", 300, 590, 0)
patrol8.addPatrolPoint("BF1_303", 140, 380, 0)
patrol8.addPatrolPoint("BF1_302", 190, 90, 0)

patrol9 = makeNewPatrol()
patrol9.addPatrolPoint("BF1_302", 800, 600, 0)
patrol9.addPatrolPoint("BF1_303", 550, 280, 0)
patrol9.addPatrolPoint("BF1_203", 70, 510, 0)
patrol9.addPatrolPoint("BF1_103", 400, 630, 0)
patrol9.addPatrolPoint("BF1_104", 80, 570, 0)
patrol9.addPatrolPoint("BF1_4", 320, 520, 0)
patrol9.addPatrolPoint("BF1_3", 300, 400, 0)
patrol9.addPatrolPoint("BF1_103", 100, 100, 0)
patrol9.addPatrolPoint("BF1_102", 200, 300, 0)
patrol9.addPatrolPoint("BF1_202", 170, 200, 0)
patrol9.addPatrolPoint("BF1_201", 520, 570, 0)
patrol9.addPatrolPoint("BF1_301", 830, 330, 0)
patrol9.addPatrolPoint("BF1_302", 560, 410, 0)
patrol9.addPatrolPoint("BF1_402", 480, 240, 0)

patrol10 = makeNewPatrol()
patrol10.addPatrolPoint("BF1_304", 140, 640, 0)
patrol10.addPatrolPoint("BF1_303", 200, 500, 0)
patrol10.addPatrolPoint("BF1_203", 840, 500, 0)
patrol10.addPatrolPoint("BF1_204", 60, 180, 0)
patrol10.addPatrolPoint("BF1_104", 220, 140, 0)
patrol10.addPatrolPoint("BF1_103", 310, 610, 0)
patrol10.addPatrolPoint("BF1_203", 950, 550, 0)
patrol10.addPatrolPoint("BF1_304", 90, 90, 0)
patrol10.addPatrolPoint("BF1_303", 460, 580, 0)
patrol10.addPatrolPoint("BF1_403", 890, 110, 0)
patrol10.addPatrolPoint("BF1_404", 250, 430, 0)

patrol11 = makeNewPatrol()
patrol11.addPatrolPoint("BF1_203", 320, 510, 0)
patrol11.addPatrolPoint("BF1_202", 530, 340, 0)
patrol11.addPatrolPoint("BF1_302", 300, 170, 0)
patrol11.addPatrolPoint("BF1_301", 890, 600, 0)
patrol11.addPatrolPoint("BF1_302", 760, 500, 0)
patrol11.addPatrolPoint("BF1_303", 590, 180, 0)
patrol11.addPatrolPoint("BF1_203", 910, 80, 0)
patrol11.addPatrolPoint("BF1_204", 80, 240, 0)

patrol12 = makeNewPatrol()
patrol12.addPatrolPoint("BF1_104", 70, 600, 0)
patrol12.addPatrolPoint("BF1_4", 220, 300, 0)
patrol12.addPatrolPoint("BF1_3", 580, 570, 0)
patrol12.addPatrolPoint("BF1_2", 960, 620, 0)
patrol12.addPatrolPoint("BF1_102", 140, 350, 0)
patrol12.addPatrolPoint("BF1_202", 290, 170, 0)
patrol12.addPatrolPoint("BF1_201", 960, 420, 0)
patrol12.addPatrolPoint("BF1_301", 950, 110, 0)
patrol12.addPatrolPoint("BF1_302", 760, 250, 0)
patrol12.addPatrolPoint("BF1_202", 580, 320, 0)
patrol12.addPatrolPoint("BF1_201", 480, 620, 0)
patrol12.addPatrolPoint("BF1_101", 680, 620, 0)
patrol12.addPatrolPoint("BF1_102", 250, 100, 0)
patrol12.addPatrolPoint("BF1_103", 250, 40, 0)
patrol12.addPatrolPoint("BF1_3", 660, 310, 0)
patrol12.addPatrolPoint("BF1_4", 530, 600, 0)
patrol12.addPatrolPoint("BF1_104", 300, 110, 0)
patrol12.addPatrolPoint("BF1_103", 410, 410, 0)

patrol13 = makeNewPatrol()
patrol13.addPatrolPoint("BF1_101", 870, 490, 0)
patrol13.addPatrolPoint("BF1_201", 500, 330, 0)
patrol13.addPatrolPoint("BF1_301", 780, 200, 0)
patrol13.addPatrolPoint("BF1_302", 810, 530, 0)
patrol13.addPatrolPoint("BF1_202", 450, 590, 0)
patrol13.addPatrolPoint("BF1_201", 960, 390, 0)
patrol13.addPatrolPoint("BF1_101", 640, 620, 0)
patrol13.addPatrolPoint("BF1_102", 60, 470, 0)
patrol13.addPatrolPoint("BF1_2", 350, 430, 0)
patrol13.addPatrolPoint("BF1_102", 680, 190, 0)

patrol14 = makeNewPatrol()
patrol14.addPatrolPoint("BF1_402", 180, 160, 0)
patrol14.addPatrolPoint("BF1_302", 590, 210, 0)
patrol14.addPatrolPoint("BF1_303", 840, 480, 0)
patrol14.addPatrolPoint("BF1_403", 560, 230, 0)
patrol14.addPatrolPoint("BF1_402", 970, 310, 0)
patrol14.addPatrolPoint("BF1_302", 640, 560, 0)
patrol14.addPatrolPoint("BF1_301", 710, 560, 0)
patrol14.addPatrolPoint("BF1_401", 980, 200, 0)

patrol15 = makeNewPatrol()
patrol15.addPatrolPoint("BF1_303", 100, 530, 0)
patrol15.addPatrolPoint("BF1_302", 180, 300, 0)
patrol15.addPatrolPoint("BF1_202", 560, 570, 0)
patrol15.addPatrolPoint("BF1_203", 180, 280, 0)
patrol15.addPatrolPoint("BF1_103", 330, 630, 0)
patrol15.addPatrolPoint("BF1_203", 960, 500, 0)
patrol15.addPatrolPoint("BF1_303", 390, 500, 0)
patrol15.addPatrolPoint("BF1_403", 945, 555, 0)

patrol16 = makeNewPatrol()
patrol16.addPatrolPoint("BF1_302", 190, 90, 0)
patrol16.addPatrolPoint("BF1_303", 140, 380, 0)
patrol16.addPatrolPoint("BF1_304", 300, 590, 0)
patrol16.addPatrolPoint("BF1_403", 70, 160, 0)
patrol16.addPatrolPoint("BF1_303", 300, 470, 0)
patrol16.addPatrolPoint("BF1_203", 790, 610, 0)
patrol16.addPatrolPoint("BF1_204", 70, 220, 0)
patrol16.addPatrolPoint("BF1_203", 950, 230, 0)
patrol16.addPatrolPoint("BF1_202", 790, 250, 0)

//-----------------------------------------------------------------------------------
//Maps, Lists, and Variables                                                         
//-----------------------------------------------------------------------------------
patrol1Location = [myRooms.BF1_402, 480, 240, patrol1]
patrol2Location = [myRooms.BF1_404, 250, 430, patrol2]
patrol3Location = [myRooms.BF1_204, 80, 240, patrol3]
patrol4Location = [myRooms.BF1_103, 410, 410, patrol4]
patrol5Location = [myRooms.BF1_102, 680, 190, patrol5]
patrol6Location = [myRooms.BF1_401, 980, 200, patrol6]
patrol7Location = [myRooms.BF1_403, 945, 555, patrol7]
patrol8Location = [myRooms.BF1_202, 790, 250, patrol8]
patrol9Location = [myRooms.BF1_302, 800, 600, patrol9]
patrol10Location = [myRooms.BF1_304, 140, 640, patrol10]
patrol11Location = [myRooms.BF1_203, 320, 510, patrol11]
patrol12Location = [myRooms.BF1_104, 70, 600, patrol12]
patrol13Location = [myRooms.BF1_101, 870, 490, patrol13]
patrol14Location = [myRooms.BF1_402, 180, 160, patrol14]
patrol15Location = [myRooms.BF1_303, 100, 530, patrol15]
patrol16Location = [myRooms.BF1_302, 190, 90, patrol16]

spawnLocations = [patrol1Location, patrol2Location, patrol3Location, patrol4Location, patrol5Location, patrol6Location, patrol7Location, patrol8Location, patrol9Location, patrol10Location, patrol11Location, patrol12Location, patrol13Location, patrol14Location, patrol15Location, patrol16Location, patrol1Location, patrol2Location, patrol3Location, patrol4Location, patrol5Location, patrol6Location, patrol7Location, patrol8Location, patrol9Location, patrol10Location, patrol11Location, patrol12Location, patrol13Location, patrol14Location, patrol15Location, patrol16Location]
spawnLocationMap = new HashMap()
carriedMap = new HashMap()
gruckenList = []
gruckenGrabberList = []
playerList = []

spawnTime = true
numberOfPlayers = 0
chickensDropped = 0

//-----------------------------------------------------------------------------------
//Chicken well logic                                                         
//-----------------------------------------------------------------------------------
gruckenHole = makeSwitch("waterWell", myRooms.BF1_304, 250, 420)
gruckenHole.setMouseoverText("Grucken Hole")
gruckenHole.setUsable(false)
gruckenHole.off()
gruckenHole.setRange(250)

def gruckenPlunge = { event ->
	if(carriedMap.containsKey(event.actor)) {
		carriedMap?.get(event.actor).unLatch()
		//carriedMap?.get(event.actor)?.dispose()
		//gruckenList.remove(carriedMap?.get(event.actor))
		//carriedMap.remove(event.actor)
		
		event.actor.centerPrint("You drop the Grucken in the well.")
		sound("chickenWell").toPlayer(event.actor)
		
		zoneCounter = getZoneCounter("Gruckens Grabbed")
		if(zoneCounter) { 
			zoneCounter.getPlayerCounter(event.actor).increment()
		}
		if(!gruckenGrabberList.contains(event.actor)) { gruckenGrabberList << event.actor; event.actor.setPlayerVar("Z03_GruckensCaught", 0) }
		if(event.actor.getPlayerVar("Z03_GruckensCaught") == null && event.actor.getPlayerVar("Z03_GruckensCaughtTotal") == null) { //if player doesn't have kill trackers, set them
			event.actor.setPlayerVar("Z03_GruckensCaught", 1)
			event.actor.setPlayerVar("Z03_GruckensCaughtTotal", 1)
		} else { //if player has kill trackers, increment them
			event.actor.addToPlayerVar("Z03_GruckensCaught", 1)
			event.actor.addToPlayerVar("Z03_GruckensCaughtTotal", 1)
		}
	}
}

gruckenHole.whenOn(gruckenPlunge)

//-----------------------------------------------------------------------------------
//Event logic                                                                        
//-----------------------------------------------------------------------------------
def startEvent() {
	println "#### Starting Grucken Grab ####"
	sound("gruckenEventStart").toZone()
	myManager.schedule(10.9) { stopSound( "gruckenEventStart" ).toZone() } //TODO (remove me!) GARRETT...this command is just to stop the chicken sounds because I screwed up the mapSound so it plays an infinite amount of times. I'll fix that for next build. Meanwhile, this command prevents you from going insane while running the event.
	myManager.schedule(2) {
		gruckenHole.setUsable(true)
		makeZoneCounter("Gruckens Grabbed", 0, true)
		zoneBroadcast("Purvis-VQS", "Grab That Grucken Thing!", "Buh-dur, we got some weird lookin' chickens runnin' 'round the field. Pa wants them gone. Grabem and stuffem down the well like muh feelins fer Rubella!")
		addMiniMapMarker("gruckenHoleMarker", "markerChickenWell", "BF1_304", 250, 420, "Grucken Hole")
		myManager.schedule(30) { 
			spawnCheck()
			makeZoneTimer("Grab That Grucken Thing!", "0:20:0", "0:0:0").onCompletion({ event -> 
				spawnTime = false
				removeZoneTimer("Grab That Grucken Thing!")
				removeZoneCounter("Gruckens Grabbed")
				removeZoneBroadcast("Grab That Grucken Thing!")
				zoneBroadcast("Rancher Bill-VQS", "Grucken Glory", "Durn-it, we showed them Gruckens! They're all gone. Thanks, everyone!")
				myManager.schedule(15) { removeZoneBroadcast("Grucken Glory")}
				completeEvent()
			}).start() 
		}
	}
}

def completeEvent() {
	myManager.schedule(1) {
		//println "#### gruckenList - ${gruckenList} ####"
		gruckenList.clone().each() {
			//println "##### disposing ${it} ####"
			if(it?.isLatched()) { it.unLatch() }
			it?.dispose()
		}
		removeMiniMapMarker("gruckenHoleMarker")
		gruckenHole.setUsable(false)
		if(gruckenGrabberList.size() > 0) {
			println "#*#*#* BEGIN GRUCKEN PRINTOUT *#*#*#"
			println "#*#*#* gruckenGrabberList = ${gruckenGrabberList} *#*#*#"
			gruckenGrabberList.clone().each() {
				if(isPlayer(it)) {
					if(it != null && isOnline(it) && !isInOtherLayer(it)) {
						numberOfPlayers++
						chickensDropped = chickensDropped + it.getPlayerVar("Z03_GruckensCaught").toInteger()
						println "#*#*#* PlayerID = ${it} *#*#*#"
						println "#*#*#* Gruckens caught this event = ${it.getPlayerVar("Z03_GruckensCaught")} *#*#*#"
						println "#*#*#* Gruckens caught all time = ${it.getPlayerVar("Z03_GruckensCaughtTotal")} *#*#*#"
						if(it.getPlayerVar("Z03_GruckensCaught") > 0 && it.getPlayerVar("Z03_GruckensCaught") < 10) { 
							tinyReward = random(1, 2)
							goldReward = random(5, 10)
							it.centerPrint("You tossed ${it.getPlayerVar("Z03_GruckensCaught").toInteger()} Gruckens and earned a tiny reward.")
							it.centerPrint("You receive ${goldReward} gold and ${tinyReward} orbs.")
							it.grantQuantityItem(100257, tinyReward)
							it.grantCoins(goldReward)
							lootChance = random(100)
							/*if(lootChance > 99) {
								it.grantItem("100440")
								it.centerPrint("You have received a Grucken Feather as an additional reward!")
							}*/
						}
						if(it.getPlayerVar("Z03_GruckensCaught") >= 10 && it.getPlayerVar("Z03_GruckensCaught") < 35) {
							moderateReward = random(2, 3)
							goldReward = random(10, 15)
							it.centerPrint("You tossed ${it.getPlayerVar("Z03_GruckensCaught").toInteger()} Gruckens and earned a moderate reward.")
							it.centerPrint("You receive ${goldReward} gold and ${moderateReward} orbs.")
							it.grantQuantityItem(100257, moderateReward)
							it.grantCoins(goldReward)
							lootChance = random(100)
							/*if(lootChance > 80) {
								it.grantItem("100440")
								it.centerPrint("You have received a Grucken Feather as an additional reward!")
							}*/
						}
						if(it.getPlayerVar("Z03_GruckensCaught") >= 35 && it.getPlayerVar("Z03_GruckensCaught") < 60) {
							largeReward = random(4, 6)
							goldReward = random(25, 35)
							it.centerPrint("You tossed ${it.getPlayerVar("Z03_GruckensCaught").toInteger()} Gruckens and earned a large reward.")
							it.centerPrint("You receive ${goldReward} gold and ${largeReward} orbs.")
							it.grantQuantityItem(100257, largeReward)
							it.grantCoins(goldReward)
							lootChance = random(100)
							/*if(lootChance > 65) {
								it.grantItem("100440")
								it.centerPrint("You have received a Grucken Feather as an additional reward!")
							}*/
						}
						if(it.getPlayerVar("Z03_GruckensCaught") >= 60) {
							massiveReward = random(8, 10)
							goldReward = random(50, 60)
							it.centerPrint("You tossed ${it.getPlayerVar("Z03_GruckensCaught").toInteger()} Gruckens and earned a massive reward.")
							it.centerPrint("You receive ${goldReward} gold and ${massiveReward} orbs.")
							it.grantQuantityItem(100257, massiveReward)
							it.grantCoins(goldReward)
							lootChance = random(100)
							/*if(lootChance > 0) {
								it.grantItem("100440")
								it.centerPrint("You have received a Grucken Feather as an additional reward!")
							}*/
						}
						if(it.getPlayerVar("Z03_GruckensCaughtTotal") >= 100 && !it.isDoneQuest(326)) {
							it.centerPrint("You have been awarded the Stop That Grucken Around badge.")
							it.updateQuest(326, "Purvis-VQS")
						}
						if(it.getPlayerVar("Z03_GruckensCaughtTotal") >= 1000 && !it.isDoneQuest(327)) {
							it.centerPrint("You have been awarded the That's Grucken Amazing! badge.")
							it.updateQuest(327, "Purvis-VQS")
						}
					}
				}
			}
			if(numberOfPlayers > 0) { println "*#*#*# Average per player = ${chickensDropped / numberOfPlayers} *#*#*#" }
			println "*#*#*# END GRUCKEN PRINTOUT *#*#*#"
		}
		myManager.schedule(15) { eventDone() }
	}
}

def spawnCheck() {
	time = gst()
	if(spawnTime == true) { //If spawn is allowed, and there are Gruckens to spawn, spawn one
		if(spawnLocations.size() > 0) {
			spawnRandom()
		} else {
			myManager.schedule(30) { spawnCheck() } //If no Grucken spawns are available, schedule a check again in 30 seconds
		}
	} 
}

def spawnRandom() {
	//println "#### spawnLocations = ${spawnLocations} ####"
	myManager.schedule(5) { spawnCheck() }
	//Location selection
	spawnLocation = random(spawnLocations)
	spawnLocations.remove(spawnLocation)
	
	//println "#### spawnLocation = ${spawnLocation} ####"
	
	//Grab Room and X, Y from map
	spawnRoom = spawnLocation?.get(0)
	spawnX = spawnLocation?.get(1)
	spawnY = spawnLocation?.get(2)
	
	if(spawnRoom != null) { //Safety net to keep from spawning when no locations available
		//Spawn a Grucken, name it, and start it on a patrol
		//println "#### Making Grucken at ${spawnRoom}, ${spawnX}, ${spawnY} ####"
		grucken = makeCritter("gruncken", spawnRoom, spawnX, spawnY)
		gruckenList << grucken
		//println "#### gruckenList = ${gruckenList} ####"
		grucken.setDisplayName("Grucken")
		grucken.setBaseSpeed(300)
	
		grucken.setPatrol(spawnLocation?.get(3))
		grucken.startPatrol()
	
		spawnLocationMap.put(grucken, spawnLocation)
	
		//Critter logic
		grucken.setUsable(true)
		grucken.onUse { event ->
			if(whoIsLatchedTo(event.player).size() > 0) {
				whoIsLatchedTo(event.player).each{
					it.unLatch()
					println "#### ${event.player} dropped ${it} ####"
				}
			}
			//println "##### event.actor = ${event.actor} event.player = ${event.player} event.critter = ${event.critter} ####"
			if(!event.critter.isLatched() && !carriedMap.containsKey(event.player)) {
				event.critter.latchOnTo(event.player)
				carriedMap.put(event.player, event.critter)
				event.player.centerPrint("You grabbed a Grucken! Go drop it in the well!")
				//println "#### Adding ${spawnLocationMap?.get(event.critter)} ####"
				spawnLocations << spawnLocationMap?.get(event.critter)
				spawnLocationMap.remove(event.critter)
			
				myManager.schedule(120) {
					if(event.critter.isLatched()) {
						event.critter.unLatch()
						//event.critter.dispose()
						//carriedMap.remove(event.player)
					}
				}
			}
			/*else if(event.critter.isLatched() && event.critter.getLatchedTo() == event.player) {
				event.critter.unLatch()
			}*/
			event.critter.onDrop { dropEvent ->
				if(dropEvent.reason == "dazed" || dropEvent.reason == "zoned" || dropEvent.reason == "script") {
					dropEvent.actor.startWander(true)
					gruckenList.remove(dropEvent.actor)
					carriedMap.remove(dropEvent.dropper)
					dropEvent.actor?.dispose()	
				}
			}
		}
	}
	//println "#### Made ${grucken} at ${spawnRoom}, ${spawnX}, ${spawnY} ####"
}

startEvent()
