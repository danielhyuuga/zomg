import com.gaiaonline.mmo.battle.script.*;

//==============================================
// STAR PORTAL SWITCH                           
//==============================================
portal = makeSwitch( "starPortal", myRooms.Aqueduct_306, 420, 420 )

portal.lock()
portal.off()
portal.setRange( 200 )
portal.setUsable( false )

//--------------------------------------------
// TRIGGER ZONE FOR BUG BREAKOUT              
//--------------------------------------------
disposalOkay = false

def bugWarpOut = "bugWarpOut"
myRooms.Aqueduct_306.createTriggerZone( bugWarpOut, 340, 360, 500, 460 )

myManager.onTriggerIn(myRooms.Aqueduct_306, bugWarpOut) { event ->
	//Warp the bugs out when they are recalled and return
	if( isMonster( event.actor ) && disposalOkay == true ) { 
		sound( "portalTravel" ).toZone()
		event.actor.dispose()
	}
}


//============================================
// SPAWNERS                                   
//============================================

bugML = 5.5

//Portal Buster Bomb is invulnerable
def busterBombSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() > 20.0
		if( player == attacker && !allowed ) { player.centerPrint( "The energy shield around the bomb shrugs off your attack." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//Portal Buster Bomb
busterBombSpawner = createStoppedEventSpawner( "Aqueduct_306", "busterBombSpawner", "egg1", 1)
busterBombSpawner.setPos( 420, 420 )
busterBombSpawner.setMonsterLevelForChildren( 20.0 )
busterBombSpawner.setMiniEventSpec( busterBombSpec )

//Make bug patrols with CL max limit
def bugBreakoutSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() < 6.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 6.0 or lower to fight against the Bug Breakout." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//Death Roach Spawner
deathRoachSpawner = createStoppedEventSpawner( "Aqueduct_306", "deathRoachSpawner", "alien_death_bug", 50)
deathRoachSpawner.setPos( 420, 420 )
deathRoachSpawner.setMonsterLevelForChildren( bugML )
deathRoachSpawner.setMiniEventSpec( bugBreakoutSpec )

//Weak Patrol
walker1 = createStoppedEventSpawner( "Aqueduct_306", "walker1", "alien_walker", 6)
walker1.setPos( 420, 420 )
walker1.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
walker1.setMonsterLevelForChildren( bugML )
walker1.feudWithMonsterType( "p3_war" )
walker1.feudWithMonsterType( "p3" )
walker1.feudWithMonsterType( "p3_LT" )
walker1.feudWithMonsterType( "p3_demi" )
walker1.setMiniEventSpec( bugBreakoutSpec )

roach1 = createStoppedEventSpawner( "Aqueduct_306", "roach1", "alien_bug", 30)
roach1.setPos( 420, 420 )
roach1.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roach1.setMonsterLevelForChildren( bugML )
roach1.feudWithMonsterType( "p3_war" )
roach1.feudWithMonsterType( "p3" )
roach1.feudWithMonsterType( "p3_LT" )
roach1.feudWithMonsterType( "p3_demi" )
roach1.setMiniEventSpec( bugBreakoutSpec )

roach2 = createStoppedEventSpawner( "Aqueduct_306", "roach2", "alien_bug", 30)
roach2.setPos( 420, 420 )
roach2.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roach2.setMonsterLevelForChildren( bugML )
roach2.feudWithMonsterType( "p3_war" )
roach2.feudWithMonsterType( "p3" )
roach2.feudWithMonsterType( "p3_LT" )
roach2.feudWithMonsterType( "p3_demi" )
roach2.setMiniEventSpec( bugBreakoutSpec )

roach3 = createStoppedEventSpawner( "Aqueduct_306", "roach3", "alien_bug", 30)
roach3.setPos( 420, 420 )
roach3.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roach3.setMonsterLevelForChildren( bugML )
roach3.feudWithMonsterType( "p3_war" )
roach3.feudWithMonsterType( "p3" )
roach3.feudWithMonsterType( "p3_LT" )
roach3.feudWithMonsterType( "p3_demi" )
roach3.setMiniEventSpec( bugBreakoutSpec )

//Strong Patrol
walkerLT1 = createStoppedEventSpawner( "Aqueduct_306", "walkerLT1", "alien_walker_LT", 6)
walkerLT1.setPos( 420, 420 )
walkerLT1.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
walkerLT1.setMonsterLevelForChildren( bugML )
walkerLT1.feudWithMonsterType( "p3_war" )
walkerLT1.feudWithMonsterType( "p3" )
walkerLT1.feudWithMonsterType( "p3_LT" )
walkerLT1.feudWithMonsterType( "p3_demi" )
walkerLT1.setMiniEventSpec( bugBreakoutSpec )

roachLT1 = createStoppedEventSpawner( "Aqueduct_306", "roachLT1", "alien_bug_LT", 30)
roachLT1.setPos( 420, 420 )
roachLT1.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roachLT1.setMonsterLevelForChildren( bugML )
roachLT1.feudWithMonsterType( "p3_war" )
roachLT1.feudWithMonsterType( "p3" )
roachLT1.feudWithMonsterType( "p3_LT" )
roachLT1.feudWithMonsterType( "p3_demi" )
roachLT1.setMiniEventSpec( bugBreakoutSpec )

roachLT2 = createStoppedEventSpawner( "Aqueduct_306", "roachLT2", "alien_bug_LT", 30)
roachLT2.setPos( 420, 420 )
roachLT2.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roachLT2.setMonsterLevelForChildren( bugML )
roachLT2.feudWithMonsterType( "p3_war" )
roachLT2.feudWithMonsterType( "p3" )
roachLT2.feudWithMonsterType( "p3_LT" )
roachLT2.feudWithMonsterType( "p3_demi" )
roachLT2.setMiniEventSpec( bugBreakoutSpec )

roachLT3 = createStoppedEventSpawner( "Aqueduct_306", "roachLT3", "alien_bug_LT", 30)
roachLT3.setPos( 420, 420 )
roachLT3.setWanderBehaviorForChildren( 75, 200, 1, 3, 250)
roachLT3.setMonsterLevelForChildren( bugML )
roachLT3.feudWithMonsterType( "p3_war" )
roachLT3.feudWithMonsterType( "p3" )
roachLT3.feudWithMonsterType( "p3_LT" )
roachLT3.feudWithMonsterType( "p3_demi" )
roachLT3.setMiniEventSpec( bugBreakoutSpec )

//========================================
// BUG BREAKOUT LOGIC                     
//========================================

eventEnded = false
disposeList = []
deathRoachList = [] as Set
originalDeathRoachList = []
masterHateCollector = [] as Set
deathRoachMap = [:]

totalHateCollected = 0

myManager.schedule(3) { startBugBreakout() }

homeList = [ "Aqueduct_5", "Aqueduct_6", "Aqueduct_7", "Aqueduct_107", "Aqueduct_207", "Aqueduct_307", "Aqueduct_407", "Aqueduct_506", "Aqueduct_505", "Aqueduct_504", "Aqueduct_403", "Aqueduct_304", "Aqueduct_205", "Aqueduct_105" ]

homeMap = [ "Aqueduct_5": [520, 330], "Aqueduct_6": [520, 330], "Aqueduct_7": [520, 330], "Aqueduct_107": [520, 330], "Aqueduct_207": [550, 330], "Aqueduct_307": [520, 280], "Aqueduct_407": [340, 460], "Aqueduct_506": [520, 330], "Aqueduct_505": [380, 250], "Aqueduct_504": [520, 330], "Aqueduct_403": [520, 440], "Aqueduct_304": [520, 330], "Aqueduct_205": [400, 490], "Aqueduct_105": [520, 330] ]

def startBugBreakout() {
	sound( "bugBreakoutEventStart" ).toZone()
	
	myManager.schedule(2) {
		zoneBroadcast( "Agent Caruthers-VQS", "Bug Breakout!", "The alien Hive World is establishing a foothold on our world! We need everyone's help to push them back!" )
		deathRoachSpawn()
		makeZoneCounter( "Drive the Bugs Back!", 0 ).watchForKill( "alien_walker", "alien_bug", "alien_walker_LT", "alien_bug_LT" ).setGoal(100).onCompletion( { event ->
			removeZoneBroadcast( "Bug Breakout!" )
			myManager.schedule(2) { zoneBroadcast( "Agent Caruthers-VQS", "Aliens Driven to Hive!", "Great work, Gaians! We defeated enough Hive aliens that they're retreating back through the Portal. For now anyway..." ) }
			myManager.schedule(17) { removeZoneBroadcast( "Aliens Driven to Hive!" ) }

			//call the bugs back to the portal
			eventEnded = true

			//cancel the regular recall, in case it's already in effect and sound a new Recall
			regularRecall.cancel()
			//bomb1Timer.cancel()
			//bomb2Timer.cancel()
			soundRecall()

			//cancel the lastFiveMinutesCountdown so the countdown timer doesn't start (if it hasn't already)
			lastFiveMinutesCountdown.cancel()

			//cancel the countDown timer if it is already in existence.
			removeZoneTimer("Remaining Time")

			//get rid of the killCounter
			removeZoneCounter( "Drive the Bugs Back!" )

			if( !masterHateCollector.isEmpty() ) {
				//print out some data mining so we can look in logs during tests or live events
				println "************************************************"
				println "********** EVENT HATE SUMMARY ***************"
				println "**** The Player Hate Map for this event is: ****"
				println "**********       (AQUEDUCT)     ***************"
				println ""
				println "masterHateCollector = ${masterHateCollector}"
				println ""
				println "totalHateCollected = ${ totalHateCollected }"
				println ""
				println "numPeople in Event = ${masterHateCollector.size()} ****"
				println ""
				println "average Hate per person = ${ totalHateCollected / masterHateCollector.size() }"
				println ""
				println "********** EVENT HATE SUMMARY ***************"
				println "************************************************"

				rewardPlayers()
			}
		} )

		lastFiveMinutesCountdown = myManager.schedule( 1500 ) {
			makeZoneTimer("Remaining Time", "0:5:0", "0:0:0").onCompletion( { event ->
				removeZoneTimer( "Remaining Time" )
				removeZoneCounter( "Drive the Bugs Back!" )
				regularRecall.cancel()
				//bomb1Timer.cancel()
				//bomb2Timer.cancel()
				eventEnded = true
				soundRecall()
				removeZoneBroadcast( "Bug Breakout!" )
				myManager.schedule(2) { zoneBroadcast( "Agent Caruthers-VQS", "The Bugs Succeeded!", "Unfortunately, whatever the bugs were attempting to do, they accomplished. Our defenses need to be stronger next time, or Gaia is doomed!" ) }
				myManager.schedule(15) { removeZoneBroadcast( "The Bugs Succeeded!" ) }
			}).start()
		}
	}
}

def deathRoachSpawn() {
	if( eventEnded == true ) return

	//Active the Star Portal and star the Death Roach process
	portal.on()
	sound( "portalActivate").toZone()
	myManager.schedule(2) {
		sound( "portalTravel" ).toZone()
		
		// Make a playerList in the buffer rooms around Aqueduct_306
		findDeathRoachList()
		
		// Spawn one DEATHROACH for each player in that area
		if( !deathRoachList.isEmpty() ) {
			deathRoachList.clone().each{ 
				deathRoachSpawner.setMonsterLevelForChildren( it.getConLevel() )
				room = it.getRoom().toString()
				X = it.getX().intValue()
				Y = it.getY().intValue()
				deathRoachSpawner.setHomeForChildren( room, X, Y )
				deathRoachSpawner.setInitialMoveForChildren( room, X, Y )
				d1 = deathRoachSpawner.forceSpawnNow()
			
				
				//warn the player
				it.centerPrint( "Look out! There's a Deathroach with your name on it! Don't let it touch you!!!" )
				
				d1.setDisplayName( "Death to ${it}!" )

				// The deathroach HATES the player they are spawned for and beelines for them. (Assign 100000 hate to the player so it can't be de-taunted.)
				d1.addHate( it, 100000 )

				//add the player and matched roach to a map so the roach can be deleted later if the player moves out of the buffer zone
				deathRoachMap.put( it, d1 )
			}
			//Start checking the deathRoaches now to eliminate them if the player leaves the buffer zone
			originalDeathRoachList = deathRoachList.clone()
			checkDeathRoaches()
		}
	}		
	myManager.schedule(5) { portal.off() }
	//After the death roaches have a chance to clear the area, then spawn the patrols
	myManager.schedule(8) {
		portal.on()
		sound( "portalActivate").toZone()
		myManager.schedule(2) {
			sound( "portalTravel" ).toZone()
			numPatrols = 5
			tempHomeList = homeList.clone()
			tempHomeMap = homeMap.clone()
			spawnPatrols()
		}
	}
}

def findDeathRoachList() {
	deathRoachList.clear()
	myRooms.Aqueduct_306.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_305.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_205.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_206.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_207.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_307.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_405.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_406.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
	myRooms.Aqueduct_407.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() && it.getConLevel() < 6.1 ) { deathRoachList << it } }
}

def checkDeathRoaches() {
	//if there's no players left in the deathRoachMap, then stop running the routine
	if( deathRoachMap.isEmpty() ) return
	//find players still in the buffer zone
	findDeathRoachList()
	
	//now go through the original deathRoachList one at a time to see if the player is still in the buffer zone
	originalDeathRoachList.clone().each{ 
		//if the player is NOT in the area, or if the player is Dazed, then the deathRoach for him needs to be destroyed...if it's still alive
		if( !deathRoachList.contains( it ) || it.isDazed() ) {
			//if the player still exists in the deathRoachMap, then keep checking
			if( deathRoachMap.containsKey( it ) ) {
				//check to see if the bug associated with that player is still alive
				playersBug = deathRoachMap.get( it )
				//if that bug is still alive, then destroy it
				if( !playersBug.isDead() ) {
					playersBug.instantPercentDamage( 100 )
				}
				//whether the bug was alive or not, now remove the player from the deathRoachMap so he doesn't keep getting checked
				deathRoachMap.remove( it )
			}
		//if the player IS in the area, and is still contained in the deathRoachMap, then check the state of its deathRoach and remove the player from the deathRoachMap if the roach is dead aleady.
		} else if( deathRoachList.contains( it ) ) {
			if( deathRoachMap.containsKey( it ) ) {
				playersBug = deathRoachMap.get( it )
				if( playersBug.isDead() ) {
					deathRoachMap.remove( it )
				}
			}
		}
	}
	myManager.schedule(3) { checkDeathRoaches() }
}



patrol5 = []
patrol4 = []
patrol3 = []
patrol2 = []
patrol1 = []

def spawnPatrols() {
	if( eventEnded == true ) return
	if( numPatrols > 0 ) {
		//determine the "HOME" location for this spawn cycle
		room = random( tempHomeList )
		mapRoom = tempHomeMap.get( room )
		X = mapRoom.get(0).intValue()
		Y = mapRoom.get(1).intValue()
		//remove that room so it's not selected for the next patrol
		tempHomeList.remove( room )
		//randomize the ML for the patrol
		randombugML()
		//figure out which type of patrol to spawn
		roll = random( 100 ) 
		
		//add miniMapMarker to let players know where to go to find the bugs (to pull them away from the Star Portal more)
		if( numPatrols == 5 ) { addMiniMapMarker("patrolMarker5", "markerOther", room, X, Y, "Bug Patrol") }
		if( numPatrols == 4 ) { addMiniMapMarker("patrolMarker4", "markerOther", room, X, Y, "Bug Patrol") }
		if( numPatrols == 3 ) { addMiniMapMarker("patrolMarker3", "markerOther", room, X, Y, "Bug Patrol") }
		if( numPatrols == 2 ) { addMiniMapMarker("patrolMarker2", "markerOther", room, X, Y, "Bug Patrol") }
		if( numPatrols == 1 ) { addMiniMapMarker("patrolMarker1", "markerOther", room, X, Y, "Bug Patrol") }
		
		if( roll < 75 ) {
			//spawn Weak Patrol
			walker1.setHomeForChildren( room, X, Y )
			walker1.setInitialMoveForChildren( room, X, Y )
			roach1.setHomeForChildren( room, X, Y )
			roach1.setInitialMoveForChildren( room, X, Y )
			roach2.setHomeForChildren( room, X, Y )
			roach2.setInitialMoveForChildren( room, X, Y )
			roach3.setHomeForChildren( room, X, Y )
			roach3.setInitialMoveForChildren( room, X, Y )
			w1 = walker1.forceSpawnNow()
			runOnDeath( w1, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 1210 ) { bugHate = 1210 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
			r1 = roach1.forceSpawnNow()
			runOnDeath( r1, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 410 ) { bugHate = 410 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
			r2 = roach2.forceSpawnNow()
			runOnDeath( r2, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 410 ) { bugHate = 410 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
			r3 = roach3.forceSpawnNow()
			runOnDeath( r3, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 410 ) { bugHate = 410 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
		} else {
			//spawn Strong Patrol
			walkerLT1.setHomeForChildren( room, X, Y )
			walkerLT1.setInitialMoveForChildren( room, X, Y )
			roachLT1.setHomeForChildren( room, X, Y )
			roachLT1.setInitialMoveForChildren( room, X, Y )
			roachLT2.setHomeForChildren( room, X, Y )
			roachLT2.setInitialMoveForChildren( room, X, Y )
			roachLT3.setHomeForChildren( room, X, Y )
			roachLT3.setInitialMoveForChildren( room, X, Y )
			w1 = walkerLT1.forceSpawnNow()
			runOnDeath( w1, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 4900 ) { bugHate = 4900 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
			r1 = roachLT1.forceSpawnNow()
			runOnDeath( r1, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 1210 ) { bugHate = 1210 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
			r2 = roachLT2.forceSpawnNow()
			runOnDeath( r2, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 1210 ) { bugHate = 1210 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
			r3 = roachLT3.forceSpawnNow()
			runOnDeath( r3, { event ->
				event.actor.getHated().each{
					//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
					if( !masterHateCollector.contains( it ) && isPlayer( it ) ) {
						it.setPlayerVar( "Z07BugBreakoutCurrentHate", 0 )
					}
					if( isPlayer( it ) ) { masterHateCollector << it }
				}
				event.actor.getHatedMap().each() { player, bugHate ->
					if( isPlayer( player ) ) {
						if( bugHate > 1210 ) { bugHate = 1210 }
						totalHateCollected = totalHateCollected + bugHate //NOTE: This line is only for data tracking...not needed by the logic of the event
						//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
						player.addPlayerVar( "Z07BugBreakoutCurrentHate", bugHate )
					}
				}
			} )
		}
		numPatrols --
		spawnPatrols()
	} else {
		myManager.schedule(5) { portal.off() }
		//bomb1Timer = myManager.schedule( random( 20, 50 ) ) { portalBuster() }
		//bomb2Timer = myManager.schedule( random( 70, 110 ) ) { portalBuster() }
		
		//recall the aliens after being out there for 75-120 seconds
		regularRecall = myManager.schedule( random( 75, 120 ) ) { soundRecall() } 
	}
}

//wait a certain amount of time, and then setHomeForEveryone and make them come back to the Star Portal
def soundRecall() {
	//remove the miniMap Markers
	removeMiniMapMarker( "patrolMarker5" )
	removeMiniMapMarker( "patrolMarker4" )
	removeMiniMapMarker( "patrolMarker3" )
	removeMiniMapMarker( "patrolMarker2" )
	removeMiniMapMarker( "patrolMarker1" )

	sound( "hiveRecallSiren" ).toZone()
	portal.on()
	sound( "portalActivate").toZone()
	disposalOkay = true //enables the trigger zone for disposing of the bugs as they return to the portal
	myManager.schedule(2) {
		//if( eventEnded == false ) {
		//	zoneBroadcast( "Agent Caruthers-VQS", "Heading to the Hive!", "The bugs are heading back to their Hive! Watch yourselves!" )
		//	myManager.schedule(5) { removeZoneBroadcast( "Heading to the Hive!" ) }
		//}
		walker1.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		roach1.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		roach2.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		roach3.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		walkerLT1.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		roachLT1.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		roachLT2.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		roachLT3.setHomeForEveryone( "Aqueduct_306", 420, 420 )
		myManager.schedule( 40 ) { cleanUpPatrols(); stopSound( "hiveRecallSiren" ).toZone() }
	}
}

def cleanUpPatrols() {
	//go through all rooms in the zone and dispose of any instances of alien creatures
	//this is to avoid pathfinding errors screwing over the spawners
	
	myRooms.Aqueduct_1.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_2.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_4.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_5.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_6.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_7.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_102.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_103.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_104.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_105.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_106.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_107.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_202.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_203.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_204.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_205.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_206.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_207.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_303.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_304.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_305.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_306.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_307.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_403.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_404.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_405.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_406.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_407.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_504.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_505.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_506.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }
	myRooms.Aqueduct_507.getActorList().each { if( isMonster( it ) && ( it.getMonsterType() == "alien_walker" || it.getMonsterType() == "alien_bug" || it.getMonsterType() == "alien_walker_LT" || it.getMonsterType() == "alien_bug_LT" ) ) { disposeList << it } }

	disposeList.clone().each{
		if( !it.isDead() ) {
			it.dispose()
		}
		patrol5.clear()
		patrol4.clear()
		patrol3.clear()
		patrol2.clear()
		patrol1.clear()
	}
	
	//deactivate the Portal effect
	portal.off()
	disposalOkay = false
	
	if( eventEnded == true ) {
		eventDone()
	} else {
		//after the clean-up, schedule the next deathRoachSpawn()
		myManager.schedule( random( 15, 30 ) ) {
			zoneBroadcast( "Agent Caruthers-VQS", "Warning!", "Lookout! The GIB sensors are spiking. Those Deathroaches are back! Get away from the Portal area!" )
			myManager.schedule(2) {
				deathRoachSpawn()
			}
			myManager.schedule(5){ removeZoneBroadcast( "Warning!" ) }
		}
	}
}

//calculate a random riverML between 5.5 and 6.5
def randombugML() {
	roll = random( 10 )
	bugML = 5.3 + ( roll * 0.1 )
	//Now update all the spawners with the new ML so they start creating monsters at that new level
	walker1.setMonsterLevelForChildren( bugML )
	roach1.setMonsterLevelForChildren( bugML )
	roach2.setMonsterLevelForChildren( bugML )
	roach3.setMonsterLevelForChildren( bugML )
	walkerLT1.setMonsterLevelForChildren( bugML )
	roachLT1.setMonsterLevelForChildren( bugML )
	roachLT2.setMonsterLevelForChildren( bugML )
	roachLT3.setMonsterLevelForChildren( bugML )
}

/*
//============================
//SET US UP THE BOMB!         
//============================
def portalBuster() {
	bomb = busterBombSpawner.forceSpawnNow()
	bomb.setDisplayName( "Portal Buster" )

	bomb.say( "I am a 10-second bomb!" )
	myManager.schedule(1) { bomb.say( "9" ) }
	myManager.schedule(2) { bomb.say( "8" ) }
	myManager.schedule(3) { bomb.say( "7" ) }
	myManager.schedule(4) { bomb.say( "6" ) }
	myManager.schedule(5) { bomb.say( "5" ) }
	myManager.schedule(6) { bomb.say( "4" ) }
	myManager.schedule(7) { bomb.say( "3" ) }
	myManager.schedule(8) { bomb.say( "2" ) }
	myManager.schedule(9) { bomb.say( "1" ) }
	myManager.schedule(10) { goBoom() }
}

def goBoom() {
	bomb.say( "BOOM!" )
	//explode the bomb when the countdown reaches zero
	bomb.instantPercentDamage(100)
	//now kill the players in the room
	myRooms.Aqueduct_306.getActorList().each {
		//knock 'em back away from the portal...even if dazed
		if( isPlayer( it ) ) { knockBack( 300, 600, "Aqueduct_306", 420, 420 ) }
		//kill them...if not already dazed
		if( isPlayer( it ) && !it.isDazed() ) { it.smite() }
	}
}
*/

//======================
//REWARD ROUTINE        
//======================

averageHatePerPlayer = 4500
maxGoldGrant = 300
maxOrbGrant = 20
desiredChanceComponentReceived = 50

def rewardPlayers() {
	masterHateCollector.clone().each() {
		//make sure it's a player (pets are coming!) and make sure that player is still online
		if( isOnline( it ) && isPlayer( it ) && !isInOtherLayer( it ) ) {
			
			//All players recieve gold relating to the amount of Hate they generate during the event
			goldGrant = (it.getPlayerVar( "Z07BugBreakoutCurrentHate" ) * (maxGoldGrant / averageHatePerPlayer) ).intValue()
			//cap the grant at 300 gold
			if( goldGrant > maxGoldGrant ) { goldGrant = maxGoldGrant }
			if( goldGrant < 1 ) { goldGrant = 1 }
			it.grantCoins( goldGrant )
			it.centerPrint( "You earned ${goldGrant} gold!" )

			//grant orbs for playing in the mini-event
			orbGrant = (it.getPlayerVar( "Z07BugBreakoutCurrentHate" ) * (maxOrbGrant / averageHatePerPlayer) ).intValue()
			if( orbGrant > maxOrbGrant ) { orbGrant = maxOrbGrant }
			if( orbGrant < 1 ) { orbGrant = 1 }
			it.grantQuantityItem( 100257, orbGrant )
			it.centerPrint( "You also earned ${orbGrant} Charge Orbs!")

			//Increment a playerVar (by one) that records the number of times the player has participated in this particular event
			it.addPlayerVar( "Z07BugBreakoutTotalEventVisits", 1 )
			
			//Increment the "All-Time" Hate Collector with the amount of hate accumulated during this event
			thisEventHate = it.getPlayerVar( "Z07BugBreakoutCurrentHate" )
			it.addPlayerVar( "Z07BugBreakoutAllTimeEventHate", thisEventHate )
			if( thisEventHate > 7000 ) { thisEventHate = 7000 } //cap at 7000 for a single event.
			it.centerPrint( "And you earned ${thisEventHate.intValue()} points toward Bug Breakout Badges!" )
			
			//Award badges for fighting Airshark if beyond certain all-time Hate levels...
			if( it.getPlayerVar( "Z07BugBreakoutCurrentHate" ) > averageHatePerPlayer * 1.2 ) { 
				it.updateQuest( 318, "Agent Caruthers-VQS" ) //complete the BUG KILLER badge
			}
			//... played the Event a certain number of times
			if( it.getPlayerVar( "Z07BugBreakoutAllTimeEventHate" ) > averageHatePerPlayer * 10 ) { 
				it.updateQuest( 319, "Agent Caruthers-VQS" ) //complete the INSECTICIDE badge
			}
			
			/*
			//Player receive a "big monster"-specific loot item *if* the roll under a certain percentage...which is based on the amount of hate accumulated in the fights.
			roll = random( 100 ) 
			eventHate = it.getPlayerVar( "Z07BugBreakoutCurrentHate" )
			//this multiplier is just a scaling value to adjust hate points. Figure out the average hate accumulated during the event and adjust accordingly.
			multiplier = desiredChanceComponentReceived / averageHatePerPlayer

			if( roll < (eventHate * multiplier).intValue() ) { 
				//grant the loot component for this mini-event
				it.grantItem( "100442" ) //grant ALIEN EGG SAC
				it.centerPrint( "You found an Alien Egg Sac!" )
			} else {
				it.centerPrint( "You find no specialty item this time. Try again when next the Hive busts out!" )
			}
			*/
		}
	}
}


