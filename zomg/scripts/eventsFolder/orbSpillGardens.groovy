import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (BILL'S RANCH)                  
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 4.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 4.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "ZENGARDEN_503", "orb", "orb_gardens", 100 )
orb.setPos( 350, 100 )
orb.setMonsterLevelForChildren( 3.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )

//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "Katsumi-VQS", "The Swirling Eddies", "The lifeforce eddies my Uncle speaks of are now creating Orbs in addition to Animated! Gather them while our luck holds!" )
	makeZoneTimer("Orb Spill Timer", "0:15:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "The Swirling Eddies" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "Katsumi-VQS", "Eddies have Calmed", "Whatever rippled through the eddies to make them create Charge Orbs has subsided. Perhaps we will be lucky again another time." )
			orbDisposal()
			myManager.schedule(15) { removeZoneBroadcast( "Eddies have Calmed" ); eventDone() }
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.ZENGARDEN_1.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_2.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_4.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_5.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_101.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_102.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_104.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_105.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_201.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_205.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_301.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_302.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_401.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_402.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_403.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_405.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_501.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_503.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_504.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_505.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_601.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_602.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_603.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_604.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_605.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_701.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_702.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_703.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.ZENGARDEN_704.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.ZENGARDEN_1.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_2.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_4.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_5.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_101.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_102.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_104.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_105.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_201.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_205.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_301.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_302.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_401.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_402.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_403.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_404.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_405.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_501.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_503.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_504.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_505.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_601.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_602.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_603.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_604.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_605.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_701.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_702.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_703.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
	myRooms.ZENGARDEN_704.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_gardens" ) { it.dispose() } }
}

zen1Map = [ 1:[190, 490, 0], 2:[440, 170, 0], 3:[600, 370, 0], 4:[950, 550, 0], 5:"ZENGARDEN_1" ]
zen2Map = [ 1:[100, 100, 0], 2:[300, 560, 0], 3:[640, 120, 0], 4:[880, 390, 0], 5:"ZENGARDEN_2" ]
//SAFE ROOM! zen3Map = [ 1:[40, 60, 0], 2:[320, 500, 0], 3:[690, 50, 0], 4:[920, 450, 0], 5:"ZENGARDEN_3" ]
zen4Map = [ 1:[200, 420, 0], 2:[480, 340, 0], 3:[650, 650, 0], 4:[980, 160, 0], 5:"ZENGARDEN_4" ]
zen5Map = [ 1:[120, 90, 0], 2:[370, 40, 0], 3:[630, 650, 0], 4:[840, 230, 0], 5:"ZENGARDEN_5" ]
zen101Map = [ 1:[90, 130, 0], 2:[210, 520, 0], 3:[680, 410, 0], 4:[1000, 120, 0], 5:"ZENGARDEN_101" ]
zen102Map = [ 1:[60, 650, 0], 2:[240, 190, 0], 3:[600, 250, 0], 4:[870, 620, 0], 5:"ZENGARDEN_102" ]
zen103Map = [ 1:[190, 630, 0], 2:[450, 280, 0], 3:[780, 630, 0], 4:[990, 210, 0], 5:"ZENGARDEN_103" ]
zen104Map = [ 1:[280, 300, 0], 2:[450, 650, 0], 3:[710, 260, 0], 4:[970, 470, 0], 5:"ZENGARDEN_104" ]
zen105Map = [ 1:[30, 450, 0], 2:[440, 500, 0], 3:[730, 280, 0], 4:[860, 650, 0], 5:"ZENGARDEN_105" ]
zen201Map = [ 1:[100, 560, 0], 2:[340, 320, 0], 3:[640, 260, 0], 4:[1000, 260, 0], 5:"ZENGARDEN_201" ]
zen202Map = [ 1:[40, 120, 0], 2:[330, 400, 0], 3:[500, 440, 0], 4:[1000, 110, 0], 5:"ZENGARDEN_202" ]
zen203Map = [ 1:[130, 40, 0], 2:[290, 470, 0], 3:[740, 140, 0], 4:[1000, 560, 0], 5:"ZENGARDEN_203" ]
zen204Map = [ 1:[90, 500, 0], 2:[300, 310, 0], 3:[670, 405, 0], 4:[980, 410, 0], 5:"ZENGARDEN_204" ]
zen205Map = [ 1:[100, 440, 0], 2:[510, 150, 0], 3:[630, 340, 0], 4:[860, 530, 0], 5:"ZENGARDEN_205" ]
zen301Map = [ 1:[70, 370, 0], 2:[310, 530, 0], 3:[840, 520, 0], 4:[1000, 270, 0], 5:"ZENGARDEN_301" ]
zen302Map = [ 1:[40, 230, 0], 2:[400, 150, 0], 3:[770, 630, 0], 4:[920, 140, 0], 5:"ZENGARDEN_302" ]
zen303Map = [ 1:[165, 420, 0], 2:[300, 170, 0], 3:[730, 220, 0], 4:[1000, 520, 0], 5:"ZENGARDEN_303" ]
zen304Map = [ 1:[260, 190, 0], 2:[500, 260, 0], 3:[60, 470, 0], 4:[750, 250, 0], 5:"ZENGARDEN_304" ]
//SAFE ROOM! zen305Map = [ 1:[200, 570, 0], 2:[380, 230, 0], 3:[640, 640, 0], 4:[840, 160, 0], 5:"ZENGARDEN_305" ]
zen401Map = [ 1:[80, 160, 0], 2:[460, 540, 0], 3:[650, 550, 0], 4:[970, 160, 0], 5:"ZENGARDEN_401" ]
zen402Map = [ 1:[120, 160, 0], 2:[450, 620, 0], 3:[700, 200, 0], 4:[990, 260, 0], 5:"ZENGARDEN_402" ]
zen403Map = [ 1:[60, 260, 0], 2:[380, 390, 0], 3:[540, 70, 0], 4:[940, 440, 0], 5:"ZENGARDEN_403" ]
zen404Map = [ 1:[140, 400, 0], 2:[300, 320, 0], 3:[530, 350, 0], 4:[1010, 610, 0], 5:"ZENGARDEN_404" ]
zen405Map = [ 1:[280, 400, 0], 2:[450, 380, 0], 3:[680, 40, 0], 4:[910, 430, 0], 5:"ZENGARDEN_405" ]
zen501Map = [ 1:[220, 140, 0], 2:[370, 650, 0], 3:[800, 200, 0], 4:[900, 580, 0], 5:"ZENGARDEN_501" ]
//SAFE ROOM! zen502Map = [ 1:[150, 410, 0], 2:[610, 480, 0], 3:[740, 130, 0], 4:[1010, 170, 0], 5:"ZENGARDEN_502" ]
zen503Map = [ 1:[40, 170, 0], 2:[500, 200, 0], 3:[810, 570, 0], 4:[1010, 200, 0], 5:"ZENGARDEN_503" ]
zen504Map = [ 1:[160, 200, 0], 2:[350, 490, 0], 3:[590, 200, 0], 4:[1010, 600, 0], 5:"ZENGARDEN_504" ]
zen505Map = [ 1:[40, 120, 0], 2:[260, 610, 0], 3:[690, 520, 0], 4:[920, 520, 0], 5:"ZENGARDEN_505" ]
zen601Map = [ 1:[260, 150, 0], 2:[490, 530, 0], 3:[810, 70, 0], 4:[970, 500, 0], 5:"ZENGARDEN_601" ]
zen602Map = [ 1:[60, 410, 0], 2:[270, 520, 0], 3:[750, 110, 0], 4:[910, 560, 0], 5:"ZENGARDEN_602" ]
zen603Map = [ 1:[190, 430, 0], 2:[390, 300, 0], 3:[660, 170, 0], 4:[920, 330, 0], 5:"ZENGARDEN_603" ]
zen604Map = [ 1:[280, 380, 0], 2:[490, 460, 0], 3:[760, 140, 0], 4:[1010, 350, 0], 5:"ZENGARDEN_604" ]
zen605Map = [ 1:[230, 100, 0], 2:[330, 540, 0], 3:[630, 600, 0], 4:[960, 440, 0], 5:"ZENGARDEN_605" ]
zen701Map = [ 1:[290, 660, 0], 2:[60, 420, 0], 3:[860, 320, 0], 4:[1010, 120, 0], 5:"ZENGARDEN_701" ]
zen702Map = [ 1:[30, 140, 0], 2:[430, 460, 0], 3:[810, 460, 0], 4:[1010, 110, 0], 5:"ZENGARDEN_702" ]
zen703Map = [ 1:[90, 330, 0], 2:[400, 490, 0], 3:[630, 430, 0], 4:[900, 590, 0], 5:"ZENGARDEN_703" ]
zen704Map = [ 1:[200, 530, 0], 2:[600, 250, 0], 3:[830, 370, 0], 4:[900, 620, 0], 5:"ZENGARDEN_704" ]

roomList = [ zen1Map, zen2Map, zen4Map, zen5Map, zen101Map, zen102Map, zen103Map, zen104Map, zen105Map, zen201Map, zen202Map, zen203Map, zen204Map, zen205Map, zen301Map, zen302Map, zen303Map, zen304Map, zen401Map, zen402Map, zen403Map, zen404Map, zen405Map, zen501Map, zen503Map, zen504Map, zen505Map, zen601Map, zen602Map, zen603Map, zen604Map, zen605Map, zen701Map, zen702Map, zen703Map, zen704Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.ZENGARDEN_1.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_2.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_4.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_5.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_101.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_102.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_103.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_104.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_105.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_201.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_202.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_203.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_204.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_205.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_301.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_302.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_303.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_304.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_401.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_402.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_403.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_404.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_405.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_501.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_503.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_504.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_505.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_601.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_602.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_603.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_604.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_605.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_701.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_702.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_703.getSpawnTypeCount( "orb_gardens" )
	orbCount = orbCount + myRooms.ZENGARDEN_704.getSpawnTypeCount( "orb_gardens" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 20 ) {
		maxOrbs = 20
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 5 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb, { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			} )
		}
		//now spawn another orb a quarter-second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


