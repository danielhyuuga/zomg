import com.gaiaonline.mmo.battle.script.*;

BASSKEN_1 = []
BASSKEN_2 = []
BASSKEN_3 = []
BASSKEN_4 = []
BASSKEN_5 = []
BASSKEN_7 = []
BASSKEN_101 = []
BASSKEN_102 = []
BASSKEN_103 = []
BASSKEN_104 = []
BASSKEN_105 = []
BASSKEN_106 = []
BASSKEN_107 = []
BASSKEN_201 = []
BASSKEN_202 = []
BASSKEN_203 = []
BASSKEN_204 = []
BASSKEN_205 = []
BASSKEN_206 = []
BASSKEN_207 = []
BASSKEN_301 = []
BASSKEN_302 = []
BASSKEN_305 = []
BASSKEN_306 = []
BASSKEN_307 = []
BASSKEN_401 = []
BASSKEN_402 = []
BASSKEN_404 = []
BASSKEN_405 = []
BASSKEN_406 = []
BASSKEN_407 = []
BASSKEN_501 = []
BASSKEN_502 = []
BASSKEN_503 = []
BASSKEN_504 = []
BASSKEN_505 = []
BASSKEN_506 = []
BASSKEN_507 = []
BASSKEN_601 = []
BASSKEN_602 = []
BASSKEN_603 = []
BASSKEN_604 = []
BASSKEN_605 = []
BASSKEN_606 = []
	
//======================
//BUZZKILL SPAWNER      
//======================

//Make BuzzKill invulnerable to players over CL 5.0
def buzzKillEventSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 5.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 5.0 or lower to attack BuzzKill." ); }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

buzzKillSpawner = createStoppedEventSpawner( "BASSKEN_103", "buzzKillSpawner", "buzz_saw_boss", 1 )
buzzKillSpawner.setMiniEventSpec( buzzKillEventSpec )
buzzKillSpawner.setPos( 760, 370 )
buzzKillSpawner.setMonsterLevelForChildren( 5.4 )

//======================
//STARTUP & SELECT ROOM 
//======================
buzzKill = null
totalHateCollected = 0

myManager.schedule(3) { nullifyRooms() }

def nullifyRooms() {
	//null out all the lists to prevent errors
	BASSKEN_1.clear()
	BASSKEN_2.clear()
	BASSKEN_3.clear()
	BASSKEN_4.clear()
	BASSKEN_5.clear()
	BASSKEN_7.clear()
	BASSKEN_101.clear()
	BASSKEN_102.clear()
	BASSKEN_103.clear()
	BASSKEN_104.clear()
	BASSKEN_105.clear()
	BASSKEN_106.clear()
	BASSKEN_107.clear()
	BASSKEN_201.clear()
	BASSKEN_202.clear()
	BASSKEN_203.clear()
	BASSKEN_204.clear()
	BASSKEN_205.clear()
	BASSKEN_206.clear()
	BASSKEN_207.clear()
	BASSKEN_301.clear()
	BASSKEN_302.clear()
	BASSKEN_305.clear()
	BASSKEN_306.clear()
	BASSKEN_307.clear()
	BASSKEN_401.clear()
	BASSKEN_402.clear()
	BASSKEN_404.clear()
	BASSKEN_405.clear()
	BASSKEN_406.clear()
	BASSKEN_407.clear()
	BASSKEN_501.clear()
	BASSKEN_502.clear()
	BASSKEN_503.clear()
	BASSKEN_504.clear()
	BASSKEN_505.clear()
	BASSKEN_506.clear()
	BASSKEN_507.clear()
	BASSKEN_601.clear()
	BASSKEN_602.clear()
	BASSKEN_603.clear()
	BASSKEN_604.clear()
	BASSKEN_605.clear()
	BASSKEN_606.clear()
	
	listOfPlayerLists = [ BASSKEN_1, BASSKEN_2, BASSKEN_3, BASSKEN_4, BASSKEN_5, BASSKEN_7, BASSKEN_101, BASSKEN_102, BASSKEN_103, BASSKEN_104, BASSKEN_105, BASSKEN_106, BASSKEN_107, BASSKEN_201, BASSKEN_202, BASSKEN_203, BASSKEN_204, BASSKEN_205, BASSKEN_206, BASSKEN_207, BASSKEN_301, BASSKEN_302, BASSKEN_305, BASSKEN_306, BASSKEN_307, BASSKEN_401, BASSKEN_402, BASSKEN_404, BASSKEN_405, BASSKEN_406, BASSKEN_407, BASSKEN_501, BASSKEN_502, BASSKEN_503, BASSKEN_504, BASSKEN_505, BASSKEN_506, BASSKEN_507, BASSKEN_601, BASSKEN_602, BASSKEN_603, BASSKEN_604, BASSKEN_605, BASSKEN_606 ]
	
	if( buzzKill == null ) {
		startUp()
	} else {
		findPlayers()
	}
}

//Start the scenario
def startUp() {
	sound( "buzzKillEventStart" ).toZone()
	
	//initialize the current room and room list
	currentRoom = random( listOfPlayerLists )
	
	myManager.schedule(2) {
		//announce the event to the zone
		zoneBroadcast( "[NPC] Old Man Logan", "BuzzKill!", "Calling all able-bodied Gaians! There's a new evil in the woods of Bass'ken! Watch yourselves!" )

		//spawn BuzzKill
		buzzKill = buzzKillSpawner.forceSpawnNow()
		buzzKill.setMiniMapMarker( "markerBuzzKill", "BuzzKill" ) 
		//Watch BuzzKill's health and warp him away when he hits certain health thresholds
		myManager.onHealth( buzzKill ) { event ->
			if( event.didTransition( 80 ) && event.isDecrease() ) {
				//select a new room
				normalWarp.cancel()
				emergencyRoom = random( emergencyWarpList )
				warpMeThere()
				//he "rests" for a bit seconds and then warps to a new room with players in it
				myManager.schedule( random( 30, 90 ) ) { nullifyRooms() }
			} else if( event.didTransition( 60 ) && event.isDecrease() ) {
				normalWarp.cancel()
				emergencyRoom = random( emergencyWarpList )
				warpMeThere()
				myManager.schedule( random( 30, 90 ) ) { nullifyRooms() }
			} else if( event.didTransition( 40 ) && event.isDecrease() ) {
				normalWarp.cancel()
				emergencyRoom = random( emergencyWarpList )
				warpMeThere()
				myManager.schedule( random( 30, 90 ) ) { nullifyRooms() }
			} else if( event.didTransition( 20 ) && event.isDecrease() ) {
				normalWarp.cancel()
				emergencyRoom = random( emergencyWarpList )
				warpMeThere()
				myManager.schedule( random( 30, 90 ) ) { nullifyRooms() }
			}
		}

		runOnDeath( buzzKill, { event ->
			//cleanup all timers and zoneBroadcasts
			removeZoneBroadcast( "BuzzKill!" )
			buzzKill.clearMiniMapMarker()
			lastFiveMinutesCountdown.cancel()
			removeZoneTimer( "Remaining Time" )
			removeZoneBroadcast( "It Ran Away!" )
			removeZoneBroadcast( "BuzzKill Wants Fresh Meat!" )

			//each time the event starts up, initialize the playerVar that holds Hate for that event
			event.actor.getHated().each{
				if( !masterHateCollector.contains( it ) ) {
					it.setPlayerVar( "Z06BuzzKillCurrentEventHate", 0 )
				}
			}
			//now make the master lists and currentEvent Hate totals
			event.actor.getHatedMap().each{ player, hateAmount ->
				
				//cap the hate you can get in one scenario
				if( hateAmount > 1500 ) { hateAmount = 1500 }
				
				totalHateCollected = totalHateCollected + hateAmount //For data mining only. Not used in logic of the script

				//add the hate accumulated into the current and all-time playerVars
				player.addPlayerVar( "Z06BuzzKillCurrentEventHate", hateAmount )

				//add the player to the master hate collector and then ensure there is only one instance of each player in that list
				masterHateCollector << player
			}
			
			myManager.schedule(2) {
				//Have Logan sound the "all clear" once BuzzKill is dead
				zoneBroadcast( "[NPC] Old Man Logan", "Buzz Killed!!", "You've done it! Defending our town against threats like that is an act of true heroism! Congrats, Gaians!" )
				myManager.schedule( 15 ) {
					removeZoneBroadcast( "Buzz Killed!!" )
					eventDone()
				}
				if( !masterHateCollector.isEmpty() ) {
					//print out some data mining so we can look in logs during tests or live events
					println "************************************************"
					println "********** EVENT HATE SUMMARY ***************"
					println "**** The Player Hate Map for this event is: ****"
					println "**********       (BASSKEN)     ***************"
					println ""
					println "masterHateCollector = ${masterHateCollector}"
					println ""
					println "totalHateCollected = ${ totalHateCollected }"
					println ""
					println "numPeople in Event = ${masterHateCollector.size()} ****"
					println ""
					println "average Hate per person = ${ totalHateCollected / masterHateCollector.size() }"
					println ""
					println "********** EVENT HATE SUMMARY ***************"
					println "************************************************"

					rewardPlayers()
				}
			}
		} )

		myManager.schedule(2) {
			//start looking for players to kill after we take a couple seconds to ensure buzzkill is properly spawned
			findPlayers()
		}
		
		lastFiveMinutesCountdown = myManager.schedule( 1500 ) {
			makeZoneTimer("Remaining Time", "0:5:0", "0:0:0").onCompletion( { event ->
				if( !buzzKill.isDead() ) { buzzKill.dispose(); buzzKill.clearMiniMapMarker() }
				removeZoneTimer( "Remaining Time" )
				removeZoneBroadcast( "BuzzKill!" )
				removeZoneBroadcast( "It Ran Away!" )
				removeZoneBroadcast( "BuzzKill Wants Fresh Meat!" )
				myManager.schedule(2) { zoneBroadcast( "[NPC] Old Man Logan", "Faugh! It got away!", "Well...it looks like BuzzKill escaped intact. We'll have to do better than that next time!" ) }
				myManager.schedule(15) { removeZoneBroadcast( "Faugh! It got away!" ); eventDone() }
			}).start()
		}
	}
}	

//Look through the various rooms of the zone. Try to find one with players in it. Warp BuzzKill to that location.
def findPlayers() {
	if( buzzKill.isDead() ) return
	//Look through each room and compile a list of players present in that room
	myRooms.BASSKEN_1.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_1 << it } }
	myRooms.BASSKEN_2.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_2 << it } }
	myRooms.BASSKEN_3.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_3 << it } }
	myRooms.BASSKEN_4.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_4 << it } }
	myRooms.BASSKEN_5.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_5 << it } }
	myRooms.BASSKEN_7.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_7 << it } }
	myRooms.BASSKEN_101.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_101 << it } }
	myRooms.BASSKEN_102.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_102 << it } }
	myRooms.BASSKEN_103.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_103 << it } }
	myRooms.BASSKEN_104.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_104 << it } }
	myRooms.BASSKEN_105.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_105 << it } }
	myRooms.BASSKEN_106.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_106 << it } }
	myRooms.BASSKEN_107.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_107 << it } }
	myRooms.BASSKEN_201.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_201 << it } }
	myRooms.BASSKEN_202.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_202 << it } }
	myRooms.BASSKEN_203.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_203 << it } }
	myRooms.BASSKEN_204.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_204 << it } }
	myRooms.BASSKEN_205.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_205 << it } }
	myRooms.BASSKEN_206.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_206 << it } }
	myRooms.BASSKEN_207.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_207 << it } }
	myRooms.BASSKEN_301.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_301 << it } }
	myRooms.BASSKEN_302.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_302 << it } }
	myRooms.BASSKEN_305.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_305 << it } }
	myRooms.BASSKEN_306.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_306 << it } }
	myRooms.BASSKEN_307.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_307 << it } }
	myRooms.BASSKEN_401.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_401 << it } }
	myRooms.BASSKEN_402.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_402 << it } }
	myRooms.BASSKEN_404.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_404 << it } }
	myRooms.BASSKEN_405.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_405 << it } }
	myRooms.BASSKEN_406.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_406 << it } }
	myRooms.BASSKEN_407.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_407 << it } }
	myRooms.BASSKEN_501.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_501 << it } }
	myRooms.BASSKEN_502.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_502 << it } }
	myRooms.BASSKEN_503.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_503 << it } }
	myRooms.BASSKEN_504.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_504 << it } }
	myRooms.BASSKEN_505.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_505 << it } }
	myRooms.BASSKEN_506.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_506 << it } }
	myRooms.BASSKEN_507.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_507 << it } }
	myRooms.BASSKEN_601.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_601 << it } }
	myRooms.BASSKEN_602.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_602 << it } }
	myRooms.BASSKEN_603.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_603 << it } }
	myRooms.BASSKEN_604.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_604 << it } }
	myRooms.BASSKEN_605.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_605 << it } }
	myRooms.BASSKEN_606.getActorList().clone().each { if( isPlayer( it ) && !it.isDazed() ) { BASSKEN_606 << it } }
	
	//Now determine which rooms actually have players in it and discard the empty rooms
	winnowList = listOfPlayerLists.clone()
	listOfPlayerLists.each{
		if( it.isEmpty() ) {
			winnowList.remove( it )
		}
	}
	//println "**** PLAYERS ARE IN THE FOLLOWING ROOMS: ${ winnowList } ****"
	selectRoom()
	//save the new room in the "currentRoom" variable for comparison the next time around
	currentRoom = room
	
	//Now that the room is selected, warp there!
	warpMeThere()
}

def selectRoom() {
	if( !winnowList.isEmpty() ) {
		selectedRoom = random( winnowList )
	} else {
		selectedRoom = random( listOfPlayerLists )
	}
	//ensure the room selected is a different one than current, as long as there is another room with players in it
	if( selectedRoom == currentRoom && winnowList.size() > 1 ) {
		selectRoom()
	}
	if( selectedRoom == BASSKEN_1 ) { room = "BASSKEN_1" }
	else if( selectedRoom == BASSKEN_2 ) { room = "BASSKEN_2" }
	else if( selectedRoom == BASSKEN_3 ) { room = "BASSKEN_3" }
	else if( selectedRoom == BASSKEN_4 ) { room = "BASSKEN_4" }
	else if( selectedRoom == BASSKEN_5 ) { room = "BASSKEN_5" }
	else if( selectedRoom == BASSKEN_7 ) { room = "BASSKEN_7" }
	else if( selectedRoom == BASSKEN_101 ) { room = "BASSKEN_101" }
	else if( selectedRoom == BASSKEN_102 ) { room = "BASSKEN_102" }
	else if( selectedRoom == BASSKEN_103 ) { room = "BASSKEN_103" }
	else if( selectedRoom == BASSKEN_104 ) { room = "BASSKEN_104" }
	else if( selectedRoom == BASSKEN_105 ) { room = "BASSKEN_105" }
	else if( selectedRoom == BASSKEN_106 ) { room = "BASSKEN_106" }
	else if( selectedRoom == BASSKEN_107 ) { room = "BASSKEN_107" }
	else if( selectedRoom == BASSKEN_201 ) { room = "BASSKEN_201" }
	else if( selectedRoom == BASSKEN_202 ) { room = "BASSKEN_202" }
	else if( selectedRoom == BASSKEN_203 ) { room = "BASSKEN_203" }
	else if( selectedRoom == BASSKEN_204 ) { room = "BASSKEN_204" }
	else if( selectedRoom == BASSKEN_205 ) { room = "BASSKEN_205" }
	else if( selectedRoom == BASSKEN_206 ) { room = "BASSKEN_206" }
	else if( selectedRoom == BASSKEN_207 ) { room = "BASSKEN_207" }
	else if( selectedRoom == BASSKEN_301 ) { room = "BASSKEN_301" }
	else if( selectedRoom == BASSKEN_302 ) { room = "BASSKEN_302" }
	else if( selectedRoom == BASSKEN_305 ) { room = "BASSKEN_305" }
	else if( selectedRoom == BASSKEN_306 ) { room = "BASSKEN_306" }
	else if( selectedRoom == BASSKEN_307 ) { room = "BASSKEN_307" }
	else if( selectedRoom == BASSKEN_401 ) { room = "BASSKEN_401" }
	else if( selectedRoom == BASSKEN_402 ) { room = "BASSKEN_402" }
	else if( selectedRoom == BASSKEN_404 ) { room = "BASSKEN_404" }
	else if( selectedRoom == BASSKEN_405 ) { room = "BASSKEN_405" }
	else if( selectedRoom == BASSKEN_406 ) { room = "BASSKEN_406" }
	else if( selectedRoom == BASSKEN_407 ) { room = "BASSKEN_407" }
	else if( selectedRoom == BASSKEN_501 ) { room = "BASSKEN_501" }
	else if( selectedRoom == BASSKEN_502 ) { room = "BASSKEN_502" }
	else if( selectedRoom == BASSKEN_503 ) { room = "BASSKEN_503" }
	else if( selectedRoom == BASSKEN_504 ) { room = "BASSKEN_504" }
	else if( selectedRoom == BASSKEN_505 ) { room = "BASSKEN_505" }
	else if( selectedRoom == BASSKEN_506 ) { room = "BASSKEN_506" }
	else if( selectedRoom == BASSKEN_507 ) { room = "BASSKEN_507" }
	else if( selectedRoom == BASSKEN_601 ) { room = "BASSKEN_601" }
	else if( selectedRoom == BASSKEN_602 ) { room = "BASSKEN_602" }
	else if( selectedRoom == BASSKEN_603 ) { room = "BASSKEN_603" }
	else if( selectedRoom == BASSKEN_604 ) { room = "BASSKEN_604" }
	else if( selectedRoom == BASSKEN_605 ) { room = "BASSKEN_605" }
	else if( selectedRoom == BASSKEN_606 ) { room = "BASSKEN_606" }
}
		
//======================
//WARPING ROUTINE       
//======================

//Reset the monster's home point each time it is warped so that if nothing attacks it, it can wander around that point.
//Warp the monster every 120-180 seconds.

markerInitiallySet = false
emergencyRoom = null

oldRoom = "BASSKEN_1"

coordMap = [ "BASSKEN_1" : [500, 350], "BASSKEN_2" : [500, 350], "BASSKEN_3" : [500, 350], "BASSKEN_4" : [500, 350], "BASSKEN_5" : [500, 350], "BASSKEN_7" : [500, 350], "BASSKEN_101" : [500, 350], "BASSKEN_102" : [500, 350], "BASSKEN_103" : [500, 350], "BASSKEN_104" : [500, 350], "BASSKEN_105" : [500, 350], "BASSKEN_106" : [500, 350], "BASSKEN_107" : [500, 350], "BASSKEN_201" : [500, 350], "BASSKEN_202" : [500, 350], "BASSKEN_203" : [500, 350], "BASSKEN_204" : [500, 350], "BASSKEN_205" : [500, 350], "BASSKEN_206" : [500, 350], "BASSKEN_207" : [500, 350], "BASSKEN_301" : [500, 350], "BASSKEN_302" : [500, 350], "BASSKEN_305" : [500, 350], "BASSKEN_306" : [500, 350], "BASSKEN_307" : [500, 350], "BASSKEN_401" : [500, 350], "BASSKEN_402" : [500, 350], "BASSKEN_404" : [500, 350], "BASSKEN_405" : [500, 350], "BASSKEN_406" : [500, 350], "BASSKEN_407" : [500, 350], "BASSKEN_501" : [500, 350], "BASSKEN_502" : [500, 350], "BASSKEN_503" : [500, 350], "BASSKEN_504" : [500, 350], "BASSKEN_505" : [500, 350], "BASSKEN_506" : [500, 350], "BASSKEN_507" : [500, 350], "BASSKEN_601" : [500, 350], "BASSKEN_602" : [500, 350], "BASSKEN_603" : [500, 350], "BASSKEN_604" : [500, 350], "BASSKEN_605" : [500, 350], "BASSKEN_606" : [500, 350] ]

emergencyWarpList = [ "BASSKEN_1", "BASSKEN_2", "BASSKEN_3", "BASSKEN_4", "BASSKEN_5", "BASSKEN_7", "BASSKEN_101", "BASSKEN_102", "BASSKEN_103", "BASSKEN_104", "BASSKEN_105", "BASSKEN_106", "BASSKEN_107", "BASSKEN_201", "BASSKEN_202", "BASSKEN_203", "BASSKEN_204", "BASSKEN_205", "BASSKEN_206", "BASSKEN_207", "BASSKEN_301", "BASSKEN_302", "BASSKEN_305", "BASSKEN_306", "BASSKEN_307", "BASSKEN_401", "BASSKEN_402", "BASSKEN_404", "BASSKEN_405", "BASSKEN_406", "BASSKEN_407", "BASSKEN_501", "BASSKEN_502", "BASSKEN_503", "BASSKEN_504", "BASSKEN_505", "BASSKEN_506", "BASSKEN_507", "BASSKEN_601", "BASSKEN_602", "BASSKEN_603", "BASSKEN_604", "BASSKEN_605", "BASSKEN_606" ] 

masterHateCollector = [] as Set
hateCollector = [:]

def warpMeThere() {
	if( buzzKill.isDead() ) return
	//each time the event starts up, initialize the playerVar that holds Hate for that event
	buzzKill.getHated().clone().each{
		if( !masterHateCollector.contains( it ) ) {
			it.setPlayerVar( "Z06BuzzKillCurrentEventHate", 0 )
		}
	}
	
	//Each time buzzKill gets ready to warp, preserve its Hated players within a masterHateCollector list.
	buzzKill.getHatedMap().clone().each{ player, hateAmount ->
		if( hateAmount > 1500 ) { hateAmount = 1500 }
	
		//add the hate accumulated into the current and all-time playerVars
		player.addPlayerVar( "Z06BuzzKillCurrentEventHate", hateAmount )
		
		//add the player to the master hate collector and then ensure there is only one instance of each player in that list
		masterHateCollector << player
	}
	
	//If this is an emergency warp, then use the emergencyRoom selected in the onHealth statement
	if( emergencyRoom != null ) {
		room = emergencyRoom
	}
	//now get the coords
	coords = coordMap.get( room )
	X = coords.get(0)
	Y = coords.get(1)
	
	buzzKill.clearMiniMapMarker()
	
	//Add a map marker to show BuzzKill's new position so that players can chase it.
	buzzKill.setMiniMapMarker("markerBuzzKill", "BuzzKill")

	
	//don't warp the monster if the room chosen is the same room that the monster is already within
	if( oldRoom != room ) {
	
		//Now warp the monster's home to that coordinate AND warp the monster at the same time
		buzzKill.warpHome( room, X.intValue(), Y.intValue(), 8 )


		if( !emergencyRoom == null ) {
			//nullify the emergencyRoom so normal warps can occur 10 seconds later
			//Tell the players they're doing a good job!
			myManager.schedule(2) {
				zoneBroadcast( "[NPC] Old Man Logan", "It Ran Away!", "You must be getting to it! It warped away to lick its wounds! Keep after it!" )
				myManager.schedule(5) {
					removeZoneBroadcast( "It Ran Away!" )
				}
			}
		} else {
			//Have Logan shout out that BuzzKill warped!
			zoneBroadcast( "[NPC] Old Man Logan", "BuzzKill Wants Fresh Meat!", "BuzzKill is looking for a new challenge! It's warping elsewhere in the Lake! Heads up!" )
			myManager.schedule(5) {
				removeZoneBroadcast( "BuzzKill Wants Fresh Meat!" )
			}
		}

		emergencyRoom = null
		oldRoom = room
	}

	
	//Start the countdown clock in that room to show how long BuzzKill will stay in that room
	normalWarp = myManager.schedule( random( 30, 90 ) ) { nullifyRooms() }
}


//======================
//REWARD ROUTINE        
//======================

averageHatePerPlayer = 300
maxGoldGrant = 250
maxOrbGrant = 15
desiredChanceComponentReceived = 50

def rewardPlayers() {
	masterHateCollector.clone().each() {
		//make sure it's a player (pets are coming!) and make sure that player is still online
		if( isOnline( it ) && isPlayer( it ) && !isInOtherLayer( it ) ) {
						
			//All players recieve gold relating to the amount of Hate they generate during the event
			goldGrant = (it.getPlayerVar( "Z06BuzzKillCurrentEventHate" ) * (maxGoldGrant / averageHatePerPlayer) ).intValue()
			//cap the grant at 250 gold
			if( goldGrant > maxGoldGrant ) { goldGrant = maxGoldGrant }
			if( goldGrant < 1 ) { goldGrant = 1 }
			it.grantCoins( goldGrant )
			it.centerPrint( "You earned ${goldGrant} gold!" )
			
			//grant orbs for killing BuzzKill
			orbGrant = (it.getPlayerVar( "Z06BuzzKillCurrentEventHate" ) * (maxOrbGrant / averageHatePerPlayer) ).intValue()
			if( orbGrant > maxOrbGrant ) { orbGrant = maxOrbGrant }
			if( orbGrant < 1 ) { orbGrant = 1 }
			it.grantQuantityItem( 100257, orbGrant )
			it.centerPrint( "You also earned ${orbGrant} Charge Orbs!")
			
			//Increment a playerVar (by one) that records the number of times the player has participated in this particular event
			it.addPlayerVar( "Z06BuzzKillTotalEventVisits", 1 )
			
			//Increment the "All-Time" Hate Collector with the amount of hate accumulated during this event
			thisEventHate = it.getPlayerVar( "Z06BuzzKillCurrentEventHate" )
			it.addPlayerVar( "Z06BuzzKillAllTimeEventHate", thisEventHate )
			if( thisEventHate > 1000 ) { thisEventHate = 1000 } //cap at 1000 for a single event.
			it.centerPrint( "And you earned ${thisEventHate.intValue()} points toward BuzzKill Badges!" )
			
			//Award badges for fighting BuzzKill if you did really well in this specific event
			if( it.getPlayerVar( "Z06BuzzKillCurrentEventHate" ) > averageHatePerPlayer * 1.2 ) { 
				it.updateQuest( 317, "[NPC] Old Man Logan" ) //complete the BEAT THE BUZZER badge
			}
			//... accumulated a certain amount of all-time Hate for killing BuzzKill
			if( it.getPlayerVar( "Z06BuzzKillAllTimeEventHate" ) > averageHatePerPlayer * 10 ) {
				it.updateQuest( 316, "[NPC] Old Man Logan" ) //complete the BUZZKILLER badge
			}
			/*
			//Player receive a "big monster"-specific loot item *if* the roll under a certain percentage...which is based on the amount of hate accumulated in the fights.
			roll = random( 100 ) 
			eventHate = it.getPlayerVar( "Z06BuzzKillCurrentEventHate" )
			//this multiplier is just a scaling value to adjust hate points. Figure out the average hate accumulated during the event and adjust accordingly.
			multiplier = desiredChanceComponentReceived / averageHatePerPlayer

			if( roll < (eventHate * multiplier).intValue() ) { 
				//award the event item
				it.grantItem( "100437" )
				it.centerPrint( "You find the central screw that held BuzzKill together!" )
			} else {
				it.centerPrint( "You find no specialty item this time. Try again when BuzzKill returns!" )
			}
			*/
		}
	}
}

