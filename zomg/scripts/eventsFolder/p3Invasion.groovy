//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//-----------------------------------------------------------------------------------
//Spawners                                                                           
//-----------------------------------------------------------------------------------
//Make P3s invulnerable to players over CL 4.5
def p3EventSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() < 7.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 7.0 or below to repel the P3 raiders." ); }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

patrolLeaderPalmTree = createStoppedEventSpawner("Beach_905", "patrolLeaderPalmTree", "p3_LT", 100)
patrolLeaderPalmTree.feudWithMonsterType("sand_fluff")
patrolLeaderPalmTree.feudWithMonsterType("anchor_bug")
patrolLeaderPalmTree.feudWithMonsterType("water_spout")
patrolLeaderPalmTree.feudWithMonsterType("sand_golem")
patrolLeaderPalmTree.feudWithMonsterType("sand_golem_LT")
patrolLeaderPalmTree.setMiniEventSpec(p3EventSpec)
patrolLeaderPalmTree.setPos(45, 130)
patrolLeaderPalmTree.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderPalmTree.setHomeTetherForChildren(1000)
patrolLeaderPalmTree.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderPalmTree.setMonsterLevelForChildren(6.8)

patrolLeaderLighthouse = createStoppedEventSpawner("Beach_905", "patrolLeaderLighthouse", "p3_LT", 100)
patrolLeaderLighthouse.feudWithMonsterType("sand_fluff")
patrolLeaderLighthouse.feudWithMonsterType("anchor_bug")
patrolLeaderLighthouse.feudWithMonsterType("water_spout")
patrolLeaderLighthouse.feudWithMonsterType("sand_golem")
patrolLeaderLighthouse.feudWithMonsterType("sand_golem_LT")
patrolLeaderLighthouse.setMiniEventSpec(p3EventSpec)
patrolLeaderLighthouse.setPos(45, 130)
patrolLeaderLighthouse.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderLighthouse.setHomeTetherForChildren(1000)
patrolLeaderLighthouse.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderLighthouse.setMonsterLevelForChildren(6.8)

patrolLeaderSandblast = createStoppedEventSpawner("Beach_905", "patrolLeaderSandblast", "p3_LT", 100)
patrolLeaderSandblast.feudWithMonsterType("sand_fluff")
patrolLeaderSandblast.feudWithMonsterType("anchor_bug")
patrolLeaderSandblast.feudWithMonsterType("water_spout")
patrolLeaderSandblast.feudWithMonsterType("sand_golem")
patrolLeaderSandblast.feudWithMonsterType("sand_golem_LT")
patrolLeaderSandblast.setMiniEventSpec(p3EventSpec)
patrolLeaderSandblast.setPos(45, 130)
patrolLeaderSandblast.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderSandblast.setHomeTetherForChildren(1000)
patrolLeaderSandblast.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderSandblast.setMonsterLevelForChildren(6.8)

patrolLeaderLifeguards = createStoppedEventSpawner("Beach_905", "patrolLeaderLifeguards", "p3_LT", 100)
patrolLeaderLifeguards.feudWithMonsterType("sand_fluff")
patrolLeaderLifeguards.feudWithMonsterType("anchor_bug")
patrolLeaderLifeguards.feudWithMonsterType("water_spout")
patrolLeaderLifeguards.feudWithMonsterType("sand_golem")
patrolLeaderLifeguards.feudWithMonsterType("sand_golem_LT")
patrolLeaderLifeguards.setMiniEventSpec(p3EventSpec)
patrolLeaderLifeguards.setPos(45, 130)
patrolLeaderLifeguards.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderLifeguards.setHomeTetherForChildren(1000)
patrolLeaderLifeguards.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderLifeguards.setMonsterLevelForChildren(6.8)

patrolLeaderDuneslam = createStoppedEventSpawner("Beach_905", "patrolLeaderDuneslam", "p3_LT", 100)
patrolLeaderDuneslam.feudWithMonsterType("sand_fluff")
patrolLeaderDuneslam.feudWithMonsterType("anchor_bug")
patrolLeaderDuneslam.feudWithMonsterType("water_spout")
patrolLeaderDuneslam.feudWithMonsterType("sand_golem")
patrolLeaderDuneslam.feudWithMonsterType("sand_golem_LT")
patrolLeaderDuneslam.setMiniEventSpec(p3EventSpec)
patrolLeaderDuneslam.setPos(45, 130)
patrolLeaderDuneslam.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderDuneslam.setHomeTetherForChildren(1000)
patrolLeaderDuneslam.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderDuneslam.setMonsterLevelForChildren(6.8)

patrolLeaderEmoIsland = createStoppedEventSpawner("Beach_905", "patrolLeaderEmoIsland", "p3_LT", 100)
patrolLeaderEmoIsland.feudWithMonsterType("sand_fluff")
patrolLeaderEmoIsland.feudWithMonsterType("anchor_bug")
patrolLeaderEmoIsland.feudWithMonsterType("water_spout")
patrolLeaderEmoIsland.feudWithMonsterType("sand_golem")
patrolLeaderEmoIsland.feudWithMonsterType("sand_golem_LT")
patrolLeaderEmoIsland.setMiniEventSpec(p3EventSpec)
patrolLeaderEmoIsland.setPos(45, 130)
patrolLeaderEmoIsland.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderEmoIsland.setHomeTetherForChildren(1000)
patrolLeaderEmoIsland.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderEmoIsland.setMonsterLevelForChildren(6.8)

patrolLeaderBigRock = createStoppedEventSpawner("Beach_905", "patrolLeaderBigRock", "p3_LT", 100)
patrolLeaderBigRock.feudWithMonsterType("sand_fluff")
patrolLeaderBigRock.feudWithMonsterType("anchor_bug")
patrolLeaderBigRock.feudWithMonsterType("water_spout")
patrolLeaderBigRock.feudWithMonsterType("sand_golem")
patrolLeaderBigRock.feudWithMonsterType("sand_golem_LT")
patrolLeaderBigRock.setMiniEventSpec(p3EventSpec)
patrolLeaderBigRock.setPos(45, 130)
patrolLeaderBigRock.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderBigRock.setHomeTetherForChildren(1000)
patrolLeaderBigRock.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderBigRock.setMonsterLevelForChildren(6.8)

patrolLeaderTidepool = createStoppedEventSpawner("Beach_905", "patrolLeaderTidepool", "p3_LT", 100)
patrolLeaderTidepool.feudWithMonsterType("sand_fluff")
patrolLeaderTidepool.feudWithMonsterType("anchor_bug")
patrolLeaderTidepool.feudWithMonsterType("water_spout")
patrolLeaderTidepool.feudWithMonsterType("sand_golem")
patrolLeaderTidepool.feudWithMonsterType("sand_golem_LT")
patrolLeaderTidepool.setMiniEventSpec(p3EventSpec)
patrolLeaderTidepool.setPos(45, 130)
patrolLeaderTidepool.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderTidepool.setHomeTetherForChildren(1000)
patrolLeaderTidepool.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderTidepool.setMonsterLevelForChildren(6.8)

patrolLeaderMiddle = createStoppedEventSpawner("Beach_905", "patrolLeaderMiddle", "p3_LT", 100)
patrolLeaderMiddle.feudWithMonsterType("sand_fluff")
patrolLeaderMiddle.feudWithMonsterType("anchor_bug")
patrolLeaderMiddle.feudWithMonsterType("water_spout")
patrolLeaderMiddle.feudWithMonsterType("sand_golem")
patrolLeaderMiddle.feudWithMonsterType("sand_golem_LT")
patrolLeaderMiddle.setMiniEventSpec(p3EventSpec)
patrolLeaderMiddle.setPos(45, 130)
patrolLeaderMiddle.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patrolLeaderMiddle.setHomeTetherForChildren(1000)
patrolLeaderMiddle.setSpawnWhenPlayersAreInRoom(true)
patrolLeaderMiddle.setMonsterLevelForChildren(6.8)

patroller = createStoppedEventSpawner("Beach_905", "patroller01a", "p3", 100)
patroller.feudWithMonsterType("sand_fluff")
patroller.feudWithMonsterType("anchor_bug")
patroller.feudWithMonsterType("water_spout")
patroller.feudWithMonsterType("sand_golem")
patroller.feudWithMonsterType("sand_golem_LT")
patroller.setMiniEventSpec(p3EventSpec)
patroller.setPos(45, 130)
patroller.setWanderBehaviorForChildren(25, 75, 5, 10, 150)
patroller.setHomeTetherForChildren(1000)
patroller.setSpawnWhenPlayersAreInRoom(true)
patroller.setMonsterLevelForChildren(6.8)

//-----------------------------------------------------------------------------------
//Alliances                                                                          
//-----------------------------------------------------------------------------------
patrolLeaderPalmTree.allyWithSpawner(patroller)
patrolLeaderLighthouse.allyWithSpawner(patroller)
patrolLeaderSandblast.allyWithSpawner(patroller)
patrolLeaderLifeguards.allyWithSpawner(patroller)
patrolLeaderDuneslam.allyWithSpawner(patroller)
patrolLeaderEmoIsland.allyWithSpawner(patroller)
patrolLeaderBigRock.allyWithSpawner(patroller)
patrolLeaderTidepool.allyWithSpawner(patroller)
patrolLeaderMiddle.allyWithSpawner(patroller)

//-----------------------------------------------------------------------------------
//Maps and Lists for Spawn Info                                                      
//-----------------------------------------------------------------------------------
palmTreeMap = [1:"Beach_803", 2:700, 3:250, 4:patrolLeaderPalmTree]
lighthouseMap = [1:"Beach_202", 2:270, 3:160, 4:patrolLeaderLighthouse]
sandblastMap = [1:"Beach_602", 2:550, 3:330, 4:patrolLeaderSandblast]
lifeguardsMap = [1:"Beach_1004", 2:840, 3:340, 4:patrolLeaderLifeguards]
duneslamMap = [1:"Beach_604", 2:230, 3:350, 4:patrolLeaderDuneslam]
emoIslandMap = [1:"Beach_204", 2:700, 3:400, 4:patrolLeaderEmoIsland]
bigRockMap = [1:"Beach_103", 2:200, 3:300, 4:patrolLeaderBigRock]
tidepoolMap = [1:"Beach_303", 2:930, 3:400, 4:patrolLeaderTidepool]
middleMap = [1:"Beach_503", 2:650, 3:400, 4:patrolLeaderMiddle]

spawnLocations = [palmTreeMap, lighthouseMap, sandblastMap, lifeguardsMap, duneslamMap, emoIslandMap, bigRockMap, tidepoolMap, middleMap]
spawnMap = [(patrolLeaderPalmTree):palmTreeMap, (patrolLeaderLighthouse):lighthouseMap, (patrolLeaderSandblast):sandblastMap, (patrolLeaderLifeguards):lifeguardsMap, (patrolLeaderDuneslam):duneslamMap, (patrolLeaderEmoIsland):emoIslandMap, (patrolLeaderBigRock):bigRockMap, (patrolLeaderTidepool):tidepoolMap, (patrolLeaderMiddle):middleMap]
attackLocations = [palmTreeMap, lighthouseMap, sandblastMap, lifeguardsMap, duneslamMap, emoIslandMap, bigRockMap, tidepoolMap, middleMap]
spawnerList = [patrolLeaderPalmTree, patrolLeaderLighthouse, patrolLeaderSandblast, patrolLeaderLifeguards, patrolLeaderDuneslam, patrolLeaderEmoIsland, patrolLeaderBigRock, patrolLeaderTidepool, patrolLeaderMiddle, patroller]

p3SpawnedList = []
p3KillerList = []
p3List = []
p3Map = new HashMap()
p3Counter = 0
spawnTime = true
numberOfPlayers = 0
p3HuntersKilled = 0

//-----------------------------------------------------------------------------------
//Spawning Logic                                                                     
//-----------------------------------------------------------------------------------
def startEvent() {
	println "#### Starting P3 Hunt ####"
	sound("p3InvasionEventStart").toZone()
	myManager.schedule(2) {
		p3Counter = 0
		makeZoneCounter("P3 Hunters Repelled", 0).watchForKill("p3", "p3_LT").setGoal(100).onCompletion({ completeEvent() })
		zoneBroadcast("Jacques-VQS", "P3 Punisher", "Yar, look lively! Thar be Predators huntin' upon the Beach. Raidin' parties be swarmin' all over. Ye better do somethin' about it!")
		myManager.schedule(30) { 
			spawnCheck()
			makeZoneTimer("P3 Hunt", "0:20:0", "0:0:0").onCompletion({ event -> 
				spawnTime = false
				myManager.schedule(1) {
					removeZoneTimer("P3 Hunt")
					removeZoneCounter("P3 Hunters Repelled")
					removeZoneBroadcast("P3 Punisher")
					zoneBroadcast("Jacques-VQS", "Failure!", "Avast! Woe be upon ye! The Predators 'ave o'er run the Beach and taken the prey they wished. Ye all 'ave failed me!")
					//println "#### p3SpawnedList = ${p3SpawnedList} ####"
					p3SpawnedList.clone().each() {
						//println "#### disposing ${it} ####"
						it?.dispose()
					}
					if(p3KillerList.size() > 0) {
						println "#*#*#* BEGIN P3 PRINTOUT *#*#*#"
						p3KillerList.clone().each() {
							numberOfPlayers++
							p3HuntersKilled = p3HuntersKilled + it.getPlayerVar("Z09_P3AttackersCaught").intValue()
							println "#*#*#* PlayerID = ${it} *#*#*#"
							println "#*#*#* P3s killed this event = ${it.getPlayerVar("Z09_P3AttackersCaught")} *#*#*#"
							println "#*#*#* P3s killed all time = ${it.getPlayerVar("Z09_P3AttackersCaughtTotal")} *#*#*#"
						}
						if(numberOfPlayers > 0 ) { println "*#*#*# Average = ${p3HuntersKilled / numberOfPlayers} *#*#*#" }
						println "*#*#*# END P3 PRINTOUT *#*#*#"
					}
					myManager.schedule(15) { removeZoneBroadcast("Failure!"); eventDone() }
				}
			}).start() 
		} 
	}
}

def completeEvent() {
	//println "p3SpawnedList = ${p3SpawnedList} ####"
	spawnTime = false
	myManager.schedule(1) {
		removeZoneTimer("P3 Hunt")
		removeZoneCounter("P3 Hunters Repelled")
		removeZoneBroadcast("P3 Punisher")
		p3SpawnedList.clone().each() {
			//println "#### disposing ${it} ####"
			it?.dispose()
		}
		//println "#### p3SpawnedList = ${p3SpawnedList} ####"
	
		zoneBroadcast("Jacques-VQS", "P3wned", "Yar, that oughta teach those vile Predators! Methinks they shant be usin' this beach as a huntin' grounds again anytime soon.")

		if(p3KillerList.size() > 0) {
			println "#*#*#* BEGIN P3 PRINTOUT *#*#*#"
			println "#*#*#* p3KillerList = ${p3KillerList} *#*#*#"
			p3KillerList.clone().each() {
				if( it != null && isOnline(it) && !isInOtherLayer(it)) {
					numberOfPlayers++
					p3HuntersKilled = p3HuntersKilled + it.getPlayerVar("Z09_P3AttackersCaught").intValue()
					println "#*#*#* PlayerID = ${it} *#*#*#"
					println "#*#*#* P3s killed this event = ${it.getPlayerVar("Z09_P3AttackersCaught")} *#*#*#"
					println "#*#*#* P3s killed all time = ${it.getPlayerVar("Z09_P3AttackersCaughtTotal")} *#*#*#"
					if(it.getPlayerVar("Z09_P3AttackersCaught") > 0 && it.getPlayerVar("Z09_P3AttackersCaught") < 10) { 
						tinyReward = random(2, 5)
						goldReward = random(10, 20)

						it.centerPrint("You killed ${it.getPlayerVar("Z09_P3AttackersCaught").intValue()} P3 raiders and earned a tiny reward.")
						it.centerPrint("You receive ${goldReward} gold and ${tinyReward} orbs.")
						it.grantQuantityItem(100257, tinyReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 99) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z09_P3AttackersCaught") >= 10 && it.getPlayerVar("Z09_P3AttackersCaught") < 35) {
						moderateReward = random(5, 9)
						goldReward = random(20, 50)

						it.centerPrint("You killed ${it.getPlayerVar("Z09_P3AttackersCaught").intValue()} P3 raiders and earned a moderate reward.")
						it.centerPrint("You receive ${goldReward} gold and ${moderateReward} orbs.")
						it.grantQuantityItem(100257, moderateReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 80) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z09_P3AttackersCaught") >= 35 && it.getPlayerVar("Z09_P3AttackersCaught") < 60) {
						largeReward = random(10, 15)
						goldReward = random(50, 70)

						it.centerPrint("You killed ${it.getPlayerVar("Z09_P3AttackersCaught").intValue()} P3 raiders and earned a large reward.")
						it.centerPrint("You receive ${goldReward} gold and ${largeReward} orbs.")
						it.grantQuantityItem(100257, largeReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 65) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					} else if(it.getPlayerVar("Z09_P3AttackersCaught") >= 60) {
						massiveReward = random(25, 30)
						goldReward = random(100, 120)

						it.centerPrint("You killed ${it.getPlayerVar("Z09_P3AttackersCaught").intValue()} P3 raiders and earned a massive reward.")
						it.centerPrint("You receive ${goldReward} gold and ${massiveReward} orbs.")
						it.grantQuantityItem(100257, massiveReward)
						it.grantCoins(goldReward)
						lootChance = random(100)
						/*if(lootChance > 0) {
							it.grantItem("100443")
							it.centerPrint("You have received a Power Cell as an additional reward!")
						}*/
					}
					if(it.getPlayerVar("Z09_P3AttackersCaughtTotal") >= 100 && !it.isDoneQuest(322)) {
						it.centerPrint("You have been awarded the Hunt Harasser badge.")
						it.updateQuest(322, "Jacques-VQS")
					}
					if(it.getPlayerVar("Z09_P3AttackersCaughtTotal") >= 1000 && !it.isDoneQuest(323)) {
						it.centerPrint("You have been awarded the P3TA badge.")
						it.updateQuest(323, "Jacques-VQS")
					}
				}
			}
		}
		if(numberOfPlayers > 0) { println "*#*#*# Average = ${p3HuntersKilled / numberOfPlayers} *#*#*#" }
		println "*#*#*# END P3 PRINTOUT *#*#*#"
	
		myManager.schedule(15) { removeZoneBroadcast("P3wned"); eventDone() }
	}
}

def spawnCheck() {
	time = gst()
	if(spawnTime == true && p3Counter <= 97) { //if it's night and there are patrols to spawn, spawn one
		locationChange()
		if(spawnLocations.size() > 0) {
			spawnRandom()
		} else {
			myManager.schedule(10) { spawnCheck() }
		}
	}
}

def spawnRandom() {
	//println "#### spawnLocations = ${spawnLocations} ####"
	p3Counter++
	randomizeML()
	myManager.schedule(20) { spawnCheck() }
	spawnLocation = random(spawnLocations) //select a spawn location from the list of locations
	spawnLocations.remove(spawnLocation)
	//println "#### spawnLocation = ${spawnLocation} ####"
	
	//Get the correct spawner
	leaderSpawner = spawnLocation.get(4)
	//println "#### leaderSpawner = ${leaderSpawner} ####"
	
	//Get room name and X, Y for spawn
	spawnRoom = spawnLocation.get(1)
	spawnX = spawnLocation.get(2)
	spawnY = spawnLocation.get(3)
	
	//Warp the spawners to the desired location
	leaderSpawner.warp(spawnRoom.toString(), spawnX, spawnY)
	patroller.warp(spawnRoom.toString(), spawnX, spawnY)
	
	myManager.schedule(3) {
		//Spawn the leader of the patrol and add it to list of clutches
		leader = leaderSpawner.forceSpawnNow()
		leader.setDisplayName("P3 Hunter")
		p3SpawnedList << leader
		p3Map.put(leader, leaderSpawner)
		//println "#### Spawned a P3, it was ${leader} ####"
		
		//println "#### p3Map keys = ${p3Map.keySet()} ####"
		//println "#### p3Map values = ${p3Map.values()} ####"
		
		attackLocation = random(attackLocations)
		
		//pull room and x/y locations for attack room
		attackRoom = attackLocation.get(1)
		attackX = attackLocation.get(2)
		attackY = attackLocation.get(3)
		
		runOnDeath(leader) { event ->
			//println "#### ${spawnMap.get(p3Map.get(event.actor).toString())} ####"
			//println "#### ${p3Map.get(event.actor)} ####"
			spawnLocations << spawnMap.get(p3Map.get(event.actor))
			p3SpawnedList.remove(event.actor)
			p3Map.remove(event.actor)
			event.actor.getHated().each() {
				if(isPlayer(it)) {
					if(!p3KillerList.contains(it)) { p3KillerList << it; it.setPlayerVar("Z09_P3AttackersCaught", 0) }
					it.addToPlayerVar("Z09_P3AttackersCaught", 1)
					it.addToPlayerVar("Z09_P3AttackersCaughtTotal", 1)
				}
			}
		}
	
	
		//spawn the first follower	
		patroller1 = patroller.forceSpawnNow()
		patroller1.setDisplayName("P3 Tracker")
		patroller1.startFollow(leader)
		p3SpawnedList << patroller1
		//println "#### Spawned a P3, it was ${patroller1} ####"
		
		runOnDeath(patroller1) { event ->
			p3SpawnedList.remove(event.actor)
			event.actor.getHated().each() {
				if(isPlayer(it)) {
					if(!p3KillerList.contains(it)) { p3KillerList << it; it.setPlayerVar("Z09_P3AttackersCaught", 0) }
					it.addToPlayerVar("Z09_P3AttackersCaught", 1)
					it.addToPlayerVar("Z09_P3AttackersCaughtTotal", 1)
				}
			}
		}
	
		//spawn the second follower
		patroller2 = patroller.forceSpawnNow()
		patroller2.setDisplayName("P3 Tracker")
		patroller2.startFollow(leader)
		p3SpawnedList << patroller2
		//println "#### Spawned a P3, it was ${patroller2} ####"
		
		runOnDeath(patroller2) { event ->
			p3SpawnedList.remove(event.actor)
			event.actor.getHated().each() {
				if(isPlayer(it)) {
					if(!p3KillerList.contains(it)) { p3KillerList << it; it.setPlayerVar("Z09_P3AttackersCaught", 0) } //If the player isn't in the killer list, add them and reset their caught playerVar to 0
					it.addToPlayerVar("Z09_P3AttackersCaught", 1)
					it.addToPlayerVar("Z09_P3AttackersCaughtTotal", 1)
				}
			}
		}
		leaderSpawner.setHomeForEveryone(attackRoom.toString(), attackX, attackY)
	}
}

def locationChange() {
	p3Map.keySet().each() {
		//Selects a random location for the next attack run
		attackLocation = random(attackLocations)
		
		//pull room and x/y locations for attack room
		attackRoom = attackLocation.get(1)
		attackX = attackLocation.get(2)
		attackY = attackLocation.get(3)
		
		p3Map.get(it).setHomeForEveryone(attackRoom.toString(), attackX, attackY)
		//println "#### Set home for ${it} (spawner: ${p3Map.get(it)}) to ${attackRoom.toString()}, ${attackX}, ${attackY} ####"
	}
}

def randomizeML() {
	spawnerList.clone().each() {
		randomVariance = random(0,5)/100
		//println "#### randomVariance = ${randomVariance} ####"
		monsterLevel = 6.8 + randomVariance
		//println "#### monsterLevel = ${monsterLevel} ####"
		it.setMonsterLevelForChildren(monsterLevel)
	}
}

startEvent()
