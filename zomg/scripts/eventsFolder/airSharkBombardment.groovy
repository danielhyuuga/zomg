import com.gaiaonline.mmo.battle.script.*;
import com.gaiaonline.mmo.battle.ring.PointTarget

ML = 1.5
skyBoxLevelLimit = 2.1

//TODO: Set this scenario up so that after a certain number of Sky Boxes destroyed, AirShark is driven off. (kill count)

//=====================================
//SPAWNERS AND INVULNERABILITY ROUTINE 
//=====================================

//Make AirShark invulnerable
def airSharkSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() >= 100
		if( player == attacker && !allowed ) { player.centerPrint( "Your attack bounces off the AirShark with no visible effect." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

airSharkSpawner = createStoppedEventSpawner( "BARTON_202", "airSharkSpawner", "airshark", 1 )
airSharkSpawner.setMiniEventSpec( airSharkSpec )
airSharkSpawner.setPos( 1260, 940 )
airSharkSpawner.setRotationForChildren( 225 )
//airSharkSpawner.setInitialMoveForChildren( "BARTON_202", 740, 450 )
airSharkSpawner.setMonsterLevelForChildren( 2.0 )
airSharkSpawner.setEdgeHinting( false )
airSharkSpawner.setTargetCycle( false )

//Now make the bombs invulnerable 
def bombSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= skyBoxLevelLimit
		if( player == attacker && !allowed ) { player.centerPrint( "You must change your level to 2.0 or lower to help defend against AirShark!" ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

bombSpawner = createStoppedEventSpawner( "BARTON_202", "bombSpawner", "airshark_bombs", 200 )
bombSpawner.setMiniEventSpec( bombSpec )
bombSpawner.setPos( 640, 520 )
bombSpawner.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
bombSpawner.setMonsterLevelForChildren( ML )

//======================================
//MOVEMENT & BOMBING ROUTINE            
//======================================

//figure eight
figureEight = makeNewPatrol()
figureEight.addPatrolPoint( "BARTON_202", 640, 520, 0)
figureEight.addPatrolPoint( "BARTON_202", 200, 200, 0)
figureEight.addPatrolPoint( "BARTON_102", 200, 400, 0)
figureEight.addPatrolPoint( "BARTON_2", 490, 760, 0)
figureEight.addPatrolPoint( "BARTON_2", 1270, 700, 0)
figureEight.addPatrolPoint( "BARTON_3", 230, 700, 0)
figureEight.addPatrolPoint( "BARTON_103", 550, 660, 0)
figureEight.addPatrolPoint( "BARTON_203", 1080, 370, 0)
figureEight.addPatrolPoint( "BARTON_203", 1360, 810, 0)
figureEight.addPatrolPoint( "BARTON_204", 530, 550, 0)
figureEight.addPatrolPoint( "BARTON_104", 920, 600, 0)
figureEight.addPatrolPoint( "BARTON_104", 400, 320, 0)
figureEight.addPatrolPoint( "BARTON_103", 1060, 380, 0)
figureEight.addPatrolPoint( "BARTON_103", 550, 660, 0)
figureEight.addPatrolPoint( "BARTON_102", 1420, 820, 0)
figureEight.addPatrolPoint( "BARTON_202", 720, 500, 0)

//perimeter circle
perimeterCircle = makeNewPatrol()
perimeterCircle.addPatrolPoint( "BARTON_202", 640, 520, 0)
perimeterCircle.addPatrolPoint( "BARTON_202", 200, 200, 0)
perimeterCircle.addPatrolPoint( "BARTON_102", 200, 400, 0)
perimeterCircle.addPatrolPoint( "BARTON_2", 490, 760, 0)
perimeterCircle.addPatrolPoint( "BARTON_2", 1270, 700, 0)
perimeterCircle.addPatrolPoint( "BARTON_3", 1000, 780, 0)
perimeterCircle.addPatrolPoint( "BARTON_103", 1280, 200, 0)
perimeterCircle.addPatrolPoint( "BARTON_104", 760, 350, 0)
perimeterCircle.addPatrolPoint( "BARTON_104", 1030, 600, 0)
perimeterCircle.addPatrolPoint( "BARTON_104", 850, 900, 0)
perimeterCircle.addPatrolPoint( "BARTON_204", 420, 570, 0)
perimeterCircle.addPatrolPoint( "BARTON_203", 900, 900, 0)
perimeterCircle.addPatrolPoint( "BARTON_303", 460, 370, 0)
perimeterCircle.addPatrolPoint( "BARTON_302", 900, 250, 0)
perimeterCircle.addPatrolPoint( "BARTON_202", 920, 700, 0)
perimeterCircle.addPatrolPoint( "BARTON_202", 720, 500, 0)


//======================================
// AIRSHARK TRIGGER ZONES               
//======================================


//pathDecisionTrigger (room 202)
def pathDecisionTrigger = "pathDecisionTrigger"
myRooms.BARTON_202.createTriggerZone( pathDecisionTrigger, 520, 240, 930, 700 )

myManager.onTriggerIn( myRooms.BARTON_202, pathDecisionTrigger ) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		roll = random(1, 2)
		if( roll == 1 ) {
			airShark.setPatrol( figureEight )
			path = "figureEight"
		} else {
			airShark.setPatrol( perimeterCircle )
			path = "perimeter"
		}
		airShark.startPatrol()
	}
}


//Trigger zone logic set up so triggers are active when certain paths are active...but not when others are (to allow bomb runs to correctly occur).

def bomb202a = "bomb202a"
myRooms.BARTON_202.createTriggerZone( bomb202a, 550, 900, 970, 1000 )

myManager.onTriggerIn(myRooms.BARTON_202, bomb202a) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar202a" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_202.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb202b = "bomb202b"
myRooms.BARTON_202.createTriggerZone( bomb202b, 740, 90, 1100, 360 )

myManager.onTriggerIn(myRooms.BARTON_202, bomb202b) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar202b" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_202.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb102 = "bomb102"
myRooms.BARTON_102.createTriggerZone( bomb102, 30, 870, 460, 990 )

myManager.onTriggerIn(myRooms.BARTON_102, bomb102) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar102" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_102.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb2 = "bomb2"
myRooms.BARTON_2.createTriggerZone( bomb2, 230, 710, 730, 960 )

myManager.onTriggerIn(myRooms.BARTON_2, bomb2) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar2" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_2.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb3 = "bomb3"
myRooms.BARTON_3.createTriggerZone( bomb3, 30, 500, 210, 770 )

myManager.onTriggerIn(myRooms.BARTON_3, bomb3) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar3" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_3.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb103Perimeter = "bomb103Perimeter"
myRooms.BARTON_103.createTriggerZone( bomb103Perimeter, 870, 50, 1250, 120 )

myManager.onTriggerIn(myRooms.BARTON_103, bomb103Perimeter) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar103perimeter" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_103.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb103FigureEightA = "bomb103FigureEightA"
myRooms.BARTON_103.createTriggerZone( bomb103FigureEightA, 170, 40, 550, 170 )

myManager.onTriggerIn(myRooms.BARTON_103, bomb103FigureEightA) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar103figureA" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_103.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb103FigureEightB = "bomb103FigureEightB"
myRooms.BARTON_103.createTriggerZone( bomb103FigureEightB, 1350, 140, 1500, 430 )

myManager.onTriggerIn(myRooms.BARTON_103, bomb103FigureEightB) { event ->
	if( isMonster( event.actor ) && event.actor == airShark && path == "figureEight" ) {
		sound( "roar103figureB" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_103.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb104Perimeter = "bomb104Perimeter"
myRooms.BARTON_104.createTriggerZone( bomb104Perimeter, 70, 230, 370, 530 )

myManager.onTriggerIn(myRooms.BARTON_104, bomb104Perimeter) { event ->
	if( isMonster( event.actor ) && event.actor == airShark && path == "perimeter" ) {
		sound( "roar104perimeter" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_104.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb104FigureEight = "bomb104FigureEight"
myRooms.BARTON_104.createTriggerZone( bomb104FigureEight, 530, 890, 1050, 1000 )

myManager.onTriggerIn(myRooms.BARTON_104, bomb104FigureEight) { event ->
	if( isMonster( event.actor ) && event.actor == airShark && path == "figureEight" ) {
		sound( "roar104figure" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_104.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb204Perimeter = "bomb204Perimeter"
myRooms.BARTON_204.createTriggerZone( bomb204Perimeter, 500, 10, 1010, 150 )

myManager.onTriggerIn(myRooms.BARTON_204, bomb204Perimeter) { event ->
	if( isMonster( event.actor ) && event.actor == airShark && path == "perimeter" ) {
		sound( "roar204perimeter" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_204.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb204FigureEight = "bomb204FigureEight"
myRooms.BARTON_204.createTriggerZone( bomb204FigureEight, 10, 550, 175, 990 )

myManager.onTriggerIn(myRooms.BARTON_204, bomb204FigureEight) { event ->
	if( isMonster( event.actor ) && event.actor == airShark && path == "figureEight" ) {
		sound( "roar204figure" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_204.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb203Perimeter = "bomb203Perimeter"
myRooms.BARTON_203.createTriggerZone( bomb203Perimeter, 1410, 530, 1550, 980 )

myManager.onTriggerIn(myRooms.BARTON_203, bomb203Perimeter) { event ->
	if( isMonster( event.actor ) && event.actor == airShark && path == "perimeter" ) {
		sound( "roar203perimeter" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_203.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb203FigureEight = "bomb203FigureEight"
myRooms.BARTON_203.createTriggerZone( bomb203FigureEight, 590, 10, 1190, 130 )

myManager.onTriggerIn(myRooms.BARTON_203, bomb203FigureEight) { event ->
	if( isMonster( event.actor ) && event.actor == airShark && path == "figureEight" ) {
		sound( "roar203figure" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_203.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb303Perimeter = "bomb303Perimeter"
myRooms.BARTON_303.createTriggerZone( bomb303Perimeter, 710, 10, 1370, 200 )

myManager.onTriggerIn(myRooms.BARTON_303, bomb303Perimeter) { event ->
	if( isMonster( event.actor ) && event.actor == airShark ) {
		sound( "roar303perimeter" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_303.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

def bomb302Perimeter = "bomb302Perimeter"
myRooms.BARTON_302.createTriggerZone( bomb302Perimeter, 1350, 10, 1540, 410 )

myManager.onTriggerIn(myRooms.BARTON_302, bomb302Perimeter) { event ->
	if( isMonster( event.actor ) && event.actor == airShark  ) {
		sound( "roar302perimeter" ).toZone()
		//delay the bomb drop to increase position uncertainty.
		myManager.schedule( random( 1, 3 ) ) {
			dropBomb( event )
			if( myRooms.BARTON_302.getMobTypeCount( "airshark_bombs" ) == 0 && playerCheckList.size() > 0 ) { myManager.schedule(2) { numSpawn = random( 2, 5 ); spawnBombSquad() } }
		}
	}
}

taunt = [ "I got a present for ya!", "Gifts from above!", "For Sam!", "La, La, La, La, La, La-BOMB-ba!", "Look out below!", "When you care enough to send the very best!" ]


playerCheckList = [] 

//======================================
// BOMB ROUTINES                        
//======================================
//When reaching a trigger zone, a bomb is dropped which bursts into a spawn of Grunnies...but *only* if players are in the room
def dropBomb( event ) {
	//figure out the bomb drop location
	dropRoom = event.actor.getRoom()
	dropX = event.actor.getX()
	dropY = event.actor.getY()

	playerCheckList.clear()
	//check all the actors in the room to see if any players are in the room
	dropRoom.getActorList().clone().each { if( isPlayer( it ) ) { playerCheckList << it } }
	
	//if the playerCheckList isn't empty, then players *are* present, and a bomb should be dropped
	if( playerCheckList.size() > 0 ) {

		target = makePointTarget( dropRoom, dropX.intValue(), dropY.intValue() )

		//say something smarmy when the bomb drops
		airShark.say( random( taunt ) )

		//check to see if there is anyone in the room. If not, then skip it so grunnies are not spawned
		airShark.attackTarget( target, 0 )
	}
}

skyBoxSet = [] as Set

def spawnBombSquad() {
	if( numSpawn > 0 ) {
		//the next bomb spawned will be between ML 1.6 and 2.5
		ML = 1.3 + ( random( 0, 3 ) * 0.1 )
		
		//do the proper offset for each spawn so they appear around the center point in a star pattern
		//warp the spawner to each offset location in turn
		if( numSpawn == 5 ) {
			//tip of star
			bombSpawner.warp( dropRoom.toString(), (dropX + 0).intValue(), (dropY + 200).intValue() )
			bombSpawner.setHomeForChildren( dropRoom.toString(), (dropX + 0).intValue(), (dropY + 200).intValue() )
		} else if( numSpawn == 4 ) {
			//right wing
			bombSpawner.warp( dropRoom.toString(), (dropX + 150).intValue(), (dropY + 50).intValue() )
			bombSpawner.setHomeForChildren( dropRoom.toString(), (dropX + 150).intValue(), (dropY + 50).intValue() )
		} else if( numSpawn == 3 ) {
			//left foot
			bombSpawner.warp( dropRoom.toString(), (dropX - 75).intValue(), (dropY - 50).intValue() )
			bombSpawner.setHomeForChildren( dropRoom.toString(), (dropX - 75).intValue(), (dropY - 50).intValue() )
		} else if( numSpawn == 2 ) {
			//left wing
			bombSpawner.warp( dropRoom.toString(), (dropX - 150).intValue(), (dropY + 50).intValue() )
			bombSpawner.setHomeForChildren( dropRoom.toString(), (dropX - 150).intValue(), (dropY + 50).intValue() )
		} else if( numSpawn == 1 ) {
			//right foot
			bombSpawner.warp( dropRoom.toString(), (dropX + 75).intValue(), (dropY - 50).intValue() )
			bombSpawner.setHomeForChildren( dropRoom.toString(), (dropX + 75).intValue(), (dropY - 50).intValue() )
		}
		f1 = bombSpawner.forceSpawnNow()
		f1.setDisplayName( "Sky Box" )
		skyBoxSet << f1
		//println "**** skyBoxSet = ${skyBoxSet} ****"
		runOnDeath( f1, { event ->
			//figure out everyone hated by the bomb at the time of death
			//then add that to a master hate list so we know everyone that partcipated in the event			
			
			//NOTE: use this later when we have a "stats list" function or for badges
			event.actor.getHated().each{
				//reset the players eventHate collector if this is the first time they've killed a Sky Box in this event
				if( !masterHateCollector.contains( it ) ) {
					it.setPlayerVar( "Z01AirSharkEventCurrentHate", 0 )
				}
				masterHateCollector << it
			}
		
			event.actor.getHatedMap().each() { player, bombHate ->
				if( bombHate > 70 ) { bombHate = 70 }
				if( gameEnded == false ) {
					//Add the hate generated by a player during the fight to their playerVar that stores the total FOR THIS EVENT ONLY
					player.addPlayerVar( "Z01AirSharkEventCurrentHate", bombHate )
					player.centerPrint( "You earned ${bombHate} points for helping fight that Sky Box." )
					//this line is for data checking while running the event
					totalHateCollected = totalHateCollected + bombHate
				}
				
			}
			
			//remove the sky box from the "skyBoxSet" so we have a list of surviving skyboxes for the end of the scenario (cleanup)
			skyBoxSet.remove( event.actor )
		} )
		//decrement the counter and keep spawning until they're all spawned
		numSpawn --
		spawnBombSquad()
	}
}


//======================
// START EVENT          
//======================

bombHate = 0
eventHate = 0
totalHate = 0
averageHate = 0
totalHateCollected = 0
stopClaxon = false
gameEnded = false

masterHateCollector = [] as Set

myManager.schedule(3) { startEventTimer() }

//Let the event run for 15 minutes
def startEventTimer() {
	sound( "airSharkEventStart" ).toZone()
	
	myManager.schedule(2) { 
		zoneBroadcast("Rina-VQS", "AirShark Attack!", "Oh, schnapps! This big shark thingee flew down from the clouds and is dropping bombs around Barton Town! HELP!!!")
		airShark = airSharkSpawner.forceSpawnNow()
		airShark.setDisplayCL( 20 )
		airShark.setDisplayName( "AirShark" )
		//airShark.setMiniMapMarker("markerQuest", "AirShark") 
		path = "perimeter"
		airShark.setPatrol( perimeterCircle )
		airShark.startPatrol()
		myManager.schedule(2){
			townAlarms()
			makeZoneCounter( "Destroy the Sky Boxes!", 0).setGoal(200).watchForKill("airshark_bombs").onCompletion( { event ->
				timeExpired = false
				endGame()
			} )
		}
	}
	//20-minute timer until the event ends
	makeZoneTimer("Bombardment Timer", "0:20:0", "0:0:0").onCompletion( { event ->
		timeExpired = true
		endGame()		
	} ).start()
}

def endGame() {
	gameEnded = true
	removeZoneBroadcast( "AirShark Attack!" )
	removeZoneTimer( "Bombardment Timer" )
	removeZoneCounter( "Destroy the Sky Boxes!" )
	//airShark.clearMiniMapMarker()
	//get rid of airShark
	airShark.dispose()
	stopClaxon = true
	
	skyBoxLevelLimit = 100

	//clean up the remaining Skyboxes from around town
	if( !skyBoxSet.isEmpty() ) {
		//println "**** skyBoxSet = ${skyBoxSet} ****"
		skyBoxSet.clone().each{
			if( !it.isDead() ) {
				it.instantPercentDamage( 100 ) 
			}
		}
	}
	//double-check that all sky boxes are removed from town
	boxDisposal()
	
	myManager.schedule(2) { 
		if( timeExpired == true ) {
			zoneBroadcast("Rina-VQS", "Sound the All Clear!", "It looks like the AirShark got tired of bombing us and has flown back up into the sky! We're safe...for now!")
			myManager.schedule( 15 ) {
				removeZoneBroadcast( "Sound the All Clear!" )
				eventDone()
			}
		} else {
			zoneBroadcast("Rina-VQS", "We Drove it Off!", "You did it! You defeated so many of the Sky Boxes that you drove AirShark away! SQUEEERIFIC!")
			myManager.schedule( 15 ) {
				removeZoneBroadcast( "We Drove it Off!" )
				eventDone()
			}
		}

		if( !masterHateCollector.isEmpty() ) {
			//print out some data mining so we can look in logs during tests or live events
			println "************************************************"
			println "********** EVENT HATE SUMMARY ***************"
			println "**********       (BARTON)     ***************"
			println "**** The Player Hate Map for this event is: ****"
			println ""
			println "masterHateCollector = ${masterHateCollector}"
			println ""
			println "totalHateCollected = ${ totalHateCollected }"
			println ""
			println "numPeople in Event = ${masterHateCollector.size()} ****"
			println ""
			println "average Hate per person = ${ totalHateCollected / masterHateCollector.size() }"
			println ""
			println "********** EVENT HATE SUMMARY ***************"
			println "************************************************"

			rewardPlayers()
		}
	}
}

//Dispose of the Sky Boxes after the countdown is complete
def boxDisposal(){
	myRooms.BARTON_1.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_2.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_3.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_4.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_101.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_102.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_104.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_201.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_301.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_302.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
	myRooms.BARTON_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "airshark_bombs" ) { it.instantPercentDamage( 100 )  } }
}


def townAlarms() {
	if( stopClaxon == false ) {
		sound( "sharkAlarm1" ).toZone()
		sound( "sharkAlarm2" ).toZone()
		sound( "sharkAlarm3" ).toZone()
		sound( "sharkAlarm4" ).toZone()
		sound( "sharkAlarm5" ).toZone()
		sound( "sharkAlarm6" ).toZone()
		sound( "sharkAlarm7" ).toZone()
		sound( "sharkAlarm8" ).toZone()
		sound( "sharkAlarm9" ).toZone()
		sound( "sharkAlarm10" ).toZone()
		myManager.schedule(12) {
			stopSound( "sharkAlarm1" ).toZone()
			stopSound( "sharkAlarm2" ).toZone()
			stopSound( "sharkAlarm3" ).toZone()
			stopSound( "sharkAlarm4" ).toZone()
			stopSound( "sharkAlarm5" ).toZone()
			stopSound( "sharkAlarm6" ).toZone()
			stopSound( "sharkAlarm7" ).toZone()
			stopSound( "sharkAlarm8" ).toZone()
			stopSound( "sharkAlarm9" ).toZone()
			stopSound( "sharkAlarm10" ).toZone()
			myManager.schedule(48) { 
				townAlarms()
			}
		}
	}
}


//======================
//REWARD ROUTINE        
//======================
//After he leaves, reward everyone that accumulated hate, based on how much hate was generated.

//NOTE: There are multiple "big monster" encounters. Gathering all items from all "big monster" fights allows the creation of a special, unique item.

averageHatePerPlayer = 400
maxGoldGrant = 400
maxOrbGrant = 8
desiredChanceComponentReceived = 50
timeExpiredPenalty = 0.5

def rewardPlayers() {
	println "###################################"
	println "#### ENTERING REWARDPLAYERS() #####"
	println "#### timeExpired = ${timeExpired} #####"
	println "#######################################"
	masterHateCollector.clone().each() {
		//make sure it's a player (pets are coming!) and make sure that player is still online
		if( isOnline( it ) && isPlayer( it ) && !isInOtherLayer( it ) ) {
			//All players recieve gold relating to the amount of Hate they generate during the event
			goldGrant = (it.getPlayerVar( "Z01AirSharkEventCurrentHate" ) * (maxGoldGrant / averageHatePerPlayer) ).intValue()
			if( goldGrant > maxGoldGrant ) { goldGrant = maxGoldGrant }
			if( timeExpired == true ) { goldGrant = (goldGrant * timeExpiredPenalty).intValue(); println "###### goldGrant = ${goldGrant} #######" }
			it.grantCoins( goldGrant )
			it.centerPrint( "You were rewarded ${goldGrant.intValue()} gold!" )

			//grant orbs for playing in the mini-event
			orbGrant = (it.getPlayerVar( "Z01AirSharkEventCurrentHate" ) * (maxOrbGrant / averageHatePerPlayer) ).intValue()
			if( orbGrant > maxOrbGrant ) { orbGrant = maxOrbGrant }
			if( orbGrant < 1 ) { orbGrant = 1 }
			if( timeExpired == true ) { orbGrant = (orbGrant * timeExpiredPenalty).intValue(); println "###### orbGrant = ${orbGrant} #######"  }
			it.grantQuantityItem( 100257, orbGrant )
			it.centerPrint( "You also earned ${orbGrant.intValue()} Charge Orbs!")

			//Increment a playerVar (by one) that records the number of times the player has participated in this particular event
			it.addPlayerVar( "Z01AirSharkEventTotalEventVisits", 1 )

			//Add in the current Hate total into the player's ALL TIME Hate total
			thisEventHate = it.getPlayerVar( "Z01AirSharkEventCurrentHate" )
			it.addPlayerVar( "Z01AirSharkEventAllTimeHate", thisEventHate )
			if( thisEventHate > 750 ) { thisEventHate = 750 } //cap at 750 for a single event.
			it.centerPrint( "And you earned ${thisEventHate.intValue()} points toward AirShark Badges!" )

			//DEBUG CHECKS
			numVisits = it.getPlayerVar( "Z01AirSharkEventTotalEventVisits" )
			allTimeHate = it.getPlayerVar( "Z01AirSharkEventAllTimeHate" )

			//Award badges for fighting Airshark if beyond certain all-time Hate levels...
			if( it.getPlayerVar( "Z01AirSharkEventCurrentHate" ) > averageHatePerPlayer * 1.2 ) { 
				it.updateQuest( 313, "Rina-VQS" ) //complete the CHUM badge
			}
			//... or beyond a certain event-specific Hate level, or...
			if( it.getPlayerVar( "Z01AirSharkEventAllTimeHate" ) > averageHatePerPlayer * 10 ) { 
				it.updateQuest( 314, "Rina-VQS" ) //complete the WE'RE GONNA NEED A BIGGER BOAT badge
			}
			
			/*
			//Player receive a "big monster"-specific loot item *if* the roll under a certain percentage...which is based on the amount of hate accumulated in the fights.
			roll = random( 100 ) 
			eventHate = it.getPlayerVar( "Z01AirSharkEventCurrentHate" )
			
			//this multiplier is just a scaling value to adjust hate points. Figure out the average hate accumulated during the event and adjust accordingly.
			multiplier = desiredChanceComponentReceived / averageHatePerPlayer
			if( timeExpired == true ) { multiplier = multiplier * timeExpiredPenalty  }

			if( roll < (eventHate * multiplier).intValue() ) { 
				//award the event item
				it.grantItem( "100438" ) //grant the Airshark Plating loot item
				it.centerPrint( "You find a piece of the AirShark itself!" )
			} else {
				it.centerPrint( "No specialty loot this time, but AirShark will be back. And next time, it'll be personal!" )
			}
			*/
		}
	}
}
			
			



