import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (OTAMI RUINS)                   
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 8.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 8.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "OtRuins_805", "orb", "orb_otami", 100 )
orb.setPos( 350, 350 )
orb.setMonsterLevelForChildren( 7.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )

//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "Blaze-VQS", "Grab Them Fast!", "Quick! There's Charge Orbs appearing all over the jungle! Grab them while they're still appearing!" )
	makeZoneTimer("Orb Spill Timer", "0:15:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "Grab Them Fast!" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "Blaze-VQS", "That was Fun!", "It looks like whatever caused the Orbs to appear has faded...at least for now. I hope you all got some!" )
			orbDisposal()
			myManager.schedule(15) { removeZoneBroadcast( "That was Fun!" ); eventDone() }
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.OtRuins_5.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_6.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_105.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_106.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_107.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }   
	myRooms.OtRuins_205.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_206.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_207.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_305.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_306.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_307.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_402.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_405.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_406.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_407.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_502.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_503.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_504.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_505.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_506.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_602.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_603.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_604.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_605.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_606.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_702.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_703.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_704.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_705.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_706.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_801.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_802.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_803.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_804.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_805.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_806.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_902.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_903.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_904.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.OtRuins_905.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.OtRuins_5.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_6.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_105.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_106.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_107.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }   
	myRooms.OtRuins_205.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_206.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_207.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_305.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_306.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_307.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_402.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_404.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_405.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_406.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_407.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_502.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_503.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_504.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_505.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_506.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_602.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_603.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_604.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_605.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_606.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_702.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_703.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_704.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_705.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_706.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_801.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_802.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_803.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_804.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_805.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_806.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_902.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_903.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_904.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
	myRooms.OtRuins_905.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_otami" ) { it.dispose() } }
}

otami5Map = [ 1:[220, 170, 0], 2:[40, 600, 0], 3:[750, 240, 0], 4:[910, 360, 0], 5:"OtRuins_5" ]
otami6Map = [ 1:[130, 30, 0], 2:[900, 205, 0], 3:[780, 620, 0], 4:[490, 100, 0], 5:"OtRuins_6" ]
otami105Map = [ 1:[40, 60, 0], 2:[540, 390, 0], 3:[840, 420, 0], 4:[25, 570, 0], 5:"OtRuins_105" ]
otami106Map = [ 1:[150, 260, 0], 2:[370, 430, 0], 3:[420, 60, 0], 4:[960, 560, 0], 5:"OtRuins_106" ]
otami107Map = [ 1:[45, 315, 0], 2:[470, 380, 0], 3:[460, 640, 0], 4:[320, 60, 0], 5:"OtRuins_107" ]
otami205Map = [ 1:[50, 470, 0], 2:[230, 30, 0], 3:[540, 290, 0], 4:[950, 400, 0], 5:"OtRuins_205" ]
otami206Map = [ 1:[230, 50, 0], 2:[350, 220, 0], 3:[680, 270, 0], 4:[170, 530, 0], 5:"OtRuins_206" ]
otami207Map = [ 1:[30, 60, 0], 2:[590, 90, 0], 3:[560, 630, 0], 4:[760, 160, 0], 5:"OtRuins_207" ]
otami305Map = [ 1:[40, 475, 0], 2:[420, 650, 0], 3:[890, 650, 0], 4:[980, 200, 0], 5:"OtRuins_305" ]
otami306Map = [ 1:[110, 270, 0], 2:[300, 610, 0], 3:[960, 210, 0], 4:[660, 640, 0], 5:"OtRuins_306" ]
otami307Map = [ 1:[30, 320, 0], 2:[520, 650, 0], 3:[740, 420, 0], 4:[840, 70, 0], 5:"OtRuins_307" ]
otami402Map = [ 1:[520, 120, 0], 2:[250, 570, 0], 3:[700, 140, 0], 4:[960, 500, 0], 5:"OtRuins_402" ]
otami404Map = [ 1:[370, 360, 0], 2:[800, 380, 0], 3:[850, 40, 0], 4:[720, 590, 0], 5:"OtRuins_404" ]
otami405Map = [ 1:[430, 420, 0], 2:[120, 340, 0], 3:[970, 80, 0], 4:[1010, 470, 0], 5:"OtRuins_405" ]
otami406Map = [ 1:[550, 230, 0], 2:[270, 260, 0], 3:[960, 220, 0], 4:[220, 650, 0], 5:"OtRuins_406" ]
otami407Map = [ 1:[170, 160, 0], 2:[150, 620, 0], 3:[550, 650, 0], 4:[490, 40, 0], 5:"OtRuins_407" ]
otami502Map = [ 1:[60, 620, 0], 2:[420, 150, 0], 3:[670, 40, 0], 4:[990, 240, 0], 5:"OtRuins_502" ]
otami503Map = [ 1:[60, 130, 0], 2:[650, 140, 0], 3:[650, 410, 0], 4:[940, 230, 0], 5:"OtRuins_503" ]
otami504Map = [ 1:[140, 370, 0], 2:[450, 530, 0], 3:[570, 50, 0], 4:[1020, 260, 0], 5:"OtRuins_504" ]
otami505Map = [ 1:[150, 230, 0], 2:[720, 530, 0], 3:[970, 40, 0], 4:[310, 530, 0], 5:"OtRuins_505" ]
otami506Map = [ 1:[190, 90, 0], 2:[160, 490, 0], 3:[480, 560, 0], 4:[570, 160, 0], 5:"OtRuins_506" ]
otami602Map = [ 1:[440, 440, 0], 2:[840, 610, 0], 3:[990, 170, 0], 4:[560, 130, 0], 5:"OtRuins_602" ]
otami603Map = [ 1:[210, 430, 0], 2:[590, 390, 0], 3:[690, 240, 0], 4:[840, 510, 0], 5:"OtRuins_603" ]
otami604Map = [ 1:[150, 540, 0], 2:[490, 390, 0], 3:[860, 190, 0], 4:[940, 540, 0], 5:"OtRuins_604" ]
otami605Map = [ 1:[140, 520, 0], 2:[330, 250, 0], 3:[600, 100, 0], 4:[1020, 530, 0], 5:"OtRuins_605" ]
otami606Map = [ 1:[250, 340, 0], 2:[820, 170, 0], 3:[930, 450, 0], 4:[530, 550, 0], 5:"OtRuins_606" ]
otami702Map = [ 1:[360, 120, 0], 2:[520, 400, 0], 3:[860, 600, 0], 4:[860, 210, 0], 5:"OtRuins_702" ]
otami703Map = [ 1:[20, 190, 0], 2:[480, 610, 0], 3:[860, 140, 0], 4:[520, 170, 0], 5:"OtRuins_703" ]
otami704Map = [ 1:[240, 220, 0], 2:[210, 450, 0], 3:[830, 630, 0], 4:[650, 80, 0], 5:"OtRuins_704" ]
otami705Map = [ 1:[130, 420, 0], 2:[360, 530, 0], 3:[760, 50, 0], 4:[710, 320, 0], 5:"OtRuins_705" ]
otami706Map = [ 1:[190, 210, 0], 2:[710, 215, 0], 3:[480, 100, 0], 4:[910, 640, 0], 5:"OtRuins_706" ]
otami801Map = [ 1:[80, 230, 0], 2:[300, 530, 0], 3:[770, 50, 0], 4:[950, 500, 0], 5:"OtRuins_801" ]
otami802Map = [ 1:[40, 60, 0], 2:[450, 370, 0], 3:[650, 530, 0], 4:[960, 280, 0], 5:"OtRuins_802" ]
otami803Map = [ 1:[30, 270, 0], 2:[350, 440, 0], 3:[790, 580, 0], 4:[860, 30, 0], 5:"OtRuins_803" ]
otami804Map = [ 1:[80, 390, 0], 2:[400, 470, 0], 3:[650, 300, 0], 4:[960, 100, 0], 5:"OtRuins_804" ]
otami805Map = [ 1:[320, 530, 0], 2:[660, 580, 0], 3:[980, 230, 0], 4:[90, 40, 0], 5:"OtRuins_805" ]
otami806Map = [ 1:[110, 530, 0], 2:[530, 230, 0], 3:[910, 80, 0], 4:[370, 200, 0], 5:"OtRuins_806" ]
otami902Map = [ 1:[100, 500, 0], 2:[410, 20, 0], 3:[1020, 490, 0], 4:[920, 170, 0], 5:"OtRuins_902" ]
otami903Map = [ 1:[280, 10, 0], 2:[270, 580, 0], 3:[650, 270, 0], 4:[1010, 130, 0], 5:"OtRuins_903" ]
otami904Map = [ 1:[20, 150, 0], 2:[670, 270, 0], 3:[970, 380, 0], 4:[710, 510, 0], 5:"OtRuins_904" ]
otami905Map = [ 1:[430, 650, 0], 2:[370, 130, 0], 3:[890, 620, 0], 4:[980, 145, 0], 5:"OtRuins_905" ]

roomList = [ otami5Map, otami6Map, otami105Map, otami106Map, otami107Map, otami205Map, otami206Map, otami207Map, otami305Map, otami306Map, otami307Map, otami402Map, otami404Map, otami405Map, otami406Map, otami407Map, otami502Map, otami503Map, otami504Map, otami505Map, otami506Map, otami602Map, otami603Map, otami604Map, otami605Map, otami606Map, otami702Map, otami703Map, otami704Map, otami705Map, otami706Map, otami801Map, otami802Map, otami803Map, otami804Map, otami805Map, otami806Map, otami902Map, otami903Map, otami904Map, otami905Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.OtRuins_5.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_6.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_105.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_106.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_107.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_205.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_206.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_207.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_305.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_306.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_307.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_402.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_404.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_405.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_406.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_407.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_502.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_503.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_504.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_505.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_506.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_602.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_603.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_604.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_605.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_606.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_702.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_703.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_704.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_705.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_706.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_801.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_802.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_803.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_804.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_805.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_806.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_902.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_903.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_904.getSpawnTypeCount( "orb_otami" )
	orbCount = orbCount + myRooms.OtRuins_905.getSpawnTypeCount( "orb_otami" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 20 ) {
		maxOrbs = 20
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 5 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb, { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			} )
		}
		//now spawn another orb a quarter-second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


