import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (BASS'KEN LAKE)                 
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 5.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 5.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "BASSKEN_1", "orb", "orb_lake", 100 )
orb.setPos( 50, 260 )
orb.setMonsterLevelForChildren( 4.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )

//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "Gustav-VQS", "Ze Shining Orbs!", "You Gaians who always fight to get ze ring orbs...you look and see zey appear everywhere! Get zem!" )
	makeZoneTimer("Orb Spill Timer", "0:15:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "Ze Shining Orbs!" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "Gustav-VQS", "Ze Orbs are No More!", "Ah. It is too late now. Ze shining orbs have gone adieu. Mebbe zey come back another time." )
			orbDisposal()
			myManager.schedule(15) { removeZoneBroadcast( "Ze Orbs are No More!" ); eventDone() }
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.BASSKEN_1.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_2.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_3.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_4.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_5.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_6.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_7.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_101.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_102.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_104.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_105.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_106.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_107.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_201.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_205.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_206.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_207.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_301.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_302.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_305.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_306.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_307.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_401.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_402.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_403.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_405.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_406.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_407.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_501.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_502.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_503.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_504.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_505.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_506.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_507.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_601.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_602.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_603.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_604.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_605.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_606.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BASSKEN_607.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.BASSKEN_1.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_2.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_3.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_4.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_5.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_6.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_7.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_101.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_102.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_104.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_105.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_106.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_107.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_201.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_205.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_206.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_207.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_301.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_302.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_305.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_306.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_307.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_401.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_402.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_403.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_404.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_405.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_406.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_407.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_501.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_502.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_503.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_504.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_505.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_506.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_507.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_601.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_602.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_603.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_604.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_605.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_606.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
	myRooms.BASSKEN_607.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_lake" ) { it.dispose() } }
}

bassken1Map = [ 1:[80, 150, 0], 2:[160, 490, 0], 3:[810, 250, 0], 4:[740, 70, 0], 5:"BASSKEN_1" ]
bassken2Map = [ 1:[450, 100, 0], 2:[460, 560, 0], 3:[860, 340, 0], 4:[170, 390, 0], 5:"BASSKEN_2" ]
bassken3Map = [ 1:[280, 500, 0], 2:[630, 520, 0], 3:[900, 440, 0], 4:[400, 310, 0], 5:"BASSKEN_3" ]
bassken4Map = [ 1:[270, 600, 0], 2:[430, 230, 0], 3:[610, 330, 0], 4:[800, 580, 0], 5:"BASSKEN_4" ]
bassken5Map = [ 1:[260, 430, 0], 2:[790, 520, 0], 3:[1000, 600, 0], 4:[320, 460, 0], 5:"BASSKEN_5" ]
bassken7Map = [ 1:[250, 310, 0], 2:[620, 200, 0], 3:[970, 130, 0], 4:[790, 460, 0], 5:"BASSKEN_7" ]
bassken101Map = [ 1:[140, 430, 0], 2:[530, 620, 0], 3:[940, 230, 0], 4:[400, 130, 0], 5:"BASSKEN_101" ]
bassken102Map = [ 1:[130, 100, 0], 2:[480, 120, 0], 3:[810, 390, 0], 4:[910, 540, 0], 5:"BASSKEN_102" ]
bassken103Map = [ 1:[100, 100, 0], 2:[420, 560, 0], 3:[850, 640, 0], 4:[985, 475, 0], 5:"BASSKEN_103" ]
bassken104Map = [ 1:[260, 170, 0], 2:[530, 230, 0], 3:[920, 270, 0], 4:[565, 470, 0], 5:"BASSKEN_104" ]
bassken105Map = [ 1:[140, 270, 0], 2:[460, 140, 0], 3:[900, 210, 0], 4:[710, 630, 0], 5:"BASSKEN_105" ]
bassken106Map = [ 1:[140, 290, 0], 2:[480, 640, 0], 3:[900, 500, 0], 4:[490, 60, 0], 5:"BASSKEN_106" ]
bassken107Map = [ 1:[550, 190, 0], 2:[740, 460, 0], 3:[780, 640, 0], 4:[920, 240, 0], 5:"BASSKEN_107" ]
bassken201Map = [ 1:[530, 130, 0], 2:[60, 560, 0], 3:[940, 630, 0], 4:[150, 250, 0], 5:"BASSKEN_201" ]
bassken202Map = [ 1:[210, 630, 0], 2:[480, 580, 0], 3:[980, 30, 0], 4:[40, 550, 0], 5:"BASSKEN_202" ]
bassken203Map = [ 1:[130, 100, 0], 2:[770, 260, 0], 3:[430, 450, 0], 4:[570, 200, 0], 5:"BASSKEN_203" ]
bassken205Map = [ 1:[1000, 110, 0], 2:[460, 220, 0], 3:[790, 370, 0], 4:[360, 300, 0], 5:"BASSKEN_205" ]
bassken206Map = [ 1:[150, 50, 0], 2:[720, 50, 0], 3:[870, 630, 0], 4:[480, 20, 0], 5:"BASSKEN_206" ]
bassken207Map = [ 1:[30, 140, 0], 2:[460, 590, 0], 3:[220, 340, 0], 4:[800, 460, 0], 5:"BASSKEN_207" ]
bassken301Map = [ 1:[70, 530, 0], 2:[570, 420, 0], 3:[1000, 400, 0], 4:[420, 230, 0], 5:"BASSKEN_301" ]
bassken302Map = [ 1:[60, 410, 0], 2:[330, 510, 0], 3:[530, 210, 0], 4:[300, 180, 0], 5:"BASSKEN_302" ]
bassken305Map = [ 1:[150, 360, 0], 2:[500, 370, 0], 3:[880, 500, 0], 4:[460, 610, 0], 5:"BASSKEN_305" ]
bassken306Map = [ 1:[40, 530, 0], 2:[300, 350, 0], 3:[750, 90, 0], 4:[860, 370, 0], 5:"BASSKEN_306" ]
bassken307Map = [ 1:[820, 320, 0], 2:[480, 30, 0], 3:[180, 560, 0], 4:[850, 90, 0], 5:"BASSKEN_307" ]
bassken401Map = [ 1:[610, 70, 0], 2:[280, 440, 0], 3:[50, 190, 0], 4:[740, 640, 0], 5:"BASSKEN_401" ]
bassken402Map = [ 1:[770, 480, 0], 2:[50, 360, 0], 3:[350, 80, 0], 4:[930, 340, 0], 5:"BASSKEN_402" ]
bassken404Map = [ 1:[140, 600, 0], 2:[420, 180, 0], 3:[750, 460, 0], 4:[110, 320, 0], 5:"BASSKEN_404" ]
bassken405Map = [ 1:[280, 580, 0], 2:[640, 40, 0], 3:[450, 90, 0], 4:[30, 570, 0], 5:"BASSKEN_405" ]
bassken406Map = [ 1:[380,210, 0], 2:[680, 420, 0], 3:[850, 520, 0], 4:[30, 30, 0], 5:"BASSKEN_406" ]
bassken407Map = [ 1:[70, 550, 0], 2:[470, 350, 0], 3:[990, 130, 0], 4:[700, 580, 0], 5:"BASSKEN_407" ]
bassken501Map = [ 1:[60, 130, 0], 2:[520, 440, 0], 3:[950, 570, 0], 4:[400, 60, 0], 5:"BASSKEN_501" ]
bassken502Map = [ 1:[50, 300, 0], 2:[310, 450, 0], 3:[820, 540, 0], 4:[470, 180, 0], 5:"BASSKEN_502" ]
bassken503Map = [ 1:[130, 150, 0], 2:[420, 510, 0], 3:[800, 650, 0], 4:[860, 140, 0], 5:"BASSKEN_503" ]
bassken504Map = [ 1:[220, 350, 0], 2:[590, 560, 0], 3:[820, 570, 0], 4:[880, 20, 0], 5:"BASSKEN_504" ]
bassken505Map = [ 1:[80, 430, 0], 2:[380, 260, 0], 3:[760, 640, 0], 4:[720, 300, 0], 5:"BASSKEN_505" ]
bassken506Map = [ 1:[110, 340, 0], 2:[430, 620, 0], 3:[680, 610, 0], 4:[920, 60, 0], 5:"BASSKEN_506" ]
bassken507Map = [ 1:[530, 330, 0], 2:[810, 610, 0], 3:[650, 120, 0], 4:[280, 250, 0], 5:"BASSKEN_507" ]
bassken601Map = [ 1:[70, 500, 0], 2:[490, 620, 0], 3:[980, 130, 0], 4:[40, 120, 0], 5:"BASSKEN_601" ]
bassken602Map = [ 1:[580, 550, 0], 2:[730, 100, 0], 3:[1000, 380, 0], 4:[240, 330, 0], 5:"BASSKEN_602" ]
bassken603Map = [ 1:[250, 530, 0], 2:[370, 310, 0], 3:[890, 160, 0], 4:[870, 640, 0], 5:"BASSKEN_603" ]
bassken604Map = [ 1:[350, 250, 0], 2:[530, 390, 0], 3:[940, 170, 0], 4:[200, 550, 0], 5:"BASSKEN_604" ]
bassken605Map = [ 1:[120, 260, 0], 2:[250, 60, 0], 3:[870, 320, 0], 4:[770, 20, 0], 5:"BASSKEN_605" ]
bassken606Map = [ 1:[110, 220, 0], 2:[490, 120, 0], 3:[940, 330, 0], 4:[880, 80, 0], 5:"BASSKEN_606" ]

roomList = [ bassken1Map, bassken2Map, bassken3Map, bassken4Map, bassken5Map, bassken7Map, bassken101Map, bassken102Map, bassken103Map, bassken104Map, bassken105Map, bassken106Map, bassken107Map, bassken201Map, bassken202Map, bassken203Map, bassken205Map, bassken206Map, bassken207Map, bassken301Map, bassken302Map, bassken305Map, bassken306Map, bassken307Map, bassken401Map, bassken402Map, bassken404Map, bassken405Map, bassken406Map, bassken407Map, bassken501Map, bassken502Map, bassken503Map, bassken504Map, bassken505Map, bassken506Map, bassken507Map, bassken601Map, bassken602Map, bassken603Map, bassken604Map, bassken605Map, bassken606Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.BASSKEN_1.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_2.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_3.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_4.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_5.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_6.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_7.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_101.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_102.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_103.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_104.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_105.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_106.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_107.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_201.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_202.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_203.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_204.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_205.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_206.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_207.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_301.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_302.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_303.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_304.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_305.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_306.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_307.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_401.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_402.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_403.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_404.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_405.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_406.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_407.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_501.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_502.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_503.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_504.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_505.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_506.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_507.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_601.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_602.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_603.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_604.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_605.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_606.getSpawnTypeCount( "orb_lake" )
	orbCount = orbCount + myRooms.BASSKEN_607.getSpawnTypeCount( "orb_lake" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 20 ) {
		maxOrbs = 20
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 5 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb, { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			} )
		}
		//now spawn another orb a quarter-second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


