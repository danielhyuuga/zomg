import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
//ORB SPILL (BARTON TOWN)                   
//------------------------------------------

//Make the orbs collectible only at the appropriate CL for the zone
def orbSpillSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 2.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must be level 2.0 or below to collect the orbs." ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

orb = createStoppedEventSpawner( "BARTON_103", "orb", "orb_barton", 100 )
orb.setPos( 1200, 600 )
orb.setMonsterLevelForChildren( 1.6 )
orb.setHomeTetherForChildren( 6000 )
orb.setMiniEventSpec( orbSpillSpec )
orb.setEdgeHinting( false )
orb.setTargetCycle( false )


//------------------------------------------
//EVENT TIMER                               
//------------------------------------------

stopSpawning = false

def startEventTimer() {
	zoneBroadcast( "Agatha-VQS", "Lifeforce Overflow!", "Ghi energy is overflowing and pooling into the form of Charge Orbs! Who knows how long it will last! Grab them quick, dearies!" )
	makeZoneTimer("Orb Spill Timer", "0:20:0", "0:0:0").onCompletion( { event ->
		removeZoneTimer( "Orb Spill Timer" )
		removeZoneBroadcast( "Lifeforce Overflow!" )
		stopSpawning = true
		myManager.schedule(2) {
			zoneBroadcast( "Agatha-VQS", "Overflow Abated", "It looks like whatever caused the Ghi overflow has ebbed and the Charge Orbs are no longer appearing." )
			orbDisposal()
			myManager.schedule(15) { removeZoneBroadcast( "Overflow Abated" ); eventDone() }
		}
	} ).start()
}

//------------------------------------------
//SPAWNING LOGIC                            
//------------------------------------------

orbEventPlayerList = []
numOrbPositionsPerRoom = 4
zonePlayerList = []

//Get the names of all the players in the zone
def makePlayerList(){
	//Empty the check list and then look into all rooms to find all players, whether in a Crew or not
	zonePlayerList.clear()
	myRooms.BARTON_1.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_2.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_3.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_4.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_101.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_102.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_104.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_201.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_301.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_302.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
	myRooms.BARTON_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerList << it } }
}

//Dispose of the orbs after the countdown is complete
def orbDisposal(){
	myRooms.BARTON_1.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_2.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_3.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_4.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_101.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_102.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_103.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_104.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_201.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_202.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_203.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_204.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_301.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_302.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_303.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
	myRooms.BARTON_304.getActorList().each { if( isMonster( it ) && it.getMonsterType() == "orb_barton" ) { it.dispose() } }
}

barton1Map = [ 1:[980, 260, 0], 2:[1410, 240, 0], 3:[1480, 880, 0], 4:[180, 890, 0], 5:"BARTON_1" ]
barton2Map = [ 1:[730, 250, 0], 2:[1070, 335, 0], 3:[270, 550, 0], 4:[1115, 915, 0], 5:"BARTON_2" ]
barton3Map = [ 1:[180, 260, 0], 2:[1040, 350, 0], 3:[1120, 710, 0], 4:[500, 810, 0], 5:"BARTON_3" ]
barton4Map = [ 1:[1205, 520, 0], 2:[1460, 710, 0], 3:[1200, 720, 0], 4:[1330, 970, 0], 5:"BARTON_4" ]
barton101Map = [ 1:[360, 160, 0], 2:[870, 390, 0], 3:[315, 630, 0], 4:[960, 950, 0], 5:"BARTON_101" ]
barton102Map = [ 1:[1070, 770, 0], 2:[1420, 330, 0], 3:[485, 160, 0], 4:[70, 40, 0], 5:"BARTON_102" ]
barton103Map = [ 1:[610, 540, 0], 2:[125, 200, 0], 3:[1470, 940, 0], 4:[1355, 120, 0], 5:"BARTON_103" ]
barton104Map = [ 1:[575, 450, 0], 2:[580, 960, 0], 3:[1280, 790, 0], 4:[1450, 245, 0], 5:"BARTON_104" ]
barton201Map = [ 1:[790, 290, 0], 2:[375, 90, 0], 3:[600, 970, 0], 4:[1470, 760, 0], 5:"BARTON_201" ]
barton202Map = [ 1:[60, 750, 0], 2:[615, 230, 0], 3:[1295, 540, 0], 4:[1180, 95, 0], 5:"BARTON_202" ]
barton203Map = [ 1:[260, 160, 0], 2:[1140, 120, 0], 3:[570, 600, 0], 4:[220, 910, 0], 5:"BARTON_203" ]
barton204Map = [ 1:[570, 60, 0], 2:[955, 555, 0], 3:[380, 960, 0], 4:[975, 225, 0], 5:"BARTON_204" ]
barton301Map = [ 1:[265, 110, 0], 2:[1115, 120, 0], 3:[460, 470, 0], 4:[1370, 460, 0], 5:"BARTON_301" ]
barton302Map = [ 1:[360, 200, 0], 2:[1100, 360, 0], 3:[1030, 690, 0], 4:[70, 560, 0], 5:"BARTON_302" ]
barton303Map = [ 1:[55, 425, 0], 2:[1005, 730, 0], 3:[825, 360, 0], 4:[1330, 360, 0], 5:"BARTON_303" ]
barton304Map = [ 1:[380, 250, 0], 2:[800, 550, 0], 3:[140, 740, 0], 4:[640, 740, 0], 5:"BARTON_304" ]

roomList = [ barton1Map, barton2Map, barton3Map, barton4Map, barton101Map, barton102Map, barton103Map, barton104Map, barton201Map, barton202Map, barton203Map, barton204Map, barton301Map, barton302Map, barton303Map, barton304Map ]

def countOrbs() {
	orbCount = 0
	//count the number of orbs in the zone
	orbCount = orbCount + myRooms.BARTON_1.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_2.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_3.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_4.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_101.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_102.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_103.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_104.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_201.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_202.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_203.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_204.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_301.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_302.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_303.getSpawnTypeCount( "orb_barton" )
	orbCount = orbCount + myRooms.BARTON_304.getSpawnTypeCount( "orb_barton" )
}

//NOTE: The min limit for orbs in this zone is 10 instead of 20 because there are far fewer rooms in Barton Town than in other zones. Too many orbs with too few players is more like orb "harvesting" instead of orb "hunting".
def setMaxOrbs() {
	maxOrbs = areaPlayerCount()
	if( areaPlayerCount() < 10 ) {
		maxOrbs = 10
	}
}

//After the orbs have been spawned, keep checking until five or less of them exist...then spawn another wave of orbs
def checkForOrbRespawn() {
	if( stopSpawning == true ) return
	countOrbs()
	if( orbCount <= 3 ) {
		setMaxOrbs()
		spawnOrbs()
	} else {
		myManager.schedule(30) { checkForOrbRespawn() }
	}
}

//randomly spawn orbs
def spawnOrbs() {
	countOrbs()
	//if the current number of orbs in the zone is less than "maxOrbs", then spawn an orb
	if( orbCount < maxOrbs ) {
		//find the room in the zone to warp to
		room = random( roomList )
		//figure out the spawn position within that room
		position = random( numOrbPositionsPerRoom )
		positionInfo = room.get( position )
		//now look in position 2 of the "positionInfo" to see if the orb is spawned yet or not. (0 = not spawned; 1 = spawned)
		spawnHolder = positionInfo.get(2)
		if( spawnHolder == 0 ) {
			//change the "0" to a "1"
			positionInfo.remove( 2 ) ///remove the "placeholder" element in the current list
			positionInfo.add( 1 ) //put "1" at the end
			//now update the Map with the new list
			room.remove( position )
			room.put( position, positionInfo )
			roomName = room.get(5)
			X = positionInfo.get(0)
			Y = positionInfo.get(1)
			//orb.setHomeForChildren( roomName, X, Y )
			//now spawn the orb
			orb.warp( roomName.toString(), X, Y )
			orb.setHomeForChildren( "${roomName}", X, Y )
			currentOrb = orb.forceSpawnNow()
			//when the orb is destroyed, reset its record to 0 so the orb can spawn again later
			runOnDeath( currentOrb) { event ->
				event.killer.addPlayerVar( "Z01OrbCounter", 1 )
				deathRoomName = event.actor.getRoomName()
				deathX = event.actor.getX().intValue()
				deathY = event.actor.getY().intValue()
				//search through all the maps to find the map with the correct room name in it
				roomList.clone().each{
					if( it.get(5) == deathRoomName ) {
						deathRoom = it
					}
				}
				//once the correct map is found, use the death X to make a match for which position to read
				seed = 1
				deathPosition = 0
				findCorrectPosition()
				deathPositionInfo = deathRoom.get( deathPosition )
				//now that we have the correct position, change out the info properly so an orb can spawn there again
				deathPositionInfo.remove(2) //remove the placeholder at the end of the list
				deathPositionInfo.add(0)
				//now update the deathRoom Map with the updated list
				deathRoom.remove( deathPosition)
				deathRoom.put( deathPosition, deathPositionInfo )
			}
		}
		//now spawn another orb one second from now
		myManager.schedule( 1 ) { spawnOrbs() }
	//otherwise, delay a few seconds and try again
	} else {
		myManager.schedule( 30 ) { checkForOrbRespawn() } 
	}
}

def findCorrectPosition() {
	if( seed <= 4 ) {
		deathPositionInfo = deathRoom.get(seed)
		//if the first element in this list is the same place the orb "died", then that's the correct position in the map
		if( deathPositionInfo.get(0) >= deathX - 40 && deathPositionInfo.get(0) <= deathX + 40 ) { //check a wide margin in case the exact position is slightly different than the spawner
			deathPosition = seed
		}
		seed ++
		findCorrectPosition()
	}
}


//start it all up
myManager.schedule(3) { startEventTimer() }
myManager.schedule(3) { checkForOrbRespawn() }


