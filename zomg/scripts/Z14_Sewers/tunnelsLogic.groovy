import com.gaiaonline.mmo.battle.script.*;

//-------------------------------------------
// KICKING OUT CHEATERS                      
//-------------------------------------------

MaxML = 2.1
zonePlayerSet = [] as Set
statusSet = [] as Set
checkMeSet = [] as Set
warpOutSet = [] as Set

playerHated = false

onQuestStep( 332, 2 ) { event -> event.actor.centerPrint( "Congratulations! You completed the 'Plunge into the Fray' task!" ); questReward( event ); checkBoth( event ) }
onQuestStep( 333, 2 ) { event -> event.actor.centerPrint( "Well done! You finished the 'Gramster Goodness' task!" ); questReward( event ); checkBoth( event ) }

def questReward( event ) {
	event.actor.grantCoins( 75 )
	event.actor.grantQuantityItem( 100257, 4 )
}

def checkBoth( event ) {
	if( event.actor.isDoneQuest( 332 ) && event.actor.isDoneQuest( 333 ) ) {
		myManager.schedule(2) { event.actor.centerPrint( "That completes both of the Waterworks tasks! Great job!" ) }
	}
}

kickOutCheatingPlayers() //start the routine running so it's always working while server is up

def kickOutCheatingPlayers() {
	makeZonePlayerSet()
	if( !zonePlayerSet.isEmpty() ) {
		zonePlayerSet.clone().each{
			if( it.getConLevel() >= MaxML ) {
				it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
				warpOutSet << it
			}
		}
	}
	myManager.schedule(2) {
		warpOutSet.each{
			it.warp( "BARTON_104", 1060, 240 )
			it.unsetQuestFlag( GLOBAL, "Z14RingRingGiven" )
			it.unsetQuestFlag( GLOBAL, "Z14BossWarningGiven" )
			myManager.stopListenForHealth( it )
			it.setQuestFlag( GLOBAL, "Z14HasLeftWaterworks" )

		}
	}
	myManager.schedule(10) { kickOutCheatingPlayers() }
}

def makeZonePlayerSet() {
	zonePlayerSet.clear()
	warpOutSet.clear()
	myRooms.Sewers_2.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_3.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_5.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_6.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_102.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_103.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_104.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_105.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_106.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_107.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_202.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_203.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_204.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_205.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_206.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_207.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_301.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_302.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_303.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_304.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_305.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_306.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_402.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_403.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_404.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_405.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
	myRooms.Sewers_406.getActorList().each { if( isPlayer( it ) ) { zonePlayerSet << it } }
}

//==============================================
//ENTERING FROM SEWERSTRAINING (DANI)           
//==============================================

myManager.onEnter( myRooms.Sewers_5 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )

		if( event.actor.hasQuestFlag( GLOBAL, "Z25ComingFromDani" ) ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z25ComingFromDani" )
			event.actor.setQuestFlag( GLOBAL, "Z14RegPromptShouldBeShown" )
			event.actor.setQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" )
			event.actor.centerPrint( "The door closes and locks behind you. You're on your own now!" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveOneTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveTwoTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveThreeTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveFourTurned" )
			event.actor.addMiniMapQuestLocation( "Z14ValveOneFlag", "Sewers_202", 230, 145, "Valve" )
			event.actor.addMiniMapQuestLocation( "Z14ValveTwoFlag", "Sewers_403", 290, 165, "Valve" )
			event.actor.addMiniMapQuestLocation( "Z14ValveThreeFlag", "Sewers_305", 805, 320, "Valve" )
			event.actor.addMiniMapQuestLocation( "Z14ValveFourFlag", "Sewers_207", 905, 215, "Valve" )

			//automatically turn on the tutorials the first time, let them select thereafter
			event.actor.setQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" )
			
			//start the player-specific tutorials
			player = event.actor
			setupTutorials( event )
			
			//put the player on the kill quests
			myManager.schedule(2) { tutorialNPC.pushDialog( event.actor, "newQuests" ) }
			
		}
		if( event.actor.hasQuestFlag( GLOBAL, "Z22ComingFromTrainStation" ) ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z22ComingFromTrainStation" )
			event.actor.centerPrint( "The door closes and locks behind you." )
			player = event.actor
			makeTutorialChoicesMenu( player )
		}				
	}
}

//STAIRS TRIGGER ZONE ("DOOR IS LOCKED")
def noReturnTrigger = "noReturnTrigger"
myRooms.Sewers_5.createTriggerZone( noReturnTrigger, 10, 140, 300, 470 )

myManager.onTriggerIn( myRooms.Sewers_5, noReturnTrigger ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.centerPrint( "The one-way door at the top of these stairs is locked. You'll need to find another way out!" )
	}
}

//GIVE THE STARTER QUESTS
newQuests = tutorialNPC.createConversation( "newQuests", true )

def quest1 = [id:1]
quest1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/twoTasks.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
quest1.exec = { event ->
	event.player.updateQuest( 332, "BFG-Dani" )
	event.player.updateQuest( 333, "BFG-Dani" )
	//open up with a pop-up tutorial telling players to check out the HyperNet
}
quest1.result = DONE
newQuests.addDialog( quest1, tutorialNPC )

//USE THE HYPERNET, FOOL! :)
useTheHyperNet = tutorialNPC.createConversation( "useTheHyperNet", true )

def use1 = [id:1]
use1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/hyperNet.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
use1.flag = "Z14HyperNetAccessed"
use1.result = DONE
useTheHyperNet.addDialog( use1, tutorialNPC )

//-------------------------------------------
// ENTERING FROM BARTON TOWN                 
//-------------------------------------------

myManager.onEnter( myRooms.Sewers_104 ) { event ->
	if( isPlayer( event.actor ) ) {
		//first, if they are coming into this room from Barton Town, ask the player if they want to turn tutorials on or off.
		if( event.actor.hasQuestFlag( GLOBAL, "Z01ComingFromBartonTown" ) && !event.actor.hasQuestFlag( GLOBAL, "Z14AlreadySawTutorialOptionMenu" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z14AlreadySawTutorialOptionMenu" )
			event.actor.unsetQuestFlag( GLOBAL, "Z25ComingFromDani" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveOneTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveTwoTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveThreeTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveFourTurned" )
			event.actor.addMiniMapQuestLocation( "Z14ValveOneFlag", "Sewers_202", 230, 145, "Valve" )
			event.actor.addMiniMapQuestLocation( "Z14ValveTwoFlag", "Sewers_403", 290, 165, "Valve" )
			event.actor.addMiniMapQuestLocation( "Z14ValveThreeFlag", "Sewers_305", 805, 320, "Valve" )
			event.actor.addMiniMapQuestLocation( "Z14ValveFourFlag", "Sewers_207", 905, 215, "Valve" )

			player = event.actor
			makeTutorialChoicesMenu( player )
		}
	}
}

//-------------------------------------------
// TUTORIALS ON/OFF MENU                     
//-------------------------------------------

def synchronized makeTutorialChoicesMenu( player ) {
	titleString = "Tutorial Options"
	descripString = "If you're learning the game still, turn the tutorials on and you'll receive hints as you play through this area. If you already know how to play, then turn these off."
	diffOptions = [ "Tutorials On", "Tutorials Off" ]
	
	uiButtonMenu( player, "tutorialChoices", titleString, descripString, diffOptions, 300 ) { event ->
		if( event.selection == "Tutorials On" ) {
			event.actor.setQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" )
			event.actor.centerPrint( "Tutorials are ON" )
			player = event.actor
			
			//set up all the player-specific tutorials that are context sensitive
			setupTutorials( event )
			event.actor.unsetQuestFlag( GLOBAL, "Z22FirstTimeDazedDialog" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14RingRingGiven" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ChestOpeningTutorial" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14BossWarningGiven" )
			
			//push the combat tutorial dialog
			myManager.schedule(2) {
				tutorialNPC.pushDialog( event.actor, "ringRing" )
				event.actor.setQuestFlag( GLOBAL, "Z14RingRingGiven" )
			}				
		}
		if( event.selection == "Tutorials Off" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" )
			event.actor.centerPrint( "Tutorials are OFF" )
		}
	}
}

//-------------------------------------------
// onHealth, onDeath and KillCount TUTORIALS 
//-------------------------------------------

def synchronized setupTutorials( event ) {
	//set up the noob kill event messages
	if( !event.actor.hasQuestFlag( GLOBAL, "ZXXCrewUpCounterStarted" ) ) {
		event.actor.setQuestFlag( GLOBAL, "ZXXCrewUpCounterStarted" )
		makeCounter(event.actor, "crewUpKillCounter", 0, false).watchForKill( "gramster", "gramster_LT", "peelunger" ).setGoal(5).onCompletion( { 
			tryToShowCrewTutorial( event )
			removeCounter( event.actor, "crewUpKillCounter" )
			event.actor.unsetQuestFlag( GLOBAL, "ZXXCrewUpCounterStarted" )
		} )
	}
	if( !event.actor.hasQuestFlag( GLOBAL, "ZXXPowerupCounterStarted" ) ) {
		event.actor.setQuestFlag( GLOBAL, "ZXXPowerupCounterStarted" )
		makeCounter(event.actor, "powerupKillCounter", 0, false).watchForKill( "gramster", "gramster_LT", "peelunger" ).setGoal(10).onCompletion( { 
			tryToShowPowerUpsTutorial( event )
			removeCounter( event.actor, "powerupKillCounter" )
			event.actor.unsetQuestFlag( GLOBAL, "ZXXPowerupCounterStarted" )
		} )
	}
	
	player = event.actor
	println "**** ZXXOnHealthStarted = ${event.actor.hasQuestFlag( GLOBAL, "ZXXOnHealthStarted" )} and ZXXOnDeathStarted = ${event.actor.hasQuestFlag( GLOBAL, "ZXXOnDeathStarted" )} ****"

	//Add the player to the offline status check list
	statusSet << player

	if( !event.actor.hasQuestFlag( GLOBAL, "ZXXOnHealthStarted" ) ) {
		event.actor.setQuestFlag( GLOBAL, "ZXXOnHealthStarted" )
		startOnHealth()
	}
	if( !event.actor.hasQuestFlag( GLOBAL, "ZXXOnDeathStarted" ) ) {
		event.actor.setQuestFlag( GLOBAL, "ZXXOnDeathStarted" )
		startOnDeath()
	}
}

def synchronized checkIfPlayerAggroed( event ) {
	println "**** CHECKING FOR AGGRO ****"
	playerHated = false
	println "**** event.actor = ${ event.actor } ****"
	println "**** battleroom = ${event.actor.getRoom()}****"
	event.actor.getRoom().getActorList().each() {
		if( isMonster( it ) ) {
			println "**** Hate list for monster = ${it.getHated()} ****"
			if( it.getHated().contains( event.actor ) ) {
				playerHated = true
			}
		}
	}
	println "**** playerHated = ${playerHated} ****"
}

def synchronized tryToShowCrewTutorial( event ) {
	checkIfPlayerAggroed( event )
	if( playerHated == false ) {
		tutorialNPC.pushDialog( event.actor, "crewAdv" )
	} else {
		myManager.schedule(3) { tryToShowCrewTutorial( event ) }
	}
}

def synchronized tryToShowPowerUpsTutorial( event ) {
	checkIfPlayerAggroed( event )
	if( playerHated == false ) {
		tutorialNPC.pushDialog( event.actor, "powerups" )
	} else {
		myManager.schedule(3) { tryToShowPowerUpsTutorial( event ) }
	}
}


def startOnHealth() {
	println "**** STARTING ON HEALTH MANAGER ****"
	//set up some onHealth tutorials
	myManager.onHealth( player ) { event ->
		if( event.didTransition( 50 ) && event.isDecrease() ) {
			event.actor.centerPrint( "You're down to half-health right now. Find a place to Kneel and Heal!" )
		} else if( event.didTransition( 20 ) && event.isDecrease() ) {
			event.actor.centerPrint( "That heartbeat sound you hear is a warning that your health is very low!" )
		} else if( event.didTransition( 1 ) && event.isDecrease() ) {
			myManager.stopListenForHealth( event.actor )
		}
	}
}

def startOnDeath() {
	println "**** STARTING RUN ON DEATH ****"
	//set up the Dazed message
	runOnDeath( player ) { event ->
		if( !event.actor.hasQuestFlag( GLOBAL, "Z14HasLeftWaterworks" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z14PlayerDazed" )
			event.actor.unsetQuestFlag( GLOBAL, "ZXXOnDeathStarted" )
			event.actor.unsetQuestFlag( GLOBAL, "ZXXOnHealthStarted" )
			tutorialNPC.pushDialog( event.actor, "dazedMessage" )
			myManager.stopListenForHealth( event.actor )
		}
	}
}

//MESSAGE SEEN WHEN DAZED
dazedMessage = tutorialNPC.createConversation( "dazedMessage", true )

def dazed1 = [id:1]
dazed1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/awakenTutorial.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
dazed1.result = DONE
dazedMessage.addDialog( dazed1, tutorialNPC )


//onHealth TUTORIALS
crewAdv = tutorialNPC.createConversation( "crewAdv", true )

def crew1 = [id:1]
crew1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/whyCrew.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
crew1.result = DONE
crewAdv.addDialog( crew1, tutorialNPC )

powerups = tutorialNPC.createConversation( "powerups", true )

def power1 = [id:1]
power1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/powerupTutorial.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
power1.result = DONE
powerups.addDialog( power1, tutorialNPC )

//-------------------------------------------
// CLEANING UP/RESETTING OFFLINE PLAYERS     
//-------------------------------------------

//if a player is offline, then unset the onHealth and onDeath flags so they can be set again upon their return
def checkOfflineStatus() {
	if( statusSet.size() > 0 ) {
		statusSet.each{
			checkMeSet << it 
		}
		checkMeSet.each{
			if( !isOnline( it ) ) {
				it.unsetQuestFlag( GLOBAL, "ZXXOnDeathStarted" )
				it.unsetQuestFlag( GLOBAL, "ZXXOnHealthStarted" )
				statusSet.remove( it )
			}
		}
	}
	checkMeSet.clear()
	myManager.schedule(30) { checkOfflineStatus() }	
}
			
//-------------------------------------------
// ROOM-SPECIFIC TUTORIALS                   
//-------------------------------------------

//TUTORIAL ABOUT TARGET SELECTION (ROOM 105)
myManager.onEnter( myRooms.Sewers_105 ) { event ->
	if( isPlayer( event.actor ) && event.actor.hasQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" ) && !event.actor.hasQuestFlag( GLOBAL, "Z14RingRingGiven" ) ) {
		myManager.schedule(2) { 
			tutorialNPC.pushDialog( event.actor, "ringRing" )
			event.actor.setQuestFlag( GLOBAL, "Z14RingRingGiven" )
		}
	}
}

ringRing = tutorialNPC.createConversation( "ringRing", true )

def ring1 = [id:1]
ring1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/basicCombat.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
ring1.result = DONE
ringRing.addDialog( ring1, tutorialNPC )

//TUTORIAL ABOUT THE BOSS LAIR (ROOM 202 & ROOM 303)
myManager.onEnter( myRooms.Sewers_202 ) { event ->
	if( isPlayer( event.actor ) && event.actor.hasQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" ) && !event.actor.hasQuestFlag( GLOBAL, "Z14BossWarningGiven" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z14BossWarningGiven" )
		tutorialNPC.pushDialog( event.actor, "bossWarning" )
	}
}

myManager.onEnter( myRooms.Sewers_303 ) { event ->
	if( isPlayer( event.actor ) && event.actor.hasQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" ) && !event.actor.hasQuestFlag( GLOBAL, "Z14BossWarningGiven" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z14BossWarningGiven" )
		tutorialNPC.pushDialog( event.actor, "bossWarning" )
	}
}

//boss warning tutorial
bossWarning = tutorialNPC.createConversation( "bossWarning", true )

def boss1 = [id:1]
boss1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/bossWarning.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
boss1.result = DONE
bossWarning.addDialog( boss1, tutorialNPC )

//TUTORIAL ABOUT OPENING CHESTS
chestOpening = tutorialNPC.createConversation( "chestOpening", true )

def chest1 = [id:1]
chest1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/chestLoot.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
chest1.result = DONE
chestOpening.addDialog( chest1, tutorialNPC )






//==============================================
//==============================================
//MONSTER LOGIC AND SPAWNERS                    
//==============================================
//==============================================
def tunnelSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = player.getConLevel() <= 20.1 //TODO: DEBUG!!! Change this to 2.1
		if( player == attacker && !allowed ) { player.centerPrint( "You must lower your level to 2.0 or lower to fight in these tunnels!" ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

//==============================================
// FRONT-ROOMS WANDERERS (103, 104, 105)        
//==============================================

//Room 103
wander103A = myRooms.Sewers_103.spawnSpawner( "wander103A", "peelunger", 2 )
wander103A.setWaitTime( 20, 40 )
wander103A.setPos( 420, 30 )
wander103A.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
wander103A.setMiniEventSpec( tunnelSpec )
wander103A.setMonsterLevelForChildren( 1.0 )
wander103A.spawnAllNow()

wander103B = myRooms.Sewers_103.spawnSpawner( "wander103B", "peelunger", 1 )
wander103B.setWaitTime( 20, 40 )
wander103B.setPos( 760, 350 )
wander103B.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
wander103B.setMiniEventSpec( tunnelSpec )
wander103B.setMonsterLevelForChildren( 1.0 )
wander103B.spawnAllNow()

wander103C = myRooms.Sewers_103.spawnSpawner( "wander103C", "peelunger", 1 )
wander103C.setWaitTime( 20, 40 )
wander103C.setPos( 980, 360 )
wander103C.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
wander103C.setMiniEventSpec( tunnelSpec )
wander103C.setMonsterLevelForChildren( 1.0 )
wander103C.spawnAllNow()

gramWander103 = myRooms.Sewers_103.spawnSpawner( "gramWander103", "gramster", 2 )
gramWander103.setPos( 170, 370 )
gramWander103.setWaitTime( 20, 40 )
gramWander103.setWanderBehaviorForChildren( 75, 150, 3, 7, 250)
gramWander103.setMiniEventSpec( tunnelSpec )
gramWander103.setMonsterLevelForChildren( 1.0 )
gramWander103.spawnAllNow()

gramWander105 = myRooms.Sewers_105.spawnSpawner( "gramWander105", "gramster", 3 )
gramWander105.setPos( 660, 510 )
gramWander105.setWaitTime( 20, 40 )
gramWander105.setWanderBehaviorForChildren( 75, 150, 3, 7, 250)
gramWander105.setMiniEventSpec( tunnelSpec )
gramWander105.setMonsterLevelForChildren( 1.0 )
gramWander105.spawnAllNow()

//PATROL
patrol204 = myRooms.Sewers_204.spawnStoppedSpawner( "patrol204", "peelunger", 3 )
patrol204.setPos( 190, 160 )
patrol204.setMiniEventSpec( tunnelSpec )
patrol204.setMonsterLevelForChildren( 1.0 )
patrol204.setBaseSpeed( 100 )
patrol204.setChildrenToFollow( patrol204 )
patrol204.addPatrolPointForSpawner( "Sewers_204", 400, 90, 10)
patrol204.addPatrolPointForSpawner( "Sewers_104", 500, 475, 0)
patrol204.addPatrolPointForSpawner( "Sewers_105", 180, 250, 0)
patrol204.addPatrolPointForSpawner( "Sewers_105", 840, 190, 0)
patrol204.addPatrolPointForSpawner( "Sewers_105", 570, 590, 0)
patrol204.addPatrolPointForSpawner( "Sewers_205", 340, 80, 10)
patrol204.addPatrolPointForSpawner( "Sewers_105", 570, 590, 0)
patrol204.addPatrolPointForSpawner( "Sewers_105", 840, 190, 0)
patrol204.addPatrolPointForSpawner( "Sewers_105", 180, 250, 0)
patrol204.addPatrolPointForSpawner( "Sewers_104", 500, 475, 0)
patrol204.startPatrol()

//patrol204 respawn triggers
def respawn204Trigger = "respawn204Trigger"
myRooms.Sewers_204.createTriggerZone( respawn204Trigger, 260, 40, 600, 220 )

def respawn205Trigger = "respawn205Trigger"
myRooms.Sewers_205.createTriggerZone( respawn205Trigger, 210, 10, 490, 180 )

myManager.onTriggerIn( myRooms.Sewers_204, respawn204Trigger ) { event ->
	if( isSpawner( event.actor ) ) {
		if( patrol204.spawnsInUse() < 3 ) {
			numSpawn204 = 3 - patrol204.spawnsInUse()
			respawnPatrol204()
		}
	}
}

myManager.onTriggerIn( myRooms.Sewers_205, respawn205Trigger ) { event ->
	if( isSpawner( event.actor ) ) {
		if( patrol204.spawnsInUse() < 3 ) {
			numSpawn204 = 3 - patrol204.spawnsInUse()
			respawnPatrol204()
		}
	}
}

def respawnPatrol204() {
	if( numSpawn204 > 0 ) {
		patrol204.forceSpawnNow()
		numSpawn204 --
		respawnPatrol204()
	}
}

numSpawn204 = 3
respawnPatrol204()


//==============================================
//CAMP CAVORT                                   
//==============================================
//106-107-206-207 area is a camp. The peelungers frolic and breed beneath the water fountains. (Small groups of peelungers in all fountain locations everywhere on map.)

//Room 106
cavort106A = myRooms.Sewers_106.spawnSpawner( "cavort106A", "peelunger", 2 )
cavort106A.setWaitTime( 20, 40 )
cavort106A.setPos( 560, 470 )
cavort106A.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
cavort106A.setMiniEventSpec( tunnelSpec )
cavort106A.setMonsterLevelForChildren( 1.1 )
cavort106A.spawnAllNow()

cavort106B = myRooms.Sewers_106.spawnSpawner( "cavort106B", "peelunger", 2 )
cavort106B.setWaitTime( 20, 40 )
cavort106B.setPos( 750, 520 )
cavort106B.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
cavort106B.setMiniEventSpec( tunnelSpec )
cavort106B.setMonsterLevelForChildren( 1.1 )
cavort106B.spawnAllNow()

//Room 107
cavort107 = myRooms.Sewers_107.spawnSpawner( "cavort107", "peelunger", 2 )
cavort107.setWaitTime( 20, 40 )
cavort107.setPos( 260, 620 )
cavort107.setCFH( 300 )
cavort107.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
cavort107.setMiniEventSpec( tunnelSpec )
cavort107.setMonsterLevelForChildren( 1.1 )
cavort107.spawnAllNow()

//Room 206
cavortGuard206A = myRooms.Sewers_206.spawnSpawner( "cavortGuard206A", "gramster", 1)
cavortGuard206A.setPos( 770, 390 )
cavortGuard206A.setWaitTime( 60, 90 )
cavortGuard206A.setGuardPostForChildren( "Sewers_206", 770, 390, 135 )
cavortGuard206A.setCFH( 400 )
cavortGuard206A.setMiniEventSpec( tunnelSpec )
cavortGuard206A.setMonsterLevelForChildren( 1.1 )
cavortGuard206A.forceSpawnNow()

cavortGuard206B = myRooms.Sewers_206.spawnSpawner( "cavortGuard206B", "gramster", 1)
cavortGuard206B.setPos( 970, 440 )
cavortGuard206B.setWaitTime( 60, 90 )
cavortGuard206B.setGuardPostForChildren( "Sewers_206", 970, 440, 135 )
cavortGuard206B.setCFH( 400 )
cavortGuard206B.setMiniEventSpec( tunnelSpec )
cavortGuard206B.setMonsterLevelForChildren( 1.1 )
cavortGuard206B.forceSpawnNow()

cavortGuard206A.allyWithSpawner( cavortGuard206B )

//Room 207 (the bathing room)
cavort207A = myRooms.Sewers_207.spawnSpawner( "cavort207A", "peelunger", 2 )
cavort207A.setPos( 260, 40 )
cavort207A.setWaitTime( 20, 40 )
cavort207A.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
cavort207A.setMiniEventSpec( tunnelSpec )
cavort207A.setMonsterLevelForChildren( 1.1 )
cavort207A.spawnAllNow()

cavort207B = myRooms.Sewers_207.spawnSpawner( "cavort207B", "peelunger", 2 )
cavort207B.setPos( 410, 190 )
cavort207B.setWaitTime( 20, 40 )
cavort207B.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
cavort207B.setMiniEventSpec( tunnelSpec )
cavort207B.setMonsterLevelForChildren( 1.1 )
cavort207B.spawnAllNow()

cavort207C = myRooms.Sewers_207.spawnSpawner( "cavort207C", "peelunger", 2 )
cavort207C.setWaitTime( 20, 40 )
cavort207C.setPos( 670, 140 )
cavort207C.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
cavort207C.setMiniEventSpec( tunnelSpec )
cavort207C.setMonsterLevelForChildren( 1.1 )
cavort207C.spawnAllNow()

gramGuards207 = myRooms.Sewers_207.spawnSpawner( "gramGuards207", "gramster", 3 )
gramGuards207.setPos( 820, 350 )
gramGuards207.setWaitTime( 60, 90 )
gramGuards207.setCFH( 400 )
gramGuards207.setWanderBehaviorForChildren( 50, 150, 3, 7, 150)
gramGuards207.setMiniEventSpec( tunnelSpec )
gramGuards207.setMonsterLevelForChildren( 1.2 )
gramGuards207.spawnAllNow()


//==============================================
//STAIR HALLWAY                                 
//==============================================
//Stair Patrol (206 to 305 and back)
stairPatrol = myRooms.Sewers_206.spawnStoppedSpawner( "stairPatrol", "gramster", 3 )
stairPatrol.setPos( 650, 180 )
stairPatrol.setMiniEventSpec( tunnelSpec )
stairPatrol.setMonsterLevelForChildren( 1.2 )
stairPatrol.setBaseSpeed( 140 )
stairPatrol.setChildrenToFollow( stairPatrol )
stairPatrol.addPatrolPointForSpawner( "Sewers_206", 650, 180, 10)
stairPatrol.addPatrolPointForSpawner( "Sewers_206", 950, 280, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_306", 470, 400, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_406", 160, 230, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_405", 880, 470, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_405", 100, 250, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_404", 900, 180, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_404", 710, 400, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_404", 120, 170, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_304", 470, 310, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_304", 970, 190, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_305", 660, 350, 10)
stairPatrol.addPatrolPointForSpawner( "Sewers_304", 970, 190, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_304", 470, 310, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_404", 120, 170, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_404", 710, 400, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_404", 900, 180, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_405", 100, 250, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_405", 880, 470, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_406", 160, 230, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_306", 470, 400, 0)
stairPatrol.addPatrolPointForSpawner( "Sewers_206", 950, 280, 0)
stairPatrol.startPatrol()

//Stair Patrol (305 to 206 and back)
stairPatrolReverse = myRooms.Sewers_305.spawnStoppedSpawner( "stairPatrolReverse", "gramster", 3 )
stairPatrolReverse.setPos( 660, 350 )
stairPatrolReverse.setMiniEventSpec( tunnelSpec )
stairPatrolReverse.setMonsterLevelForChildren( 1.2 )
stairPatrolReverse.setBaseSpeed( 140 )
stairPatrolReverse.setChildrenToFollow( stairPatrolReverse )
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_305", 660, 350, 10)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_304", 970, 190, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_304", 470, 310, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_404", 120, 170, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_404", 710, 400, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_404", 900, 180, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_405", 100, 250, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_405", 880, 470, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_406", 160, 230, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_306", 470, 400, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_206", 950, 280, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_206", 650, 180, 10)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_206", 950, 280, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_306", 470, 400, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_406", 160, 230, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_405", 880, 470, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_405", 100, 250, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_404", 900, 180, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_404", 710, 400, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_404", 120, 170, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_304", 470, 310, 0)
stairPatrolReverse.addPatrolPointForSpawner( "Sewers_304", 970, 190, 0)
stairPatrolReverse.startPatrol()

//stairPatrol respawn triggers
def respawn206Trigger = "respawn206Trigger"
myRooms.Sewers_206.createTriggerZone( respawn206Trigger, 490, 60, 820, 320 )

def respawn305Trigger = "respawn305Trigger"
myRooms.Sewers_305.createTriggerZone( respawn305Trigger, 600, 250, 900, 540 )

myManager.onTriggerIn( myRooms.Sewers_206, respawn206Trigger ) { event ->
	if( isSpawner( event.actor ) && event.actor == stairPatrol ) {
		if( stairPatrol.spawnsInUse() < 3 ) {
			numSpawnStairPatrol = 3 - stairPatrol.spawnsInUse()
			respawnStairPatrol()
		}
	}
}

myManager.onTriggerIn( myRooms.Sewers_305, respawn305Trigger ) { event ->
	if( isSpawner( event.actor ) && event.actor == stairPatrolReverse ) {
		if( stairPatrolReverse.spawnsInUse() < 3 ) {
			numSpawnStairPatrolReverse = 3 - stairPatrolReverse.spawnsInUse()
			respawnStairPatrolReverse()
		}
	}
}

def respawnStairPatrol() {
	if( numSpawnStairPatrol > 0 ) {
		stairPatrol.forceSpawnNow()
		numSpawnStairPatrol --
		respawnStairPatrol()
	}
}

def respawnStairPatrolReverse() {
	if( numSpawnStairPatrolReverse > 0 ) {
		stairPatrolReverse.forceSpawnNow()
		numSpawnStairPatrolReverse --
		respawnStairPatrolReverse()
	}
}

numSpawnStairPatrol = 3
respawnStairPatrol()
numSpawnStairPatrolReverse = 3
respawnStairPatrolReverse()

//A couple of loners just for color
hallwayLoner405 = myRooms.Sewers_405.spawnSpawner( "hallwayLoner405", "gramster", 1)
hallwayLoner405.setPos( 100, 320 )
hallwayLoner405.setWaitTime( 60, 90 )
hallwayLoner405.setMiniEventSpec( tunnelSpec )
hallwayLoner405.setWanderBehaviorForChildren( 50, 150, 3, 7, 200)
hallwayLoner405.setMonsterLevelForChildren( 1.2 )
hallwayLoner405.forceSpawnNow()

hallwayLoner404 = myRooms.Sewers_404.spawnSpawner( "hallwayLoner404", "gramster", 2)
hallwayLoner404.setPos( 560, 520 )
hallwayLoner404.setWaitTime( 60, 90 )
hallwayLoner404.setMiniEventSpec( tunnelSpec )
hallwayLoner404.setWanderBehaviorForChildren( 50, 150, 3, 7, 200)
hallwayLoner404.setMonsterLevelForChildren( 1.2 )
hallwayLoner404.forceSpawnNow()


//==============================================
//CAMP HOLDING PEN                              
//==============================================
//304 is a camp. (Peelunger holding pen. Surrounded by Gramsters.)

//the peelungers in the holding pen
holdingPenA = myRooms.Sewers_304.spawnSpawner( "holdingPenA", "peelunger", 3 )
holdingPenA.setPos( 440, 70 )
holdingPenA.setWaitTime( 60, 90 )
holdingPenA.setCFH( 400 )
holdingPenA.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
holdingPenA.setMiniEventSpec( tunnelSpec )
holdingPenA.setMonsterLevelForChildren( 1.2 )
holdingPenA.spawnAllNow()

holdingPenB = myRooms.Sewers_304.spawnSpawner( "holdingPenB", "peelunger", 3 )
holdingPenB.setPos( 770, 160 )
holdingPenB.setWaitTime( 60, 90 )
holdingPenB.setCFH( 400 )
holdingPenB.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
holdingPenB.setMiniEventSpec( tunnelSpec )
holdingPenB.setMonsterLevelForChildren( 1.2 )
holdingPenB.spawnAllNow()

holdingPenA.allyWithSpawner( holdingPenB )

//gramster guards
gramGuard304A = myRooms.Sewers_304.spawnSpawner( "gramGuard304A", "gramster", 1)
gramGuard304A.setPos( 160, 190 )
gramGuard304A.setWaitTime( 60, 90 )
gramGuard304A.setGuardPostForChildren( "Sewers_304", 160, 190, 135 )
gramGuard304A.setCFH( 400 )
gramGuard304A.setMiniEventSpec( tunnelSpec )
gramGuard304A.setMonsterLevelForChildren( 1.2 )
gramGuard304A.forceSpawnNow()

gramGuard304B = myRooms.Sewers_304.spawnSpawner( "gramGuard304B", "gramster", 1)
gramGuard304B.setPos( 310, 250 )
gramGuard304B.setWaitTime( 60, 90 )
gramGuard304B.setGuardPostForChildren( "Sewers_304", 340, 250, 135 )
gramGuard304B.setCFH( 400 )
gramGuard304B.setMiniEventSpec( tunnelSpec )
gramGuard304B.setMonsterLevelForChildren( 1.2 )
gramGuard304B.forceSpawnNow()

gramGuard304C = myRooms.Sewers_304.spawnSpawner( "gramGuard304C", "gramster", 1)
gramGuard304C.setPos( 520, 320 )
gramGuard304C.setWaitTime( 60, 90 )
gramGuard304C.setGuardPostForChildren( "Sewers_304", 570, 320, 135 )
gramGuard304C.setCFH( 400 )
gramGuard304C.setMiniEventSpec( tunnelSpec )
gramGuard304C.setMonsterLevelForChildren( 1.2 )
gramGuard304C.forceSpawnNow()

gramGuard304D = myRooms.Sewers_304.spawnSpawner( "gramGuard304D", "gramster", 1)
gramGuard304D.setPos( 790, 330 )
gramGuard304D.setWaitTime( 60, 90 )
gramGuard304D.setGuardPostForChildren( "Sewers_304", 790, 330, 90 )
gramGuard304D.setCFH( 400 )
gramGuard304D.setMiniEventSpec( tunnelSpec )
gramGuard304D.setMonsterLevelForChildren( 1.2 )
gramGuard304D.forceSpawnNow()

gramGuard304E = myRooms.Sewers_304.spawnSpawner( "gramGuard304E", "gramster", 1)
gramGuard304E.setPos( 1000, 200 )
gramGuard304E.setWaitTime( 60, 90 )
gramGuard304E.setGuardPostForChildren( "Sewers_304", 1000, 200, 45 )
gramGuard304E.setCFH( 400 )
gramGuard304E.setMiniEventSpec( tunnelSpec )
gramGuard304E.setMonsterLevelForChildren( 1.2 )
gramGuard304E.forceSpawnNow()

gramGuard304A.allyWithSpawner( gramGuard304B )
gramGuard304A.allyWithSpawner( gramGuard304C )
gramGuard304A.allyWithSpawner( gramGuard304D )
gramGuard304A.allyWithSpawner( gramGuard304E )
gramGuard304B.allyWithSpawner( gramGuard304C )
gramGuard304B.allyWithSpawner( gramGuard304D )
gramGuard304B.allyWithSpawner( gramGuard304E )
gramGuard304C.allyWithSpawner( gramGuard304D )
gramGuard304C.allyWithSpawner( gramGuard304E )
gramGuard304D.allyWithSpawner( gramGuard304E )

//==============================================
//CAMP TRIAD                                    
//==============================================
//2-3-102 area is a set of three camps where it is *very* easy to make aggro go bad.

//CAMPTRIAD2 (many peelungers)
triadPeelunger2A = myRooms.Sewers_2.spawnSpawner( "triadPeelunger2A", "peelunger", 2 )
triadPeelunger2A.setPos( 440, 630 )
triadPeelunger2A.setWaitTime( 20, 40 )
triadPeelunger2A.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
triadPeelunger2A.setMiniEventSpec( tunnelSpec )
triadPeelunger2A.setMonsterLevelForChildren( 1.1 )
triadPeelunger2A.spawnAllNow()

triadPeelunger2B = myRooms.Sewers_2.spawnSpawner( "triadPeelunger2B", "peelunger", 2 )
triadPeelunger2B.setPos( 670, 420 )
triadPeelunger2B.setWaitTime( 20, 40 )
triadPeelunger2B.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
triadPeelunger2B.setMiniEventSpec( tunnelSpec )
triadPeelunger2B.setMonsterLevelForChildren( 1.1 )
triadPeelunger2B.spawnAllNow()

gramsters2A = myRooms.Sewers_2.spawnSpawner( "gramsters2A", "gramster", 2 )
gramsters2A.setPos( 860, 330 )
gramsters2A.setWaitTime( 60, 90 )
gramsters2A.setCFH( 400 )
gramsters2A.setWanderBehaviorForChildren( 50, 150, 3, 7, 150)
gramsters2A.setMiniEventSpec( tunnelSpec )
gramsters2A.setMonsterLevelForChildren( 1.1 )
gramsters2A.spawnAllNow()

gramsters2B = myRooms.Sewers_2.spawnSpawner( "gramsters2B", "gramster", 2 )
gramsters2B.setPos( 830, 520 )
gramsters2B.setWaitTime( 60, 90 )
gramsters2B.setCFH( 400 )
gramsters2B.setWanderBehaviorForChildren( 50, 150, 3, 7, 150)
gramsters2B.setMiniEventSpec( tunnelSpec )
gramsters2B.setMonsterLevelForChildren( 1.1 )
gramsters2B.spawnAllNow()

//CAMPTRIAD3 (small one)
gramsters3 = myRooms.Sewers_3.spawnSpawner( "gramsters3", "gramster", 3 )
gramsters3.setPos( 180, 410 )
gramsters3.setWaitTime( 60, 90 )
gramsters3.setCFH( 400 )
gramsters3.setWanderBehaviorForChildren( 50, 150, 3, 7, 150)
gramsters3.setMiniEventSpec( tunnelSpec )
gramsters3.setMonsterLevelForChildren( 1.1 )
gramsters3.spawnAllNow()

//CAMPTRIAD102 (mostly grams)
triadPeelunger102 = myRooms.Sewers_102.spawnSpawner( "triadPeelunger102", "peelunger", 3 )
triadPeelunger102.setPos( 360, 70 )
triadPeelunger102.setWaitTime( 20, 40 )
triadPeelunger102.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
triadPeelunger102.setMiniEventSpec( tunnelSpec )
triadPeelunger102.setMonsterLevelForChildren( 1.1 )
triadPeelunger102.spawnAllNow()

gramsters102A = myRooms.Sewers_102.spawnSpawner( "gramsters102A", "gramster", 2 )
gramsters102A.setPos( 910, 260 )
gramsters102A.setWaitTime( 60, 90 )
gramsters102A.setCFH( 400 )
gramsters102A.setWanderBehaviorForChildren( 50, 150, 3, 7, 150)
gramsters102A.setMiniEventSpec( tunnelSpec )
gramsters102A.setMonsterLevelForChildren( 1.1 )
gramsters102A.spawnAllNow()

gramsters102B = myRooms.Sewers_102.spawnSpawner( "gramsters102B", "gramster", 2 )
gramsters102B.setPos( 450, 450 )
gramsters102B.setWaitTime( 60, 90 )
gramsters102B.setCFH( 400 )
gramsters102B.setWanderBehaviorForChildren( 50, 150, 3, 7, 150)
gramsters102B.setMiniEventSpec( tunnelSpec )
gramsters102B.setMonsterLevelForChildren( 1.1 )
gramsters102B.spawnAllNow()


//==============================================
//CIRCLE SPAWNER                                
//==============================================
circleSpawner = myRooms.Sewers_403.spawnSpawner( "circleSpawner", "gramster", 6 )
circleSpawner.setPos( 680, 280 )
circleSpawner.setWaitTime( 60, 90 )
circleSpawner.setWanderBehaviorForChildren( 50, 100, 3, 7, 150)
circleSpawner.setMiniEventSpec( tunnelSpec )
circleSpawner.setMonsterLevelForChildren( 1.3 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 770, 170, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 640, 140, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 500, 160, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 430, 260, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 470, 340, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 560, 410, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 730, 430, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 900, 400, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 940, 300, 0 )
circleSpawner.addPatrolPointForChildren( "Sewers_403", 880, 220, 0 )
circleSpawner.stopSpawning()

circleBoss = myRooms.Sewers_403.spawnSpawner( "circleBoss", "gramster_LT", 1)
circleBoss.setPos( 680, 280 )
circleBoss.setGuardPostForChildren( "Sewers_403", 680, 280, 135 )
circleBoss.setCFH( 400 )
circleBoss.setMiniEventSpec( tunnelSpec )
circleBoss.setMonsterLevelForChildren( 1.3 )
circleBoss.forceSpawnNow()

numCircle = 0
circleSpawn()

def circleSpawn() {
	if( numCircle < 6 ) {
		circleSpawner.forceSpawnNow()
		numCircle ++
		myManager.schedule(1.3){ circleSpawn() }
	}
}
	

//==============================================
//CAMP BOSS                                     
//==============================================
//301 - 402 area is the "big, bad Alpha Gram nest".
//This area has guards that will attempt to run to the *opposite* areas to pull non-aggroed friends from. (Long tether and hate radii.) Southern guards run to each of the three //Triad Camps. Northern guards run to 403 to pull from the reserves and then north to the Peelunger Pen.

//playing peelungers
playingPeelungers202 = myRooms.Sewers_202.spawnSpawner( "playingPeelungers202", "peelunger", 3 )
playingPeelungers202.setPos( 340, 230 )
playingPeelungers202.setWaitTime( 20, 40 )
playingPeelungers202.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
playingPeelungers202.setMiniEventSpec( tunnelSpec )
playingPeelungers202.setMonsterLevelForChildren( 1.2 )
playingPeelungers202.spawnAllNow()

playingPeelungers203 = myRooms.Sewers_203.spawnSpawner( "playingPeelungers203", "peelunger", 2 )
playingPeelungers203.setPos( 560, 650 )
playingPeelungers203.setWaitTime( 20, 40 )
playingPeelungers203.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
playingPeelungers203.setMiniEventSpec( tunnelSpec )
playingPeelungers203.setMonsterLevelForChildren( 1.2 )
playingPeelungers203.spawnAllNow()

playingPeelungers303 = myRooms.Sewers_303.spawnSpawner( "playingPeelungers303", "peelunger", 2 )
playingPeelungers303.setPos( 540, 40 )
playingPeelungers303.setWaitTime( 20, 40 )
playingPeelungers303.setWanderBehaviorForChildren( 50, 100, 2, 4, 150)
playingPeelungers303.setMiniEventSpec( tunnelSpec )
playingPeelungers303.setMonsterLevelForChildren( 1.2 )
playingPeelungers303.spawnAllNow()

//Guards
bossGuard202A = myRooms.Sewers_102.spawnSpawner( "bossGuard202A", "gramster", 1)
bossGuard202A.setPos( 620, 550 )
bossGuard202A.setWaitTime( 60, 90 )
bossGuard202A.setGuardPostForChildren( "Sewers_202", 680, 420, 315 )
bossGuard202A.setHomeTetherForChildren( 2000 )
bossGuard202A.setHomeForChildren( "Sewers_102", 330, 370 )
bossGuard202A.setRFH( true )
bossGuard202A.setCFH( 400 )
bossGuard202A.setDispositionForChildren( "coward" )
bossGuard202A.setCowardLevelForChildren( 100 )
bossGuard202A.setMiniEventSpec( tunnelSpec )
bossGuard202A.setMonsterLevelForChildren( 1.2 )
bossGuard202A.forceSpawnNow()

bossGuard202B = myRooms.Sewers_102.spawnSpawner( "bossGuard202B", "gramster", 1)
bossGuard202B.setPos( 620, 550 )
bossGuard202B.setWaitTime( 60, 90 )
bossGuard202B.setGuardPostForChildren( "Sewers_202", 840, 460, 315 )
bossGuard202B.setHomeTetherForChildren( 2000 )
bossGuard202B.setHomeForChildren( "Sewers_102", 330, 370 )
bossGuard202B.setCFH( 400 )
bossGuard202B.setMiniEventSpec( tunnelSpec )
bossGuard202B.setMonsterLevelForChildren( 1.2 )
bossGuard202B.forceSpawnNow()

bossGuard303A = myRooms.Sewers_402.spawnSpawner( "bossGuard303A", "gramster", 1)
bossGuard303A.setPos( 390, 170 )
bossGuard303A.setWaitTime( 60, 90 )
bossGuard303A.setGuardPostForChildren( "Sewers_303", 450, 210, 315 )
bossGuard303A.setHomeTetherForChildren( 2000 )
bossGuard303A.setHomeForChildren( "Sewers_402", 660, 170 )
bossGuard303A.setRFH( true )
bossGuard303A.setCFH( 400 )
bossGuard303A.setDispositionForChildren( "coward" )
bossGuard303A.setMiniEventSpec( tunnelSpec )
bossGuard303A.setMonsterLevelForChildren( 1.2 )
bossGuard303A.forceSpawnNow()

bossGuard303B = myRooms.Sewers_402.spawnSpawner( "bossGuard303B", "gramster", 1)
bossGuard303B.setPos( 390, 170 )
bossGuard303B.setWaitTime( 60, 90 )
bossGuard303B.setGuardPostForChildren( "Sewers_303", 500, 350, 45 )
bossGuard303B.setHomeTetherForChildren( 2000 )
bossGuard303B.setCFH( 400 )
bossGuard303B.setMiniEventSpec( tunnelSpec )
bossGuard303B.setMonsterLevelForChildren( 1.2 )
bossGuard303B.forceSpawnNow()

//Criss-cross Guard Patrols
rightToLeft = myRooms.Sewers_301.spawnStoppedSpawner( "rightToLeft", "gramster", 3 )
rightToLeft.setPos( 600, 170 )
rightToLeft.setMiniEventSpec( tunnelSpec )
rightToLeft.setMonsterLevelForChildren( 1.3 )
rightToLeft.setBaseSpeed( 140 )
rightToLeft.setChildrenToFollow( rightToLeft )
rightToLeft.addPatrolPointForSpawner( "Sewers_301", 600, 170, 10)
rightToLeft.addPatrolPointForSpawner( "Sewers_301", 940, 340, 0)
rightToLeft.addPatrolPointForSpawner( "Sewers_302", 230, 430, 0)
rightToLeft.addPatrolPointForSpawner( "Sewers_302", 720, 600, 0)
rightToLeft.addPatrolPointForSpawner( "Sewers_402", 800, 150, 10)
rightToLeft.addPatrolPointForSpawner( "Sewers_302", 720, 600, 0)
rightToLeft.addPatrolPointForSpawner( "Sewers_302", 230, 430, 0)
rightToLeft.addPatrolPointForSpawner( "Sewers_301", 940, 340, 0)
rightToLeft.startPatrol()

leftToRight = myRooms.Sewers_402.spawnStoppedSpawner( "leftToRight", "gramster", 3 )
leftToRight.setPos( 800, 150 )
leftToRight.setMiniEventSpec( tunnelSpec )
leftToRight.setMonsterLevelForChildren( 1.3 )
leftToRight.setBaseSpeed( 140 )
leftToRight.setChildrenToFollow( leftToRight )
leftToRight.addPatrolPointForSpawner( "Sewers_402", 800, 150, 10)
leftToRight.addPatrolPointForSpawner( "Sewers_302", 720, 600, 0)
leftToRight.addPatrolPointForSpawner( "Sewers_302", 230, 430, 0)
leftToRight.addPatrolPointForSpawner( "Sewers_301", 940, 340, 0)
leftToRight.addPatrolPointForSpawner( "Sewers_301", 600, 170, 10)
leftToRight.addPatrolPointForSpawner( "Sewers_301", 940, 340, 0)
leftToRight.addPatrolPointForSpawner( "Sewers_302", 230, 430, 0)
leftToRight.addPatrolPointForSpawner( "Sewers_302", 720, 600, 0)
leftToRight.startPatrol()

//Criss-cross respawn routines
def respawn301Trigger = "respawn301Trigger"
myRooms.Sewers_301.createTriggerZone( respawn301Trigger, 300, 50, 640, 250 )

def respawn402Trigger = "respawn402Trigger"
myRooms.Sewers_402.createTriggerZone( respawn402Trigger, 470, 70, 890, 230 )

myManager.onTriggerIn( myRooms.Sewers_301, respawn301Trigger ) { event ->
	if( isSpawner( event.actor ) && event.actor == rightToLeft ) {
		if( rightToLeft.spawnsInUse() < 3 ) {
			numSpawn301 = 3 - rightToLeft.spawnsInUse()
			respawn301Patrol()
		}
	}
}

myManager.onTriggerIn( myRooms.Sewers_402, respawn402Trigger ) { event ->
	if( isSpawner( event.actor ) && event.actor == leftToRight ) {
		if( leftToRight.spawnsInUse() < 3 ) {
			numSpawn402 = 3 - leftToRight.spawnsInUse()
			respawn402Patrol()
		}
	}
}

def respawn301Patrol() {
	if( numSpawn301 > 0 ) {
		rightToLeft.forceSpawnNow()
		numSpawn301 --
		respawn301Patrol()
	}
}

def respawn402Patrol() {
	if( numSpawn402 > 0 ) {
		leftToRight.forceSpawnNow()
		numSpawn402 --
		respawn402Patrol()
	}
}

numSpawn301 = 3
respawn301Patrol()

numSpawn402 = 3
respawn402Patrol()

//Boss Camp 301
//One LT & three grunts
bossGuard301A = myRooms.Sewers_301.spawnSpawner( "bossGuard301A", "gramster", 1)
bossGuard301A.setPos( 230, 310 )
bossGuard301A.setGuardPostForChildren( "Sewers_301", 230, 310, 45 )
bossGuard301A.setCFH( 400 )
bossGuard301A.setMiniEventSpec( tunnelSpec )
bossGuard301A.setMonsterLevelForChildren( 1.3 )

bossGuard301B = myRooms.Sewers_301.spawnSpawner( "bossGuard301B", "gramster", 1)
bossGuard301B.setPos( 350, 320 )
bossGuard301B.setGuardPostForChildren( "Sewers_301", 350, 320, 45 )
bossGuard301B.setCFH( 400 )
bossGuard301B.setMiniEventSpec( tunnelSpec )
bossGuard301B.setMonsterLevelForChildren( 1.3 )

bossGuard301C = myRooms.Sewers_301.spawnSpawner( "bossGuard301C", "gramster", 1)
bossGuard301C.setPos( 440, 420 )
bossGuard301C.setGuardPostForChildren( "Sewers_301", 440, 420, 315 )
bossGuard301C.setCFH( 400 )
bossGuard301C.setMiniEventSpec( tunnelSpec )
bossGuard301C.setMonsterLevelForChildren( 1.3 )

boss301 = myRooms.Sewers_301.spawnSpawner( "boss301", "gramster_LT", 1)
boss301.setPos( 250, 420 )
boss301.setWaitTime( 60, 90 )
boss301.setGuardPostForChildren( "Sewers_301", 190, 400, 45 )
boss301.setCFH( 400 )
boss301.setMiniEventSpec( tunnelSpec )
boss301.setMonsterLevelForChildren( 1.4 )

bossGuard301A.allyWithSpawner( bossGuard301B )
bossGuard301A.allyWithSpawner( bossGuard301C )
bossGuard301B.allyWithSpawner( bossGuard301C )

boss301.allyWithSpawner( bossGuard301A )
boss301.allyWithSpawner( bossGuard301B )
boss301.allyWithSpawner( bossGuard301C )

//Boss Camp 402
//Two LTs
boss402A = myRooms.Sewers_402.spawnSpawner( "boss402A", "gramster_LT", 1)
boss402A.setPos( 530, 190 )
boss402A.setGuardPostForChildren( "Sewers_402", 530, 190, 315 )
boss402A.setCFH( 400 )
boss402A.setMiniEventSpec( tunnelSpec )
boss402A.setMonsterLevelForChildren( 1.4 )

boss402B = myRooms.Sewers_402.spawnSpawner( "boss402B", "gramster_LT", 1)
boss402B.setPos( 730, 250 )
boss402B.setGuardPostForChildren( "Sewers_402", 730, 250, 315 )
boss402B.setCFH( 400 )
boss402B.setMiniEventSpec( tunnelSpec )
boss402B.setMonsterLevelForChildren( 1.4 )

boss402A.allyWithSpawner( boss402B )

//Outlying Guard Alliances for Boss Camps
bossGuard202A.allyWithSpawner( bossGuard202B )
bossGuard202A.allyWithSpawner( bossGuard301A )
bossGuard202A.allyWithSpawner( bossGuard301B )
bossGuard202A.allyWithSpawner( bossGuard301C  )
bossGuard202A.allyWithSpawner( boss301 )

bossGuard303A.allyWithSpawner( bossGuard303B )
bossGuard303A.allyWithSpawner( boss402A )
bossGuard303A.allyWithSpawner( boss402B )



//==============================================
//VALVE GAME                                    
//==============================================

//Valve 1
valve1 = makeSwitch( "checkpoint1", myRooms.Sewers_202, 230, 145 )
valve1.unlock()
valve1.off()
valve1.setRange( 200 )
valve1.setMouseoverText("Overflow Valve")

def turnValveOne = { event ->
	if( event.actor.getConLevel() < 2.1 ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z14ValveOneTurned" ) ) {
			valve1.off()
			event.actor.setQuestFlag( GLOBAL, "Z14ValveOneTurned" )
			event.actor.centerPrint( "The valve turns and you hear water sluice through the pipes." )
			event.actor.removeMiniMapQuestLocation( "Z14ValveOneFlag" )
			checkForReward( event )
		} else {
			event.actor.centerPrint( "This valve is already opened!" )
		}
	} else {
		event.actor.centerPrint( "You need to change your level to 2.0 or lower to do things here in the Waterworks." )
	}
}
	
valve1.whenOn( turnValveOne )


//Valve 2
valve2 = makeSwitch( "checkpoint2", myRooms.Sewers_403, 290, 165 )
valve2.unlock()
valve2.off()
valve2.setRange( 200 )
valve2.setMouseoverText("Overflow Valve")

def turnValveTwo = { event ->
	if( event.actor.getConLevel() < 2.1 ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z14ValveTwoTurned" ) ) {
			valve2.off()
			event.actor.setQuestFlag( GLOBAL, "Z14ValveTwoTurned" )
			event.actor.centerPrint( "The valve turns and you hear water sluice through the pipes." )
			event.actor.removeMiniMapQuestLocation( "Z14ValveTwoFlag" )
			checkForReward( event )
		} else {
			event.actor.centerPrint( "This valve is already opened!" )
		}
	} else {
		event.actor.centerPrint( "You need to change your level to 2.0 or lower to do things here in the Waterworks." )
	}
}
	
valve2.whenOn( turnValveTwo )

//Valve 3
valve3 = makeSwitch( "checkpoint3", myRooms.Sewers_305, 805, 320 )
valve3.unlock()
valve3.off()
valve3.setRange( 200 )
valve3.setMouseoverText("Overflow Valve")

def turnValveThree = { event ->
	if( event.actor.getConLevel() < 2.1 ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z14ValveThreeTurned" ) ) {
			valve3.off()
			event.actor.setQuestFlag( GLOBAL, "Z14ValveThreeTurned" )
			event.actor.centerPrint( "The valve turns and you hear water sluice through the pipes." )
			event.actor.removeMiniMapQuestLocation( "Z14ValveThreeFlag" )
			checkForReward( event )
		} else {
			event.actor.centerPrint( "This valve is already opened!" )
		}
	} else {
		event.actor.centerPrint( "You need to change your level to 2.0 or lower to do things here in the Waterworks." )
	}
}
	
valve3.whenOn( turnValveThree )

//Valve 4
valve4 = makeSwitch( "checkpoint4", myRooms.Sewers_207, 905, 215 )
valve4.unlock()
valve4.off()
valve4.setRange( 200 )
valve4.setMouseoverText("Overflow Valve")

def turnValveFour = { event ->
	if( event.actor.getConLevel() < 2.1 ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z14ValveFourTurned" ) ) {
			valve4.off()
			event.actor.setQuestFlag( GLOBAL, "Z14ValveFourTurned" )
			event.actor.centerPrint( "The valve turns and you hear water sluice through the pipes." )
			event.actor.removeMiniMapQuestLocation( "Z14ValveFourFlag" )
			checkForReward( event )
		} else {
			event.actor.centerPrint( "This valve is already opened!" )
		}
	} else {
		event.actor.centerPrint( "You need to change your level to 2.0 or lower to do things here in the Waterworks." )
	}
}
	
valve4.whenOn( turnValveFour )


//Give a reward when all four valves have been turned
def checkForReward( event ) {
	if( event.actor.hasQuestFlag( GLOBAL, "Z14ValveOneTurned" ) && event.actor.hasQuestFlag( GLOBAL, "Z14ValveTwoTurned" ) && event.actor.hasQuestFlag( GLOBAL, "Z14ValveThreeTurned" ) && event.actor.hasQuestFlag( GLOBAL, "Z14ValveFourTurned" ) ) {
		
		myManager.schedule(2) {
			event.actor.centerPrint( "The Barton Maintenance Commission sends you a reward for clearing the pipes!" )

			//grant a reward for turning all four valves
			//gold reward
			goldGrant = random( 150, 300 ) 
			event.actor.centerPrint( "You receive ${goldGrant} gold!" )
			event.actor.grantCoins( goldGrant )

			//orb reward
			orbGrant = random( 3, 6 )
			event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
			event.actor.grantQuantityItem( 100257, orbGrant )
		}
	}
}
	

//===================================
//BOSS TREASURE CHESTS               
//===================================

//CHEST 301
chest301 = makeSwitch( "chest301", myRooms.Sewers_301, 150, 360 )
chest301.lock()
chest301.off()
chest301.setUsable( false )

playerSet301 = [] as Set
hateCollector301 = [] as Set
zone = 14 //Sewers

def openChest301() {
	//look at every player in the room and grant them loot after the chest opens
	myRooms.Sewers_301.getActorList().each { if( isPlayer( it ) ) { playerSet301 << it } }
	playerSet301.each{
		if( hateCollector301.contains( it ) ) {
			chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe

			//scale the returned results down by relative con levels if the player is below the normal CL for the area
			if( it.getConLevel() > CLMap[zone] ) {
				lootMultiplier = 1.0
			} else {
				lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
			}

			//grant gold
			goldGrant = goldMap[zone] 
			goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
			it.grantCoins( ( goldGrant * lootMultiplier * chestType ).intValue() ) 

			//grant common items
			chance = (commonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				commonGrant = random( commonMap[zone] )
				it.grantItem( commonGrant )
			}

			//grant uncommon items
			chance = (uncommonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				uncommonGrant = random( uncommonMap[zone] )
				it.grantItem( uncommonGrant )
			}

			//grant recipes
			chance = (recipeChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				recipeGrant = random( recipeMap[zone] )
				it.grantItem( recipeGrant )
			}

			//grant orbs
			chance = (orbChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				orbGrant = orbMap[zone]
				orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType ).intValue()
				it.grantQuantityItem( 100257, orbGrant )
			}

			//grant rings
			chance = (ringChance * chestType).intValue()
			roll = random( 1, 100 )
			if( roll <= chance ) {
				ringGrant = random( ringMap[zone] )
				it.grantRing( ringGrant, true )
			}
		} else {
			if( it.getConLevel() > CLMap[zone]+1 ) {
				it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
			} else {
				it.centerPrint( "You did not help defeat the creatures protecting this container. No risk = no reward." )
			}
		}
	}
}

//Respawn Logic
def checkForChest301Unlock() {
	if( bossGuard301A.spawnsInUse() + bossGuard301B.spawnsInUse() + bossGuard301C.spawnsInUse() + boss301.spawnsInUse() == 0 ) {
		myManager.schedule(3) {
			chest301.on()
			openChest301()
			//give the tutorial for chest opening
			hateCollector301.each{
				if( it.hasQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" ) && !it.hasQuestFlag( GLOBAL, "Z14ChestOpeningTutorial" ) ) {
					it.setQuestFlag( GLOBAL, "Z14ChestOpeningTutorial" )
					tutorialNPC.pushDialog( it, "chestOpening" )
				}
			}

			//respawn the camp after a suitable delay
			myManager.schedule( random( 300, 600) ) {
				//close and reset the chest
				chest301.off()
				hateCollector301.clear()
				playerSet301.clear()
				gramster301 = bossGuard301A.forceSpawnNow()
				runOnDeath( gramster301, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )
				gramster301 = bossGuard301B.forceSpawnNow()
				runOnDeath( gramster301, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )
				gramster301 = bossGuard301C.forceSpawnNow()
				runOnDeath( gramster301, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )
				boss3 = boss301.forceSpawnNow()
				runOnDeath( boss3, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )

				//start watching to unlock the container again
				checkForChest301Unlock()
			}
		}
	} else {
		myManager.schedule(2) { checkForChest301Unlock() }
	}
}

//STARTUP LOGIC
gramster301 = bossGuard301A.forceSpawnNow()
runOnDeath( gramster301, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )
gramster301 = bossGuard301B.forceSpawnNow()
runOnDeath( gramster301, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )
gramster301 = bossGuard301C.forceSpawnNow()
runOnDeath( gramster301, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )
boss3 = boss301.forceSpawnNow()
runOnDeath( boss3, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector301 << it } } } )

checkForChest301Unlock()


//CHEST 402
chest402 = makeSwitch( "chest402", myRooms.Sewers_402, 530, 260 )
chest402.lock()
chest402.off()
chest402.setUsable( false )

playerSet402 = [] as Set
hateCollector402 = [] as Set

def openChest402() {
	//look at every player in the room and grant them loot after the chest opens
	myRooms.Sewers_402.getActorList().each { if( isPlayer( it ) ) { playerSet402 << it } }
	playerSet402.each{
		if( hateCollector402.contains( it ) ) {
			chestType = 3 //1 = basket; 2 = crate; 3 = chest; 4 = safe

			//scale the returned results down by relative con levels if the player is below the normal CL for the area
			if( it.getConLevel() > CLMap[zone] ) {
				lootMultiplier = 1.0
			} else {
				lootMultiplier = it.getConLevel() / CLMap[zone] //the lower the overall CL of a player in relation to the zone CL, the lower the reward they receive (always receive a reward appropriate to their level).
			}

			//grant gold
			goldGrant = goldMap[zone] 
			goldGrant = random( (goldGrant * 0.5).intValue(), goldGrant ) 
			it.grantCoins( ( goldGrant * lootMultiplier * chestType ).intValue() ) 

			//grant common items
			chance = (commonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				commonGrant = random( commonMap[zone] )
				it.grantItem( commonGrant )
			}

			//grant uncommon items
			chance = (uncommonChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				uncommonGrant = random( uncommonMap[zone] )
				it.grantItem( uncommonGrant )
			}

			//grant recipes
			chance = (recipeChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				recipeGrant = random( recipeMap[zone] )
				it.grantItem( recipeGrant )
			}

			//grant orbs
			chance = (orbChance * chestType).intValue()
			roll = random( 100 )
			if( roll <= chance ) {
				orbGrant = orbMap[zone]
				orbGrant = ( random( (orbGrant * 0.5).intValue(), orbGrant ) * lootMultiplier * chestType ).intValue()
				it.grantQuantityItem( 100257, orbGrant )
			}

			//grant rings
			chance = (ringChance * chestType).intValue()
			roll = random( 1, 100 )
			if( roll <= chance ) {
				ringGrant = random( ringMap[zone] )
				it.grantRing( ringGrant, true )
			}
		} else {
			if( it.getConLevel() > CLMap[zone]+1 ) {
				it.centerPrint( "Your level is too high for loot from this container. Change your level to below ${(CLMap[zone]+1).intValue() * 1.0} when fighting here." )
			} else {
				it.centerPrint( "You did not help defeat the creatures protecting this container. No risk = no reward." )
			}
		}
	}
}

//Respawn Logic
def checkForChest402Unlock() {
	if( boss402A.spawnsInUse() + boss402B.spawnsInUse() == 0 ) {
		myManager.schedule(3) {
			chest402.on()
			openChest402()
			//give the tutorial for chest opening
			hateCollector402.each{
				if( it.hasQuestFlag( GLOBAL, "Z14TunnelTutorialsOn" ) && !it.hasQuestFlag( GLOBAL, "Z14ChestOpeningTutorial" ) ) {
					it.setQuestFlag( GLOBAL, "Z14ChestOpeningTutorial" )
					tutorialNPC.pushDialog( it, "chestOpening" )
				}
			}
			//respawn the camp after a suitable delay
			myManager.schedule( random( 300, 600) ) {
				//close and reset the chest
				chest402.off()
				hateCollector402.clear()
				playerSet402.clear()
				boss402 = boss402A.forceSpawnNow()
				runOnDeath( boss402, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector402 << it } } } )
				boss402 = boss402B.forceSpawnNow()
				runOnDeath( boss402, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector402 << it } } } )

				//start watching to unlock the container again
				checkForChest402Unlock()
			}
		}
	} else {
		myManager.schedule(2) { checkForChest402Unlock() }
	}
}

//STARTUP LOGIC
boss402 = boss402A.forceSpawnNow()
runOnDeath( boss402, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector402 << it } } } )
boss402 = boss402B.forceSpawnNow()
runOnDeath( boss402, { event -> event.actor.getHated().each{ if( isPlayer(it) && it.getConLevel() <= CLMap[zone]+1 ) { hateCollector402 << it } } } )

checkForChest402Unlock()


//====================================================================================
//====================================================================================
commonChance = 20 
uncommonChance = 10 
recipeChance = 1
orbChance = 5
ringChance = 1 

CLMap = [ 2:1, 3:2, 4:2.5, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

goldMap = [ 2:10, 3:20, 4:30, 5:40, 6:50, 7:60, 8:70, 9:80, 10:90, 11:100, 14:10, 16:90, 18:80 ]

commonMap = [ 2:["100272", "100289", "100385", "100297", "100397"], 3:["100291", "100262", "100263", "100298"], 4:["100388", "100278", "100275", "100283"], 5:["100281", "100367", "100411", "100394", "100284", "100405", "100267"], 6:["100265", "100285", "100398", "100397", "100376", "100296"], 7:["100373", "100260"], 8:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 9:["100290", "100273", "100408", "100392", "100370"], 10:["100393", "100293", "100259", "100292"], 11:["100371", "100294", "100391", "100277", "100290", "100368", "100386", "100403"], 14:[0], 16:["100287", "100409"], 18:["100393", "100293", "100259", "100292"] ]

uncommonMap = [ 2:["100280", "100279", "100270", "100380", "100384"], 3:["100378", "100261", "100271"], 4:["100276", "100258", "100268"], 5:["100381", "100382", "100282", "100383"], 6:["100390", "100299", "100286", "100369", "100400", "100387"], 7:["100365", "100410"], 8:["100389", "100264", "100406", "100399", "100402"], 9:["100389", "100264", "100406", "100399", "100402"], 10:["100395", "100413", "100407", "100266"], 11:["100396", "100269", "100401", "100372", "100375", "100366", "100379", "100274"], 14:[0], 16:["100404", "100288"], 18:["100395", "100413", "100407", "100266"] ]

recipeMap = [ 2:["17766", "17764", "17772", "17758", "17756"], 3:["17861", "17857", "17755"], 4:["17848", "17849", "17852", "17851", "17850", "17753"], 5:["17833", "17831", "17754", "17836", "17835"], 6:["17774", "17824", "17780", "17823", "17845", "17778", "17752", "17777", "17822", "17779"], 7:["17785", "17793", "17788", "17787"], 8:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 9:["17789", "17757", "17792", "17805", "17801", "17802", "17820", "17800"], 10:["17796", "17846", "17803", "17844"], 11:["17819", "17816", "17811", "17808", "17806", "17812", "17813", "17814", "17810", "17809", "17815"], 14:[0], 16:["17786", "17791", "17790", "17794"], 18:["17796", "17846", "17803", "17844"] ]

orbMap = [ 2:1, 3:2, 4:3, 5:3, 6:4, 7:5, 8:6, 9:7, 10:8, 11:9, 14:1, 16:8, 18:7 ]

ringMap = [ 2:["17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 3:["17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 4:["17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 5:["17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 6:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 7:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 8:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 9:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 10:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 11:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 14:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 16:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"], 18:["17716", "17722", "17723", "17737", "17738", "17741", "17712", "17715", "17719", "17736", "17742", "17743", "17711", "17725", "17727", "17734", "17744", "17748", "17714", "17721", "17724", "17726", "17729", "17731", "17740", "17745", "17747", "17749", "17710", "17713", "17717", "17718", "17720", "17728", "17730", "17732", "17733", "17735", "17739", "17866"] ]

//====================================================================================
//====================================================================================







