/*

import com.gaiaonline.mmo.battle.script.*;

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("kioskPosition", "markerOther", "Sewers_5", 560, 320, "HyperNet Access")

kiosk = makeSwitch( "botKiosk", myRooms.Sewers_5, 560, 320 )
kiosk.unlock()
kiosk.off()
kiosk.setRange( 300 )
kiosk.setMouseoverText("Access the HyperNet")

def clickKiosk = { event ->
	kiosk.off()
	if( !event.actor.hasQuestFlag( GLOBAL, "Z02UsingHyperNet" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		player = event.actor
		makeKioskMenu( player )
	}
}

kiosk.whenOn( clickKiosk )


//====================================================
// TUTORIAL TREE                                      
//====================================================

kioskWidth = 300

def giveReward( event ) {
	//gold reward
	goldGrant = random( 50, 150 )
	event.player.centerPrint( "You receive ${goldGrant} gold!" )
	event.player.grantCoins( goldGrant )

	//orb reward
	orbGrant = 2
	event.player.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
	event.player.grantQuantityItem( 100257, orbGrant )
}
	
def synchronized makeKioskMenu( player ) {
	titleString = "The HyperNet"
	descripString = "Choose Game Help to access loads of information about the game, or one of the other categories for easy downloads of useful tidbits."
	diffOptions = ["Game Help", "Tutorials", "Area Info", "Exit HyperNet"]
	
	uiButtonMenu( player, "kioskMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Game Help" ) {
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXGameHelpSelected" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXGameHelpSelected" )
				//gold reward
				goldGrant = random( 50, 150 )
				event.actor.centerPrint( "You receive ${goldGrant} gold!" )
				event.actor.grantCoins( goldGrant )

				//orb reward
				orbGrant = 2
				event.actor.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				event.actor.grantQuantityItem( 100257, orbGrant )
			}
			showWidget( event.actor, "GameHelp", true, true )
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
		if( event.selection == "Tutorials" ) {
			makeTutorialMenu( player )
		}
		if( event.selection == "Area Info" ) {
			makeAreaMenu( player )
		}
		if( event.selection == "Exit HyperNet" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
		}
	}
}

def makeAreaMenu( player ) {
	titleString = "Area Info"
	descripString = "A small archive of stories and tidbits of the area around you."
	diffOptions = ["The HyperNet", "The Waterworks", "Main Menu"]
	
	uiButtonMenu( player, "fictionMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "The HyperNet" ) {
			tutorialNPC.pushDialog( event.actor, "hyperNet" )
		}
		if( event.selection == "The Waterworks" ) {
			tutorialNPC.pushDialog( event.actor, "bartonSewers" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}
	


def makeTutorialMenu( player ) {
	titleString = "Tutorials"
	descripString = "These tutorials give you quick overviews on various features in the game."
	diffOptions = ["Basic Combat", "Recrewting", "Charge Level", "Judging Toughness", "Leaving the Waterworks", "Main Menu"]
	
	uiButtonMenu( player, "tutorialMenu", titleString, descripString, diffOptions, kioskWidth ) { event ->
		if( event.selection == "Basic Combat" ) {
			tutorialNPC.pushDialog( event.actor, "ringRing" )
		}
		if( event.selection == "Recrewting" ) {
			tutorialNPC.pushDialog( event.actor, "recrewting" )
		}
		if( event.selection == "Charge Level" ) {
			tutorialNPC.pushDialog( event.actor, "chargeLevel" )
		}
		if( event.selection == "Judging Toughness" ) {
			tutorialNPC.pushDialog( event.actor, "judgeToughness" )
		}
		if( event.selection == "Leaving the Waterworks" ) {
			tutorialNPC.pushDialog( event.actor, "sewersExit" )
		}
		if( event.selection == "Main Menu" ) {
			player = event.actor
			makeKioskMenu( player )
		}
	}
}

//======================================================================================
//======================================================================================
// TUTORIAL MENU                                                                        
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//RING RING TUTORIAL                                                                    
//--------------------------------------------------------------------------------------
ringRing = tutorialNPC.createConversation( "ringRing", true )

def ring1 = [id:1]
ring1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Basic Combat</font></b></h1><br><font face='Arial' size ='12'>Click a target to select it. Once a target is selected, you can use any of your rings on that target by clicking that ring.<br><br>Rings consume Stamina when used and they take time to Recharge between uses.<br><br>To recover your Stamina, hit the Pose button to kneel, but don't get hit while kneeling or you'll take big damage!<br><br>]]></zOMG>"
ring1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXRingRing" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXRingRing" )
			giveReward( event )
		}
	}
}
ring1.result = DONE
ringRing.addDialog( ring1, tutorialNPC )

//--------------------------------------------------------------------------------------
//RECREWTING TUTORIAL                                                                   
//--------------------------------------------------------------------------------------
recrewting = tutorialNPC.createConversation( "recrewting", true )

def crew1 = [id:1]
crew1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>The Recrewt List</font></b></h1><br><font face='Arial' size ='12'>Crewing with other players is safer, you get more loot, and it's fun.<br><br>To look for a Crew, first open your Crew List. Then click the pull-down menu next to your name and choose the 'Recrewt' option.<br><br>You can easily see the relative strength of other players by looking at the colors of their names. Just like with monsters, purple names are much weaker than you and red is much stronger. You'll get best results by looking for green names.<br><br>]]></zOMG>"
crew1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXRecrewting" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXRecrewting" )
			giveReward( event )
		}
	}
}
crew1.result = DONE
recrewting.addDialog(crew1, tutorialNPC)

//--------------------------------------------------------------------------------------
//CHARGE LEVEL TUTORIAL                                                                 
//--------------------------------------------------------------------------------------
def chargeLevel = tutorialNPC.createConversation( "chargeLevel", true )

def cl1 = [id:1]
cl1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Ring Charge Level (CL)</font></b></h1><br><font face='Arial' size ='12'>Your ring's CL indicates the power infused into that ring. As a ring is upgraded with Charge Orbs, its CL will increase.<br><br>Your overall CL is a value calculated from your individual ring CLs.<br><br>Increase your ring's Charge Level (and your overall CL) by going to the Null Chamber and infusing Charge Orbs into your ring.<br><br>Talk to Leon in the Village Greens to get more info about the Null Chamber.<br><br>]]></zOMG>"
cl1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXChargeLevel" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXChargeLevel" )
			giveReward( event )
		}
	}
}
cl1.result = DONE
chargeLevel.addDialog(cl1, tutorialNPC)

//--------------------------------------------------------------------------------------
//JUDGING TOUGHNESS TUTORIAL                                                            
//--------------------------------------------------------------------------------------
judgeToughness = tutorialNPC.createConversation( "judgeToughness", true )

def judge1 = [id:1]
judge1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Judging Target Toughness</font></b></h1><br><font face='Arial' size ='12'>Click a target to select it. The CL (Charge Level) of the target is shown. This compares directly to your own CL.<br><br>We also use colors to give you a quick glance reference as to toughness.<br><br>Purple : A cakewalk!<br>Blue : Weaker than you.<br>Green : About as tough as you.<br>Yellow : Tougher than you.<br>Red : Very dangerous!<br><br>]]></zOMG>"
judge1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXJudgeToughness" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXJudgeToughness" )
			giveReward( event )
		}
	}
}
judge1.result = DONE
judgeToughness.addDialog( judge1, tutorialNPC )

//--------------------------------------------------------------------------------------
//LEAVING THE SEWERS TUTORIAL                                                           
//--------------------------------------------------------------------------------------
sewersExit = tutorialNPC.createConversation( "sewersExit", true )

def exit1 = [id:1]
exit1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Leaving the Waterworks</font></b></h1><br><font face='Arial' size ='12'>There are two exits from the Waterworks.<br><br>The ladder east of here takes you to a tunnel that comes out in the Village Greens. Use this ladder if you want to go talk to Leon and start fighting the Animated.<br><br>The ladder southwest of here takes you up to Barton Town itself. Use this ladder if you want to head somewhere a bit quieter, where you can do tasks for citizens without having to worry about combat so much.<br><br>]]></zOMG>"
exit1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeTutorialMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXSewersExit" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXSewersExit" )
			giveReward( event )
		}
	}
}
exit1.result = DONE
sewersExit.addDialog( exit1, tutorialNPC )

//======================================================================================
//======================================================================================
// AREA MENU                                                                            
//======================================================================================
//======================================================================================

//--------------------------------------------------------------------------------------
//HYPERNET BACKGROUND                                                                   
//--------------------------------------------------------------------------------------
def hyperNet = tutorialNPC.createConversation( "hyperNet", true )

def hyper1 = [id:1]
hyper1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Origins of the HyperNet</font></b></h1><br><font face='Arial' size ='12'>The town of Aekea is well-known for its AI (artificial intelligence) personalities.<br><br>Those AIs move around in robotic bodies, but they aren't constrained to those mere metal forms.<br><br>The vast intellects of the AIs dwell within a cyberspace area that they call the 'HyperNet'.<br><br>The AIs decided long ago to let Gaians access that thoughtsphere and tap it for data when needed.<br><br>Try not to think too hard about the fact that you're rummaging around in someone's mind for this information.<br><br>]]></zOMG>"
hyper1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXHyperNet" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXHyperNet" )
			giveReward( event )
		}
	}
}
hyper1.result = DONE
hyperNet.addDialog( hyper1, tutorialNPC )

//--------------------------------------------------------------------------------------
//VILLAGE GREENS BACKGROUND                                                             
//--------------------------------------------------------------------------------------
def bartonSewers = tutorialNPC.createConversation( "bartonSewers", true )

def sewers1 = [id:1]
sewers1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>The Waterworks</font></b></h1><br><font face='Arial' size ='12'>The Waterworks beneath Barton Town were carefully crafted by a team of dedicated artisans.<br><br>Designed with a ridiculous amount of aesthetically pleasing grillwork and water sprays, it was crafted to be not only functional and long-lasting, but also a work of art on its own merit.<br><br>]]></zOMG>"
sewers1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXBartonSewers" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXBartonSewers" )
			giveReward( event )
		}
	}
}
sewers1.result = DONE
bartonSewers.addDialog( sewers1, tutorialNPC )

//--------------------------------------------------------------------------------------
//BARTON REGULARS BACKGROUND                                                            
//--------------------------------------------------------------------------------------
def bartonRegulars = tutorialNPC.createConversation( "bartonRegulars", true )

def regular1 = [id:1]
regular1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>The Barton Regulars</font></b></h1><br><font face='Arial' size ='12'>The Barton Regulars are the law enforcement squad for Barton Town. Their commander, Leon, is a huge fan of swords & sorcery novels. He figures that his forces will be better protected while wearing plate armor...and it makes his guards less intimidating to other citizens.<br><br>Plus, it never ceases to make him grin when they get together for photos.<br><br>]]></zOMG>"
regular1.exec = { event ->
	if( event.actor.getRoom() == myRooms.Sewers_5 ) {
		player = event.player
		makeAreaMenu( player )
		if( !event.player.hasQuestFlag( GLOBAL, "ZXXBartonRegulars" ) ) {
			event.player.setQuestFlag( GLOBAL, "ZXXBartonRegulars" )
			giveReward( event )
		}
	}
}
regular1.result = DONE
bartonRegulars.addDialog( regular1, tutorialNPC )

*/
