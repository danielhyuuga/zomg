import com.gaiaonline.mmo.battle.script.*;

//Add a map marker when the script starts up and never turn it off
addMiniMapMarker("exitLadder5", "markerOther", "Sewers_104", 165, 310, "Exit to Barton Town" )
addMiniMapMarker("exitLadder6", "markerOther", "Sewers_6", 540, 315, "Exit to Barton Town")

//-------------------------------------------
// LADDER 104 (Exit to Barton Town)          
//-------------------------------------------
onClickExitLadder104 = new Object()

ladder104 = makeSwitch( "ladder5", myRooms.Sewers_104, 165, 310 )
ladder104.unlock()
ladder104.setRange( 200 )
ladder104.setMouseoverText("Exit to Barton Town")

def goToBartonLadder104 = { event ->
	synchronized( onClickExitLadder104 ) {
		ladder104.off()
		if( !event.actor.hasQuestFlag( GLOBAL, "Z01ComingFromBartonTown" ) ) {
			event.actor.centerPrint( "You can't enter Barton Town yet." )
			event.actor.centerPrint( "You need to go see Commander Leon in the Village Greens first." )
		} else {
			event.actor.centerPrint( "You climb up the ladder and out into Barton Town." )
			event.actor.unsetQuestFlag( GLOBAL, "Z14AlreadySawTutorialOptionMenu" )
			event.actor.unsetQuestFlag( GLOBAL, "Z01ComingFromBartonTown" )
			event.actor.removeMiniMapQuestLocation( "Z14ValveOneFlag" )
			event.actor.removeMiniMapQuestLocation( "Z14ValveTwoFlag" )
			event.actor.removeMiniMapQuestLocation( "Z14ValveThreeFlag" )
			event.actor.removeMiniMapQuestLocation( "Z14ValveFourFlag" )
			//unset the flags again to force the player to go revisit all the pipes even if they've done it before
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveOneTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveTwoTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveThreeTurned" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14ValveFourTurned" )

			event.actor.unsetQuestFlag( GLOBAL, "Z14RingRingGiven" )
			event.actor.unsetQuestFlag( GLOBAL, "Z14BossWarningGiven" )
			myManager.stopListenForHealth( event.actor )
			event.actor.warp( "BARTON_104", 1060, 240 )
			event.actor.setQuestFlag( GLOBAL, "Z14HasLeftWaterworks" )
		}
	}
}

ladder104.whenOn( goToBartonLadder104 )


//-------------------------------------------
// LADDER 6 (Exit to Village Greens)         
//-------------------------------------------
onClickExitLadder6 = new Object()

ladder6 = makeSwitch( "ladder6", myRooms.Sewers_6, 540, 315 )
ladder6.unlock()
ladder6.setRange( 200 )
ladder6.setMouseoverText("Exit to Barton Town")

def goToBartonLadder6 = { event ->
	synchronized( onClickExitLadder6 ) {
		ladder6.off()
		event.actor.centerPrint( "You climb up the ladder and out into the Barton Town." )
		event.actor.unsetQuestFlag( GLOBAL, "Z14AlreadySawTutorialOptionMenu" )
		event.actor.unsetQuestFlag( GLOBAL, "Z01ComingFromBartonTown" )
		event.actor.removeMiniMapQuestLocation( "Z14ValveOneFlag" )
		event.actor.removeMiniMapQuestLocation( "Z14ValveTwoFlag" )
		event.actor.removeMiniMapQuestLocation( "Z14ValveThreeFlag" )
		event.actor.removeMiniMapQuestLocation( "Z14ValveFourFlag" )
		//unset the flags again to force the player to go revisit all the pipes even if they've done it before
		event.actor.unsetQuestFlag( GLOBAL, "Z14ValveOneTurned" )
		event.actor.unsetQuestFlag( GLOBAL, "Z14ValveTwoTurned" )
		event.actor.unsetQuestFlag( GLOBAL, "Z14ValveThreeTurned" )
		event.actor.unsetQuestFlag( GLOBAL, "Z14ValveFourTurned" )

		event.actor.unsetQuestFlag( GLOBAL, "Z14RingRingGiven" )
		event.actor.unsetQuestFlag( GLOBAL, "Z14BossWarningGiven" )
		myManager.stopListenForHealth( event.actor )
		event.actor.warp( "BARTON_104", 1060, 240 )
		event.actor.setQuestFlag( GLOBAL, "Z14HasLeftWaterworks" )
	}
}

ladder6.whenOn( goToBartonLadder6 )

myManager.onEnter( myRooms.Sewers_6 ) { event ->
	if( isPlayer( event.actor ) ) {
		if( event.actor.hasQuestFlag( GLOBAL, "Z14RegPromptShouldBeShown" ) ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z14RegPromptShouldBeShown" )
			myManager.schedule(1) { showRegPrompt( event.actor ) }
		}
	}
}

//-------------------------------------------
// LADDER 301 (UNUSED)                       
//-------------------------------------------
ladder301 = makeSwitch( "ladder3", myRooms.Sewers_301, 170, 315 )
ladder301.unlock()
ladder301.setRange( 200 )

def goToBartonLadderThree = { event ->
	ladder301.off()
	event.actor.centerPrint( "The manhole at the top of this ladder is sealed shut." )
}

ladder301.whenOn( goToBartonLadderThree )

//-------------------------------------------
// LADDER 3 (UNUSED)                         
//-------------------------------------------
ladder3 = makeSwitch( "ladder1", myRooms.Sewers_3, 145, 255 )
ladder3.unlock()
ladder3.setRange( 200 )

def goToBartonLadderOne = { event ->
	ladder3.off()
	event.actor.centerPrint( "The manhole at the top of this ladder is sealed shut." )
}
	
ladder3.whenOn( goToBartonLadderOne )

//-------------------------------------------
// LADDER 2 (UNUSED)                         
//-------------------------------------------
ladder2 = makeSwitch( "ladder2", myRooms.Sewers_107, 375, 535 )
ladder2.unlock()
ladder2.setRange( 200 )

def goToBartonLadderTwo = { event ->
	ladder2.off()
	event.actor.centerPrint( "The manhole at the top of this ladder is sealed shut." )
}

ladder2.whenOn( goToBartonLadderTwo )

//-------------------------------------------
// LADDER 4 (UNUSED)                         
//-------------------------------------------
ladder4 = makeSwitch( "ladder4", myRooms.Sewers_203, 330, 375 )
ladder4.unlock()
ladder4.setRange( 200 )

def goToBartonLadderFour = { event ->
	ladder4.off()
	event.actor.centerPrint( "The manhole at the top of this ladder is sealed shut." )
}

ladder4.whenOn( goToBartonLadderFour )

/*
//Ladder waypoints
event.actor.addMiniMapQuestLocation( "ladderOne", "Sewers_3", 145, 255 ) //to BARTON_3
event.actor.addMiniMapQuestLocation( "ladderThree", "Sewers_301", 170, 315 ) //to BARTON_202
event.actor.addMiniMapQuestLocation( "ladderSix", "Sewers_6", 540, 315 ) //to BARTON_104
event.actor.addMiniMapQuestLocation( "ladderTwo", "Sewers_107", 375, 535 ) //LOCKED
event.actor.addMiniMapQuestLocation( "ladderFour", "Sewers_203", 330, 375 ) //LOCKED
event.actor.addMiniMapQuestLocation( "ladderFive", "Sewers_104", 165, 310 ) //LOCKED

//Valve waypoints
event.actor.addMiniMapQuestLocation( "valve1", "Sewers_202", 230, 145 )
event.actor.addMiniMapQuestLocation( "valve2", "Sewers_403", 290, 165 )
event.actor.addMiniMapQuestLocation( "valve3", "Sewers_305", 805, 320 )

*/

