import com.gaiaonline.mmo.battle.script.*;


/*PetalSwitchA = makeGeneric("PetalSwitchA", myRooms.BartonColiseum_1, 720, 520)
PetalSwitchA.setStateful()
PetalSwitchA.setUsable(false)

PetalSwitchB = makeGeneric("PetalSwitchB", myRooms.BartonColiseum_1, 720, 520)
PetalSwitchB.setStateful()
PetalSwitchB.setUsable(false)

PetalSwitchC = makeGeneric("PetalSwitchC", myRooms.BartonColiseum_1, 720, 520)
PetalSwitchC.setStateful()
PetalSwitchC.setUsable(false)

PetalSwitchD = makeGeneric("PetalSwitchD", myRooms.BartonColiseum_1, 720, 520)
PetalSwitchD.setStateful()
PetalSwitchD.setUsable(false)

PetalSwitchE = makeGeneric("PetalSwitchE", myRooms.BartonColiseum_1, 720, 520)
PetalSwitchE.setStateful()
PetalSwitchE.setUsable(false)


Frank = spawnNPC("Frank-VQS", myRooms.BartonColiseum_1, 615, 415)
Frank.setRotation( 45 )
Frank.setDisplayName( "Frank" )
Frank.setRange( 1000 )

def frankConv = Frank.createConversation("franksconv", true)
def frankDialog = [id:1]
frankDialog.npctext = "What shall we test, oh great one?"
frankDialog.options = []
frankDialog.options << [text: "Do the Wav left to right", result: 1, exec: { startWave() }]
frankDialog.options << [text: "Do the Wav right to left", result: 1, exec: { startWaveBack() }]
frankDialog.options << [text: "Start Jump", result: 1,  exec: { startJump() }]
frankDialog.options << [text: "Stop Jump", result: 1,  exec: { stopJump() }]

frankDialog.options << [text: "Switch ON", result: 1,  exec: { PetalSwitchA.setActorState(1); PetalSwitchB.setActorState(1); PetalSwitchC.setActorState(1); PetalSwitchD.setActorState(0); PetalSwitchE.setActorState(0) }]
frankDialog.options << [text: "Switch OFF", result: 1,  exec: { PetalSwitchA.setActorState(0); PetalSwitchB.setActorState(0); PetalSwitchC.setActorState(0); PetalSwitchD.setActorState(0); PetalSwitchE.setActorState(0) }]

frankConv.addDialog(frankDialog, Frank)



def startWave()
{
	new WorldEvent(Frank.getArea(), myManager.postmaster, "Crowd", [doWave:"right_left"]).toZone();
}
def startWaveBack()
{
	new WorldEvent(Frank.getArea(), myManager.postmaster, "Crowd", [doWave:"left_right"]).toZone();
}



def startJump()
{
	new WorldEvent(Frank.getArea(), myManager.postmaster, "Crowd", [jump:true]).toZone();
}

def stopJump()
{
	new WorldEvent(Frank.getArea(), myManager.postmaster, "Crowd", [jump:false]).toZone();
}*/
