//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//testCritter = makeCritter("labtech_male", myRooms.BartonColiseum_1, 800, 650)
//testCritter.pause()

//------------------------------------------------------------------------------
//VARIABLES, CONSTANTS, SETS, MAPS                                              
//------------------------------------------------------------------------------

//Variable Definition
monsterLevel = 1.0
coliseumInitialized = false
coliseumDifficulty = 1
coliseumWaves = 0
endWaveCounter = 0
spawningWave = false
delayTime = 0
spawnerRandom1 = 0
spawnerRandom2 = 0
spawnerRandom3 = 0
timerActive = false
trialFailed = false
allDazed = false
numDazed = 0
trialType = "null"
portal1Active = false
portal2Active = false
openPortal1 = false
openPortal2 = false
portalDelay = 30
portalMonster1 = null
portalMonster2 = null
impossibleBossWaveCounter = 0
impossibleTrialActive = false
incrementImpossibleRoundCounter = false
eventOver = false

//Constant Definition
MAX_MONSTER_LEVEL = 10
HATE_VALUE = 3
SURVIVAL_COUNTER = "Monsters Defeated"
IMPOSSIBLE_COUNTER = "Rounds Completed"
TIME_TRIAL_TIMER = "Time Remaining"

EASY_PHASE_ONE_TIMER = 6
EASY_PHASE_TWO_TIMER = 8

MEDIUM_PHASE_ONE_TIMER = 3
MEDIUM_PHASE_TWO_TIMER = 4

HARD_PHASE_ONE_TIMER = 6
HARD_PHASE_TWO_TIMER = 8
HARD_PHASE_THREE_TIMER = 10

EXTREME_PHASE_ONE_TIMER = 15
EXTREME_PHASE_TWO_TIMER = 20
EXTREME_PHASE_THREE_TIMER = 25
EXTREME_PHASE_FOUR_TIMER = 25
EXTREME_PHASE_FIVE_TIMER = 25

IMPOSSIBLE_PHASE_ONE_TIMER = 15
IMPOSSIBLE_PHASE_TWO_TIMER = 20
IMPOSSIBLE_PHASE_THREE_TIMER = 25
IMPOSSIBLE_PHASE_FOUR_TIMER = 25
IMPOSSIBLE_PHASE_FIVE_TIMER = 25

COLISEUM_PROTECTION_EASY_PHASE_ONE_LIMIT = 5
COLISEUM_PROTECTION_EASY_PHASE_TWO_LIMIT = 10
COLISEUM_PROTECTION_EASY_FINAL_WAVE = 11

COLISEUM_PROTECTION_MEDIUM_PHASE_ONE_LIMIT = 5
COLISEUM_PROTECTION_MEDIUM_PHASE_TWO_LIMIT = 10
COLISEUM_PROTECTION_MEDIUM_FINAL_WAVE = 11

COLISEUM_PROTECTION_HARD_PHASE_ONE_LIMIT = 5
COLISEUM_PROTECTION_HARD_PHASE_TWO_LIMIT = 10
COLISEUM_PROTECTION_HARD_PHASE_THREE_LIMIT = 15
COLISEUM_PROTECTION_HARD_FINAL_WAVE = 16

COLISEUM_PROTECTION_EXTREME_PHASE_ONE_LIMIT = 5
COLISEUM_PROTECTION_EXTREME_PHASE_TWO_LIMIT = 10
COLISEUM_PROTECTION_EXTREME_PHASE_THREE_LIMIT = 15
COLISEUM_PROTECTION_EXTREME_PHASE_FOUR_LIMIT = 20
COLISEUM_PROTECTION_EXTREME_PHASE_FIVE_LIMIT = 25
COLISEUM_PROTECTION_EXTREME_FINAL_WAVE = 26

COLISEUM_SURVIVAL_EASY_PHASE_ONE_LIMIT = 5
COLISEUM_SURVIVAL_EASY_PHASE_TWO_LIMIT = 10
COLISEUM_SURVIVAL_EASY_FINAL_WAVE = 11

COLISEUM_SURVIVAL_MEDIUM_PHASE_ONE_LIMIT = 5
COLISEUM_SURVIVAL_MEDIUM_PHASE_TWO_LIMIT = 10
COLISEUM_SURVIVAL_MEDIUM_FINAL_WAVE = 11

COLISEUM_SURVIVAL_HARD_PHASE_ONE_LIMIT = 5
COLISEUM_SURVIVAL_HARD_PHASE_TWO_LIMIT = 10
COLISEUM_SURVIVAL_HARD_PHASE_THREE_LIMIT = 15
COLISEUM_SURVIVAL_HARD_FINAL_WAVE = 16

COLISEUM_SURVIVAL_EXTREME_PHASE_ONE_LIMIT = 5
COLISEUM_SURVIVAL_EXTREME_PHASE_TWO_LIMIT = 10
COLISEUM_SURVIVAL_EXTREME_PHASE_THREE_LIMIT = 15
COLISEUM_SURVIVAL_EXTREME_PHASE_FOUR_LIMIT = 20
COLISEUM_SURVIVAL_EXTREME_PHASE_FIVE_LIMIT = 25
COLISEUM_SURVIVAL_EXTREME_FINAL_WAVE = 26

COLISEUM_TIME_EASY_PHASE_ONE_LIMIT = 5
COLISEUM_TIME_EASY_PHASE_TWO_LIMIT = 10
COLISEUM_TIME_EASY_FINAL_WAVE = 11

COLISEUM_TIME_MEDIUM_PHASE_ONE_LIMIT = 5
COLISEUM_TIME_MEDIUM_PHASE_TWO_LIMIT = 10
COLISEUM_TIME_MEDIUM_FINAL_WAVE = 11

COLISEUM_TIME_HARD_PHASE_ONE_LIMIT = 5
COLISEUM_TIME_HARD_PHASE_TWO_LIMIT = 10
COLISEUM_TIME_HARD_PHASE_THREE_LIMIT = 15
COLISEUM_TIME_HARD_FINAL_WAVE = 16

COLISEUM_TIME_EXTREME_PHASE_ONE_LIMIT = 5
COLISEUM_TIME_EXTREME_PHASE_TWO_LIMIT = 10
COLISEUM_TIME_EXTREME_PHASE_THREE_LIMIT = 15
COLISEUM_TIME_EXTREME_PHASE_FOUR_LIMIT = 20
COLISEUM_TIME_EXTREME_PHASE_FIVE_LIMIT = 25
COLISEUM_TIME_EXTREME_FINAL_WAVE = 26

EASY_PORTAL_DELAY = 30
MEDIUM_PORTAL_DELAY = 30
HARD_PORTAL_DELAY = 30
EXTREME_PORTAL_DELAY = 45

EASY_PHASE_ONE_ADDED_TIME = 10
EASY_PHASE_TWO_ADDED_TIME = 12
EASY_BOSS_ADDED_TIME = 30

MEDIUM_PHASE_ONE_ADDED_TIME = 10
MEDIUM_PHASE_TWO_ADDED_TIME = 12
MEDIUM_BOSS_ADDED_TIME = 30

HARD_PHASE_ONE_ADDED_TIME = 10
HARD_PHASE_TWO_ADDED_TIME = 12
HARD_PHASE_THREE_ADDED_TIME = 18
HARD_BOSS_ADDED_TIME = 30

EXTREME_PHASE_ONE_ADDED_TIME = 15
EXTREME_PHASE_TWO_ADDED_TIME = 20
EXTREME_PHASE_THREE_ADDED_TIME = 25
EXTREME_PHASE_FOUR_ADDED_TIME = 30
EXTREME_PHASE_FIVE_ADDED_TIME = 35
EXTREME_BOSS_ADDED_TIME = 45

IMPOSSIBLE_PHASE_ONE_ADDED_TIME = 15
IMPOSSIBLE_PHASE_TWO_ADDED_TIME = 20
IMPOSSIBLE_PHASE_THREE_ADDED_TIME = 30
IMPOSSIBLE_PHASE_FOUR_ADDED_TIME = 40
IMPOSSIBLE_PHASE_FIVE_ADDED_TIME = 50
IMPOSSIBLE_BOSS_ADDED_TIME = 60

EASY_ORB_CHANCE = 20
MEDIUM_ORB_CHANCE = 19
HARD_ORB_CHANCE = 18
EXTREME_ORB_CHANCE = 17
IMPOSSIBLE_ORB_CHANCE = 17

//Set Definition
playerList = [] as Set
hateList = [] as Set
monsterList = [] as Set
spawnerRandomSequence = [1, 2, 3] as Set

//Map Definition
impossibleWaveTimerMap = [1:20, 2:19, 3:18, 4:17, 5:16, 6:15, 7:14, 8:13, 9:12, 10:11, 11:10, 12:9, 13:8, 14:7, 15:6, 16:5, 17:4, 18:3, 19:3, 20:3, 40:2, 100:1]

//Trigger zone definition
timeTrigger = "timeTrigger"
myRooms.BartonColiseum_1.createTriggerZone(timeTrigger, 1140, 810, 1240, 910)

//------------------------------------------------------------------------------
//SPAWNERS                                                                      
//------------------------------------------------------------------------------

//-----------------------------------Normal Spawners-----------------------------------
//OMG
omgSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgSpawner1", "omg", 50)
omgSpawner1.setPos(210, 480)
omgSpawner1.setHateRadiusForChildren(2000)
omgSpawner1.setMonsterLevelForChildren(monsterLevel)

omgSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgSpawner2", "omg", 50)
omgSpawner2.setPos(780, 380)
omgSpawner2.setHateRadiusForChildren(2000)
omgSpawner2.setMonsterLevelForChildren(monsterLevel)

omgSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgSpawner3", "omg", 50)
omgSpawner3.setPos(1350, 480)
omgSpawner3.setHateRadiusForChildren(2000)
omgSpawner3.setMonsterLevelForChildren(monsterLevel)

//Alarmskeeter
mosquitoSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoSpawner1", "mosquito", 50)
mosquitoSpawner1.setPos(210, 480)
mosquitoSpawner1.setHateRadiusForChildren(2000)
mosquitoSpawner1.setMonsterLevelForChildren(monsterLevel)

mosquitoSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoSpawner2", "mosquito", 50)
mosquitoSpawner2.setPos(780, 380)
mosquitoSpawner2.setHateRadiusForChildren(2000)
mosquitoSpawner2.setMonsterLevelForChildren(monsterLevel)

mosquitoSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoSpawner3", "mosquito", 50)
mosquitoSpawner3.setPos(1350, 480)
mosquitoSpawner3.setHateRadiusForChildren(2000)
mosquitoSpawner3.setMonsterLevelForChildren(monsterLevel)

//Ghost Lantern
lanternSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternSpawner1", "ghost_lantern", 50)
lanternSpawner1.setPos(210, 480)
lanternSpawner1.setHateRadiusForChildren(2000)
lanternSpawner1.setMonsterLevelForChildren(monsterLevel)

lanternSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternSpawner2", "ghost_lantern", 50)
lanternSpawner2.setPos(780, 380)
lanternSpawner2.setHateRadiusForChildren(2000)
lanternSpawner2.setMonsterLevelForChildren(monsterLevel)

lanternSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternSpawner3", "ghost_lantern", 50)
lanternSpawner3.setPos(1350, 480)
lanternSpawner3.setHateRadiusForChildren(2000)
lanternSpawner3.setMonsterLevelForChildren(monsterLevel)

//-------------------------------------LT Spawners--------------------------------------
//OMG
omgLTSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgLTSpawner1", "omg_LT", 50)
omgLTSpawner1.setPos(210, 480)
omgLTSpawner1.setHateRadiusForChildren(2000)
omgLTSpawner1.setMonsterLevelForChildren(monsterLevel)

omgLTSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgLTSpawner2", "omg_LT", 50)
omgLTSpawner2.setPos(780, 380)
omgLTSpawner2.setHateRadiusForChildren(2000)
omgLTSpawner2.setMonsterLevelForChildren(monsterLevel)

omgLTSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgLTSpawner3", "omg_LT", 50)
omgLTSpawner3.setPos(1350, 480)
omgLTSpawner3.setHateRadiusForChildren(2000)
omgLTSpawner3.setMonsterLevelForChildren(monsterLevel)

//Alarmskeeter
mosquitoLTSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoLTSpawner1", "mosquito_LT", 50)
mosquitoLTSpawner1.setPos(210, 480)
mosquitoLTSpawner1.setHateRadiusForChildren(2000)
mosquitoLTSpawner1.setMonsterLevelForChildren(monsterLevel)

mosquitoLTSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoLTSpawner2", "mosquito_LT", 50)
mosquitoLTSpawner2.setPos(780, 380)
mosquitoLTSpawner2.setHateRadiusForChildren(2000)
mosquitoLTSpawner2.setMonsterLevelForChildren(monsterLevel)

mosquitoLTSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoLTSpawner3", "mosquito_LT", 50)
mosquitoLTSpawner3.setPos(1350, 480)
mosquitoLTSpawner3.setHateRadiusForChildren(2000)
mosquitoLTSpawner3.setMonsterLevelForChildren(monsterLevel)

//Ghost Lantern
lanternLTSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternLTSpawner1", "ghost_lantern_LT", 50)
lanternLTSpawner1.setPos(210, 480)
lanternLTSpawner1.setHateRadiusForChildren(2000)
lanternLTSpawner1.setMonsterLevelForChildren(monsterLevel)

lanternLTSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternLTSpawner2", "ghost_lantern_LT", 50)
lanternLTSpawner2.setPos(780, 380)
lanternLTSpawner2.setHateRadiusForChildren(2000)
lanternLTSpawner2.setMonsterLevelForChildren(monsterLevel)

lanternLTSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternLTSpawner3", "ghost_lantern_LT", 50)
lanternLTSpawner3.setPos(1350, 480)
lanternLTSpawner3.setHateRadiusForChildren(2000)
lanternLTSpawner3.setMonsterLevelForChildren(monsterLevel)

//------------------------------------Elite Spawners------------------------------------
//OMGLOL
omgEliteSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgEliteSpawner1", "omg_coliseum", 50)
omgEliteSpawner1.setPos(210, 480)
omgEliteSpawner1.setHateRadiusForChildren(2000)
omgEliteSpawner1.setMonsterLevelForChildren(monsterLevel)

omgEliteSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgEliteSpawner2", "omg_coliseum", 50)
omgEliteSpawner2.setPos(780, 380)
omgEliteSpawner2.setHateRadiusForChildren(2000)
omgEliteSpawner2.setMonsterLevelForChildren(monsterLevel)

omgEliteSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgEliteSpawner3", "omg_coliseum", 50)
omgEliteSpawner3.setPos(1350, 480)
omgEliteSpawner3.setHateRadiusForChildren(2000)
omgEliteSpawner3.setMonsterLevelForChildren(monsterLevel)

//Alertskeeter
mosquitoEliteSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoEliteSpawner1", "mosquito_coliseum", 50)
mosquitoEliteSpawner1.setPos(210, 480)
mosquitoEliteSpawner1.setHateRadiusForChildren(2000)
mosquitoEliteSpawner1.setMonsterLevelForChildren(monsterLevel)

mosquitoEliteSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoEliteSpawner2", "mosquito_coliseum", 50)
mosquitoEliteSpawner2.setPos(780, 380)
mosquitoEliteSpawner2.setHateRadiusForChildren(2000)
mosquitoEliteSpawner2.setMonsterLevelForChildren(monsterLevel)

mosquitoEliteSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoEliteSpawner3", "mosquito_coliseum", 50)
mosquitoEliteSpawner3.setPos(1350, 480)
mosquitoEliteSpawner3.setHateRadiusForChildren(2000)
mosquitoEliteSpawner3.setMonsterLevelForChildren(monsterLevel)

//Spirit Lantern
lanternEliteSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternEliteSpawner1", "ghost_lantern_coliseum", 50)
lanternEliteSpawner1.setPos(210, 480)
lanternEliteSpawner1.setHateRadiusForChildren(2000)
lanternEliteSpawner1.setMonsterLevelForChildren(monsterLevel)

lanternEliteSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternEliteSpawner2", "ghost_lantern_coliseum", 50)
lanternEliteSpawner2.setPos(780, 380)
lanternEliteSpawner2.setHateRadiusForChildren(2000)
lanternEliteSpawner2.setMonsterLevelForChildren(monsterLevel)

lanternEliteSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternEliteSpawner3", "ghost_lantern_coliseum", 50)
lanternEliteSpawner3.setPos(1350, 480)
lanternEliteSpawner3.setHateRadiusForChildren(2000)
lanternEliteSpawner3.setMonsterLevelForChildren(monsterLevel)

//-----------------------------------Elite LT Spawners-----------------------------------
//OMGLOL
omgEliteLTSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgEliteLTSpawner1", "omg_coliseum_LT", 50)
omgEliteLTSpawner1.setPos(210, 480)
omgEliteLTSpawner1.setHateRadiusForChildren(2000)
omgEliteLTSpawner1.setMonsterLevelForChildren(monsterLevel)

omgEliteLTSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgEliteLTSpawner2", "omg_coliseum_LT", 50)
omgEliteLTSpawner2.setPos(780, 380)
omgEliteLTSpawner2.setHateRadiusForChildren(2000)
omgEliteLTSpawner2.setMonsterLevelForChildren(monsterLevel)

omgEliteLTSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgEliteLTSpawner3", "omg_coliseum_LT", 50)
omgEliteLTSpawner3.setPos(1350, 480)
omgEliteLTSpawner3.setHateRadiusForChildren(2000)
omgEliteLTSpawner3.setMonsterLevelForChildren(monsterLevel)

//Alertskeeter
mosquitoEliteLTSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoEliteLTSpawner1", "mosquito_coliseum_LT", 50)
mosquitoEliteLTSpawner1.setPos(210, 480)
mosquitoEliteLTSpawner1.setHateRadiusForChildren(2000)
mosquitoEliteLTSpawner1.setMonsterLevelForChildren(monsterLevel)

mosquitoEliteLTSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoEliteLTSpawner2", "mosquito_coliseum_LT", 50)
mosquitoEliteLTSpawner2.setPos(780, 380)
mosquitoEliteLTSpawner2.setHateRadiusForChildren(2000)
mosquitoEliteLTSpawner2.setMonsterLevelForChildren(monsterLevel)

mosquitoEliteLTSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoEliteLTSpawner3", "mosquito_coliseum_LT", 50)
mosquitoEliteLTSpawner3.setPos(1350, 480)
mosquitoEliteLTSpawner3.setHateRadiusForChildren(2000)
mosquitoEliteLTSpawner3.setMonsterLevelForChildren(monsterLevel)

//Spirit Lantern
lanternEliteLTSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternEliteLTSpawner1", "ghost_lantern_coliseum_LT", 50)
lanternEliteLTSpawner1.setPos(210, 480)
lanternEliteLTSpawner1.setHateRadiusForChildren(2000)
lanternEliteLTSpawner1.setMonsterLevelForChildren(monsterLevel)

lanternEliteLTSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternEliteLTSpawner2", "ghost_lantern_coliseum_LT", 50)
lanternEliteLTSpawner2.setPos(780, 380)
lanternEliteLTSpawner2.setHateRadiusForChildren(2000)
lanternEliteLTSpawner2.setMonsterLevelForChildren(monsterLevel)

lanternEliteLTSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternEliteLTSpawner3", "ghost_lantern_coliseum_LT", 50)
lanternEliteLTSpawner3.setPos(1350, 480)
lanternEliteLTSpawner3.setHateRadiusForChildren(2000)
lanternEliteLTSpawner3.setMonsterLevelForChildren(monsterLevel)

//------------------------------------Veteran Spawners-----------------------------------
//OMGLOL
omgVeteranSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgVeteranSpawner1", "omg_coliseum_veteran", 50)
omgVeteranSpawner1.setPos(210, 480)
omgVeteranSpawner1.setHateRadiusForChildren(2000)
omgVeteranSpawner1.setMonsterLevelForChildren(monsterLevel)

omgVeteranSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgVeteranSpawner2", "omg_coliseum_veteran", 50)
omgVeteranSpawner2.setPos(780, 380)
omgVeteranSpawner2.setHateRadiusForChildren(2000)
omgVeteranSpawner2.setMonsterLevelForChildren(monsterLevel)

omgVeteranSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgVeteranSpawner3", "omg_coliseum_veteran", 50)
omgVeteranSpawner3.setPos(1350, 480)
omgVeteranSpawner3.setHateRadiusForChildren(2000)
omgVeteranSpawner3.setMonsterLevelForChildren(monsterLevel)

//Alertskeeter
mosquitoVeteranSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoVeteranSpawner1", "mosquito_coliseum_veteran", 50)
mosquitoVeteranSpawner1.setPos(210, 480)
mosquitoVeteranSpawner1.setHateRadiusForChildren(2000)
mosquitoVeteranSpawner1.setMonsterLevelForChildren(monsterLevel)

mosquitoVeteranSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoVeteranSpawner2", "mosquito_coliseum_veteran", 50)
mosquitoVeteranSpawner2.setPos(780, 380)
mosquitoVeteranSpawner2.setHateRadiusForChildren(2000)
mosquitoVeteranSpawner2.setMonsterLevelForChildren(monsterLevel)

mosquitoVeteranSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("mosquitoVeteranSpawner3", "mosquito_coliseum_veteran", 50)
mosquitoVeteranSpawner3.setPos(1350, 480)
mosquitoVeteranSpawner3.setHateRadiusForChildren(2000)
mosquitoVeteranSpawner3.setMonsterLevelForChildren(monsterLevel)

//Spirit Lantern
lanternVeteranSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternVeteranSpawner1", "ghost_lantern_coliseum_veteran", 50)
lanternVeteranSpawner1.setPos(210, 480)
lanternVeteranSpawner1.setHateRadiusForChildren(2000)
lanternVeteranSpawner1.setMonsterLevelForChildren(monsterLevel)

lanternVeteranSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternVeteranSpawner2", "ghost_lantern_coliseum_veteran", 50)
lanternVeteranSpawner2.setPos(780, 380)
lanternVeteranSpawner2.setHateRadiusForChildren(2000)
lanternVeteranSpawner2.setMonsterLevelForChildren(monsterLevel)

lanternVeteranSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("lanternVeteranSpawner3", "ghost_lantern_coliseum_veteran", 50)
lanternVeteranSpawner3.setPos(1350, 480)
lanternVeteranSpawner3.setHateRadiusForChildren(2000)
lanternVeteranSpawner3.setMonsterLevelForChildren(monsterLevel)

//-------------------------------------Boss Spawners------------------------------------
//OMG Demi
omgDemiSpawner = myRooms.BartonColiseum_1.spawnStoppedSpawner("omgDemiSpawner", "omg_demi", 50)
omgDemiSpawner.setPos(780, 380)
omgDemiSpawner.setHateRadiusForChildren(2000)
omgDemiSpawner.setMonsterLevelForChildren(monsterLevel)

//Outlaw Pup Demi
outlawPupDemiSpawner = myRooms.BartonColiseum_1.spawnStoppedSpawner("outlawPupDemiSpawner", "outlaw_pup_demi", 50)
outlawPupDemiSpawner.setPos(780, 380)
outlawPupDemiSpawner.setHateRadiusForChildren(2000)
outlawPupDemiSpawner.setMonsterLevelForChildren(monsterLevel)

//Lawn Mower
lawnmowerSpawner = myRooms.BartonColiseum_1.spawnStoppedSpawner("lawnmowerSpawner", "lawnmower", 50)
lawnmowerSpawner.setPos(780, 380)
lawnmowerSpawner.setHateRadiusForChildren(2000)
lawnmowerSpawner.setMonsterLevelForChildren(monsterLevel)

//Vampire
vampireSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("vampireSpawner1", "vampire_batmonster", 50)
vampireSpawner1.setPos(210, 480)
vampireSpawner1.setHateRadiusForChildren(2000)
vampireSpawner1.setMonsterLevelForChildren(monsterLevel)

vampireSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("vampireSpawner2", "vampire_batmonster", 50)
vampireSpawner2.setPos(780, 380)
vampireSpawner2.setHateRadiusForChildren(2000)
vampireSpawner2.setMonsterLevelForChildren(monsterLevel)

vampireSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("vampireSpawner3", "vampire_batmonster", 50)
vampireSpawner3.setPos(1350, 480)
vampireSpawner3.setHateRadiusForChildren(2000)
vampireSpawner3.setMonsterLevelForChildren(monsterLevel)

//Werewolf
werewolfSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("werewolfSpawner1", "werewolf_LT", 50)
werewolfSpawner1.setPos(210, 480)
werewolfSpawner1.setHateRadiusForChildren(2000)
werewolfSpawner1.setMonsterLevelForChildren(monsterLevel)

werewolfSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("werewolfSpawner2", "werewolf_LT", 50)
werewolfSpawner2.setPos(780, 380)
werewolfSpawner2.setHateRadiusForChildren(2000)
werewolfSpawner2.setMonsterLevelForChildren(monsterLevel)

werewolfSpawner3 = myRooms.BartonColiseum_1.spawnStoppedSpawner("werewolfSpawner3", "werewolf_LT", 50)
werewolfSpawner3.setPos(1350, 480)
werewolfSpawner3.setHateRadiusForChildren(2000)
werewolfSpawner3.setMonsterLevelForChildren(monsterLevel)

//----------------------------------------Portals---------------------------------------
portalSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalSpawner1", "portal", 50)
portalSpawner1.setPos(440, 820)
portalSpawner1.setMonsterLevelForChildren(monsterLevel)

portalSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalSpawner2", "portal", 50)
portalSpawner2.setPos(1120, 820)
portalSpawner2.setMonsterLevelForChildren(monsterLevel)

portalOMGSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalOMGSpawner1", "omg", 50)
portalOMGSpawner1.setPos(440, 820)
portalOMGSpawner1.setHateRadiusForChildren(2000)
portalOMGSpawner1.setMonsterLevelForChildren(monsterLevel)

portalOMGSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalOMGSpawner2", "omg", 50)
portalOMGSpawner2.setPos(1120, 820)
portalOMGSpawner2.setHateRadiusForChildren(2000)
portalOMGSpawner2.setMonsterLevelForChildren(monsterLevel)

portalMosquitoSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalMosquitoSpawner1", "mosquito", 50)
portalMosquitoSpawner1.setPos(440, 820)
portalMosquitoSpawner1.setHateRadiusForChildren(2000)
portalMosquitoSpawner1.setMonsterLevelForChildren(monsterLevel)

portalMosquitoSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalMosquitoSpawner2", "mosquito", 50)
portalMosquitoSpawner2.setPos(1120, 820)
portalMosquitoSpawner2.setHateRadiusForChildren(2000)
portalMosquitoSpawner2.setMonsterLevelForChildren(monsterLevel)

portalLanternSpawner1 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalLanternSpawner1", "ghost_lantern", 50)
portalLanternSpawner1.setPos(440, 820)
portalLanternSpawner1.setHateRadiusForChildren(2000)
portalLanternSpawner1.setMonsterLevelForChildren(monsterLevel)

portalLanternSpawner2 = myRooms.BartonColiseum_1.spawnStoppedSpawner("portalLanternSpawner2", "ghost_lantern", 50)
portalLanternSpawner2.setPos(1140, 820)
portalLanternSpawner2.setHateRadiusForChildren(2000)
portalLanternSpawner2.setMonsterLevelForChildren(monsterLevel)

//Flag Stand Monster for damage tracking
def portalSpec = [
	isAllowed : { attacker, target ->
		def player = isPlayer( attacker ) ? attacker : target
		def allowed = !isPlayer(attacker)
		if( player == attacker && !allowed ) { player.centerPrint( "Protect the Flag Stand!" ) }
		return allowed
	}
] as com.gaiaonline.mmo.battle.actor.IMiniEventInteractionSpec

flagStandSpawner = myRooms.BartonColiseum_1.spawnStoppedSpawner("flagStandSpawner", "flag_stand", 1)
flagStandSpawner.setMiniEventSpec(portalSpec)
flagStandSpawner.setPos(790, 720)
flagStandSpawner.setTargetCycle(false)
flagStandSpawner.setMonsterLevelForChildren(monsterLevel)

//-------------------------------------Spawner Lists------------------------------------
gruntSpawnerMap = [1:[omgSpawner1, mosquitoSpawner1, lanternSpawner1], 2:[omgSpawner2, mosquitoSpawner2, lanternSpawner2], 3:[omgSpawner3, mosquitoSpawner3, lanternSpawner3]]
ltSpawnerMap = [1:[omgLTSpawner1, mosquitoLTSpawner1, lanternLTSpawner1], 2:[omgLTSpawner2, mosquitoLTSpawner2, lanternLTSpawner2], 3:[omgLTSpawner3, mosquitoLTSpawner3, lanternLTSpawner3]]
eliteSpawnerMap = [1:[omgEliteSpawner1, mosquitoEliteSpawner1, lanternEliteSpawner1], 2:[omgEliteSpawner2, mosquitoEliteSpawner2, lanternEliteSpawner2], 3:[omgEliteSpawner3, mosquitoEliteSpawner3, lanternEliteSpawner3]]
eliteLTSpawnerMap = [1:[omgEliteLTSpawner1, mosquitoEliteLTSpawner1, lanternEliteLTSpawner1], 2:[omgEliteLTSpawner2, mosquitoEliteLTSpawner2, lanternEliteLTSpawner2], 3:[omgEliteLTSpawner3, mosquitoEliteLTSpawner3, lanternEliteLTSpawner3]]
veteranSpawnerMap = [1:[omgVeteranSpawner1, mosquitoVeteranSpawner2, lanternVeteranSpawner1], 2:[omgVeteranSpawner2, mosquitoVeteranSpawner2, lanternVeteranSpawner2], 3:[omgVeteranSpawner3, mosquitoVeteranSpawner3, lanternVeteranSpawner3]]
portalSpawnerMap= [1:[portalOMGSpawner1, portalMosquitoSpawner1, portalLanternSpawner1], 2:[portalOMGSpawner2,  portalMosquitoSpawner2, portalLanternSpawner2]]

gruntSpawnerList = [omgSpawner1, omgSpawner2, omgSpawner3, mosquitoSpawner1, mosquitoSpawner2, mosquitoSpawner3, lanternSpawner1, lanternSpawner2, lanternSpawner3]
ltSpawnerList = [omgLTSpawner1, omgLTSpawner2, omgLTSpawner3, mosquitoLTSpawner1, mosquitoLTSpawner2, mosquitoLTSpawner3, lanternLTSpawner1, lanternLTSpawner2, lanternLTSpawner3]
eliteSpawnerList = [omgEliteSpawner1, omgEliteSpawner2, omgEliteSpawner3, mosquitoEliteSpawner1, mosquitoEliteSpawner2, mosquitoEliteSpawner3, lanternEliteSpawner1, lanternEliteSpawner2, lanternEliteSpawner3]
eliteLTSpawnerList = [omgEliteLTSpawner1, omgEliteLTSpawner2, omgEliteLTSpawner3, mosquitoEliteLTSpawner1, mosquitoEliteLTSpawner2, mosquitoEliteLTSpawner3, lanternEliteLTSpawner1, lanternEliteLTSpawner2, lanternEliteLTSpawner3]
veteranSpawnerList = [omgVeteranSpawner1, omgVeteranSpawner2, omgVeteranSpawner3, mosquitoVeteranSpawner1, mosquitoVeteranSpawner2, mosquitoVeteranSpawner3, lanternVeteranSpawner1, lanternVeteranSpawner2, lanternVeteranSpawner3]
vampireSpawnerList = [vampireSpawner1, vampireSpawner2, vampireSpawner3]
werewolfSpawnerList = [werewolfSpawner1, werewolfSpawner2, werewolfSpawner3]
portalSpawnerList = [portalSpawner1, portalSpawner2]
portalMonsterSpawnerList = [portalOMGSpawner1, portalOMGSpawner2, portalMosquitoSpawner1, portalMosquitoSpawner2, portalLanternSpawner1, portalLanternSpawner2]

//------------------------------------------------------------------------------
//EVENT LOGIC                                                                   
//------------------------------------------------------------------------------

//Init check
myManager.onEnter(myRooms.BartonColiseum_1) { event ->
	if(isPlayer(event.actor)) {
		if(!playerList.contains(event.actor)) { playerList << event.actor } //Add player to playerList - tracks players in room
		initializeTrial(event) //Call init logic
	}
}

myManager.onExit(myRooms.BartonColiseum_1) { event ->
	if(isPlayer(event.actor)) {
		if(playerList.contains(event.actor)) { playerList.remove(event.actor) }
	}
}

//Init logic
def synchronized initializeTrial(event) {
	if(coliseumInitialized == false) {
		checkDazedStatus()
		coliseumInitialized = true //Prevent re-initialization
		crowdCheer(event)
		
		if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Protection_Difficulty") == true) {
			coliseumDifficulty = event.actor.getTeam().getAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Protection_Difficulty") //Look up and set difficulty
		}
		if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty") == true) {
			coliseumDifficulty = event.actor.getTeam().getAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty") //Look up and set difficulty
			//println "^^^^ Set Difficulty for Coliseum ^^^^"
		}
		if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Time_Difficulty") == true) {
			coliseumDifficulty = event.actor.getTeam().getAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Time_Difficulty") //Look up and set difficulty
		}
		if(coliseumDifficulty == 0) { coliseumDifficulty = 1 } //Default to easy for testing
		
		adjustLevel(event)
		
		if(coliseumDifficulty == 1) { monsterLevel = monsterLevel - 0.6; if(monsterLevel < 1.0) { monsterLevel = 1.0 }} //Monster level cannot be less than 1.0
		if(coliseumDifficulty == 3) { monsterLevel = monsterLevel + 0.5 }
		
		if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Protection_Difficulty") == true) {
			flagStand.setActorState(1)
		
			flagStandMonster = flagStandSpawner.forceSpawnNow()
			myManager.onHealth(flagStandMonster) { health ->
				if(health.didTransition(50) && health.isDecrease()) {
					flagStand.setActorState(2)
				}
			}
		
			runOnDeath(flagStandMonster) {
				flagStand.setActorState(3)
				failProtectionTrial()
			}
		}
		
		if(coliseumDifficulty == 5) {
			makeZoneCounter(IMPOSSIBLE_COUNTER, 0)
		}
		
		myManager.schedule(5) {
			if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Protection_Difficulty") == true) {
				trialType = "protection"
			
				myManager.schedule(portalDelay) { openPortals() }
				nextProtectionWave(event)
			}
			if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty") == true) {
				trialType = "survival"
				makeZoneCounter(SURVIVAL_COUNTER, 0)
				nextSurvivalWave(event)
			}
			if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Time_Difficulty") == true) {
				trialType = "time"
				makeZoneTimer(TIME_TRIAL_TIMER, "0:0:60", "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() })
				nextTimeWave(event)
			}
		}
	}
}

//------------------------------------------------------------------------------
//PROTECTION TRIALS SECTION                                                     
//------------------------------------------------------------------------------
//Initial Protection Logic
def nextProtectionWave(event) {
	crewSize = event.actor.getCrew().size()
	adjustLevel(event)
	if(coliseumDifficulty == 1) {
		if(coliseumWaves <= COLISEUM_PROTECTION_EASY_PHASE_ONE_LIMIT) {
			delayTime = EASY_PHASE_ONE_TIMER
			portalDelay = EASY_PORTAL_DELAY
			spawnProtectionEasyPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_EASY_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_EASY_PHASE_TWO_LIMIT) {
			delayTime = EASY_PHASE_TWO_TIMER
			spawnProtectionEasyPhaseTwo(event)
		}
		if(coliseumWaves == COLISEUM_PROTECTION_EASY_FINAL_WAVE) {
			delayTime = EASY_PHASE_TWO_TIMER
			spawnProtectionEasyFinal(event)
		}
	}
	if(coliseumDifficulty == 2) {
		if(coliseumWaves <= COLISEUM_PROTECTION_MEDIUM_PHASE_ONE_LIMIT) {
			delayTime = MEDIUM_PHASE_ONE_TIMER
			portalDelay = MEDIUM_PORTAL_DELAY
			spawnProtectionMediumPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_MEDIUM_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_MEDIUM_PHASE_TWO_LIMIT) {
			delayTime = MEDIUM_PHASE_TWO_TIMER
			spawnProtectionMediumPhaseTwo(event)
		}
		if(coliseumWaves == COLISEUM_PROTECTION_MEDIUM_FINAL_WAVE) {
			delayTime = MEDIUM_PHASE_TWO_TIMER
			spawnProtectionMediumFinal(event)
		}
	}
	if(coliseumDifficulty == 3) {
		if(coliseumWaves <= COLISEUM_PROTECTION_HARD_PHASE_ONE_LIMIT) {
			delayTime = HARD_PHASE_ONE_TIMER
			portalDelay = HARD_PORTAL_DELAY
			spawnProtectionHardPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_HARD_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_HARD_PHASE_TWO_LIMIT) {
			delayTime = HARD_PHASE_TWO_TIMER
			spawnProtectionHardPhaseTwo(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_HARD_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_HARD_PHASE_THREE_LIMIT) {
			delayTime = HARD_PHASE_THREE_TIMER
			spawnProtectionHardPhaseThree(event)
		}
		if(coliseumWaves == COLISEUM_PROTECTION_HARD_FINAL_WAVE) {
			delayTime = HARD_PHASE_THREE_TIMER
			spawnProtectionHardFinal(event)
		}
	}
	if(coliseumDifficulty == 4) {
		if(coliseumWaves <= COLISEUM_PROTECTION_EXTREME_PHASE_ONE_LIMIT) {
			delayTime = EXTREME_PHASE_ONE_TIMER
			portalDelay = EXTREME_PORTAL_DELAY
			spawnProtectionExtremePhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_EXTREME_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_EXTREME_PHASE_TWO_LIMIT) {
			delayTime = EXTREME_PHASE_TWO_TIMER
			spawnProtectionExtremePhaseTwo(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_EXTREME_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_EXTREME_PHASE_THREE_LIMIT) {
			delayTime = EXTREME_PHASE_THREE_TIMER
			spawnProtectionExtremePhaseThree(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_EXTREME_PHASE_THREE_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_EXTREME_PHASE_FOUR_LIMIT) {
			delayTime = EXTREME_PHASE_FOUR_TIMER
			spawnProtectionExtremePhaseFour(event)
		}
		if(coliseumWaves > COLISEUM_PROTECTION_EXTREME_PHASE_FOUR_LIMIT && coliseumWaves <= COLISEUM_PROTECTION_EXTREME_PHASE_FIVE_LIMIT) {
			delayTime = EXTREME_PHASE_FIVE_TIMER
			spawnProtectionExtremePhaseFive(event)
		}
		if(coliseumWaves == COLISEUM_PROTECTION_EXTREME_FINAL_WAVE) {
			delayTime = EXTREME_PHASE_FIVE_TIMER
			spawnProtectionExtremeFinal(event)
		}
	}
	if(coliseumDifficulty == 5) {
		spawnProtectionImpossibleWave(event)
		if(impossibleTrialActive == false) {
			impossibleTrialActive = true
			delayTime = IMPOSSIBLE_PHASE_ONE_TIMER
		}
	}
}

//-------------------------------------Easy-------------------------------------
def spawnProtectionEasyPhaseOne(event) {
	//println "^^^^ Starting Protection Easy Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monsterList << monster1
			
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		}
	}
}

def spawnProtectionEasyPhaseTwo(event) {
	//println "^^^^ Starting Protection Easy Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		}
	}
}

def spawnProtectionEasyFinal(event) {
	//println "^^^^ Started Protection Easy Final Phase ^^^^"
	spawnerRandom1 = 2
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = omgDemiSpawner.forceSpawnNow() //Spawn boss
		
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
		
		closeGates()
	}
}

//------------------------------------Medium------------------------------------
def spawnProtectionMediumPhaseOne(event) {
	//println "^^^^ Starting Protection Medium Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		}
	}
}

def spawnProtectionMediumPhaseTwo(event) {
	//println "^^^^ Starting Protection Medium Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		} else {
			spawner1 = random(ltSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			ltSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(ltSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			ltSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			ltSpawnerList << spawner1 //Re-add to list for next wave
			ltSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		}
	}
}

def spawnProtectionMediumFinal(event) {
	//println "^^^^ Started Protection Medium Final Phase ^^^^"
	spawnerRandom1 = 2
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = outlawPupDemiSpawner.forceSpawnNow() //Spawn boss
		
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
		
		closeGates()
	}
}

//-------------------------------------Hard-------------------------------------
def spawnProtectionHardPhaseOne(event) {
	//println "^^^^ Starting Protection Hard Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		} else {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		}
	}
}

def spawnProtectionHardPhaseTwo(event) {
	//println "^^^^ Starting Protection Hard Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		} else {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		}
	}
}

def spawnProtectionHardPhaseThree(event) {
	//println "^^^^ Starting Protection Hard Phase Three ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
		}
	}
}

def spawnProtectionHardFinal(event) {
	//println "^^^^ Started Protection Hard Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = lawnmowerSpawner.forceSpawnNow() //Spawn boss
	
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
	}
}

//-----------------------------------Extreme------------------------------------
def spawnProtectionExtremePhaseOne(event) {
	//println "^^^^ Starting Protection Extreme Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		}
	}
}

def spawnProtectionExtremePhaseTwo(event) {
	//println "^^^^ Starting Protection Extreme Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			eliteLTSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		}
	}
}

def spawnProtectionExtremePhaseThree(event) {
	//println "^^^^ Starting Protection Extreme Phase Three ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			eliteLTSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		}
	}
}

def spawnProtectionExtremePhaseFour(event) {
	//println "^^^^ Starting Protection Extreme Phase Four ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		}
	}
}

def spawnProtectionExtremePhaseFive(event) {
	//println "^^^^ Starting Protection Extreme Phase Five ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(veteranSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			veteranSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			veteranSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			hateValue = HATE_VALUE * monsterLevel
			monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
			
			closeGates()
			nextProtectionWave(event)
		}
	}
}

def spawnProtectionExtremeFinal(event) {
	//println "^^^^ Started Protection Extreme Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = lawnmowerSpawner.forceSpawnNow() //Spawn boss
	
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
	}
}

//---------------------------------Impossible-----------------------------------

def spawnProtectionImpossibleWave(event) {
	//println "^^^^ Started Protection Impossible Wave"
	impossibleBossWaveCounter++
	coliseumWaves++
	if(impossibleBossWaveCounter == 10) {
		if(coliseumWaves == 10) {
			spawnerRandom1 = 2
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner2.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				hateValue = HATE_VALUE * monsterLevel
				boss1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

				closeGates()
				nextProtectionWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_TWO_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 20) {
			spawnerRandom1 = 1
			spawnerRandom2 = 3
			
			openGates()
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner1.forceSpawnNow()
				boss2 = vampireSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				hateValue = HATE_VALUE * monsterLevel
				boss1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
				boss2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

				closeGates()
				nextProtectionWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_THREE_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 30) {
			spawnerRandom1 = 1
			spawnerRandom2 = 2
			spawnerRandom3 = 3
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner1.forceSpawnNow()
				boss2 = vampireSpawner2.forceSpawnNow()
				boss3 = vampireSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss3) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				hateValue = HATE_VALUE * monsterLevel
				boss1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
				boss2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
				boss3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

				closeGates()
				nextProtectionWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_FOUR_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 40) {
			spawnerRandom1 = 2
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner2.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				hateValue = HATE_VALUE * monsterLevel
				boss1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

				closeGates()
				nextProtectionWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_FIVE_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 50) {
			spawnerRandom1 = 1
			spawnerRandom2 = 3
			
			openGates()
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner1.forceSpawnNow()
				boss2 = werewolfSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				hateValue = HATE_VALUE * monsterLevel
				boss1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
				boss2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

				closeGates()
				nextProtectionWave(event)
				
				if(delayTime > 5) {
					delayTime = delayTime - 3
				} else if(delayTime > 1) {
					delayTime = delayTime - 1
				}
			}
		}
		if(coliseumWaves >= 60) {
			spawnerRandom1 = 1
			spawnerRandom2 = 2
			spawnerRandom3 = 3
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner1.forceSpawnNow()
				boss2 = werewolfSpawner2.forceSpawnNow()
				boss3 = werewolfSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss3) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				hateValue = HATE_VALUE * monsterLevel
				boss1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
				boss2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
				boss3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

				closeGates()
				nextProtectionWave(event)
			}
		}
	} else {
		if(coliseumWaves < 10) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteSpawnerList << spawner1 //Re-add to list for next wave
					eliteSpawnerList << spawner2 //Re-add to list for next wave
					eliteSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				} else {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				}
			}
		}
		if(coliseumWaves >= 10 && coliseumWaves < 20) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				} else {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave
					eliteLTSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				}
			}
		}
		if(coliseumWaves >= 20 && coliseumWaves < 30) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave
					eliteLTSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster

					monsterList << monster1

					veteranSpawnerList << spawner1 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				}
			}
		}
		if(coliseumWaves >= 30 && coliseumWaves < 40) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster

					monsterList << monster1

					veteranSpawnerList << spawner1 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				}
			}
		}
		if(coliseumWaves >= 40) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(veteranSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					veteranSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave
					veteranSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					hateValue = HATE_VALUE * monsterLevel
					monster1.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster2.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand
					monster3.addHate(flagStandMonster, hateValue.intValue()) //Add hate to flag stand

					closeGates()
					nextProtectionWave(event)
				}
			}
		}
	}	
}

//------------------------------------------------------------------------------
//SURVIVAL TRIALS SECTION                                                       
//------------------------------------------------------------------------------
//Initial Survival Logic
def nextSurvivalWave(event) {
	adjustLevel(event)
	crewSize = event.actor.getCrew().size()
	if(coliseumDifficulty == 1) {
		if(coliseumWaves <= COLISEUM_SURVIVAL_EASY_PHASE_ONE_LIMIT) {
			delayTime = EASY_PHASE_ONE_TIMER
			spawnSurvivalEasyPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_EASY_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_EASY_PHASE_TWO_LIMIT) {
			delayTime = EASY_PHASE_TWO_TIMER
			spawnSurvivalEasyPhaseTwo(event)
		}
		if(coliseumWaves == COLISEUM_SURVIVAL_EASY_FINAL_WAVE) {
			delayTime = EASY_PHASE_TWO_TIMER
			spawnSurvivalEasyFinal(event)
		}
	}
	if(coliseumDifficulty == 2) {
		if(coliseumWaves <= COLISEUM_SURVIVAL_MEDIUM_PHASE_ONE_LIMIT) {
			delayTime = MEDIUM_PHASE_ONE_TIMER
			spawnSurvivalMediumPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_MEDIUM_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_MEDIUM_PHASE_TWO_LIMIT) {
			delayTime = MEDIUM_PHASE_TWO_TIMER
			spawnSurvivalMediumPhaseTwo(event)
		}
		if(coliseumWaves == COLISEUM_SURVIVAL_MEDIUM_FINAL_WAVE) {
			delayTime = MEDIUM_PHASE_TWO_TIMER
			spawnSurvivalMediumFinal(event)
		}
	}
	if(coliseumDifficulty == 3) {
		if(coliseumWaves <= COLISEUM_SURVIVAL_HARD_PHASE_ONE_LIMIT) {
			delayTime = HARD_PHASE_ONE_TIMER
			spawnSurvivalHardPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_HARD_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_HARD_PHASE_TWO_LIMIT) {
			delayTime = HARD_PHASE_TWO_TIMER
			spawnSurvivalHardPhaseTwo(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_HARD_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_HARD_PHASE_THREE_LIMIT) {
			delayTime = HARD_PHASE_THREE_TIMER
			spawnSurvivalHardPhaseThree(event)
		}
		if(coliseumWaves == COLISEUM_SURVIVAL_HARD_FINAL_WAVE) {
			delayTime = HARD_PHASE_THREE_TIMER
			spawnSurvivalHardFinal(event)
		}
	}
	if(coliseumDifficulty == 4) {
		if(coliseumWaves <= COLISEUM_SURVIVAL_EXTREME_PHASE_ONE_LIMIT) {
			delayTime = EXTREME_PHASE_ONE_TIMER
			spawnSurvivalExtremePhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_EXTREME_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_EXTREME_PHASE_TWO_LIMIT) {
			delayTime = EXTREME_PHASE_TWO_TIMER
			spawnSurvivalExtremePhaseTwo(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_EXTREME_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_EXTREME_PHASE_THREE_LIMIT) {
			delayTime = EXTREME_PHASE_THREE_TIMER
			spawnSurvivalExtremePhaseThree(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_EXTREME_PHASE_THREE_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_EXTREME_PHASE_FOUR_LIMIT) {
			delayTime = EXTREME_PHASE_FOUR_TIMER
			spawnSurvivalExtremePhaseFour(event)
		}
		if(coliseumWaves > COLISEUM_SURVIVAL_EXTREME_PHASE_FOUR_LIMIT && coliseumWaves <= COLISEUM_SURVIVAL_EXTREME_PHASE_FIVE_LIMIT) {
			delayTime = EXTREME_PHASE_FIVE_TIMER
			spawnSurvivalExtremePhaseFive(event)
		}
		if(coliseumWaves == COLISEUM_SURVIVAL_EXTREME_FINAL_WAVE) {
			delayTime = EXTREME_PHASE_FIVE_TIMER
			spawnSurvivalExtremeFinal(event)
		}
	}
	if(coliseumDifficulty == 5) {
		spawnSurvivalImpossibleWave(event)
		if(impossibleTrialActive == false) {
			impossibleTrialActive = true
			delayTime = IMPOSSIBLE_PHASE_ONE_TIMER
		}
	}
}

//-------------------------------------Easy-------------------------------------
def spawnSurvivalEasyPhaseOne(event) {
	//println "^^^^ Starting Survival Easy Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monsterList << monster1
			
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnSurvivalEasyPhaseTwo(event) {
	//println "^^^^ Starting Survival Easy Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnSurvivalEasyFinal(event) {
	//println "^^^^ Started Survival Easy Final Phase ^^^^"
	spawnerRandom1 = 2
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = omgDemiSpawner.forceSpawnNow() //Spawn boss
		
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
		
		closeGates()
	}
}

//------------------------------------Medium------------------------------------
def spawnSurvivalMediumPhaseOne(event) {
	//println "^^^^ Starting Survival Medium Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnSurvivalMediumPhaseTwo(event) {
	//println "^^^^ Starting Survival Medium Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(ltSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			ltSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(ltSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			ltSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			ltSpawnerList << spawner1 //Re-add to list for next wave
			ltSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnSurvivalMediumFinal(event) {
	//println "^^^^ Started Survival Medium Final Phase ^^^^"
	spawnerRandom1 = 2
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = outlawPupDemiSpawner.forceSpawnNow() //Spawn boss
		
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
		
		closeGates()
	}
}

//-------------------------------------Hard-------------------------------------
def spawnSurvivalHardPhaseOne(event) {
	//println "^^^^ Starting Survival Hard Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnSurvivalHardPhaseTwo(event) {
	//println "^^^^ Starting Survival Hard Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnSurvivalHardPhaseThree(event) {
	//println "^^^^ Starting Survival Hard Phase Three ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnSurvivalHardFinal(event) {
	//println "^^^^ Started Survival Hard Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = lawnmowerSpawner.forceSpawnNow() //Spawn boss
	
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
	}	
}

//-----------------------------------Extreme------------------------------------
def spawnSurvivalExtremePhaseOne(event) {
	//println "^^^^ Starting Survival Extreme Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			ltSpawnerList << spawner1 //Re-add to list for next wave
			ltSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		}
	}
}

def spawnSurvivalExtremePhaseTwo(event) {
	//println "^^^^ Starting Survival Extreme Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			eliteLTSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		}
	}
}

def spawnSurvivalExtremePhaseThree(event) {
	//println "^^^^ Starting Survival Extreme Phase Three ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			eliteLTSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		}
	}
}

def spawnSurvivalExtremePhaseFour(event) {
	//println "^^^^ Starting Survival Extreme Phase Four ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		}
	}
}

def spawnSurvivalExtremePhaseFive(event) {
	//println "^^^^ Starting Survival Extreme Phase Five ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(veteranSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			veteranSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			veteranSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextSurvivalWave(event)
		}
	}
}

def spawnSurvivalExtremeFinal(event) {
	//println "^^^^ Started Survival Extreme Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = lawnmowerSpawner.forceSpawnNow() //Spawn boss
	
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
	}
}

//---------------------------------Impossible-----------------------------------

def spawnSurvivalImpossibleWave(event) {
	//println "^^^^ Started Survival Impossible Wave"
	impossibleBossWaveCounter++
	coliseumWaves++
	if(impossibleBossWaveCounter == 10) {
		if(coliseumWaves == 10) {
			spawnerRandom1 = 2
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner2.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextSurvivalWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_TWO_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 20) {
			spawnerRandom1 = 1
			spawnerRandom2 = 3
			
			openGates()
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner1.forceSpawnNow()
				boss2 = vampireSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextSurvivalWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_THREE_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 30) {
			spawnerRandom1 = 1
			spawnerRandom2 = 2
			spawnerRandom3 = 3
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner1.forceSpawnNow()
				boss2 = vampireSpawner2.forceSpawnNow()
				boss3 = vampireSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss3) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand
				boss3.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextSurvivalWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_FOUR_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 40) {
			spawnerRandom1 = 2
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner2.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextSurvivalWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_FIVE_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 50) {
			spawnerRandom1 = 1
			spawnerRandom2 = 3
			
			openGates()
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner1.forceSpawnNow()
				boss2 = werewolfSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextSurvivalWave(event)
				
				if(delayTime > 5) {
					delayTime = delayTime - 3
				} else if(delayTime > 1) {
					delayTime = delayTime - 1
				}
			}
		}
		if(coliseumWaves >= 60) {
			spawnerRandom1 = 1
			spawnerRandom2 = 2
			spawnerRandom3 = 3
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner1.forceSpawnNow()
				boss2 = werewolfSpawner2.forceSpawnNow()
				boss3 = werewolfSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss3) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand
				boss3.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextSurvivalWave(event)
			}
		}
	} else {
		if(coliseumWaves < 10) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteSpawnerList << spawner1 //Re-add to list for next wave
					eliteSpawnerList << spawner2 //Re-add to list for next wave
					eliteSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				} else {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				}
			}
		}
		if(coliseumWaves >= 10 && coliseumWaves < 20) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				} else {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave
					eliteLTSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				}
			}
		}
		if(coliseumWaves >= 20 && coliseumWaves < 30) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave
					eliteLTSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster

					monsterList << monster1

					veteranSpawnerList << spawner1 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				}
			}
		}
		if(coliseumWaves >= 30 && coliseumWaves < 40) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster

					monsterList << monster1

					veteranSpawnerList << spawner1 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				}
			}
		}
		if(coliseumWaves >= 40) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(veteranSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					veteranSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave
					veteranSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
						getZoneCounter(SURVIVAL_COUNTER)?.increment()
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextSurvivalWave(event)
				}
			}
		}
	}
}

//------------------------------------------------------------------------------
//TIME TRIALS SECTION                                                           
//------------------------------------------------------------------------------
//Initial Time Logic
def nextTimeWave(event) {
	adjustLevel(event)
	crewSize = event.actor.getCrew().size()
	if(coliseumDifficulty == 1) {
		if(coliseumWaves <= COLISEUM_TIME_EASY_PHASE_ONE_LIMIT) {
			delayTime = EASY_PHASE_ONE_TIMER
			spawnTimeEasyPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_TIME_EASY_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_EASY_PHASE_TWO_LIMIT) {
			delayTime = EASY_PHASE_TWO_TIMER
			spawnTimeEasyPhaseTwo(event)
		}
		if(coliseumWaves == COLISEUM_TIME_EASY_FINAL_WAVE) {
			delayTime = EASY_PHASE_TWO_TIMER
			spawnTimeEasyFinal(event)
		}
	}
	if(coliseumDifficulty == 2) {
		if(coliseumWaves <= COLISEUM_TIME_MEDIUM_PHASE_ONE_LIMIT) {
			delayTime = MEDIUM_PHASE_ONE_TIMER
			spawnTimeMediumPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_TIME_MEDIUM_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_MEDIUM_PHASE_TWO_LIMIT) {
			delayTime = MEDIUM_PHASE_TWO_TIMER
			spawnTimeMediumPhaseTwo(event)
		}
		if(coliseumWaves == COLISEUM_TIME_MEDIUM_FINAL_WAVE) {
			delayTime = MEDIUM_PHASE_TWO_TIMER
			spawnTimeMediumFinal(event)
		}
	}
	if(coliseumDifficulty == 3) {
		if(coliseumWaves <= COLISEUM_TIME_HARD_PHASE_ONE_LIMIT) {
			delayTime = HARD_PHASE_ONE_TIMER
			spawnTimeHardPhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_TIME_HARD_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_HARD_PHASE_TWO_LIMIT) {
			delayTime = HARD_PHASE_TWO_TIMER
			spawnTimeHardPhaseTwo(event)
		}
		if(coliseumWaves > COLISEUM_TIME_HARD_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_TIME_HARD_PHASE_THREE_LIMIT) {
			delayTime = HARD_PHASE_THREE_TIMER
			spawnTimeHardPhaseThree(event)
		}
		if(coliseumWaves == COLISEUM_TIME_HARD_FINAL_WAVE) {
			delayTime = HARD_PHASE_THREE_TIMER
			spawnTimeHardFinal(event)
		}
	}
	if(coliseumDifficulty == 4) {
		if(coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_ONE_LIMIT) {
			delayTime = EXTREME_PHASE_ONE_TIMER
			spawnTimeExtremePhaseOne(event)
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_TWO_LIMIT) {
			delayTime = EXTREME_PHASE_TWO_TIMER
			spawnTimeExtremePhaseTwo(event)
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_THREE_LIMIT) {
			delayTime = EXTREME_PHASE_THREE_TIMER
			spawnTimeExtremePhaseThree(event)
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_THREE_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_FOUR_LIMIT) {
			delayTime = EXTREME_PHASE_FOUR_TIMER
			spawnTimeExtremePhaseFour(event)
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_FOUR_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_FIVE_LIMIT) {
			delayTime = EXTREME_PHASE_FIVE_TIMER
			spawnTimeExtremePhaseFive(event)
		}
		if(coliseumWaves == COLISEUM_TIME_EXTREME_FINAL_WAVE) {
			delayTime = EXTREME_PHASE_FIVE_TIMER
			spawnTimeExtremeFinal(event)
		}
	}
	if(coliseumDifficulty == 5) {
		spawnTimeImpossibleWave(event)
		if(impossibleTrialActive == false) {
			impossibleTrialActive = true
			delayTime = IMPOSSIBLE_PHASE_ONE_TIMER
		}
	}
}

//-------------------------------------Easy-------------------------------------
def spawnTimeEasyPhaseOne(event) {
	//println "^^^^ Starting Time Easy Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		if(timerActive == false) { //Start the countdown timer if it isn't running already
			timerActive = true
			getZoneTimer(TIME_TRIAL_TIMER).start()
		}
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monsterList << monster1 //Add monster to a list for disposal on failure
			
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
				getZoneCounter(SURVIVAL_COUNTER)?.increment()
			} 
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			} 
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			} 
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnTimeEasyPhaseTwo(event) {
	//println "^^^^ Starting Time Easy Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnTimeEasyFinal(event) {
	//println "^^^^ Started Time Easy Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = omgDemiSpawner.forceSpawnNow() //Spawn boss
		
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
		
		closeGates()
	}
}

//------------------------------------Medium------------------------------------
def spawnTimeMediumPhaseOne(event) {
	//println "^^^^ Starting Time Medium Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		if(timerActive == false) { //Start the countdown timer if it isn't running already
			timerActive = true
			getZoneTimer(TIME_TRIAL_TIMER).start()
		}
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnTimeMediumPhaseTwo(event) {
	//println "^^^^ Starting Time Medium Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(ltSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			ltSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(ltSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			ltSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			ltSpawnerList << spawner1 //Re-add to list for next wave
			ltSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnTimeMediumFinal(event) {
	//println "^^^^ Started Time Medium Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = outlawPupDemiSpawner.forceSpawnNow() //Spawn boss
		
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
		
		closeGates()
	}
}

//-------------------------------------Hard-------------------------------------
def spawnTimeHardPhaseOne(event) {
	//println "^^^^ Starting Time Hard Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		if(timerActive == false) { //Start the countdown timer if it isn't running already
			timerActive = true
			getZoneTimer(TIME_TRIAL_TIMER).start()
		}
		coliseumWaves++ //Increment counter for wave tracking
		if(crewSize <= 3) {
			spawner1 = random(gruntSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			gruntSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(gruntSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			gruntSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(gruntSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			gruntSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			gruntSpawnerList << spawner1 //Re-add to list for next wave
			gruntSpawnerList << spawner2 //Re-add to list for next wave
			gruntSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnTimeHardPhaseTwo(event) {
	//println "^^^^ Starting Time Hard Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnTimeHardPhaseThree(event) {
	//println "^^^^ Starting Time Hard Phase Three ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			ltSpawnerList << spawner1 //Re-add to list for next wave
			ltSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
		}
	}
}

def spawnTimeHardFinal(event) {
	//println "^^^^ Started Time Hard Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = lawnmowerSpawner.forceSpawnNow() //Spawn boss
	
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
	}
}

//-----------------------------------Extreme------------------------------------
def spawnTimeExtremePhaseOne(event) {
	//println "^^^^ Starting Time Extreme Phase One ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		if(timerActive == false) { //Start the countdown timer if it isn't running already
			timerActive = true
			getZoneTimer(TIME_TRIAL_TIMER).start()
		}
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteSpawnerList << spawner1 //Re-add to list for next wave
			eliteSpawnerList << spawner2 //Re-add to list for next wave
			eliteSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		}
	}
}

def spawnTimeExtremePhaseTwo(event) {
	//println "^^^^ Starting Time Extreme Phase Two ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		} else {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			eliteLTSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		}
	}
}

def spawnTimeExtremePhaseThree(event) {
	//println "^^^^ Starting Time Extreme Phase Three ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			eliteLTSpawnerList << spawner1 //Re-add to list for next wave
			eliteLTSpawnerList << spawner2 //Re-add to list for next wave
			eliteLTSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		}
	}
}

def spawnTimeExtremePhaseFour(event) {
	//println "^^^^ Starting Time Extreme Phase Four ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		}
	}
}

def spawnTimeExtremePhaseFive(event) {
	//println "^^^^ Starting Time Extreme Phase Five ^^^^"
	if(crewSize <= 3) { 
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
	}
	else {
		spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
		
		spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
		
		spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
		spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
	}
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++
		if(crewSize <= 3) {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		} else {
			spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
			veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated
			
			spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
			veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated
			
			spawner3 = random(veteranSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
			veteranSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated
		
			monster1 = spawner1.forceSpawnNow() //Spawn monster
			monster2 = spawner2.forceSpawnNow() //Spawn monster
			monster3 = spawner3.forceSpawnNow() //Spawn monster
			
			monsterList << monster1
			monsterList << monster2
			monsterList << monster3
		
			veteranSpawnerList << spawner1 //Re-add to list for next wave
			veteranSpawnerList << spawner2 //Re-add to list for next wave
			veteranSpawnerList << spawner3 //Re-add to list for next wave
			
			spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
			spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave
		
			runOnDeath(monster1) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster2) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
			runOnDeath(monster3) { death ->
				endWaveCheck(event) //Check for end of wave to schedule spawn of next
				monsterList.remove(death.actor) //Remove from disposal list
			}
		
			hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
			monster1.addHate(random(hateList), 1) //Add hate to a random player
			monster2.addHate(random(hateList), 1) //Add hate to a random player
			monster3.addHate(random(hateList), 1) //Add hate to a random player
			
			closeGates()
			nextTimeWave(event)
		}
	}
}

def spawnTimeExtremeFinal(event) {
	//println "^^^^ Started Time Extreme Final Phase ^^^^"
	spawnerRandom1 = 2
	
	openGates()
	myManager.schedule(delayTime) {
		coliseumWaves++ //Increment counter for wave tracking
	
		boss = lawnmowerSpawner.forceSpawnNow() //Spawn boss
	
		runOnDeath(boss) { endTrial(event) }
	
		hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
		boss.addHate(random(hateList), 1) //Add hate to a random player
	}
}

//---------------------------------Impossible-----------------------------------

def spawnTimeImpossibleWave(event) {
	//println "^^^^ Started Time Impossible Wave"
	impossibleBossWaveCounter++
	coliseumWaves++
	if(impossibleBossWaveCounter == 10) {
		if(coliseumWaves == 10) {
			spawnerRandom1 = 2
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner2.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextTimeWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_TWO_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 20) {
			spawnerRandom1 = 1
			spawnerRandom2 = 3
			
			openGates()
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner1.forceSpawnNow()
				boss2 = vampireSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextTimeWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_THREE_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 30) {
			spawnerRandom1 = 1
			spawnerRandom2 = 2
			spawnerRandom3 = 3
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = vampireSpawner1.forceSpawnNow()
				boss2 = vampireSpawner2.forceSpawnNow()
				boss3 = vampireSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss3) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand
				boss3.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextTimeWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_FOUR_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 40) {
			spawnerRandom1 = 2
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner2.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextTimeWave(event)
				
				incrementImpossibleRoundCounter = true
				delayTime = IMPOSSIBLE_PHASE_FIVE_TIMER //Set timer for next phase
			}
		}
		if(coliseumWaves == 50) {
			spawnerRandom1 = 1
			spawnerRandom2 = 3
			
			openGates()
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner1.forceSpawnNow()
				boss2 = werewolfSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextTimeWave(event)
				
				if(delayTime > 5) {
					delayTime = delayTime - 3
				} else if(delayTime > 1) {
					delayTime = delayTime - 1
				}
			}
		}
		if(coliseumWaves >= 60) {
			spawnerRandom1 = 1
			spawnerRandom2 = 2
			spawnerRandom3 = 3
			
			openGates()
			
			myManager.schedule(delayTime) {
				boss1 = werewolfSpawner1.forceSpawnNow()
				boss2 = werewolfSpawner2.forceSpawnNow()
				boss3 = werewolfSpawner3.forceSpawnNow()
				
				impossibleBossWaveCounter = 0
				
				runOnDeath(boss1) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss2) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				runOnDeath(boss3) { death ->
					endWaveCheck(event) //Check for end of wave to schedule spawn of next
					monsterList.remove(death.actor) //Remove from disposal list
				}
				
				hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
				boss1.addHate(random(hateList), 1) //Add hate to flag stand
				boss2.addHate(random(hateList), 1) //Add hate to flag stand
				boss3.addHate(random(hateList), 1) //Add hate to flag stand

				closeGates()
				nextTimeWave(event)
			}
		}
	} else {
		if(coliseumWaves < 10) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(timerActive == false) { //Start the countdown timer if it isn't running already
					timerActive = true
					getZoneTimer(TIME_TRIAL_TIMER).start()
				}
				if(crewSize <= 3) {
					spawner1 = random(eliteSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteSpawnerList << spawner1 //Re-add to list for next wave
					eliteSpawnerList << spawner2 //Re-add to list for next wave
					eliteSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				} else {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				}
			}
		}
		if(coliseumWaves >= 10 && coliseumWaves < 20) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				} else {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave
					eliteLTSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				}
			}
		}
		if(coliseumWaves >= 20 && coliseumWaves < 30) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(eliteLTSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(eliteLTSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(eliteLTSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					eliteLTSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					eliteLTSpawnerList << spawner1 //Re-add to list for next wave
					eliteLTSpawnerList << spawner2 //Re-add to list for next wave
					eliteLTSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster

					monsterList << monster1

					veteranSpawnerList << spawner1 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				}
			}
		}
		if(coliseumWaves >= 30 && coliseumWaves < 40) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster

					monsterList << monster1

					veteranSpawnerList << spawner1 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				}
			}
		}
		if(coliseumWaves >= 40) {
			if(crewSize <= 3) { 
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused
			}
			else {
				spawnerRandom1 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom1) //Remove it so it isn't reused

				spawnerRandom2 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom2) //Remove it so it isn't reused

				spawnerRandom3 = random(spawnerRandomSequence) //Choose random gate
				spawnerRandomSequence.remove(spawnerRandom3) //Remove it so it isn't reused
			}

			openGates()
			myManager.schedule(delayTime) {
				if(crewSize <= 3) {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				} else {
					spawner1 = random(veteranSpawnerMap.get(spawnerRandom1)) //Get the spawner to use
					veteranSpawnerList.remove(spawner1) //Remove from list so it isn't duplicated

					spawner2 = random(veteranSpawnerMap.get(spawnerRandom2)) //Get the spawner to use
					veteranSpawnerList.remove(spawner2) //Remove from list so it isn't duplicated

					spawner3 = random(veteranSpawnerMap.get(spawnerRandom3)) //Get the spawner to use
					veteranSpawnerList.remove(spawner3) //Remove from list so it isn't duplicated

					monster1 = spawner1.forceSpawnNow() //Spawn monster
					monster2 = spawner2.forceSpawnNow() //Spawn monster
					monster3 = spawner3.forceSpawnNow() //Spawn monster

					monsterList << monster1
					monsterList << monster2
					monsterList << monster3

					veteranSpawnerList << spawner1 //Re-add to list for next wave
					veteranSpawnerList << spawner2 //Re-add to list for next wave
					veteranSpawnerList << spawner3 //Re-add to list for next wave

					spawnerRandomSequence << spawnerRandom1 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom2 //Re-add gate to list for next wave
					spawnerRandomSequence << spawnerRandom3 //Re-add gate to list for next wave

					runOnDeath(monster1) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster2) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}
					runOnDeath(monster3) { death ->
						endWaveCheck(event) //Check for end of wave to schedule spawn of next
						monsterList.remove(death.actor) //Remove from disposal list
					}

					hateListCleanUp() //Clean up hate list so not attempting to add hate to dazed player
					monster1.addHate(random(hateList), 1) //Add hate to flag stand
					monster2.addHate(random(hateList), 1) //Add hate to flag stand
					monster3.addHate(random(hateList), 1) //Add hate to flag stand

					closeGates()
					nextTimeWave(event)
				}
			}
		}
	}
}

//------------------------------------------------------------------------------
//LEVEL, HATE LIST, END WAVE, END TRIAL, DAZED                                  
//------------------------------------------------------------------------------
def adjustLevel(event) {
	monsterLevel = 1.0
	event.actor.getCrew().clone().each() { //Clone crew and each through to set monster level
		if(it.getConLevel() > monsterLevel && it.getConLevel() <= MAX_MONSTER_LEVEL) {
			monsterLevel = it.getConLevel()
		} else if(it.getConLevel() > MAX_MONSTER_LEVEL) {
			monsterLevel = MAX_MONSTER_LEVEL
			if(it.getConLevel() > 10.0 && playerList.contains(it)) {
				it.centerPrint("You must be level 10.0 or lower for this task.")
				it.centerPrint("Click 'MENU' and select 'Change Level' to lower your level temporarily.")
				it.warp("BARTON_4", 920, 860, 6)
			}
		}
	}
	if(coliseumDifficulty == 1) { monsterLevel = monsterLevel - 0.6 }
	if(coliseumDifficulty == 3) { monsterLevel = monsterLevel + 0.5 }
	if(coliseumDifficulty == 4) { monsterLevel = monsterLevel + 1.0 }
	if(coliseumDifficulty == 5) { monsterLevel = monsterLevel + 1.0 }
	
	gruntSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	ltSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	eliteSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	eliteLTSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	veteranSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	omgDemiSpawner.setMonsterLevelForChildren(monsterLevel)
	outlawPupDemiSpawner.setMonsterLevelForChildren(monsterLevel)
	lawnmowerSpawner.setMonsterLevelForChildren(monsterLevel)
	vampireSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	werewolfSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	portalSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	portalMonsterSpawnerList.clone().each() { it.setMonsterLevelForChildren(monsterLevel) }
	flagStandSpawner.setMonsterLevelForChildren(monsterLevel)
}

def hateListCleanUp() {
	hateList = playerList.clone()
	hateList.clone().each() {
		if(it.isDazed()) { hateList.remove(it) }
	}
}

def synchronized endWaveCheck(event) {
	endWaveCounter = 0
	gruntSpawnerList.clone().each() {
		endWaveCounter = endWaveCounter + it.spawnsInUse()
	}
	ltSpawnerList.clone().each() {
		endWaveCounter = endWaveCounter + it.spawnsInUse()
	}
	eliteSpawnerList.clone().each() {
		endWaveCounter = endWaveCounter + it.spawnsInUse()
	}
	eliteLTSpawnerList.clone().each() {
		endWaveCounter = endWaveCounter + it.spawnsInUse()
	}
	veteranSpawnerList.clone().each() {
		endWaveCounter = endWaveCounter + it.spawnsInUse()
	}
	vampireSpawnerList.clone().each() {
		endWaveCounter = endWaveCounter + it.spawnsInUse()
	}
	werewolfSpawnerList.clone().each() {
		endWaveCounter = endWaveCounter + it.spawnsInUse()
	}
	if(incrementImpossibleRoundCounter == true) {
		incrementImpossibleRoundCounter = false
		getZoneCounter(IMPOSSIBLE_COUNTER).increment()
	}
	if(endWaveCounter == 0) {
		if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Protection_Difficulty") == true) {
			if(coliseumDifficulty < 4) { nextProtectionWave(event) }
			crowdWave(event)
		}
		if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Survival_Difficulty") == true) {
			if(coliseumDifficulty < 4) { nextSurvivalWave(event) }
			crowdWave(event)
		}
		if(event.actor.getTeam().hasAreaVar("Z33_Barton_Coliseum", "Z33_Coliseum_Time_Difficulty") == true) {
			adjustTimer()
			if(coliseumDifficulty < 4) { nextTimeWave(event) }
			crowdWave(event)
		}
	}
}

def adjustTimer() {
	if(coliseumDifficulty == 1) {
		if(coliseumWaves <= COLISEUM_TIME_EASY_PHASE_ONE_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EASY_PHASE_ONE_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_EASY_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_EASY_PHASE_TWO_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EASY_PHASE_TWO_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves == COLISEUM_TIME_EASY_FINAL_WAVE) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EASY_BOSS_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
	}
	if(coliseumDifficulty == 2) {
		if(coliseumWaves <= COLISEUM_TIME_MEDIUM_PHASE_ONE_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + MEDIUM_PHASE_ONE_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_MEDIUM_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_MEDIUM_PHASE_TWO_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + MEDIUM_PHASE_TWO_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves == COLISEUM_TIME_MEDIUM_FINAL_WAVE) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + MEDIUM_BOSS_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
	}
	if(coliseumDifficulty == 3) {
		if(coliseumWaves <= COLISEUM_TIME_HARD_PHASE_ONE_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + HARD_PHASE_ONE_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_HARD_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_HARD_PHASE_TWO_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + HARD_PHASE_TWO_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_HARD_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_TIME_HARD_PHASE_THREE_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + HARD_PHASE_THREE_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves == COLISEUM_TIME_HARD_FINAL_WAVE) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + HARD_BOSS_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
	}
	if(coliseumDifficulty == 4) {
		if(coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_ONE_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EXTREME_PHASE_ONE_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_ONE_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_TWO_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EXTREME_PHASE_TWO_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_TWO_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_THREE_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EXTREME_PHASE_THREE_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_THREE_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_FOUR_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EXTREME_PHASE_FOUR_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves > COLISEUM_TIME_EXTREME_PHASE_FOUR_LIMIT && coliseumWaves <= COLISEUM_TIME_EXTREME_PHASE_FIVE_LIMIT) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EXTREME_PHASE_FIVE_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
		if(coliseumWaves == COLISEUM_TIME_EXTREME_FINAL_WAVE) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + EXTREME_BOSS_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		}
	}
	if(coliseumDifficulty == 5) {
		if(impossibleBossWaveCounter == 10) {
			currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + IMPOSSIBLE_BOSS_ADDED_TIME

			newTimeLimit = "0:0:".concat(currentTime.toString())

			getZoneTimer(TIME_TRIAL_TIMER).stop()
			makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
		} else {
			if(coliseumWaves < 10) {
				currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + IMPOSSIBLE_PHASE_ONE_ADDED_TIME

				newTimeLimit = "0:0:".concat(currentTime.toString())

				getZoneTimer(TIME_TRIAL_TIMER).stop()
				makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
			}
			if(coliseumWaves >= 10 && coliseumWaves < 20) {
				currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + IMPOSSIBLE_PHASE_TWO_ADDED_TIME

				newTimeLimit = "0:0:".concat(currentTime.toString())

				getZoneTimer(TIME_TRIAL_TIMER).stop()
				makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
			}
			if(coliseumWaves >= 20 && coliseumWaves < 30) {
				currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + IMPOSSIBLE_PHASE_THREE_ADDED_TIME

				newTimeLimit = "0:0:".concat(currentTime.toString())

				getZoneTimer(TIME_TRIAL_TIMER).stop()
				makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
			}
			if(coliseumWaves >= 30 && coliseumWaves < 40) {
				currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + IMPOSSIBLE_PHASE_FOUR_ADDED_TIME

				newTimeLimit = "0:0:".concat(currentTime.toString())

				getZoneTimer(TIME_TRIAL_TIMER).stop()
				makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
			}
			if(coliseumWaves >= 40) {
				if(coliseumWaves < 80) {
					timeToAdd = IMPOSSIBLE_PHASE_FIVE_ADDED_TIME - (coliseumWaves - 40)
					//println "^^^^ timeToAdd = ${timeToAdd} ^^^^"
				}
				currentTime = getZoneTimer(TIME_TRIAL_TIMER).getRemainingSeconds() + timeToAdd

				newTimeLimit = "0:0:".concat(currentTime.toString())

				getZoneTimer(TIME_TRIAL_TIMER).stop()
				makeZoneTimer(TIME_TRIAL_TIMER, newTimeLimit, "0:0:0").onCompletion({ trialFailed = true; failTimeTrial() }).start()
			}
		}
	}
}

def endTrial(event) {
	//println "^^^^ Ending Trial ^^^^"
	eventOver = true
	monsterList.clone().each() {
		it.dispose()
		monsterList.remove(it)
	}
	
	if(trialType == "time") {
		removeZoneTimer(TIME_TRIAL_TIMER)
	}
	playerList.clone().each() {
		it.centerPrint("Congratulations! You have won!")
	}
	victoryCheer(event)
	
	if(coliseumDifficulty == 1) {
		orbChance = random(1, 20)
		playerList.clone().each() {
			it.grantCoins(random(150, 200))
			if(orbChance >= EASY_ORB_CHANCE) {
				orbGrant = random(2, 4) * (monsterLevel / 2)
				it.grantQuantityItem(100257, orbGrant.intValue())
			}
		}
	}
	
	if(coliseumDifficulty == 2) {
		orbChance = random(1, 20)
		playerList.clone().each() {
			it.grantCoins(random(250, 400))
			if(orbChance >= MEDIUM_ORB_CHANCE) {
				orbGrant = random(5, 7) * (monsterLevel / 2)
				it.grantQuantityItem(100257, orbGrant.intValue())
			}
		}
	}
	
	if(coliseumDifficulty == 3) {
		orbChance = random(1, 20)
		playerList.clone().each() {
			it.grantCoins(random(500, 750))
			if(orbChance >= HARD_ORB_CHANCE) {
				orbGrant = random(10, 15) * (monsterLevel / 2)
				it.grantQuantityItem(100257, orbGrant.intValue())
			}
		}
	}
	
	if(coliseumDifficulty == 4) {
		orbChance = random(1, 20)
		playerList.clone().each() {
			it.grantCoins(random(1000, 1500))
			if(orbChance >= EXTREME_ORB_CHANCE) {
				orbGrant = random(20, 25) * (monsterLevel / 2)
				it.grantQuantityItem(100257, orbGrant.intValue())
			}
		}
	}
	
	playerList.clone().each() {
		if(trialType == "protection") {
			if(isPlayer(it) && coliseumDifficulty == 1 && !it.isDoneQuest(354)) {
				it.updateQuest(354, "Story-VQS")
			}
			if(isPlayer(it) && coliseumDifficulty == 2 && !it.isDoneQuest(355)) {
				it.updateQuest(355, "Story-VQS")
			}
			if(isPlayer(it) && coliseumDifficulty == 3 && !it.isDoneQuest(356)) {
				it.updateQuest(356, "Story-VQS")
			}
		}
		if(trialType == "survival") {
			if(isPlayer(it) && coliseumDifficulty == 1 && !it.isDoneQuest(351)) {
				it.updateQuest(351, "Story-VQS")
			}
			if(isPlayer(it) && coliseumDifficulty == 2 && !it.isDoneQuest(352)) {
				it.updateQuest(352, "Story-VQS")
			}
			if(isPlayer(it) && coliseumDifficulty == 3 && !it.isDoneQuest(353)) {
				it.updateQuest(353, "Story-VQS")
			}
		}
		if(trialType == "time") {
			if(isPlayer(it) && coliseumDifficulty == 1 && !it.isDoneQuest(357)) {
				it.updateQuest(357, "Story-VQS")
			}
			if(isPlayer(it) && coliseumDifficulty == 2 && !it.isDoneQuest(358)) {
				it.updateQuest(358, "Story-VQS")
			}
			if(isPlayer(it) && coliseumDifficulty == 3 && !it.isDoneQuest(359)) {
				it.updateQuest(359, "Story-VQS")
			}
		}
	}
	myManager.schedule(10) {
		playerList.clone().each() {
			it.centerPrint("This concludes the trial. Please play again!")
			it.warp("BARTON_4", 920, 860, 6)
		}
	}
}

def failTimeTrial() {
	//println "^^^^ Time Trial Failed ^^^^"
	removeZoneTimer(TIME_TRIAL_TIMER)
	sound("CrowdBoo").toZone()
	monsterList.clone().each() {
		it.dispose()
		monsterList.remove(it)
	}
	playerList.clone().each() {
		if(coliseumDifficulty == 5) {
			if(getZoneCounter(IMPOSSIBLE_COUNTER).getValue() < 1) {
				if(coliseumWaves >= it.getPlayerVar("impossibleTrackingVar")) {
					it.setPlayerVar("impossibleTrackingVar", coliseumWaves)
				}
				it.centerPrint("You must complete at least one round to earn a reward.")
				it.warp("BARTON_4", 920, 860, 6)
			} else {
				if(coliseumWaves >= it.getPlayerVar("impossibleTrackingVar")) {
					it.setPlayerVar("impossibleTrackingVar", coliseumWaves)
				}
				coinGrant = random(150, 200) * getZoneCounter(IMPOSSIBLE_COUNTER).getValue()
				it.grantCoins(coinGrant.intValue())
				orbChance = random(1, 20)
				if(orbChance > IMPOSSIBLE_ORB_CHANCE) {
					orbGrant = random(4, 5) * (monsterLevel / 2) * getZoneCounter(IMPOSSIBLE_COUNTER).getValue()
					it.grantQuantityItem(100257, orbGrant.intValue())
				}
				it.warp("BARTON_4", 920, 860, 6)
			}
		} else {
			it.centerPrint("You have failed the trial!")
			it.warp("BARTON_4", 920, 860, 6)
		}
	}
}

def failProtectionTrial() {
	//println "^^^^ Protection Trial Failed ^^^^"
	sound("CrowdBoo").toZone()
	monsterList.clone().each() {
		if(!it.isDead()) {
			it.dispose()
			monsterList.remove(it)
		}
	}
	if(!portalMonster1?.isDead()) {
		portalMonster1?.dispose()
	}
	if(!portalMonster2?.isDead()) {
		portalMonster2?.dispose()
	}
	playerList.clone().each() {
		if(coliseumDifficulty == 5) {
			if(getZoneCounter(IMPOSSIBLE_COUNTER).getValue() < 1) {
				if(coliseumWaves >= it.getPlayerVar("impossibleTrackingVar")) {
					it.setPlayerVar("impossibleTrackingVar", coliseumWaves)
				}
				it.centerPrint("You must complete at least one round to earn a reward.")
				it.warp("BARTON_3", 1410, 900, 6)
			} else {
				if(coliseumWaves >= it.getPlayerVar("impossibleTrackingVar")) {
					it.setPlayerVar("impossibleTrackingVar", coliseumWaves)
				}
				coinGrant = random(150, 200) * getZoneCounter(IMPOSSIBLE_COUNTER).getValue()
				it.grantCoins(coinGrant.intValue())
				orbChance = random(1, 20)
				if(orbChance > IMPOSSIBLE_ORB_CHANCE) {
					orbGrant = random(4, 5) * (monsterLevel / 2) * getZoneCounter(IMPOSSIBLE_COUNTER).getValue()
					it.grantQuantityItem(100257, orbGrant.intValue())
					it.centerPrint("This concludes the trial. You earned ${coinGrant.intValue()} gold and ${orbGrant.intValue()} orbs!")
				} else {
					it.centerPrint("This concludes the trial. You earned ${coinGrant.intValue()} gold!")
				}
				it.warp("BARTON_3", 1410, 900, 6)
			}
		} else {
			it.centerPrint("You have failed the trial!")
			it.warp("BARTON_3", 1410, 900, 6)
		}
	}
}

def checkDazedStatus() {
	allDazed = false
	numDazed = 0
	myManager.schedule(5) { checkDazedStatus() }
	playerList.clone().each { if( it.isDazed() ) { numDazed ++ } }
	if( numDazed >= playerList.size() ) { //If all players in room are dazed, warp them out
		allDazed = true
		myRooms.BartonColiseum_1.getActorList().each{
			if( isPlayer( it ) ) {
				if(coliseumDifficulty == 5) {
					if(getZoneCounter(IMPOSSIBLE_COUNTER).getValue() < 1) {
						if(coliseumWaves >= it.getPlayerVar("impossibleTrackingVar")) {
							it.setPlayerVar("impossibleTrackingVar", coliseumWaves)
						}
						it.centerPrint("You must complete at least one round to earn a reward.")
						it.warp("BARTON_4", 920, 860, 6)
					} else {
						if(coliseumWaves >= it.getPlayerVar("impossibleTrackingVar")) {
							it.setPlayerVar("impossibleTrackingVar", coliseumWaves)
						}
						coinGrant = random(150, 200) * getZoneCounter(IMPOSSIBLE_COUNTER).getValue()
						it.grantCoins(coinGrant.intValue())
						orbChance = random(1, 20)
						if(orbChance > IMPOSSIBLE_ORB_CHANCE) {
							orbGrant = random(4, 5) * (monsterLevel / 2) * getZoneCounter(IMPOSSIBLE_COUNTER).getValue()
							it.grantQuantityItem(100257, orbGrant.intValue())
							it.centerPrint("This concludes the trial. You earned ${coinGrant.intValue()} gold and ${orbGrant.intValue()} orbs!")
						} else {
							it.centerPrint("This concludes the trial. You earned ${coinGrant.intValue()} gold!")
						}
						it.warp("BARTON_4", 920, 860, 6)
					}
				} else {
					it.centerPrint( "You have failed the trial." )
					it.warp("BARTON_4", 920, 860, 6)
				}
			}
		}			
	}
}

//------------------------------------------------------------------------------
//CROWD AND SWITCHES                                                            
//------------------------------------------------------------------------------
//Crowd
def waveLeft(event) {
	new WorldEvent(event.actor.getArea(), myManager.postmaster, "Crowd", [doWave:"right_left"]).toZone();
}

def waveRight(event) {
	new WorldEvent(event.actor.getArea(), myManager.postmaster, "Crowd", [doWave:"left_right"]).toZone();
}

def startJump(event) {
	new WorldEvent(event.actor.getArea(), myManager.postmaster, "Crowd", [jump:true]).toZone();
}

def stopJump(event) {
	new WorldEvent(event.actor.getArea(), myManager.postmaster, "Crowd", [jump:false]).toZone();
}

//Flags
orangeFlag01 = makeGeneric("OrangeFlag01", myRooms.BartonColiseum_1, 185, 960)
orangeFlag01.setStateful()
orangeFlag01.setUsable(false)

def startOrangeFlag01() {
	orangeFlag01.setActorState(1)
}

def stopOrangeFlag01() {
	orangeFlag01.setActorState(0)
}

orangeFlag02 = makeGeneric("OrangeFlag02", myRooms.BartonColiseum_1, 1330, 960)
orangeFlag02.setStateful()
orangeFlag02.setUsable(false)

def startOrangeFlag02() {
	orangeFlag02.setActorState(1)
}

def stopOrangeFlag02() {
	orangeFlag02.setActorState(0)
}

yellowFlag01 = makeGeneric("YellowFlag01", myRooms.BartonColiseum_1, 1490, 960)
yellowFlag01.setStateful()
yellowFlag01.setUsable(false)

def startYellowFlag01() {
	yellowFlag01.setActorState(1)
}

def stopYellowFlag01() {
	yellowFlag01.setActorState(0)
}

//Flag Stand
flagStand = makeGeneric("FlagStand", myRooms.BartonColiseum_1, 680, 380)
flagStand.setStateful()
flagStand.setUsable(false)

//Petals
petalSwitchA = makeGeneric("PetalSwitchA", myRooms.BartonColiseum_1, 270, 40)
petalSwitchA.setStateful()
petalSwitchA.setUsable(false)

def startPetalSwitchA() {
	petalSwitchA.setActorState(1)
}

def stopPetalSwitchA() {
	petalSwitchA.setActorState(0)
}

petalSwitchB = makeGeneric("PetalSwitchB", myRooms.BartonColiseum_1, 270, 40)
petalSwitchB.setStateful()
petalSwitchB.setUsable(false)

def startPetalSwitchB() {
	petalSwitchB.setActorState(1)
}

def stopPetalSwitchB() {
	petalSwitchB.setActorState(0)
}

petalSwitchC = makeGeneric("PetalSwitchC", myRooms.BartonColiseum_1, 270, 40)
petalSwitchC.setStateful()
petalSwitchC.setUsable(false)

def startPetalSwitchC() {
	petalSwitchC.setActorState(1)
}

def stopPetalSwitchC() {
	petalSwitchC.setActorState(0)
}

petalSwitchD = makeGeneric("PetalSwitchD", myRooms.BartonColiseum_1, 270, 40)
petalSwitchD.setStateful()
petalSwitchD.setUsable(false)

def startPetalSwitchD() {
	petalSwitchD.setActorState(1)
}

def stopPetalSwitchD() {
	petalSwitchD.setActorState(0)
}

//Gates
leftGate = makeGeneric("GateLeft", myRooms.BartonColiseum_1, 185, 960)
leftGate.setStateful()
leftGate.setUsable(false)

centerGate = makeGeneric("GateCenter", myRooms.BartonColiseum_1, 185, 960)
centerGate.setStateful()
centerGate.setUsable(false)

rightGate = makeGeneric("GateRight", myRooms.BartonColiseum_1, 185, 960)
rightGate.setStateful()
rightGate.setUsable(false)

//Plaques
plaqueLeft = makeGeneric("WinnersLeft", myRooms.BartonColiseum_1, 280, 110)
plaqueLeft.setStateful()
plaqueLeft.setUsable(true)
plaqueLeft.setRange(1000)
plaqueLeft.setMouseoverText("Honored Guests - West")

plaqueLeft.onUse { event -> displayHonoredGuests1(event) }

def displayHonoredGuests1(event) {
	tutorialNPC.pushDialog(event.player, "readLeftPlaque")
}

def readLeftPlaque = tutorialNPC.createConversation("readLeftPlaque", true)

def readLeftPlaque1 = [id:1]
readLeftPlaque1.npctext = "<zOMG dialogWidth='200'><![CDATA[<h1><b><font face='Arial' size='14'>Honored Guests - West</font></b></h1><font face='Arial' size ='12'>II gneiss II<br>Rogue Jaguar<br>Nevets Cloud<br>EvoNation<br>timaty<br>KLN<br><br>Tazenda<br>Redeye War<br>Kaitan Guo<br>i am with u all the way<br>Flux<br>Sensual Soul<br><br>lethal01<br>xo_Tararox4life_xo<br>Parfait Pichu<br><br>K` the warrior<br>Lohari<br>The Soap Faerie<br>Gideon the Twilight Sage<br>]]></zOMG>"
readLeftPlaque1.result = DONE
readLeftPlaque.addDialog(readLeftPlaque1, tutorialNPC)

plaqueRight = makeGeneric("WinnersRight", myRooms.BartonColiseum_1, 1270, 110)
plaqueRight.setStateful()
plaqueRight.setUsable(true)
plaqueRight.setRange(1000)
plaqueRight.setMouseoverText("Honored Guests - East")

plaqueRight.onUse { event -> displayHonoredGuests2(event) }

def displayHonoredGuests2(event) {
	tutorialNPC.pushDialog(event.player, "readRightPlaque")
}

def readRightPlaque = tutorialNPC.createConversation("readRightPlaque", true)

def readRightPlaque1 = [id:1]
readRightPlaque1.npctext = "<zOMG dialogWidth='200'><![CDATA[<h1><b><font face='Arial' size='14'>Honored Guests - East</font></b></h1><font face='Arial' size ='12'>Lissianthus<br>Bladeglory<br>[-Teh Doomy Doom-]<br>xXx_escape_the_fate_xXx17<br>dimentia44<br>RAYEARTH-princess<br><br>Joot Box<br>Shiori Miko<br>xBISHOPx<br>Luzianne<br>Viper_1<br>Pudding Disaster<br><br>YeaSoWatIm2Hott2Handle<br>idleRAT<br><br>Nagato rin-negan<br>Environmental Unity<br>]]></zOMG>"
readRightPlaque1.result = DONE
readRightPlaque.addDialog(readRightPlaque1, tutorialNPC)

//------------------------------------------------------------------------------
//FLAG CROWD PETALS GATES                                                       
//------------------------------------------------------------------------------
def crowdCheer(event) {
	if(orangeFlag01.getActorState() == 0 && orangeFlag02.getActorState() == 0 && yellowFlag01.getActorState() == 0) {
		myManager.schedule(5) { crowdCheer(event) }
		startJump(event)
		startOrangeFlag01()
		startOrangeFlag02()
		startYellowFlag01()
		sound("CrowdCheer").toZone()
	} else {
		stopJump(event)
		stopOrangeFlag01()
		stopOrangeFlag02()
		stopYellowFlag01()
	}
}

def victoryCheer(event) {
	if(orangeFlag01.getActorState() == 0 && orangeFlag02.getActorState() == 0 && yellowFlag01.getActorState() == 0) {
		myManager.schedule(10) { victoryCheer(event) }
		startJump(event)
		startOrangeFlag01()
		startOrangeFlag02()
		startYellowFlag01()
		sound("CrowdCheer").toZone()
		
		startPetalSwitchA()
		startPetalSwitchB()
		startPetalSwitchC()
		startPetalSwitchD()
	} else {
		stopJump(event)
		stopOrangeFlag01()
		stopOrangeFlag02()
		stopYellowFlag01()
		
		stopPetalSwitchA()
		stopPetalSwitchB()
		stopPetalSwitchC()
		stopPetalSwitchD()
	}
}

def crowdWave(event) {
	waveLeft(event)
	myManager.schedule(2) { waveRight(event) }
}

def openGates() {
	if(spawnerRandom1 == 1 || spawnerRandom2 == 1 || spawnerRandom3 == 1) {
		leftGate.setActorState(1)
		sound("GateLeftOpen").toZone()
	}
	if(spawnerRandom1 == 2 || spawnerRandom2 == 2 || spawnerRandom3 == 2) {
		centerGate.setActorState(1)
		sound("GateCenterOpen").toZone()
	}
	if(spawnerRandom1 == 3 || spawnerRandom2 == 3 || spawnerRandom3 == 3) {
		rightGate.setActorState(1)
		sound("GateRightOpen").toZone()
	}
}

def synchronized openPortals() {
	if(eventOver == false) {
		myManager.schedule(portalDelay) { openPortals() }
		if(portal1Active == false && portal2Active == false) {
			portalRandom = random(1, 2)
			if(portalRandom == 1) { openPortal1 = true }
			if(portalRandom == 2) { openPortal2 = true }
		} else {
			if(portal1Active == false) { openPortal1 = true }
			if(portal2Active == false) { openPortal2 = true }
		}
		if(openPortal1 == true) {
			openPortal1 = false
			portal1Active = true
		
			spawnPortal1()
		}
		if(openPortal2 == true) {
			openPortal2 = false
			portal2Active = true
		
			spawnPortal2()
		}
	}
}

def synchronized spawnPortal1() {
	if(portal1Active == true) {
		portal1Spawner = random(portalSpawnerMap.get(1))
		
		portal1 = portalSpawner1.forceSpawnNow()
		portalMonster1 = portal1Spawner.forceSpawnNow()
		//println "^^^^ Spawned ${portalMonster1} at portal 1 ^^^^"
		
		monsterList << portal1
		monsterList << portalMonster1
		
		hateValue = HATE_VALUE * monsterLevel
		portalMonster1.addHate(flagStandMonster, hateValue.intValue())
		
		portal1ROD = myManager.schedule(30) { spawnPortal1() }
		
		runOnDeath(portal1) { death ->
			portal1Active = false 
			monsterList.remove(death.actor)
			portal1ROD.cancel()
		}
		
		runOnDeath(portalMonster1) { death ->
			monsterList.remove(death.actor)
		}
	}
}

def synchronized spawnPortal2() {
	if(portal2Active == true) {
		portal2Spawner = random(portalSpawnerMap.get(2))
		
		portal2 = portalSpawner2.forceSpawnNow()
		portalMonster2 = portal2Spawner.forceSpawnNow()
		//println "^^^^ Spawned ${portalMonster2} at portal 2 ^^^^"
		
		monsterList << portal2
		monsterList << portalMonster2
		
		hateValue = HATE_VALUE * monsterLevel
		portalMonster2.addHate(flagStandMonster, hateValue.intValue())
		
		portal2ROD = myManager.schedule(30) { spawnPortal2() }
		
		runOnDeath(portal2) { death ->
			portal2Active = false 
			monsterList.remove(death.actor)
			portal2ROD.cancel()
		}
		
		runOnDeath(portalMonster2) { death ->
			monsterList.remove(death.actor)
		}
	}
}

def closeGates() {
	if(leftGate.getActorState() == 1) {
		leftGate.setActorState(0)
		sound("GateLeftClose").toZone()
	}
	if(centerGate.getActorState() == 1) {
		centerGate.setActorState(0)
		sound("GateCenterClose").toZone()
	}
	if(rightGate.getActorState() == 1) {
		rightGate.setActorState(0)
		sound("GateRightClose").toZone()
	}
	spawnerRandom1 = 0
	spawnerRandom2 = 0
	spawnerRandom3 = 0
}

//------------------------------------------------------------------------------
//FLAG AND CROWD TESTING                                                        
//------------------------------------------------------------------------------
/*def flagStandTest() {
	myManager.schedule(30) { 
		println "^^^^ Changing Flag Stand state from ${flagStand.getActorState()} to 1 ^^^^"
		flagStand.setActorState(1) 
	}
	myManager.schedule(60) { 
		println "^^^^ Changing Flag Stand state from ${flagStand.getActorState()} to 2 ^^^^"
		flagStand.setActorState(2) 
	}
}

flagStandTest()

def flagTestLoop() {
	myManager.schedule(5) { flagTestLoop() }
	if(orangeFlag01.getActorState() == 0 && orangeFlag02.getActorState() == 0 && yellowFlag01.getActorState() == 0) {
		startJump()
		startOrangeFlag01()
		startOrangeFlag02()
		startYellowFlag01()
	} else {
		stopJump()
		stopOrangeFlag01()
		stopOrangeFlag02()
		stopYellowFlag01()
	}
}

def petalTestLoop(event) {
	myManager.schedule(10) { petalTestLoop(event) }
	if(petalSwitchA.getActorState() == 0 && petalSwitchB.getActorState() == 0 && petalSwitchC.getActorState() == 0 && petalSwitchD.getActorState() == 0) {
		startPetalSwitchA()
		startPetalSwitchB()
		startPetalSwitchC()
		startPetalSwitchD()
		waveTest(event)
	} else {
		stopPetalSwitchA()
		stopPetalSwitchB()
		stopPetalSwitchC()
		stopPetalSwitchD()
	}
}

def waveTest(event) {
	waveLeft(event)
	myManager.schedule(2) { waveRight(event) }
}

flagTestLoop()
myManager.onEnter(myRooms.BartonColiseum_1) { event ->
	if(isPlayer(event.actor)) {
		petalTestLoop(event)
	}
}*/