import com.gaiaonline.mmo.battle.script.*;

ML = 3.0
minML = 3.0
MaxML = 4.1

// CHERRY FLUFF SPAWNER
cherry1 = myRooms.KatsumisDoll_1.spawnStoppedSpawner("cherry1", "cherry_fluff", 20) 
cherry1.setPos( 160, 600 )
cherry1.setHateRadiusForChildren( 2000 )
cherry1.setMonsterLevelForChildren( ML )

cherry2 = myRooms.KatsumisDoll_1.spawnStoppedSpawner("cherry2", "cherry_fluff", 20) 
cherry2.setPos( 90, 180 )
cherry2.setHateRadiusForChildren( 2000 )
cherry2.setMonsterLevelForChildren( ML )

cherryList = [ cherry1, cherry2 ]

//===========================================
// DOLL SPAWNERS                             
//===========================================

doll1 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "doll1", "kokeshi_doll", 12 )
doll1.setPos( 250, 570 )
doll1.setHateRadiusForChildren( 2000 )
doll1.setMonsterLevelForChildren( ML )

doll2 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "doll2", "kokeshi_doll", 12 )
doll2.setPos( 510, 600 )
doll2.setHateRadiusForChildren( 2000 )
doll2.setMonsterLevelForChildren( ML )

doll3 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "doll3", "kokeshi_doll", 12 )
doll3.setPos( 810, 620 )
doll3.setHateRadiusForChildren( 2000 )
doll3.setMonsterLevelForChildren( ML )

doll4 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "doll4", "kokeshi_doll", 12 )
doll4.setPos( 970, 280 )
doll4.setHateRadiusForChildren( 2000 )
doll4.setMonsterLevelForChildren( ML )

doll5 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "doll5", "kokeshi_doll", 12 )
doll5.setPos( 950, 140 )
doll5.setHateRadiusForChildren( 2000 )
doll5.setMonsterLevelForChildren( ML )

doll6 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "doll6", "kokeshi_doll", 12 )
doll6.setPos( 610, 110 )
doll6.setHateRadiusForChildren( 2000 )
doll6.setMonsterLevelForChildren( ML )

dollList = [ doll1, doll2, doll3, doll4, doll5, doll6 ]

//===========================================
// LT SPAWNERS                               
//===========================================

LT1 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "LT1", "kokeshi_doll_LT", 6 )
LT1.setPos( 250, 570 )
LT1.setHateRadiusForChildren( 2000 )
LT1.setMonsterLevelForChildren( ML )

LT2 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "LT2", "kokeshi_doll_LT", 6 )
LT2.setPos( 510, 600 )
LT2.setHateRadiusForChildren( 2000 )
LT2.setMonsterLevelForChildren( ML )

LT3 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "LT3", "kokeshi_doll_LT", 6 )
LT3.setPos( 810, 620 )
LT3.setHateRadiusForChildren( 2000 )
LT3.setMonsterLevelForChildren( ML )

LT4 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "LT4", "kokeshi_doll_LT", 6 )
LT4.setPos( 970, 280 )
LT4.setGuardPostForChildren( "KatsumisDoll_1", 820, 370, 180 )
LT4.setHateRadiusForChildren( 2000 )
LT4.setMonsterLevelForChildren( ML )

LT5 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "LT5", "kokeshi_doll_LT", 6 )
LT5.setPos( 950, 140 )
LT5.setHateRadiusForChildren( 2000 )
LT5.setMonsterLevelForChildren( ML )

LT6 = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "LT6", "kokeshi_doll_LT", 6 )
LT6.setPos( 610, 110 )
LT6.setHateRadiusForChildren( 2000 )
LT6.setMonsterLevelForChildren( ML )

LTList = [ LT1, LT2, LT3, LT4, LT5, LT6 ]

//===========================================
// KATSUMI'S KOKESHI SPAWNER                 
//===========================================

katDoll = myRooms.KatsumisDoll_1.spawnStoppedSpawner( "katDoll", "kat_kokeshi_doll", 1 )
katDoll.setPos( 965, 420 )
katDoll.setCFH( 600 )
katDoll.setMonsterLevelForChildren( ML )

//===========================================
// ALLIANCES                                 
//===========================================
katDoll.allyWithSpawner( doll1) 
katDoll.allyWithSpawner( doll2) 
katDoll.allyWithSpawner( doll3) 
katDoll.allyWithSpawner( doll4) 
katDoll.allyWithSpawner( doll5) 
katDoll.allyWithSpawner( doll6) 
katDoll.allyWithSpawner( LT1) 
katDoll.allyWithSpawner( LT2) 
katDoll.allyWithSpawner( LT3) 
katDoll.allyWithSpawner( LT4) 
katDoll.allyWithSpawner( LT5) 
katDoll.allyWithSpawner( LT6) 

//===========================================
// onEnter/Exit LOGIC                        
//===========================================

gameStarted = false
katKokDoll = null
playerSet = [] as Set
katDollHasBeenKilled = false
difficulty = null


myManager.onEnter( myRooms.KatsumisDoll_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet << event.actor
		
		myManager.schedule(3) {
			if( katDollHasBeenKilled == false ) {
				event.actor.centerPrint( "A shimmering one-way wall of force hangs behind you within the arch." )
				myManager.schedule(3) {
					event.actor.centerPrint( "You are blocked from retreat!" )
				}
			}
		}
		//The first player into the scenario sets the difficulty for everyone else
		if( difficulty == null ) {
			difficulty = event.actor.getTeam().getAreaVar( "Z26_Katsumis_Doll", "Z26DollShrineDifficulty" )
			if( difficulty == 0 ) { difficulty = 1 }
		}			
	}
}

myManager.onExit( myRooms.KatsumisDoll_1 ) { event ->
	if( isPlayer( event.actor ) ) {
		playerSet.remove( event.actor )
	}
}

//===========================================
// GAME LOGIC                                
//===========================================

def startGame( event ) {
	//initialize the number of waves to send at the players
	numWaves = 4
	numLaps = 2
	
	playerSet.clone().each { it.centerPrint( "You feel the eddies of power swirling in this meadow." ) }
	myManager.schedule(3) { playerSet.clone().each { it.centerPrint( "There's something stranger than usual happening here..." ) } }
	myManager.schedule(6) { playerSet.clone().each { it.centerPrint( "Did the bushes just rustle?" ) } }
	myManager.schedule(9) { playerSet.clone().each { it.centerPrint( "Watch it!" ) } }
	myManager.schedule(10) { adjustML(); makeGuardian() }
}

def adjustML() {
	ML = minML
	
	//kick players out if they are cheating
	playerSet.clone().each{
		if( it.getConLevel() >= MaxML ) {
			it.centerPrint( "Ah, ah, ah! Your level is too high for this scenario! Out you go!" )
			it.warp( "ZENGARDEN_704", 615, 450 )
		}
	}
	
	//adjust the encounter ML to the group
	random( playerSet ).getCrew().each() {
		if( it.getConLevel() >= ML && it.getConLevel() < MaxML ) { 
			ML = it.getConLevel()
		} else if( it.getConLevel() >= MaxML ) {
			ML = MaxML
		}
	}
	
	//DIFFICULTY
	if( difficulty == 1 ) { ML = ML - 0.6 }
	if( difficulty == 3 ) { ML = ML + 0.6 }
	
	doll1.setMonsterLevelForChildren( ML )
	doll2.setMonsterLevelForChildren( ML )
	doll3.setMonsterLevelForChildren( ML )
	doll4.setMonsterLevelForChildren( ML )
	doll5.setMonsterLevelForChildren( ML )
	doll6.setMonsterLevelForChildren( ML )
	LT1.setMonsterLevelForChildren( ML )
	LT2.setMonsterLevelForChildren( ML )
	LT3.setMonsterLevelForChildren( ML )
	LT4.setMonsterLevelForChildren( ML )
	LT5.setMonsterLevelForChildren( ML )
	LT6.setMonsterLevelForChildren( ML )
	cherry1.setMonsterLevelForChildren( ML )
	cherry2.setMonsterLevelForChildren( ML )
	katDoll.setMonsterLevelForChildren( ML )
}

def checkDazedStatus() {
	allDazed = false
	numDazed = 0
	myRooms.KatsumisDoll_1.getActorList().each { if( isPlayer( it ) && it.isDazed() ) { numDazed ++ } }
	if( numDazed >= playerSet.size() ) {
		allDazed = true
		myRooms.KatsumisDoll_1.getActorList().each {
			if( isPlayer( it ) ) {
				it.centerPrint( "The dolls roll your unconscious bodies through the archway and out of the Shrine." )
				it.warp( "ZENGARDEN_704", 615, 450 )
			}
		}			
	} else {
		myManager.schedule(2) { checkDazedStatus() }
	}
}

def makeGuardian() {
	guardian = LT4.forceSpawnNow()
	runOnDeath( guardian) { event ->
		if( playerSet.size() > 3 ) { avengeMe( event ) }
		playerSet.each{ it.centerPrint( "The air fairly thrums with energy." ) }
		myManager.schedule( 5 ) { nextWave() }
	}
		
	//pause a second so it can begin moving forward to its guardpost
	myManager.schedule(2) { if( !guardian.isDead() ) { guardian.say( "This place is not for Naturals!" ) } }
	myManager.schedule(5) { if( !guardian.isDead() ) { guardian.say( "You may not stay! You cannot leave!" ) } }
	myManager.schedule(8) { if( !guardian.isDead() ) { guardian.say( "You must not BE! Attack me at your peril!" ); guardian.addHate( intruder, 200 ) } }
}

def avengeMe( event ) {
	avenger = random( cherryList ).forceSpawnNow()
	avenger.addHate( event.killer, 50 )
	avenger.say( "I will avenge you!" )
	playerName = event.killer
	avenger.setDisplayName( "Kill ${playerName}!" )
}

def spawnDolls() {
	if( numDolls > 0 ) {
		doll = random( dollList ).forceSpawnNow()
		doll.addHate( random( playerSet ), 50 )
		//on death, spawn a cherry fluff. Chance of a respawn is based on difficulty (Easy = 25%, Normal = 50%, Difficult = 75%)
		runOnDeath( doll, { event -> roll = random(100); if( roll <= difficulty * 25 ) { avengeMe( event ) } } )
		numDolls --
	}
	if( numLTs > 0 ) {
		LT = random( LTList ).forceSpawnNow()
		LT.addHate( random( playerSet ), 50 )
		runOnDeath( LT, { event -> avengeMe( event ) } )
		numLTs --
	}
	if( numDolls <= 0 && numLTs <= 0 ) {
		watchForWaveDeath()
	} else {
		spawnDolls()
	}
}


def watchForWaveDeath() {
	if( myRooms.KatsumisDoll_1.getSpawnTypeCount( "kokeshi_doll" ) + myRooms.KatsumisDoll_1.getSpawnTypeCount( "kokeshi_doll_LT" ) + myRooms.KatsumisDoll_1.getSpawnTypeCount( "cherry_fluff" ) == 0 ) {
		nextWave()
	} else {
		myManager.schedule(1) { watchForWaveDeath() }
	}
}

def nextWave() {
	if( numWaves == 4 ) {
		numDolls = playerSet.size()
		numLTs = ( playerSet.size() / 4 ).intValue()
		numWaves --
		waitTime = 12 - (difficulty * 3)
		myManager.schedule( waitTime ) {
			adjustML()
			spawnDolls()
		}
	} else if( numWaves == 3 ) {
		numDolls = ( playerSet.size() * 1.33 ).intValue()
		numLTs = ( playerSet.size() / 3 ).intValue()
		numWaves --
		waitTime = 14 - (difficulty * 3)
		myManager.schedule( waitTime ) {
			playerSet.clone().each{ it.centerPrint( "The energy from the defeated dolls gathers again from the air." ) }
			adjustML()
			spawnDolls()
		}
	} else if( numWaves == 2 ) { 
		numDolls = ( playerSet.size() * 1.66 ).intValue()
		numLTs = ( playerSet.size() / 2 ).intValue()
		numWaves --
		waitTime = 16 - (difficulty * 3)
		myManager.schedule( waitTime ) {
			playerSet.clone().each{ it.centerPrint( "Your skin prickles with the energy building up around you." ) }
			adjustML()
			spawnDolls()
		}
	} else if( numWaves == 1 ) {
		numDolls = ( playerSet.size() * 2 )
		numLTs = ( playerSet.size() / 1.5 ).intValue()
		numWaves --
		waitTime = 18 - (difficulty * 3)
		myManager.schedule( waitTime ) {
			playerSet.clone().each{ it.centerPrint( "The doll energy buildup feels almost ready to explode!" ) }
			adjustML()
			spawnDolls()
		}
	} else if( numWaves == 0 ) {
		if( numLaps > 0 ) {
			numWaves = 4
			numLaps --
			playerSet.clone().each{ it.centerPrint( "The energy ebbs somewhat, like the tide going out before the next big wave." ) }
			nextWave()
		} else {
			myManager.schedule(5) { playerSet.clone().each{ it.centerPrint( "The garden suddenly goes eerily silent." ) } }
			myManager.schedule(10) { playerSet.clone().each{ it.centerPrint( "The energy felt tremendous before, but suddenly ramps up off the scale!" ) } }
			myManager.schedule(15) { spawnKatKokDoll() }
		}
	}
}

katDollSayings = [ "You cannot win!", "Bow before our might!", "Death for all Naturals!", "Your energy feeds us!" ]

def spawnKatKokDoll() {
	//suitable entrance speech and dramatic delays
	playerSet.clone().each{ it.centerPrint( "The energy all around you rushes inward, forming around a single point." ) }
	
	myManager.schedule(2) {
		//spawn Kat's Doll
		katKokDoll = katDoll.forceSpawnNow()
		myManager.schedule(2) { if( !katKokDoll.isDead() ) { katKokDoll.say( "I live again!!!" ) } }
		myManager.schedule(4) { if( !katKokDoll.isDead() ) { katKokDoll.say( "The March of the Dolls cannot be slowed!" ) } }
		myManager.schedule(6) { if( !katKokDoll.isDead() ) { katKokDoll.say( "We will consume you!" ) } }
		katKokDoll.addHate( random( playerSet ), 50 )

		runOnDeath( katKokDoll, { katsDeathMessage(); katDollHasBeenKilled = true } )

		myManager.onHealth( katKokDoll ) { event ->
			if( event.didTransition( 66 ) && event.isDecrease() ) {
				katKokDoll.say( random( katDollSayings) )
				numBombSpawn = ( playerSet.size() / 3 ).intValue()
				cherrySpawn()
			} else if( event.didTransition( 33 ) && event.isDecrease() ) {
				katKokDoll.say("I will not return to the swirling darkness alone! Not again!!!")
				numBombSpawn = ( playerSet.size() / 2 ).intValue()
				cherrySpawn()
			}
		}
	}
}

nonDazedSet = [] as Set

def cherrySpawn() {
	if( numBombSpawn > 0 ) {
		//check if everyone is dazed or not. If not, then don't spawn any cherry fluffs this time around
		adjustML()
		cherryBomb = random( cherryList ).forceSpawnNow()
		cherryBomb.setDisplayName( "Cherry Bomb" )
		cherryBomb.say( "I'll save you!" )
		//the cherry bomb hates and attack a random player
		nonDazedSet = playerSet.clone()
		playerSet.each{ if( it.isDazed() ) { nonDazedSet.remove( it ) }	}	
		cherryBomb.addHate( random( nonDazedSet ), 50 )
		numBombSpawn --
		myManager.schedule(1) { cherrySpawn() }
	}
}
	
def katsDeathMessage() {
	//a nice centerPrint for everyone at the end of the fight
	playerSet.clone().each{ it.centerPrint( "Nooo! Not the darkness! Not the VOID!!!" ) }
	myManager.schedule(3) { updatesAndRewards() }
}

def updatesAndRewards() {
	if( !playerSet.isEmpty() ) {
		playerSet.clone().each {
			//REWARDS
			if( it.getConLevel() <= MaxML ) {
				//DIFFICULTY: set multipliers. They are set so that the min of one difficulty is higher than the max of the difficulty below it
				if( difficulty == 1 ) { goldMult = 0.75; orbMult = 0.75 }
				else if( difficulty == 2 ) { goldMult = 3; orbMult = 2 }
				else if( difficulty == 3 ) { goldMult = 8; orbMult = 4 }

				//gold reward
				goldGrant = ( random( 10, 25 ) * goldMult ).intValue()
				it.centerPrint( "You receive ${goldGrant} gold!" )
				it.grantCoins( goldGrant )

				//orb reward
				orbGrant = ( random( 3, 5 ) * orbMult ).intValue()
				it.centerPrint( "And you receive ${orbGrant} Charge Orbs!" )
				it.grantQuantityItem( 100257, orbGrant )

				//check to see if anyone in the room at the time of Kat's Doll's death is on the quest and update them if they are
				if( it.isOnQuest( 47, 2 ) ) {
					it.updateQuest( 47, "Katsumi-VQS" )
					it.addMiniMapQuestActorName( "Katsumi-VQS" )
				}
			} else {
				it.centerPrint( "Your level is too high for this scenario. No reward for you!" )
			}
		}
	}
	//after the reward, let players know they can leave now
	myManager.schedule(3) { 
		playerSet.clone().each { it.centerPrint( "With the demise of Katsumi's Kokeshi, the energy fades from the archway." ) }
		myManager.schedule(3) { playerSet.clone().each { it.centerPrint( "You may now leave this meadow. Well done!" ) } }
	}
}
	

//===========================================
// FORCE WALL SWITCH LOGIC                   
//===========================================

onClickExitShrine = new Object()

forceWall = makeSwitch( "ShrineArchway_Out", myRooms.KatsumisDoll_1, 30, 420 )
forceWall.unlock()
forceWall.off()
forceWall.setRange( 300 )

def passForceWall = { event ->
	synchronized( onClickExitShrine ) {
		forceWall.off()
		if( isPlayer( event.actor ) ) {
			if( katDollHasBeenKilled == false ) {
				event.actor.centerPrint( "You are blocked from exiting until the energy wall is lowered somehow." )
			} else {
				event.actor.warp( "ZENGARDEN_704", 615, 450 )
			}
		}
	}
}

forceWall.whenOn( passForceWall )

//============================================
// GAME START TRIGGER (center of room)        
//============================================
enterBlock = new Object()

def stageAreaTrigger = "stageAreaTrigger"
myRooms.KatsumisDoll_1.createTriggerZone( stageAreaTrigger, 280, 80, 350, 650 )

myManager.onTriggerIn(myRooms.KatsumisDoll_1, stageAreaTrigger ) { event ->
	synchronized( enterBlock ) {
		if( isPlayer( event.actor ) && gameStarted == false ) {
			intruder = event.actor
			gameStarted = true
			startGame()
			checkDazedStatus()
		}
	}
}
