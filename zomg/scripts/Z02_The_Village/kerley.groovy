import com.gaiaonline.mmo.battle.script.*;

Kerley = spawnNPC("BFG-Kerley", myRooms.VILLAGE_406, 200, 120)
Kerley.setDisplayName( "Kerley" )

def kerleyPatrol = makeNewPatrol()
Kerley.addPatrolPoint ( "VILLAGE_405", 800, 275, 3)
Kerley.addPatrolPoint ( "VILLAGE_405", 700, 90, 0)
Kerley.addPatrolPoint ( "VILLAGE_305", 520, 265, 0)
Kerley.addPatrolPoint ( "VILLAGE_205", 300, 275, 0)
Kerley.addPatrolPoint ( "VILLAGE_105", 510, 408, 0)
Kerley.addPatrolPoint ( "VILLAGE_105", 920, 315, 0)
Kerley.addPatrolPoint ( "VILLAGE_106", 240, 90, 3)
Kerley.addPatrolPoint ( "VILLAGE_105", 920, 315, 0)
Kerley.addPatrolPoint ( "VILLAGE_105", 510, 408, 0)
Kerley.addPatrolPoint ( "VILLAGE_205", 300, 275, 0)
Kerley.addPatrolPoint ( "VILLAGE_305", 520, 265, 0)
Kerley.addPatrolPoint ( "VILLAGE_405", 700, 90, 0)

Kerley.startPatrol()

/* COMMENTED OUT UNTIL RYAN CAN TAKE A LOOK AT IT
kerleySayings = ["Howdy, Tex", "The end is nigh!"]

greet()

def greet() {
	Kerley.say( random( kerleySayings ) )
	myManager.schedule(5) { greet() }
}
*/


