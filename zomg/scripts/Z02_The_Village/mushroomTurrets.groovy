import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// MUSHROOM TURRETS (North Half of Map)     
//------------------------------------------

Mush1 = myRooms.VILLAGE_403.spawnSpawner( "Mush1", "mushroom_turret", 1 )
Mush1.setPos( 430, 480 )
Mush1.setWaitTime( 50, 70 )
Mush1.setRotationForChildren( 315 )
Mush1.setMonsterLevelForChildren( 1.3 )
Mush1.stopSpawning()

Mush2 = myRooms.VILLAGE_403.spawnSpawner( "Mush2", "mushroom_turret", 1 )
Mush2.setPos( 840, 330 )
Mush2.setWaitTime( 50, 70 )
Mush2.setRotationForChildren( 315 )
Mush2.setMonsterLevelForChildren( 1.3 )
Mush2.stopSpawning()

//Next two are the Guns of Navarone. :)  Guarding the twisty path from south to north.
Mush4 = myRooms.VILLAGE_701.spawnSpawner( "Mush4", "mushroom_turret", 1 )
Mush4.setPos( 495, 50 )
Mush4.setWaitTime( 50, 70 )
Mush4.setRotationForChildren( 180 )
Mush4.setMonsterLevelForChildren( 1.7 )
Mush4.stopSpawning()

Mush5 = myRooms.VILLAGE_701.spawnSpawner( "Mush5", "mushroom_turret", 1 )
Mush5.setPos( 810, 330 )
Mush5.setWaitTime( 50, 70 )
Mush5.setRotationForChildren( 225 )
Mush5.setMonsterLevelForChildren( 1.7 )
Mush5.stopSpawning()


//------------------------------------------
// THE MUSHINOT LINE                        
//------------------------------------------

// Line of Turrets that helps defend the southern regions from incursion from the north

Mushinot1 = myRooms.VILLAGE_702.spawnSpawner( "Mushinot1", "mushroom_turret", 1 )
Mushinot1.setPos( 890, 620 )
Mushinot1.setWaitTime( 50, 70 )
Mushinot1.setRotationForChildren( 45 )
Mushinot1.setMonsterLevelForChildren( 1.6 )
Mushinot1.stopSpawning()

Mushinot2 = myRooms.VILLAGE_703.spawnSpawner( "Mushinot2", "mushroom_turret", 1 )
Mushinot2.setPos( 115, 600 )
Mushinot2.setWaitTime( 50, 70 )
Mushinot2.setRotationForChildren( 315 )
Mushinot2.setMonsterLevelForChildren( 1.5 )
Mushinot2.stopSpawning()

//Next three are overlapping fields of fire
Mushinot3 = myRooms.VILLAGE_803.spawnSpawner( "Mushinot3", "mushroom_turret", 1 )
Mushinot3.setPos( 170, 120 )
Mushinot3.setWaitTime( 50, 70 )
Mushinot3.setRotationForChildren( 135 )
Mushinot3.setMonsterLevelForChildren( 1.6 )
Mushinot3.stopSpawning()

Mushinot4 = myRooms.VILLAGE_803.spawnSpawner( "Mushinot4", "mushroom_turret", 1 )
Mushinot4.setPos( 575, 270 )
Mushinot4.setWaitTime( 50, 70 )
Mushinot4.setRotationForChildren( 135 )
Mushinot4.setMonsterLevelForChildren( 1.6 )
Mushinot4.stopSpawning()

Mushinot5 = myRooms.VILLAGE_803.spawnSpawner( "Mushinot5", "mushroom_turret", 1 )
Mushinot5.setPos( 200, 455 )
Mushinot5.setWaitTime( 50, 70 )
Mushinot5.setRotationForChildren( 45 )
Mushinot5.setMonsterLevelForChildren( 1.6 )
Mushinot5.stopSpawning()

//Next three are overlapping fields of fire in a side room
Mushinot6 = myRooms.VILLAGE_804.spawnSpawner( "Mushinot6", "mushroom_turret", 1 )
Mushinot6.setPos( 220, 400 )
Mushinot6.setWaitTime( 50, 70 )
Mushinot6.setRotationForChildren( 45 )
Mushinot6.setMonsterLevelForChildren( 1.5 )
Mushinot6.stopSpawning()

Mushinot7 = myRooms.VILLAGE_804.spawnSpawner( "Mushinot7", "mushroom_turret", 1 )
Mushinot7.setPos( 500, 540 )
Mushinot7.setWaitTime( 50, 70 )
Mushinot7.setRotationForChildren( 0 )
Mushinot7.setMonsterLevelForChildren( 1.5 )
Mushinot7.stopSpawning()

Mushinot8 = myRooms.VILLAGE_804.spawnSpawner( "Mushinot8", "mushroom_turret", 1 )
Mushinot8.setPos( 800, 315 )
Mushinot8.setWaitTime( 50, 70 )
Mushinot8.setRotationForChildren( 315 )
Mushinot8.setMonsterLevelForChildren( 1.5 )
Mushinot8.stopSpawning()

//NORTH OUTPOST MUSHROOM
Mush3 = myRooms.VILLAGE_503.spawnSpawner( "Mush3", "mushroom_turret", 1 )
Mush3.setPos( 515, 375 )
Mush3.setWaitTime( 1, 2 )
Mush3.setCFH( 0 )
Mush3.setRotationForChildren( 225 )
Mush3.setMonsterLevelForChildren( 1.4 )
Mush3.stopSpawning()

//SOUTH GATE MUSHROOMS
SouthGateTurretOne = myRooms.VILLAGE_1002.spawnSpawner( "SouthGateTurretOne", "mushroom_turret", 1 )
SouthGateTurretOne.setPos( 340, 520 )
SouthGateTurretOne.setWaitTime( 1, 2 )
SouthGateTurretOne.setRotationForChildren( 45 )
SouthGateTurretOne.setMonsterLevelForChildren( 1.9 )
SouthGateTurretOne.stopSpawning()

SouthGateTurretTwo = myRooms.VILLAGE_1002.spawnSpawner( "SouthGateTurretTwo", "mushroom_turret", 1 )
SouthGateTurretTwo.setPos( 805, 440 )
SouthGateTurretTwo.setWaitTime( 1, 2 )
SouthGateTurretTwo.setRotationForChildren( 315 )
SouthGateTurretTwo.setMonsterLevelForChildren( 1.9 )
SouthGateTurretTwo.stopSpawning()

//===============================================
//ALLIANCES                                      
//===============================================
SouthGateTurretOne.allyWithSpawner( SouthGateTurretTwo )

//==========================
//REPOPULATING LOGIC        
//==========================
// All Mushroom Turrets listed above are included in this respawn strategy
// - check if the area is below totalAreaMaxSpawn
// - spawn from a random spawner every 5-15 seconds
// - totalAreaCurrentSpawn = (add all SpawnsInUse together) Don't exceed totalAreaMaxSpawn total spawns within Village Greens
// - if the spawners is aleady at max spawns (spawnsInUse() == maxSpawn ) then spawn at another spawner

totalAreaMaxSpawn = 15
maxSpawn = 1
turretList = [ Mush1, Mush2, Mush3, Mush4, Mush5, Mushinot1, Mushinot2, Mushinot3, Mushinot4, Mushinot5, Mushinot6, Mushinot7, Mushinot8, SouthGateTurretOne, SouthGateTurretTwo ]
mushHateCollector = []
mushTopPercent = 60

//continuously try to repopluate the forest and river area
def repopulate() {
	totalAreaCurrentSpawn = Mush1.spawnsInUse() + Mush2.spawnsInUse() + Mush3.spawnsInUse() + Mush4.spawnsInUse() + Mush5.spawnsInUse() + Mushinot1.spawnsInUse() + Mushinot2.spawnsInUse() + Mushinot3.spawnsInUse() + Mushinot4.spawnsInUse() + Mushinot5.spawnsInUse() + Mushinot6.spawnsInUse() + Mushinot7.spawnsInUse() + Mushinot8.spawnsInUse() + SouthGateTurretOne.spawnsInUse() + SouthGateTurretTwo.spawnsInUse()
	if( totalAreaCurrentSpawn < totalAreaMaxSpawn ) {
		spawner = random( turretList )
		if( spawner.spawnsInUse() < maxSpawn ) {
			newMush = spawner.forceSpawnNow()
			runOnDeath( newMush, { event -> mushHateCollector = []; mushHateCollector.addAll( event.actor.getHated() ); checkForTurretTop() } )
		}
	}
	myManager.schedule( random( 10, 20 ) ) { repopulate() }
}

//upon the death of a pup in the area, see if the killer is on the quest and whether they find a Vox or not
//denialMessage = [ "No head on this mushroom!", "You just can't get a Head!", "Nope. No Head here!", "Hmmm...maybe the next one.", "Rats. The Head isn't here.", "Not this time. Keep trying!" ]

def synchronized checkForTurretTop() {
	mushHateCollector.each{ 
		if( isPlayer( it ) && it.isOnQuest( 87, 2 ) ) {
			roll = random( 100 )
			if( roll <= mushTopPercent ) {
				it.addPlayerVar( "Z02NumberMushroomTopsCollected", 1 )
				it.grantItem( "100380" )
				//if the player has already collected two or more mushroom tops, then complete the mission
				if( it.getPlayerVar( "Z02NumberMushroomTopsCollected" ) >= 6 ) {
					//it.centerPrint( "You have all the Mushroom Heads that Remo needs!" )
					it.deletePlayerVar( "Z02NumberMushroomTopsCollected" ) //clean up this playerVar so it's not in the player record forever.
				}					
			}
		}
	}
}

repopulate()
