
//--------------------------------------------
// WARP CRYSTAL TO THE NULL CHAMBER           
//--------------------------------------------

toNullChamber = makeSwitch( "crystalVillage206", myRooms.VILLAGE_206, 375, 250 )
toNullChamber.unlock()
toNullChamber.setRange( 200 )
toNullChamber.setMouseoverText("Null Crystal: Click this to enter the Null Chamber")

onClickCrystal = new Object()

def goToNullChamber = { event ->
	synchronized( onClickCrystal ) {
		toNullChamber.off()
		if( event.actor.isOnQuest( 74, 2 ) ) {
			event.actor.updateQuest( 74, "BFG-Elizabeth" ) //push the completion of step 2 of the "Charge Up" quest (Q74)
			event.actor.removeMiniMapQuestLocation( "Z02CrystalFlag" )
			event.actor.setQuestFlag( GLOBAL, "Z1NullChamberCrystalTouched" )
		}

		event.actor.centerPrint( "A powerful force moves you through space..." )
		event.actor.warp( "nullChamber1_1", 690, 675, 6 )
		event.actor.setQuestFlag( GLOBAL, "Z21VillageWarpOkay" )
		event.actor.setQuestFlag( GLOBAL, "Z21ComingFromOutside" )
	}
}

toNullChamber.whenOn( goToNullChamber )

/*
//The "useTheHyperNet" tutorial dialog
myManager.onEnter( myRooms.VILLAGE_206 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02UsingHyperNet" )
	}
	if( isPlayer( event.actor ) && event.actor.isOnQuest( 74, 2 ) && !event.actor.hasQuestFlag( GLOBAL, "Z02HyperNetTutorialDialog" ) && !event.actor.hasQuestFlag( GLOBAL, "Z14HyperNetAccessed" ) ) {
		event.actor.setQuestFlag( GLOBAL, "Z02HyperNetTutorialDialog" )
		myManager.schedule(1) { tutorialNPC.pushDialog( event.actor, "useTheHyperNet" ) }
	}
}


useTheHyperNet = tutorialNPC.createConversation( "useTheHyperNet", true )

def use1 = [id:1]
use1.npctext = "<zOMG dialogWidth='325'><![CDATA[<h1><b><font face='Arial' size='14'>Click on the HyperNet!</font></b></h1><br><font face='Arial' size ='12'>Want to earn *easy* gold and charge orbs? Then walk over to the nearby robot and click on it!<br><br>This is the 'HyperNet' terminal and it has lots of information about how to play the game.<br><br>Everytime you click on a new topic, you'll get gold *and* charge orbs (which you use to power your rings up).<br><br>So go check it out and remember you can come back to it anytime!<br><br>]]></zOMG>"
use1.result = DONE
useTheHyperNet.addDialog( use1, tutorialNPC )
*/
