//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

def gnomansUpdater = "gnomansUpdater"
myRooms.VILLAGE_1002.createTriggerZone(gnomansUpdater, 400, 560, 685, 645)
myManager.onTriggerIn(myRooms.VILLAGE_1002, gnomansUpdater) { event ->
	if( isPlayer(event.actor) && !event.actor.isDoneQuest(222)) {
		event.actor.updateQuest(222, "Story-VQS")
	}
}