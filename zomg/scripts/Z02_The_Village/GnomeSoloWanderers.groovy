import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// MISCELLANEOUS LAWN GNOME SOLO SPAWNERS   
//------------------------------------------

def soloLawnGnomeOne = myRooms.VILLAGE_803.spawnSpawner( "soloLawnGnomeOne", "lawn_gnome", 2 )
soloLawnGnomeOne.setPos( 588, 384 )
soloLawnGnomeOne.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
soloLawnGnomeOne.setCFH( 0 )
soloLawnGnomeOne.setWaitTime( 30, 60 )
soloLawnGnomeOne.setMonsterLevelForChildren( 1.6 )

def soloLawnGnomeTwo = myRooms.VILLAGE_803.spawnSpawner( "soloLawnGnomeTwo", "pink_flamingo", 2 )
soloLawnGnomeTwo.setPos( 762, 278 )
soloLawnGnomeTwo.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
soloLawnGnomeTwo.setCFH( 0 )
soloLawnGnomeTwo.setWaitTime( 30, 60 )
soloLawnGnomeTwo.setMonsterLevelForChildren( 1.6 )

def soloLawnGnomeThree = myRooms.VILLAGE_904.spawnSpawner( "soloLawnGnomeThree", "lawn_gnome", 3 )
soloLawnGnomeThree.setPos( 584, 376 )
soloLawnGnomeThree.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
soloLawnGnomeThree.setCFH( 0 )
soloLawnGnomeThree.setWaitTime( 30, 60 )
soloLawnGnomeThree.setMonsterLevelForChildren( 1.5 )

def soloLawnGnomeFive = myRooms.VILLAGE_804.spawnSpawner( "soloLawnGnomeFive", "lawn_gnome", 2 )
soloLawnGnomeFive.setPos( 208, 190 )
soloLawnGnomeFive.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
soloLawnGnomeFive.setCFH( 0 )
soloLawnGnomeFive.setWaitTime( 30, 60 )
soloLawnGnomeFive.setMonsterLevelForChildren( 1.5 )

def soloLawnGnomeTen = myRooms.VILLAGE_602.spawnSpawner( "soloLawnGnomeTen", "lawn_gnome", 3 )
soloLawnGnomeTen.setPos( 560, 400 )
soloLawnGnomeTen.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
soloLawnGnomeTen.setCFH( 0 )
soloLawnGnomeTen.setWaitTime( 30, 60 )
soloLawnGnomeTen.setMonsterLevelForChildren( 1.6 )

def soloLawnGnomeEleven = myRooms.VILLAGE_702.spawnSpawner( "soloLawnGnomeEleven", "lawn_gnome", 3 )
soloLawnGnomeEleven.setPos( 565, 120 )
soloLawnGnomeEleven.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
soloLawnGnomeEleven.setCFH( 0 )
soloLawnGnomeEleven.setWaitTime( 30, 60 )
soloLawnGnomeEleven.setMonsterLevelForChildren( 1.6 )

//------------------------------------------
// STAIR GUARDS                             
//------------------------------------------

stairGuardOne = myRooms.VILLAGE_603.spawnSpawner( "stairGuardOne", "lawn_gnome", 1 )
stairGuardOne.setPos( 820, 200 )
stairGuardOne.setWaitTime( 1, 2 )
stairGuardOne.setGuardPostForChildren( "VILLAGE_603", 310, 470, 0 )
stairGuardOne.setCFH( 0 )
stairGuardOne.setMonsterLevelForChildren( 1.5 )

stairGuardTwo = myRooms.VILLAGE_603.spawnSpawner( "stairGuardTwo", "lawn_gnome", 1 )
stairGuardTwo.setPos( 820, 200 )
stairGuardTwo.setWaitTime( 1, 2 )
stairGuardTwo.setGuardPostForChildren( "VILLAGE_603", 620, 440, 0 )
stairGuardTwo.setCFH( 0 )
stairGuardTwo.setMonsterLevelForChildren( 1.5 )

stairGuardThree = myRooms.VILLAGE_603.spawnSpawner( "stairGuardThree", "lawn_gnome", 1 )
stairGuardThree.setPos( 820, 200 )
stairGuardThree.setWaitTime( 1, 2 )
stairGuardThree.setGuardPostForChildren( "VILLAGE_603", 190, 510, 0 )
stairGuardThree.setCFH( 0 )
stairGuardThree.setMonsterLevelForChildren( 1.5 )

stairGuardFour = myRooms.VILLAGE_603.spawnSpawner( "stairGuardFour", "lawn_gnome", 1 )
stairGuardFour.setPos( 820, 200 )
stairGuardFour.setWaitTime( 1, 2 )
stairGuardFour.setGuardPostForChildren( "VILLAGE_603", 700, 410, 0 )
stairGuardFour.setCFH( 0 )
stairGuardFour.setMonsterLevelForChildren( 1.5 )

def respawnStairGuards() {
//	println "**** Checking stair guard death statuses ****"
	if( stairGuard1.isDead() && stairGuard2.isDead() && stairGuard3.isDead() && stairGuard4.isDead() ) {
		myManager.schedule( random( 50, 70 ) ) { 
//			println "**** respawning the stair gnomes in 50-70 seconds ****"
			stairGuard1 = stairGuardOne.forceSpawnNow()
			myManager.schedule(1) { stairGuard2 = stairGuardTwo.forceSpawnNow() }
			myManager.schedule(2) { stairGuard3 = stairGuardThree.forceSpawnNow() }
			myManager.schedule(3) { stairGuard4 = stairGuardFour.forceSpawnNow() }
			myManager.schedule( 15 ) { respawnStairGuards() }
		}
	} else {
		myManager.schedule( 15 ) { respawnStairGuards() }
	}
}

//------------------------------------------
// PINK FLAMINGO SPIRAL SPAWNER             
//------------------------------------------

flamingoGuard1 = myRooms.VILLAGE_604.spawnSpawner( "flamingoGuard1", "lawn_gnome", 1 )
flamingoGuard1.setPos( 970, 370 )
flamingoGuard1.setWaitTime( 1, 2 )
flamingoGuard1.setGuardPostForChildren( "VILLAGE_604", 675, 255, 245 )
flamingoGuard1.setMonsterLevelForChildren( 1.4 )

flamingoGuard2 = myRooms.VILLAGE_604.spawnSpawner( "flamingoGuard2", "lawn_gnome", 1 )
flamingoGuard2.setPos( 970, 370 )
flamingoGuard2.setWaitTime( 1, 2 )
flamingoGuard2.setGuardPostForChildren( "VILLAGE_604", 525, 255, 135 )
flamingoGuard2.setMonsterLevelForChildren( 1.4 )

def respawnFlamingoGuards() {
	if( spiralGuard1.isDead() && spiralGuard2.isDead() ) {
		myManager.schedule( random( 50, 70 ) ) { 
			spiralGuard1 = flamingoGuard1.forceSpawnNow();
			myManager.schedule(1) { spiralGuard2 = flamingoGuard2.forceSpawnNow();}
			myManager.schedule( 15 ) { respawnFlamingoGuards() }
		}
	} else {
		myManager.schedule( 15 ) { respawnFlamingoGuards() }
	}
}
	
def spiralFlamingoSpawner = myRooms.VILLAGE_604.spawnSpawner( "spiralFlamingoSpawner", "pink_flamingo", 4 )
spiralFlamingoSpawner.setPos( 360, 475 )
spiralFlamingoSpawner.setWanderBehaviorForChildren( 50, 150, 3, 7, 300)
spiralFlamingoSpawner.setWaitTime( 50, 70 )
spiralFlamingoSpawner.setCFH( 0 )
spiralFlamingoSpawner.setMonsterLevelForChildren( 1.4 )

//INITIAL SERVER SPAWNS
stairGuard1 = stairGuardOne.forceSpawnNow()
stairGuard2 = stairGuardTwo.forceSpawnNow()
stairGuard3 = stairGuardThree.forceSpawnNow()
stairGuard4 = stairGuardFour.forceSpawnNow()
stairGuardOne.stopSpawning()
stairGuardTwo.stopSpawning()
stairGuardThree.stopSpawning()
stairGuardFour.stopSpawning()

respawnStairGuards()

spiralFlamingoSpawner.spawnAllNow()

soloLawnGnomeOne.spawnAllNow()
soloLawnGnomeTwo.spawnAllNow()
soloLawnGnomeThree.spawnAllNow()
soloLawnGnomeFive.spawnAllNow()
soloLawnGnomeTen.spawnAllNow()
soloLawnGnomeEleven.spawnAllNow()

spiralGuard1 = flamingoGuard1.forceSpawnNow()
spiralGuard2 = flamingoGuard2.forceSpawnNow()
flamingoGuard1.stopSpawning()
flamingoGuard2.stopSpawning()

respawnFlamingoGuards()

//CANDICE's PLASTIC BOUQUET QUEST SPAWNERS
upperGnomeOne = myRooms.VILLAGE_504.spawnSpawner( "upperGnomeOne", "lawn_gnome", 1 )
upperGnomeOne.setPos( 490, 315 )
upperGnomeOne.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeOne.setCFH( 0 )
upperGnomeOne.setWaitTime( 30, 60 )
upperGnomeOne.setMonsterLevelForChildren( 1.3 )

upperGnomeOneB = myRooms.VILLAGE_504.spawnSpawner( "upperGnomeOneB", "lawn_gnome", 1 )
upperGnomeOneB.setPos( 490, 315 )
upperGnomeOneB.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeOneB.setCFH( 0 )
upperGnomeOneB.setWaitTime( 30, 60 )
upperGnomeOneB.setMonsterLevelForChildren( 1.3 )

upperGnomeOneC = myRooms.VILLAGE_504.spawnSpawner( "upperGnomeOneC", "lawn_gnome", 1 )
upperGnomeOneC.setPos( 490, 315 )
upperGnomeOneC.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeOneC.setCFH( 0 )
upperGnomeOneC.setWaitTime( 30, 60 )
upperGnomeOneC.setMonsterLevelForChildren( 1.3 )

upperGnomeTwo = myRooms.VILLAGE_404.spawnSpawner( "upperGnomeTwo", "lawn_gnome", 1 )
upperGnomeTwo.setPos( 677, 284 )
upperGnomeTwo.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeTwo.setCFH( 0 )
upperGnomeTwo.setWaitTime( 30, 60 )
upperGnomeTwo.setMonsterLevelForChildren( 1.2 )

upperGnomeTwoB = myRooms.VILLAGE_404.spawnSpawner( "upperGnomeTwoB", "lawn_gnome", 1 )
upperGnomeTwoB.setPos( 677, 284 )
upperGnomeTwoB.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeTwoB.setCFH( 0 )
upperGnomeTwoB.setWaitTime( 30, 60 )
upperGnomeTwoB.setMonsterLevelForChildren( 1.2 )

upperGnomeTwoC = myRooms.VILLAGE_404.spawnSpawner( "upperGnomeTwoC", "lawn_gnome", 1 )
upperGnomeTwoC.setPos( 677, 284 )
upperGnomeTwoC.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeTwoC.setCFH( 0 )
upperGnomeTwoC.setWaitTime( 30, 60 )
upperGnomeTwoC.setMonsterLevelForChildren( 1.2 )

upperGnomeThree = myRooms.VILLAGE_304.spawnSpawner( "upperGnomeThree", "lawn_gnome", 1 )
upperGnomeThree.setPos( 470, 130 )
upperGnomeThree.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeThree.setCFH( 0 )
upperGnomeThree.setWaitTime( 30, 60 )
upperGnomeThree.setMonsterLevelForChildren( 1.1 )

upperGnomeThreeB = myRooms.VILLAGE_304.spawnSpawner( "upperGnomeThreeB", "lawn_gnome", 1 )
upperGnomeThreeB.setPos( 750, 490 )
upperGnomeThreeB.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeThreeB.setCFH( 0 )
upperGnomeThreeB.setWaitTime( 30, 60 )
upperGnomeThreeB.setMonsterLevelForChildren( 1.1 )

upperGnomeThreeC = myRooms.VILLAGE_304.spawnSpawner( "upperGnomeThreeC", "lawn_gnome", 1 )
upperGnomeThreeC.setPos( 115, 590 )
upperGnomeThreeC.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeThreeC.setCFH( 0 )
upperGnomeThreeC.setWaitTime( 30, 60 )
upperGnomeThreeC.setMonsterLevelForChildren( 1.1 )

upperGnomeFour = myRooms.VILLAGE_204.spawnSpawner( "upperGnomeFour", "lawn_gnome", 1 )
upperGnomeFour.setPos( 526, 284 )
upperGnomeFour.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeFour.setCFH( 0 )
upperGnomeFour.setWaitTime( 30, 60 )
upperGnomeFour.setMonsterLevelForChildren( 1.0 )

upperGnomeFourB = myRooms.VILLAGE_204.spawnSpawner( "upperGnomeFourB", "lawn_gnome", 1 )
upperGnomeFourB.setPos( 526, 284 )
upperGnomeFourB.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperGnomeFourB.setCFH( 0 )
upperGnomeFourB.setWaitTime( 30, 60 )
upperGnomeFourB.setMonsterLevelForChildren( 1.0 )

upperGnomeFive = myRooms.VILLAGE_405.spawnSpawner("upperGnomeFive", "lawn_gnome", 1)
upperGnomeFive.setPos(110, 160)
upperGnomeFive.setWanderBehaviorForChildren(100, 300, 3, 7, 500)
upperGnomeFive.setCFH(0)
upperGnomeFive.setWaitTime(30, 60)
upperGnomeFive.setMonsterLevelForChildren(1.0)

upperGnomeSix = myRooms.VILLAGE_305.spawnSpawner("upperGnomeSix", "lawn_gnome", 1)
upperGnomeSix.setPos(100, 490)
upperGnomeSix.setWanderBehaviorForChildren(100, 200, 3, 7, 300)
upperGnomeSix.setCFH(0)
upperGnomeSix.setWaitTime(30, 60)
upperGnomeSix.setMonsterLevelForChildren(1.0)

upperGnomeSeven = myRooms.VILLAGE_306.spawnSpawner("upperGnomeSeven", "lawn_gnome", 1)
upperGnomeSeven.setPos(170, 100)
upperGnomeSeven.setWanderBehaviorForChildren(50, 150, 3, 7, 500)
upperGnomeSeven.setCFH(0)
upperGnomeSeven.setWaitTime(30, 60)
upperGnomeSeven.setMonsterLevelForChildren(1.0)

upperGnomeSevenB = myRooms.VILLAGE_306.spawnSpawner("upperGnomeSevenB", "lawn_gnome", 1)
upperGnomeSevenB.setPos(390, 130)
upperGnomeSevenB.setWanderBehaviorForChildren(50, 150, 3, 7, 500)
upperGnomeSevenB.setCFH(0)
upperGnomeSevenB.setWaitTime(30, 60)
upperGnomeSevenB.setMonsterLevelForChildren(1.0)

upperGnomeOne.stopSpawning()
upperGnomeOneB.stopSpawning()
upperGnomeOneC.stopSpawning()
upperGnomeTwo.stopSpawning()
upperGnomeTwoB.stopSpawning()
upperGnomeTwoC.stopSpawning()
upperGnomeThree.stopSpawning()
upperGnomeThreeB.stopSpawning()
upperGnomeThreeC.stopSpawning()
upperGnomeFour.stopSpawning()
upperGnomeFourB.stopSpawning()
upperGnomeFive.stopSpawning()
upperGnomeSix.stopSpawning()
upperGnomeSeven.stopSpawning()
upperGnomeSevenB.stopSpawning()

//==========================
//REPOPULATING LOGIC - GNOMES
//==========================

onQuestStep( 35, 2 ) { event -> event.player.addMiniMapQuestActorName( "Rina-VQS" ) }

totalAreaMaxSpawn = 15
maxSpawn = 1
gnomeList = [ upperGnomeOne, upperGnomeOneB, upperGnomeOneC, upperGnomeTwo, upperGnomeTwoB, upperGnomeTwoC, upperGnomeThree, upperGnomeThreeB, upperGnomeThreeC, upperGnomeFour, upperGnomeFourB, upperGnomeFive, upperGnomeSix, upperGnomeSeven, upperGnomeSevenB ]
gnomeSpawnMap = new HashMap()
gnomeHateCollector = []
leafPercent = 60

//continuously try to repopluate the area near the road
def repopulateGnomes() {
	totalAreaCurrentSpawn = upperGnomeOne.spawnsInUse() + upperGnomeOneB.spawnsInUse() + upperGnomeOneC.spawnsInUse() + upperGnomeTwo.spawnsInUse() + upperGnomeTwoB.spawnsInUse() + upperGnomeTwoC.spawnsInUse() + upperGnomeThree.spawnsInUse() + upperGnomeThreeB.spawnsInUse() + upperGnomeThreeC.spawnsInUse() + upperGnomeFour.spawnsInUse() + upperGnomeFourB.spawnsInUse() + upperGnomeFive.spawnsInUse() + upperGnomeSix.spawnsInUse() + upperGnomeSeven.spawnsInUse() + upperGnomeSevenB.spawnsInUse()
	if( totalAreaCurrentSpawn < totalAreaMaxSpawn ) {
		gnomeSpawner = random( gnomeList )
		if( gnomeSpawner.spawnsInUse() < maxSpawn ) {
			newGnome = gnomeSpawner.forceSpawnNow()
			gnomeSpawnMap.put(newGnome, gnomeSpawner)
			gnomeList.remove(gnomeSpawner)
			
			runOnDeath( newGnome, { event -> gnomeHateCollector = []; gnomeHateCollector.addAll( event.actor.getHated() ); checkForGnomeLoot( ); gnomeList << gnomeSpawnMap.get(event.actor); gnomeSpawnMap.remove(event.actor) } )
		}
	}
	myManager.schedule( random( 5, 10 ) ) { repopulateGnomes() }
}

//upon the death of a gnome, see if it drops loot for a player on the quest
//leafDenialMessage = [ "Hmmm...no leaves on this one!", "Nothing in its pockets this time!", "Nope. No leaf here!", "Nada. Zilch. Zero. No leaves.", "Maybe next time!", "Perhaps if you make like a tree...", "No leaf here!" ]

def synchronized checkForGnomeLoot( ) {
	gnomeHateCollector.clone().each{ 
		if( isPlayer( it ) && it.isOnQuest( 35, 2 ) ) { //if the player is on the RINA'S LEAVES
			roll = random( 100 )
			if( roll <= leafPercent ) {
				it.addPlayerVar( "Z02WingTreeLeavesCollected", 1 )
				it.grantItem( "100280" )
				//if the player has already collected ten or more leaves, then complete the mission
				if( it.getPlayerVar( "Z02WingTreeLeavesCollected" ) >= 10 ) {
					//it.centerPrint( "You have all the Wing Tree Leaves that Rina needs!" )
					it.deletePlayerVar( "Z02WingTreeLeavesCollected" ) //clean up this playerVar so it's not in the player record forever.
				}				
			}
		}
	}
}

repopulateGnomes()

	
//RINA's PINK FLAMINGO COLLECTION QUEST SPAWNERS

upperFlamingoOne = myRooms.VILLAGE_4.spawnSpawner( "upperFlamingoOne", "pink_flamingo", 1 )
upperFlamingoOne.setPos( 540, 505 )
upperFlamingoOne.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoOne.setCFH( 0 )
upperFlamingoOne.setWaitTime( 30, 60 )
upperFlamingoOne.setMonsterLevelForChildren( 1.0 )

upperFlamingoOneB = myRooms.VILLAGE_4.spawnSpawner( "upperFlamingoOneB", "pink_flamingo", 1 )
upperFlamingoOneB.setPos( 540, 505 )
upperFlamingoOneB.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoOneB.setCFH( 0 )
upperFlamingoOneB.setWaitTime( 30, 60 )
upperFlamingoOneB.setMonsterLevelForChildren( 1.0 )

upperFlamingoTwo = myRooms.VILLAGE_5.spawnSpawner( "upperFlamingoTwo", "pink_flamingo", 1 )
upperFlamingoTwo.setPos( 310, 420 )
upperFlamingoTwo.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoTwo.setCFH( 0 )
upperFlamingoTwo.setWaitTime( 30, 60 )
upperFlamingoTwo.setMonsterLevelForChildren( 1.0 )

upperFlamingoTwoB = myRooms.VILLAGE_5.spawnSpawner( "upperFlamingoTwoB", "pink_flamingo", 1 )
upperFlamingoTwoB.setPos( 310, 420 )
upperFlamingoTwoB.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoTwoB.setCFH( 0 )
upperFlamingoTwoB.setWaitTime( 30, 60 )
upperFlamingoTwoB.setMonsterLevelForChildren( 1.0 )

upperFlamingoThree = myRooms.VILLAGE_104.spawnSpawner( "upperFlamingoThree", "pink_flamingo", 1 )
upperFlamingoThree.setPos( 793, 193 )
upperFlamingoThree.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoThree.setCFH( 0 )
upperFlamingoThree.setWaitTime( 30, 60 )
upperFlamingoThree.setMonsterLevelForChildren( 1.0 )

upperFlamingoThreeB = myRooms.VILLAGE_104.spawnSpawner( "upperFlamingoThreeB", "pink_flamingo", 1 )
upperFlamingoThreeB.setPos( 793, 193 )
upperFlamingoThreeB.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoThreeB.setCFH( 0 )
upperFlamingoThreeB.setWaitTime( 30, 60 )
upperFlamingoThreeB.setMonsterLevelForChildren( 1.0 )

upperFlamingoThreeC = myRooms.VILLAGE_104.spawnSpawner( "upperFlamingoThreeC", "pink_flamingo", 1 )
upperFlamingoThreeC.setPos( 793, 193 )
upperFlamingoThreeC.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoThreeC.setCFH( 0 )
upperFlamingoThreeC.setWaitTime( 30, 60 )
upperFlamingoThreeC.setMonsterLevelForChildren( 1.0 )

upperFlamingoFour = myRooms.VILLAGE_306.spawnSpawner( "upperFlamingoFour", "pink_flamingo", 2 )
upperFlamingoFour.setPos( 330, 315 )
upperFlamingoFour.setWanderBehaviorForChildren( 50, 150, 3, 7, 200)
upperFlamingoFour.setCFH( 0 )
upperFlamingoFour.setWaitTime( 30, 60 )
upperFlamingoFour.setMonsterLevelForChildren( 1.0 )

upperFlamingoFourB = myRooms.VILLAGE_306.spawnSpawner( "upperFlamingoFourB", "pink_flamingo", 2 )
upperFlamingoFourB.setPos( 70, 315 )
upperFlamingoFourB.setWanderBehaviorForChildren( 50, 150, 3, 7, 200)
upperFlamingoFourB.setCFH( 0 )
upperFlamingoFourB.setWaitTime( 30, 60 )
upperFlamingoFourB.setMonsterLevelForChildren( 1.0 )

upperFlamingoFive = myRooms.VILLAGE_305.spawnSpawner( "upperFlamingoFive", "pink_flamingo", 1 )
upperFlamingoFive.setPos( 150, 280 )
upperFlamingoFive.setWanderBehaviorForChildren( 100, 200, 3, 7, 300)
upperFlamingoFive.setCFH( 0 )
upperFlamingoFive.setWaitTime( 30, 60 )
upperFlamingoFive.setMonsterLevelForChildren( 1.0 )

upperFlamingoSix = myRooms.VILLAGE_404.spawnSpawner( "upperFlamingoSix", "pink_flamingo", 1 )
upperFlamingoSix.setPos( 205, 260 )
upperFlamingoSix.setWanderBehaviorForChildren( 100, 300, 3, 7, 500)
upperFlamingoSix.setCFH( 0 )
upperFlamingoSix.setWaitTime( 30, 60 )
upperFlamingoSix.setMonsterLevelForChildren( 1.1 )

upperFlamingoSeven = myRooms.VILLAGE_405.spawnSpawner("upperFlamingoSeven", "pink_flamingo", 1)
upperFlamingoSeven.setPos(420, 350)
upperFlamingoSeven.setWanderBehaviorForChildren(100, 300, 3, 7, 500)
upperFlamingoSeven.setCFH(0)
upperFlamingoSeven.setWaitTime(30, 60)
upperFlamingoSeven.setMonsterLevelForChildren(1.0)

upperFlamingoSevenB = myRooms.VILLAGE_405.spawnSpawner("upperFlamingoSevenB", "pink_flamingo", 1)
upperFlamingoSevenB.setPos(130, 70)
upperFlamingoSevenB.setWanderBehaviorForChildren(100, 300, 3, 7, 500)
upperFlamingoSevenB.setCFH(0)
upperFlamingoSevenB.setWaitTime(30, 60)
upperFlamingoSevenB.setMonsterLevelForChildren(1.0)

upperFlamingoEight = myRooms.VILLAGE_304.spawnSpawner("upperFlamingoEight", "pink_flamingo", 1)
upperFlamingoEight.setPos(150, 150)
upperFlamingoEight.setWanderBehaviorForChildren(100, 300, 3, 7, 500)
upperFlamingoEight.setCFH(0)
upperFlamingoEight.setWaitTime(30, 60)
upperFlamingoEight.setMonsterLevelForChildren(1.1)

upperFlamingoNine = myRooms.VILLAGE_204.spawnSpawner("upperFlamingoNine", "pink_flamingo", 1)
upperFlamingoNine.setPos(690, 490)
upperFlamingoNine.setWanderBehaviorForChildren(100, 300, 3, 7, 500)
upperFlamingoNine.setCFH(0)
upperFlamingoNine.setWaitTime(30, 60)
upperFlamingoNine.setMonsterLevelForChildren(1.1)

upperFlamingoOne.stopSpawning()
upperFlamingoOneB.stopSpawning()
upperFlamingoTwo.stopSpawning()
upperFlamingoTwoB.stopSpawning()
upperFlamingoThree.stopSpawning()
upperFlamingoThreeB.stopSpawning()
upperFlamingoThreeC.stopSpawning()
upperFlamingoFour.stopSpawning()
upperFlamingoFourB.stopSpawning()
upperFlamingoFive.stopSpawning()
upperFlamingoSix.stopSpawning()
upperFlamingoSeven.stopSpawning()
upperFlamingoSevenB.stopSpawning()
upperFlamingoEight.stopSpawning()
upperFlamingoNine.stopSpawning()

//==========================
//REPOPULATING LOGIC - FLAMINGOS
//==========================

onQuestStep( 43, 2 ) { event -> event.player.addMiniMapQuestActorName( "Candice-VQS" ) }
onQuestStep( 45, 2 ) { event -> event.player.addMiniMapQuestActorName( "Rina-VQS" ) }

totalAreaMaxSpawn = 15
maxSpawn = 1
flamingoList = [ upperFlamingoOne, upperFlamingoOneB, upperFlamingoTwo, upperFlamingoTwoB, upperFlamingoThree, upperFlamingoThreeB, upperFlamingoThreeC, upperFlamingoFour, upperFlamingoFourB, upperFlamingoFive, upperFlamingoSix, upperFlamingoSeven, upperFlamingoSevenB, upperFlamingoEight, upperFlamingoNine ]
flamingoSpawnMap = new HashMap()
flamingoHateCollector = [] as Set
featherPercent = 60
plasticPercent = 60

//continuously try to repopluate the area near the road
def repopulateFlamingos() {
	totalAreaCurrentSpawn = upperFlamingoOne.spawnsInUse() + upperFlamingoOneB.spawnsInUse() + upperFlamingoTwo.spawnsInUse() + upperFlamingoTwoB.spawnsInUse() + upperFlamingoThree.spawnsInUse() + upperFlamingoThreeB.spawnsInUse() + upperFlamingoThreeC.spawnsInUse() + upperFlamingoFour.spawnsInUse() + upperFlamingoFourB.spawnsInUse() + upperFlamingoFive.spawnsInUse() + upperFlamingoSix.spawnsInUse() + upperFlamingoSeven.spawnsInUse() + upperFlamingoSevenB.spawnsInUse() + upperFlamingoEight.spawnsInUse() + upperFlamingoNine.spawnsInUse()
	if( totalAreaCurrentSpawn < totalAreaMaxSpawn ) {
		flamingoSpawner = random( flamingoList )
		if( flamingoSpawner.spawnsInUse() < maxSpawn ) {
			newFlamingo = flamingoSpawner.forceSpawnNow()
			flamingoSpawnMap.put(newFlamingo, flamingoSpawner)
			flamingoList.remove(flamingoSpawner)
			
			runOnDeath( newFlamingo, { event -> flamingoHateCollector.clear(); flamingoHateCollector.addAll( event.actor.getHated() ); checkForFlamingoLoot( event ); flamingoList << flamingoSpawnMap.get(event.actor); flamingoSpawnMap.remove(event.actor) } )
		}
	}
	myManager.schedule( random( 5, 10 ) ) { repopulateFlamingos() }
}

//upon the death of a flamingo, see if it drops loot for a player on the quest
//featherDenialMessage = [ "This flamingo's not ripe for the plucking.", "No feathers here!", "All the feathers are broken on this one.", "Hmmm...maybe the next one.", "Drat. Still can't find a feather!", "Not this time. Keep trying!", "How can there be a flamingo with no feathers? But none here!" ]

//plasticDenialMessage = [ "No plastic on this one!", "No plastic? On a plastic creature? Dang!", "Not even a little piece left!", "No plastic here!", "Maybe next time.", "Where's the plastic?", "Oh well. No plastic this time!" ]

def synchronized checkForFlamingoLoot( event ) {
	flamingoHateCollector.each{ 
		if( isPlayer( it ) && it.isOnQuest( 45, 2 ) ) { //if the player is on the FEATHER QUEST
			roll = random( 100 )
			if( roll <= featherPercent ) {
				it.addPlayerVar( "Z02PinkFlamingoFeathersCollected", 1 )
				it.grantItem( "100270" )
				//if the player has already collected ten or more feathers, then complete the mission
				if( it.getPlayerVar( "Z02PinkFlamingoFeathersCollected" ) >= 10 ) {
					//it.centerPrint( "You have all the Flamingo Feathers that Rina needs!" )
					it.deletePlayerVar( "Z02PinkFlamingoFeathersCollected" ) //clean up this playerVar so it's not in the player record forever.
				}				
			}
		}
		if( isPlayer( it ) && it.isOnQuest( 43, 2 ) ) { //if the player is on the PLASTIC BOUQUET quest
			roll = random( 100 )
			if( roll <= plasticPercent ) {
				it.addPlayerVar( "Z02PlasticCollected", 1 )
				it.grantItem( "100385" )
				if( it.getPlayerVar( "Z02PlasticCollected" ) >= 10 ) {
					//it.centerPrint( "You have all the Plastic Pieces that Candice needs!" )
					it.deletePlayerVar( "Z02PlasticCollected" )
				}
			}
		}
	}
}

repopulateFlamingos()
