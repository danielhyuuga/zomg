import com.gaiaonline.mmo.battle.script.*;

Leon = spawnNPC("Leon-VQS", myRooms.VILLAGE_406, 800, 200)
Leon.setRotation( 45 )
Leon.setDisplayName( "Commander Leon" )

onQuestStep( 78, 4 ) { event -> event.player.addMiniMapQuestActorName( "Leon-VQS" ) } 
onQuestStep( 82, 2 ) { event -> event.player.removeMiniMapQuestLocation("Gnome Courier"); event.player.addMiniMapQuestActorName( "Leon-VQS" ) }
onQuestStep( 89, 2 ) { event -> event.player.addMiniMapQuestActorName( "Leon-VQS" ) }
onQuestStep(339, 2) { event -> if(event.player.isOnQuest(340, 3)) { event.player.addMiniMapQuestActorName("Leon-VQS") }}
onQuestStep(340, 2) { event -> if(event.player.isOnQuest(339, 3)) { event.player.addMiniMapQuestActorName("Leon-VQS") }}

myManager.onEnter( myRooms.VILLAGE_406 ) { event ->
	if( isPlayer( event.actor ) ) {
		//if the player enters this room from outside it, then set this flag so the reg prompt doesn't get shown.
		if( !event.actor.hasQuestFlag( GLOBAL, "Z02RegPromptShown" ) && !event.actor.hasQuestFlag( GLOBAL, "Z25ComingFromDani" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z02RegPromptShown" )
		}
		//unset the "ring grant in progress" flag in case of logout during ring selection
		event.actor.unsetQuestFlag( GLOBAL, "Z02RingRewardInProgress" )
		
		//unset the conversation flag so the logic trees, below, will work properly
		event.actor.unsetQuestFlag( GLOBAL, "Z02LeonFlag" )
		event.actor.unsetQuestFlag( GLOBAL, "Z02AfterPreamble" )
		
		if( event.actor.hasQuestFlag( GLOBAL, "Z25ComingFromDani" ) ) {
			
			Leon.pushDialog(event.actor, "fromElsewhere")
			event.actor.unsetQuestFlag(GLOBAL, "Z25ComingFromDani")

			//set up the noob kill event messages
			/*if( !event.actor.hasQuestFlag( GLOBAL, "ZXXCrewUpCounterStarted" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXCrewUpCounterStarted" )
				makeCounter(event.actor, "crewUpKillCounter", 0, false).watchForKill( "lawn_gnome", "lawn_gnome_LT", "pink_flamingo", "mushroom_turret", "lawn_gnome_LT2", "lawn_gnome_courier", "lawn_gnome_demi", "greens_fluff", "sandtrap_fluff" ).setGoal(5).onCompletion( { 
					tryToShowCrewTutorial( event )
					removeCounter( event.actor, "crewUpKillCounter" )
					event.actor.unsetQuestFlag( GLOBAL, "ZXXCrewUpCounterStarted" )
				} )
			}
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXPowerupCounterStarted" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXPowerupCounterStarted" )
				makeCounter(event.actor, "powerupKillCounter", 0, false).watchForKill( "lawn_gnome", "lawn_gnome_LT", "pink_flamingo", "mushroom_turret", "lawn_gnome_LT2", "lawn_gnome_courier", "lawn_gnome_demi", "greens_fluff", "sandtrap_fluff" ).setGoal(10).onCompletion( { 
					tryToShowPowerUpsTutorial( event )
					removeCounter( event.actor, "powerupKillCounter" )
					event.actor.unsetQuestFlag( GLOBAL, "ZXXPowerupCounterStarted" )
				} )
			}

			player = event.actor
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXOnHealthStarted" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXOnHealthStarted" )
				startOnHealth()
			}
			if( !event.actor.hasQuestFlag( GLOBAL, "ZXXOnDeathStarted" ) ) {
				event.actor.setQuestFlag( GLOBAL, "ZXXOnDeathStarted" )
				startOnDeath()
			}*/

			event.actor.centerPrint( "Commander Leon stands nearby, ready to speak with you!" )
		}
	}
}

myManager.onExit( myRooms.VILLAGE_406 ) { event ->
	if( isPlayer( event.actor ) ) {
		if( !event.actor.hasQuestFlag( GLOBAL, "Z02RegPromptShown" ) ) {
			event.actor.setQuestFlag( GLOBAL, "Z02RegPromptShown" )
			myManager.schedule(1) { showRegPrompt( event.actor ) }
		}
	}
}

def synchronized checkIfPlayerAggroed( event ) {
	println "**** CHECKING FOR AGGRO ****"
	playerHated = false
	println "**** event.actor = ${ event.actor } ****"
	println "**** battleroom = ${event.actor.getRoom()}****"
	event.actor.getRoom().getActorList().each() {
		if( isMonster( it ) ) {
			println "**** Hate list for monster = ${it.getHated()} ****"
			if( it.getHated().contains( event.actor ) ) {
				playerHated = true
			}
		}
	}
	println "**** playerHated = ${playerHated} ****"
}

def synchronized tryToShowCrewTutorial( event ) {
	checkIfPlayerAggroed( event )
	if( playerHated == false ) {
		tutorialNPC.pushDialog( event.actor, "crewAdv" )
	} else {
		myManager.schedule(3) { tryToShowCrewTutorial( event ) }
	}
}

def synchronized tryToShowPowerUpsTutorial( event ) {
	checkIfPlayerAggroed( event )
	if( playerHated == false ) {
		tutorialNPC.pushDialog( event.actor, "powerups" )
	} else {
		myManager.schedule(3) { tryToShowPowerUpsTutorial( event ) }
	}
}

def startOnHealth() {
	//set up some onHealth tutorials
	myManager.onHealth( player ) { event ->
		if( event.didTransition( 50 ) && event.isDecrease() ) {
			event.actor.centerPrint( "You're down to half-health right now. Find a place to Kneel and Heal!" )
		} else if( event.didTransition( 20 ) && event.isDecrease() ) {
			event.actor.centerPrint( "That heartbeat sound you hear is a warning that your health is very low!" )
		} else if( event.didTransition( 1 ) && event.isDecrease() ) {
			myManager.stopListenForHealth( event.actor )
		}
	}
}

def startOnDeath() {
	//set up the Dazed message
	runOnDeath( player ) { event ->
		tutorialNPC.pushDialog( event.actor, "dazedMessage" )
		event.actor.setQuestFlag( GLOBAL, "Z02PlayerDazed" )
		myManager.stopListenForHealth( event.actor )
	}
}

crewAdv = tutorialNPC.createConversation( "crewAdv", true )

def crew1 = [id:1]
crew1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/whyCrew.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
crew1.result = DONE
crewAdv.addDialog( crew1, tutorialNPC )

powerups = tutorialNPC.createConversation( "powerups", true )

def power1 = [id:1]
power1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/powerupTutorial.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
power1.result = DONE
powerups.addDialog( power1, tutorialNPC )


//MESSAGE SEEN WHEN DAZED
dazedMessage = tutorialNPC.createConversation( "dazedMessage", true )

def dazed1 = [id:1]
dazed1.npctext = "<zOMG dialogWidth='550'><![CDATA[<p><img src='help-files/topic-content/default/awakenTutorial.png'></p><textformat leading='350'> </textformat><br>]]></zOMG>"
dazed1.result = DONE
dazedMessage.addDialog( dazed1, tutorialNPC )

//=========================================================
// CONVERSATION ROUTING HUB                                
//=========================================================
/*def convoHub = Leon.createConversation( "convoHub", true, "!Z02LeonFlag", "!Z02AfterPreamble", "!Z02RingRewardInProgress" )

def hub1 = [id:1]
hub1.npctext = "Hark and well met, citizen!"
hub1.exec = { event ->
	if( event.player.hasQuestFlag( GLOBAL, "Z25ComingFromDani" ) ) {
		event.player.setQuestFlag( GLOBAL, "Z02LeonFlag" )
		Leon.pushDialog( event.player, "fromDani" )

	//if the user doesn't have "Z25ComingFromDani"
	} else {
		if( !event.player.isOnQuest( 249 ) && !event.player.isDoneQuest( 249 ) && !event.player.hasQuestFlag( GLOBAL, "Z02FindElizabeth" ) ) {
			event.player.setQuestFlag( GLOBAL, "Z02LeonFlag" )
			Leon.pushDialog( event.player, "fromElsewhere" )

		} else if( !event.player.isOnQuest( 249 ) && !event.player.isDoneQuest( 249 ) && event.actor.hasQuestFlag( GLOBAL, "Z02FindElizabeth" ) ) {
			event.player.setQuestFlag( GLOBAL, "Z02LeonFlag" )
			Leon.pushDialog( event.player, "findLiz" )			

		} else if( event.player.isOnQuest( 249 ) && !event.player.isDoneQuest( 249 ) ) { //finding elizabeth
			event.player.setQuestFlag( GLOBAL, "Z02LeonFlag" )
			Leon.pushDialog( event.player, "lizInterim" )	

		} else if( event.player.isOnQuest( 54, 3 ) ) {  //lunchbox quest
			event.player.setQuestFlag( GLOBAL, "Z02LeonFlag" )
			Leon.pushDialog( event.player, "LeonLunch" )

		} else if( event.player.isDoneQuest( 249 ) && event.player.isDoneQuest( 74 ) ) { //if everything else is done, then move to Leon's quest chain
			event.player.setQuestFlag( GLOBAL, "Z02AfterPreamble" )

			//Start convo logic analysis
			if(event.player.hasQuestFlag(GLOBAL, "Z02AfterPreamble") && !event.player.isOnQuest(78) && !event.player.isDoneQuest(78) && !event.player.isOnQuest(54, 2) && !event.player.isOnQuest(89)) {
				Leon.pushDialog(event.player, "leonAdvancingGive")

			} else if(event.player.isOnQuest(78) && !event.player.isOnQuest(78, 5) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonAdvancingActive")

			} else if(event.player.isOnQuest(78, 5) && !event.player.isOnQuest(82) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonAdvancingComplete")

			} else if(event.player.hasQuestFlag(GLOBAL, "Z2_Leon_GnomishBattlePlans_Break") && !event.player.isOnQuest(82) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonPlansBreak")

			} else if(event.player.isOnQuest(82, 2) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonPlansActive")

			} else if(event.player.isOnQuest(82, 3) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonPlansComplete")

			} else if(event.player.hasQuestFlag(GLOBAL, "Z2_Leon_Call_Break") && !event.player.isOnQuest(84) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonCallBreak")

			} else if(event.player.isOnQuest(84, 2) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonCallActive")

			} else if(event.player.isOnQuest(84,3) && !event.player.isOnQuest(89) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonCallComplete")

			} else if(event.player.hasQuestFlag(GLOBAL, "Z2_Leon_Balance_Break") && !event.player.isOnQuest(89) && !event.player.isDoneQuest(89) && !event.player.isOnQuest(90) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonBalanceBreak")

			} else if(event.actor.hasQuestFlag( GLOBAL, "Z02LeonRingGrantAllowed" ) && !event.actor.isOnQuest( 54, 3 ) && !event.actor.hasQuestFlag( GLOBAL, "Z02FirstRingGrantReceived" ) && !event.actor.hasQuestFlag( GLOBAL, "Z02RingRewardInProgress" ) ) {
				Leon.pushDialog(event.player, "ringGrant" )

			} else if(event.player.isOnQuest(89, 2) && !event.player.isOnQuest(54, 3)) {
				Leon.pushDialog(event.player, "leonBalanceActive")

			} else if(event.player.isOnQuest(89, 3) && !event.player.isOnQuest(90) && !event.player.isDoneQuest(90) && !event.player.isOnQuest(54, 3) && !event.player.hasQuestFlag(GLOBAL, "Z02LeonRingGrantAllowed")) {
				Leon.pushDialog(event.player, "leonBalanceComplete")

			} else if(event.player.hasQuestFlag(GLOBAL, "Z02LeonRingGrantAllowed") && !event.player.isOnQuest(54, 3) && !event.player.hasQuestFlag(GLOBAL, "!Z02FirstRingGrantReceived")) {
				Leon.pushDialog(event.player, "ringGrant")

			} else if(event.player.hasQuestFlag(GLOBAL, "Z2_Leon_Remo_Break") && !event.player.isOnQuest(54, 3) && !event.player.isOnQuest(86) && !event.player.isDoneQuest(86)) {
				Leon.pushDialog(event.player, "leonRemoBreak")

			} else if(event.player.isOnQuest(90) && !event.player.isOnQuest(54, 3) && !event.player.hasQuestFlag(GLOBAL, "Z02LeonRingGrantAllowed")) {
				Leon.pushDialog(event.player, "leonRemoStarted")

			} else if(event.player.isDoneQuest(90) && !event.player.isOnQuest(54, 3) && !event.player.hasQuestFlag(GLOBAL, "Z02LeonRingGrantAllowed")) {
				Leon.pushDialog(event.player, "leonRemoDone")
			}
		}
	}
}
convoHub.addDialog( hub1, Leon )*/

//=========================================================
// INITIAL CONVERSATION WHEN COMING FROM DANI (Sewers)     
//=========================================================
/*def fromDani = Leon.createConversation( "fromDani", true, "Z25ComingFromDani", "Z02LeonFlag" )
fromDani.setUrgent( true )

def from1 = [id:1]
from1.playertext = "Ummm...is this Barton Town?"
from1.result = 2
fromDani.addDialog( from1, Leon )

def from2 = [id:2]
from2.npctext = "Heh. Hi there. You're not quite in Barton Town yet. You're in the Village Greens. I'm Commander Leon of the Barton Regulars. Are you ready for action?"
from2.playertext = "Why? Is there an attack of some sort coming?"
from2.result = 3
fromDani.addDialog( from2, Leon )

def from3 = [id:3]
from3.npctext = "No, no. Or at least, not that we know of anyway. But you never can tell. If you wander away from the road, then you're bound to run afoul of the Animated somewhere."
from3.playertext = "But I'm safe on the road?"
from3.result = 4
fromDani.addDialog( from3, Leon )

def from4 = [id:4]
from4.npctext = "Most of the time. If you head north from here, you'll pass a guard named Elizabeth and then Barton Town is just beyond that."
from4.result = 5
fromDani.addDialog( from4, Leon )

def from5 = [id:5]
from5.npctext = "I **highly recommend** that you speak with Elizabeth. Until you find out about the Null Chamber, you won't be able to increase the power in your rings and that's really important. So head up and talk to her. She'll even grant you a new ring!"
from5.options = []
from5.options << [text: "Okay. I'll go talk to Elizabeth!", exec: { event-> event.player.unsetQuestFlag( GLOBAL, "Z25ComingFromDani" ); event.player.setQuestFlag( GLOBAL, "Z02FindElizabeth" ); Leon.pushDialog( event.player, "findLiz" ) }, result: DONE]
from5.options << [text: "I'm going to wander around a bit, first. Maybe later.", flag: "!Z02LeonFlag", result: DONE]
fromDani.addDialog( from5, Leon )*/

//=========================================================
// INITIAL CONVERSATION WHEN COMING FROM ELSEWHERE         
//=========================================================
def fromElsewhere = Leon.createConversation( "fromElsewhere", true, "!QuestStarted_339", "!QuestCompleted_339", "!QuestStarted_340", "!QuestCompleted_340", "!QuestStarted_54:3", "!QuestStarted_249", "!QuestCompleted_249", "!QuestStarted_78", "!QuestCompleted_78" )
fromElsewhere.setUrgent( true )

def else1 = [id:1]
else1.npctext = "Well met, citizen."
else1.playertext = "And back to you too! But, who are you?"
else1.result = 2
fromElsewhere.addDialog( else1, Leon )

def else2 = [id:2]
else2.npctext = "My name is Leon. I'm the Commander of the Barton Regulars. Are you ready for action?"
else2.playertext = "Why? Is there an attack of some sort coming?"
else2.result = 3
fromElsewhere.addDialog( else2, Leon )

def else3 = [id:3]
else3.npctext = "Who knows? Maybe! I hope you've got a ring ready!"
else3.playertext = "Ready for what?"
else3.exec = { event ->
	event.player.setQuestFlag(GLOBAL, "Z02_FlamingoGnome_Allowed")
	event.player.removeMiniMapQuestActorName("Leon-VQS")
	Leon.pushDialog(event.player, "flamingoGnomeFightGrant")
}
else3.result = DONE
fromElsewhere.addDialog( else3, Leon )

/*def else4 = [id:4]
else4.npctext = "I highly recommend you speak with Elizabeth. Until you find out about the Null Chamber, you won't be able to increase the power in your rings and that's really important. So head up and talk to her. She'll even grant you a new ring!"
else4.options = []
else4.options << [text: "Okay. I'll go talk to Elizabeth!", exec: { event-> event.player.setQuestFlag( GLOBAL, "Z02FindElizabeth" ); Leon.pushDialog( event.player, "findLiz" ) }, result: DONE]
else4.options << [text: "Not now. Thanks though.", flag: "!Z02LeonFlag", exec: { event -> if( event.player.isOnQuest( 54, 3 ) ) { event.player.setQuestFlag( GLOBAL, "Z02LunchBridgeEnabled" ); Leon.pushDialog( event.player, "lunchBridge" ) } }, result: DONE]
fromElsewhere.addDialog( else4, Leon )*/

//=========================================================
// Flamingo/Gnome Fight (grant)                            
//=========================================================
def flamingoGnomeFightGrant = Leon.createConversation("flamingoGnomeFightGrant", true, "!QuestStarted_339", "!QuestCompleted_339", "!QuestStarted_340", "!QuestCompleted_340", "!QuestStarted_54:3", "!Z02AfterPreamble", "Z02_FlamingoGnome_Allowed")
flamingoGnomeFightGrant.setUrgent(true)

def flamingoGnomeFightGrant1 = [id:1]
flamingoGnomeFightGrant1.npctext = "Ready for a fight, of course."
flamingoGnomeFightGrant1.playertext = "Oh, sounds fun! What do you need me to fight?"
flamingoGnomeFightGrant1.result = 2
flamingoGnomeFightGrant.addDialog(flamingoGnomeFightGrant1, Leon)

def flamingoGnomeFightGrant2 = [id:2]
flamingoGnomeFightGrant2.npctext = "Truthfully, I have many tasks that need doing. But before we begin those, I need to make sure you're ready for the challenges ahead."
flamingoGnomeFightGrant2.result = 3
flamingoGnomeFightGrant.addDialog(flamingoGnomeFightGrant2, Leon)

def flamingoGnomeFightGrant3 = [id:3]
flamingoGnomeFightGrant3.npctext = "Search the nearby area. Defeat five Pink Flamingos and five Lawn Gnomes to show your readiness."
flamingoGnomeFightGrant3.flag = "Z02_FlamingoGnome_Allowed"
flamingoGnomeFightGrant3.exec = { event ->
	event.player.setQuestFlag(GLOBAL, "Z02AfterPreamble")
	event.player.updateQuest(339, "Leon-VQS")
	event.player.updateQuest(340, "Leon-VQS")
}
flamingoGnomeFightGrant3.result = DONE
flamingoGnomeFightGrant.addDialog(flamingoGnomeFightGrant3, Leon)

//=========================================================
// Flamingo/Gnome Fight (active)                           
//=========================================================
def flamingoGnomeFightActive = Leon.createConversation("flamingoGnomeFightActive", true, "QuestStarted_339:2", "QuestStarted_340:2", "!QuestStarted_54:3")

def flamingoGnomeFightActive1 = [id:1]
flamingoGnomeFightActive1.npctext = "You need to defeat five Pink Flamingos and five Lawn Gnomes in the nearby area before we continue."
flamingoGnomeFightActive1.result = DONE
flamingoGnomeFightActive.addDialog(flamingoGnomeFightActive1, Leon)

//=========================================================
// Flamingo Fight (active)                                 
//=========================================================
def flamingoFightActive = Leon.createConversation("flamingoFightActive", true, "QuestStarted_339:2", "!QuestStarted_340:2", "!QuestStarted_54:3")

def flamingoFightActive1 = [id:1]
flamingoFightActive1.npctext = "You need to defeat five Pink Flamingos in the nearby area before we continue."
flamingoFightActive1.result = DONE
flamingoFightActive.addDialog(flamingoFightActive1, Leon)

//=========================================================
// Gnome Fight (active)                                    
//=========================================================
def gnomeFightActive = Leon.createConversation("gnomeFightActive", true, "!QuestStarted_339:2", "QuestStarted_340:2", "!QuestStarted_54:3")

def gnomeFightActive1 = [id:1]
gnomeFightActive1.npctext = "You need to defeat five Lawn Gnomes in the nearby area before we continue."
gnomeFightActive1.result = DONE
gnomeFightActive.addDialog(gnomeFightActive1, Leon)

//=========================================================
// Flamingo/Gnome Fight (complete)                         
//=========================================================
def flamingoGnomeFightComplete = Leon.createConversation("flamingoGnomeFightComplete", true, "QuestStarted_339:3", "QuestStarted_340:3", "!QuestStarted_54:3")
flamingoGnomeFightComplete.setUrgent(true)

def flamingoGnomeFightComplete1 = [id:1]
flamingoGnomeFightComplete1.npctext = "Well done! You will definitely be able to help."
flamingoGnomeFightComplete1.playertext = "Thanks."
flamingoGnomeFightComplete1.result = 2
flamingoGnomeFightComplete.addDialog(flamingoGnomeFightComplete1, Leon)

def flamingoGnomeFightComplete2 = [id:2]
flamingoGnomeFightComplete2.npctext = "Before you begin, though, it's time to upgrade that ring of yours."
flamingoGnomeFightComplete2.result = 3
flamingoGnomeFightComplete.addDialog(flamingoGnomeFightComplete2, Leon)

def flamingoGnomeFightComplete3 = [id:3]
flamingoGnomeFightComplete3.npctext = "Take these Charge Orbs and go see Elizabeth, one of the other guards. She'll help you with upgrading your ring. I'll mark your map with a flag to help you find her."
flamingoGnomeFightComplete3.playertext = "Okay, Leon. I'll go see her."
flamingoGnomeFightComplete3.exec = { event ->
	event.player.removeMiniMapQuestActorName("Leon-VQS")
	event.player.addMiniMapQuestActorName("BFG-Elizabeth")
	event.player.updateQuest(339, "Leon-VQS")
	event.player.updateQuest(340, "Leon-VQS")
	event.player.updateQuest(249, "Leon-VQS")
}
flamingoGnomeFightComplete3.result = DONE
flamingoGnomeFightComplete.addDialog(flamingoGnomeFightComplete3, Leon)

//=========================================================
// Finding Elizabeth (active)                              
//=========================================================
def findingElizabethActive = Leon.createConversation("findingElizabethActive", true, "QuestStarted_249:2", "!QuestStarted_54:3")

def findingElizabethActive1 = [id:1]
findingElizabethActive1.npctext = "Are you having trouble finding Elizabeth? She's just up to the north from here."
findingElizabethActive1.result = DONE
findingElizabethActive.addDialog(findingElizabethActive1, Leon)

//=========================================================
// LUNCHBOX BRIDGE (coming from "fromElsewhere")           
//=========================================================
/*def lunchBridge = Leon.createConversation( "lunchBridge", true, "Z02LunchBridgeEnabled", "Z02LeonFlag" )

def bridge1 = [id:1]
bridge1.playertext = "But hey, before I go..."
bridge1.flag = [ "!Z02LunchBridgeEnabled", "!Z02LeonFlag" ]
bridge1.exec = { event ->
	Leon.pushDialog( event.player, "LeonLunch" )
}
bridge1.result = DONE
lunchBridge.addDialog( bridge1, Leon )*/

//---------------------------------------------------------
// LUNCHBOX QUEST (Leon's Part)                            
//---------------------------------------------------------

def LeonLunch = Leon.createConversation("LeonLunch", true, "QuestStarted_54:3")

def Lunch1 = [id:1]
Lunch1.playertext = "Your mom asked me to bring you some lunch."
Lunch1.exec = { event ->
		event.player.removeMiniMapQuestActorName( "Leon-VQS" )
	}
Lunch1.result = 2
LeonLunch.addDialog(Lunch1, Leon)

def Lunch2 = [id:2]
Lunch2.npctext = "Aha! I thought I detected the delicious low-tide bouquet of her famous pineapple oyster casserole."
Lunch2.options = []
Lunch2.options << [text:"Bon appetit!", result: 5]
Lunch2.options << [text:"Are you sure this is safe to eat? it smells like hot garbage.", result: 3]
LeonLunch.addDialog(Lunch2, Leon)

def Lunch3 = [id:3]
Lunch3.npctext = "What? Are you saying there's something WRONG with my dear mother's cooking, which nourished me into the stout, strong man I am today?"
Lunch3.options = []
Lunch3.options << [text:"Sorry, Leon, I didn't mean to offend you.", result: 4]
Lunch3.options << [text:"Yes. Her cookies nearly poisoned half the guards in Barton", result: 8]
LeonLunch.addDialog(Lunch3, Leon)

def Lunch4 = [id:4]
Lunch4.npctext = "Ha! Don't worry, I'm just pulling your leg. I know I'm the only one in the world who can stand this muck."
Lunch4.result = 5
LeonLunch.addDialog(Lunch4, Leon)

def Lunch5 = [id:5]
Lunch5.npctext = "Thanks for bringing it all the way out here. This'll really hit the spot after a long day of fighting."
Lunch5.playertext = "You're welcome!"
Lunch5.result = 6
LeonLunch.addDialog(Lunch5, Leon)

def Lunch6 = [id:6]
Lunch6.npctext = "Say, would you mind taking the empty pail back to my mom next time you're in Barton?"
Lunch6.options = []
Lunch6.options << [text:"I'll do that right away.", result: 7]
Lunch6.options << [text:"Sorry, I won't be heading to town anytime soon.", result: 7]
LeonLunch.addDialog(Lunch6, Leon)

def Lunch7 = [id:7]
Lunch7.npctext = "There's no rush. Whenever you happen to be back that way, just drop it off. I'm sure she'll reward you for the effort. Thanks, %p!"
Lunch7.quest = 54
Lunch7.flag = "!Z02LeonFlag"
Lunch7.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Leon-VQS" )
	event.player.addMiniMapQuestActorName( "Olivia-VQS" )
}
Lunch7.result = DONE
LeonLunch.addDialog(Lunch7, Leon)

def Lunch8 = [id:8]
Lunch8.npctext = "Poisoned? The guards!? What sort of a madman would feed my mother's cookies to the guards?"
Lunch8.playertext = "Actually, it was me..."
Lunch8.result = 9
LeonLunch.addDialog(Lunch8, Leon)

def Lunch9 = [id:9]
Lunch9.npctext = "What were you thinking!? Do you have any idea what she puts in those things? How many of the guards are in the hospital now?"
Lunch9.playertext = "None. They all refused to eat the cookies when they found out your mother baked them."
Lunch9.result = 10
LeonLunch.addDialog(Lunch9, Leon)

def Lunch10 = [id:10]
Lunch10.npctext = "Phew, that's a relief. Sorry for going off on you like that, but we need every guard to be in peak physical form these days. We can't have them all clutching their bellies and moaning."
Lunch10.playertext = "Is your mother's cooking really so dangerous?"
Lunch10.result = 11
LeonLunch.addDialog(Lunch10, Leon)

def Lunch11 = [id:11]
Lunch11.npctext = "Most people can't stand it, but I can't get enough. I guess I'm immune, since I grew up on it. Although her coconut calamari stroganoff does get to me once in a while... But thanks for bringing my lunch all the way out here. This'll really hit the spot after a long day of fighting."
Lunch11.result = 6
LeonLunch.addDialog(Lunch11, Leon)

//--------------------------------------------------------------
//AN ADVANCING ENEMY - QUEST GIVE                               
//--------------------------------------------------------------
def leonAdvancingGive = Leon.createConversation("leonAdvancingGive", true, "!QuestStarted_78", "!QuestCompleted_78", "!QuestStarted_89", "QuestStarted_341:2", "!QuestStarted_54:3" )

def advancingGive1 = [id:1]
advancingGive1.npctext = "Ah, you return. We must clean up the Village Greens, %p. It used to be such a nice place. Johnny Gambino himself used to come down every week."
advancingGive1.playertext = "What happened?"
advancingGive1.result = 2
leonAdvancingGive.addDialog(advancingGive1, Leon)

def advancingGive2 = [id:2]
advancingGive2.npctext = "Can you believe our own lawn ornaments have turned against us? It has puts a real damper on tourism, and I have a feeling it's going to get worse before it gets better."
advancingGive2.playertext = "How can it get any worse?"
advancingGive2.result = 3
leonAdvancingGive.addDialog(advancingGive2, Leon)

def advancingGive3 = [id:3]
advancingGive3.npctext = "I think the Animated around here are getting smarter. The gnomes used to mill around mindlessly, but now they seem to be gathering up in little camps, setting up defenses... preparing to attack, you might say."
advancingGive3.result = 4
leonAdvancingGive.addDialog(advancingGive3, Leon)

def advancingGive4 = [id:4]
advancingGive4.npctext = "I just need someone to scout the gnome camps and see what they're up to. "
advancingGive4.options = []
advancingGive4.options << [text:"Consider it done.", result: 5]
advancingGive4.options << [text:"Why don't you just go yourself?", result: 6]
leonAdvancingGive.addDialog(advancingGive4, Leon)

def advancingGive5 = [id:5]
advancingGive5.npctext = "The gnomish encampments are just south of the goof course. Head to the west, taking care not to alert the gnomes to your presence as you approach. Survey the camps and return to me."
advancingGive5.flag = "!Z02AfterPreamble"
advancingGive5.quest = 78
advancingGive5.exec = { event ->
	event.player.updateQuest(341, "Leon-VQS")
	event.player.addMiniMapQuestLocation( "Gnomish Encampment North", "VILLAGE_402", 675, 460, "North Outpost" )
	event.player.addMiniMapQuestLocation( "Gnomish Encampment West", "VILLAGE_501", 785, 285, "West Outpost" )
	event.player.addMiniMapQuestLocation( "Gnomish Encampment East", "VILLAGE_503", 730, 150, "East Outpost" )
	event.player.removeMiniMapQuestActorName( "Leon-VQS" )
}
advancingGive5.result = DONE
leonAdvancingGive.addDialog(advancingGive5, Leon)

def advancingGive6 = [id:6]
advancingGive6.npctext = "I'd love to, but I have to maintain my post."
advancingGive6.options = []
advancingGive6.options << [text:"I understand. I'll get right on it.", result: 5]
advancingGive6.options << [text:"Why? It looks like you're just standing around...", result: 7]
leonAdvancingGive.addDialog(advancingGive6, Leon)

def advancingGive7 = [id:7]
advancingGive7.npctext = "Exactly. I have to keep up the appearance of vigilance so the people of Barton can sleep soundly at night knowing that the Barton Regulars will always be here."
advancingGive7.options = []
advancingGive7.options << [text:"That seems reasonable. I'll scout the camps for you.", result: 5]
advancingGive7.options << [text:"Sure, Leon. Enjoy your vacation.", result: 8]
leonAdvancingGive.addDialog(advancingGive7, Leon)

def advancingGive8 = [id:8]
advancingGive8.npctext = "Hey!"
advancingGive8.playertext = "Looks like you need all the help you can get. I guess I could go scout those camps for you."
advancingGive8.result = 5
leonAdvancingGive.addDialog(advancingGive8, Leon)

//--------------------------------------------------------------
//AN ADVANCING ENEMY - QUEST ACTIVE                             
//--------------------------------------------------------------
def leonAdvancingActive = Leon.createConversation("leonAdvancingActive", true, "QuestStarted_78", "!QuestStarted_78:5", "!QuestStarted_54:3" )

def advancingActive1 = [id:1]
advancingActive1.npctext = "It's time to get those gnome encampments scouted. I've marked the locations on your map, so hop to it!"
advancingActive1.result = DONE
leonAdvancingActive.addDialog(advancingActive1, Leon)

//--------------------------------------------------------------
//AN ADVANCING ENEMY - QUEST COMPLETE                           
//--------------------------------------------------------------
def leonAdvancingComplete = Leon.createConversation("leonAdvancingComplete", true, "QuestStarted_78:5", "!QuestStarted_82", "!QuestStarted_54:3" )
leonAdvancingComplete.setUrgent( true )

def advancingComplete1 = [id:1]
advancingComplete1.npctext = "So, what's the news from the gnome camps? Are they planning something?"
advancingComplete1.options = []
advancingComplete1.options << [text:"I think so. Why else would they be gathering their forces like that?", result: 3]
advancingComplete1.options << [text:"The motivations of lawn ornaments are foreign to me, Leon.", result: 2]
leonAdvancingComplete.addDialog(advancingComplete1, Leon)

def advancingComplete2 = [id:2]
advancingComplete2.npctext = "Ha! Yeah, I guess I got a little ahead of myself. Who knows what those little buggers are up to? There must be some other way to figure it out..."
advancingComplete2.result = 5
leonAdvancingComplete.addDialog(advancingComplete2, Leon)

def advancingComplete3 = [id:3]
advancingComplete3.npctext = "I'm worried that they might be organizing for some kind of attack on Barton Town. Obviously, we can't have that."
advancingComplete3.result = 5
leonAdvancingComplete.addDialog(advancingComplete3, Leon)

def advancingComplete5 = [id:5]
advancingComplete5.npctext = "Are you up for a bit of reconnaissance work, %p? I'd love to gather some intelligence on the gnomes - assuming they have any intelligence to gather."
advancingComplete5.options = []
advancingComplete5.options << [text:"What do you have in mind?", result: 6]
advancingComplete5.options << [text:"They looked a little too dumb for 'intelligence.' Seems like a waste of time", result: 10]
leonAdvancingComplete.addDialog(advancingComplete5, Leon)

def advancingComplete6 = [id:6]
advancingComplete6.npctext = "This might be a little tricky: I've seen a particularly sprightly little gnome scooting along the path between the gnome fortress and the northern outposts. I think he may be some sort of courier."
advancingComplete6.result = 7
leonAdvancingComplete.addDialog(advancingComplete6, Leon)

def advancingComplete7 = [id:7]
advancingComplete7.npctext = "I'd like you to find him, rough him up, and intercept any documents he might be carrying. Look for a gnome with a blue hat."
advancingComplete7.options = []
advancingComplete7.options << [text:"Got it.", result: 9]
advancingComplete7.options << [text:"Maybe another time, Leon. I'm pretty busy.", result: 10]
leonAdvancingComplete.addDialog(advancingComplete7, Leon)

def advancingComplete9 = [id:9]
advancingComplete9.npctext = "Great, let me know if that cute little menace has any information."
advancingComplete9.quest = 82
advancingComplete9.flag = ["!Z2_GnomeOutpost501", "!Z2_GnomeOutpost402", "!Z2_GnomeOutpost503" ]
advancingComplete9.exec = { event -> 
	event.player.updateQuest(78, "Leon-VQS") 
	event.player.removeMiniMapQuestActorName( "Leon-VQS" )
	event.player.addMiniMapQuestLocation("Gnome Courier", "VILLAGE_603", 400, 240, "Crossroads - Gnome Courier")
}
advancingComplete9.result = DONE
leonAdvancingComplete.addDialog(advancingComplete9, Leon)

def advancingComplete10 = [id:10]
advancingComplete10.npctext = "Aww, c'mon! I thought we were a team! Leon and %p, defenders of Barton! Well, come see me again if you change your mind."
advancingComplete10.quest = 78
advancingComplete10.flag = ["Z2_Leon_GnomishBattlePlans_Break", "!Z2_GnomeOutpost501", "!Z2_GnomeOutpost402", "!Z2_GnomeOutpost503"]
advancingComplete10.exec = { event ->
	event.player.removeMiniMapQuestActorName("Leon-VQS")
}
advancingComplete10.result = DONE
leonAdvancingComplete.addDialog(advancingComplete10, Leon)

//--------------------------------------------------------------
//GNOMISH BATTLE PLANS - QUEST BREAK                            
//--------------------------------------------------------------
def leonPlansBreak = Leon.createConversation("leonPlansBreak", true, "Z2_Leon_GnomishBattlePlans_Break", "!QuestStarted_82", "!QuestStarted_54:3" )

def plansBreak1 = [id:1]
plansBreak1.npctext = "So, ready to help with some intelligence work? We've got to find out what those gnomes are up to."
plansBreak1.options = []
plansBreak1.options << [text:"I'll give it a try.", result:2]
plansBreak1.options << [text:"Nah, I've got better things to do.", result:3]
leonPlansBreak.addDialog(plansBreak1, Leon)

def plansBreak2 = [id:2]
plansBreak2.npctext = "You'll find a little Gnomish Courier scurrying along the path between the lawn gnome fortress and the northern outposts. I want you to rough him up and grab his battle plans, then bring them to me."
plansBreak2.quest = 82
plansBreak2.flag = "!Z2_Leon_GnomishBattlePlans_Break"
plansBreak2.exec = { event ->
	event.player.addMiniMapQuestLocation("Gnome Courier", "VILLAGE_603", 400, 240, "Crossroads - Gnome Courier")
}
plansBreak2.result = DONE
leonPlansBreak.addDialog(plansBreak2, Leon)

def plansBreak3 = [id:3]
plansBreak3.npctext = "Fine, go have fun. I'll just stand here, all alone, and try to prevent the destruction of Barton Town by myself."
plansBreak3.result = DONE
leonPlansBreak.addDialog(plansBreak3, Leon)

//--------------------------------------------------------------
//GNOMISH BATTLE PLANS - QUEST ACTIVE                           
//--------------------------------------------------------------
def leonPlansActive = Leon.createConversation("leonPlansActive", true, "QuestStarted_82:2", "!QuestStarted_54:3" )

def plansActive1 = [id:1]
plansActive1.npctext = "Have you spotted the courier yet?"
plansActive1.options = []
plansActive1.options << [text:"I'm having a little trouble. Any suggestions?", result:2]
plansActive1.options << [text:"Not yet, but I'll get him soon.", result:3]
leonPlansActive.addDialog(plansActive1, Leon)

def plansActive2 = [id:2]
plansActive2.npctext = "If I were you, I would start with the path that runs from the southern fortress to the final holes of the northern outposts. I've seen him skittering along it several times."
plansActive2.result = DONE
leonPlansActive.addDialog(plansActive2, Leon)

def plansActive3 = [id:3]
plansActive3.npctext = "Good luck, %p."
plansActive3.result = DONE
leonPlansActive.addDialog(plansActive3, Leon)

//--------------------------------------------------------------
//GNOMISH BATTLE PLANS - QUEST COMPLETE                         
//--------------------------------------------------------------
def leonPlansComplete = Leon.createConversation("leonPlansComplete", true, "QuestStarted_82:3", "!QuestStarted_54:3" )

def plansComplete1 = [id:1]
plansComplete1.npctext = "Hey, you're back! Do you have the battle plans, then?"
plansComplete1.playertext = "Here you go. I can't make any sense of them."
plansComplete1.result = 2
leonPlansComplete.addDialog(plansComplete1, Leon)

def plansComplete2 = [id:2]
plansComplete2.npctext = "Well, let me see... that could be an 'a,' if you tilt your head right. Is that... a little picture of a windmill? Or maybe it's a cactus. And that could be a little campfire, or a sword..."
plansComplete2.result = 3
leonPlansComplete.addDialog(plansComplete2, Leon)

def plansComplete3 = [id:3]
plansComplete3.npctext = "Sword plus windmill? I guess it could mean they are going to attack Barton Town. What do you think?"
plansComplete3.options = []
plansComplete3.options << [text:"I have no idea. For all I know, it could be a grocery list.", result:4]
plansComplete3.options << [text:"This is indisputable proof of a gnomish plot to invade Barton.", result:5]
leonPlansComplete.addDialog(plansComplete3, Leon)

def plansComplete4 = [id:4]
plansComplete4.npctext = "Well, I guess we'd better be on the safe side. You should take these plans up to Clara, my second in command. She's been keeping a close eye on the Village Greens, so she'll have a valuable second opinion."
plansComplete4.options = []
plansComplete4.options << [text:"I'll have them to her in no time.", result:6]
plansComplete4.options << [text:"I'm a little swamped right now, Leon. Maybe another time.", result:7]
leonPlansComplete.addDialog(plansComplete4, Leon)

def plansComplete5 = [id:5]
plansComplete5.npctext = "Really? Wow, maybe you're right... you'd better take these plans up to Clara, my second in command. If this is proof of a gnomish attack, she'll muster the troops to take immediate action."
plansComplete5.options = []
plansComplete5.options << [text:"I'll have them to her in no time.", result:6]
plansComplete5.options << [text:"I'm a little swamped right now, Leon. Maybe another time.", result:7]
leonPlansComplete.addDialog(plansComplete5, Leon)

def plansComplete6 = [id:6]
plansComplete6.npctext = "She's posted by the south gate in Barton Town. Let me know what she says."
plansComplete6.quest = 84
plansComplete6.exec = { event -> 
	event.player.updateQuest(82, "Leon-VQS")
	event.player.addMiniMapQuestActorName("BFG-Clara")
	event.player.removeMiniMapQuestActorName("Leon-VQS")
}
plansComplete6.result = DONE
leonPlansComplete.addDialog(plansComplete6, Leon)

def plansComplete7 = [id:7]
plansComplete7.npctext = "What? Seriously? But we've got the plans and everything! Well, come see me again if you get a chance."
plansComplete7.flag = "Z2_Leon_Call_Break"
plansComplete7.quest = 82
plansComplete7.exec = { event -> event.player.removeMiniMapQuestActorName( "Leon-VQS" ) }
plansComplete7.result = DONE
leonPlansComplete.addDialog(plansComplete7, Leon)

//--------------------------------------------------------------
//A CALL TO ARMS - QUEST BREAK                                  
//--------------------------------------------------------------
def leonCallBreak = Leon.createConversation("leonCallBreak", true, "Z2_Leon_Call_Break", "!QuestStarted_84", "!QuestStarted_54:3" )

def callBreak1 = [id:1]
callBreak1.npctext = "So, do you have time to take these battle plans up to Clara at south gate?"
callBreak1.options = []
callBreak1.options << [text:"Sure, I'll do it.", result:2]
callBreak1.options << [text:"Some other time, Leon.", result:3]
leonCallBreak.addDialog(callBreak1, Leon)

def callBreak2 = [id:2]
callBreak2.npctext = "Great! She'll give us a good second opinion."
callBreak2.quest = 84
callBreak2.flag = "!Z2_Leon_Call_Break"
callBreak2.exec = { event -> event.player.addMiniMapQuestActorName("BFG-Clara") }
callBreak2.result = DONE
leonCallBreak.addDialog(callBreak2, Leon)

def callBreak3 = [id:3]
callBreak3.npctext = "Are you just coming back here to taunt me, or what?"
callBreak3.result = DONE
leonCallBreak.addDialog(callBreak3, Leon)

//--------------------------------------------------------------
//A CALL TO ARMS - QUEST ACTIVE                                 
//--------------------------------------------------------------
def leonCallActive = Leon.createConversation("leonCallActive", true, "QuestStarted_84:2", "!QuestStarted_54:3" )

def callActive1 = [id:1]
callActive1.npctext = "Remember, Clara's right up there by the south gate in Barton. She'll be very interested in those gnomish battle plans we found."
callActive1.result = DONE
leonCallActive.addDialog(callActive1, Leon)

//--------------------------------------------------------------
//A CALL TO ARMS - QUEST COMPLETE                               
//--------------------------------------------------------------
def leonCallComplete = Leon.createConversation("leonCallComplete", true, "QuestStarted_84:3", "!QuestStarted_89", "!QuestStarted_54:3" )

def callComplete1 = [id:1]
callComplete1.npctext = "Well? What did Clara have to say about the battle plans?"
callComplete1.playertext = "She doesn't think they're battle plans at all."
callComplete1.result = 2
leonCallComplete.addDialog(callComplete1, Leon)

def callComplete2 = [id:2]
callComplete2.npctext = "What are they, then?"
callComplete2.playertext = "She thinks the gnomes are mimicking what they see the guards doing, and the 'plans' are meaningless. She also thinks you should stop worrying."
callComplete2.result = 3
leonCallComplete.addDialog(callComplete2, Leon)

def callComplete3 = [id:3]
callComplete3.npctext = "She may be right about the plans being meaningless, but she could also be wrong. The day we stop worrying is the day Barton is overrun and I can't shake the hunch that they are plotting something. They're too well organized."
callComplete3.playertext = "What can we do?"
callComplete3.result = 4
leonCallComplete.addDialog(callComplete3, Leon)

def callComplete4 = [id:4]
callComplete4.npctext = "Actually, I thought of something. If they're really planning some kind of attack, they must have mid-level officers who relay orders from their central command to their outposts."
callComplete4.options = []
callComplete4.options << [text:"That seems logical.", result:6]
callComplete4.options << [text:"A lawn gnome chain of command? Isn't that a bit of a stretch?", result:5]
leonCallComplete.addDialog(callComplete4, Leon)

def callComplete5 = [id:5]
callComplete5.npctext = "You know what else is a bit of a stretch? Novelty garden products coming to life and beating us senseless."
callComplete5.result = 6
leonCallComplete.addDialog(callComplete5, Leon)

def callComplete6 = [id:6]
callComplete6.npctext = "We've run across some tougher gnomes who wear different uniforms and lead the outposts. If you take out these majors, it may temporarily cripple their chain of command."
callComplete6.options = []
callComplete6.options << [text:"Sounds like a plan.", result:7]
callComplete6.options << [text:"What if I just go straight for the commanders?", result:8]
callComplete6.options << [text:"I don't think I'm up for it right now.", result:9]
leonCallComplete.addDialog(callComplete6, Leon)

def callComplete7 = [id:7]
callComplete7.npctext = "Excellent, get out there and take out Major Catastrophe, Major Disaster, and Major Tragedy, %p. That ought to throw a wrench in their jaunty little military machine."
callComplete7.playertext = "LOL, are those names for real?!"
callComplete7.exec = { event ->
	event.player.setPlayerVar( "lieutenantCount", 0 )
	event.player.addMiniMapQuestLocation( "Major Tragedy", "VILLAGE_402", 675, 460, "Major Tragedy" )
	event.player.addMiniMapQuestLocation( "Major Catastrophe", "VILLAGE_501", 785, 285, "Major Catastrophe" )
	event.player.addMiniMapQuestLocation( "Major Disaster", "VILLAGE_503", 730, 150, "Major Disaster" )
	event.player.updateQuest(84, "Leon-VQS")
	event.player.updateQuest(89, "Leon-VQS")
	event.player.removeMiniMapQuestActorName("Leon-VQS")
}
callComplete7.result = DONE
leonCallComplete.addDialog(callComplete7, Leon)

def callComplete8 = [id:8]
callComplete8.npctext = "First things first, %p. We must break the threat which lies closest to Barton Town before we do anything else."
callComplete8.options = []
callComplete8.options << [text:"That seems reasonable. I'll attack the majors!", result:7]
callComplete8.options << [text:"I just don't have time right now, Leon.", result:9]
leonCallComplete.addDialog(callComplete8, Leon)

def callComplete9 = [id:9]
callComplete9.npctext = "Whenever you're ready, %p. I could always use a hand. I'll be waiting."
callComplete9.flag = "Z2_Leon_Balance_Break"
callComplete9.quest = 84
callComplete9.exec = { event -> event.player.removeMiniMapQuestActorName("Leon-VQS") }
callComplete9.result = DONE
leonCallComplete.addDialog(callComplete9, Leon)

//--------------------------------------------------------------
//BALANCE OF POWER - QUEST BREAK                                
//--------------------------------------------------------------
def leonBalanceBreak = Leon.createConversation("leonBalanceBreak", true, "!QuestStarted_89", "!QuestCompleted_89", "Z2_Leon_Balance_Break", "!QuestStarted_90", "!QuestStarted_54:3" )

def balanceBreak1 = [id:1]
balanceBreak1.npctext = "What do you say, %p? Ready to strike at the heart of the gnomish war machine?"
balanceBreak1.options = []
balanceBreak1.options << [text:"I'm ready to take on their majors!", result:2]
balanceBreak1.options << [text:"Where'd the time go? I've got to be running!", result:3]
leonBalanceBreak.addDialog(balanceBreak1, Leon)

def balanceBreak2 = [id:2]
balanceBreak2.npctext = "Excellent! Head to the northern encampments and give Major Catastrophe, Major Disaster, and Major Tragedy hell!"
balanceBreak2.quest = 89
balanceBreak2.flag = "!Z2_Leon_Balance_Break"
balanceBreak2.exec = { event -> 
	event.player.setPlayerVar( "lieutenantCount", 0 )
	event.player.addMiniMapQuestLocation( "Major Tragedy", "VILLAGE_402", 675, 460, "Major Tragedy" )
	event.player.addMiniMapQuestLocation( "Major Catastrophe", "VILLAGE_501", 785, 285, "Major Catastrophe" )
	event.player.addMiniMapQuestLocation( "Major Disaster", "VILLAGE_503", 730, 150, "Major Disaster" )
}
balanceBreak2.result = DONE
leonBalanceBreak.addDialog(balanceBreak2, Leon)

def balanceBreak3 = [id:3]
balanceBreak3.npctext = "I only hope the gnomes are as lazy as you, %p."
balanceBreak3.result = DONE
leonBalanceBreak.addDialog(balanceBreak3, Leon)

//--------------------------------------------------------------
//BALANCE OF POWER - QUEST ACTIVE                               
//--------------------------------------------------------------
def leonBalanceActive = Leon.createConversation("leonBalanceActive", true, "QuestStarted_89:2", "!QuestStarted_54:3" )

def balanceActive1 = [id:1]
balanceActive1.npctext = "You must decapitate the beast - without their majors, the gnomes cannot attack Barton Town."
balanceActive1.result = DONE
leonBalanceActive.addDialog(balanceActive1, Leon)

//--------------------------------------------------------------
//BALANCE OF POWER - QUEST COMPLETE                             
//--------------------------------------------------------------
def leonBalanceComplete = Leon.createConversation("leonBalanceComplete", true, "QuestStarted_89:3", "!QuestCompleted_90", "!QuestStarted_90", "!Z02LeonRingGrantAllowed", "!QuestStarted_54:3" )
leonBalanceComplete.setUrgent(true)

def balanceComplete1 = [id:1]
balanceComplete1.npctext = "With their majors gone, whatever organization the gnomes might have had in the north has fallen to ruin. Great work, %p."
balanceComplete1.options = []
balanceComplete1.options << [text:"Between the gnomes, flamingo sentries, and mushroom cannons I'm lucky to have survived.", result: 2]
balanceComplete1.options << [text:"No sweat. So, I guess my work here is done, right?", result: 3]
leonBalanceComplete.addDialog(balanceComplete1, Leon)

def balanceComplete2 = [id:2]
balanceComplete2.npctext = "You've been a huge help, %p. We've beaten the gnomes back to a safe distance from Barton. However, Remo is planning to storm the gnome fortress. Perhaps you can help him, with a crew of able bodied Gaians."
balanceComplete2.options = []
balanceComplete2.options << [text:"I think I'll get some friends together and go see Remo.", result:4]
balanceComplete2.options << [text:"Barton is safe. I think I'll take a well deserved break.", result:5]
leonBalanceComplete.addDialog(balanceComplete2, Leon)

def balanceComplete3 = [id:3]
balanceComplete3.npctext = "I suppose it is. We've beaten the gnomes back to a safe distance from Barton. However, Remo is planning to storm the gnome fortress. Perhaps you can help him, with a crew of able bodied Gaians."
balanceComplete3.options = []
balanceComplete3.options << [text:"I think I'll get some friends together and go see Remo.", result:4]
balanceComplete3.options << [text:"Barton is safe. I think I'll take a well-deserved break.", result:5]
leonBalanceComplete.addDialog(balanceComplete3, Leon)

def balanceComplete4 = [id:4]
balanceComplete4.npctext = "He'll need all the help he can get. But, before you head off, I'd like to offer you a ring as reward for your help."
balanceComplete4.exec = { event -> 
	event.player.addMiniMapQuestActorName("BFG-Remo")
	event.player.removeMiniMapQuestActorName( "Leon-VQS" )
	event.player.unsetQuestFlag(GLOBAL, "majorDisasterKilled")
	event.player.unsetQuestFlag(GLOBAL, "majorCatastropheKilled")
	event.player.unsetQuestFlag(GLOBAL, "majorTragedyKilled")
	event.player.updateQuest(89, "Leon-VQS")
	event.player.updateQuest(90, "Leon-VQS")
	event.player.deletePlayerVar("lieutenantCount")
	event.player.setQuestFlag( GLOBAL, "Z02LeonRingGrantAllowed" )
	Leon.pushDialog( event.player, "ringGrant" )
}
balanceComplete4.result = DONE
leonBalanceComplete.addDialog(balanceComplete4, Leon)

def balanceComplete5 = [id:5]
balanceComplete5.npctext = "Okay. Come back and speak with me when you're ready to help Remo. But before you go, I'd like to offer you a ring as reward for your help so far!"
balanceComplete5.flag = ["Z2_Leon_Remo_Break", "!majorDisasterKilled", "!majorCatastropheKilled", "!majorTragedyKilled"]
balanceComplete5.quest = 89
balanceComplete5.exec = { event ->
	event.player.deletePlayerVar("lieutenantCount")
	event.player.setQuestFlag( GLOBAL, "Z02LeonRingGrantAllowed" )
	event.player.removeMiniMapQuestActorName("Leon-VQS")
	Leon.pushDialog( event.player, "ringGrant" )
}
balanceComplete5.result = DONE
leonBalanceComplete.addDialog(balanceComplete5, Leon)

//--------------------------------------------------------------
//RING GRANT CONVERSATION                                       
//--------------------------------------------------------------
def ringGrant = Leon.createConversation( "ringGrant", true, "Z02LeonRingGrantAllowed", "!Z02FirstRingGrantReceived", "!QuestStarted_54:3" )

def grant1 = [id:1]
grant1.npctext = "This is the Bandage ring. You can use it to heal yourself or other Gaians."
grant1.result = 2
ringGrant.addDialog( grant1, Leon )

def grant2 = [id:2]
grant2.npctext = "Just target yourself or someone else, like you would a monster, and use it."
grant2.flag = "!Z02LeonRingGrantAllowed"
grant2.exec = { event ->
	player = event.player
	event.player.setQuestFlag( GLOBAL, "Z02FirstRingGrantReceived" )
	event.player.grantRing( "17732", 1, 5, true )
	//makeMainMenu( player )
}
grant2.result = DONE
ringGrant.addDialog(grant2, Leon)

//--------------------------------------------------------------
//REMO BREAK                                                    
//--------------------------------------------------------------
def leonRemoBreak = Leon.createConversation("leonRemoBreak", true, "Z2_Leon_Remo_Break", "!QuestStarted_86", "!QuestCompleted_86", "!QuestStarted_54:3" )

def leonRemoBreak1 = [id:1]
leonRemoBreak1.npctext = "Back from your respite and ready to help Remo out?"
leonRemoBreak1.options = []
leonRemoBreak1.options << [text:"Yes, I'm thoroughly rested and ready to go!", result: 2]
leonRemoBreak1.options << [text:"Nah, I'm still exhausted from the majors, Leon.", result: 3]
leonRemoBreak.addDialog(leonRemoBreak1, Leon)

def leonRemoBreak2 = [id:2]
leonRemoBreak2.npctext = "Great, let's get busy then. Finding Remo is easy enough, just follow the wall to the left and continue to do so when it turns south. That'll lead you right to the west gate, and Remo."
leonRemoBreak2.quest = 90
leonRemoBreak2.flag = "!Z2_Leon_Remo_Break"
leonRemoBreak2.exec = { event ->
	event.player.addMiniMapQuestActorName( "BFG-Remo" )
}
leonRemoBreak2.result = DONE
leonRemoBreak.addDialog(leonRemoBreak2, Leon)

def leonRemoBreak3 = [id:3]
leonRemoBreak3.npctext = "Very well. Return when you are rested."
leonRemoBreak3.result = DONE
leonRemoBreak.addDialog(leonRemoBreak3, Leon)

//--------------------------------------------------------------
//REMO ACTIVE                                                   
//--------------------------------------------------------------
def leonRemoStarted = Leon.createConversation("leonRemoStarted", true, "QuestStarted_90", "!Z02LeonRingGrantAllowed", "!QuestStarted_54:3" )

def leonRemoStarted1 = [id:1]
leonRemoStarted1.npctext = "Have you been to see Remo?"
leonRemoStarted1.options = []
leonRemoStarted1.options << [text:"Actually, I forgot where Remo was.", result: 3]
leonRemoStarted1.options << [text:"No, I was just heading over there!", result: DONE]
leonRemoStarted.addDialog(leonRemoStarted1, Leon)

def leonRemoStarted3 = [id:3]
leonRemoStarted3.npctext = "You will find him at the west gate. Follow the wall to the left and continue along it when it turns south. You can't miss him."
leonRemoStarted3.result = DONE
leonRemoStarted.addDialog(leonRemoStarted3, Leon)

//--------------------------------------------------------------
//REMO DONE                                                     
//--------------------------------------------------------------
def leonRemoDone = Leon.createConversation("leonRemoDone", true, "QuestCompleted_90", "!Z02LeonRingGrantAllowed", "!QuestStarted_54:3" )

def leonRemoDone1 = [id:1]
leonRemoDone1.npctext = "I have nothing more for you to do. Time to move on, %p."
leonRemoDone1.result = DONE
leonRemoDone.addDialog(leonRemoDone1, Leon)

//=========================================================
// FINDING ELIZABETH (start)                               
//=========================================================
def findLiz = Leon.createConversation( "findLiz", true, "!QuestStarted_249", "Z02FindElizabeth", "Z02LeonFlag", "!QuestStarted_54:3" )

def find1 = [id:1]
find1.npctext = "Great! Like I said, just head north on this road. If you get to Barton Town, you went too far."
find1.quest = 249  //start FINDING ELIZABETH
find1.flag = [ "!Z02LeonFlag" ]
find1.exec = { event ->
	event.player.removeMiniMapQuestActorName( "Leon-VQS" )
	event.player.addMiniMapQuestActorName( "BFG-Elizabeth" )
}
find1.result = DONE
findLiz.addDialog( find1, Leon )

//=========================================================
// FINDING ELIZABETH (interim)                             
//=========================================================
def findLizInterim = Leon.createConversation( "findLizInterim", true, "QuestStarted_249", "Z02LeonFlag", "!QuestStarted_54:3" )

def lizInt1 = [id:1]
lizInt1.npctext = "Oh, hi. Are you still looking for Elizabeth? Just head up the road toward Barton Town and watch on the right side of the road!"
lizInt1.playertext = "Okay! Got it. I'll keep looking."
lizInt1.flag = "!Z02LeonFlag"
lizInt1.result = DONE
findLizInterim.addDialog( lizInt1, Leon )

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 1
CLdecimal = 0

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["New Rings", "Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "New Rings" ) {
			makeNewRingMenu( player )
		}
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02RingRewardInProgress" )
		}
	}
}

//====================================================
// NEW RINGS                                          
//====================================================
def makeNewRingMenu( player ) {
	titleString = "New Rings Menu"
	descripString = "These rings are new to the list this time."
	diffOptions = [ "Diagnose", "Keen Aye", "Solar Rays", "Taunt", "Main Menu"  ]
	
	uiButtonMenu( player, "newRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
		if( event.selection == "Cancel" ) return
	}
}

//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Hornet Nest", "Hot Foot", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Quicksand", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Teflon Spray", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and theﾅprobability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Diagnose", "Meat", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fitness", "Keen Aye", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.setQuestFlag( GLOBAL, "Z02FirstRingGrantReceived" )
	event.actor.unsetQuestFlag(GLOBAL, "Z02LeonRingGrantAllowed")
}
