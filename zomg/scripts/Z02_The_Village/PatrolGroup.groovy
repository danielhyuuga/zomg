import com.gaiaonline.mmo.battle.script.*;


//Patrol Leader
patrolLeader = myRooms.VILLAGE_402.spawnSpawner( "patrolLeader", "lawn_gnome_LT", 1 )
patrolLeader.setPos(370, 400)
patrolLeader.setHomeTetherForChildren( 2000 )
patrolLeader.setHateRadiusForChildren( 2000 )
patrolLeader.setBaseSpeed( 160 )
patrolLeader.setChildrenToFollow( patrolLeader )
patrolLeader.setMonsterLevelForChildren( 1.5 )
patrolLeader.setCFH( 600 )
patrolLeader.addPatrolPointForSpawner( "VILLAGE_401", 650, 180, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_301", 300, 350, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_201", 250, 600, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_202", 120, 350, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_203", 270, 570, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_103", 350, 470, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_103", 720, 270, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_203", 680, 450, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_303", 260, 400, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_302", 530, 610, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_301", 900, 610, 0)
patrolLeader.addPatrolPointForSpawner( "VILLAGE_401", 960, 220, 0)
patrolLeader.startPatrol()

patrolLeader.stopSpawning()

infantryOne = myRooms.VILLAGE_402.spawnSpawner( "infantryOne", "lawn_gnome", 3 )
infantryOne.setPos(370, 400)
infantryOne.setHomeTetherForChildren( 2000 )
infantryOne.setHateRadiusForChildren( 2000 )
infantryOne.setCFH( 600 )
infantryOne.startFollow( patrolLeader )
infantryOne.setMonsterLevelForChildren( 1.5 )
infantryOne.setChildrenToFollow( patrolLeader )

infantryOne.stopSpawning()

//ALLIANCES
patrolLeader.allyWithSpawner( infantryOne )

leader = null
inf1 = null
inf2 = null
inf3 = null

def gnomePatrolSpawn() {
	if( leader == null || leader.isDead() ) {
		leader = patrolLeader.forceSpawnNow()
		leader.setDisplayName( "Kapitan Kaos" )
	}
	if( inf1 == null || inf1.isDead() ) { 
		inf1 = infantryOne.forceSpawnNow()
		inf1.setDisplayName( "Corp. Moe" )
	}
	if( inf2 == null || inf2.isDead() ) { 
		inf2 = infantryOne.forceSpawnNow()
		inf2.setDisplayName( "Corp. Larry" )
	}
	if( inf3 == null || inf3.isDead() ) { 
		inf3 = infantryOne.forceSpawnNow()
		inf3.setDisplayName( "Corp. Shemp" )
	}
	myManager.schedule( random(1200, 2400) ) { gnomePatrolSpawn() }
}

//Spawn the patrol group initially
gnomePatrolSpawn()

