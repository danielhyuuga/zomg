//Script created by gfern

import com.gaiaonline.mmo.battle.script.*

//Variables
allowCourier1Despawn = false
allowCourier2Despawn = false
allowCourier3Despawn = false

courier1InZone = false
courier2InZone = false
courier3InZone = false

//Define courier patrols
northeastCourierPatrol = makeNewPatrol()
northeastCourierPatrol.addPatrolPoint("VILLAGE_803", 400, 400, 0)
northeastCourierPatrol.addPatrolPoint("VILLAGE_703", 550, 250, 0)
northeastCourierPatrol.addPatrolPoint("VILLAGE_603", 485, 630, 0)
northeastCourierPatrol.addPatrolPoint("VILLAGE_603", 350, 115, 0)
northeastCourierPatrol.addPatrolPoint("VILLAGE_503", 545, 215, 5)
northeastCourierPatrol.addPatrolPoint("VILLAGE_503", 580, 120, 0)
northeastCourierPatrol.addPatrolPoint("VILLAGE_503", 720, 100, 10)

northwestCourierPatrol = makeNewPatrol();
northwestCourierPatrol.addPatrolPoint("VILLAGE_803", 400, 400, 0)
northwestCourierPatrol.addPatrolPoint("VILLAGE_703", 550, 250, 0)
northwestCourierPatrol.addPatrolPoint("VILLAGE_603", 485, 630, 0)
northwestCourierPatrol.addPatrolPoint("VILLAGE_503", 180, 550, 0)
northwestCourierPatrol.addPatrolPoint("VILLAGE_502", 650, 350, 0)
northwestCourierPatrol.addPatrolPoint("VILLAGE_502", 350, 115, 0)
northwestCourierPatrol.addPatrolPoint( "VILLAGE_501", 970, 155, 0)
northwestCourierPatrol.addPatrolPoint( "VILLAGE_501", 570, 275, 5)
northwestCourierPatrol.addPatrolPoint( "VILLAGE_501", 750, 210, 0 )
northwestCourierPatrol.addPatrolPoint( "VILLAGE_501", 780, 210, 10 )

northCourierPatrol = makeNewPatrol()
northCourierPatrol.addPatrolPoint("VILLAGE_903", 400, 400, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_703", 550, 250, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_603", 485, 630, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_503", 180, 550, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_502", 650, 350, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_502", 350, 115, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_402", 600, 615, 5)
northCourierPatrol.addPatrolPoint("VILLAGE_402", 455, 530, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_402", 430, 380, 0)
northCourierPatrol.addPatrolPoint("VILLAGE_402", 660, 425, 10)

//Define trigger zones for courier entry
courierDeliveryTrigger1 = "courierDeliveryTrigger1"
myRooms.VILLAGE_503.createTriggerZone(courierDeliveryTrigger1, 495, 165, 595, 265)

courierDeliveryTrigger2 = "courierDeliveryTrigger2"
myRooms.VILLAGE_501.createTriggerZone(courierDeliveryTrigger2, 520, 225, 620, 325)

courierDeliveryTrigger3 = "courierDeliveryTrigger3"
myRooms.VILLAGE_402.createTriggerZone(courierDeliveryTrigger3, 550, 565, 700, 665)

courierRemoveTrigger1 = "courierRemoveTrigger1"
myRooms.VILLAGE_503.createTriggerZone(courierRemoveTrigger1, 670, 50, 820, 150)

courierRemoveTrigger2 = "courierRemoveTrigger2"
myRooms.VILLAGE_501.createTriggerZone(courierRemoveTrigger2, 730, 160, 830, 260)

courierRemoveTrigger3 = "courierRemoveTrigger3"
myRooms.VILLAGE_402.createTriggerZone(courierRemoveTrigger3, 610, 375, 710, 475)

//Spawners
courier1Spawner = myRooms.VILLAGE_903.spawnSpawner("courier1Spawner", "lawn_gnome_courier", 1)
courier1Spawner.setPos(590, 170)
courier1Spawner.setSpawnWhenPlayersAreInRoom(true)
courier1Spawner.setMonsterLevelForChildren(1.4)

courier2Spawner = myRooms.VILLAGE_903.spawnSpawner("courier2Spawner", "lawn_gnome_courier", 1)
courier2Spawner.setPos(590, 170)
courier2Spawner.setSpawnWhenPlayersAreInRoom(true)
courier2Spawner.setMonsterLevelForChildren(1.4)

courier3Spawner = myRooms.VILLAGE_903.spawnSpawner("courier3Spawner", "lawn_gnome_courier", 1)
courier3Spawner.setPos(590, 170)
courier3Spawner.setSpawnWhenPlayersAreInRoom(true)
courier3Spawner.setMonsterLevelForChildren(1.4)

//Spawn control for couriers
def spawnCourier1() {
	courier1 = courier1Spawner.forceSpawnNow()
	courier1.setPatrol(northeastCourierPatrol)
	courier1.startPatrol()
	runOnDeath(courier1) { 
		courier1 = null
		//println "***COURIER DEBUG*** Caught death of courier1."
		myManager.schedule(60) {
			spawnCourier1()
		}
	} 
}

def spawnCourier2() {
	courier2 = courier2Spawner.forceSpawnNow()
	courier2.setPatrol(northwestCourierPatrol)
	courier2.startPatrol()
	runOnDeath(courier2) {
		courier2 = null
		//println "***COURIER DEBUG*** Caught death of courier2."
		myManager.schedule(60) {
			spawnCourier2()
		}
	} 
}

def spawnCourier3() {
	courier3 = courier3Spawner.forceSpawnNow()
	courier3.setPatrol(northCourierPatrol)
	courier3.startPatrol()
	runOnDeath(courier3) {
		courier3 = null
		//println "***COURIER DEBUG*** Caught death of courier3."
		myManager.schedule(60) {
			spawnCourier3()
		}
	} 
}

spawnCourier1()
spawnCourier2()
spawnCourier3()

myManager.onTriggerIn(myRooms.VILLAGE_503, courierDeliveryTrigger1) { event ->
	if(event.actor == courier1 && allowCourier1Despawn == false) {
		allowCourier1Despawn = true
	}
}

myManager.onTriggerIn(myRooms.VILLAGE_503, courierRemoveTrigger1) { event ->
	if(event.actor == courier1 && allowCourier1Despawn == true) {
		courier1InZone = true
		allowCourier1Despawn = false
		courier1.stopPatrol()
		myManager.schedule(10) {
			if(courier1 != null && courier1InZone == true && courier1.getHated().size() == 0) {
				event.actor.warp( "VILLAGE_903", 590, 170 )
				courier1.setPatrol(northeastCourierPatrol)
				courier1.startPatrol()
			}
		}
	}
}

myManager.onTriggerIn(myRooms.VILLAGE_501, courierDeliveryTrigger2) { event ->
	if(event.actor == courier2 && allowCourier2Despawn == false) {
		allowCourier2Despawn = true
	}
}

myManager.onTriggerIn(myRooms.VILLAGE_501, courierRemoveTrigger2) { event ->
	if(event.actor == courier2 && allowCourier2Despawn == true) {
		courier2InZone = true
		allowCourier2Despawn = false
		courier2.stopPatrol()
		myManager.schedule(10) {
			if(courier2 != null && courier2InZone == true && courier2.getHated().size() == 0) {
				event.actor.warp( "VILLAGE_903", 590, 170 )
				courier2.setPatrol(northwestCourierPatrol)
				courier2.startPatrol()
			}
		}
	}
}

myManager.onTriggerIn(myRooms.VILLAGE_402, courierDeliveryTrigger3) { event ->
	if(event.actor == courier3 && allowCourier3Despawn == false) {
		allowCourier3Despawn = true
	}
}

myManager.onTriggerIn(myRooms.VILLAGE_402, courierRemoveTrigger3) { event ->
	if(event.actor == courier3 && allowCourier3Despawn == true) {
		courier3InZone = true
		allowCourier3Despawn = false
		courier3.stopPatrol()
		myManager.schedule(10) {
			if(courier3 != null && courier3InZone == true && courier3.getHated().size() == 0){
				event.actor.warp( "VILLAGE_903", 590, 170 )
				courier3.setPatrol(northCourierPatrol)
				courier3.startPatrol()
			}
		}
	}
}

myManager.onTriggerOut(myRooms.VILLAGE_503, courierRemoveTrigger1) { event ->
	if(event.actor == courier1 && courier1InZone == true) {
		courier1InZone = false
	}
}

myManager.onTriggerOut(myRooms.VILLAGE_501, courierRemoveTrigger2) { event ->
	if(event.actor == courier2 && courier2InZone == true) {
		courier2InZone = false
	}
}	

myManager.onTriggerOut(myRooms.VILLAGE_402, courierRemoveTrigger3) { event ->
	if(event.actor == courier3 && courier3InZone == true) {
		courier3InZone = false
	}
}