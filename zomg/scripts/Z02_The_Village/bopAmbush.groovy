//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

gnome103_1Spawned = false
gnome103_2Spawned = false
gnome403_1Spawned = false
gnome403_2Spawned = false
gnome504_1Spawned = false
gnome504_2Spawned = false

//This script is for the BALANCE OF POWER quest
//It creates an ambush of lawn gnomes after player has killed the requisite 3 lieutenants
gnome103_1Spawner = myRooms.VILLAGE_103.spawnSpawner("gnome103_1Spawner", "lawn_gnome", 1)
gnome103_1Spawner.setPos(610, 500)
gnome103_1Spawner.setSpawnWhenPlayersAreInRoom(true)
gnome103_1Spawner.setMonsterLevelForChildren(1.5)
gnome103_1Spawner.stopSpawning()

gnome103_2Spawner = myRooms.VILLAGE_103.spawnSpawner("gnome103_2Spawner", "lawn_gnome", 1)
gnome103_2Spawner.setPos(650, 260)
gnome103_2Spawner.setSpawnWhenPlayersAreInRoom(true)
gnome103_2Spawner.setMonsterLevelForChildren(1.5)
gnome103_2Spawner.stopSpawning()

gnome403_1Spawner = myRooms.VILLAGE_403.spawnSpawner("gnome403_1Spawner", "lawn_gnome", 1)
gnome403_1Spawner.setPos(385, 420)
gnome403_1Spawner.setSpawnWhenPlayersAreInRoom(true)
gnome403_1Spawner.setMonsterLevelForChildren(1.5)
gnome403_1Spawner.stopSpawning()

gnome403_2Spawner = myRooms.VILLAGE_403.spawnSpawner("gnome403_2Spawner", "lawn_gnome", 1)
gnome403_2Spawner.setPos(890, 470)
gnome403_2Spawner.setSpawnWhenPlayersAreInRoom(true)
gnome403_2Spawner.setMonsterLevelForChildren(1.5)
gnome403_2Spawner.stopSpawning()

gnome504_1Spawner = myRooms.VILLAGE_504.spawnSpawner("gnome504_1Spawner", "lawn_gnome", 1)
gnome504_1Spawner.setPos(350, 465)
gnome504_1Spawner.setSpawnWhenPlayersAreInRoom(true)
gnome504_1Spawner.setMonsterLevelForChildren(1.5)
gnome504_1Spawner.stopSpawning()

gnome504_2Spawner = myRooms.VILLAGE_504.spawnSpawner("gnome504_2Spawner", "lawn_gnome", 1)
gnome504_2Spawner.setPos(540, 550)
gnome504_2Spawner.setSpawnWhenPlayersAreInRoom(true)
gnome504_2Spawner.setMonsterLevelForChildren(1.5)
gnome504_2Spawner.stopSpawning()

//Disposes gnome if not aggro on anything, schedules check again in 30 seconds if aggro
def gnome103_1Dispose() {
	//println "##### gnome103_1.getHated().size() = ${gnome103_1.getHated().size()} #####"
	if(gnome103_1Spawned == true && gnome103_1.getHated().size() == 0) {
		//println "##### Disposing gnome103_1 #####"
		gnome103_1.dispose()
		gnome103_1Spawned = false
	}
	if(gnome103_1Spawned == true && gnome103_1.getHated().size() > 0) {
		//println "##### Not disposing gnome103_2 #####"
		myManager.schedule(30) { gnome103_1Dispose() }
	}
}

def gnome103_2Dispose() {
	//println "##### gnome103_2.getHated().size() = ${gnome103_2.getHated().size()} #####"
	if(gnome103_2Spawned == true && gnome103_2.getHated().size() == 0) {
		//println "##### Disposing gnome103_2 #####"
		gnome103_2.dispose()
		gnome103_2Spawned = false
	}
	if(gnome103_2Spawned == true && gnome103_2.getHated().size() > 0) {
		//println "##### Not disposing gnome103_2 #####"
		myManager.schedule(30) { gnome103_2Dispose() }
	}
}

def gnome403_1Dispose() {
	if(gnome403_1Spawned == true && gnome403_1.getHated().size() == 0) {
		gnome403_1.dispose()
		gnome403_1Spawned = false
	}
	if(gnome403_1Spawned == true && gnome403_1.getHated().size() > 0) {
		myManager.schedule(30) { gnome403_1Dispose() }
	}
}

def gnome403_2Dispose() {
	if(gnome403_2Spawned == true && gnome403_2.getHated().size() == 0) {
		gnome403_2.dispose()
		gnome403_2Spawned = false
	}
	if(gnome403_2Spawned == true && gnome403_2.getHated().size() > 0) {
		myManager.schedule(30) { gnome403_2Dispose() }
	}
}

def gnome504_1Dispose() {
	if(gnome504_1Spawned == true && gnome504_1.getHated().size() == 0) {
		gnome504_1.dispose()
		gnome504_1Spawned = false
	}
	if(gnome504_1Spawned == true && gnome504_1.getHated().size() > 0) {
		myManager.schedule(30) { gnome504_1Dispose() }
	}
}

def gnome504_2Dispose() {
	if(gnome504_2Spawned == true && gnome504_2.getHated().size() == 0) {
		gnome504_2.dispose()
		gnome504_2Spawned = false
	}
	if(gnome504_2Spawned == true && gnome504_2.getHated().size() > 0) {
		myManager.schedule(30) { gnome504_2Dispose() }
	}
}

//Ambush in room 103
myManager.onEnter( myRooms.VILLAGE_103 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(89, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z2_BalanceOfPower_Ambushed") && gnome103_1Spawned == false && gnome103_2Spawned == false) {
		event.actor.setQuestFlag(GLOBAL, "Z2_BalanceOfPower_Ambushed")
		gnome103_1 = gnome103_1Spawner.forceSpawnNow()
		gnome103_1Spawned = true
		gnome103_2 = gnome103_2Spawner.forceSpawnNow()
		gnome103_2Spawned = true
		
		gnome103_1.addHate( event.actor, 1 )
		gnome103_2.addHate( event.actor, 1 )
		
		myManager.schedule(1) { gnome103_1.say("We will avenge the majors!") }
		myManager.schedule(2) { gnome103_2.say("You will pay, ${event.actor}.") }
		
		myManager.schedule(180) {
			gnome103_1Dispose()
		}
		myManager.schedule(180) {
			gnome103_2Dispose()
		}
		runOnDeath( gnome103_1 ) { gnome103_1Spawned = false }
		runOnDeath( gnome103_2 ) { gnome103_2Spawned = false }
		
	}
}

myManager.onEnter( myRooms.VILLAGE_403 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(89, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z2_BalanceOfPower_Ambushed") && gnome403_1Spawned == false && gnome403_2Spawned == false) {
		event.actor.setQuestFlag(GLOBAL, "Z2_BalanceOfPower_Ambushed")
		gnome403_1 = gnome403_1Spawner.forceSpawnNow()
		gnome403_1Spawned = true
		gnome403_2 = gnome403_2Spawner.forceSpawnNow()
		gnome403_2Spawned = true
		
		gnome403_1.addHate( event.actor, 1 )
		gnome403_2.addHate( event.actor, 1 )
		
		myManager.schedule(1) { gnome403_1.say("We will avenge the majors!") }
		myManager.schedule(2) { gnome403_2.say("You will pay, ${event.actor}.") }
		
		myManager.schedule(180) {
			gnome403_1Dispose()
		}
		myManager.schedule(180) {
			gnome403_2Dispose()
		}
		runOnDeath( gnome403_1 ) { gnome403_1Spawned = false }
		runOnDeath( gnome403_2 ) { gnome403_2Spawned = false }
		
	}
}

myManager.onEnter( myRooms.VILLAGE_504 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(89, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z2_BalanceOfPower_Ambushed") && gnome504_1Spawned == false && gnome504_2Spawned == false) {
		event.actor.setQuestFlag(GLOBAL, "Z2_BalanceOfPower_Ambushed")
		gnome504_1 = gnome504_1Spawner.forceSpawnNow()
		gnome504_1Spawned = true
		gnome504_2 = gnome504_2Spawner.forceSpawnNow()
		gnome504_2Spawned = true
		
		gnome504_1.addHate( event.actor, 1 )
		gnome504_2.addHate( event.actor, 1 )
		
		myManager.schedule(1) { gnome504_1.say("We will avenge the majors!") }
		myManager.schedule(2) { gnome504_2.say("You will pay, ${event.actor}.") }
		
		myManager.schedule(180) {
			gnome504_1Dispose()
		}
		myManager.schedule(180) {
			gnome504_2Dispose()
		}
		runOnDeath( gnome504_1 ) { gnome504_1Spawned = false }
		runOnDeath( gnome504_2 ) { gnome504_2Spawned = false }
		
	}
}