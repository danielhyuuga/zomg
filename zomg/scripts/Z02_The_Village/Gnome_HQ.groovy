import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// THE WINDMILL GARRISON & GUARDS           
//------------------------------------------

isResettingCamp = false

//Windmill Gnome Guards
windMillOneLeftSide = myRooms.VILLAGE_801.spawnStoppedSpawner( "windMillOneLeftSide", "lawn_gnome", 3 )
windMillOneLeftSide.setPos( 920, 530 )
windMillOneLeftSide.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
windMillOneLeftSide.setWaitTime( 50, 70 )
windMillOneLeftSide.setMonsterLevelForChildren( 1.8 )

def respawn801() {
	if( myRooms.VILLAGE_801.getSpawnTypeCount( "lawn_gnome" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			windMillOneLeftSide.spawnAllNow();
			myManager.schedule( 15 ) { respawn801() }
		}
	} else {
		myManager.schedule( 15 ) { respawn801() }
	}
}

windMillOneRightSide = myRooms.VILLAGE_802.spawnStoppedSpawner( "windMillOneRightSide", "lawn_gnome", 3 )
windMillOneRightSide.setPos( 90, 540 )
windMillOneRightSide.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
windMillOneRightSide.setHomeTetherForChildren( 2000 )
windMillOneRightSide.setHateRadiusForChildren( 2000 )
windMillOneRightSide.setWaitTime( 50, 70 )
windMillOneRightSide.setMonsterLevelForChildren( 1.8 )

def respawn802() {
	if( myRooms.VILLAGE_802.getSpawnTypeCount( "lawn_gnome" ) == 0 ) {
		myManager.schedule( random( 50, 70 ) ) { 
			windMillOneRightSide.spawnAllNow();
			myManager.schedule( 15 ) { respawn802() }
		}
	} else {
		myManager.schedule( 15 ) { respawn802() }
	}
}

def flamingoPen = myRooms.VILLAGE_801.spawnSpawner( "flamingoPen", "pink_flamingo", 4 )
flamingoPen.setPos( 350, 560 )
flamingoPen.setWanderBehaviorForChildren( 30, 100, 1, 7, 200)
flamingoPen.setWaitTime( 50, 70 )
flamingoPen.setCFH( 0 )
flamingoPen.setMonsterLevelForChildren( 1.8 )

//Individual Flamingo Guards that run to get help when aggroed
def windmillGuardOne = myRooms.VILLAGE_802.spawnSpawner( "windmillGuardOne", "pink_flamingo", 1 )
windmillGuardOne.setPos( 30, 350 )
windmillGuardOne.setHomeTetherForChildren( 2000 )
windmillGuardOne.setHateRadiusForChildren( 2000 )
windmillGuardOne.setWaitTime( 50, 70 )
windmillGuardOne.setHome( windMillOneRightSide )
windmillGuardOne.setGuardPostForChildren( "VILLAGE_902", 345, 245, 45 )
windmillGuardOne.setCFH( 400 )
windmillGuardOne.setMonsterLevelForChildren( 1.9 )

def windmillGuardTwo = myRooms.VILLAGE_802.spawnSpawner( "windmillGuardTwo", "pink_flamingo", 1 )
windmillGuardTwo.setPos( 30, 350 )
windmillGuardTwo.setHomeTetherForChildren( 2000 )
windmillGuardTwo.setHateRadiusForChildren( 2000 )
windmillGuardTwo.setWaitTime( 50, 70 )
windmillGuardTwo.setHome( windMillOneRightSide )
windmillGuardTwo.setGuardPostForChildren( "VILLAGE_902", 125, 335, 45 )
windmillGuardTwo.setCFH( 400 )
windmillGuardTwo.setMonsterLevelForChildren( 1.9 )

//WINDMILL ALLIANCES
windmillGuardOne.allyWithSpawner( windmillGuardTwo )
windmillGuardOne.allyWithSpawner( windMillOneRightSide )
windmillGuardTwo.allyWithSpawner( windMillOneRightSide )
windMillOneLeftSide.allyWithSpawner( windMillOneRightSide )
//Flamingo pen is not allied and may be safely ignored if not aggroed.

//-------------------------------------------
// HEADQUARTERS GENERALS, ADJUTANTS & GUARDS 
//-------------------------------------------

// The General!
hqGeneral = myRooms.VILLAGE_901.spawnStoppedSpawner( "hqGeneral", "lawn_gnome_demi", 1 )
hqGeneral.setPos( 640, 620 )
hqGeneral.setHomeTetherForChildren( 3000 )
hqGeneral.setHateRadiusForChildren( 3000 )
hqGeneral.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
hqGeneral.setMonsterLevelForChildren( 1.9 )

// General's Adjutants Camp
hqAdjutants = myRooms.VILLAGE_901.spawnStoppedSpawner( "hqAdjutants", "lawn_gnome_LT", 2 )
hqAdjutants.setPos( 640, 620 )
hqAdjutants.setHomeTetherForChildren( 3000 )
hqAdjutants.setHateRadiusForChildren( 3000 )
hqAdjutants.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
hqAdjutants.setMonsterLevelForChildren( 1.9 )

// Adjutant Body Guards
adjBodyGuards = myRooms.VILLAGE_901.spawnStoppedSpawner( "adjBodyGuards", "lawn_gnome", 3 )
adjBodyGuards.setPos( 640, 620 )
adjBodyGuards.setHomeTetherForChildren( 3000 )
adjBodyGuards.setHateRadiusForChildren( 3000 )
adjBodyGuards.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
adjBodyGuards.setMonsterLevelForChildren( 1.9 )

// HQ Roaming Gnomes
hqRoamers = myRooms.VILLAGE_901.spawnStoppedSpawner( "hqRoamers", "lawn_gnome", 4 )
hqRoamers.setPos( 240, 450 )
hqRoamers.setWanderBehaviorForChildren( 50, 150, 2, 5, 350)
hqRoamers.setHomeTetherForChildren( 3000 )
hqRoamers.setHateRadiusForChildren( 3000 )
hqRoamers.setMonsterLevelForChildren( 1.8 )
hqRoamers.enableForceSpawnFailureLogging()

// Southern Adjutants
def southAdjs = myRooms.VILLAGE_1001.spawnSpawner( "southAdjs", "lawn_gnome_LT", 3 )
southAdjs.setPos( 700, 120 )
southAdjs.setWanderBehaviorForChildren( 50, 150, 2, 5, 250)
southAdjs.setHomeTetherForChildren( 3000 )
southAdjs.setHateRadiusForChildren( 3000 )
southAdjs.setWaitTime( 50, 70 )
southAdjs.setMonsterLevelForChildren( 1.8 )

// South Entrance Guards
def hqSouthGuardOne = myRooms.VILLAGE_901.spawnSpawner( "hqSouthGuardOne", "pink_flamingo", 1 )
hqSouthGuardOne.setPos( 660, 370 )
hqSouthGuardOne.setHomeTetherForChildren( 2000 )
hqSouthGuardOne.setHateRadiusForChildren( 2000 )
hqSouthGuardOne.setWaitTime( 50, 70 )
hqSouthGuardOne.setHome( hqRoamers )
hqSouthGuardOne.setGuardPostForChildren( "VILLAGE_1001", 220, 230, 45 )
hqSouthGuardOne.setRFH( true )
hqSouthGuardOne.setCFH( 400 ) 
hqSouthGuardOne.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
hqSouthGuardOne.setCowardLevelForChildren( 100 )
hqSouthGuardOne.setMonsterLevelForChildren( 1.9 )

def hqSouthGuardTwo = myRooms.VILLAGE_901.spawnSpawner( "hqSouthGuardTwo", "pink_flamingo", 1 )
hqSouthGuardTwo.setPos( 660, 370 )
hqSouthGuardTwo.setHomeTetherForChildren( 2000 )
hqSouthGuardTwo.setHateRadiusForChildren( 2000 )
hqSouthGuardTwo.setWaitTime( 50, 70 )
hqSouthGuardTwo.setHome( hqRoamers )
hqSouthGuardTwo.setGuardPostForChildren( "VILLAGE_1001", 330, 175, 45 )
hqSouthGuardTwo.setRFH( true )
hqSouthGuardTwo.setCFH( 400 ) 
hqSouthGuardTwo.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
hqSouthGuardTwo.setCowardLevelForChildren( 100 )
hqSouthGuardTwo.setMonsterLevelForChildren( 1.9 )

// North Entrance Guards
hqNorthGuardOne = myRooms.VILLAGE_901.spawnStoppedSpawner( "hqNorthGuardOne", "pink_flamingo", 1 )
hqNorthGuardOne.setPos( 660, 370 )
hqNorthGuardOne.setHomeTetherForChildren( 2000 )
hqNorthGuardOne.setHateRadiusForChildren( 2000 )
hqNorthGuardOne.setWaitTime( 50, 70 )
hqNorthGuardOne.setHome( hqRoamers )
hqNorthGuardOne.setGuardPostForChildren( "VILLAGE_901", 840, 275, 315 )
hqNorthGuardOne.setRFH( true )
hqNorthGuardOne.setCFH( 400 ) 
hqNorthGuardOne.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
hqNorthGuardOne.setCowardLevelForChildren( 100 )
hqNorthGuardOne.setMonsterLevelForChildren( 1.9 )

hqNorthGuardTwo = myRooms.VILLAGE_901.spawnStoppedSpawner( "hqNorthGuardTwo", "pink_flamingo", 1 )
hqNorthGuardTwo.setPos( 660, 370 )
hqNorthGuardTwo.setHomeTetherForChildren( 2000 )
hqNorthGuardTwo.setHateRadiusForChildren( 2000 )
hqNorthGuardTwo.setWaitTime( 50, 70 )
hqNorthGuardTwo.setHome( hqRoamers )
hqNorthGuardTwo.setGuardPostForChildren( "VILLAGE_901", 970, 340, 315 )
hqNorthGuardTwo.setRFH( true )
hqNorthGuardTwo.setCFH( 400 ) 
hqNorthGuardTwo.setDispositionForChildren( "coward" ) //set up this guard as a "runner"
hqNorthGuardTwo.setCowardLevelForChildren( 100 )
hqNorthGuardTwo.setMonsterLevelForChildren( 1.9 )

//HQ ALLIANCES
hqGeneral.allyWithSpawner( hqAdjutants )
hqAdjutants.allyWithSpawner( adjBodyGuards )

hqRoamers.allyWithSpawner( hqGeneral )
hqRoamers.allyWithSpawner( hqAdjutants )
hqRoamers.allyWithSpawner( adjBodyGuards )
hqRoamers.allyWithSpawner( southAdjs )

southAdjs.allyWithSpawner( hqGeneral )
southAdjs.allyWithSpawner( hqAdjutants )
southAdjs.allyWithSpawner( adjBodyGuards )

hqNorthGuardOne.allyWithSpawner( hqNorthGuardTwo )
hqSouthGuardOne.allyWithSpawner( hqSouthGuardTwo )
hqNorthGuardOne.allyWithSpawner( hqRoamers )
hqNorthGuardTwo.allyWithSpawner( hqRoamers )
hqSouthGuardOne.allyWithSpawner( hqRoamers )
hqSouthGuardTwo.allyWithSpawner( hqRoamers )


//===========================================
//SPAWNING LOGIC                             
//===========================================

playerSet901 = [] as Set
roamerHateCollector = [] as Set
hateCollector = [] as Set
commandSet = [] as Set

CLCap = 2.5

autoDisposeDelay = 600 //the amount of time to leave a command group in existence without destroying them. If they exist longer than this, get rid of them.	

//Keep the Player List going for this room (enter and exit)
myManager.onEnter( myRooms.VILLAGE_901 ) { event ->
	if(isPlayer(event.actor)) {
		playerSet901 << event.actor
	}
}

myManager.onExit( myRooms.VILLAGE_901 ) { event ->
	if(isPlayer(event.actor)) {
		playerSet901.remove( event.actor )
	}
}

//After the Command Squad has been defeated, respawn the standard "default" camp setup once the players are gone from the room
def resetCamp() {
	hateCollector.clear()
	roamerHateCollector.clear()
	commandSet.clear()
	
	commandSpawnAlreadyDone = false
	commandGroupAuthorized = false
	generalDisposal = false
	
	roam1 = hqRoamers.forceSpawnNow()
	runOnDeath(roam1) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { roamerHateCollector << it } }; println "****GHQ: runOnDeath( roam1 ) ****"; checkQuestAndSpawn() }
	roam2 = hqRoamers.forceSpawnNow()
	runOnDeath(roam2) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { roamerHateCollector << it } }; println "****GHQ: runOnDeath( roam2 ) ****"; checkQuestAndSpawn() }
	roam3 = hqRoamers.forceSpawnNow()
	runOnDeath(roam3) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { roamerHateCollector << it } }; println "****GHQ: runOnDeath( roam3 ) ****"; checkQuestAndSpawn() }
	roam4 = hqRoamers.forceSpawnNow()
	runOnDeath(roam4) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { roamerHateCollector << it } }; println "****GHQ: runOnDeath( roam4 ) ****"; checkQuestAndSpawn() }

	guard1 = hqNorthGuardOne.forceSpawnNow()
	runOnDeath(guard1) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { roamerHateCollector << it } }; println "****GHQ: runOnDeath( guard1 ) ****"; checkQuestAndSpawn() }
	guard2 = hqNorthGuardTwo.forceSpawnNow()
	runOnDeath(guard2) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { roamerHateCollector << it } }; println "****GHQ: runOnDeath( guard2 ) ****"; checkQuestAndSpawn() }
	println "**** GHQ: PLACEHOLDERS HAVE SPAWNED ****"
	isResettingCamp = false
}

//Check to see if all default spawns are dead.
//If so, then either spawn the General and his staff (if someone in the room is on the quest)
//or a "sub-command" group instead (if no one in the room is on the quest).
def synchronized checkQuestAndSpawn() {
	println "**** GHQ: NUM OF PLACEHOLDERS STILL REMAINING = ${ hqRoamers.spawnsInUse() + hqNorthGuardOne.spawnsInUse() + hqNorthGuardTwo.spawnsInUse() } ****"
	if( commandSpawnAlreadyDone == false ) {
		commandSpawnAlreadyDone = true
		if( hqRoamers.spawnsInUse() + hqNorthGuardOne.spawnsInUse() + hqNorthGuardTwo.spawnsInUse() == 0 ) {
			sound("Claxon").toZone()
			
			spawnSubCommanders()

			//check to see if anyone is on the quest to spawn the General or not
			/*roamerHateCollector.clone().each{ 
				if( isPlayer( it ) && it.isOnQuest( 88, 2) ) {
					commandGroupAuthorized = true
				}
			}*/
			
			//spawn the appropriate alarm group, depending on the results of the above quest search
			/*if( commandGroupAuthorized == true ) {
				spawnCommandGroup()
				println "**** GHQ: Command Group is spawned ****"
			} else {
				spawnSubCommanders()
				println "**** GHQ: Sub-command Group is spawned ****"
			}*/
			//dispose of the alarm group after a period of time, if no one destroys them previous to that delay deadline
			deathTimer = myManager.schedule( autoDisposeDelay ) { autoDispose() }
			println "**** GHQ: SETTING deathTimer ****"
		} else {
			commandSpawnAlreadyDone = false
		}
	}
}

//This spawns the full command group, if someone in the room is on the quest (checked in previous routine)
def spawnCommandGroup() {
	myManager.schedule(2) {
		Adj1 = hqAdjutants.forceSpawnNow()
		Adj1.setDisplayName( "Major Fiasco" )
		if( !playerSet901.isEmpty() ) {
			Adj1.addHate( random(playerSet901), 1 )
			Adj1.say("You've had it now! You've woken up the General!")
		}
		commandSet << Adj1
		runOnDeath(Adj1) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { hateCollector << it } }; println "****GHQ: runOnDeath( Adj1 ), command ****"; checkForReset() }
	}
	//if( playerSet901.size() > 3 ) {
	myManager.schedule(3) {
		Adj2 = hqAdjutants.forceSpawnNow()
		Adj2.setDisplayName( "Major Snafu" )
		if( !playerSet901.isEmpty() ) {
			Adj2.addHate( random(playerSet901), 1 )
			Adj2.say("Make way! Make way! The General's awake!")
		}
		commandSet << Adj2
		runOnDeath(Adj2) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { hateCollector << it } }; println "****GHQ: runOnDeath( Adj2 ), command ****"; checkForReset() }
	}
	//}
	myManager.schedule(6) {
		general = hqGeneral.forceSpawnNow()
		general.setDisplayName( "General Mayhem" )
		if( !playerSet901.isEmpty() ) {
			general.addHate( random(playerSet901), 1 )
			general.say("I've na'had me coffee and I'm grumpy as hell. Who wants to die?")
		}
		commandSet << general
		
		myManager.onHealth( general ) { event ->
			if( event.didTransition( 66 ) && event.isDecrease() ) {
				general.say("Faugh. Tis but a scratch. I've had worse from MacTaggert's weed wacker!")
			} else if( event.didTransition( 33 ) && event.isDecrease() ) {
				general.say("Y'pack a mean punch, but y've not beaten me yet! Have at ya!")
			}
		}
		runOnDeath(general) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { hateCollector << it } }; println "****GHQ: runOnDeath( general ), command ****"; checkForReset() }
	}
}

//This spawns the "sub-command" group if no one in the room is on the quest.
def spawnSubCommanders() {
	myManager.schedule(2) {
		body1 = adjBodyGuards.forceSpawnNow()
		body1.setDisplayName( "Private Pulp" )
		commandSet << body1
		myManager.schedule(1) {
			if( !playerSet901.isEmpty() ) {
				body1.addHate( random(playerSet901), 1 )
				body1.say("Time to do some lawn bowling!")
			}
		}
		runOnDeath(body1) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { hateCollector << it } }; println "****GHQ: runOnDeath( body1 ), sub-command ****"; checkForReset() }
	}
	if( playerSet901.size() > 2 ) {
		myManager.schedule(3) {
			body2 = adjBodyGuards.forceSpawnNow()
			body2.setDisplayName( "Corporal Punishment" )
			commandSet << body2
			myManager.schedule(1) {
				if( !playerSet901.isEmpty() ) {
					body2.addHate( random(playerSet901), 1 )
					body2.say("Let's get 'em!")
				}
			}
			runOnDeath(body2) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { hateCollector << it } }; println "****GHQ: runOnDeath( body2 ), sub-command ****"; checkForReset() }
		}
	}
	if( playerSet901.size() > 4 ) {
		myManager.schedule(4) {
			body3 = adjBodyGuards.forceSpawnNow()
			body3.setDisplayName( "Sarge Slaughter" )
			commandSet << body3
			myManager.schedule(1) {
				if( !playerSet901.isEmpty() ) {
					body3.addHate( random(playerSet901), 1 )
					body3.say("Show 'em this is OUR turf now!")
				}
			}
			runOnDeath(body3) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { hateCollector << it } }; println "****GHQ: runOnDeath( body3 ), sub-command ****"; checkForReset() }
		}
	}
	myManager.schedule(5) {
		Adj1 = hqAdjutants.forceSpawnNow()
		Adj1.setDisplayName( "Major Major" )
		commandSet << Adj1
		if( !playerSet901.isEmpty() ) {
			Adj1.addHate( random(playerSet901), 1 )
			Adj1.say("Kill 'em quick before the General wakes up!")
		}
		runOnDeath(Adj1) { event -> event.actor.getHated().each{ if( isPlayer( it ) ) { hateCollector << it } }; println "****GHQ: runOnDeath( Adj1 ), sub-command ****"; checkForReset() }
	}
}

//Don't let the bad guys live too long if no one is around and too much time has gone by
def autoDispose() {
	if( generalDisposal == false && hqAdjutants.spawnsInUse() + hqGeneral.spawnsInUse() + adjBodyGuards.spawnsInUse() > 0 ) {
		generalDisposal = true
		stopSound("Claxon").toZone()
		commandSet.each{
			if( !it.isDead() ) {
				it.instantPercentDamage( 100 )
			}
		}	
		println "**** GHQ: command or sub-command group has been destroyed from autoDispose ****"
		myManager.schedule( random( 90, 150 ) ) { println "**** GHQ: Respawning Placeholders from autoDispose ****"; resetCamp() } 
	}
}

//After all members of either the Sub-Command or Command groups are defeated, 
def synchronized checkForReset() {
	println "**** GHQ: NUM OF CMD/SUB-CMD GROUP REMAINING = ${ hqAdjutants.spawnsInUse() + hqGeneral.spawnsInUse() + adjBodyGuards.spawnsInUse() } ****"
	if( isResettingCamp == false && generalDisposal == false  && hqAdjutants.spawnsInUse() + hqGeneral.spawnsInUse() + adjBodyGuards.spawnsInUse() == 0) {
		stopSound("Claxon").toZone()
		//cancel the "deathTimer" if the players killed the guards legitimately
		deathTimer.cancel()
		println "**** GHQ: CANCELING deathTimer (command or sub-command group is already DEAD) ****"
		isResettingCamp = true
		myManager.schedule(random(90, 120)) { println "**** GHQ: Respawning Placeholders from checkForReset ****"; resetCamp() }
	}
}


//SERVER STARTUP SPAWN COMMANDS
windMillOneLeftSide.spawnAllNow()
windMillOneRightSide.spawnAllNow()

respawn801()
respawn802()

flamingoPen.spawnAllNow()
windmillGuardOne.spawnAllNow()
windmillGuardTwo.spawnAllNow()
southAdjs.spawnAllNow()
hqSouthGuardOne.spawnAllNow()
hqSouthGuardTwo.spawnAllNow()

resetCamp() //start the sequence by setting up the default spawn of the camp
