import com.gaiaonline.mmo.battle.script.*;

def Dina = spawnNPC("BFG-Dina", myRooms.VILLAGE_406, 500, 230)
Dina.setDisplayName( "Dina" )

//---------------------------------------------------------
// Default Conversation                                           
//---------------------------------------------------------

def dinaDefault = Dina.createConversation( "dinaDefault", true )

def dina1 = [id:1]
dina1.npctext = "Hi there, citizen! It's good to see you here. Are you headed into the suburban Towns, or up to Barton itself?"
dina1.options = []
dina1.options << [text:"What are the Towns?", result: 2]
dina1.options << [text:"I'm going up to Barton Town.", result: 3]
dina1.options << [text:"Neither. I'm just looking around the area.", result: 5]
dina1.options << [text:"Do you have any freebies available today?", exec: { event ->
	checkDailyChance(event.player, 222, { result -> 
		//println "**** result = ${ result } ****"
		if( result == false ) {
			runDailyChance(event.player, 222)
			Dina.say("Of course. Compliments of the Barton Regulars.")
		} else {
			event.player.centerPrint( "You've already received your freebie from Dina today. Try again tomorrow!" )
		}
	})
}, result: DONE]
dinaDefault.addDialog(dina1, Dina)

def dina2 = [id:2]
dina2.npctext = "The 'Towns' area is a walled area where Gaians can establish their own residence, free of charge. The area's been booming and there are thousands of houses in there!"
dina2.playertext = "Thanks. Maybe I'll check it out soon."
dina2.result = DONE
dinaDefault.addDialog(dina2, Dina)

def dina3 = [id:3]
dina3.npctext = "Okay! Good luck up there! Tell Clara I said it's all clear here, would you?"
dina3.playertext = "Sure. If I see her."
dina3.flag = "Z2DinaFavor"
dina3.result = 4
dinaDefault.addDialog(dina3, Dina)

def dina4 = [id:4]
dina4.npctext = "That's all I ask. Good day, citizen!"
dina4.result = DONE
dinaDefault.addDialog(dina4, Dina)

def dina5 = [id:5]
dina5.npctext = "Okay...but watch yourself. They may *look* silly, but those lawn gnomes out there mean business. They're the most organized of the Animated we've seen so far and they're really aggressive. If you head down to the west gate of the 'Towns' area, be sure to check with Remo. He's been out on multiple excursions with Leon and he knows a lot more about them."
dina5.playertext = "Thanks!"
dina5.result = DONE
dinaDefault.addDialog(dina5, Dina)

dinaDefault.setUrgent(false)