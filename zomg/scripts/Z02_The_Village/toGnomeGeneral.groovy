import com.gaiaonline.mmo.battle.script.*;


//---------------------------------------------------------
// SWITCH FOR GENERAL                                      
//---------------------------------------------------------
timerMap = new HashMap()
offlineResetSet = [] as Set

toGeneral = makeGeneric("DoorOutside", myRooms.VILLAGE_901, 650, 520)
toGeneral.setStateful()
toGeneral.setUsable(true)
toGeneral.setMouseoverText("General Mayhem's Quarters")
toGeneral.setRange(200)

toGeneral.onUse() { event ->
	if(isPlayer(event.player)) {
		//Check that the player, or a crewmate, is on or has done Gnome General quest
		event.player.getCrew().each() {
			if(it.isOnQuest(88) || it.isDoneQuest(88) && !event.player.hasQuestFlag(GLOBAL, "Z02_General_AllowEntry")) {
				event.player.setQuestFlag(GLOBAL, "Z02_General_AllowEntry")
			}
		}
		if(event.player.hasQuestFlag(GLOBAL, "Z02_General_AllowEntry")) {
			//Check that none of the crew are already in General instance
			if(event.player.getTeam().hasAreaVar("Z32_Village_General", "Z32_General_Difficulty") == false) {
				if(event.player.getCrewVar("Z32_General_DifficultySelectionInProgress") == 0) {
					//Set crew var to prevent multiple difficulty selection menus
					event.player.setCrewVar("Z32_General_DifficultySelectionInProgress", 1)
				
					//Make sure the crew leader is in Village
					if(isInZone(event.player.getTeam().getLeader())) {
						event.player.unsetQuestFlag(GLOBAL, "Z02_General_AllowEntry")
					
						//Set leader and give them the menu
						leader = event.player.getTeam().getLeader()
						makeMenu()
					
						//Message the crew that the leader is choosing difficulty
						event.player.getCrew().each() {
							if(it != leader) { it.centerPrint("Your Crew Leader is choosing the challenge level for General Mayhem's Quarters.") }
						}
					
						//Schedule decision timeout
						decisionTimer = myManager.schedule(30) {
							event.player.getCrew().each() { it.centerPrint("No choice was made within 30 seconds. Click on General Mayhem's Quarters again.") }
					
							event.player.setCrewVar("Z32_General_DifficultySelectionInProgress", 0)
							timerMap.remove(event.player.getTeam().getLeader())
						}
						timerMap.put(event.player.getTeam().getLeader(), decisionTimer)
					} else {
						event.player.getCrew().each() {
							if(it != event.player.getTeam().getLeader()) {
								it.centerPrint("Your Crew Leader must be in Bill's Ranch before you can enter General Mayhem's Quarters.")
							} else {
								it.centerPrint("Your Crew is trying to enter General Mayhem's Quarters. You must be in Bill's Ranch for this.")
							}
						}
						event.player.setCrewVar("Z32_General_DifficultySelectionInProgress", 0)
					}
				} else if(event.player.getCrewVar("Z32_General_DifficultySelectionInProgress") == 1) {
					if(event.player != event.player.getTeam().getLeader()) {
						event.player.centerPrint("Your Crew Leader is already making a challenge level choice. One moment, please...")
					}
				}
			} else {
				event.player.centerPrint("The scent of mud and freshly cut grass assaults you as you enter.")
				event.player.unsetQuestFlag(GLOBAL, "Z02_General_AllowEntry")
				event.player.warp("VillageGeneral_1", 400, 440, 6)
			}
		} else {
			event.player.centerPrint("You slam your shoulder against the door, but it's locked tight.")
		}
	}
}

def synchronized makeMenu() {
	descripString = "You must choose a Challenge Level to enter General Mayhem's Quarters. You have 30 seconds to decide."
	diffOptions = ["Easy", "Normal", "Hard", "Cancel"]
	
	//println "**** trying to create the menu ****"
	uiButtonMenu( leader, "diffMenu", descripString, diffOptions, 300, 30 ) { event ->
		if( event.selection == "Easy" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Easy' challenge setting for this scenario. Click General Mayhem's Quarters to enter." )
				} else {
					it.centerPrint( "You chose the 'Easy' challenge setting for this scenario. Click General Mayhem's Quarters to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z32_Village_General", "Z32_General_Difficulty", 1 )
			event.actor.setCrewVar( "Z32_General_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Normal" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Normal' challenge setting for this scenario. Click General Mayhem's Quarters to enter." )
				} else {
					it.centerPrint( "You chose the 'Normal' challenge setting for this scenario. Click General Mayhem's Quarters to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z32_Village_General", "Z32_General_Difficulty", 2 )
			event.actor.setCrewVar( "Z32_General_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Hard" ) {
			event.actor.getCrew().each{
				if( it != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose the 'Hard' challenge setting for this scenario. Click General Mayhem's Quarters to enter." )
				} else {
					it.centerPrint( "You chose the 'Hard' challenge setting for this scenario. Click General Mayhem's Quarters to enter." )
				}
			}
			event.actor.getTeam().setAreaVar( "Z32_Village_General", "Z32_General_Difficulty", 3 )
			event.actor.setCrewVar( "Z32_General_DifficultySelectionInProgress", 0 )
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
		if( event.selection == "Cancel" ) {
			event.actor.getCrew().each{
				if( event.actor != event.actor.getTeam().getLeader() ) {
					it.centerPrint( "Your Crew Leader chose to not select a challenge setting at this time." )
				}
			}
			event.actor.setCrewVar("Z32_General_DifficultySelectionInProgress", 0)
			timer = timerMap.get( event.actor )
			timer.cancel()
			timerMap.remove( event.actor )
		}
	}
}