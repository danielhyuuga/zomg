
import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// SAND TRAP FLUFF SPAWNERS                 
//------------------------------------------

sandTrapSpawner202 = myRooms.VILLAGE_202.spawnSpawner( "sandTrapSpawner202", "sandtrap_fluff", 3 )
sandTrapSpawner202.setPos( 330, 430 )
sandTrapSpawner202.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner202.setWaitTime( 15, 25 )
sandTrapSpawner202.setMonsterLevelForChildren( 1.2 )

sandTrapSpawner203 = myRooms.VILLAGE_203.spawnSpawner( "sandTrapSpawner203", "sandtrap_fluff", 3 )
sandTrapSpawner203.setPos( 430, 195 )
sandTrapSpawner203.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
sandTrapSpawner203.setWaitTime( 15, 25 )
sandTrapSpawner203.setMonsterLevelForChildren( 1.1 )

sandTrapSpawner301 = myRooms.VILLAGE_301.spawnSpawner( "sandTrapSpawner301", "sandtrap_fluff", 3 )
sandTrapSpawner301.setPos( 640, 160 )
sandTrapSpawner301.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
sandTrapSpawner301.setWaitTime( 15, 25 )
sandTrapSpawner301.setMonsterLevelForChildren( 1.4 )

sandTrapSpawner302A = myRooms.VILLAGE_302.spawnSpawner( "sandTrapSpawner302A", "sandtrap_fluff", 3 )
sandTrapSpawner302A.setPos( 305, 450 )
sandTrapSpawner302A.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner302A.setWaitTime( 15, 25 )
sandTrapSpawner302A.setMonsterLevelForChildren( 1.3 )

sandTrapSpawner302B = myRooms.VILLAGE_302.spawnSpawner( "sandTrapSpawner302B", "sandtrap_fluff", 3 )
sandTrapSpawner302B.setPos( 820, 540 )
sandTrapSpawner302B.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner302B.setWaitTime( 15, 25 )
sandTrapSpawner302B.setMonsterLevelForChildren( 1.3 )

sandTrapSpawner302C = myRooms.VILLAGE_302.spawnSpawner( "sandTrapSpawner302C", "sandtrap_fluff", 2 )
sandTrapSpawner302C.setPos( 990, 150 )
sandTrapSpawner302C.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner302C.setWaitTime( 15, 25 )
sandTrapSpawner302C.setMonsterLevelForChildren( 1.3 )

sandTrapSpawner303 = myRooms.VILLAGE_303.spawnSpawner( "sandTrapSpawner303", "sandtrap_fluff", 2 )
sandTrapSpawner303.setPos( 100, 125 )
sandTrapSpawner303.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandTrapSpawner303.setWaitTime( 15, 25 )
sandTrapSpawner303.setMonsterLevelForChildren( 1.2 )
