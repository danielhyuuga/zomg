//Script created by gfern

import com.gaiaonline.mmo.battle.script.*;

//This script is for the AN ADVANCING ENEMY quest
//It updates the quest when players enter the rooms containing the 3 gnomish outposts


//Updates for Room 503
myManager.onEnter( myRooms.VILLAGE_503 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 2) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost503")) {
		event.actor.centerPrint("First Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost503")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment East" )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost503")) {
		event.actor.centerPrint("Second Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost503")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment East" )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 4) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost503")) {
		event.actor.centerPrint("Final Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost503")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment East" )
	}
}

//Updates for Room 402
myManager.onEnter( myRooms.VILLAGE_402 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 2) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost402")) {
		event.actor.centerPrint("First Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost402")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment North" )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost402")) {
		event.actor.centerPrint("Second Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost402")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment North" )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 4) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost402")) {
		event.actor.centerPrint("Final Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost402")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment North" )
	}
}

//Updates for Room 501
myManager.onEnter( myRooms.VILLAGE_501 ) { event ->
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 2) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost501")) {
		event.actor.centerPrint("First Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost501")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment West" )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 3) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost501")) {
		event.actor.centerPrint("Second Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost501")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment West" )
	}
	if(isPlayer(event.actor) && event.actor.isOnQuest(78, 4) && !event.actor.hasQuestFlag(GLOBAL, "Z2_GnomeOutpost501")) {
		event.actor.centerPrint("Final Lawn Gnome Encampment scouted.")
		event.actor.updateQuest( 78, "Leon-VQS" )
		event.actor.setQuestFlag(GLOBAL, "Z2_GnomeOutpost501")
		event.actor.removeMiniMapQuestLocation( "Gnomish Encampment West" )
	}
}