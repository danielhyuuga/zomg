import com.gaiaonline.mmo.battle.script.*;

//------------------------------------------
// GOLF GREENS FLUFF SPAWNERS               
//------------------------------------------

def greensSpawnerOne = myRooms.VILLAGE_401.spawnSpawner( "greensSpawnerOne", "greens_fluff", 3 )
greensSpawnerOne.setPos( 670, 206 )
greensSpawnerOne.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
greensSpawnerOne.setWaitTime( 50, 70 )
greensSpawnerOne.setMonsterLevelForChildren( 1.5 )

def greensSpawnerTwo = myRooms.VILLAGE_201.spawnSpawner( "greensSpawnerTwo", "greens_fluff", 3 )
greensSpawnerTwo.setPos( 480, 606 )
greensSpawnerTwo.setWanderBehaviorForChildren( 25, 100, 2, 5, 250)
greensSpawnerTwo.setWaitTime( 50, 70 )
greensSpawnerTwo.setMonsterLevelForChildren( 1.3 )

def greensSpawnerThree = myRooms.VILLAGE_202.spawnSpawner( "greensSpawnerThree", "greens_fluff", 3 )
greensSpawnerThree.setPos( 825, 600 )
greensSpawnerThree.setWanderBehaviorForChildren( 25, 100, 2, 5, 250)
greensSpawnerThree.setWaitTime( 50, 70 )
greensSpawnerThree.setMonsterLevelForChildren( 1.2 )

def greensSpawnerFour = myRooms.VILLAGE_103.spawnSpawner( "greensSpawnerFour", "greens_fluff", 3 )
greensSpawnerFour.setPos( 673, 272 )
greensSpawnerFour.setWanderBehaviorForChildren( 25, 100, 2, 5, 250)
greensSpawnerFour.setWaitTime( 50, 70 )
greensSpawnerFour.setMonsterLevelForChildren( 1.1)

//------------------------------------------
// PATROLLING SAND FLUFFS                   
//------------------------------------------

sandtrapPatrol202 = myRooms.VILLAGE_202.spawnSpawner( "sandtrapPatrol202", "sandtrap_fluff", 1 )
sandtrapPatrol202.setPos( 330, 430 )
sandtrapPatrol202.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandtrapPatrol202.setWaitTime( 50, 70 )
sandtrapPatrol202.setMonsterLevelForChildren( 1.2 )
sandtrapPatrol202.addPatrolPointForChildren( "VILLAGE_202", 95, 470, 1 )
sandtrapPatrol202.addPatrolPointForChildren( "VILLAGE_202", 560, 400, 1 )

sandtrapPatrol203 = myRooms.VILLAGE_203.spawnSpawner( "sandtrapPatrol203", "sandtrap_fluff", 1 )
sandtrapPatrol203.setPos( 430, 195 )
sandtrapPatrol203.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
sandtrapPatrol203.setWaitTime( 50, 70 )
sandtrapPatrol203.setMonsterLevelForChildren( 1.1 )
sandtrapPatrol203.addPatrolPointForChildren( "VILLAGE_203", 550, 280, 1 )
sandtrapPatrol203.addPatrolPointForChildren( "VILLAGE_203", 300, 145, 1 )

sandtrapPatrol301 = myRooms.VILLAGE_301.spawnSpawner( "sandtrapPatrol301", "sandtrap_fluff", 1 )
sandtrapPatrol301.setPos( 640, 160 )
sandtrapPatrol301.setWanderBehaviorForChildren( 25, 100, 2, 5, 200)
sandtrapPatrol301.setWaitTime( 50, 70 )
sandtrapPatrol301.setMonsterLevelForChildren( 1.4 )
sandtrapPatrol301.addPatrolPointForChildren( "VILLAGE_301", 460, 170, 1 )
sandtrapPatrol301.addPatrolPointForChildren( "VILLAGE_301", 845, 130, 1 )

sandtrapPatrol302A = myRooms.VILLAGE_302.spawnSpawner( "sandtrapPatrol302A", "sandtrap_fluff", 1 )
sandtrapPatrol302A.setPos( 305, 450 )
sandtrapPatrol302A.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandtrapPatrol302A.setWaitTime( 50, 70 )
sandtrapPatrol302A.setMonsterLevelForChildren( 1.3 )
sandtrapPatrol302A.addPatrolPointForChildren( "VILLAGE_302", 290, 550, 1 )
sandtrapPatrol302A.addPatrolPointForChildren( "VILLAGE_302", 310, 375, 1 )

sandtrapPatrol302B = myRooms.VILLAGE_302.spawnSpawner( "sandtrapPatrol302B", "sandtrap_fluff", 1 )
sandtrapPatrol302B.setPos( 820, 540 )
sandtrapPatrol302B.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandtrapPatrol302B.setWaitTime( 50, 70 )
sandtrapPatrol302B.setMonsterLevelForChildren( 1.3 )
sandtrapPatrol302B.addPatrolPointForChildren( "VILLAGE_302", 685, 610, 1 )
sandtrapPatrol302B.addPatrolPointForChildren( "VILLAGE_302", 980, 450, 1 )

sandtrapPatrol302C = myRooms.VILLAGE_302.spawnSpawner( "sandtrapPatrol302C", "sandtrap_fluff", 1 )
sandtrapPatrol302C.setPos( 990, 150 )
sandtrapPatrol302C.setWanderBehaviorForChildren( 25, 75, 2, 5, 150)
sandtrapPatrol302C.setWaitTime( 50, 70 )
sandtrapPatrol302C.setMonsterLevelForChildren( 1.3 )
sandtrapPatrol302C.addPatrolPointForChildren( "VILLAGE_302", 910, 205, 1 )
sandtrapPatrol302C.addPatrolPointForChildren( "VILLAGE_303", 240, 90, 1 )

// INITIAL LOGIC

greensSpawnerOne.spawnAllNow()
greensSpawnerTwo.spawnAllNow()
greensSpawnerThree.spawnAllNow()
greensSpawnerFour.spawnAllNow()
