import com.gaiaonline.mmo.battle.script.*;

Remo = spawnNPC("BFG-Remo", myRooms.VILLAGE_704, 780, 504)
Remo.setDisplayName( "Remo" )

onQuestStep( 86, 2 ) { event -> event.player.addMiniMapQuestActorName( "BFG-Remo" ) }

onQuestStep( 87, 2 ) { event -> event.player.addMiniMapQuestActorName( "BFG-Remo" ) }

myManager.onEnter( myRooms.VILLAGE_704 ) { event ->
	if( isPlayer( event.actor ) ) {
		event.actor.unsetQuestFlag( GLOBAL, "Z02RingRewardInProgress" )
		if(event.actor.isOnQuest(88) && event.actor.hasQuestFlag(GLOBAL, "Z02SecondRingGrantReceived")) {
			event.actor.unsetQuestFlag(GLOBAL, "Z02SecondRingGrantReceived")
		}
		if(event.actor.isOnQuest(88) && event.actor.hasQuestFlag(GLOBAL, "Z02RingRewardInProgress")) {
			event.actor.unsetQuestFlag(GLOBAL, "Z02RingRewardInProgress")
		}
	}
}

//---------------------------------------------------------
// Default Conversation (before completing Leon quests)    
//---------------------------------------------------------

def remoDefault = Remo.createConversation( "remoDefault", true, "!QuestStarted_90", "!QuestCompleted_90" )

def rDef1 = [id:1]
rDef1.npctext = "Good day, citizen. Watch yourself down here. Things are a lot more brutal than they are up by Barton Town."
rDef1.playertext = "Oh? How so?"
rDef1.result = 2
remoDefault.addDialog( rDef1, Remo )

def rDef2 = [id:2]
rDef2.npctext = "They're crazy busy down here, like ants after you poke their anthill, constantly moving and pretty well organized."
rDef2.playertext = "Is that why you're guarding the Town gate here?"
rDef2.result = 3
remoDefault.addDialog( rDef2, Remo )

def rDef3 = [id:3]
rDef3.npctext = "You bet. The gnomes don't seem too interested in raiding the Towns yet...but I'm sure it's just a matter of time. I'll be here to slow them down as much as I can if that happens."
rDef3.playertext = "Sounds grim. Is there some way I can help?"
rDef3.result = 4
remoDefault.addDialog( rDef3, Remo )

def rDef4 = [id:4]
rDef4.npctext = "Head up north a bit, near the other gate to Towns, and talk to Commander Leon. I'm sure he can think of some stuff for you to do!"
rDef4.playertext = "Okay. I'll do that. Thanks!"
rDef4.result = 5
remoDefault.addDialog( rDef4, Remo )

def rDef5 = [id:5]
rDef5.npctext = "Good luck to you! Stay sharp!"
rDef5.result = DONE
remoDefault.addDialog( rDef5, Remo )

//---------------------------------------------------------
// remoQuest Conversation                                  
//---------------------------------------------------------

def remoQuestQuest = Remo.createConversation( "remoQuestQuest", true, "QuestStarted_90", "!QuestStarted_86", "!QuestCompleted_86" )
remoQuestQuest.setUrgent(true)

def remoQuest1 = [id:1]
remoQuest1.npctext = "Hey there, citizen."
remoQuest1.playertext = "Hey. How's it going over here?"
remoQuest1.result = 2
remoQuestQuest.addDialog(remoQuest1, Remo)

def remoQuest2 = [id:2]
remoQuest2.npctext = "Well...we're safe here by the gate for some reason, but it's wild out there."
remoQuest2.playertext = "Yeah?"
remoQuest2.result = 3
remoQuestQuest.addDialog(remoQuest2, Remo)

def remoQuest3 = [id:3]
remoQuest3.npctext = "Yeah. It's bad enough that the Lawn Gnomes are all over the place, but they're also organizing outposts and headquarters out there in the Village Greens."
remoQuest3.playertext = "I know. I was helping Leon earlier."
remoQuest3.result = 4
remoQuestQuest.addDialog(remoQuest3, Remo)

def remoQuest4 = [id:4]
remoQuest4.npctext = "Really? That's fantastic. We could use some able-bodied folks down here, too."
remoQuest4.playertext = "Things are not going smoothly down here?"
remoQuest4.result = 5
remoQuestQuest.addDialog(remoQuest4, Remo)

def remoQuest5 = [id:5]
remoQuest5.npctext = "You've got that right. The northern half of Village Greens is a cake walk compared to the lawn gnome outposts further south."
remoQuest5.playertext = "There's more of them down there?"
remoQuest5.result = 6
remoQuestQuest.addDialog(remoQuest5, Remo)

def remoQuest6 = [id:6]
remoQuest6.npctext = "Yup. And they're tougher too. We need all the help we can get."
remoQuest6.options = []
remoQuest6.options << [text:"What can I do for you, Remo?", result: 8]
remoQuest6.options << [text:"Well, good luck then. Sounds like you'll need it.", result: 7]
remoQuestQuest.addDialog(remoQuest6, Remo)

def remoQuest7 = [id:7]
remoQuest7.npctext = "I suppose so. Well, good day, citizen. Come back later if you decide to help out."
remoQuest7.exec = { event ->
	event.player.updateQuest(90, "BFG-Remo") //complete the REMO quest
	event.player.removeMiniMapQuestActorName("BFG-Remo") //remove the flag that Leon set for the REMO quest
}
remoQuest7.result = DONE
remoQuestQuest.addDialog(remoQuest7, Remo)

def remoQuest8 = [id:8]
remoQuest8.npctext = "We Regulars are stretched so thin that we can't counterattack at all. If you're willing to help, I'll get you pointed in the right direction. What do you say?"
remoQuest8.options = []
remoQuest8.options << [text:"Tell me more about those missions. (**NOTE: The reward for completing Remo's series of tasks includes the selection of a new ring!**)", result: 11]
remoQuest8.options << [text:"That doesn't really sound like my speed. Sorry.", result: 9]
remoQuestQuest.addDialog(remoQuest8, Remo)

def remoQuest9 = [id:9]
remoQuest9.npctext = "Well, alright then. Here's hoping that Barton isn't overrun the next time you head back to town. Have a nice day, citizen."
remoQuest9.exec = { event ->
	event.player.updateQuest(90, "BFG-Remo") //complete the REMO quest
	event.player.removeMiniMapQuestActorName("BFG-Remo") //remove the flag that Leon set for the REMO quest
}
remoQuest9.result = DONE
remoQuestQuest.addDialog(remoQuest9, Remo)

//def remoQuest10 = [id:10]
//remoQuest10.npctext = "Okay...let's start slow then. Since you're down here talking to me, you've probably wandered around in the northern part of the Greens and have seen the occasional lawn gnome or fluff. Right?"
//remoQuest10.playertext = "Sure. They didn't seem too bad, either."
//remoQuest10.result = 11
//remoQuestQuest.addDialog(remoQuest10, Remo)

def remoQuest11 = [id:11]
remoQuest11.npctext = "The road we're on now stretches west toward Bill's Ranch. The area south of this road is a lot more dangerous than the areas you've seen to the north."
remoQuest11.playertext = "Dangerous? How so?"
remoQuest11.result = 12
remoQuestQuest.addDialog(remoQuest11, Remo)

def remoQuest12 = [id:12]
remoQuest12.npctext = "The pink flamingos you see down there seem to act like watchdogs. They run as soon as they see you and alert everything that moves in the area."
remoQuest12.playertext = "So what do you want me to do?"
remoQuest12.result = 13
remoQuestQuest.addDialog(remoQuest12, Remo)

def remoQuest13 = [id:13]
remoQuest13.npctext = "Let's start with them. Tackle a few of those flocks and cut down their numbers."
remoQuest13.playertext = "Okay...so go bash some of those flamingos and then come back to you?"
remoQuest13.result = 14
remoQuestQuest.addDialog(remoQuest13, Remo)

def remoQuest14 = [id:14]
remoQuest14.npctext = "Yup. That's about it."
remoQuest14.playertext = "That sounds pretty basic. I'll be back in just a few."
remoQuest14.exec = { event ->
	event.player.updateQuest(90, "BFG-Remo") //complete the REMO quest
	event.player.updateQuest(86, "BFG-Remo") //start the FLAMINGO HUNT quest
	event.player.removeMiniMapQuestActorName("BFG-Remo") //remove the flag that Leon set for the REMO quest
}
remoQuest14.result = DONE
remoQuestQuest.addDialog(remoQuest14, Remo)

//---------------------------------------------------------
// remoQuest Conversation (if you declined to continue     
// last time and have not yet stated FLAMINGO HUNT)        
//---------------------------------------------------------

def remoQuestCont = Remo.createConversation( "remoQuestCont", true, "QuestCompleted_90", "!QuestStarted_86", "!QuestCompleted_86" )
remoQuestCont.setUrgent(true)

def remoCont1 = [id:1]
remoCont1.npctext = "Hey there! Welcome back. Did you decide to help out after all?"
remoCont1.options = []
remoCont1.options << [text:"I sure did! (**NOTE: The reward for completing Remo's series of tasks includes the selection of a new ring!**)", result: 3]
remoCont1.options << [text:"Nah. Not right now. Sorry.", result: 2]
remoQuestCont.addDialog(remoCont1, Remo)

def remoCont2 = [id:2]
remoCont2.npctext = "Well, all right then. Here's hoping that Barton isn't overrun the next time you head back to town. Have a nice day, citizen."
remoCont2.result = DONE
remoQuestCont.addDialog(remoCont2, Remo)

def remoCont3 = [id:3]
remoCont3.npctext = "Fantastic. Let me get right to it then. The road we're on now stretches west toward Bill's Ranch. The area south of this road is a lot more organized than the areas you've seen to the north. More coordinated, if you know what I mean."
remoCont3.playertext = "Coordinated? How so?"
remoCont3.result = 4
remoQuestCont.addDialog(remoCont3, Remo)

def remoCont4 = [id:4]
remoCont4.npctext = "Hmmm...for example, the pink flamingos you see down there act like watchdogs. They run as soon as they see you and alert everything that moves in the area. They run pickets, patrols...it's scary how organized these 'lawn ornaments' seem now."
remoCont4.playertext = "So what do you want me to do?"
remoCont4.result = 5
remoQuestCont.addDialog(remoCont4, Remo)

def remoCont5 = [id:5]
remoCont5.npctext = "Let's start small. The gnomes seem to keep their flamingos in small flocks at different locations in the area. Tackle a few of those flocks and cut down their numbers. The fewer of those flamingos that are around, the better."
remoCont5.playertext = "Okay...so go bash some of those flamingos and then come back to you?"
remoCont5.result = 6
remoQuestCont.addDialog(remoCont5, Remo)

def remoCont6 = [id:6]
remoCont6.npctext = "Yup. That's about it."
remoCont6.playertext = "That sounds pretty basic. I'll be back in just a few."
remoCont6.exec = { event ->
	event.player.updateQuest(86, "BFG-Remo") //start the FLAMINGO HUNT quest
	event.player.removeMiniMapQuestActorName("BFG-Remo") //remove the flag that Leon set for the REMO quest
}
remoCont6.result = DONE
remoQuestCont.addDialog(remoCont6, Remo)

//---------------------------------------------------------
// FLAMINGO HUNT (Interim)                                 
//---------------------------------------------------------

def flamingoInterim = Remo.createConversation( "flamingoInterim", true, "QuestStarted_86:2" )

def flamInt1 = [id:1]
flamInt1.npctext = "How goes the hunt?"
flamInt1.playertext = "Heh. I guess I need to get that started soon."
flamInt1.result = 2
flamingoInterim.addDialog(flamInt1, Remo)

def flamInt2 = [id:2]
flamInt2.npctext = "What? You're not done yet? Okay...well...let me know."
flamInt2.playertext = "Can do. Later!"
flamInt2.result = DONE
flamingoInterim.addDialog(flamInt2, Remo)

//---------------------------------------------------------
// FLAMINGO HUNT (Success)                                 
//---------------------------------------------------------

def flamingoSuccess = Remo.createConversation( "flamingoSuccess", true, "QuestStarted_86:3" )

def flamSucc1 = [id:1]
flamSucc1.npctext = "Do those pink feathers on your shoulders indicate a little mix-up with flamingos?"
flamSucc1.playertext = "You got it! I see what you mean about the flamingos coming to help each other. That can get tricky!"
flamSucc1.result = 2
flamingoSuccess.addDialog(flamSucc1, Remo)

def flamSucc2 = [id:2]
flamSucc2.npctext = "Too right about that! You need to stay aware at all times while you're out there, or you'll get blindsided for sure."
flamSucc2.quest = 86 //push the completion for the FLAMINGO HUNT quest
flamSucc2.result = DONE
flamSucc2.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z02StartShroomQuest" )
	Remo.pushDialog(event.player, "shroomStart")
	event.player.removeMiniMapQuestActorName( "BFG-Remo" )
}
flamingoSuccess.addDialog(flamSucc2, Remo)

//---------------------------------------------------------
// SHROOM HEADS (start)                                    
//---------------------------------------------------------

def shroomStart = Remo.createConversation( "shroomStart", true, "Z02StartShroomQuest", "!QuestStarted_87", "!QuestCompleted_87")

def shroom1 = [id:1]
shroom1.npctext = "Ready for a tougher challenge now?"
shroom1.options = []
shroom1.options << [text:"I hope you've got something tougher than that flamingo task. Bring it on!", result: 3]
shroom1.options << [text:"No...not right now. Thanks.", result: 2]
shroomStart.addDialog(shroom1, Remo)

def shroom2 = [id:2]
shroom2.npctext = "I understand. This sort of thing isn't everybody's cup of tea. Come back if you change your mind!"
shroom2.playertext = "Okay, I will. Thanks."
shroom2.result = DONE
shroomStart.addDialog(shroom2, Remo)

def shroom3 = [id:3]
shroom3.npctext = "Okay. You want tougher? You've got it. The lawn gnomes use those darn mushroom cannons to good effect and the cannons are tough, like they're armored!"
shroom3.playertext = "All right...so what do you need?"
shroom3.result = 4
shroomStart.addDialog(shroom3, Remo)

def shroom4 = [id:4]
shroom4.npctext = "Take out a bunch of those cannons and gather a sampling of their mushroom tops. Bring them back and we'll talk about what to do next."
shroom4.playertext = "You bet! I'm on it."
shroom4.quest = 87 //start the MOWED OVER quest
shroom4.result = DONE
shroomStart.addDialog(shroom4, Remo)

//---------------------------------------------------------
// SHROOM HEADS (interim)                                  
//---------------------------------------------------------

def shroomInterim = Remo.createConversation( "shroomInterim", true, "QuestStarted_87:2", "!QuestStarted_87:3")

def shroomInt1 = [id:1]
shroomInt1.npctext = "How goes the struggle against the lawn ornaments?"
shroomInt1.playertext = "Not bad...but I haven't got all those mushroom tops yet."
shroomInt1.result = 2
shroomInterim.addDialog(shroomInt1, Remo)

def shroomInt2 = [id:2]
shroomInt2.npctext = "Hmmm...well, they can be tricky to gather, I suppose. Look around the main gnome outposts. They tend to use the cannons as defense lines, so you should find them in those areas. Try to find a cannon that's more isolated and the others and it should be easy pickings."
shroomInt2.playertext = "Okay. Thanks for the tips!"
shroomInt2.result = 3
shroomInterim.addDialog(shroomInt2, Remo)

def shroomInt3 = [id:3]
shroomInt3.npctext = "Anytime. Good luck!"
shroomInt3.result = DONE
shroomInterim.addDialog(shroomInt3, Remo)

//---------------------------------------------------------
// SHROOM HEADS (success)                                  
//---------------------------------------------------------

def shroomSuccess = Remo.createConversation( "shroomSuccess", true, "QuestStarted_87:3")

def shroomSucc1 = [id:1]
shroomSucc1.npctext = "What's that big grin on your face for?"
shroomSucc1.playertext = "Mission accomplished!"
shroomSucc1.result = 2
shroomSuccess.addDialog(shroomSucc1, Remo)

def shroomSucc2 = [id:2]
shroomSucc2.npctext = "Well done! Congratulations are most definitely in order. Leon likes us to reward citizens that help out in times of crisis, so here you go!"
shroomSucc2.playertext = "Great! Thanks!"
shroomSucc2.quest = 87 //push the completion for SHROOM HEADS quest
shroomSucc2.result = DONE
shroomSucc2.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z02StartBehindEnemyLines" )
	Remo.pushDialog(event.player, "outpostStart")
	event.player.removeMiniMapQuestActorName( "BFG-Remo" )
}	
shroomSuccess.addDialog(shroomSucc2, Remo)

//---------------------------------------------------------
// BEHIND ENEMY LINES (start)                              
//---------------------------------------------------------

def outpostStart = Remo.createConversation( "outpostStart", true, "Z02StartBehindEnemyLines", "!QuestStarted_88", "!QuestCompleted_88")

def outpost1 = [id:1]
outpost1.npctext = "Okay, I admit it. I was testing you to see if you had the guts to handle a really tough mission. In fact, I wouldn't suggest you try this without forming a Crew to help you out. Interested?"
outpost1.options = []
outpost1.options << [text:"An excuse to form a team for a mission? I'm there!", result: 5]
outpost1.options << [text:"Are you sure I need a Crew?", result: 3]
outpost1.options << [text:"Not right now. Maybe later.", result: 2]
outpostStart.addDialog(outpost1, Remo)

def outpost2 = [id:2]
outpost2.npctext = "Okay. Everyone needs some down time. Catch some zzz's and I'll catch you later."
outpost2.result = DONE
outpostStart.addDialog(outpost2, Remo)

def outpost3 = [id:3]
outpost3.npctext = "You could try it on your own, but be careful. The gnomes defend their territory vigilantly. The guards can swarm you before you even know they've noticed you if you let your guard down."
outpost3.playertext = "Well, maybe I'll try making a group if I don't do well on my own."
outpost3.result = 4
outpostStart.addDialog(outpost3, Remo)

def outpost4 = [id:4]
outpost4.npctext = "It's your funeral! So, are you ready to hear more?"
outpost4.playertext = "Lay it on me!"
outpost4.result = 5
outpostStart.addDialog(outpost4, Remo)

def outpost5 = [id:5]
outpost5.npctext = "The command staff operate out of a giant purple mushroom down in the southwest and they really end up causing trouble when we don't smack them down occasionally."
outpost5.playertext = "So you need help taking out that command post?"
outpost5.result = 6
outpostStart.addDialog(outpost5, Remo)

def outpost6 = [id:6]
outpost6.npctext = "That's about the sense of it. This isn't a simple mission. Are you sure you're up for it?"
outpost6.options = []
outpost6.options << [text:"Yeah. I'm ready.", result: 8]
outpost6.options << [text:"I'll come back later. Thanks anyway.", result: 7]
outpostStart.addDialog(outpost6, Remo)

def outpost7 = [id:7]
outpost7.npctext = "No harm, no foul. Come back anytime and we'll talk about it again."
outpost7.playertext = "Thanks."
outpost7.result = DONE
outpostStart.addDialog(outpost7, Remo)

def outpost8 = [id:8]
outpost8.npctext = "Okay then. Best of luck to you. Head down the road here until you can turn left. Once you start heading south, you'll be in enemy territory. Good luck!"
outpost8.playertext = "The name's %p."
outpost8.result = 9
outpostStart.addDialog(outpost8, Remo)

def outpost9 = [id:9]
outpost9.npctext = "Fine, fine. I guess you've showed your stuff enough that I should start remembering your name. Mine's Remo, by the way. Now get going."
outpost9.playertext = "See ya."
outpost9.quest = 88 //push the start of the BEHIND ENEMY LINES quest
outpost9.result = DONE
outpostStart.addDialog(outpost9, Remo)

//---------------------------------------------------------
// BEHIND ENEMY LINES (interim)                            
//---------------------------------------------------------
def outpostInterim = Remo.createConversation( "outpostInterim", true, "QuestStarted_88:2")

def outInt1 = [id:1]
outInt1.npctext = "Hmmm...you don't seem to be wearing the smug self-expression that signifies victory there, %p."
outInt1.playertext = "Heh. No. Not yet. Working on it still!"
outInt1.result = 2
outpostInterim.addDialog(outInt1, Remo)

def outInt2 = [id:2]
outInt2.npctext = "Well get down there! If you don't knock those generals around soon, we'll be up to our keesters in lawn gnome attackers soon."
outInt2.playertext = "Gotcha, Remo. I'm on it."
outInt2.result = DONE
outpostInterim.addDialog(outInt2, Remo)

//---------------------------------------------------------
// BEHIND ENEMY LINES (success)                            
//---------------------------------------------------------
def outpostSuccess = Remo.createConversation( "outpostSuccess", true, "QuestStarted_88:3")
outpostSuccess.setUrgent(true)

def outSucc1 = [id:1]
outSucc1.npctext = "Ah! Now *that's* an expression I know well. You've had some luck, have you, %p?"
outSucc1.playertext = "Mission completed. The General put up a struggle, but he's knocked down again, for a while anyway."
outSucc1.result = 2
outpostSuccess.addDialog(outSucc1, Remo)

def outSucc2 = [id:2]
outSucc2.npctext = "Well done! I think it will be a long time before the gnomes can threaten Barton Town again."
outSucc2.playertext = "Oh... I guess you don't have anymore work for me then?"
outSucc2.result = 3
outpostSuccess.addDialog(outSucc2, Remo)

def outSucc3 = [id:3]
outSucc3.npctext = "Perhaps not in the Village Greens, but we need your assistance elsewhere. In fact, Leon messaged me himself with a task specifically for you."
outSucc3.options = []
outSucc3.options << [text:"Really? He mentioned me?", result: 4]
outSucc3.options << [text:"I'm ready, what's he want me to do?", result: 8]
outpostSuccess.addDialog(outSucc3, Remo)

def outSucc4 = [id:4]
outSucc4.npctext = "By name. He said, 'Things are getting out of control over in the ranch and Larry still hasn't got the subway running over there. %p seems very competent and should be able to iron things out over there."
outSucc4.playertext = "Wow! So I need to help this Larry guy?"
outSucc4.result = 5
outpostSuccess.addDialog(outSucc4, Remo)

def outSucc5 = [id:5]
outSucc5.npctext = "Yes, and anyone else in need of help. Start with Larry, though."
outSucc5.options = []
outSucc5.options << [text:"Will do. How do I find Larry?", result: 6]
outSucc5.options << [text:"I'm not quite ready.", result: 7]
outpostSuccess.addDialog(outSucc5, Remo)

def outSucc6 = [id:6]
outSucc6.npctext = "Just follow the path west until you get to the ranch. There's a loop in the path there, and that's where you'll find Larry. Before you go, though, I've also got a bunch of bags of rings here and I'd like to give you one before you head off."
outSucc6.flag = "Z2SecondRingGrantOkay"
outSucc6.exec = { event ->
	event.player.updateQuest(92, "BFG-Remo")
	Remo.pushDialog(event.player, "ringGrant")
	event.player.removeMiniMapQuestActorName( "BFG-Remo" )	
	event.player.addMiniMapQuestActorName( "Larry-VQS" )
}
outSucc6.result = DONE
outpostSuccess.addDialog(outSucc6, Remo)

def outSucc7 = [id:7]
outSucc7.npctext = "Very well. Before you go, though I've also got a bunch of bags of rings here and I'd like to give you one before you head off."
outSucc7.playertext = "Awesome!"
outSucc7.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z2SecondRingGrantOkay" )
	event.player.setQuestFlag( GLOBAL, "Z2_Remo_Larry_Break" )
	Remo.pushDialog(event.player, "ringGrant")
	event.player.removeMiniMapQuestActorName( "BFG-Remo" )
}
outSucc7.result = DONE
outpostSuccess.addDialog(outSucc7, Remo)

def outSucc8 = [id:8]
outSucc8.npctext = "Things in Bill's Ranch to the west are getting out of hand. Leon wants you to help out in every way you can. He did ask that you help Larry first, but beyond that the order in which you offer aid is at your discretion."
outSucc8.options = []
outSucc8.options << [text:"Alright, how do I find Larry then?", result: 6]
outSucc8.options << [text:"I'm not ready yet. Give me a few.", result: 7]
outpostSuccess.addDialog(outSucc8, Remo)

//--------------------------------------------------------------
//Larry - Quest Break                                           
//--------------------------------------------------------------
def larryBreak = Remo.createConversation("larryBreak", true, "Z2_Remo_Larry_Break", "Z02SecondRingGrantReceived")

def larryBreak1 = [id:1]
larryBreak1.npctext = "Hi again. Did you decide to help out in Bill's Ranch?"
larryBreak1.options = []
larryBreak1.options << [text:"Sure. What did you need me to do again?", result:2]
larryBreak1.options << [text:"No, I didn't.", result:3]
larryBreak.addDialog(larryBreak1, Remo)

def larryBreak2 = [id:2]
larryBreak2.npctext = "We need you to help Larry. He's working on a problem with the trains. Let me mark his location on your map."
larryBreak2.quest = 92
larryBreak2.flag = "!Z2_Remo_Larry_Break"
larryBreak2.exec = { event ->
	event.player.addMiniMapQuestActorName("Larry-VQS")
}
larryBreak2.result = DONE
larryBreak.addDialog(larryBreak2, Remo)

def larryBreak3 = [id:3]
larryBreak3.npctext = "Okay. Let me know if you change your mind."
larryBreak3.result = DONE
larryBreak.addDialog(larryBreak3, Remo)

//--------------------------------------------------------------
//RING GRANT CONVERSATION                                       
//--------------------------------------------------------------
def ringGrant = Remo.createConversation( "ringGrant", true, "Z2SecondRingGrantOkay", "!Z02SecondRingGrantReceived", "!Z02RingRewardInProgress" )

def grant1 = [id:1]
grant1.npctext = "Go ahead and rummage through the bags and take the ring you want."
grant1.exec = { event ->
	event.player.setQuestFlag( GLOBAL, "Z02RingRewardInProgress" )
	player = event.player
	makeMainMenu( player )
}
grant1.result = DONE
ringGrant.addDialog( grant1, Remo )


//---------------------------------------------------------
// POST-QUEST DEFAULT DIALOG (Until after talking to Bill) 
//---------------------------------------------------------
def postQuestDefault = Remo.createConversation( "postQuestDefault", true, "Z02SecondRingGrantReceived", "!Z2TalkedToBill", "!Z2_Remo_Larry_Break")

def post1 = [id:1]
post1.npctext = "Hey, %p...you've done a lot for me here, and I'd be happy if you stayed here knocking down gnomes on a regular basis, but something tells me you're meant for greater things. Maybe you should head out west and see if Rancher Bill needs help out there. He's pretty much on his own there and I can only imagine what sorts of things he's facing."
post1.playertext = "Okay, Remo...maybe I'll head out there. You said Bill's place is out west?"
post1.result = 2
postQuestDefault.addDialog(post1, Remo)

def post2 = [id:2]
post2.npctext = "Yup. Go west, young Gaian!"
post2.playertext = "Har de har har..."
post2.result = 3
postQuestDefault.addDialog(post2, Remo)

def post3 = [id:3]
post3.npctext = "Bill's place is fenced off, so just keep moving south and west along the fence when you get to it and you'll find the gate into his ranch."
post3.playertext = "Should I come back and let you know how he's doing?"
post3.result = 4
postQuestDefault.addDialog(post3, Remo)

def post4 = [id:4]
post4.npctext = "Nah. He's capable and I'm sure he's fine, but I'm guessing he has more on his plate right now than he can get done by himself. You're quick-witted and adaptable and I'm sure he'd be happy to get to work with you. Tell him Remo sent ya."
post4.playertext = "Okay...I'll do that if I see him. Thanks for everything, Remo!"
post4.result = 5
postQuestDefault.addDialog(post4, Remo)

def post5 = [id:5]
post5.npctext = "You bet, %p. Good luck out there!"
post5.result = DONE
postQuestDefault.addDialog(post5, Remo)

//---------------------------------------------------------
// POST-BILL DEFAULT DIALOG (After player talked to Bill)  
//---------------------------------------------------------
def postBillDefault = Remo.createConversation( "postBillDefault", true, "Z02SecondRingGrantReceived", "Z2TalkedToBill", "!Z2_Remo_Larry_Break")

def postBill1 = [id:1]
postBill1.npctext = "Hey there, %p! Good to see you again!"
postBill1.playertext = "Hi again, Remo! How's life in lawn gnome land?"
postBill1.result = 2
postBillDefault.addDialog(postBill1, Remo)

def postBill2 = [id:2]
postBill2.npctext = "Same old, same old. You'd think that the gnomes would break out of their patterns and take us all out...but they never seem to get around to it."
postBill2.playertext = "Yeah...odd. It's like they're alive...but not really. You know what I mean?"
postBill2.result = 3
postBillDefault.addDialog(postBill2, Remo)

def postBill3 = [id:3]
postBill3.npctext = "Or alive...but not yet fully aware. That's what *I* worry about in bed at night. What happens to us all if they get stronger, or smarter?"
postBill3.playertext = "Yikes. What a nasty thought!"
postBill3.result = 4
postBillDefault.addDialog(postBill3, Remo)

def postBill4 = [id:4]
postBill4.npctext = "Uh huh. And from what I hear is happening in Aekea...well...it sounds like we're getting off lucky here at Barton."
postBill4.playertext = "Why? What did you hear?"
postBill4.result = 5
postBillDefault.addDialog(postBill4, Remo)

def postBill5 = [id:5]
postBill5.npctext = "More than I should have probably. Sorry, %p. Leon would kick my butt if he heard me mouthing off like this. Look...I'm sure it'll all come out eventually anyway. I'm sorry I said anything...but I can't say anything else right now."
postBill5.playertext = "...okay. That doesn't sound very promising though, Remo."
postBill5.result = 6
postBillDefault.addDialog(postBill5, Remo)

def postBill6 = [id:6]
postBill6.npctext = "No, I suppose it doesn't. Look, %p. Just be strong and do what you can, okay? We all need to stick together right now."
postBill6.playertext = "Okay, Remo. Good luck."
postBill6.result = 7
postBillDefault.addDialog(postBill6, Remo)

def postBill7 = [id:7]
postBill7.npctext = "You too, %p."
postBill7.result = DONE
postBillDefault.addDialog(postBill7, Remo)

//====================================================
// RING GRANT MENU LOGIC                              
//====================================================

dialogBoxWidth = 400
CL = 1
CLdecimal = 0

def makeMainMenu( player ) {
	titleString = "Main Menu"
	descripString = "Choose a ring from any of these categories:"
	diffOptions = ["New Rings", "Close Combat", "Ranged Combat", "Crowd Control", "Defenses", "Healing", "Buffs", "Cancel"]
	
	uiButtonMenu( player, "mainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "New Rings" ) {
			makeNewRingMenu( player )
		}
		if( event.selection == "Close Combat" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Ranged Combat" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Crowd Control" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Defenses" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Healing" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Buffs" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Cancel" ) {
			event.actor.unsetQuestFlag( GLOBAL, "Z02RingRewardInProgress" )
		}
	}
}

//====================================================
// NEW RINGS                                          
//====================================================
def makeNewRingMenu( player ) {
	titleString = "New Rings Menu"
	descripString = "These rings are new to the list this time."
	diffOptions = [ "Coyote Spirit", "Fire Rain", "Pot Lid", "Wish", "Main Menu"  ]
	
	uiButtonMenu( player, "newRingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// CLOSE COMBAT RINGS                                 
//====================================================
def makeCombatMenu( player ) {
	titleString = "Close Combat Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bump", "Slash", "Main Menu" ]
	
	uiButtonMenu( player, "combatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bump" ) {
			makeBumpMenu( player )
		}
		if( event.selection == "Slash" ) {
			makeSlashMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBumpMenu( player ) {
	titleString = "Bump"
	descripString = "Turn that hip around and *bump* your opponent away from you. Knocking them far away at higher Rage Ranks. The process *is* painful to your target."
	diffOptions = [ "Take the Bump ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "bumpMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bump ring!" ) {
			event.actor.grantRing( "17713", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDervishMenu( player ) {
	titleString = "Dervish"
	descripString = "Whirling at incredible speed, you deal damage to all foes close to you. Higher Rage Ranks knock your enemies farther back and increase the area you hit."
	diffOptions = [ "Take the Dervish ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DervishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Dervish ring!" ) {
			event.actor.grantRing( "17712", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHackMenu( player ) {
	titleString = "Hack"
	descripString = "Land a colossal blow to your foes! Hits things hard, even causing them to bleed for a bit after you hit them. At higher Rage Ranks, the bleeding lasts longer, thus causing more damage."
	diffOptions = [ "Take the Hack ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hack ring!" ) {
			event.actor.grantRing( "17714", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMantisMenu( player ) {
	titleString = "Mantis"
	descripString = "You create a katana from nothing to do your bidding. Does light damage, but attacks again very quickly. At higher Rage Ranks, it also drains an enemy's Willpower."
	diffOptions = [ "Take the Mantis ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MantisMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Mantis ring!" ) {
			event.actor.grantRing( "17710", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSlashMenu( player ) {
	titleString = "Slash"
	descripString = "You slash at the nearby foes in front of you, doing damage to all that you hit. Your slash becomes wider and deeper at higher Rage Ranks, allowing you to hit more enemies."
	diffOptions = [ "Take the Slash ring!", "Close Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SlashMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Slash ring!" ) {
			event.actor.grantRing( "17711", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Close Combat Menu" ) {
			makeCombatMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// RANGED COMBAT RINGS                                
//====================================================
def makeRangedMenu( player ) {
	titleString = "Ranged Attack Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Fire Rain", "Hornet Nest", "Hot Foot", "Solar Rays", "Main Menu" ]
	
	uiButtonMenu( player, "rangedMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Fire Rain" ) {
			makeFireRainMenu( player )
		}
		if( event.selection == "Hornet Nest" ) {
			makeHornetNestMenu( player )
		}
		if( event.selection == "Hot Foot" ) {
			makeHotFootMenu( player )
		}
		if( event.selection == "Solar Rays" ) {
			makeSolarRaysMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeFireRainMenu( player ) {
	titleString = "Fire Rain"
	descripString = "Summon burning rain from the sky to fall in an area around yourself damaging your foes and draining their Willpower. Higher Rage Ranks result in bigger damage areas and greater Willpower drains."
	diffOptions = [ "Take the Fire Rain ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FireRainMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fire Rain ring!" ) {
			event.actor.grantRing( "17748", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGunsGunsGunsMenu( player ) {
	titleString = "Guns, Guns, Guns"
	descripString = "When all else fails, haul out the artillery and drown your target in lead! Higher Rage Ranks create a wider spray of bullets, causing more damage in a bigger and bigger area around your target."
	diffOptions = [ "Take the Guns, Guns, Guns ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GunsGunsGunsMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Guns, Guns, Guns ring!" ) {
			event.actor.grantRing( "17747", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHeavyWaterBalloonMenu( player ) {
	titleString = "Heavy Water Balloon"
	descripString = "You create a giant water balloon and hurl it at your foes, causing a colossal splash in a large area, damaging those affected. Higher Rage Ranks make bigger splashes and Taunt the enemies in the area to attack you instead of your friends."
	diffOptions = [ "Take the Heavy Water Balloon ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HeavyWaterBalloonMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Heavy Water Balloon ring!" ) {
			event.actor.grantRing( "17719", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHornetNestMenu( player ) {
	titleString = "Hornet Nest"
	descripString = "Hurl a nest of hornets at the ground, creating a swarm that attacks nearby foes. Higher Rage Ranks increase the area affected, as well as making the target sometimes panic and run away. (Fear)"
	diffOptions = [ "Take the Hornet Nest ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HornetNestMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hornet Nest ring!" ) {
			event.actor.grantRing( "17718", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHotFootMenu( player ) {
	titleString = "Hot Foot"
	descripString = "Set your target's feet on fire, causing it pain for several seconds after the attack occurs. At higher Rage Ranks, the target also suffers a Dodge penalty, making it easier to hit. Higher Rage Ranks also make this ability affect an area around the target."
	diffOptions = [ "Take the Hot Foot ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HotFootMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hot Foot ring!" ) {
			event.actor.grantRing( "17717", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHuntersBowMenu( player ) {
	titleString = "Hunter's Bow"
	descripString = "This bow lets you fire arrows often and far, damaging your foe and slowing it down so they can't get to you easily. Higher Rage Ranks reduce the target's Footspeed still further and increase the duration of the effect."
	diffOptions = [ "Take the Hunter's Bow ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HuntersBowMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Hunter's Bow ring!" ) {
			event.actor.grantRing( "17721", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSharkAttackMenu( player ) {
	titleString = "Shark Attack"
	descripString = "Groundsharks attack your foe, often knocking it away from you, and also causing some bleeding to persist after the attack. Higher Rage Ranks result in longer bleeding duration and sometimes paralyzing your target with shock. (Root)"
	diffOptions = [ "Take the Shark Attack ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SharkAttackMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shark Attack ring!" ) {
			event.actor.grantRing( "17716", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeShurikenMenu( player ) {
	titleString = "Shuriken"
	descripString = "Hurl spiny metal stars at your foes! In addition to damaging your target, higher Rage Ranks increase the effect to an area around your target, plus they cause your target to have reduced Accuracy for a time."
	diffOptions = [ "Take the Shuriken ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ShurikenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Shuriken ring!" ) {
			event.actor.grantRing( "17715", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeSolarRaysMenu( player ) {
	titleString = "Solar Rays"
	descripString = "Focus the power of the sun into a beam that damages your foe and, at higher Rage Ranks, can knock it away from you, or even stun it to Sleep for a short time."
	diffOptions = [ "Take the Solar Rays ring!", "Ranged Combat Menu", "Main Menu" ]
	
	uiButtonMenu( player, "SolarRaysMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Solar Rays ring!" ) {
			event.actor.grantRing( "17720", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Ranged Combat Menu" ) {
			makeRangedMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	


//====================================================
// CROWD CONTROL RINGS                                
//====================================================
def makeCrowdControlMenu( player ) {
	titleString = "Crowd Control Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Quicksand", "Taunt", "Main Menu" ]
	
	uiButtonMenu( player, "crowdControlMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Quicksand" ) {
			makeQuicksandMenu( player )
		}
		if( event.selection == "Taunt" ) {
			makeTauntMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeDuctTapeMenu( player ) {
	titleString = "Duct Tape"
	descripString = "Wrap your target up and keep it from moving (Sleep). NOTE: Hitting a target while it is taped will weaken the tape and allow it to move again. Higher Rage Ranks start affecting foes around your original target also, as well as increasing the chance that they get bound by tape."
	diffOptions = [ "Take the Duct Tape ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DuctTapeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Duct Tape ring!" ) {
			event.actor.grantRing( "17722", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeGumshoeMenu( player ) {
	titleString = "Gumshoe"
	descripString = "Make the feet of your enemy sticky and slow its Footspeed substantially. Higher Rage Ranks make this ring affect increasingly-sized areas and slow the targets within even further."
	diffOptions = [ "Take the Gumshoe ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GumshoeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Gumshoe ring!" ) {
			event.actor.grantRing( "17743", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeQuicksandMenu( player ) {
	titleString = "Quicksand"
	descripString = "Cause the ground at an area to become mostly water and then solid mud for a short while, trapping your enemies where they stand. (Root). Higher Rage Ranks increase the area affected, as well as the chance to stick your foes in the mud."
	diffOptions = [ "Take the Quicksand ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "QuicksandMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Quicksand ring!" ) {
			event.actor.grantRing( "17723", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeScaredyCatMenu( player ) {
	titleString = "Scaredy Cat"
	descripString = "Make your foe flee from you in sheer panic! At higher Rage Ranks, this ring affects entire areas and the tendency for your foes to flee is bigger also."
	diffOptions = [ "Take the Scaredy Cat ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ScaredyCatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Scaredy Cat ring!" ) {
			event.actor.grantRing( "17725", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
def makeTauntMenu( player ) {
	titleString = "Taunt"
	descripString = "Sometimes, you need to pull enemies away from your friends. This ring does the trick, making foes in an area angered at you for a while. Higher Rage Ranks increase the area affected and the strength of the Taunt. The highest Rage Ranks also make your foes tremble, draining their Dodge for a time."
	diffOptions = [ "Take the Taunt ring!", "Crowd Control Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TauntMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Taunt ring!" ) {
			event.actor.grantRing( "17724", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Crowd Control Menu" ) {
			makeCrowdControlMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
		
//====================================================
// DEFENSE RINGS                                      
//====================================================
def makeDefenseMenu( player ) {
	titleString = "Defense Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Pot Lid", "Teflon Spray", "Main Menu" ]
	
	uiButtonMenu( player, "defenseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Pot Lid" ) {
			makePotLidMenu( player )
		}
		if( event.selection == "Teflon Spray" ) {
			makeTeflonSprayMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeImprobabilitySphereMenu( player ) {
	titleString = "Improbability Sphere"
	descripString = "Use the Improbability Sphere to give you or a friend moderate defense (Persistent Armor), as well as to Reflect an attack back against the attacker of you or a friend! Any attack Reflected back on the attacker does the damage to the attacker instead. Higher Rage Ranks increase the amount of Armor and theﾅprobability...that Reflection will occur."
	diffOptions = [ "Take the Improbability Sphere ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "ImprobabilitySphereMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Improbability Sphere ring!" ) {
			event.actor.grantRing( "17730", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makePotLidMenu( player ) {
	titleString = "Pot Lid"
	descripString = "Use Pot Lid to give you or a friend moderate defense (Persistent Armor) and to sometimes Deflect an attack away from you or a friend completely. Any Deflected attack is nullified completely! Higher Rage Ranks make it more and more likely that a Deflection will occur on an attack."
	diffOptions = [ "Take the Pot Lid ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "PotLidMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Pot Lid ring!" ) {
			event.actor.grantRing( "17729", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeRockArmorMenu( player ) {
	titleString = "Rock Armor"
	descripString = "Cover each of your allies in Rock Armor giving them strong protection against incoming damage. (Armor Pool) The Rock Armor lasts for several minutes, or until it absorbs enough damage to break up. Higher Rage Ranks make stronger and stronger Armor."
	diffOptions = [ "Take the Rock Armor ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "RockArmorMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Rock Armor ring!" ) {
			event.actor.grantRing( "17728", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTeflonSprayMenu( player ) {
	titleString = "Teflon Spray"
	descripString = "This makes some of any incoming damage bounce away instead of hurting you or a friend (Persistent Armor). At higher Rage Ranks, it also makes your target harder to hit (Dodge) and eventually can occasionally Reflect an attack back against your foe."
	diffOptions = [ "Take the Teflon Spray ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TeflonSprayMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Teflon Spray ring!" ) {
			event.actor.grantRing( "17726", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeTurtleMenu( player ) {
	titleString = "Turtle"
	descripString = "When trouble is overwhelming, the best thing to do is curl up in your shell and hope the bad things go away. This creates a protective field that can absorb an amazing amount of damage out of any incoming attack, but only lasts a short time. (Armor Pool) Higher Rage Ranks create stronger shells."
	diffOptions = [ "Take the Turtle ring!", "Defense Menu", "Main Menu" ]
	
	uiButtonMenu( player, "TurtleMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Turtle ring!" ) {
			event.actor.grantRing( "17727", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Defense Menu" ) {
			makeDefenseMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// HEALING RINGS                                      
//====================================================
def makeHealingMenu( player ) {
	titleString = "Healing Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Bandage", "Diagnose", "Meat", "Wish", "Main Menu" ]
	
	uiButtonMenu( player, "healingMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Bandage" ) {
			makeBandageMenu( player )
		}
		if( event.selection == "Diagnose" ) {
			makeDiagnoseMenu( player )
		}
		if( event.selection == "Meat" ) {
			makeMeatMenu( player )
		}
		if( event.selection == "Wish" ) {
			makeWishMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeBandageMenu( player ) {
	titleString = "Bandage"
	descripString = "Start a bandaging process on you or a friend. Healing then occurs over a short time, a bit at a time. Higher Rage Ranks increase the amount of Health recovered."
	diffOptions = [ "Take the Bandage ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "BandageMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Bandage ring!" ) {
			event.actor.grantRing( "17732", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDefibrillateMenu( player ) {
	titleString = "Defibrillate"
	descripString = "Use this on a Dazed ally, and you'll instantly Awaken them. Higher Rage Ranks increase the amount of Health and Stamina recovered, as well as reducing the number of rings temporarily locked because you had been Dazed."
	diffOptions = [ "Take the Defibrillate ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DefibrillateMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Defibrillate ring!" ) {
			event.actor.grantRing( "17734", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDiagnoseMenu( player ) {
	titleString = "Diagnose"
	descripString = "You analyze the wounds of all allies in the area around you and heal them of some of their wounds, including your own! Higher Rage Ranks increase the healing effect and the area affected."
	diffOptions = [ "Take the Diagnose ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DiagnoseMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Diagnose ring!" ) {
			event.actor.grantRing( "17733", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeDivinityMenu( player ) {
	titleString = "Divinity"
	descripString = "Use this to draw lifeforce energy to you more quickly, increasing the rate at which you and your nearby friends regain Stamina, even during combat! Higher Rage Ranks let you recover Stamina even more quickly, and the highest Rage Ranks even help you find loot more easily. (Luck)"
	diffOptions = [ "Take the Divinity ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "DivinityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Divinity ring!" ) {
			event.actor.grantRing( "17737", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeHealingHaloMenu( player ) {
	titleString = "Healing Halo"
	descripString = "Create this halo over you and your nearby allies. You all then regenerate health more quickly, even during combat! Higher Rage Ranks increase this effect and the highest Rage Ranks also make all affected targets harder to knockback. (Weight)"
	diffOptions = [ "Take the Healing Halo ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "HealingHaloMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Healing Halo ring!" ) {
			event.actor.grantRing( "17736", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMeatMenu( player ) {
	titleString = "Meat"
	descripString = "Be a meateater and beef up big and strong! You heal a big chunk of damage you've suffered as well as increasing your maximum Health the same amount. Higher Rage Ranks increase the amount of Health increased."
	diffOptions = [ "Take the Meat ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MeatMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Meat ring!" ) {
			event.actor.grantRing( "17735", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeWishMenu( player ) {
	titleString = "Wish"
	descripString = "Heal any of your friends, one at a time with this quickly-recharging and powerful ring. Higher Rage Ranks heal targets standing around your target also. The bigger the Rage Rank, the bigger the area affected."
	diffOptions = [ "Take the Wish ring!", "Healing Menu", "Main Menu" ]
	
	uiButtonMenu( player, "WishMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Wish ring!" ) {
			event.actor.grantRing( "17731", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Healing Menu" ) {
			makeHealingMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// BUFFS                                              
//====================================================
def makeBuffMenu( player ) {
	titleString = "Buff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Coyote Spirit", "Fitness", "Keen Aye", "Main Menu" ]
	
	uiButtonMenu( player, "buffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Coyote Spirit" ) {
			makeCoyoteSpiritMenu( player )
		}
		if( event.selection == "Fitness" ) {
			makeFitnessMenu( player )
		}
		if( event.selection == "Keen Aye" ) {
			makeKeenAyeMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeCoyoteSpiritMenu( player ) {
	titleString = "Coyote Spirit"
	descripString = "Use this ring to give you or any friend a faster Footspeed. Higher Rage Ranks increase the Footspeed bonus, as well as providing you the Luck of the Coyote (Luck)."
	diffOptions = [ "Take the Coyote Spirit ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "CoyoteSpiritMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Coyote Spirit ring!" ) {
			event.actor.grantRing( "17738", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFitnessMenu( player ) {
	titleString = "Fitness"
	descripString = "When you wear this ring, you just get better! Accuracy, Dodge, Willpower, Weight, Health Regeneration, Stamina Regeneration and even Luck are all given minor bonuses. This ring is passive and does not need to be clicked to be fully functional. Just wear it and it works!"
	diffOptions = [ "Take the Fitness ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FitnessMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fitness ring!" ) {
			event.actor.grantRing( "17866", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeFleetFeetMenu( player ) {
	titleString = "Fleet Feet"
	descripString = "Sometimes, you just need to get away. This makes you, and any friends around you, greatly increase your Footspeed for a brief time. Since you're probably running into or out of trouble, this also bolsters your Willpower with a modest bonus at higher Rage Ranks."
	diffOptions = [ "Take the Fleet Feet ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "FleetFeetMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Fleet Feet ring!" ) {
			event.actor.grantRing( "17749", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeGhostMenu( player ) {
	titleString = "Ghost"
	descripString = "You become slightly ethereal and matter occasionally, err, passes through you in a fairly disturbing fashion. (Dodge) Higher Rage Ranks increase the amount of Dodge bonus you receive. (Dodge bonuses also decrease the chance that a monster will Critical Hit you during a fight.)"
	diffOptions = [ "Take the Ghost ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "GhostMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Ghost ring!" ) {
			event.actor.grantRing( "17742", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeIronWillMenu( player ) {
	titleString = "Iron Will"
	descripString = "When you fight a foe using Sleep, Root, Fear or other Willpower-based ability, Iron Will erects defenses around your mind (or the minds of any of your friends) to help you resist their evil influence. Higher Rage Ranks amplifies your mind still further, allowing you to Deflect occasional incoming attacks."
	diffOptions = [ "Take the Iron Will ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "IronWillMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Iron Will ring!" ) {
			event.actor.grantRing( "17744", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKeenAyeMenu( player ) {
	titleString = "Keen Aye"
	descripString = "Use this on you or a friend to help them spy out where a foe *will* be, letting you hit it more easily. (Accuracy) Higher Rage Ranks increase the Accuracy boost. (Accuracy bonuses also increase the chance that you will Critical Hit a monster on any particular attack.)"
	diffOptions = [ "Take the Keen Aye ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KeenAyeMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Keen Aye ring!" ) {
			event.actor.grantRing( "17740", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeMyDensityMenu( player ) {
	titleString = "My Density"
	descripString = "Are you getting knocked around by monsters? There's an easy way to solve that. Weigh more! Using this ring increases your Weight and sticks you to the ground. Higher Rage Ranks actually make you dense enough to resist some damage directly! (Persistent Armor)"
	diffOptions = [ "Take the My Density ring!", "Buff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "MyDensityMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the My Density ring!" ) {
			event.actor.grantRing( "17745", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Buff Menu" ) {
			makeBuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
//====================================================
// DEBUFFS                                            
//====================================================
def makeDebuffMenu( player ) {
	titleString = "Debuff Menu"
	descripString = "Click a ring to find out more about it."
	diffOptions = [ "Adrenaline", "Knife Sharpen", "Main Menu" ]
	
	uiButtonMenu( player, "debuffMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

def makeAdrenalineMenu( player ) {
	titleString = "Adrenaline"
	descripString = "You jump up the nerves of your foe, causing them to jitter and shake, spoiling their ability to Dodge your blows for a time and causing them some damage. Higher Rage Ranks increase the Dodge penalty and deal more damage."
	diffOptions = [ "Take the Adrenaline ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "AdrenalineMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Adrenaline ring!" ) {
			event.actor.grantRing( "17741", CL, CLdecimal, true )
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}
	
def makeKnifeSharpenMenu( player ) {
	titleString = "Knife Sharpen"
	descripString = "You draw the keen edge from a foe's G'hi and use it to sharpen your own metaphorical knives. Your foe suffers an Accuracy drain for a short time as you disrupt its lifeforce and suffers some damage. Higher Rage Ranks increase the Accuracy penalty and deal more damage."
	diffOptions = [ "Take the Knife Sharpen ring!", "Debuff Menu", "Main Menu" ]
	
	uiButtonMenu( player, "KnifeSharpenMenu", titleString, descripString, diffOptions, dialogBoxWidth ) { event ->
		if( event.selection == "Take the Knife Sharpen ring!" ) {
			event.actor.grantRing( "17739", CL, CLdecimal, true )
			player = event.actor
			endRingGrant( event )
		}
		if( event.selection == "Debuff Menu" ) {
			makeDebuffMenu( player )
		}
		if( event.selection == "Main Menu" ) {
			makeMainMenu( player )
		}
	}
}

//====================================================
// RING GRANT END LOGIC                               
//====================================================

def endRingGrant( event ) {
	event.actor.updateQuest(88, "BFG-Remo")
	event.actor.updateQuest(251, "BFG-Remo")
	event.actor.setQuestFlag( GLOBAL, "Z02SecondRingGrantReceived" )
	event.actor.setQuestFlag( GLOBAL, "Z02FinishedBehindEnemyLines" )
	myManager.schedule(3) { Remo.pushDialog( event.actor, "postQuestDefault" ) }
}
